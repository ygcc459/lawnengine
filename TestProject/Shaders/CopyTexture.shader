Root
	_Type "Shader"
	
	technique
		name "CopyTexture"
		pass
			vs_entry "vsMain"
			ps_entry "psMain"
			Cull CullNone
			BlendEnable false
			DepthEnable false
			DepthWrite false
			DepthTest Always
		
	source """

struct VsIn
{
	float2 pos : POSITION;
	float2 uv : TEXCOORD0;
};

struct VsOut
{
	float4 pos : SV_POSITION;
	float2 uv : TEXCOORD0;
};

VsOut vsMain(VsIn input)
{
	VsOut output = (VsOut)0;
	output.pos = float4(input.pos.x, input.pos.y, 0, 1);
	output.uv = input.uv;
	return output;
}

Texture2D sourceTexture;
SamplerState sourceTextureSampler;

float4 psMain(VsOut input) : SV_TARGET
{
	float4 color = sourceTexture.Sample(sourceTextureSampler, input.uv);
	return color;
}

"""
