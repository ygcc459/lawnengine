Root
	_Type "Shader"
	BlendMode "Opaque"
	
	technique
		name "ForwardBasePass"
		pass
			vs_entry "VertexMain"
			ps_entry "PixelMain"
			
			Cull			CullNone
			
			BlendEnable		false
			
			WriteMask		rgba
			
			DepthEnable		false
			DepthWrite		false
			DepthTest		Always
		
	source """

#include "Common.cginc"

Texture2D skyboxTex;
SamplerState skyboxTexSampler;

float skyboxIntensity;
float skyboxRotation;

float3 cameraPos;
float4x4 viewProjMatrix;

struct VsIn
{
	float3 pos : POSITION;
};

struct VsOut
{
	float4 pos : SV_POSITION;
	float3 dir : TEXCOORD0;
};

VsOut VertexMain(VsIn input)
{
	VsOut output = (VsOut)0;
	
	float3 worldPos = input.pos + cameraPos;

	output.pos = mul(viewProjMatrix, float4(worldPos, 1));

	output.dir = normalize(input.pos);

	return output;
}

float4 PixelMain(VsOut input) : SV_TARGET
{
	float3 normalizedDir = normalize(input.dir);
	float longitude = (atan2(normalizedDir.z, normalizedDir.x) / PI) * 0.5 + 0.5;
	float latitude = -(asin(normalizedDir.y) / (PI * 0.5)) * 0.5 + 0.5;

	float2 uv = float2(longitude + skyboxRotation, latitude);

	float3 color = skyboxTex.Sample(skyboxTexSampler, uv).rgb * skyboxIntensity;

	return float4(color, 1);
}

"""
