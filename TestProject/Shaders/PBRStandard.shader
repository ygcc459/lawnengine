Root
	_Type "Shader"
	BlendMode "Opaque"

	properties
		BaseColor
			Type Color
			Default "1 0.5 0.3 1"
		Roughness
			Type Float
			Default "0.5"
			Range 0.01 1
		Metallic
			Type Float
			Default "0.5"
			Range 0 1
		Specular
			Type Float
			Default "0.5"
			Range 0 1
		BaseColorTex
			Type Texture2D
			Default "color_texture 1.0 1.0 1.0 1.0"
		NormalTex
			Type Texture2D
			Default "color_texture 0.5 0.5 1.0 1.0"
		RoughnessMetallicAOTex
			Type Texture2D
			Default "color_texture 1.0 1.0 1.0 1.0"

	technique
		name "ForwardBasePass"

		variants
			dimension DiffuseIndirectType
				value DiffuseIndirect_SH
				value DiffuseIndirect_Color
				value DiffuseIndirect_Texture
				
		pass
			vs_entry "VertexMain"
			ps_entry "PixelMain"
			
			Cull			CullBack
			FrontIsCCW		true
			
			BlendEnable		false
			SrcBlend		SrcAlpha
			DstBlend		InvSrcAlpha
			BlendOp			Add
			SrcBlendAlpha	Zero
			DstBlendAlpha	Zero
			BlendOpAlpha	Add
			
			WriteMask		rgba
			
			DepthEnable		true
			DepthWrite		true
			DepthTest		LessEqual
			DepthBias		0
			DepthBiasClamp	0.0
			
			SlopeScaledDepthBias 0.0
			DepthClipEnable true

			SamplerFromTexture BaseColorTexSampler BaseColorTex
			SamplerFromTexture NormalTexSampler NormalTex
			SamplerFromTexture RoughnessMetallicAOTexSampler RoughnessMetallicAOTex
			SamplerFromTexture diffuseIndirectSampler diffuseIndirectTexture

			ObjectParams
				cbuffer object_params
				
			MaterialParams
				cbuffer material_params

				resource BaseColorTex
				sampler BaseColorTexSampler

				resource NormalTex
				sampler NormalTexSampler

				resource RoughnessMetallicAOTex
				sampler RoughnessMetallicAOTexSampler
		
			PassParams
				cbuffer pass_params

				resource diffuseIndirectTexture
				sampler diffuseIndirectSampler

	source """

#include "ShadingModelDefault.cginc"

cbuffer material_params
{
	float3 BaseColor = float3(1, 1, 1);
	float Roughness = 0.5;
	float Specular = 1;
	float Metallic = 1;
}

Texture2D BaseColorTex;
SamplerState BaseColorTexSampler;

Texture2D NormalTex;
SamplerState NormalTexSampler;

Texture2D RoughnessMetallicAOTex;
SamplerState RoughnessMetallicAOTexSampler;

MaterialParams CalcMaterialParams(VsOut Input)
{
	float4 BaseColorSample = BaseColorTex.Sample(BaseColorTexSampler, Input.uv);
	float4 NormalTexSample = NormalTex.Sample(NormalTexSampler, Input.uv);
	float4 RoughnessMetallicAOTexSample = RoughnessMetallicAOTex.Sample(RoughnessMetallicAOTexSampler, Input.uv);

	MaterialParams Mat;
	Mat.BaseColor = BaseColorSample.rgb * BaseColor.rgb;
	Mat.Roughness = RoughnessMetallicAOTexSample.x * Roughness;
	Mat.Metallic = RoughnessMetallicAOTexSample.y * Metallic;
	Mat.Specular = Specular;
	Mat.AO = RoughnessMetallicAOTexSample.z;
	Mat.Normal = NormalTexSample.xyz * 2 - 1;

	return Mat;
}

"""
