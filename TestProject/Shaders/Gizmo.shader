Root
	_Type "Shader"
	BlendMode "Opaque"
	
	technique
		name "ForwardBasePass"
		pass
			vs_entry "VertexMain"
			ps_entry "PixelMain"
			
			Cull			CullBack
			FrontIsCCW		true
			
			BlendEnable		true
			SrcBlend		SrcAlpha
			DstBlend		InvSrcAlpha
			BlendOp			Add
			SrcBlendAlpha	Zero
			DstBlendAlpha	Zero
			BlendOpAlpha	Add
			
			WriteMask		rgba
			
			DepthEnable		true
			DepthWrite		true
			DepthTest		LessEqual
			DepthBias		0
			DepthBiasClamp	0.0
			
			SlopeScaledDepthBias 0.0
			DepthClipEnable true

			BaseColorTex_default color_texture 1.0 1.0 1.0 1.0
			NormalTex_default color_texture 0.5 0.5 0.5 1.0
			RoughnessMetallicAOTex_default color_texture 1.0 1.0 1.0 1.0
		
	source """

#include "Common.cginc"

cbuffer object_params
{
	float4x4 worldMatrix;
	float4 tintColor = float4(1, 1, 1, 1);
}

cbuffer pass_params
{
	float4x4 viewProjMatrix;
	float3 cameraPos;
}

cbuffer material_params
{
}

struct VsIn
{
	float3 pos : POSITION;
	float4 color : COLOR0;
};

struct VsOut
{
	float4 pos : SV_POSITION;
	float4 color : TEXCOORD0;
	float3 worldPos : TEXCOORD1;
};

VsOut VertexMain(VsIn input)
{
	VsOut output = (VsOut)0;
	
	float3 worldPos = mul(worldMatrix, float4(input.pos, 1)).xyz;

	output.pos = mul(viewProjMatrix, float4(worldPos, 1));

	output.color = input.color;
	
	output.worldPos = worldPos;

	return output;
}

float4 PixelMain(VsOut Input) : SV_TARGET
{
	float4 Color = Input.color * tintColor;
	
	float3 vecToCam = cameraPos - Input.worldPos;
	float distanceFade = 1 - saturate(length(vecToCam.xz) / 75.0f);

	Color.a *= pow(distanceFade, 3);

	return Color;
}

"""
