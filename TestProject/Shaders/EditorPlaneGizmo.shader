Root
	_Type "Shader"
	BlendMode "Transparent"
	
	technique
		name "ForwardBasePass"
		pass
			vs_entry "VertexMain"
			ps_entry "PixelMain"
			
			Cull			CullNone
			
			BlendEnable		true
			SrcBlend		SrcAlpha
			DstBlend		InvSrcAlpha
			BlendOp			Add
			SrcBlendAlpha	Zero
			DstBlendAlpha	Zero
			BlendOpAlpha	Add
			
			WriteMask		rgba
			
			DepthEnable		true
			DepthWrite		false
			DepthTest		LessEqual
			DepthBias		0
			DepthBiasClamp	0.0
		
	source """

#include "Common.cginc"

cbuffer object_params
{
	float4x4 worldMatrix;
	float4 tintColor = float4(1, 0, 0, 1);
}

cbuffer pass_params
{
	float4x4 viewProjMatrix;
	float4x4 viewMatrix;
	float3 cameraPos;
	float4 cameraParams;  //znear, zfar, fov, aspect (w/h)
}

cbuffer material_params
{
}

struct VsIn
{
	float3 pos : POSITION;
	float4 color : COLOR0;
};

struct VsOut
{
	float4 pos : SV_POSITION;
	float4 color : TEXCOORD0;
	float3 worldPos : TEXCOORD1;
};

VsOut VertexMain(VsIn input)
{
	VsOut output = (VsOut)0;
	
	float3 worldPos = mul(worldMatrix, float4(input.pos, 1)).xyz;

	output.pos = mul(viewProjMatrix, float4(worldPos, 1));

	output.color = input.color;

	output.worldPos = worldPos;
	
	return output;
}

float projectSphere( /* sphere        */ in float4 sph, 
                     /* camera matrix */ in float4x4 worldToCamera,
                     /* projection    */ in float fle )
{
    // transform to camera space    
    float3 o = mul(worldToCamera, float4(sph.xyz, 1.0)).xyz;

    float r2 = sph.w * sph.w;
    float z2 = o.z * o.z;    
    float l2 = dot(o, o);

    float area = -3.141593 * fle * fle * r2 * sqrt(abs((l2-r2)/(r2-z2))) / (r2-z2);

    return area;
}

float4 PixelMain(VsOut Input) : SV_TARGET
{
	float unitSphereProjectedArea = projectSphere(float4(Input.worldPos, 1.0f), viewMatrix, cameraParams.x);

	//float lineWidth = 0.0025f;
	float lineWidth = lerp(0.1f, 0.0020f, saturate(pow(unitSphereProjectedArea, 0.5) * 40000.0f));

	float2 pos2d = Input.worldPos.xz;
	float2 distanceToGrid = abs(pos2d - round(pos2d));
	float2 isGrid = smoothstep(1 - lineWidth, 1, 1 - distanceToGrid);

	float3 vecToCam = cameraPos - Input.worldPos;
	float distanceFade = 1 - saturate(length(vecToCam.xz) / 50.0f);
	
	float4 color1 = float4(0, 0, 0, 0);
	float4 color2 = float4(0.6, 0.6, 0.6, 1);
	float4 color = lerp(color1, color2, any(isGrid));

	color.a *= distanceFade;

	//return lerp(float4(0, 0, 0, 1), float4(1, 1, 1, 1), unitSphereProjectedArea * 40000.0f);

	return color;
}

"""
