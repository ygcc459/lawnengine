
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// math

#define PI 3.1415926535897932f

float Pow2(float x)
{
	float xx = x*x;
	return xx;
}
float Pow3(float x)
{
	float xx = x*x;
	return xx * x;
}
float Pow4(float x)
{
	float xx = x*x;
	return xx * xx;
}
float Pow5(float x)
{
	float xx = x*x;
	return xx * xx * x;
}

void CalcTangentSpace(out float3 tangentToWorld[3], float3 worldNormal, float3 worldTangent, float tangentSign)
{
    float3 binormal = cross(worldNormal, worldTangent) * tangentSign;
    tangentToWorld[0] = worldTangent;
    tangentToWorld[1] = binormal;
    tangentToWorld[2] = worldNormal;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// lighting

half3 EnvBRDFApprox(half3 SpecularColor, half Roughness, half NoV)
{
	// [ Lazarov 2013, "Getting More Physical in Call of Duty: Black Ops II" ]
	// Adaptation to fit our G term.
	const half4 c0 = { -1, -0.0275, -0.572, 0.022 };
	const half4 c1 = { 1, 0.0425, 1.04, -0.04 };
	half4 r = Roughness * c0 + c1;
	half a004 = min( r.x * r.x, exp2( -9.28 * NoV ) ) * r.x + r.y;
	half2 AB = half2( -1.04, 1.04 ) * a004 + r.zw;

	// Anything less than 2% is physically impossible and is instead considered to be shadowing
	// Note: this is needed for the 'specular' show flag to work, since it uses a SpecularColor of 0
	AB.y *= saturate( 50.0 * SpecularColor.g );

	return SpecularColor * AB.x + AB.y;
}

// GGX / Trowbridge-Reitz
// [Walter et al. 2007, "Microfacet models for refraction through rough surfaces"]
float D_GGX( float a2, float NoH )
{
	float d = ( NoH * a2 - NoH ) * NoH + 1;	// 2 mad
	return a2 / ( PI*d*d );					// 4 mul, 1 rcp
}

// Appoximation of joint Smith term for GGX
// [Heitz 2014, "Understanding the Masking-Shadowing Function in Microfacet-Based BRDFs"]
float Vis_SmithJointApprox(float a2, float NoV, float NoL)
{
	float a = sqrt(a2);
	float Vis_SmithV = NoL * (NoV * (1 - a) + a);
	float Vis_SmithL = NoV * (NoL * (1 - a) + a);
	float v = max(Vis_SmithV + Vis_SmithL,0.001);
	return 0.5 * rcp(v);
}

// [Schlick 1994, "An Inexpensive BRDF Model for Physically-Based Rendering"]
float3 F_Schlick(float3 SpecularColor, float VoH)
{
	float Fc = Pow5(1 - VoH);					// 1 sub, 3 mul
	//return Fc + (1 - Fc) * SpecularColor;		// 1 add, 3 mad

	// Anything less than 2% is physically impossible and is instead considered to be shadowing
	return saturate(50.0 * SpecularColor.g) * Fc + (1 - Fc) * SpecularColor;
}

float3 CalcSpecularGGX(float3 SpecularColor, half Roughness, half RoughnessWithClamp, float NoH, float NoV, float NoL, float VoH, float3 H, float3 N)
{
	float Alpha = Roughness * Roughness;
	float a2 = Alpha * Alpha;
	float D = D_GGX(a2, NoH);
	float Vis = Vis_SmithJointApprox(a2, NoV, NoL);

	float3 F = F_Schlick(SpecularColor, VoH);

	return (D * Vis) * F;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// spherical harmonics

struct sh2f
{
	float4 packed0;
};

sh2f sh2f_eval_basis(float3 direction)
{
	float4 basis_packed0 = float4(0.282095, -0.488603, 0.488603, -0.488603);

	float4 ds0 = float4(1, direction.x, direction.y, direction.z);

	sh2f sh;
	sh.packed0 = basis_packed0 * ds0;
	return sh;
}

float sh2f_eval_value(sh2f sh, float3 direction)
{
	float4 basis_packed0 = float4(0.282095, -0.488603, 0.488603, -0.488603);

	float4 ds0 = float4(1, direction.x, direction.y, direction.z);

	float sum;
	sum = dot(basis_packed0 * sh.packed0, ds0);
	return sum;
}

struct sh3f
{
	float4 packed0;
	float4 packed1;
	float packed2;
};

sh3f sh3f_eval_basis(float3 direction)
{
	float3 directionSquared = direction * direction;

	sh3f sh;
	sh.packed0[0] = 0.282095f;
	sh.packed0[1] = -0.488603f * direction.y;
	sh.packed0[2] = 0.488603f * direction.z;
	sh.packed0[3] = -0.488603f * direction.x;

	sh.packed1[4] = 1.092548f * (direction.x * direction.y);
	sh.packed1[5] = -1.092548f * (direction.y * direction.z);
	sh.packed1[6] = 0.315392f * (3.0f * directionSquared.z - 1.0f);
	sh.packed1[7] = -1.092548f * (direction.x * direction.z);

	sh.packed2 = 0.546274f * (directionSquared.x - directionSquared.y);

	return sh;
}

float sh3f_eval_value(sh3f sh, float3 direction)
{
#if 1  //original form

	float3 directionSquared = direction * direction;

	float sum;

	sum += 0.282095f * sh.packed0[0];
	sum += -0.488603f * direction.y * sh.packed0[1];
	sum += 0.488603f * direction.z * sh.packed0[2];
	sum += -0.488603f * direction.x * sh.packed0[3];

	sum += 1.092548f * (direction.x * direction.y) * sh.packed1[0];
	sum += -1.092548f * (direction.y * direction.z) * sh.packed1[1];
	sum += 0.315392f * (3.0f * directionSquared.z - 1.0f) * sh.packed1[2];
	sum += -1.092548f * (direction.x * direction.z) * sh.packed1[3];

	sum += 0.546274f * (directionSquared.x - directionSquared.y) * sh.packed2;
	return sum;

#else  //optimized form

	float4 basis_packed0 = float4(0.282095, -0.488603, 0.488603, -0.488603);
	float4 basis_packed1 = float4(1.092548, -1.092548, 0.315392, -1.092548);
	float basis_packed2 = 0.546274f;

	float4 ds0 = float4(1, direction.x, direction.y, direction.z);

	float4 ds1 = direction.xyzz * direction.yzzx;
	ds1.z = 3 * ds1.z - 1;

	float ds2 = direction.x * direction.x - direction.y * direction.y;

	float sum;
	sum = dot(basis_packed0 * sh.packed0, ds0);
	sum += dot(basis_packed1 * sh.packed1, ds1);
	sum += (basis_packed2 * sh.packed2) * ds2;
	return sum;
#endif
}

struct sh2frgb
{
	float4 r_packed0;
	float4 g_packed0;
	float4 b_packed0;
};

float3 sh2frgb_eval_value(sh2frgb sh, float3 direction)
{
	float4 basis_packed0 = float4(0.282095, -0.488603, 0.488603, -0.488603);

	float4 ds0 = float4(1, direction.x, direction.y, direction.z);

	float3 color;
	color.r = dot(basis_packed0 * sh.r_packed0, ds0);
	color.g = dot(basis_packed0 * sh.g_packed0, ds0);
	color.b = dot(basis_packed0 * sh.b_packed0, ds0);
	return color;
}

struct sh3frgb
{
	float4 r_packed0;
	float4 r_packed1;

	float4 g_packed0;
	float4 g_packed1;

	float4 b_packed0;
	float4 b_packed1;

	float3 rgb_packed2;
	float reserved;
};

float3 sh3frgb_eval_value(sh3frgb sh, float3 direction)
{
	float4 basis_packed0 = float4(0.282095, -0.488603, 0.488603, -0.488603);
	float4 basis_packed1 = float4(1.092548, -1.092548, 0.315392, -1.092548);
	float basis_packed2 = 0.546274f;

	float4 ds0 = float4(1, direction.y, direction.z, direction.x);

	float4 ds1 = direction.xyzz * direction.yzzx;
	ds1.z = 3 * ds1.z - 1;

	float ds2 = direction.x * direction.x - direction.y * direction.y;

	float3 color;
	color.r = dot(basis_packed0 * sh.r_packed0, ds0);
	color.r += dot(basis_packed1 * sh.r_packed1, ds1);
	color.r += (basis_packed2 * sh.rgb_packed2.r) * ds2;

	color.g = dot(basis_packed0 * sh.g_packed0, ds0);
	color.g += dot(basis_packed1 * sh.g_packed1, ds1);
	color.g += (basis_packed2 * sh.rgb_packed2.g) * ds2;

	color.b = dot(basis_packed0 * sh.b_packed0, ds0);
	color.b += dot(basis_packed1 * sh.b_packed1, ds1);
	color.b += (basis_packed2 * sh.rgb_packed2.b) * ds2;

//	color.r += 0.282095 * sh.r_packed0.x;
//	color.r += -0.488603 * direction.y * sh.r_packed0.y;
//	color.r += 0.488603 * direction.z * sh.r_packed0.z;
//	color.r += -0.488603 * direction.x * sh.r_packed0.w;
//
//	color.g += 0.282095 * sh.g_packed0.x;
//	color.g += -0.488603 * direction.y * sh.g_packed0.y;
//	color.g += 0.488603 * direction.z * sh.g_packed0.z;
//	color.g += -0.488603 * direction.x * sh.g_packed0.w;
//
//	color.b += 0.282095 * sh.b_packed0.x;
//	color.b += -0.488603 * direction.y * sh.b_packed0.y;
//	color.b += 0.488603 * direction.z * sh.b_packed0.z;
//	color.b += -0.488603 * direction.x * sh.b_packed0.w;

	//float sum = 0;
	//sum += 0.282095 * sh.r_packed0.x;
	//sum += -0.488603 * direction.y * sh.r_packed0.y;
	//sum += 0.488603 * direction.z * sh.r_packed0.z;
	//sum += -0.488603 * direction.x * sh.r_packed0.w;
	//return float3(sum, sum, sum);

	return color;
}
