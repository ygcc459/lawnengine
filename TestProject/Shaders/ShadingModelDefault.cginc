
#include "Common.cginc"

cbuffer object_params
{
	float4x4 worldMatrix;
	float4x4 worldViewProjMatrix;
}

cbuffer pass_params
{
	float4x4 viewProjMatrix;

	float3 cameraPos;

	float3 mainLightColor;
	float3 mainLightDir;

	float3 diffuseIndirectColor;

	sh3frgb diffuseIndirectSH;
}

Texture2D diffuseIndirectTexture;
SamplerState diffuseIndirectSampler;

struct VsIn
{
	float3 pos : POSITION;
	float3 normal : NORMAL;
    float4 tangent : TANGENT;
	float2 uv : TEXCOORD0;
};

struct VsOut
{
	float4 pos : SV_POSITION;
	float2 uv : TEXCOORD0;
    float3 tangentToWorld[3] : TEXCOORD1;
	float3 worldPos : TEXCOORD4;
};

VsOut VertexMain(VsIn input)
{
	VsOut output = (VsOut)0;
	
	float3 worldPos = mul(worldMatrix, float4(input.pos, 1)).xyz;

	float3 worldNormal = normalize(mul((float3x3)worldMatrix, input.normal));
	float3 worldTangent = normalize(mul((float3x3)worldMatrix, input.tangent.xyz));

	CalcTangentSpace(output.tangentToWorld, worldNormal, worldTangent, input.tangent.w);
	
	output.pos = mul(viewProjMatrix, float4(worldPos, 1));
	//output.pos = float4(input.uv.x, input.uv.y, 0, 1);

	output.uv = input.uv;

	output.worldPos = worldPos;
	
	return output;
}

struct MaterialParams
{
	float3 BaseColor;
	float Roughness;
	float Specular;
	float Metallic;
	float AO;
	float3 Normal;
};

struct ShadingModelContext
{
	float3 DiffuseColor;
	float3 SpecularColor;

	float RoughnessWithClamp;

	float3 N;
	float3 V;
	float3 L;
	float3 H;

	float NoL;
	float NoV;
	float NoH;
	float VoH;

	float Shadow;
};

MaterialParams CalcMaterialParams(VsOut Input);

float4 PixelMain(VsOut Input) : SV_TARGET
{
	MaterialParams Mat = CalcMaterialParams(Input);

	ShadingModelContext Context;
	
	// The smallest normalized value that can be represented in IEEE 754 (FP16) is 2^-14 = 6.1e-5.
	// The code will make the following computation involving roughness: 1.0 / Roughness^4.
	// Therefore to prevent division by zero on devices that do not support denormals, Roughness^4
	// must be >= 6.1e-5. We will clamp to 0.09 because 0.09^4 = 6.5e-5.
	// Increase value from 0.09 to 0.12 to fix missing specular lobe problem on device
	//
	// Note that we also clamp to 1.0 to match the deferred renderer on PC where the roughness is 
	// stored in an 8-bit value and thus automatically clamped at 1.0.
	// To help with IBL cube sample, we postpone the clamp only when calculate GGX_Mobile.
	Context.RoughnessWithClamp = max(0.12, Mat.Roughness);

	float3x3 tangentToWorldMatrix = float3x3(Input.tangentToWorld[0], Input.tangentToWorld[1], Input.tangentToWorld[2]);
	Context.N = normalize(mul(Mat.Normal, tangentToWorldMatrix));

	Context.V = normalize(cameraPos - Input.worldPos);
	Context.L = mainLightDir;
	Context.H = normalize(Context.V + Context.L);
	
	Context.NoL = max(dot(Context.N, Context.L), 0);
	Context.NoV = max(dot(Context.N, Context.V), 0);
	Context.NoH = max(dot(Context.N, Context.H), 0);
	Context.VoH = max(dot(Context.V, Context.H), 0);

	float DielectricSpecular = 0.08 * Mat.Specular;
	Context.DiffuseColor = Mat.BaseColor - Mat.BaseColor * Mat.Metallic;	// 1 mad
	Context.SpecularColor = (DielectricSpecular - DielectricSpecular * Mat.Metallic) + Mat.BaseColor * Mat.Metallic;	// 2 mad

	Context.SpecularColor = EnvBRDFApprox(Context.SpecularColor, Mat.Roughness, Context.NoV);

	Context.Shadow = 1;
	
	half3 DiffuseLighting = Context.DiffuseColor;

	half3 SpecularLighting = Context.SpecularColor * CalcSpecularGGX(Context.SpecularColor, Mat.Roughness, Context.RoughnessWithClamp, 
																		Context.NoH, Context.NoV, Context.NoL, Context.VoH, Context.H, Context.N);

	half3 DiffuseIndirectLighting;

#if DiffuseIndirectType == DiffuseIndirect_Color
	DiffuseIndirectLighting = diffuseIndirectColor;

#elif DiffuseIndirectType == DiffuseIndirect_SH
	DiffuseIndirectLighting = sh3frgb_eval_value(diffuseIndirectSH, Context.N);
	DiffuseIndirectLighting *= diffuseIndirectColor;

#elif DiffuseIndirectType == DiffuseIndirect_Texture
	float longitude = (atan2(Context.N.z, Context.N.x) / PI) * 0.5 + 0.5;
	float latitude = (asin(Context.N.y) / (PI * 0.5)) * 0.5 + 0.5;
	DiffuseIndirectLighting = diffuseIndirectTexture.Sample(diffuseIndirectSampler, float2(longitude, latitude)).rgb;
	DiffuseIndirectLighting *= diffuseIndirectColor;

#endif

	//return float4(DiffuseIndirectLighting, 1);

	half3 Color = 0;

	Color += (Context.Shadow * Context.NoL) * mainLightColor.rgb * (DiffuseLighting + SpecularLighting);

	//Color += DiffuseIndirectLighting;

	return float4(Color, 1);
}
