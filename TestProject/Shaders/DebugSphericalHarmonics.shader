Root
	_Type "Shader"
	BlendMode "Opaque"
	
	technique
		name "ForwardBasePass"
		pass
			vs_entry "VertexMain"
			ps_entry "PixelMain"
			
			Cull			CullBack
			FrontIsCCW		true
			
			BlendEnable		false
			
			WriteMask		rgba
			
			DepthEnable		true
			DepthWrite		true
			DepthTest		LessEqual
			
	source """

#include "Common.cginc"

cbuffer object_params
{
	float4x4 worldMatrix;
	float4 tintColor = float4(1, 1, 1, 1);
	sh3frgb value_sh3frgb;
}

cbuffer pass_params
{
	float4x4 viewProjMatrix;
}

cbuffer material_params
{
}

struct VsIn
{
	float3 pos : POSITION;
	float3 normal : NORMAL;
};

struct VsOut
{
	float4 pos : SV_POSITION;
	float3 worldNormal : TEXCOORD1;
};

VsOut VertexMain(VsIn input)
{
	VsOut output = (VsOut)0;
	
	float3 worldPos = mul(worldMatrix, float4(input.pos, 1)).xyz;
	float3 worldNormal = normalize(mul((float3x3)worldMatrix, input.normal));

	output.pos = mul(viewProjMatrix, float4(worldPos, 1));

	output.worldNormal = worldNormal;

	return output;
}

float4 PixelMain(VsOut Input) : SV_TARGET
{
	float3 shcolor = sh3frgb_eval_value(value_sh3frgb, Input.worldNormal);

	return float4(shcolor * tintColor.rgb, tintColor.a);
}

"""
