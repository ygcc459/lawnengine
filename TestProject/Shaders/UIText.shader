Root
	_Type "Shader"
	
	technique
		name "UIRenderPass"
		pass
			vs_entry "VertexMain"
			ps_entry "PixelMain"
			
			Cull			CullNone
			
			BlendEnable		true
			SrcBlend		SrcAlpha
			DstBlend		InvSrcAlpha
			BlendOp			Add
			SrcBlendAlpha	Zero
			DstBlendAlpha	Zero
			BlendOpAlpha	Add
			
			WriteMask		rgba
			
			DepthEnable		false
			DepthWrite		false
			DepthTest		Always
		
	source """

Texture2D UITex;
SamplerState UITexSampler;

float2 UITexSize;
float2 RenderTargetSize;

struct VsIn
{
	float3 pos : POSITION;
	float2 uv : TEXCOORD0;
	float4 color : COLOR;
};

struct VsOut
{
	float4 pos : SV_POSITION;
	float2 uv : TEXCOORD0;
	float4 color : TEXCOORD1;
};

VsOut VertexMain(VsIn input)
{
	VsOut output = (VsOut)0;
	
	output.pos.xy = (input.pos.xy / RenderTargetSize) * 2 - 1;
	output.pos.y *= -1;
	output.pos.z = input.pos.z;
	output.pos.w = 1;

	output.uv = input.uv / UITexSize;

	output.color = input.color;
	
	return output;
}

float4 PixelMain(VsOut input) : SV_TARGET
{
	float a = UITex.Sample(UITexSampler, input.uv).r;

	float4 FinalColor = float4(input.color.rgb, a * input.color.a);
	
	return FinalColor;
}

"""
