
#include <windows.h>
#include "MyApp.h"

#include <vcpp/math.h>
#include <vcpp/Logger.h>
#include <vcpp/Windows.h>

#include "../LawnEngineFramework/engine/EntitySystem/World.h"
#include "../LawnEngineFramework/engine/EntitySystem/SceneObject.h"
#include "../LawnEngineFramework/engine/EntitySystem/Camera.h"

//////////////////////////////////////////////////////////////////////////

#include <AntTweakBar.h>
#pragma comment(lib, "AntTweakBar.lib")

//////////////////////////////////////////////////////////////////////////

std::string chooseLeFile()
{
	std::string file_name("");

	char old_cur_dir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, old_cur_dir);

	char open_file_name[MAX_PATH] = {0};
	OPENFILENAME ofn;
	{
		ofn.lStructSize = sizeof(ofn);
		ofn.hwndOwner = NULL;
		ofn.hInstance = NULL;
		ofn.lpstrFilter = ".le files\0*.le\0\0";
		ofn.lpstrCustomFilter = NULL;
		ofn.nMaxCustFilter = 0;
		ofn.nFilterIndex = 1;
		ofn.lpstrFile = open_file_name;
		ofn.nMaxFile = MAX_PATH;
		ofn.lpstrFileTitle = NULL;
		ofn.nMaxFileTitle = 0;
		ofn.lpstrInitialDir = NULL;
		ofn.lpstrTitle = NULL;
		ofn.Flags = OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST;
		ofn.nFileOffset = 0;
		ofn.nFileExtension = 0;
		ofn.lpstrDefExt = NULL;
		ofn.lCustData = NULL;
		ofn.lpfnHook = NULL;
		ofn.lpTemplateName = NULL;
	}
	if(GetOpenFileName(&ofn)!=0)
		file_name = open_file_name;

	SetCurrentDirectory(old_cur_dir);
	return file_name;
}

AABB sceneAABB(
	-42.0f, 42.0f,
	6.0f, 6.2f,
	-42.0f, 42.0f);

void TW_CALL on_fullscreen(void *clientData)
{ 
	MyApp* _this = (MyApp*)clientData;

	BOOL isfullscreen;

	IDXGIOutput* outputWindow0 = NULL;
	_this->mainRenderView->get_swapchain()->GetFullscreenState(&isfullscreen, &outputWindow0);

	IDXGIOutput* outputWindow = NULL;
	_this->mainRenderView->get_swapchain()->GetContainingOutput(&outputWindow);
	_this->mainRenderView->get_swapchain()->SetFullscreenState(!isfullscreen, outputWindow);
	outputWindow->Release();
}

/*
void TW_CALL on_normalMapping_reload(void *clientData)
{ 
	MyApp* _this = (MyApp*)clientData;

	boost::shared_ptr<RenderableEffect> e = Resource<RenderableEffect>::get_loaded_resource("phong_normalMapping", false);
	if(e)
	{
		e->reload();
	}
}

void TW_CALL on_normalMapping_reloadMaterial(void *clientData)
{ 
	MyApp* _this = (MyApp*)clientData;

	auto e = Resource<Material>::get_loaded_resource("F:/Programming/3D/my projects/LawnEngine/lawnEngineMedia/chessboard_normalMapping/cb_PDX01_-_Default.lm", true);
	if(e)
	{
		e->reload();
	}
}

void TW_CALL on_parallexMapping_reload(void *clientData)
{ 
	MyApp* _this = (MyApp*)clientData;

	boost::shared_ptr<RenderableEffect> e = Resource<RenderableEffect>::get_loaded_resource("phong_parallexMapping", false);
	if(e)
	{
		e->reload();
	}
}

void TW_CALL on_parallexMapping_reloadMaterial(void*clientData)
{
	MyApp* _this = (MyApp*)clientData;

	auto e = Resource<Material>::get_loaded_resource("F:/Programming/3D/my projects/LawnEngine/lawnEngineMedia/chessboard_parallexMapping/cb_PDX01_-_Default.lm", true);
	if(e)
	{
		e->reload();
	}
}

void TW_CALL on_ssvo_reload(void*clientData)
{
	MyApp* _this = (MyApp*)clientData;

	auto e = Resource<Effect>::get_loaded_resource("SSVOPass", false);
	if(e)
	{
		e->reload();
	}
}
*/

bool MyApp::init()
{
	vcpp::log::set_target_printer(new vcpp::Win32DebugOutputPrinter());

	try
	{
		SetCurrentDirectory("../lawnenginemedia/");
		srand((unsigned int)time(NULL));

		//
		// debug aux geom
		//
		AuxGeometryContainer* aux_geom_container = AuxGeometryDrawPass::instance().create_container();
		aux_geom_container->add(AuxLine().from_fixed_point(0, 0, 0).towards_fixed_direction(1, 0, 0, 0.0f, 10.0f).color(1, 0, 0));
		aux_geom_container->add(AuxLine().from_fixed_point(0, 0, 0).towards_fixed_direction(0, 1, 0, 0.0f, 10.0f).color(0, 1, 0));
		aux_geom_container->add(AuxLine().from_fixed_point(0, 0, 0).towards_fixed_direction(0, 0, 1, 0.0f, 10.0f).color(0, 0, 1));

		// create render view
		RECT renderViewClientRect;
		GetClientRect(windowHwnd, &renderViewClientRect);

		this->mainRenderView = new RenderView(renderViewClientRect.right, renderViewClientRect.bottom, windowHwnd, DXGI_FORMAT_R8G8B8A8_UNORM);
		g_lawnEngine.addRenderView(mainRenderView);

		Camera* mainCamera = g_lawnEngine.getWorld().CreateSceneObject()->AddComponent<Camera>();
		mainCamera->SetRenderTarget(mainRenderView);

		g_lawnEngine.getInputManager().addKeyEventListener(this);
		g_lawnEngine.getInputManager().addMouseEventListener(this);

		g_lawnEngine.getInputManager().addKeyEventListener(&freeCameraController);
		g_lawnEngine.getInputManager().addMouseEventListener(&freeCameraController);

		//
		// the camera
		//
		this->freeCameraController.setCamera(pCamera);
		this->freeCameraController.setEnabled(true);

		//
		// lights
		//
		sunlight = g_lawnEngine.getWorld().CreateSceneObject()->AddComponent<DirectionalLight>();
		sunlight->GetSceneObject().Lookat(Vector3(0.0f, 0.0f, 0.0f));
		sunlight->color = float3(0.3f, 0.3f, 0.3f);

		spotLight = g_lawnEngine.getWorld().CreateSceneObject()->AddComponent<SpotLight>();
		spotLight->GetSceneObject().SetLocalPosition(Point3(0, 50, 0));
		spotLight->GetSceneObject().Lookat(Vector3(0.0f, 0.0f, 0.0f));
		spotLight->color = float3(0.3f, 0.3f, 0.3f);
		spotLight->innerConeAngle = degreeToRadian(25.0f);
		spotLight->outerConeAngle = degreeToRadian(45.0f);
		spotLight->generateShadow = true;

		// parameters just hard coded...
//#include "profiles.inl"

		TwInit(TW_DIRECT3D11, RenderSystem::instance().device);
		TwWindowSize(mainRenderView->GetWidth(), mainRenderView->GetHeight());

		mainRenderView->signal_before_window_resize.connect(
			[]()->void {
			TwWindowSize(0, 0);
		});

		mainRenderView->signal_after_window_resize.connect(
			[](uint w, uint h)->void {
			TwWindowSize(w, h);
		});

		mainTweakBar = TwNewBar("MainBar");
		{
			// status
			TwAddVarRO(mainTweakBar, "FPS", TW_TYPE_FLOAT, &g_lawnEngine.fps, " group=status ");
			TwAddButton(mainTweakBar, "Full screen", &on_fullscreen, this, " group=status ");

 			// normal mapping
// 			TwAddButton(mainTweakBar, "normalMapping reload", &on_normalMapping_reload, this, "group=normalMapping");
// 			TwAddButton(mainTweakBar, "normalMapping reload material", &on_normalMapping_reloadMaterial, this, "group=normalMapping");
 
 			// parallex mapping
// 			TwAddButton(mainTweakBar, "parallexMapping reload", &on_parallexMapping_reload, this, "group=parallexMapping");
// 			TwAddButton(mainTweakBar, "parallexMapping reload material", &on_parallexMapping_reloadMaterial, this, "group=parallexMapping");
		
			// vsm
// 			TwAddVarRW(mainTweakBar, "vsm enabled", TW_TYPE_BOOLCPP, &theRenderManager->vsm_enabled, " group=vsm ");
// 			TwAddVarRW(mainTweakBar, "vsm blur length x", TW_TYPE_UINT32, &theRenderManager->vsm_blur_length_x, " group=vsm min=0 max=100");
// 			TwAddVarRW(mainTweakBar, "vsm blur length y", TW_TYPE_UINT32, &theRenderManager->vsm_blur_length_y, " group=vsm min=0 max=100");
// 			TwAddVarRW(mainTweakBar, "vsm reduce light bleeding", TW_TYPE_FLOAT, &theRenderManager->vsm_lightBleeding_reduceAmount, " group=vsm min=0 step=0.001");
// 			TwAddVarRW(mainTweakBar, "vsm depth bias", TW_TYPE_FLOAT, &theRenderManager->vsm_sceneDepthBias, " group=vsm step=0.001");
// 
// 			TwAddVarRW(mainTweakBar, "shadow min distance", TW_TYPE_FLOAT, &theRenderManager->shadowMinDistance, " group=vsm min=0 step=0.01 ");
// 			TwAddVarRW(mainTweakBar, "shadow max distance", TW_TYPE_FLOAT, &theRenderManager->shadowMaxDistance, " group=vsm min=0 step=1 ");

			// ssvo
// 			TwAddVarRW(mainTweakBar, "ssvo enabled", TW_TYPE_BOOLCPP, &theRenderManager->ssvo_enabled, " group=ssvo ");
// 			TwAddVarRW(mainTweakBar, "show ssvo buffer", TW_TYPE_BOOLCPP, &theRenderManager->show_ssvo_only, " group=ssvo ");
// 			TwAddButton(mainTweakBar, "ssvo reload", &on_ssvo_reload, this, "group=ssvo");
// 			TwAddVarRW(mainTweakBar, "ambient amount", TW_TYPE_FLOAT, &theRenderManager->ssvo_ambient_amount, " group=ssvo min=0 max=1.0 step=0.01");
// 			TwAddVarRW(mainTweakBar, "ssvo radius", TW_TYPE_FLOAT, &theRenderManager->ssvo_radius, " group=ssvo min=0 step=0.01");
// 			
// 			TwAddVarRW(mainTweakBar, "ssvo epsilon", TW_TYPE_FLOAT, &theRenderManager->ssvo_epsilon, " group=ssvo min=0 step=0.000005");
// 			TwAddVarRW(mainTweakBar, "ssvo depth_bias", TW_TYPE_FLOAT, &theRenderManager->ssvo_depth_bias, " group=ssvo min=0 step=0.00001");
// 			TwAddVarRW(mainTweakBar, "ssvo reject_dist_sqr", TW_TYPE_FLOAT, &theRenderManager->ssvo_reject_dist_sqr, " group=ssvo min=0 step=0.1");
// 			TwAddVarRW(mainTweakBar, "ssvo intensity_scale", TW_TYPE_FLOAT, &theRenderManager->ssvo_intensity_scale, " group=ssvo min=0 step=0.1");
// 
// 			TwAddVarRW(mainTweakBar, "ssvo blur length x", TW_TYPE_UINT32, &theRenderManager->ssvo_blur_length_x, " group=ssvo min=1 max=20");
// 			TwAddVarRW(mainTweakBar, "ssvo blur mul x", TW_TYPE_FLOAT, &theRenderManager->ssvo_blur_mul_x, " group=ssvo min=0.01 max=100 step=0.01");
// 			TwAddVarRW(mainTweakBar, "ssvo blur length y", TW_TYPE_UINT32, &theRenderManager->ssvo_blur_length_y, " group=ssvo min=1 max=20");
// 			TwAddVarRW(mainTweakBar, "ssvo blur mul y", TW_TYPE_FLOAT, &theRenderManager->ssvo_blur_mul_y, " group=ssvo min=0.01 max=100 step=0.01");

			// hdr
// 			TwAddVarRW(mainTweakBar, "hdr enabled", TW_TYPE_BOOLCPP, &theRenderManager->hdr_enabled, " group=hdr ");
// 			TwAddVarRW(mainTweakBar, "hdr blur length x", TW_TYPE_UINT32, &theRenderManager->hdr_light_blur_length_x, " group=hdr min=1 max=50");
// 			TwAddVarRW(mainTweakBar, "hdr blur length y", TW_TYPE_UINT32, &theRenderManager->hdr_light_blur_length_y, " group=hdr min=1 max=50");
// 			TwAddVarRW(mainTweakBar, "bright_threshold", TW_TYPE_FLOAT, &theRenderManager->hdr_bright_threshold, " group=hdr min=0 step=0.01");
// 			TwAddVarRW(mainTweakBar, "middle_gray", TW_TYPE_FLOAT, &theRenderManager->hdr_middle_gray, " group=hdr min=0 step=0.01");
// 			TwAddVarRW(mainTweakBar, "lum_white", TW_TYPE_FLOAT, &theRenderManager->hdr_lum_white, " group=hdr min=0 step=0.01");
		}

		sceneTweakBar = TwNewBar("scene");
		{
			TwAddVarRW(sceneTweakBar, "camera near plane", TW_TYPE_FLOAT, &pCamera->znear, " group=camera min=0 step=0.05 ");
			TwAddVarRW(sceneTweakBar, "camera far plane", TW_TYPE_FLOAT, &pCamera->zfar, " group=camera min=0 step=1 ");
			TwAddVarRW(sceneTweakBar, "camera pan speed", TW_TYPE_FLOAT, &pCamera->cameraPanSpeed, " group=camera min=0 step=0.05 ");
			TwAddVarRW(sceneTweakBar, "camera rotate speed", TW_TYPE_FLOAT, &pCamera->cameraRotateSpeed, " group=camera min=0 step=0.0005 ");
			
			TwAddVarRW(sceneTweakBar, "sunlight dir", TW_TYPE_DIR3F, &sunlight->direction, " group=sunlight ");
			TwAddVarRW(sceneTweakBar, "sunlight color", TW_TYPE_COLOR3F, &sunlight->color, " group=sunlight ");

			TwAddVarRW(sceneTweakBar, "spotLight pos x", TW_TYPE_FLOAT, &spotLight->position.x, " group=spotLight ");
			TwAddVarRW(sceneTweakBar, "spotLight pos y", TW_TYPE_FLOAT, &spotLight->position.y, " group=spotLight ");
			TwAddVarRW(sceneTweakBar, "spotLight pos z", TW_TYPE_FLOAT, &spotLight->position.z, " group=spotLight ");
			TwAddVarRW(sceneTweakBar, "spotLight dir", TW_TYPE_DIR3F, &spotLight->direction, " group=spotLight ");
			TwAddVarRW(sceneTweakBar, "spotLight color", TW_TYPE_COLOR3F, &spotLight->color, " group=spotLight ");
			TwAddVarRW(sceneTweakBar, "spotLight innerAngle", TW_TYPE_FLOAT, &spotLight->innerConeAngle, " group=spotLight min=0 step=0.01 max=3.14 ");
			TwAddVarRW(sceneTweakBar, "spotLight outerAngle", TW_TYPE_FLOAT, &spotLight->outerConeAngle, " group=spotLight min=0 step=0.01 max=3.14 ");

			//TwAddVarRW(sceneTweakBar, "background color", TW_TYPE_COLOR3F, &renderMgr->sceneBackgroundColor, " group=scene ");

			//TwAddVarRW(sceneTweakBar, "scale x", TW_TYPE_FLOAT, &entity->matrix._11, " group=scene min=0.001 max=1000 step=5 ");
			//TwAddVarRW(sceneTweakBar, "scale y", TW_TYPE_FLOAT, &entity->matrix._22, " group=scene min=0.001 max=1000 step=5 ");
			//TwAddVarRW(sceneTweakBar, "scale z", TW_TYPE_FLOAT, &entity->matrix._33, " group=scene min=0.001 max=1000 step=5 ");
			
			//TwAddVarRW(sceneTweakBar, "translate x", TW_TYPE_FLOAT, &entity->matrix._41, " group=scene ");
			//TwAddVarRW(sceneTweakBar, "translate y", TW_TYPE_FLOAT, &entity->matrix._42, " group=scene ");
			//TwAddVarRW(sceneTweakBar, "translate z", TW_TYPE_FLOAT, &entity->matrix._43, " group=scene ");
		}

// 		for(uint i=0; i<6; i++) {
// 			UIImage* uii = new UIImage("../Media/Selector.dds", vcpp::Rand(20.0f, 620.0f), vcpp::Rand(20.0f, 400.0f), 32, 32);
// 			uiMgr->add(uii);
// 		}
// 
// 		for(uint i=0; i<6; i++) {
// 			UIText* uii = new UIText("hello!", float4(vcpp::Rand(0.3f,0.7f), vcpp::Rand(0.3f,0.7f), vcpp::Rand(0.3f,0.7f), vcpp::Rand(0.4f,1.0f)), vcpp::Rand(20.0f, 620.0f), vcpp::Rand(20.0f, 400.0f), 32, 32);
// 			uiMgr->add(uii);
// 		}

	}
	catch(const Exception& e)
	{
		vcpp::log::err("Error in (%d,%s,%s) : %s", e.src_line_number, e.src_file_name, e.src_function_name, e.message.c_str());

		::MessageBoxA(NULL, e.message.c_str(), NULL, NULL);

		return false;
	}
	
	return true;
}

bool MyApp::destroy()
{
	TwTerminate();

	return true;
}

//////////////////////////////////////////////////////////////////////////

void MyApp::onRender()
{
	mainRenderView->render();

	LawnEngineUtil::setRenderTargets(*mainRenderView, (dxDepthStencilView)NULL);
	TwDraw();

	mainRenderView->get_swapchain()->Present(0, 0);
}

void MyApp::onUpdateScene(float fElapsedTime)
{
}

//////////////////////////////////////////////////////////////////////////

bool MyApp::keyPressed(const KeyEventArg &arg)
{
	return false;
}

bool MyApp::keyReleased(const KeyEventArg &arg)
{
	return false;
}

bool MyApp::mouseMoved(const MouseEvent &arg)
{
	return false;
}

bool MyApp::mousePressed(const MouseEvent &arg, MouseButton btn)
{
	return false;
}

bool MyApp::mouseReleased(const MouseEvent &arg, MouseButton btn)
{
	return false;
}

bool MyApp::onMouseMove(uint x, uint y)
{
 	if(TwMouseMotion(x, y))
 		return true;

	return false;
}

void MyApp::onMouseLeave()
{

}

bool MyApp::onMouseLDown(uint x, uint y)
{
 	if(TwMouseButton(TW_MOUSE_PRESSED, TW_MOUSE_LEFT))
 		return true;

	return false;
}

bool MyApp::onMouseLUp(uint x, uint y)
{
 	if(TwMouseButton(TW_MOUSE_RELEASED, TW_MOUSE_LEFT))
 		return true;

	return false;
}

bool MyApp::onMouseRDown(uint x, uint y)
{
 	if(TwMouseButton(TW_MOUSE_PRESSED, TW_MOUSE_RIGHT))
 		return true;

	return false;
}

bool MyApp::onMouseRUp(uint x, uint y)
{
 	if(TwMouseButton(TW_MOUSE_RELEASED, TW_MOUSE_RIGHT))
 		return true;

	return false;
}

bool MyApp::onMouseWheel(float zDelta, uint x, uint y)
{
 	if(TwMouseWheel((int)zDelta))
 		return true;

	return false;
}

//////////////////////////////////////////////////////////////////////////

bool sendTwKeyMsg(int keyCode)
{
	switch(keyCode)
	{
	case VK_OEM_PERIOD:
		keyCode = '.'; break;
	case VK_DELETE:
		keyCode = TW_KEY_DELETE; break;
	case VK_BACK:
		keyCode = TW_KEY_BACKSPACE; break;
	}

	if(TwKeyPressed(keyCode, TW_KMOD_NONE))
		return true;
	else
		return false;
}

bool MyApp::onKeyDown(int keyCode)
{
	if(sendTwKeyMsg(keyCode))
		return true;

	switch(keyCode)
	{
	case KEY_Y:
		{
			pCamera->setFlyAroundCenter(pCamera->pos);
			return true;
		}
	case KEY_C:
		{
			using vcpp::math::rand;

			Point3 pos(rand(sceneAABB.xmin, sceneAABB.xmax), rand(sceneAABB.ymin, sceneAABB.ymax), rand(sceneAABB.zmin, sceneAABB.zmax));
			float3 color(rand(0.3f, 0.8f), rand(0.3f, 0.8f), rand(0.3f, 0.8f));
			float range = rand(6.0f, 30.0f);
			Vector3 v(rand(-1.0f, 1.0f), 0.0f, rand(-1.0f, 1.0f));
			float speed = rand(3.0f, 15.0f);

			// 				Point3 pos(vcpp::Rand(sceneAABB.xmin, sceneAABB.xmax), vcpp::Rand(sceneAABB.ymin, sceneAABB.ymax), vcpp::Rand(sceneAABB.zmin, sceneAABB.zmax));
			// 				float3 color(vcpp::Rand(0.2f, 0.8f), vcpp::Rand(0.2f, 0.8f), vcpp::Rand(0.2f, 0.8f));
			// 				float range = vcpp::Rand(150.0f, 250.0f);
			// 				Vector3 v(vcpp::Rand(-1.0f, 1.0f), 0.0f, vcpp::Rand(-1.0f, 1.0f));
			// 				float speed = vcpp::Rand(35.0f, 175.0f);
			v.normalize();
			v *= speed;

			PointLight* pointLight(new PointLight(pos, color, range));
			g_lawnEngine.getSceneManager().add_light(pointLight);

			return true;
		}
	case KEY_B:
		{
 			boost::shared_ptr<Effect> e = Resource<Effect>::get_loaded_resource("SSVOPass", false);
 			if(e)
 			{
				try {
 					e->reload();
				}
				catch(const Exception& e)
				{
					vcpp::log::err("Error in (%d,%s,%s) : %s", e.src_line_number, e.src_file_name, e.src_function_name, e.message.c_str());

					::MessageBox(NULL, e.message.c_str(), NULL, NULL);

					return false;
				}
 			}

			return true;
		}
	case KEY_L:
		{
			//sunlight->direction = pCamera->dir;
			spotLight->position = pCamera->pos;
			spotLight->direction = pCamera->dir;
			return true;
		}
	case KEY_M:
		{
			typedef Resource<Material>::ResourceMap Map;
			
			Map& resources = Resource<Material>::get_resource_map();
			
			for(auto it=resources.begin(); it!=resources.end(); ++it)
			{
				boost::weak_ptr<Material>& wp = it->second;
				boost::shared_ptr<Material> p(wp);
				if(p) {
					p->reload();
				}
			}

			return true;
		}
	}

	return false;
}

bool MyApp::onKeyUp(int keyCode)
{
	return false;
}
