#pragma once

#include "../LawnEngineFramework/lawnEngine.h"

#ifdef _DEBUG
#pragma comment(lib, "../LawnEngineFramework/LawnEngineFramework_debug.lib")
#else
#pragma comment(lib, "../LawnEngineFramework/LawnEngineFramework_release.lib")
#endif

typedef struct CTwBar TwBar;

class MyApp : public LawnEnginePlugin, public KeyEventListener, public MouseEventListener
{
public:
	virtual bool init();
	virtual bool destroy();

	virtual bool keyPressed(const KeyEventArg &arg);
	virtual bool keyReleased(const KeyEventArg &arg);
	virtual bool mouseMoved(const MouseEvent &arg);
	virtual bool mousePressed(const MouseEvent &arg, MouseButton btn);
	virtual bool mouseReleased(const MouseEvent &arg, MouseButton btn);

	virtual void onRender();
	virtual void onUpdateScene(float fElapsedTime);

	virtual bool onMouseMove(uint x, uint y);
	virtual void onMouseLeave();

	virtual bool onMouseLDown(uint x, uint y);
	virtual bool onMouseLUp(uint x, uint y);
	virtual bool onMouseRDown(uint x, uint y);
	virtual bool onMouseRUp(uint x, uint y);
	virtual bool onMouseWheel(float zDelta, uint x, uint y);

	virtual bool onKeyDown(int keyCode);
	virtual bool onKeyUp(int keyCode);

public:
	FreeCameraController freeCameraController;

	RenderView* mainRenderView;

	TwBar* mainTweakBar;
	TwBar* sceneTweakBar;

public:
	HWND windowHwnd;

private:
	DirectionalLight* sunlight;
	SpotLight* spotLight;

};

