
#include "MyApp.h"

#include <windows.h>

int renderViewW = 800;
int renderViewH = 600;

//////////////////////////////////////////////////////////////////////////

MyApp * theApp = NULL;

//////////////////////////////////////////////////////////////////////////

int runMessageLoop()
{
	BOOL bGotMsg;
	MSG  msg;
	PeekMessage(&msg, NULL, 0U, 0U, PM_NOREMOVE);

	while(WM_QUIT!=msg.message)
	{
		if(g_lawnEngine.isPaused())
			bGotMsg = GetMessage(&msg, NULL, 0U, 0U);
		else
			bGotMsg = PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE);

		if(bGotMsg)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			g_lawnEngine.stepOneFrame();
		}
	}

	g_lawnEngine.shutdown();

	return (int)msg.wParam;
}

void resizeWindowByClientArea(HWND hwnd, int clientW, int clientH)
{
	RECT originalWindowRect;
	GetWindowRect(hwnd, &originalWindowRect);

	POINT pt1 = {originalWindowRect.left, originalWindowRect.top};
	ScreenToClient(hwnd, &pt1);

	pt1.x = -pt1.x;
	pt1.y = -pt1.y;

	int newW = 2 * pt1.x + clientW;
	int newH = pt1.x + pt1.y + clientH;
	SetWindowPos(hwnd, HWND_TOP, -1, -1, newW, newH, SWP_SHOWWINDOW|SWP_NOMOVE);
}

LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	//if(g_lawnEngine.handleWindowsEvents(hwnd, message, wParam, lParam)==true)
	//	return 0;

	switch (message)
	{
	case WM_ACTIVATEAPP:
		{
			if( ((BOOL)wParam) == TRUE )
				g_lawnEngine.setPaused(false);
			else
				g_lawnEngine.setPaused(true);

			return false;
		}
	case WM_KILLFOCUS:
		{
			g_lawnEngine.setPaused(true);
			return false;
		}
	case WM_SETFOCUS:
		{
			g_lawnEngine.setPaused(false);
			return false;
		}
	case WM_SIZE:
		{
			switch(wParam)
			{
			case SIZE_MAXIMIZED:
			case SIZE_MAXSHOW:
			case SIZE_RESTORED:
				{
					unsigned w = (unsigned)LOWORD(lParam);
					unsigned h = (unsigned)HIWORD(lParam);
					g_lawnEngine.resizeRenderView(hwnd, w, h);
					return false;
				}
			case SIZE_MAXHIDE:
			case SIZE_MINIMIZED:
				{
					return false;
				}
			}
		}
	case WM_CREATE:
		return (0);
	case WM_DESTROY:
		PostQuitMessage (0);
		return (0);
	}

	return DefWindowProc (hwnd, message, wParam, lParam);
}

void MY_CRT_DUMP_CLIENT(void * ptr, size_t size)
{
	__asm{int 3}
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int iCmdShow)
{
	int tmpFlag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
	tmpFlag |= _CRTDBG_LEAK_CHECK_DF;
	_CrtSetDbgFlag(tmpFlag);

	//_CrtSetBreakAlloc(14861);

	//_CrtSetDumpClient(&MY_CRT_DUMP_CLIENT);

    static TCHAR szAppName[] = TEXT ("LawnEngineTest");
    HWND         hwnd;

    WNDCLASSEX   wndclassex = {0};
    wndclassex.cbSize        = sizeof(WNDCLASSEX);
    wndclassex.style         = CS_HREDRAW | CS_VREDRAW;
    wndclassex.lpfnWndProc   = WndProc;
    wndclassex.cbClsExtra    = 0;
    wndclassex.cbWndExtra    = 0;
    wndclassex.hInstance     = hInstance;
    wndclassex.hIcon         = LoadIcon (NULL, IDI_APPLICATION);
    wndclassex.hCursor       = LoadCursor (NULL, IDC_ARROW);
    wndclassex.hbrBackground = (HBRUSH) GetStockObject (WHITE_BRUSH);
    wndclassex.lpszMenuName  = NULL;
    wndclassex.lpszClassName = szAppName;
    wndclassex.hIconSm       = wndclassex.hIcon;
	
    if (!RegisterClassEx (&wndclassex))
    {
        MessageBox (NULL, TEXT ("RegisterClassEx failed!"), szAppName, MB_ICONERROR);
        return 0;
    }

    hwnd = CreateWindowEx (WS_EX_OVERLAPPEDWINDOW, 
		                  szAppName, 
        		          TEXT ("WindowTitle"),
                		  WS_OVERLAPPEDWINDOW,
		                  CW_USEDEFAULT, 
        		          CW_USEDEFAULT, 
                		  CW_USEDEFAULT, 
		                  CW_USEDEFAULT, 
        		          NULL, 
                		  NULL, 
		                  hInstance,
        		          NULL); 
						  
    ShowWindow (hwnd, iCmdShow);
    UpdateWindow (hwnd);

	resizeWindowByClientArea(hwnd, renderViewW, renderViewH);

	theApp = new MyApp();
	theApp->windowHwnd = hwnd;

	int retCode = -1;

	if(g_lawnEngine.startup(hwnd))
	{
		g_lawnEngine.registerPlugin(theApp);

		retCode = runMessageLoop();
	}

	delete theApp;

	return retCode;
}

