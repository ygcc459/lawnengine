#pragma once

#include "render_impl/DeferredLighting/DeferredLighting_LightingPass.h"
#include "render_impl/DeferredLighting/DeferredLightingRenderableEffectPlugin.h"
#include "render_impl/ToLinearDepthPass.h"
#include "render_impl/GenerateNoShadowMapPlugin.h"
#include "render_impl/AuxGeometryDrawPass.h"

#include "render_impl/TextureRenderPass.h"

#include "render_impl/DownSamplePass.h"
#include "render_impl/MeasureLuminancePass.h"
#include "render_impl/hdr/HDRBrightPass.h"
#include "render_impl/hdr/HDRFinalPass.h"
#include "render_impl/OneDirectionBlur.h"


class NextGenRenderManager : public RenderManager
{
public:
	SceneRenderPass pass_scene;
	DepthStencilRenderTarget scene_depth_stencil;

	float3 sceneBackgroundColor;

	RenderableCollector scene_renderables;

	// deferred lighting
	ColorRenderTarget scene_normal_shiness;
	ColorRenderTarget scene_linear_depth;

	ColorRenderTarget light_accumulation;

	DeferredLighting_LightingPass pass_lighting;

	ToLinearDepthPass toLinearDepthPass;

	DeferredLightingRenderableEffectPlugin dl_renderable_effect_plugin;

	// shadow mapping
	GenerateNoShadowMapPlugin shadow_plugin;

	// HDR & post processing
	bool hdr_enabled;
	uint hdr_light_blur_length_x, hdr_light_blur_length_y;

	float hdr_bright_threshold;
	float hdr_middle_gray;
	float hdr_lum_white;

	ColorRenderTarget hdr_image;
	ColorRenderTarget greyed_hdr_image;
	ColorRenderTarget lighted_area;
	ColorRenderTarget half_blurred_lighted_area;
	ColorRenderTarget fully_blurred_lighted_area;

	MeasureLuminancePass measureLuminancePass;
	DownSamplePass downSamplePass;
	HDRBrightPass brightPass;
	OneDirectionBlurPass blurPass;
	HDRFinalPass toneMappingPass;

public:
	NextGenRenderManager(SceneManager* sceneMgr, CameraPtr camera, bool debugRenderTargetEnabled);

	virtual ~NextGenRenderManager();

	virtual void on_swap_chain_resized(uint w, uint h);

	virtual void render(dxRenderTargetView renderTargetView, const Viewport& renderTargetViewport);

protected:
	static void getLightViewProjMatrix(const SpotLight& light, float shadowMinDistance, float shadowMaxDistance, const Viewport& viewport, Matrix& outViewMat, Matrix& outProjMat);


	//
	// for debugging
	//
public:
	bool debug_render_target_empty() const {
		return this->debugging_texture==NULL;
	}

	dxShaderResourceView get_debug_render_target() const {
		return this->debugging_texture;
	}

	void set_debug_render_target(dxShaderResourceView crt, 
		float range_min=0.0f, float range_max=1.0f, bool r=true, bool g=true, bool b=true, bool a=true);

	const std::map<std::string, ColorRenderTarget*>& get_debuggable_render_targets() const {
		return debuggable_render_targets;
	}

protected:
	TextureRenderPass* debugTextureRenderPass;
	dxShaderResourceView debugging_texture;
	float debugging_range_min, debugging_range_max;
	bool debugging_r, debugging_g, debugging_b, debugging_a;

	std::map<std::string, ColorRenderTarget*> debuggable_render_targets;

};
