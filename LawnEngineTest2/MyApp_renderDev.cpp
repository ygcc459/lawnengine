
#include "fast_compile.h"
#include "MyApp.h"

#include "UiCmdUtil.h"
#include "gui_util.h"

using namespace CEGUI;

//////////////////////////////////////////////////////////////////////////

class RenderEffectListboxItem : public ListboxTextItem
{
public:
	RenderEffectListboxItem(RenderableEffect* t)
		:ListboxTextItem(t->get_source_file_name()), renderEffect(t)
	{
		setSelectionBrushImage("TaharezLook/GenericBrush");
	}

	RenderableEffect* renderEffect;
};

bool refresh_lstRenderEffects(const CEGUI::EventArgs &args)
{
	MyApp::instance->lstRenderEffects->resetList();

	typedef Resource<RenderableEffect>::ResourceMap Map;
	
	Map& resources = Resource<RenderableEffect>::get_resource_map();
	
	for(auto it=resources.begin(); it!=resources.end(); ++it)
	{
		boost::weak_ptr<RenderableEffect>& wp = it->second;
		boost::shared_ptr<RenderableEffect> p = wp.lock();
		if(p) {
			RenderableEffect* m = p.get();
			MyApp::instance->lstRenderEffects->addItem(new RenderEffectListboxItem(m));
		}
	}

	return true;
}

bool reloadRenderEffect(const CEGUI::EventArgs &args)
{
	RenderEffectListboxItem* item = (RenderEffectListboxItem*)MyApp::instance->lstRenderEffects->getFirstSelectedItem();
	
	if(item)
	{
		RenderableEffect* m = item->renderEffect;
		SandBoxUtil::reload_resource_until_valid(m);
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////

class EffectListboxItem : public ListboxTextItem
{
public:
	EffectListboxItem(EffectPtr t)
		:ListboxTextItem(t->get_effect_name()), effect(t)
	{
		setSelectionBrushImage("TaharezLook/GenericBrush");
	}

	EffectPtr effect;
};

bool refresh_lstEffects(const CEGUI::EventArgs &args)
{
	MyApp::instance->lstEffects->resetList();

	typedef Resource<Effect>::ResourceMap Map;
	
	Map& resources = Resource<Effect>::get_resource_map();
	
	for(auto it=resources.begin(); it!=resources.end(); ++it)
	{
		boost::weak_ptr<Effect>& wp = it->second;
		boost::shared_ptr<Effect> p = wp.lock();
		if(p) {
			MyApp::instance->lstEffects->addItem(new EffectListboxItem(p));
		}
	}

	return true;
}

bool reload_effect(const CEGUI::EventArgs &args)
{
	EffectListboxItem* item = (EffectListboxItem*)MyApp::instance->lstEffects->getFirstSelectedItem();
	
	if(item)
	{
		EffectPtr p = item->effect;
		SandBoxUtil::reload_resource_until_valid(p.get());
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////

class RenderTargetListboxItem : public ListboxTextItem
{
public:
	RenderTargetListboxItem(const std::string& name, dxShaderResourceView t)
		:ListboxTextItem(name), v(t)
	{
		setSelectionBrushImage("TaharezLook/GenericBrush");
	}

	dxShaderResourceView v;
};

bool refresh_lstRenderTargets(const CEGUI::EventArgs &args)
{
	MyApp::instance->lstRenderTargets->resetList();

	const std::map<std::string, ColorRenderTarget*>& targets = MyApp::instance->renderMgr->get_debuggable_render_targets();
	for(auto it=targets.begin(); it!=targets.end(); ++it)
	{
		MyApp::instance->lstRenderTargets->addItem(new RenderTargetListboxItem(it->first, it->second->shaderResourceView));
	}

	return true;
}

bool show_or_hide_renderTarget(const CEGUI::EventArgs &args)
{
	if(MyApp::instance->renderMgr->debug_render_target_empty())
	{
		RenderTargetListboxItem* item = (RenderTargetListboxItem*)MyApp::instance->lstRenderTargets->getFirstSelectedItem();
		if(item)
		{
			dxShaderResourceView srv = item->v;

			float min = gui_util::parseFloat(MyApp::instance->renderDevWindow->getChildRecursive("txtRangeMin"), 0.0f);
			float max = gui_util::parseFloat(MyApp::instance->renderDevWindow->getChildRecursive("txtRangeMax"), 1.0f);
			bool r = ((CEGUI::ToggleButton*)MyApp::instance->renderDevWindow->getChildRecursive("btnChannelR"))->isSelected();
			bool g = ((CEGUI::ToggleButton*)MyApp::instance->renderDevWindow->getChildRecursive("btnChannelG"))->isSelected();
			bool b = ((CEGUI::ToggleButton*)MyApp::instance->renderDevWindow->getChildRecursive("btnChannelB"))->isSelected();
			bool a = ((CEGUI::ToggleButton*)MyApp::instance->renderDevWindow->getChildRecursive("btnChannelA"))->isSelected();

			CEGUI::PushButton* showHideBtn = (CEGUI::PushButton*)MyApp::instance->renderDevWindow->getChildRecursive("btnShowHideRenderTargets");
			showHideBtn->setPushedState(true);

			MyApp::instance->renderMgr->set_debug_render_target(srv, min, max, r, g, b, a);
		}
		else
		{
			CEGUI::PushButton* showHideBtn = (CEGUI::PushButton*)MyApp::instance->renderDevWindow->getChildRecursive("btnShowHideRenderTargets");
			showHideBtn->setPushedState(true);

			MyApp::instance->renderMgr->set_debug_render_target(NULL);
		}
	}
	else
	{
		CEGUI::PushButton* showHideBtn = (CEGUI::PushButton*)MyApp::instance->renderDevWindow->getChildRecursive("btnShowHideRenderTargets");
		showHideBtn->setPushedState(true);

		MyApp::instance->renderMgr->set_debug_render_target(NULL);
	}

	return true;
}

bool reshow_renderTarget(const CEGUI::EventArgs &args)
{
	dxShaderResourceView srv = MyApp::instance->renderMgr->get_debug_render_target();

	if(srv)
	{
		float min = gui_util::parseFloat(MyApp::instance->renderDevWindow->getChildRecursive("txtRangeMin"), 0.0f);
		float max = gui_util::parseFloat(MyApp::instance->renderDevWindow->getChildRecursive("txtRangeMax"), 1.0f);
		bool r = ((CEGUI::ToggleButton*)MyApp::instance->renderDevWindow->getChildRecursive("btnChannelR"))->isSelected();
		bool g = ((CEGUI::ToggleButton*)MyApp::instance->renderDevWindow->getChildRecursive("btnChannelG"))->isSelected();
		bool b = ((CEGUI::ToggleButton*)MyApp::instance->renderDevWindow->getChildRecursive("btnChannelB"))->isSelected();
		bool a = ((CEGUI::ToggleButton*)MyApp::instance->renderDevWindow->getChildRecursive("btnChannelA"))->isSelected();

		MyApp::instance->renderMgr->set_debug_render_target(srv, min, max, r, g, b, a);
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////

void MyApp::init_renderDevWindow()
{
	renderDevWindow = cegui_rootWindow->getChildRecursive("renderDevWindow");

	lstRenderEffects = (Listbox*)renderDevWindow->getChildRecursive("lstRenderEffects");
	gui_util::attach_event(renderDevWindow, "btnRefreshRenderEffects", CEGUI::PushButton::EventClicked, &refresh_lstRenderEffects);
	gui_util::attach_event(renderDevWindow, "btnReloadRenderEffect", CEGUI::PushButton::EventClicked, &reloadRenderEffect);
	
	lstEffects = (Listbox*)renderDevWindow->getChildRecursive("lstEffects");
	gui_util::attach_event(renderDevWindow, "btnRefreshEffects", CEGUI::PushButton::EventClicked, &refresh_lstEffects);
	gui_util::attach_event(renderDevWindow, "btnReloadEffect", CEGUI::PushButton::EventClicked, &reload_effect);

	lstRenderTargets = (Listbox*)renderDevWindow->getChildRecursive("lstRenderTargets");
	gui_util::attach_event(renderDevWindow, "btnRefreshRenderTargets", CEGUI::PushButton::EventClicked, &refresh_lstRenderTargets);
	gui_util::attach_event(renderDevWindow, "btnShowHideRenderTargets", CEGUI::PushButton::EventClicked, &show_or_hide_renderTarget);
	((CEGUI::ToggleButton*)MyApp::instance->renderDevWindow->getChildRecursive("btnChannelR"))->setSelected(true);
	((CEGUI::ToggleButton*)MyApp::instance->renderDevWindow->getChildRecursive("btnChannelG"))->setSelected(true);
	((CEGUI::ToggleButton*)MyApp::instance->renderDevWindow->getChildRecursive("btnChannelB"))->setSelected(true);
	((CEGUI::ToggleButton*)MyApp::instance->renderDevWindow->getChildRecursive("btnChannelA"))->setSelected(true);
	gui_util::attach_event(renderDevWindow, "btnChannelR", CEGUI::ToggleButton::EventSelectStateChanged, &reshow_renderTarget);
	gui_util::attach_event(renderDevWindow, "btnChannelG", CEGUI::ToggleButton::EventSelectStateChanged, &reshow_renderTarget);
	gui_util::attach_event(renderDevWindow, "btnChannelB", CEGUI::ToggleButton::EventSelectStateChanged, &reshow_renderTarget);
	gui_util::attach_event(renderDevWindow, "btnChannelA", CEGUI::ToggleButton::EventSelectStateChanged, &reshow_renderTarget);

	gui_util::attach_event(renderDevWindow, "txtRangeMin", CEGUI::Editbox::EventKeyUp, &reshow_renderTarget);
	gui_util::attach_event(renderDevWindow, "txtRangeMax", CEGUI::Editbox::EventKeyUp, &reshow_renderTarget);

	mainRenderView->signal_after_window_resize.connect([](unsigned, unsigned)->void {
		refresh_lstRenderTargets(CEGUI::EventArgs());
	});
}
