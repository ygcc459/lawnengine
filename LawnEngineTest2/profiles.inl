
#define ChessScene
//#define SponzaScene
//#define SponzaSceneNight
//#define NormalMappingScene
//#define ParallaxMappingScene
//#define LoadEntity

#if defined(ChessScene)  //===============================================================================

entity = Entity::create_from_file("chessScene/ChessScene.le");
this->sceneMgr->addEntity(entity);

// vsm
theRenderManager->vsm_blur_length_x = 5;
theRenderManager->vsm_blur_length_y = 5;
theRenderManager->vsm_lightBleeding_reduceAmount = 0.2f;
theRenderManager->vsm_sceneDepthBias = -0.051f;
theRenderManager->shadowMinDistance = 5.0f;
theRenderManager->shadowMaxDistance = 5000.0f;

// ssvo
theRenderManager->ssvo_ambient_amount = 0.25f;
theRenderManager->ssvo_radius = 0.5f;
theRenderManager->ssvo_blur_length_x = 8;
theRenderManager->ssvo_blur_length_y = 4;

// hdr
theRenderManager->hdr_light_blur_length_x = 10;
theRenderManager->hdr_light_blur_length_y = 10;
theRenderManager->hdr_bright_threshold = 0.5f;
theRenderManager->hdr_middle_gray = 0.72f;
theRenderManager->hdr_lum_white = 1.5f;

// camera
pCamera->znear = 1.0f;
pCamera->zfar = 10000.0f;
pCamera->cameraPanSpeed = 2.0f;
pCamera->cameraRotateSpeed = 0.0400f;

// sun light
sunlight->direction = Vector3(-1.0f, -1.0f, -1.0f);
sunlight->color = float3(0.3f, 0.3f, 0.3f);

// spot light
spotLight->position = Point3(0, 66.3f, -102.2f);
spotLight->direction = Vector3(0, -0.54f, 0.84f);
spotLight->direction.normalize();
spotLight->color = float3(1, 1, 1);
spotLight->innerConeAngle = degreeToRadian(25.0f);
spotLight->outerConeAngle = degreeToRadian(45.0f);

// scene
theRenderManager->sceneBackgroundColor = float3(0.5f, 0.5f, 0.5f);

//entity->matrix = Matrix::identity();

#elif defined(SponzaScene)  //===============================================================================

entity = Entity::create_from_file("./sponza/sp33.le");
this->sceneMgr->addEntity(entity);

// vsm
theRenderManager->vsm_blur_length_x = 5;
theRenderManager->vsm_blur_length_y = 5;
theRenderManager->vsm_lightBleeding_reduceAmount = 0.656f;
theRenderManager->vsm_sceneDepthBias = -0.015f;
theRenderManager->shadowMinDistance = 5.0f;
theRenderManager->shadowMaxDistance = 5000.0f;

// ssvo
theRenderManager->ssvo_ambient_amount = 0.08f;
theRenderManager->ssvo_radius = 0.5f;
theRenderManager->ssvo_blur_length_x = 8;
theRenderManager->ssvo_blur_length_y = 4;

// hdr
theRenderManager->hdr_light_blur_length_x = 10;
theRenderManager->hdr_light_blur_length_y = 10;
theRenderManager->hdr_bright_threshold = 0.02f;
theRenderManager->hdr_middle_gray = 0.40f;
theRenderManager->hdr_lum_white = 2.40f;

// camera
pCamera->znear = 1.0f;
pCamera->zfar = 10000.0f;
pCamera->cameraPanSpeed = 32.0f;
pCamera->cameraRotateSpeed = 0.0025f;

// sun light
sunlight->direction = Vector3(-0.77f, -0.46f, -1.48f);
sunlight->color = float3(0.43f, 0.43f, 0.43f);

// spot light
spotLight->position = Point3(1093, 2741, -508);
spotLight->direction = Vector3(-0.53, -0.83, 0.19);
spotLight->color = float3(1, 1, 1);
spotLight->innerConeAngle = degreeToRadian(25.0f);
spotLight->outerConeAngle = degreeToRadian(45.0f);

// scene
theRenderManager->sceneBackgroundColor = float3(0.5f, 0.5f, 0.5f);

//entity->matrix = Matrix::identity();

#elif defined(SponzaSceneNight)  //===============================================================================

entity = Entity::create_from_file("./sponza/sp33.le");
this->sceneMgr->addEntity(entity);

// vsm
theRenderManager->vsm_blur_length_x = 5;
theRenderManager->vsm_blur_length_y = 5;
theRenderManager->vsm_lightBleeding_reduceAmount = 0.656f;
theRenderManager->vsm_sceneDepthBias = -0.015f;
theRenderManager->shadowMinDistance = 5.0f;
theRenderManager->shadowMaxDistance = 5000.0f;

// ssvo
theRenderManager->ssvo_ambient_amount = 0.08f;
theRenderManager->ssvo_radius = 0.5f;
theRenderManager->ssvo_blur_length_x = 8;
theRenderManager->ssvo_blur_length_y = 4;

// hdr
theRenderManager->hdr_light_blur_length_x = 10;
theRenderManager->hdr_light_blur_length_y = 10;
theRenderManager->hdr_bright_threshold = 0.59f;
theRenderManager->hdr_middle_gray = 0.01f;
theRenderManager->hdr_lum_white = 0.02f;

// camera
pCamera->znear = 1.0f;
pCamera->zfar = 10000.0f;
pCamera->cameraPanSpeed = 32.0f;
pCamera->cameraRotateSpeed = 0.0025f;

// sun light
sunlight->direction = Vector3(-0.77f, -0.46f, -1.48f);
sunlight->color = float3(0.0667f, 0.0667f, 0.0667f);

// spot light
spotLight->position = Point3(1093, 2741, -508);
spotLight->direction = Vector3(-0.53, -0.83, 0.19);
spotLight->color = float3(0.5569f, 1.0f, 1.0f);
spotLight->innerConeAngle = degreeToRadian(25.0f);
spotLight->outerConeAngle = degreeToRadian(45.0f);

// scene
theRenderManager->sceneBackgroundColor = float3(0.6f, 0.725f, 0.741f);

//entity->matrix = Matrix::identity();

#elif defined(NormalMappingScene)  //===============================================================================

entity = Entity::create_from_file("./chessboard_normalMapping/cb.le");
this->sceneMgr->addEntity(entity);

// vsm
theRenderManager->vsm_enabled = false;
theRenderManager->vsm_blur_length_x = 5;
theRenderManager->vsm_blur_length_y = 5;
theRenderManager->vsm_lightBleeding_reduceAmount = 0.2f;
theRenderManager->vsm_sceneDepthBias = -0.015f;
theRenderManager->shadowMinDistance = 5.0f;
theRenderManager->shadowMaxDistance = 5000.0f;

// ssvo
theRenderManager->ssvo_ambient_amount = 0.1f;
theRenderManager->ssvo_radius = 0.5f;
theRenderManager->ssvo_blur_length_x = 8;
theRenderManager->ssvo_blur_length_y = 4;

// hdr
theRenderManager->hdr_light_blur_length_x = 10;
theRenderManager->hdr_light_blur_length_y = 10;
theRenderManager->hdr_bright_threshold = 0.5f;
theRenderManager->hdr_middle_gray = 0.72f;
theRenderManager->hdr_lum_white = 1.5f;

// camera
pCamera->znear = 1.0f;
pCamera->zfar = 10000.0f;
pCamera->cameraPanSpeed = 1.0f;
pCamera->cameraRotateSpeed = 0.01f;

// sun light
sunlight->direction = Vector3(-1.0f, -1.0f, -1.0f);
sunlight->color = float3(0.3f, 0.3f, 0.3f);

// spot light
spotLight->position = Point3(0, 100, 0);
spotLight->direction = Vector3(0, -1, 0);
spotLight->color = float3(1, 1, 1);
spotLight->innerConeAngle = degreeToRadian(25.0f);
spotLight->outerConeAngle = degreeToRadian(45.0f);

// scene
theRenderManager->sceneBackgroundColor = float3(0.5f, 0.5f, 0.5f);

//entity->matrix = Matrix::identity();

#elif defined(ParallaxMappingScene)  //===============================================================================

entity = Entity::create_from_file("./chessboard_parallexMapping/cb.le");
this->sceneMgr->addEntity(entity);

// vsm
theRenderManager->vsm_enabled = false;
theRenderManager->vsm_blur_length_x = 5;
theRenderManager->vsm_blur_length_y = 5;
theRenderManager->vsm_lightBleeding_reduceAmount = 0.2f;
theRenderManager->vsm_sceneDepthBias = -0.015f;
theRenderManager->shadowMinDistance = 5.0f;
theRenderManager->shadowMaxDistance = 5000.0f;

// ssvo
theRenderManager->ssvo_ambient_amount = 0.1f;
theRenderManager->ssvo_radius = 0.5f;
theRenderManager->ssvo_blur_length_x = 8;
theRenderManager->ssvo_blur_length_y = 4;

// hdr
theRenderManager->hdr_light_blur_length_x = 10;
theRenderManager->hdr_light_blur_length_y = 10;
theRenderManager->hdr_bright_threshold = 0.5f;
theRenderManager->hdr_middle_gray = 0.72f;
theRenderManager->hdr_lum_white = 1.5f;

// camera
pCamera->znear = 1.0f;
pCamera->zfar = 10000.0f;
pCamera->cameraPanSpeed = 1.0f;
pCamera->cameraRotateSpeed = 0.01f;

// sun light
sunlight->direction = Vector3(-1.0f, -1.0f, -1.0f);
sunlight->color = float3(0.3f, 0.3f, 0.3f);

// spot light
spotLight->position = Point3(0, 100, 0);
spotLight->direction = Vector3(0, -1, 0);
spotLight->color = float3(1, 1, 1);
spotLight->innerConeAngle = degreeToRadian(25.0f);
spotLight->outerConeAngle = degreeToRadian(45.0f);

// scene
//theRenderManager->sceneBackgroundColor = float3(0.5f, 0.5f, 0.5f);

//entity->matrix = Matrix::identity();

#elif defined(LoadEntity)  //===============================================================================

std::string le_file = chooseLeFile();
if(le_file!="") {
	entity = Entity::create_from_file(le_file.c_str());
	this->sceneMgr->addEntity(entity);
}

#else



#endif
