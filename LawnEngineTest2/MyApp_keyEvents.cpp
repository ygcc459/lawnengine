
#include "fast_compile.h"
#include "MyApp.h"

//////////////////////////////////////////////////////////////////////////

bool sendTwKeyMsg(KeyCode keycode)
{
	int newkey = -1;

	switch(keycode)
	{
	case KC_COMMA:
		newkey = '.'; break;
	case KC_DELETE:
		newkey = TW_KEY_DELETE; break;
	case KC_BACK:
		newkey = TW_KEY_BACKSPACE; break;
	default:
		newkey = (int)keycode; break;
	}

	if(TwKeyPressed(newkey, TW_KMOD_NONE))
		return true;
	else
		return false;
}

//////////////////////////////////////////////////////////////////////////

bool MyApp::keyPressed(const KeyEventArg &arg)
{
	if(cegui_enabled)
	{
		if(CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown((CEGUI::Key::Scan)arg.keycode))
			return true;

		if(CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(arg.text))
			return true;
	}

	if(twbar_enabled && sendTwKeyMsg(arg.keycode))
		return true;

	switch(arg.keycode)
	{
	case KC_P:
		{
			paused = !paused;
			return true;
		}
	case KC_Y:
		{
			pCamera->setFlyAroundCenter(pCamera->pos);
			return true;
		}
	case KC_T:
		{
			twbar_enabled = !twbar_enabled;
			return true;
		}
	case KC_C:
		{
			cegui_enabled = !cegui_enabled;
			return true;
		}
	case KC_L:
		{
			//sunlight->direction = pCamera->dir;
			spotLight->position = pCamera->pos;
			spotLight->direction = pCamera->dir;
			return true;
		}
	}

	return false;
}

bool MyApp::keyReleased(const KeyEventArg &arg)
{
	if(cegui_enabled && CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp((CEGUI::Key::Scan)arg.keycode))
		return true;

	return false;
}
