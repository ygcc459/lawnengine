
#include "fast_compile.h"
#include "MyApp.h"

struct ControlState
{
	enum State {
		None,
		MovingObjectsXZ,
	};

	enum MouseTool {
		SelectionTool,
		MoveTool,
	};

	State state;
	MouseTool mouseTool;

	struct {
		Point3 start_pt;
	} data_MovingObjectsXZ;

	std::set<SceneObjectPtr> selected_objects;
};

static ControlState controlState;
static AuxGeometryContainer* selectionAux = NULL;

void MyApp::init_user_control()
{
	controlState.state = ControlState::None;
	controlState.mouseTool = ControlState::SelectionTool;

	selectionAux = AuxGeometryDrawPass::instance().create_container();
}

SceneObjectPtr MyApp::pick_object(uint x, uint y, std::vector<SceneRayQueryResult>& results)
{
	Point3 ray_origin; this->pCamera->getPosition(ray_origin);
	Vector3 ray_direction = this->pCamera->getPickingRay(x, y, this->mainRenderView->getViewport());

	std::vector<SceneObjectPtr>& objects = this->sceneMgr->get_objects();
	for(SceneObjectPtr obj : objects)
	{
		results.resize(0);
		if(obj->rayIntersectTest(ray_direction, ray_origin, results) > 0)
		{
			return obj;
		}
	}

	return SceneObjectPtr();
}

bool MyApp::pick_point_on_plane(Point3& outPoint, uint x, uint y, const Point3 & planeStart, const Vector3 & planeNormal)
{
	Point3 ray_origin; this->pCamera->getPosition(ray_origin);
	Vector3 ray_direction = this->pCamera->getPickingRay(x, y, this->mainRenderView->getViewport());

	float dist;
	if(Intersect::_3D::ray_plane(&dist, ray_origin, ray_direction, planeStart, planeNormal)) {
		outPoint = ray_origin + dist * ray_direction;
		return true;
	}
	else
		return false;
}

bool MyApp::mouseMoved(const MouseEvent &arg)
{
	if(cegui_enabled &&CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition((float)arg.abs_x, (float)arg.abs_y))
		return true;

	if(twbar_enabled && TwMouseMotion(arg.abs_x, arg.abs_y))
 		return true;

	return false;
}

// void MyApp::onMouseLeave()
// {
// 	if(cegui_enabled)
// 		CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseLeaves();
// }

bool MyApp::mousePressed(const MouseEvent &arg, MouseButton btn)
{
	if(btn==MouseButton::MB_Left)
	{
		if(cegui_enabled &&CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(CEGUI::LeftButton))
			return true;

		if(twbar_enabled && TwMouseButton(TW_MOUSE_PRESSED, TW_MOUSE_LEFT))
			return true;

		switch(controlState.mouseTool)
		{
		case ControlState::SelectionTool:
			{
				if(!GetAsyncKeyState(VK_SHIFT) && !GetAsyncKeyState(VK_CONTROL)) {
					controlState.selected_objects.clear();
					selectionAux->clear();
				}

				SceneObjectPtr s = pick_object(arg.abs_x, arg.abs_y, tmp_picking_results);
				if(s)
				{
					controlState.selected_objects.insert(s);

					for(SceneRayQueryResult& r : tmp_picking_results) {
						AABB geomAABB;
						r.subEntity->pGeometry->getAxisAlignedBoundingBox(geomAABB);
						selectionAux->add(geomAABB, r.subEntity->getAbsoluteMatrix(), float4(0,1,0,1));
					}
				}

				controlState.state = ControlState::None;

				return true;
			}
		case ControlState::MoveTool:
			{
				if(!controlState.selected_objects.empty())
				{
					if(!GetAsyncKeyState(VK_SHIFT))
					{
						if(pick_point_on_plane(controlState.data_MovingObjectsXZ.start_pt, arg.abs_x, arg.abs_y, Point3(0,0,0), Vector3(0,1,0)))
						{
							controlState.state = ControlState::MovingObjectsXZ;
							return true;
						}
					}
				}

				break;
			}
		}
	}
	else if(btn==MouseButton::MB_Right)
	{
		if(cegui_enabled &&CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(CEGUI::RightButton))
			return true;

		if(twbar_enabled && TwMouseButton(TW_MOUSE_PRESSED, TW_MOUSE_RIGHT))
			return true;

		switch(controlState.state)
		{
		case ControlState::MovingObjectsXZ:
			{
				if(!controlState.selected_objects.empty())
				{
					Point3 newPoint;
					if(pick_point_on_plane(newPoint, arg.abs_x, arg.abs_y, Point3(0,0,0), Vector3(0,1,0)))
					{
						float dx = newPoint.x - controlState.data_MovingObjectsXZ.start_pt.x;
						float dz = newPoint.z - controlState.data_MovingObjectsXZ.start_pt.z;

						for(SceneObjectPtr s : controlState.selected_objects)
						{
							EntityPtr e = boost::dynamic_pointer_cast<Entity>(s);
							if(e)
							{
								Point3& pos = e->matrix.position();
								pos.x += dx;
								pos.z += dz;
							}
						}

						return true;
					}
				}

				break;
			}
		}
	}

	return false;
}

bool MyApp::mouseReleased(const MouseEvent &arg, MouseButton btn)
{
	if(btn==MouseButton::MB_Left)
	{
		if(cegui_enabled &&CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(CEGUI::LeftButton))
			return true;

		if(twbar_enabled && TwMouseButton(TW_MOUSE_RELEASED, TW_MOUSE_LEFT))
			return true;

		switch(controlState.state)
		{
		case ControlState::None:
			{
				break;
			}
		}

		return false;
	}
	else if(btn==MouseButton::MB_Right)
	{
		if(cegui_enabled &&CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(CEGUI::RightButton))
			return true;

		if(twbar_enabled && TwMouseButton(TW_MOUSE_RELEASED, TW_MOUSE_RIGHT))
			return true;

		return false;
	}

	return false;
}
