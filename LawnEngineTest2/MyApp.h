#pragma once

#ifdef _DEBUG
#pragma comment(lib, "../LawnEngineFramework/LawnEngineFramework_debug.lib")
#else
#pragma comment(lib, "../LawnEngineFramework/LawnEngineFramework_release.lib")
#endif

#include "SandBoxUtil.h"

#include <AntTweakBar.h>
#pragma comment(lib, "AntTweakBar.lib")

class NextGenRenderManager;


class MyApp
	: public LawnEnginePlugin, public KeyEventListener, public MouseEventListener
{
public:
	MyApp();

	virtual bool init();
	virtual bool destroy();

	virtual void onRender();
	virtual void onUpdateScene(float fElapsedTime);

	virtual bool keyPressed(const KeyEventArg &arg);
	virtual bool keyReleased(const KeyEventArg &arg);

	virtual bool mouseMoved(const MouseEvent &arg);
	virtual bool mousePressed(const MouseEvent &arg, MouseButton btn);
	virtual bool mouseReleased(const MouseEvent &arg, MouseButton btn);

	//
	// user control
	//
	SceneObjectPtr pick_object(uint x, uint y, std::vector<SceneRayQueryResult>& results);
	bool pick_point_on_plane(Point3& outPoint, uint x, uint y, const Point3 & planeStart, const Vector3 & planeNormal);

	void init_user_control();

	//
	// singleton
	//
	static MyApp* instance;
	
	HWND windowHwnd;

	//
	// ui
	//
	void init_ui();
	void init_stuff_list();
	void init_renderDevWindow();
	void render_ui();

	bool cegui_enabled;

	CEGUI::Window* mainWindow;
	CEGUI::Window* stuffListWindow;
	CEGUI::Window* renderDevWindow;
	CEGUI::Listbox* lstObjects;
	CEGUI::Listbox* lstMaterials;
	CEGUI::Listbox* lstRenderEffects;
	CEGUI::Listbox* lstRenderTargets;
	CEGUI::Listbox* lstEffects;

	std::vector<SceneRayQueryResult> tmp_picking_results;

	CEGUI::Direct3D11Renderer* cegui_renderer;
	CEGUI::Window* cegui_rootWindow;

	CEGUI::Window* ui_lblFps;

	void init_twbar();
	
	bool twbar_enabled;

	//
	// engine components
	//
	SimpleSceneManager* sceneMgr;
	NextGenRenderManager* renderMgr;
	RenderView* mainRenderView;

	InputManager* inputManager;

	boost::shared_ptr<CFreeCamera> pCamera;
	FreeCameraController freeCameraController;

	DirectionalLightPtr sunlight;
	SpotLightPtr spotLight;
	
	AuxGeometryContainer* aux_geom_container;

	bool paused;

	AABB sceneAABB;

};

