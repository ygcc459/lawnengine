#include "fast_compile.h"
#include "MyApp.h"
#include "UiCmdUtil.h"

std::string UiCmdUtil::chooseFile(LPCSTR lpstrFilter/*=".le files\0*.le\0\0"*/)
{
	std::string file_name("");

	char old_cur_dir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, old_cur_dir);

	char open_file_name[MAX_PATH] = {0};
	OPENFILENAME ofn;
	{
		ZeroMemory(&ofn, sizeof(ofn));
		ofn.lStructSize = sizeof(ofn);
		ofn.lpstrFilter = lpstrFilter;
		ofn.nFilterIndex = 1;
		ofn.lpstrFile = open_file_name;
		ofn.nMaxFile = MAX_PATH;
		ofn.Flags = OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST;
	}
	if(GetOpenFileName(&ofn)!=0)
		file_name = open_file_name;

	SetCurrentDirectory(old_cur_dir);
	return file_name;
}
