
#include "fast_compile.h"
#include "MyApp.h"

#include "UiCmdUtil.h"
#include "gui_util.h"

using namespace CEGUI;

//////////////////////////////////////////////////////////////////////////

class MaterialListboxItem : public ListboxTextItem
{
public:
	MaterialListboxItem(Material* t)
		:ListboxTextItem(t->name), material(t)
	{
		setSelectionBrushImage("TaharezLook/GenericBrush");
	}

	Material* material;
};

bool refresh_lstMaterials(const CEGUI::EventArgs &args)
{
	MyApp::instance->lstMaterials->resetList();

	typedef Resource<Material>::ResourceMap Map;
			
	Map& resources = Resource<Material>::get_resource_map();
			
	for(auto it=resources.begin(); it!=resources.end(); ++it)
	{
		boost::weak_ptr<Material>& wp = it->second;
		boost::shared_ptr<Material> p = wp.lock();
		if(p) {
			Material* m = p.get();
			MyApp::instance->lstMaterials->addItem(new MaterialListboxItem(m));
		}
	}

	return true;
}

bool reload_material(const CEGUI::EventArgs &args)
{
	MaterialListboxItem* item = (MaterialListboxItem*)MyApp::instance->lstMaterials->getFirstSelectedItem();

	if(item)
	{
		Material* m = item->material;
		SandBoxUtil::reload_resource_until_valid(m);
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////

class ObjectListboxItem : public ListboxTextItem
{
public:
	ObjectListboxItem(SceneObjectPtr scene_object)
		:ListboxTextItem(scene_object->name), scene_object(scene_object)
	{
		setSelectionBrushImage("TaharezLook/GenericBrush");
	}

	SceneObjectPtr scene_object;
};

bool refresh_lstRenderEffects(const CEGUI::EventArgs &args);

bool refreshObjects(const CEGUI::EventArgs &args) {
	MyApp::instance->lstObjects->resetList();

	SceneManager* sm = MyApp::instance->sceneMgr;
	if(sm)
	{
		sm->for_each_object_ptr([](SceneObjectPtr so)->void {
			MyApp::instance->lstObjects->addItem(new ObjectListboxItem(so));
		});
	}

	refresh_lstMaterials(args);
	refresh_lstRenderEffects(args);

	return true;
}

bool addObject(const CEGUI::EventArgs &args) {
	std::string le_file = UiCmdUtil::chooseFile(".le files\0*.le\0\0");
	if(le_file!="") {
		SceneObjectPtr p = Entity::create_from_file(le_file.c_str());
		if(p) {
			MyApp::instance->sceneMgr->add_object(p);
			refreshObjects(args);
		}
	}
	return true;
}

bool delObject(const CEGUI::EventArgs &args) {
	ObjectListboxItem* item = (ObjectListboxItem*)MyApp::instance->lstObjects->getFirstSelectedItem();
	if(item)
	{
		MyApp::instance->sceneMgr->delete_object(item->scene_object);
		refreshObjects(args);
	}
	return true;
}

bool clearObjects(const CEGUI::EventArgs &args) {
	MyApp::instance->sceneMgr->clear_objects();
	refreshObjects(args);
	return true;
}

//////////////////////////////////////////////////////////////////////////

void MyApp::init_stuff_list()
{
	stuffListWindow = cegui_rootWindow->getChildRecursive("stuffListWindow");

	lstObjects = (Listbox*)stuffListWindow->getChildRecursive("lstObjects");
	gui_util::attach_event(stuffListWindow, "btnAddObject", CEGUI::PushButton::EventClicked, &addObject);
	gui_util::attach_event(stuffListWindow, "btnDelObject", CEGUI::PushButton::EventClicked, &delObject);
	gui_util::attach_event(stuffListWindow, "btnClearObjects", CEGUI::PushButton::EventClicked, &clearObjects);
	gui_util::attach_event(stuffListWindow, "btnRefreshObjects", CEGUI::PushButton::EventClicked, &refreshObjects);
	
	lstMaterials = (Listbox*)stuffListWindow->getChildRecursive("lstMaterials");
	gui_util::attach_event(stuffListWindow, "btnRefreshMaterials", CEGUI::PushButton::EventClicked, &refresh_lstMaterials);
	gui_util::attach_event(stuffListWindow, "btnReloadMaterial", CEGUI::PushButton::EventClicked, &reload_material);
}
