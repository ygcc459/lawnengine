#pragma once

namespace SandBoxUtil
{
	template<class ResourceType>
	static void reload_resource_until_valid(ResourceType* p)
	{
		bool succees = false;
		do
		{
			try {
 				p->reload();
				succees = true;
			}
			catch(const ::Exception& e) {
				vcpp::log::err("%s", e.message.c_str());
				if(::MessageBoxA(NULL, e.message.c_str(), "resource reload error", MB_RETRYCANCEL)==IDCANCEL)
					return;
			}
		} while(!succees);
	}

	static void reload_renderableEffect_until_valid(const char* renderable_effect_name)
	{
		boost::shared_ptr<RenderableEffect> p = Resource<RenderableEffect>::get_loaded_resource(renderable_effect_name, false);
 		if(p)
 		{
			bool succees = false;
			do
			{
				try {
 					p->reload();
					succees = true;
				}
				catch(const Exception& e) {
					vcpp::log::err("error when reloading '%s', detail: '%s'", renderable_effect_name, e.message.c_str());
					::MessageBox(NULL, e.message.c_str(), NULL, NULL);
				}
			} while(!succees);
 		}
		else
		{
			vcpp::log::err("error, resource '%s' not found", renderable_effect_name);
		}
	}

	static void reload_effect_until_valid(const char* effect_file_path)
	{
		boost::shared_ptr<Effect> p = Resource<Effect>::get_loaded_resource(effect_file_path, true);
 		if(p)
 		{
			bool succees = false;
			do
			{
				try {
 					p->reload();
					succees = true;
				}
				catch(const Exception& e) {
					vcpp::log::err("error when reloading '%s', detail: '%s'", effect_file_path, e.message.c_str());
					::MessageBox(NULL, e.message.c_str(), NULL, NULL);
				}
			} while(!succees);
 		}
		else
		{
			vcpp::log::err("error, resource '%s' not found", effect_file_path);
		}
	}

	static void reload_material_until_valid(const char* material_file_path)
	{
		boost::shared_ptr<Material> p = Resource<Material>::get_loaded_resource(material_file_path, true);
 		if(p)
 		{
			bool succees = false;
			do
			{
				try {
 					p->reload();
					succees = true;
				}
				catch(const Exception& e) {
					vcpp::log::err("error when reloading '%s', detail: '%s'", material_file_path, e.message.c_str());
					::MessageBox(NULL, e.message.c_str(), NULL, NULL);
				}
			} while(!succees);
 		}
		else
		{
			vcpp::log::err("error, resource '%s' not found", material_file_path);
		}
	}
}
