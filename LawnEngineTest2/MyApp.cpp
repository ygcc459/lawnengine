
#include "fast_compile.h"
#include <windows.h>
#include "MyApp.h"

MyApp* MyApp::instance = NULL;

//////////////////////////////////////////////////////////////////////////

MyApp::MyApp()
	:sceneMgr(), renderMgr(), mainRenderView(), cegui_renderer(), inputManager()
{
	paused = false;
	instance = this;
}

bool MyApp::init()
{
	cegui_enabled = true;
	twbar_enabled = false;

	try
	{
		vcpp::log::info("MyApp init...");

		SetCurrentDirectory("../lawnenginemedia/");
		srand((unsigned int)time(NULL));

		//
		// debug aux geom
		//
		aux_geom_container = AuxGeometryDrawPass::instance().create_container();
		aux_geom_container->add(AuxLine().from_fixed_point(0,0,0).towards_fixed_direction(1,0,0, 0.0f, 10.0f).color(1,0,0));
		aux_geom_container->add(AuxLine().from_fixed_point(0,0,0).towards_fixed_direction(0,1,0, 0.0f, 10.0f).color(0,1,0));
		aux_geom_container->add(AuxLine().from_fixed_point(0,0,0).towards_fixed_direction(0,0,1, 0.0f, 10.0f).color(0,0,1));

		// create render view
		RECT renderViewClientRect;
		GetClientRect(windowHwnd, &renderViewClientRect);

		mainRenderView = new RenderView(renderViewClientRect.right, renderViewClientRect.bottom, windowHwnd, DXGI_FORMAT_R8G8B8A8_UNORM);
		g_lawnEngine.addRenderView(mainRenderView);
		
		// create input manager
		inputManager = new OISInputManager(windowHwnd);
		
		inputManager->addKeyEventListener(this);
		inputManager->addMouseEventListener(this);

		inputManager->addKeyEventListener(&freeCameraController);
		inputManager->addMouseEventListener(&freeCameraController);

		//
		// the scene
		//
		sceneMgr = new SimpleSceneManager();

		//
		// the camera
		//
		pCamera.reset(new CFreeCamera());
		pCamera->pos = Point3(50, 50, -50);
		pCamera->lookAtPoint(Point3(0, 0, 0));

		freeCameraController.setCamera(pCamera);
		freeCameraController.setEnabled(true);

		//
		// lights
		//
		sunlight.reset(new DirectionalLight(Vector3(-1.0f, -1.0f, -1.0f), float3(0.3f, 0.3f, 0.3f)));
		sunlight->direction.normalize();
		sceneMgr->add_light(sunlight);

		spotLight.reset(new SpotLight(Point3(0, 50, 0), Vector3(0, -1, 0), float3(1.0f, 1.0f, 1.0f), degreeToRadian(25.0f), degreeToRadian(45.0f)));
		spotLight->direction.normalize();
		spotLight->generateShadow = true;
		sceneMgr->add_light(spotLight);

		//
		// the rendering
		//
		renderMgr = new NextGenRenderManager(sceneMgr, pCamera, true);
		mainRenderView->set_render_manager(renderMgr);

		//
		// UI
		//
		init_ui();
		
		init_twbar();

		init_user_control();

		vcpp::log::info("MyApp init done.");
	}
	catch(const Exception& e)
	{
		vcpp::log::err("Error in (%d,%s,%s) : %s", e.src_line_number, e.src_file_name, e.src_function_name, e.message.c_str());

		::MessageBoxA(NULL, e.message.c_str(), NULL, NULL);

		return false;
	}
	
	return true;
}

bool MyApp::destroy()
{
	vcpp::log::info("MyApp destroy...");

	pCamera.reset();
	freeCameraController.setCamera(pCamera);
	freeCameraController.setEnabled(false);

	SAFE_DELETE(sceneMgr);
	SAFE_DELETE(renderMgr);
	SAFE_DELETE(mainRenderView);
	SAFE_DELETE(inputManager);

	TwTerminate();

	cegui_renderer->destroySystem();
	
	vcpp::log::info("MyApp destroy done.");

	return true;
}

//////////////////////////////////////////////////////////////////////////

void MyApp::onRender()
{
	mainRenderView->render();

	LawnEngineUtil::setRenderTargets(*mainRenderView, (dxDepthStencilView)NULL);
	
	if(twbar_enabled)
		TwDraw();

	if(cegui_enabled)
		render_ui();

	mainRenderView->get_swapchain()->Present(0, 0);
}

void MyApp::onUpdateScene(float fElapsedTime)
{
	if(paused)
		return;

	if(cegui_enabled)
		CEGUI::System::getSingleton().injectTimePulse(fElapsedTime);

	if(inputManager) {
		inputManager->onWindowResized(mainRenderView->getViewport().width, mainRenderView->getViewport().height);
		inputManager->onNextFrame();
	}
}
