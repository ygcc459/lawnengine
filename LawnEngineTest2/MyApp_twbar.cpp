
#include "fast_compile.h"
#include "MyApp.h"

void TW_CALL on_fullscreen(void *clientData)
{ 
	MyApp* myapp = (MyApp*)clientData;

	BOOL isfullscreen;

	IDXGIOutput* outputWindow0 = NULL;
	myapp->mainRenderView->get_swapchain()->GetFullscreenState(&isfullscreen, &outputWindow0);

	IDXGIOutput* outputWindow = NULL;
	myapp->mainRenderView->get_swapchain()->GetContainingOutput(&outputWindow);
	myapp->mainRenderView->get_swapchain()->SetFullscreenState(!isfullscreen, outputWindow);
	outputWindow->Release();
}

void TW_CALL reload_renderableEffect(void *clientData)
{
	SandBoxUtil::reload_renderableEffect_until_valid((const char*)clientData);
}

void TW_CALL reload_effect(void *clientData)
{
	SandBoxUtil::reload_effect_until_valid((const char*)clientData);
}

void MyApp::init_twbar()
{
	TwInit(TW_DIRECT3D11, RenderSystem::instance().device);
	TwWindowSize(mainRenderView->getViewport().width, mainRenderView->getViewport().height);
		
	mainRenderView->signal_before_window_resize.connect(
		[]()->void {
			TwWindowSize(0, 0);
	});
		
	mainRenderView->signal_after_window_resize.connect(
		[](uint w, uint h)->void {
			TwWindowSize(w, h);
	});

	TwBar* mainTweakBar = TwNewBar("MainBar");
	{
		// status
		TwAddVarRO(mainTweakBar, "Width", TW_TYPE_UINT32, &mainRenderView->getViewport().width, " group=status ");
		TwAddVarRO(mainTweakBar, "Height", TW_TYPE_UINT32, &mainRenderView->getViewport().height, " group=status ");
		TwAddButton(mainTweakBar, "Full screen", &on_fullscreen, this, " group=status ");

		// deferred lighting
		TwAddButton(mainTweakBar, "bp_reload", &reload_renderableEffect, "blinn_phong", " group=deferred_lighting ");

		// deferred lighting
//		TwAddVarRW(mainTweakBar, "cone_base_distance", TW_TYPE_FLOAT, &renderMgr->pass_lighting.cone_base_distance, " group=deferred_lighting ");
//		TwAddButton(mainTweakBar, "dl_reload", &reload_effect, "DeferredLighting_LightingPass.fx", " group=deferred_lighting ");

 		// normal mapping
// 		TwAddButton(mainTweakBar, "normalMapping reload", &on_normalMapping_reload, this, "group=normalMapping");
// 		TwAddButton(mainTweakBar, "normalMapping reload material", &on_normalMapping_reloadMaterial, this, "group=normalMapping");
 
 		// parallex mapping
// 		TwAddButton(mainTweakBar, "parallexMapping reload", &on_parallexMapping_reload, this, "group=parallexMapping");
// 		TwAddButton(mainTweakBar, "parallexMapping reload material", &on_parallexMapping_reloadMaterial, this, "group=parallexMapping");
		
		// vsm
// 		TwAddVarRW(mainTweakBar, "vsm enabled", TW_TYPE_BOOLCPP, &theRenderManager->vsm_enabled, " group=vsm ");
// 		TwAddVarRW(mainTweakBar, "vsm blur length x", TW_TYPE_UINT32, &theRenderManager->vsm_blur_length_x, " group=vsm min=0 max=100");
// 		TwAddVarRW(mainTweakBar, "vsm blur length y", TW_TYPE_UINT32, &theRenderManager->vsm_blur_length_y, " group=vsm min=0 max=100");
// 		TwAddVarRW(mainTweakBar, "vsm reduce light bleeding", TW_TYPE_FLOAT, &theRenderManager->vsm_lightBleeding_reduceAmount, " group=vsm min=0 step=0.001");
// 		TwAddVarRW(mainTweakBar, "vsm depth bias", TW_TYPE_FLOAT, &theRenderManager->vsm_sceneDepthBias, " group=vsm step=0.001");
// 
// 		TwAddVarRW(mainTweakBar, "shadow min distance", TW_TYPE_FLOAT, &theRenderManager->shadowMinDistance, " group=vsm min=0 step=0.01 ");
// 		TwAddVarRW(mainTweakBar, "shadow max distance", TW_TYPE_FLOAT, &theRenderManager->shadowMaxDistance, " group=vsm min=0 step=1 ");

		// ssvo
// 		TwAddVarRW(mainTweakBar, "ssvo enabled", TW_TYPE_BOOLCPP, &theRenderManager->ssvo_enabled, " group=ssvo ");
// 		TwAddVarRW(mainTweakBar, "show ssvo buffer", TW_TYPE_BOOLCPP, &theRenderManager->show_ssvo_only, " group=ssvo ");
// 		TwAddButton(mainTweakBar, "ssvo reload", &on_ssvo_reload, this, "group=ssvo");
// 		TwAddVarRW(mainTweakBar, "ambient amount", TW_TYPE_FLOAT, &theRenderManager->ssvo_ambient_amount, " group=ssvo min=0 max=1.0 step=0.01");
// 		TwAddVarRW(mainTweakBar, "ssvo radius", TW_TYPE_FLOAT, &theRenderManager->ssvo_radius, " group=ssvo min=0 step=0.01");
// 		
// 		TwAddVarRW(mainTweakBar, "ssvo epsilon", TW_TYPE_FLOAT, &theRenderManager->ssvo_epsilon, " group=ssvo min=0 step=0.000005");
// 		TwAddVarRW(mainTweakBar, "ssvo depth_bias", TW_TYPE_FLOAT, &theRenderManager->ssvo_depth_bias, " group=ssvo min=0 step=0.00001");
// 		TwAddVarRW(mainTweakBar, "ssvo reject_dist_sqr", TW_TYPE_FLOAT, &theRenderManager->ssvo_reject_dist_sqr, " group=ssvo min=0 step=0.1");
// 		TwAddVarRW(mainTweakBar, "ssvo intensity_scale", TW_TYPE_FLOAT, &theRenderManager->ssvo_intensity_scale, " group=ssvo min=0 step=0.1");
// 
// 		TwAddVarRW(mainTweakBar, "ssvo blur length x", TW_TYPE_UINT32, &theRenderManager->ssvo_blur_length_x, " group=ssvo min=1 max=20");
// 		TwAddVarRW(mainTweakBar, "ssvo blur mul x", TW_TYPE_FLOAT, &theRenderManager->ssvo_blur_mul_x, " group=ssvo min=0.01 max=100 step=0.01");
// 		TwAddVarRW(mainTweakBar, "ssvo blur length y", TW_TYPE_UINT32, &theRenderManager->ssvo_blur_length_y, " group=ssvo min=1 max=20");
// 		TwAddVarRW(mainTweakBar, "ssvo blur mul y", TW_TYPE_FLOAT, &theRenderManager->ssvo_blur_mul_y, " group=ssvo min=0.01 max=100 step=0.01");

		// hdr
// 		TwAddVarRW(mainTweakBar, "hdr enabled", TW_TYPE_BOOLCPP, &theRenderManager->hdr_enabled, " group=hdr ");
// 		TwAddVarRW(mainTweakBar, "hdr blur length x", TW_TYPE_UINT32, &theRenderManager->hdr_light_blur_length_x, " group=hdr min=1 max=50");
// 		TwAddVarRW(mainTweakBar, "hdr blur length y", TW_TYPE_UINT32, &theRenderManager->hdr_light_blur_length_y, " group=hdr min=1 max=50");
// 		TwAddVarRW(mainTweakBar, "bright_threshold", TW_TYPE_FLOAT, &theRenderManager->hdr_bright_threshold, " group=hdr min=0 step=0.01");
// 		TwAddVarRW(mainTweakBar, "middle_gray", TW_TYPE_FLOAT, &theRenderManager->hdr_middle_gray, " group=hdr min=0 step=0.01");
// 		TwAddVarRW(mainTweakBar, "lum_white", TW_TYPE_FLOAT, &theRenderManager->hdr_lum_white, " group=hdr min=0 step=0.01");
	}

	TwBar* sceneTweakBar = TwNewBar("scene");
	{
		TwAddVarRW(sceneTweakBar, "camera near plane", TW_TYPE_FLOAT, &pCamera->znear, " group=camera min=0 step=0.05 ");
		TwAddVarRW(sceneTweakBar, "camera far plane", TW_TYPE_FLOAT, &pCamera->zfar, " group=camera min=0 step=1 ");
		TwAddVarRW(sceneTweakBar, "camera pan speed", TW_TYPE_FLOAT, &pCamera->cameraPanSpeed, " group=camera min=0 step=0.05 ");
		TwAddVarRW(sceneTweakBar, "camera rotate speed", TW_TYPE_FLOAT, &pCamera->cameraRotateSpeed, " group=camera min=0 step=0.0005 ");
			
		TwAddVarRW(sceneTweakBar, "sunlight dir", TW_TYPE_DIR3F, &sunlight->direction, " group=sunlight ");
		TwAddVarRW(sceneTweakBar, "sunlight color", TW_TYPE_COLOR3F, &sunlight->color, " group=sunlight ");

		TwAddVarRW(sceneTweakBar, "spotLight pos x", TW_TYPE_FLOAT, &spotLight->position.x, " group=spotLight ");
		TwAddVarRW(sceneTweakBar, "spotLight pos y", TW_TYPE_FLOAT, &spotLight->position.y, " group=spotLight ");
		TwAddVarRW(sceneTweakBar, "spotLight pos z", TW_TYPE_FLOAT, &spotLight->position.z, " group=spotLight ");
		TwAddVarRW(sceneTweakBar, "spotLight dir", TW_TYPE_DIR3F, &spotLight->direction, " group=spotLight ");
		TwAddVarRW(sceneTweakBar, "spotLight color", TW_TYPE_COLOR3F, &spotLight->color, " group=spotLight ");
		TwAddVarRW(sceneTweakBar, "spotLight innerAngle", TW_TYPE_FLOAT, &spotLight->innerConeAngle, " group=spotLight min=0 step=0.01 max=3.14 ");
		TwAddVarRW(sceneTweakBar, "spotLight outerAngle", TW_TYPE_FLOAT, &spotLight->outerConeAngle, " group=spotLight min=0 step=0.01 max=3.14 ");

		TwAddVarRW(sceneTweakBar, "background color", TW_TYPE_COLOR3F, &renderMgr->sceneBackgroundColor, " group=scene ");

		//TwAddVarRW(sceneTweakBar, "scale x", TW_TYPE_FLOAT, &entity->matrix._11, " group=scene min=0.001 max=1000 step=5 ");
		//TwAddVarRW(sceneTweakBar, "scale y", TW_TYPE_FLOAT, &entity->matrix._22, " group=scene min=0.001 max=1000 step=5 ");
		//TwAddVarRW(sceneTweakBar, "scale z", TW_TYPE_FLOAT, &entity->matrix._33, " group=scene min=0.001 max=1000 step=5 ");
			
		//TwAddVarRW(sceneTweakBar, "translate x", TW_TYPE_FLOAT, &entity->matrix._41, " group=scene ");
		//TwAddVarRW(sceneTweakBar, "translate y", TW_TYPE_FLOAT, &entity->matrix._42, " group=scene ");
		//TwAddVarRW(sceneTweakBar, "translate z", TW_TYPE_FLOAT, &entity->matrix._43, " group=scene ");
	}

}
