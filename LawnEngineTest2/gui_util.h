#pragma once

namespace gui_util
{
	template<class fn_type>
	void attach_event(CEGUI::Window* parent_window, const char* window_name, const CEGUI::String& event_name, const fn_type& uicmd)
	{
		CEGUI::Window* w = parent_window->getChildRecursive(window_name);
		if(w)
			w->subscribeEvent(event_name, CEGUI::Event::Subscriber(uicmd));
		else
			vcpp::log::err("window '%s' not found, event not attached.", window_name);
	}

	static float parseFloat(CEGUI::Window* window, float value_on_error=0.0f)
	{
		const CEGUI::String& txt = window->getText();
		
		float v;
		if(sscanf(txt.c_str(), "%f", &v)==1)
			return v;
		else
		{
// 			char buf[32];
// 			sprintf(buf, "%f", value_on_error);
// 			window->setText(buf);
			return value_on_error;
		}
	}
}
