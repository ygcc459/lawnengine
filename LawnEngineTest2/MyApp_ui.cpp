
#include "fast_compile.h"
#include "MyApp.h"

#include "UiCmdUtil.h"

#ifdef _DEBUG
#pragma comment(lib, "CEGUIDirect3D11Renderer-0_d.lib")
#pragma comment(lib, "CEGUIBase-0_d.lib")
#else
#pragma comment(lib, "CEGUIDirect3D11Renderer-0.lib")
#pragma comment(lib, "CEGUIBase-0.lib")
#endif

using namespace CEGUI;

//////////////////////////////////////////////////////////////////////////

void MyApp::init_ui()
{
	VCPP_SCOPED_CURRENT_DIR("cegui_res");

	LawnEngineUtil::setViewport(mainRenderView->getViewport());

	cegui_renderer = &CEGUI::Direct3D11Renderer::bootstrapSystem(RenderSystem::instance().device, RenderSystem::instance().deviceContext);
	
	CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");
	CEGUI::FontManager::getSingleton().createFromFile("DejaVuSans-12.font");

	CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-12");
	CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");
	CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultTooltipType("TaharezLook/Tooltip");

	using namespace CEGUI;
	WindowManager& wmgr = WindowManager::getSingleton();

	cegui_rootWindow = WindowManager::getSingleton().loadLayoutFromFile("sandbox_ui_layout.layout");
	cegui_rootWindow->setMousePassThroughEnabled(true);
	System::getSingleton().getDefaultGUIContext().setRootWindow(cegui_rootWindow);

	mainRenderView->signal_after_window_resize.connect([this](unsigned int w, unsigned int h)->void {
		cegui_renderer->setDisplaySize(CEGUI::Sizef((float)w, (float)h));
	});

	mainWindow = cegui_rootWindow->getChildRecursive("mainWindow");

	ui_lblFps = cegui_rootWindow->getChildRecursive("lblFps");

	init_stuff_list();
	init_renderDevWindow();
}

void MyApp::render_ui()
{
	char _buf[32];
	sprintf(_buf, "Fps: %.2f", g_lawnEngine.get_fps());
	ui_lblFps->setText(_buf);

	CEGUI::System::getSingleton().renderAllGUIContexts();
}
