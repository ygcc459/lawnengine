
#include "../LawnEngineFramework/lawnEngine.h"
#include "NextGenRenderManager.h"

#include "render_impl/DeferredLighting/DeferredLighting_LightingPass.h"
#include "render_impl/DeferredLighting/DeferredLightingRenderableEffectPlugin.h"
#include "render_impl/ToLinearDepthPass.h"
#include "render_impl/GenerateNoShadowMapPlugin.h"
#include "render_impl/AuxGeometryDrawPass.h"

//#include "render_impl/DeferredLighting/DeferredLighting_LightingPass.h"
//#include "render_impl/VarianceShadowMapping/GenerateVarianceShadowMapPlugin.h"
//#include "render_impl/VarianceShadowMapping/VarianceShadowMappingRenderPlugin.h"
//#include "render_impl/ssvo/SSVOPass.h"
//#include "render_impl/BilateralBlur.h"
//#include "render_impl/TextureOperationPass.h"
#include "render_impl/DownSamplePass.h"
#include "render_impl/MeasureLuminancePass.h"
#include "render_impl/hdr/HDRBrightPass.h"
#include "render_impl/hdr/HDRFinalPass.h"
#include "render_impl/OneDirectionBlur.h"

//////////////////////////////////////////////////////////////////////////

// sort mainly by material, secondly by distance (near to far)
struct DeferredRenderableSorter {
	bool operator () (const Renderable& a, const Renderable& b) {
		if(a.material==b.material)
			return a.center_distance < b.center_distance;
		
		return a.material < b.material;
	}
};

// sort only by distance (near to far)
struct TransparentRenderableSorter {
	bool operator () (const Renderable& a, const Renderable& b) {
		return a.center_distance < b.center_distance;
	}
};

// sort only by material
struct ForwardRenderableSorter {
	bool operator () (const Renderable& a, const Renderable& b) {
		return a.material < b.material;
	}
};

//////////////////////////////////////////////////////////////////////////

NextGenRenderManager::NextGenRenderManager(SceneManager* sceneMgr, CameraPtr camera, bool debugRenderTargetEnabled)
	:RenderManager(sceneMgr, camera),
	debugTextureRenderPass()
{
	// assign debug names
	scene_normal_shiness.name = "scene_normal_shiness";
	scene_linear_depth.name = "scene_linear_depth";
	light_accumulation.name = "light_accumulation";
	hdr_image.name = "hdr_image";
	greyed_hdr_image.name = "greyed_hdr_image";
	lighted_area.name = "lighted_area";
	half_blurred_lighted_area.name = "half_blurred_lighted_area";
	fully_blurred_lighted_area.name = "fully_blurred_lighted_area";

	set_debug_render_target(NULL);

	sceneBackgroundColor = float3(0.5f, 0.5f, 0.5f);

	RenderableEffectPlugin* plugins[] = {&dl_renderable_effect_plugin};
	RenderableEffect::setup_plugins(plugins, sizeof(plugins) / sizeof(plugins[0]));

	pass_lighting.init(&shadow_plugin);

	toLinearDepthPass.init();

	AuxGeometryDrawPass::instance().startup();

	// HDR & Post processing
	hdr_enabled = true;
	hdr_light_blur_length_x = hdr_light_blur_length_y = 10;
	hdr_bright_threshold = 0.5f;
	hdr_middle_gray = 0.72f;
	hdr_lum_white = 1.5f;

	measureLuminancePass.init();
	downSamplePass.init();
	brightPass.init();
	blurPass.init();
	toneMappingPass.init();

	//debug
	{
		if(debugRenderTargetEnabled) {
			debugTextureRenderPass = new TextureRenderPass();
			debugTextureRenderPass->init();
		}
	}
}

NextGenRenderManager::~NextGenRenderManager()
{
	SAFE_DELETE(debugTextureRenderPass);
	RenderableEffect::clear_plugins();
	AuxGeometryDrawPass::instance().shutdown();
}

void NextGenRenderManager::on_swap_chain_resized(uint w, uint h)
{
	set_debug_render_target(NULL);
	debuggable_render_targets.clear();

	scene_linear_depth.init(w, h, DXGI_FORMAT_R32_FLOAT);
	scene_normal_shiness.init(w, h, DXGI_FORMAT_R16G16B16A16_FLOAT);
	light_accumulation.init(w, h, DXGI_FORMAT_R16G16B16A16_FLOAT);

	scene_depth_stencil.init(w, h, DXGI_FORMAT_R24G8_TYPELESS, DXGI_FORMAT_D24_UNORM_S8_UINT, DXGI_FORMAT_R24_UNORM_X8_TYPELESS);

	hdr_image.init(w, h, DXGI_FORMAT_R16G16B16A16_FLOAT);
	greyed_hdr_image.init(w, h, DXGI_FORMAT_R16_FLOAT);
	lighted_area.init(w/3, h/3, DXGI_FORMAT_R8G8B8A8_UNORM);
	half_blurred_lighted_area.init(w/3, h/3, DXGI_FORMAT_R8G8B8A8_UNORM);
	fully_blurred_lighted_area.init(w/3, h/3, DXGI_FORMAT_R8G8B8A8_UNORM);
	downSamplePass.reset_source_texture(greyed_hdr_image.texture);

	//
	// debuggable_render_targets
	//
	{
		typedef std::map<std::string, ColorRenderTarget*>::value_type _pair_type;

		debuggable_render_targets.insert(_pair_type(scene_linear_depth.name, &scene_linear_depth));
		debuggable_render_targets.insert(_pair_type(scene_normal_shiness.name, &scene_normal_shiness));
		debuggable_render_targets.insert(_pair_type(light_accumulation.name, &light_accumulation));
		debuggable_render_targets.insert(_pair_type(hdr_image.name, &hdr_image));
		debuggable_render_targets.insert(_pair_type(greyed_hdr_image.name, &greyed_hdr_image));
		debuggable_render_targets.insert(_pair_type(lighted_area.name, &lighted_area));
		debuggable_render_targets.insert(_pair_type(half_blurred_lighted_area.name, &half_blurred_lighted_area));
		debuggable_render_targets.insert(_pair_type(fully_blurred_lighted_area.name, &fully_blurred_lighted_area));

		for(uint i=0; i<downSamplePass.down_sampled_texture_count(); i++)
		{
			ColorRenderTarget& crt = downSamplePass.get_down_sampled_texture(i);
			std::string name = formatString("downsampled_lum_%ux%u", crt.w, crt.h);
			debuggable_render_targets.insert(_pair_type(name, &crt));
		}
	}
	
}

void NextGenRenderManager::render(dxRenderTargetView renderTargetView, const Viewport& renderTargetViewport)
{
	Matrix view_matrix, proj_matrix, viewproj_matrix;
	camera->getViewMatrix(view_matrix);
	camera->getProjectionMatrix(proj_matrix, renderTargetViewport);
	viewproj_matrix = view_matrix * proj_matrix;

	Point3 cameraPos;
	camera->getPosition(cameraPos);

	//
	// build the render queue
	//
	pass_scene.setCamera(camera);
	pass_scene.setSceneMgr(sceneMgr);
	pass_scene.setViewport(renderTargetViewport);
	
	Frustum camera_frustum(viewproj_matrix);
	scene_renderables.collect(sceneMgr, camera_frustum, cameraPos, SOA_VISIBLE, SOA_VISIBLE);

	RenderableContainer& deferred_renderables = scene_renderables.get_renderables(PRQ_Deferred);

	std::sort(deferred_renderables.begin(), deferred_renderables.end(), DeferredRenderableSorter());

	//
	// deferred lighting - GBuffer
	//
	{
		LawnEngineUtil::clearDepthStencil(scene_depth_stencil, 1.0f);
		LawnEngineUtil::clearRenderTarget(scene_normal_shiness, float4(0.0f, 0.0f, 0.0f, 0.0f));

		LawnEngineUtil::setViewport(renderTargetViewport);

		LawnEngineUtil::setRenderTargets(scene_normal_shiness, scene_depth_stencil);

		pass_scene.setRenderTechnique("gbuffer");
		pass_scene.render(deferred_renderables);

		LawnEngineUtil::releaseRenderTargets();
	}

	//
	// to linear depth
	//
	toLinearDepthPass.hardwareDepth_to_linearDepth(scene_depth_stencil, scene_linear_depth, *camera);

	//
	// deferred lighting - Lights
	//
	{
		LawnEngineUtil::clearRenderTarget(light_accumulation, float4(0.0f, 0.0f, 0.0f, 0.0f));

		pass_lighting.setRenderTargets(light_accumulation, scene_depth_stencil);
		pass_lighting.setInputs(scene_linear_depth, scene_normal_shiness, view_matrix, proj_matrix, cameraPos);

		pass_lighting.render_pointLights(scene_renderables.get_pointLights().begin(), scene_renderables.get_pointLights().end());
		pass_lighting.render_directionalLights(scene_renderables.get_directionalLights().begin(), scene_renderables.get_directionalLights().end());
		pass_lighting.render_spotLights(scene_renderables.get_spotLights().begin(), scene_renderables.get_spotLights().end());

		pass_lighting.releaseInputs();
	}

	//
	// deferred lighting - final
	//
	LawnEngineUtil::clearRenderTarget(hdr_image, float4(sceneBackgroundColor, 1.0f));

	LawnEngineUtil::setRenderTargets(hdr_image, scene_depth_stencil);	
	{
		LawnEngineUtil::setViewport(renderTargetViewport);

		//
		// deferred rendering
		//
		dl_renderable_effect_plugin.set_inputs(light_accumulation, scene_normal_shiness);

		pass_scene.setRenderTechnique("deferred_lighting");
		pass_scene.render(deferred_renderables);

		dl_renderable_effect_plugin.realease_inputs();

		//
		// transparent rendering
		//
		RenderableContainer& transparent_renderables = scene_renderables.get_renderables(PRQ_Transparent);
		std::sort(transparent_renderables.begin(), transparent_renderables.end(), TransparentRenderableSorter());
	
		pass_scene.setRenderTechnique("transparent_rendering");
		pass_scene.render(transparent_renderables);
	
		//
		// forward rendering
		//
		RenderableContainer& forward_renderables = scene_renderables.get_renderables(PRQ_Forward);
		std::sort(forward_renderables.begin(), forward_renderables.end(), ForwardRenderableSorter());
	
		pass_scene.setRenderTechnique("forward_rendering");
		pass_scene.render(forward_renderables);
	}
	LawnEngineUtil::releaseRenderTargets();

	//
	// aux geometry
	//
 	AuxGeometryDrawPass::instance().begin_draw(viewproj_matrix, hdr_image, scene_depth_stencil);
 	AuxGeometryDrawPass::instance().draw_all();
 	AuxGeometryDrawPass::instance().end_draw();

	//
	// HDR & Post Processing
	//
	{
		measureLuminancePass.measure_luminance_rgb(hdr_image, greyed_hdr_image);

		ColorRenderTarget& greyed_hdr_image_1x1 = downSamplePass.do_down_sample(greyed_hdr_image);

		brightPass.set_parameters(hdr_bright_threshold, hdr_middle_gray, hdr_lum_white);
		brightPass.do_bright_pass(hdr_image, greyed_hdr_image_1x1, lighted_area);

		blurPass.begin_blur();
		blurPass.blur(lighted_area, half_blurred_lighted_area, float2(1.0f, 0.0f), hdr_light_blur_length_x);
		blurPass.blur(half_blurred_lighted_area, fully_blurred_lighted_area, float2(0.0f, 1.0f), hdr_light_blur_length_y);
		blurPass.end_blur();

		toneMappingPass.set_parameters(hdr_middle_gray, hdr_lum_white);
		toneMappingPass.do_final_pass(hdr_image, greyed_hdr_image_1x1, fully_blurred_lighted_area, renderTargetView, renderTargetViewport);
	}
	
	//
	// debug texture
	//
	if(debugging_texture!=NULL && debugTextureRenderPass!=NULL)
	{
		LawnEngineUtil::setViewport(renderTargetViewport);
		LawnEngineUtil::setRenderTargets(renderTargetView, scene_depth_stencil);
		debugTextureRenderPass->render_texture(debugging_texture, debugging_range_min, debugging_range_max, debugging_r, debugging_g, debugging_b, debugging_a);
		LawnEngineUtil::releaseRenderTargets();
	}
}

void NextGenRenderManager::getLightViewProjMatrix(const SpotLight& light, float shadowMinDistance, float shadowMaxDistance, const Viewport& viewport, Matrix& outViewMat, Matrix& outProjMat)
{
	//FIXME: left will be invalid if light.direction is parallel to Vector3::up
	Vector3 left; Vector3::cross(left, light.direction, Vector3::up());
	Vector3 up; Vector3::cross(up, left, light.direction);
	up.normalize();

	Matrix::createLookAtLH(outViewMat, light.position, light.position + light.direction, up);

	float aspect = float(viewport.width) / viewport.height;
	Matrix::createPerspectiveFovLH(outProjMat, light.outerConeAngle, aspect, shadowMinDistance, shadowMaxDistance);
}

void NextGenRenderManager::set_debug_render_target(dxShaderResourceView crt, float min, float max, bool r, bool g, bool b, bool a)
{
	this->debugging_texture = crt;

	debugging_range_min = min;
	debugging_range_max = max;
	debugging_r = r;
	debugging_g = g;
	debugging_b = b;
	debugging_a = a;
}

