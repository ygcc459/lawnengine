#pragma once

#include "../LawnEngineFramework/engine/engine.h"
#include "../LawnEngineFramework/engine/InputSystem/InputManager.h"

class RenderView;
typedef std::shared_ptr<RenderView> RenderViewPtr;

struct GizmoGroup;

class MyApp : public LawnEnginePlugin
{
public:
	MyApp();
	~MyApp();

	virtual bool preInit() override;
	virtual bool init();
	virtual bool destroy();

	virtual void onRender();
	virtual void onUpdateScene(float fElapsedTime);

public:
	HWND windowHwnd;
};
