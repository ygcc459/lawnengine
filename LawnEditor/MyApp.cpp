
#include <windows.h>
#include "MyApp.h"

#include <vcpp/math.h>
#include <vcpp/Logger.h>
#include <vcpp/Windows.h>

#include "AssetSystem/AssetManager.h"
#include "EntitySystem/MeshRenderer.h"
#include "EntitySystem/Camera.h"
#include "EntitySystem/FreeCameraController.h"
#include "EntitySystem/Light.h"
#include "RenderSystem/RenderView.h"
#include "LawnEngine.h"
#include "EntitySystem/World.h"
#include "EntitySystem/SceneObject.h"
#include "EntitySystem/Light.h"
#include "EntitySystem/Transform.h"
#include "EntitySystem/SceneSettings.h"
#include "RenderSystem/RenderView.h"
#include "RenderSystem/Sprite.h"
#include "UISystem/UICanvas.h"
#include "UISystem/UIImage.h"
#include "UISystem/UIText.h"
#include "UISystem/UIButton.h"
#include "UISystem/UIInput.h"
#include "UISystem/UILinearLayout.h"
#include "UISystem/UIInspector.h"
#include "UISystem/UISceneHierarchyView.h"
#include "EntitySystem/GizmoRenderer.h"
#include "EntitySystem/SceneSettings.h"
#include "UISystem/imgui/imgui.h"
#include "D3D11RHI/D3D11RHI.h"
#include "RenderSystem/RenderScene.h"
#include "RenderSystem/ImageBasedLighting.h"

int fontSize = 24;

//////////////////////////////////////////////////////////////////////////

MyApp::MyApp()
{
}

MyApp::~MyApp()
{

}

bool MyApp::preInit()
{
	//SetCurrentDirectory("../lawnenginemedia/");

	AssetPath projectPath = AssetPath(AST_AbsolutePath, FileSystem::Default().GetCurrentPath() + "/../TestProject");

	g_lawnEngine.setProjectPath(projectPath);

	return true;
}

bool MyApp::init()
{
	AssetManager& assetManager = g_lawnEngine.getAssetManager();

	RECT renderViewClientRect;
	GetClientRect(windowHwnd, &renderViewClientRect);

	RenderViewPtr mainRenderView = g_lawnEngine.createRenderView(RenderViewDesc(renderViewClientRect.right, renderViewClientRect.bottom, windowHwnd));

	SceneObject* sceneSettingsSO = g_lawnEngine.getWorld().CreateSceneObject();
	sceneSettingsSO->name = "SceneSettings";

	SceneSettings* sceneSettings = sceneSettingsSO->AddComponent<SceneSettings>();
	sceneSettings->renderSettings->skybox = assetManager.Load<Texture2D>(assetManager.AssetsDir() / "hdrenvmap/SingleLight01.Texture2D");
	//sceneSettings->renderSettings->skyboxIBLData = assetManager.Load<TextureIBLData>(assetManager.AssetsDir() / "hdrenvmap/SingleLight01.TextureIBLData");

	SceneObject* lightSceneObject = g_lawnEngine.getWorld().CreateSceneObject();
	lightSceneObject->name = "MainLight";
	//lightSceneObject->GetTransform().SetWorldForward(normalize(float3(-1, -2, 1)));
	lightSceneObject->GetTransform().SetLookAt(normalize(float3(0, -1, 0)), normalize(float3(1, 0, 0)));

	DirectionalLight* mainLight = lightSceneObject->AddComponent<DirectionalLight>();
	mainLight->color = float3(1.0f, 1.0f, 1.0f);
	mainLight->intensity = 3.14f;

	SceneObject* cameraSceneObject = g_lawnEngine.getWorld().CreateSceneObject();
	cameraSceneObject->name = "MainCamera";

	Camera* mainCamera = cameraSceneObject->AddComponent<Camera>();
	mainCamera->SetRenderTarget(mainRenderView);

	FreeCameraController* cameraController = cameraSceneObject->AddComponent<FreeCameraController>();
	cameraSceneObject->GetTransform().SetWorldPosition(float3(4, 4, -4));
	cameraSceneObject->GetTransform().SetLookAt(float3(0, 0, 0));
	g_lawnEngine.getInputManager().setKeyFocus(cameraController, nullptr);

	//SceneObjectPtr spherePrefab = assetManager.Load<SceneObject>(assetManager.AssetsDir() / "sphere_1.SceneObject");
	//SceneObject* sphere1 = SceneObject::Instanciate(spherePrefab, g_lawnEngine.getWorld());

	bool SaveTestResources = false;

	if (SaveTestResources)
	{
		Texture2DPtr brickwallTex = assetManager.Load<Texture2D>(assetManager.AssetsDir() / "brickwall.Texture2D");

		Image2DMipmaps image(512, 512, PF_R8G8B8A8, 1);
		for (int y = 0; y < image.height; y++)
		{
			for (int x = 0; x < image.width; x++)
			{
				byte r = (byte)math::floor_int(((float)x / image.width - 1) * 255);
				byte g = (byte)math::floor_int(((float)y / image.height - 1) * 255);
				byte b = (byte)math::floor_int(math::rand(0.0f, 1.0f) * 255);
				byte a = 255;
				PixelAccess<PF_R8G8B8A8>::WriteWithConvert(image.GetMipmap(0), x, y, float4(0.5f, 0.5f, 0.5f, 1.0f));
			}
		}

		Texture2DPtr baseTex = std::make_shared<Texture2D>();
		baseTex->Create(image);

		ShaderPtr shader = assetManager.Load<Shader>(assetManager.ProjectDir() / "Shaders/PBRStandard.shader");

		MeshPtr boxMesh = Mesh::CreateBox(float3(0, 0, 0), float3(1, 1, 1));
		assetManager.Save(boxMesh, assetManager.AssetsDir() / "Box.mesh");

		MeshPtr sphereMesh = Mesh::CreateSphere(float3(0, 0, 0), 1.0f, 64, 64);
		assetManager.Save(sphereMesh, assetManager.AssetsDir() / "Sphere.mesh");

		MeshPtr planeMesh = Mesh::CreatePlane(float3(-2.5f, 0, -2.5f), 5, 5);
		assetManager.Save(planeMesh, assetManager.AssetsDir() / "Plane.mesh");

		MaterialPtr material(new Material());
		material->BindShader(shader);
		material->SetFloat3("BaseColor", float3(0.9f, 0.5f, 0.3f));
		material->SetResource("BaseColorTex", brickwallTex);
		material->SetFloat("Roughness", 0.25f);
		material->SetFloat("Metallic", 0.5f);
		assetManager.Save(material, assetManager.AssetsDir() / "mat1.m");

		SceneObject* sphereSceneObject = g_lawnEngine.getWorld().CreateSceneObject();
		sphereSceneObject->GetTransform().SetWorldPosition(float3(2, 0, 0));
		MeshRenderer* rd = sphereSceneObject->AddComponent<MeshRenderer>();
		rd->SetMesh(sphereMesh);
		rd->SetMaterial(material);

		SceneObjectPtr spherePrefab = SceneObject::CreateTemplate(sphereSceneObject);
		assetManager.Save(spherePrefab, assetManager.AssetsDir() / "Sphere.SceneObject");
	}

	//MeshPtr boxMesh = assetManager.Load<Mesh>(assetManager.AssetsDir() / "Box.mesh");

	//MeshPtr sphereMesh = assetManager.Load<Mesh>(assetManager.AssetsDir() / "Sphere.mesh");

	//MaterialPtr material = assetManager.Load<Material>(assetManager.AssetsDir() / "mat1.m");

	//SceneObjectPtr boxPrefab = assetManager.Load<SceneObject>(assetManager.AssetsDir() / "Box.SceneObject");
	//SceneObject* boxSceneObject = SceneObject::Instanciate(boxPrefab, g_lawnEngine.getWorld());

	SceneObjectPtr spherePrefab = assetManager.Load<SceneObject>(assetManager.AssetsDir() / "sphere.SceneObject");
	SceneObject* sphereSceneObject = SceneObject::Instanciate(spherePrefab, g_lawnEngine.getWorld());

	SceneObjectPtr couchPrefab = assetManager.Load<SceneObject>(assetManager.AssetsDir() / "BasicScene1/SM_BS_Guest_Couch.SceneObject");
	SceneObject* couchSceneObject = SceneObject::Instanciate(couchPrefab, g_lawnEngine.getWorld());

#if 1
	{
		SpriteAtlasPtr atlas = assetManager.Load<SpriteAtlas>(assetManager.ProjectDir() / "DefaultUIAtlas.SpriteAtlas");

		SceneObject* canvasObj = g_lawnEngine.getWorld().CreateSceneObject();
		canvasObj->name = "UICanvas";

#if 0
		UICanvas* canvas = canvasObj->AddComponent<UICanvas>();

		mainRenderView->AddUICanvas(canvas);

		SceneObject* backPanelSO = canvasObj->AddChild("BackPanel");
		{
			UIImage* image1 = backPanelSO->AddComponent<UIImage>();
			image1->SetRelativePositionsAndPaddings(edge4f(0, 0, 0.25f, 0.5f), edge4f(10, 10, 10, 10));
			image1->SetColor(float4(1, 1, 1, 0.5));
			image1->SetSprite(atlas, "Panel");
		}

		SceneObject* backPanelLinearSO = backPanelSO->AddChild("BackPanelLinear");
		{
			UILinearLayout* layout = backPanelLinearSO->AddComponent<UILinearLayout>();
			layout->SetDirection(LinearLayoutDirection::Vertical);
			layout->SetItemMinSize(32);
			layout->fill = false;
		}

		SceneObject* testSO1 = backPanelLinearSO->AddChild("testSO1");
		{
			UIImage* image1 = testSO1->AddComponent<UIImage>();
			image1->SetColor(float4(1, 0, 0, 0.5));
			image1->SetSprite(atlas, "Panel");
		}
		SceneObject* testSO2 = backPanelLinearSO->AddChild("testSO2");
		{
			UIImage* image1 = testSO2->AddComponent<UIImage>();
			image1->SetColor(float4(0, 1, 0, 0.5));
			image1->SetSprite(atlas, "Panel");
		}
#endif

#if 0
		SceneObject* backPanelLinearSO = backPanelSO->AddChild("BackPanelLinear");
		{
			UILinearLayout* layout = backPanelLinearSO->AddComponent<UILinearLayout>();
			layout->SetDirection(LinearLayoutDirection::Vertical);
			layout->SetItemMinSize(32);
			layout->fill = false;
		}

		SceneObject* testBtnSO = backPanelLinearSO->AddChild("Refresh UI Button");
		{
			UIButton* comp = testBtnSO->AddComponent<UIButton>();
			comp->GetSceneObject().GetComponentInChildren<UIText>()->SetText("Refresh UI");

			comp->OnClick = [=]()
			{
				canvas->MarkLayoutDirty();
			};
		}
#endif

#if 0
		SceneObject* titleTextSO = backPanelLinearSO->AddChild("TitleText");
		{
			UIText* text1 = titleTextSO->AddComponent<UIText>();
			text1->SetText("Hello LawnEngine!");
			text1->SetAlignment(TextAlignmentHorizontal_Left, TextAlignmentVertical_Bottom);
		}

		SceneObject* fontSizePlusBtnSO = backPanelLinearSO->AddChild("Font+");
		{
			UIButton* comp = fontSizePlusBtnSO->AddComponent<UIButton>();
			comp->GetSceneObject().GetComponentInChildren<UIText>()->SetText("FontSize+");

			comp->OnClick = [=]()
			{
				fontSize++;
				titleTextSO->GetComponent<UIText>()->SetText(formatString("Hello LawnEngine! fontSize=%d", fontSize));
				titleTextSO->GetComponent<UIText>()->SetFontSize(fontSize);
			};
		}

		SceneObject* fontSizeMinusBtnSO = backPanelLinearSO->AddChild("Font-");
		{
			UIButton* comp = fontSizeMinusBtnSO->AddComponent<UIButton>();
			comp->GetSceneObject().GetComponentInChildren<UIText>()->SetText("FontSize-");

			comp->OnClick = [=]()
			{
				fontSize--;
				titleTextSO->GetComponent<UIText>()->SetText(formatString("Hello LawnEngine! fontSize=%d", fontSize));
				titleTextSO->GetComponent<UIText>()->SetFontSize(fontSize);
			};
		}

		SceneObject* inputSO = backPanelLinearSO->AddChild("Input");
		{
			UIInput* input = inputSO->AddComponent<UIInput>();
			input->SetText("Input box is here!");
		}
#endif

//  		SceneObject* inspectorSO = backPanelLinearSO->AddChild("Inspector");
//  		{
//  			MeshRenderer* rd = testPrefab->GetComponentInChildren<MeshRenderer>();
//  			MaterialPtr mat = rd ? rd->GetMaterial() : nullptr;
//  
//  			UIObjectInspector* inspector = inspectorSO->AddComponent<UIObjectInspector>();
//  			inspector->SetTargetObject(mat.get());
//  		}
// 
// 		SceneObject* sceneHierarchyViewSO = backPanelLinearSO->AddChild("SceneHierarchyView");
// 		{
// 			UISceneHierarchyView* sceneHierarchyView = sceneHierarchyViewSO->AddComponent<UISceneHierarchyView>();
// 			sceneHierarchyView->SetRelativePositionsAndPaddings(edge4f(0, 0, 1, 1), edge4f(1, 1, 1, 1));
//  		}

		SceneObjectPtr canvasPrefab = SceneObject::CreateTemplate(canvasObj);
		assetManager.Save(canvasPrefab, assetManager.AssetsDir() / "TestUI.SceneObject");
	}
#endif

	return true;
}

bool MyApp::destroy()
{
	return true;
}

//////////////////////////////////////////////////////////////////////////

void MyApp::onRender()
{
}

void MyApp::onUpdateScene(float fElapsedTime)
{
}
