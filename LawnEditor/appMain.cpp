
#include "MyApp.h"

#include <windows.h>

#include "UISystem/imgui/imgui_impl_win32.h"
extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

int renderViewW = 1980;
int renderViewH = 1260;

MyApp* theApp = NULL;

int runMessageLoop()
{
	BOOL bGotMsg;
	MSG  msg;
	PeekMessage(&msg, NULL, 0U, 0U, PM_NOREMOVE);

	while(WM_QUIT!=msg.message)
	{
		if(g_lawnEngine.isPaused())
			bGotMsg = GetMessage(&msg, NULL, 0U, 0U);
		else
			bGotMsg = PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE);

		if(bGotMsg)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			g_lawnEngine.stepOneFrame();
		}
	}

	g_lawnEngine.shutdown();

	return (int)msg.wParam;
}

void resizeWindowByClientArea(HWND hwnd, int clientW, int clientH)
{
	RECT originalWindowRect;
	GetWindowRect(hwnd, &originalWindowRect);

	POINT pt1 = {originalWindowRect.left, originalWindowRect.top};
	ScreenToClient(hwnd, &pt1);

	pt1.x = -pt1.x;
	pt1.y = -pt1.y;

	int newW = 2 * pt1.x + clientW;
	int newH = pt1.x + pt1.y + clientH;
	SetWindowPos(hwnd, HWND_TOP, -1, -1, newW, newH, SWP_SHOWWINDOW|SWP_NOMOVE);
}

LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	//if(g_lawnEngine.handleWindowsEvents(hwnd, message, wParam, lParam)==true)
	//	return 0;

	if (ImGui_ImplWin32_WndProcHandler(hwnd, message, wParam, lParam))
		return true;

	switch (message)
	{
	case WM_CHAR:
		{
			char cc = (char)wParam;
			g_lawnEngine.getInputManager().triggerCharacterInput(cc);
			//if (wParam > 127)
			//	Log::Info("WM_CHAR %X %X", wParam, lParam);
			//else
			//	Log::Info("WM_CHAR %d(%c) %d", wParam, wParam, lParam);
			return false;
		}
	case WM_IME_CHAR:
		{
			Log::Info("WM_IME_CHAR %d %d", wParam, lParam);
			return false;
		}
	case WM_UNICHAR:
		{
			Log::Info("WM_UNICHAR %d %d", wParam, lParam);
			return false;
		}
	case WM_ACTIVATEAPP:
		{
			if( ((BOOL)wParam) == TRUE )
				g_lawnEngine.setPaused(false);
			else
				g_lawnEngine.setPaused(true);

			return false;
		}
	case WM_KILLFOCUS:
		{
			g_lawnEngine.setPaused(true);
			return false;
		}
	case WM_SETFOCUS:
		{
			g_lawnEngine.setPaused(false);
			return false;
		}
	case WM_SIZE:
		{
			switch(wParam)
			{
			case SIZE_MAXIMIZED:
			case SIZE_MAXSHOW:
			case SIZE_RESTORED:
				{
					unsigned w = (unsigned)LOWORD(lParam);
					unsigned h = (unsigned)HIWORD(lParam);
					g_lawnEngine.resizeRenderView(hwnd, w, h);
					return false;
				}
			case SIZE_MAXHIDE:
			case SIZE_MINIMIZED:
				{
					return false;
				}
			}
		}
	case WM_CREATE:
		return (0);
	case WM_DESTROY:
		PostQuitMessage (0);
		return (0);
	}

	return DefWindowProc (hwnd, message, wParam, lParam);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int iCmdShow)
{
	int tmpFlag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
	tmpFlag |= _CRTDBG_LEAK_CHECK_DF;
	_CrtSetDbgFlag(tmpFlag);

    static TCHAR szAppName[] = TEXT ("LawnEngineTest");
    HWND         hwnd;

    WNDCLASSEX   wndclassex = {0};
    wndclassex.cbSize        = sizeof(WNDCLASSEX);
    wndclassex.style         = CS_HREDRAW | CS_VREDRAW;
    wndclassex.lpfnWndProc   = WndProc;
    wndclassex.cbClsExtra    = 0;
    wndclassex.cbWndExtra    = 0;
    wndclassex.hInstance     = hInstance;
    wndclassex.hIcon         = LoadIcon (NULL, IDI_APPLICATION);
    wndclassex.hCursor       = LoadCursor (NULL, IDC_ARROW);
    wndclassex.hbrBackground = (HBRUSH) GetStockObject (WHITE_BRUSH);
    wndclassex.lpszMenuName  = NULL;
    wndclassex.lpszClassName = szAppName;
    wndclassex.hIconSm       = wndclassex.hIcon;
	
    if (!RegisterClassEx (&wndclassex))
    {
        MessageBox (NULL, TEXT ("RegisterClassEx failed!"), szAppName, MB_ICONERROR);
        return 0;
    }

    hwnd = CreateWindowEx (WS_EX_OVERLAPPEDWINDOW, 
		                  szAppName, 
        		          TEXT ("WindowTitle"),
                		  WS_OVERLAPPEDWINDOW,
		                  CW_USEDEFAULT, 
        		          CW_USEDEFAULT, 
                		  CW_USEDEFAULT, 
		                  CW_USEDEFAULT, 
        		          NULL, 
                		  NULL, 
		                  hInstance,
        		          NULL); 
						  
    ShowWindow (hwnd, iCmdShow);
    UpdateWindow (hwnd);

	resizeWindowByClientArea(hwnd, renderViewW, renderViewH);

	theApp = new MyApp();
	theApp->windowHwnd = hwnd;

	int retCode = -1;

	g_lawnEngine.registerPlugin(theApp);

	if(g_lawnEngine.startup(hwnd))
	{
		retCode = runMessageLoop();
	}

	delete theApp;

	return retCode;
}

