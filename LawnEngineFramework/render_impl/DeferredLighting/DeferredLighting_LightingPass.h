#pragma once

class DeferredLighting_LightingPass : public RenderPass
{
	int dl_lightColor;
	int dl_lightDirection;
	int pl_lightColor;
	int pl_lightRadius;
	int pl_lightPosition;
	int pl_light_obj_wvp;
	int sl_lightColor;
	int sl_lightPosition;
	int sl_lightDirection;
	int sl_light_cos_inner_outer;
	int sl_light_obj_wvp;

	ShaderVariable* var_matInvProj;
	
	ShaderCBufferSlot cb_per_scene;

	int cb_directionalLight;
	int cb_pointLight;
	int cb_spotLight;

	int var_linear_depth_tex;
	int var_gbuffer0_tex;

	EffectTechnique* tech_directionalLight;
	EffectTechnique* tech_pointLight;
	EffectTechnique* tech_spotLight;
	EffectTechnique* tech_spotLight_shadowed;

	ShaderPtr shader;
	ShaderParamValues shader_params;

	float4x4 cameraView, cameraViewProj;
	float3 cameraPos;

	MeshPtr sphereGeometry;
	MeshPtr coneGeometry;

	dxRasterizerState rs_cullBack;
	dxRasterizerState rs_cullFront;
	dxRasterizerState rs_noCull;

	dxDepthStencilState dss_depthTest;
	dxDepthStencilState dss_noDepthTest;

public:
	float cone_base_distance;

public:
	DeferredLighting_LightingPass()
		:cone_base_distance(1000.0f), rs_cullBack(), rs_cullFront()
	{
		
	}

	virtual ~DeferredLighting_LightingPass(void)
	{
		SAFE_RELEASE(rs_cullFront);
		SAFE_RELEASE(rs_cullBack);
		SAFE_RELEASE(rs_noCull);
	}

	void init(RenderPassPlugin* shadowRenderPlugin=NULL)
	{
		dxDevice device = RenderSystem::instance().device;

		struct on_effect_loaded {
			typedef void result_type;

			DeferredLighting_LightingPass* _this;

			on_effect_loaded(DeferredLighting_LightingPass* _this)
				:_this(_this)
			{}

			void operator () (ShaderPtr effect)
			{
				_this->tech_directionalLight = effect->get_technique("directionalLight");
				_this->tech_pointLight = effect->get_technique("pointLight");
				_this->tech_spotLight = effect->get_technique("spotLight");
				_this->tech_spotLight_shadowed = effect->get_technique("spotLightShadowed");
			}
		};

		if(shadowRenderPlugin)
		{
			std::string header = "#include <" + std::string(shadowRenderPlugin->get_file_name()) + ">\n";

			effect = Effect::create("DeferredLighting_LightingPass", "DeferredLighting_LightingPass.fx", 
				on_effect_loaded(this), header);
		}
		else
		{
			effect = Effect::create("DeferredLighting_LightingPass", "DeferredLighting_LightingPass.fx", 
				on_effect_loaded(this));
		}

		shader_params.bind_to_shader(effect);

		this->var_matInvProj = shader_params.GetVariable("matInvProj");
		this->dl_lightColor = shader_params.GetVariable("dl_lightColor");
		this->dl_lightDirection = shader_params.GetVariable("dl_lightDirection");
		this->pl_lightColor = shader_params.GetVariable("pl_lightColor");
		this->pl_lightRadius = shader_params.GetVariable("pl_lightRadius");
		this->pl_lightPosition = shader_params.GetVariable("pl_lightPosition");
		this->pl_light_obj_wvp = shader_params.GetVariable("pl_light_obj_wvp");
		this->sl_lightColor = shader_params.GetVariable("sl_lightColor");
		this->sl_lightPosition = shader_params.GetVariable("sl_lightPosition");
		this->sl_lightDirection = shader_params.GetVariable("sl_lightDirection");
		this->sl_light_cos_inner_outer = shader_params.GetVariable("sl_light_cos_inner_outer");
		this->sl_light_obj_wvp = shader_params.GetVariable("sl_light_obj_wvp");

		this->var_linear_depth_tex = shader_params.GetVariable("linear_depth_texture");
		this->var_gbuffer0_tex = shader_params.GetVariable("gbuffer0_texture");

		this->cb_per_scene = shader_params.GetVariable("cb_per_scene");
		this->cb_directionalLight = shader_params.GetVariable("cb_directionalLight");
		this->cb_pointLight = shader_params.GetVariable("cb_pointLight");
		this->cb_spotLight = shader_params.GetVariable("cb_spotLight");

		sphereGeometry = Mesh::create_sphere(float3(0,0,0), 1.0f, 32, 32);
		coneGeometry = Mesh::create_cone(float3(0,0,0), float3(0,0,1), 1.0f, 32, true);

		{
			D3D11_RASTERIZER_DESC rs_desc;
			rs_desc.FillMode = D3D11_FILL_SOLID;
			rs_desc.FrontCounterClockwise = FALSE;
			rs_desc.DepthBias = 0;
			rs_desc.DepthBiasClamp = 0.0f;
			rs_desc.SlopeScaledDepthBias = 0.0f;
			rs_desc.DepthClipEnable = FALSE;
			rs_desc.ScissorEnable = FALSE;
			rs_desc.MultisampleEnable = FALSE;
			rs_desc.AntialiasedLineEnable = FALSE;

			rs_desc.CullMode = D3D11_CULL_BACK;
			device->CreateRasterizerState(&rs_desc, &rs_cullBack);

			rs_desc.CullMode = D3D11_CULL_FRONT;
			device->CreateRasterizerState(&rs_desc, &rs_cullFront);

			rs_desc.CullMode = D3D11_CULL_NONE;
			device->CreateRasterizerState(&rs_desc, &rs_noCull);
		}

		{
			D3D11_DEPTH_STENCIL_DESC dss_desc;
			dss_desc.StencilEnable = TRUE;
			dss_desc.StencilReadMask = 0xFF;
			dss_desc.StencilWriteMask = 0x00;
			dss_desc.FrontFace.StencilFunc = D3D11_COMPARISON_EQUAL;
			dss_desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
			dss_desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
			dss_desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
			dss_desc.BackFace.StencilFunc = D3D11_COMPARISON_EQUAL;
			dss_desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
			dss_desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
			dss_desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
			
			dss_desc.DepthEnable = TRUE;
			dss_desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
			dss_desc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
			device->CreateDepthStencilState(&dss_desc, &dss_depthTest);
			
			dss_desc.DepthEnable = FALSE;
			device->CreateDepthStencilState(&dss_desc, &dss_noDepthTest);
		}
	}

	void setRenderTargets(dxRenderTargetView lighting, dxDepthStencilView depth) {
		LawnEngineUtil::setRenderTargets(lighting, depth);
	}
	
	void setInputs(dxShaderResourceView depth_texture, dxShaderResourceView normal_texture, 
		const float4x4& cameraView, const float4x4& cameraProj, const float3& cameraPos)
	{
		var_linear_depth_tex->set_resource_view(depth_texture);
		var_gbuffer0_tex->set_resource_view(normal_texture);

		this->cameraView = cameraView;

		this->cameraViewProj = cameraView * cameraProj;

		float4x4 invProj = cameraProj;
		invProj.inverse();
		var_matInvProj->set_matrix(invProj);

		cb_per_scene.update_to_device();

		this->cameraPos = cameraPos;
	}

	template<class iterater_t>
	void render_directionalLights(iterater_t begin, iterater_t end)
	{
		dxDeviceContext deviceContext = RenderSystem::instance().deviceContext;

		LawnEngineUtil::full_screen_quad_geometry().pre_render();

		deviceContext->RSSetState(rs_noCull);
		deviceContext->OMSetDepthStencilState(dss_noDepthTest, 0x01);

		for(uint i=0; i<tech_directionalLight->pass_count(); i++)
		{
			EffectPass& pass = tech_directionalLight->get_pass(i);

			pass.bind_states(shader_params);

			for(iterater_t it=begin; it!=end; ++it)
			{
				const DirectionalLight& light = (*it);

				float3 lightDir;
				light.direction.transformNormal(cameraView, OUT &lightDir);
				dl_lightDirection->set_vector3(lightDir);

				dl_lightColor->set_vector3(light.color);

				cb_directionalLight.update_to_device();

				LawnEngineUtil::full_screen_quad_geometry().render();
			}
		}		
	}

	template<class iterater_t>
	void render_pointLights(iterater_t begin, iterater_t end)
	{
		dxDeviceContext deviceContext = RenderSystem::instance().deviceContext;

		sphereGeometry->pre_render();

		for(uint i=0; i<tech_pointLight->pass_count(); i++)
		{
			EffectPass& pass = tech_pointLight->get_pass(i);

			pass.bind_states(shader_params);

			for(iterater_t it=begin; it!=end; ++it)
			{
				const PointLight& pointLight = (*it);

				float3 lightPos;
				pointLight.position.transformCoord(cameraView, OUT &lightPos);
				pl_lightPosition->set_vector3(lightPos);

				pl_lightRadius->set_float(pointLight.range);

				pl_lightColor->set_vector3(pointLight.color);

				// light_obj_wvp
				{
					float4x4 mat_world(
						pointLight.range, 0, 0, 0,
						0, pointLight.range, 0, 0,
						0, 0, pointLight.range, 0,
						pointLight.position.x, pointLight.position.y, pointLight.position.z, 1.0f);

					float4x4 mat_wvp = mat_world * cameraViewProj;
					pl_light_obj_wvp->set_matrix(mat_wvp);
				}
		
				cb_pointLight.update_to_device();
				
				float3 dist = pointLight.position - cameraPos;
				if(dist.lengthSquared() <= pointLight.range * pointLight.range)  //if camera is inside the point light sphere
				{
					deviceContext->RSSetState(rs_cullFront);
					deviceContext->OMSetDepthStencilState(dss_noDepthTest, 0x01);
				}
				else
				{
					deviceContext->RSSetState(rs_cullBack);
					deviceContext->OMSetDepthStencilState(dss_depthTest, 0x01);
				}

				sphereGeometry->render();
			}
		}
	}

	template<class iterater_t>
	void render_spotLights(iterater_t begin, iterater_t end)
	{
		dxDeviceContext deviceContext = RenderSystem::instance().deviceContext;

		coneGeometry->pre_render();

		for(uint i=0; i<tech_spotLight->pass_count(); i++)
		{
			EffectPass& pass = tech_spotLight->get_pass(i);

			pass.bind_states(shader_params);

			for(iterater_t it=begin; it!=end; ++it)
			{
				const SpotLight& spotLight = (*it);

				float3 lightPos;
				spotLight.position.transformCoord(cameraView, OUT &lightPos);
				sl_lightPosition->set_vector3(lightPos);

				float3 lightDir;
				spotLight.direction.transformNormal(cameraView, OUT &lightDir);
				sl_lightDirection->set_vector3(lightDir);

				float cos_inner_outer[2] = {cos(spotLight.innerConeAngle), cos(spotLight.outerConeAngle)};
				sl_light_cos_inner_outer->set_vector2(Vector2(cos_inner_outer));

				//var_light_falloff->set_float(spotLight.falloff);

				sl_lightColor->set_vector3(spotLight.color);

				// light_obj_wvp
				{
					float cone_base_radius = cone_base_distance * tan(spotLight.outerConeAngle);

					float3 base_z = spotLight.direction * cone_base_distance;
					float3 base_x = base_z.make_perpendicular_vector(); base_x.normalize(); base_x *= cone_base_radius;
					float3 base_y = float3::cross(base_z, base_x); base_y.normalize(); base_y *= cone_base_radius;

					float4x4 mat_world(
						base_x.x, base_x.y, base_x.z, 0.0f,
						base_y.x, base_y.y, base_y.z, 0.0f,
						base_z.x, base_z.y, base_z.z, 0.0f,
						spotLight.position.x, spotLight.position.y, spotLight.position.z, 1.0f);

					float4x4 mat_wvp = mat_world * cameraViewProj;
					sl_light_obj_wvp->set_matrix(mat_wvp);
				}
		
				cb_spotLight.update_to_device();

				// set cull state
				{
					float3 dist = cameraPos - spotLight.position;
					dist.normalize();

					float cosAngle = dot(spotLight.direction, dist);  //assuming spotLight.direction is normalized

					if(cosAngle > cos_inner_outer[1])
					{
						deviceContext->RSSetState(rs_cullFront);  //camera is inside the spot light cone
						deviceContext->OMSetDepthStencilState(dss_noDepthTest, 0x01);
					}
					else
					{
						deviceContext->RSSetState(rs_cullBack);
						deviceContext->OMSetDepthStencilState(dss_depthTest, 0x01);
					}
				}

				coneGeometry->render();
			}
		}
	}

	void releaseInputs()
	{
		LawnEngineUtil::releasePixelShaderResources();
	}
	
};
