#pragma once

class DeferredLightingRenderableEffectPlugin : public RenderableEffectPlugin
{
	int dl_light_acculumation_texture;
	int dl_gbuffer0_texture;

public:
	virtual const char* get_file_name() {
		return "deferred_lighting_use.fxh";
	}

	virtual void init() {
		EffectAutoParameterManager& autoParamMgr = EffectAutoParameterManager::instance();
 		dl_light_acculumation_texture = autoParamMgr.register_resource_parameter("dl_light_acculumation_texture");
		dl_gbuffer0_texture = autoParamMgr.register_resource_parameter("dl_gbuffer0_texture");
	}

	virtual void destroy() {
	}

	void set_inputs(dxShaderResourceView light_acc_tex, dxShaderResourceView gbuffer0) {
		EffectAutoParameterManager& autoParamMgr = EffectAutoParameterManager::instance();
		autoParamMgr.set_resource(dl_light_acculumation_texture, light_acc_tex);
		autoParamMgr.set_resource(dl_gbuffer0_texture, gbuffer0);
	}

	void realease_inputs() {
		LawnEngineUtil::releasePixelShaderResources();
	}

	virtual void pre_render_scene(SceneRenderPass& currentSceneRenderPass) {
		
	}

	virtual void post_render_scene(SceneRenderPass& currentSceneRenderPass) {
		
	}
};
