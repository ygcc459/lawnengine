#pragma once

#include "DeferredLighting_LightingPass.h"


class DeferredLightingRenderManager : public RenderPipeline
{
	ColorRenderTarget depth;
	ColorRenderTarget normal;

	ColorRenderTarget light;

	DepthStencilRenderTarget depthStencilRenderTarget;

	SceneRenderPass pass_scene;
	ToLinearDepthPass pass_toLinearDepth;
	DeferredLighting_LightingPass pass_lighting;

	GenerateNoShadowMapPlugin generateNoShadowMapPlugin;

public:
	DeferredLightingRenderManager(SceneManager* sceneMgr, LightCollection* lightCollection, CameraPtr camera, RenderView* renderView)
		:RenderPipeline(sceneMgr, lightCollection, camera, renderView)
	{
		RenderableEffect::addPlugin(&generateNoShadowMapPlugin);

		pass_scene.sceneMgr = sceneMgr;
		pass_scene.lightCollection = lightCollection;
		pass_scene.camera = camera;

		pass_toLinearDepth.init();
	}

	virtual ~DeferredLightingRenderManager()
	{
	}

	virtual void on_swap_chain_resized()
	{
		uint w = renderView->viewport.Width;
		uint h = renderView->viewport.Height;

		depth.init(w, h, DXGI_FORMAT_R32_FLOAT);
		normal.init(w, h, DXGI_FORMAT_R16G16B16A16_FLOAT);
		light.init(w, h, DXGI_FORMAT_R8G8B8A8_UNORM);

		depthStencilRenderTarget.init(w, h, DXGI_FORMAT_R32_TYPELESS, DXGI_FORMAT_D32_FLOAT, DXGI_FORMAT_R32_FLOAT);
		
		pass_scene.viewport = renderView->viewport;
	}

	virtual void render()
	{
		dxDevice device = RenderSystem::instance().device;

		device->ClearRenderTargetView(*renderView, float4(0.0f, 0.0f, 0.0f));
		device->ClearDepthStencilView(depthStencilRenderTarget, D3D11_CLEAR_DEPTH, 1.0f, 0);

		device->ClearRenderTargetView(depth, float4(1.0f, 0.0f, 0.0f));
		device->ClearRenderTargetView(normal, float4(0.0f, 0.0, 0.0f));

		LawnEngineUtil::setViewport(pass_scene.viewport);

		pass_scene.renderTechnique = RenderableEffect::RenderTechnique::GBuffer_min;
		pass_scene.setRenderTargetsGBufferMin(normal, depthStencilRenderTarget);
		pass_scene.render();
		pass_scene.releaseRenderTargets();

		pass_toLinearDepth.hardwareDepth_to_linearDepth(depthStencilRenderTarget, depth, *camera);

		device->ClearRenderTargetView(light, float4(0.0f, 0.0f, 0.0f));
		pass_lighting.setRenderTargets(light, depthStencilRenderTarget);
		pass_lighting.setInputs(depth, normal, pass_scene.camera, pass_scene.viewport);

		for(PointLightIterator it=lightCollection->point_light_begin(); it!=lightCollection->point_light_end(); ++it)
			this->pass_lighting.render_pointLight(*it);

		for(DirectionalLightIterator it=lightCollection->directional_light_begin(); it!=lightCollection->directional_light_end(); ++it)
			this->pass_lighting.render_directionalLight(*it);

		for(SpotLightIterator it=lightCollection->spot_light_begin(); it!=lightCollection->spot_light_end(); ++it)
			this->pass_lighting.render_spotLight(*it);

		pass_lighting.releaseInputs();

		device->ClearDepthStencilView(depthStencilRenderTarget, D3D11_CLEAR_DEPTH, 1.0f, 0);

		pass_scene.renderTechnique = RenderableEffect::RenderTechnique::DeferredLighting;
		pass_scene.setDeferredLightingLightTexture(light);
		pass_scene.setRenderTargetsDeferredLighting(*renderView, depthStencilRenderTarget);
		pass_scene.render();
		pass_scene.releaseRenderTargets();
		pass_scene.unsetDeferredLightingLightTexture();
	}
};
