#pragma once

class ForwardLightingRenderManager : public RenderPipeline
{
	DepthStencilRenderTarget depthStencilRenderTarget;

	SceneRenderPass forwardLightingPass;

	GenerateNoShadowMapPlugin generateNoShadowMapPlugin;

public:
	ForwardLightingRenderManager(SceneManager* sceneMgr, LightCollection* lightCollection, CameraPtr camera, RenderView* renderView)
		:RenderPipeline(sceneMgr, lightCollection, camera, renderView)
	{
		RenderableEffect::addPlugin(&generateNoShadowMapPlugin);

		// forwardLightingPass
		{
			forwardLightingPass.sceneMgr = sceneMgr;
			forwardLightingPass.lightCollection = lightCollection;
			forwardLightingPass.camera = camera;
			forwardLightingPass.renderTechnique = RenderableEffect::RenderTechnique::ForwardLighting;
		}
	}

	virtual ~ForwardLightingRenderManager()
	{
		
	}

	virtual void on_swap_chain_resized()
	{
		uint w = renderView->viewport.Width;
		uint h = renderView->viewport.Height;

		depthStencilRenderTarget.init(w, h, DXGI_FORMAT_D32_FLOAT, false);

		forwardLightingPass.viewport = renderView->viewport;
	}

	virtual void render()
	{
		dxDevice device = RenderSystem::instance().device;

		device->ClearRenderTargetView(*renderView, float4(0.0f, 0.0f ,1.0f));
		device->ClearDepthStencilView(depthStencilRenderTarget, D3D11_CLEAR_DEPTH, 1.0f, 0);

		LawnEngineUtil::setViewport(renderView->viewport);

		forwardLightingPass.setRenderTargetsForwardLighting(*renderView, depthStencilRenderTarget);
		forwardLightingPass.render();
	}
};
