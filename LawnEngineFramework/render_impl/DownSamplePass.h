#pragma once

// stores an array of textures for the down-sampling of the source
// the size(width & height) of these textures are: 1, 3, 9, 27, 81, 243, ...
class DownSamplePass : public RenderPass
{
public:
	ColorRenderTarget* textures;
	uint texture_count;
	UINT sourceWidth, sourceHeight;

	EffectPtr effect;
	EffectTechnique* technique;
	ShaderResourceVariable* var_sourceTexture;

	ShaderParamValues shader_params;

	DownSamplePass()
		:textures(NULL), texture_count(0)
	{

	}

	virtual ~DownSamplePass()
	{
		destroy();
	}

	void init()
	{
		struct on_effect_loaded {
			typedef void result_type;

			DownSamplePass* _this;

			on_effect_loaded(DownSamplePass* _this)
				:_this(_this)
			{}

			void operator () (EffectPtr effect)
			{
				_this->technique = effect->get_technique("down_sample_3x3_single_channel");
			}
		};

		effect = Effect::create("DownSamplePass", "DownSamplePass.fx", on_effect_loaded(this));

		shader_params.bind_to_shader(effect);
		this->var_sourceTexture = shader_params.get_resource_variable("sourceTexture");
	}

	void reset_source_texture(dxTexture2D src)
	{
		texture_count = 0;
		SAFE_DELETE_ARRAY(textures);

		//
		// create the texture chain
		//
		D3D11_TEXTURE2D_DESC desc;
		src->GetDesc(&desc);

		D3D11_RENDER_TARGET_VIEW_DESC rtv_desc;
		{
			rtv_desc.Format = desc.Format;
			rtv_desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
			rtv_desc.Texture2D.MipSlice = 0;
		}

		D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc;
		{
			srv_desc.Format = desc.Format;
			srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
			srv_desc.Texture2D.MipLevels = 1;
			srv_desc.Texture2D.MostDetailedMip = 0;
		}

		sourceWidth = desc.Width;
		sourceHeight = desc.Height;

		float src_size = float(std::min(desc.Width, desc.Height));
		int count = int(floor(log(src_size) / log(3.0f)));
		if(pow(3.0f, count)==src_size)
			count--;

		texture_count = (uint)count;
		textures = new ColorRenderTarget[texture_count];

		for(int i=0; i<(int)texture_count; i++)
		{
			desc.Width = desc.Height = uint(pow(3.0f, i));

			textures[i].init(desc, rtv_desc, &srv_desc);
		}
	}

	void destroy()
	{
		SAFE_DELETE_ARRAY(textures);
		texture_count = 0;
	}

	uint down_sampled_texture_count() {
		return texture_count;
	}

	ColorRenderTarget& get_down_sampled_texture(uint i = 0)
	{
		return textures[i];
	}

	//down-sample the given source texture, and return the result texture
	ColorRenderTarget& do_down_sample(dxShaderResourceView source)
	{
		EffectPass& pass = technique->get_pass(0);

		LawnEngineUtil::setRenderTargets(textures[texture_count-1], dxDepthStencilView(NULL));
		LawnEngineUtil::setViewport(0, 0, textures[texture_count-1].w, textures[texture_count-1].h);

		pass.bind_states(shader_params);
		LawnEngineUtil::full_screen_quad_geometry().pre_render();
				
		var_sourceTexture->set_resource_view(source);
		pass.bind_states(shader_params, BS_BindPSResources);
		LawnEngineUtil::full_screen_quad_geometry().render();

		for(uint i=texture_count-1; i>=1; i--)
		{
			LawnEngineUtil::setRenderTargets(textures[i-1], dxDepthStencilView(NULL));
			LawnEngineUtil::setViewport(0, 0, textures[i-1].w, textures[i-1].h);
			
			var_sourceTexture->set_resource_view(textures[i]);
			pass.bind_states(shader_params, BS_BindPSResources);
			LawnEngineUtil::full_screen_quad_geometry().render();
		}

		LawnEngineUtil::releaseRenderTargets();
		LawnEngineUtil::releasePixelShaderResources(1);

		return textures[0];
	}
	
};
