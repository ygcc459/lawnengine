#pragma once

class GenerateNoShadowMapPlugin : public RenderPassPlugin
{
public:
	virtual const char* get_file_name() {
		return "NoShadow.fxh";
	}
	
	virtual void on_effect_compiled(dxEffect effect) {

	}
};