#pragma once

//
// to draw auxlines, just create an AuxGeometryContainer, and add lines to it, these added lines will be automatically drawed
//

struct AuxLine
{
public:
	float3 v1;
	const float3* v1_ptr;

	float3 v2;
	const float3* v2_ptr;

	bool is_v2_direction;
	float v2_direction_length_scale, v2_direction_length_bias;

	float4 line_color;

public:
	AuxLine()
		:v1_ptr(), v2_ptr(), line_color(0,0,0,1), is_v2_direction(false)
	{
	}

	AuxLine& from_fixed_point(float pt[3]) {
		this->v1 = float3(pt);
		this->v1_ptr = NULL;
		return *this;
	}
	AuxLine& from_fixed_point(float pt_x, float pt_y, float pt_z) {
		this->v1 = float3(pt_x, pt_y, pt_z);
		this->v1_ptr = NULL;
		return *this;
	}
	AuxLine& from_dynamic_point(const float3* pt_ptr) {
		this->v1_ptr = pt_ptr;
		return *this;
	}

	AuxLine& to_fixed_point(float pt[3]) {
		this->v2 = float3(pt);
		this->v2_ptr = NULL;
		this->is_v2_direction = false;
		return *this;
	}
	AuxLine& to_fixed_point(float pt_x, float pt_y, float pt_z) {
		this->v2 = float3(pt_x, pt_y, pt_z);
		this->v2_ptr = NULL;
		this->is_v2_direction = false;
		return *this;
	}
	AuxLine& to_dynamic_point(const float3* pt_ptr) {
		this->v2_ptr = pt_ptr;
		this->is_v2_direction = false;
		return *this;
	}

	AuxLine& towards_fixed_direction(float dir[3], float length_scale=1.0f, float length_bias=0.0f) {
		this->v2 = float3(dir);
		this->v2_ptr = NULL;
		this->is_v2_direction = true;
		this->v2_direction_length_scale = length_scale;
		this->v2_direction_length_bias = length_bias;
		return *this;
	}
	AuxLine& towards_fixed_direction(float dir_x, float dir_y, float dir_z, float length_scale=1.0f, float length_bias=0.0f) {
		this->v2 = float3(dir_x, dir_y, dir_z);
		this->v2_ptr = NULL;
		this->is_v2_direction = true;
		this->v2_direction_length_scale = length_scale;
		this->v2_direction_length_bias = length_bias;
		return *this;
	}
	AuxLine& towards_dynamic_direction(const float3* dir_ptr, float length_scale=1.0f, float length_bias=0.0f) {
		this->v2_ptr = dir_ptr;
		this->is_v2_direction = true;
		this->v2_direction_length_scale = length_scale;
		this->v2_direction_length_bias = length_bias;
		return *this;
	}

	AuxLine& color(const float4& color) {
		this->line_color = color;
		return *this;
	}
	AuxLine& color(const float3& color) {
		this->line_color.x = color.x;
		this->line_color.y = color.y;
		this->line_color.z = color.z;
		this->line_color.w = 1.0f;
		return *this;
	}
	AuxLine& color(float r, float g, float b, float a=1.0f) {
		this->line_color = float4(r, g, b, a);
		return *this;
	}
};

class AuxGeometryContainer
{
protected:
	std::vector<AuxLine> lines;

	AuxGeometryContainer()
		:visible(true)
	{}

	friend class AuxGeometryDrawPass;

public:
	typedef std::vector<AuxLine>::iterator iterator;

	iterator add(const AuxLine& line) {
		lines.push_back(line);
		return lines.end() - 1;
	}
	
	void add(const AABB& aabb, const float4& color) {
		lines.push_back(AuxLine().from_fixed_point(aabb.xmin, aabb.ymin, aabb.zmin).to_fixed_point(aabb.xmax, aabb.ymin, aabb.zmin).color(color));
		lines.push_back(AuxLine().from_fixed_point(aabb.xmax, aabb.ymin, aabb.zmin).to_fixed_point(aabb.xmax, aabb.ymin, aabb.zmax).color(color));
		lines.push_back(AuxLine().from_fixed_point(aabb.xmax, aabb.ymin, aabb.zmax).to_fixed_point(aabb.xmin, aabb.ymin, aabb.zmax).color(color));
		lines.push_back(AuxLine().from_fixed_point(aabb.xmin, aabb.ymin, aabb.zmax).to_fixed_point(aabb.xmin, aabb.ymin, aabb.zmin).color(color));

		lines.push_back(AuxLine().from_fixed_point(aabb.xmin, aabb.ymax, aabb.zmin).to_fixed_point(aabb.xmax, aabb.ymax, aabb.zmin).color(color));
		lines.push_back(AuxLine().from_fixed_point(aabb.xmax, aabb.ymax, aabb.zmin).to_fixed_point(aabb.xmax, aabb.ymax, aabb.zmax).color(color));
		lines.push_back(AuxLine().from_fixed_point(aabb.xmax, aabb.ymax, aabb.zmax).to_fixed_point(aabb.xmin, aabb.ymax, aabb.zmax).color(color));
		lines.push_back(AuxLine().from_fixed_point(aabb.xmin, aabb.ymax, aabb.zmax).to_fixed_point(aabb.xmin, aabb.ymax, aabb.zmin).color(color));

		lines.push_back(AuxLine().from_fixed_point(aabb.xmin, aabb.ymin, aabb.zmin).to_fixed_point(aabb.xmin, aabb.ymax, aabb.zmin).color(color));
		lines.push_back(AuxLine().from_fixed_point(aabb.xmax, aabb.ymin, aabb.zmin).to_fixed_point(aabb.xmax, aabb.ymax, aabb.zmin).color(color));
		lines.push_back(AuxLine().from_fixed_point(aabb.xmax, aabb.ymin, aabb.zmax).to_fixed_point(aabb.xmax, aabb.ymax, aabb.zmax).color(color));
		lines.push_back(AuxLine().from_fixed_point(aabb.xmin, aabb.ymin, aabb.zmax).to_fixed_point(aabb.xmin, aabb.ymax, aabb.zmax).color(color));
	}

	void add(const AABB& aabb, const float4x4& transform_matrix, const float4& color) {
		float3 bottom_00(aabb.xmin, aabb.ymin, aabb.zmin); bottom_00.transformCoord(transform_matrix);
		float3 bottom_01(aabb.xmin, aabb.ymin, aabb.zmax); bottom_01.transformCoord(transform_matrix);
		float3 bottom_10(aabb.xmax, aabb.ymin, aabb.zmin); bottom_10.transformCoord(transform_matrix);
		float3 bottom_11(aabb.xmax, aabb.ymin, aabb.zmax); bottom_11.transformCoord(transform_matrix);

		float3 top_00(aabb.xmin, aabb.ymax, aabb.zmin); top_00.transformCoord(transform_matrix);
		float3 top_01(aabb.xmin, aabb.ymax, aabb.zmax); top_01.transformCoord(transform_matrix);
		float3 top_10(aabb.xmax, aabb.ymax, aabb.zmin); top_10.transformCoord(transform_matrix);
		float3 top_11(aabb.xmax, aabb.ymax, aabb.zmax); top_11.transformCoord(transform_matrix);

		lines.push_back(AuxLine().from_fixed_point(bottom_00).to_fixed_point(bottom_01).color(color));
		lines.push_back(AuxLine().from_fixed_point(bottom_01).to_fixed_point(bottom_11).color(color));
		lines.push_back(AuxLine().from_fixed_point(bottom_11).to_fixed_point(bottom_10).color(color));
		lines.push_back(AuxLine().from_fixed_point(bottom_10).to_fixed_point(bottom_00).color(color));
		
		lines.push_back(AuxLine().from_fixed_point(top_00).to_fixed_point(top_01).color(color));
		lines.push_back(AuxLine().from_fixed_point(top_01).to_fixed_point(top_11).color(color));
		lines.push_back(AuxLine().from_fixed_point(top_11).to_fixed_point(top_10).color(color));
		lines.push_back(AuxLine().from_fixed_point(top_10).to_fixed_point(top_00).color(color));
		
		lines.push_back(AuxLine().from_fixed_point(bottom_00).to_fixed_point(top_00).color(color));
		lines.push_back(AuxLine().from_fixed_point(bottom_01).to_fixed_point(top_01).color(color));
		lines.push_back(AuxLine().from_fixed_point(bottom_11).to_fixed_point(top_11).color(color));
		lines.push_back(AuxLine().from_fixed_point(bottom_10).to_fixed_point(top_10).color(color));
	}

	void remove(iterator it) {
		lines.erase(it);
	}

	void clear() {
		lines.clear();
	}

	bool visible;
};

//
// singleton class
//
class AuxGeometryDrawPass : public RenderPass
{
	ShaderVariable* mat_vp;
	ShaderVariable* line_pt1;
	ShaderVariable* line_pt2;
	ShaderVariable* line_color;
	int cb_per_line;
	int cb_per_scene;

	ShaderParamValues shader_params;

	EffectTechnique* tech_line;

	std::vector<AuxGeometryContainer*> aux_containers;

	bool enabled;

public:
	AuxGeometryDrawPass()
		:enabled(false)
	{
		
	}

	virtual ~AuxGeometryDrawPass(void)
	{
		shutdown();
	}

	void startup()
	{
		struct on_effect_loaded {

			AuxGeometryDrawPass* _this;
			typedef void result_type;

			on_effect_loaded(AuxGeometryDrawPass* _this)
				:_this(_this)
			{}

			void operator () (EffectPtr effect)
			{
				_this->tech_line = effect->get_technique("tech_line");
			}
		};

		effect = Effect::create("AuxGeometryDrawPass", "AuxGeometryDrawPass.fx", on_effect_loaded(this));

		shader_params.bind_to_shader(effect);
		this->mat_vp = shader_params.get_variable("mat_vp");
		this->line_pt1 = shader_params.get_variable("line_pt1");
		this->line_pt2 = shader_params.get_variable("line_pt2");
		this->line_color = shader_params.get_variable("line_color");

		this->cb_per_line = shader_params.get_cbuffer("cb_per_line");
		this->cb_per_scene = shader_params.get_cbuffer("cb_per_scene");

		enabled = true;
	}

	void shutdown()
	{
		effect.reset();
		shader_params.unbind_shader();
		destroy_all_containers();
	}

	static AuxGeometryDrawPass& instance()
	{
		static AuxGeometryDrawPass t;
		return t;
	}

	static AuxGeometryContainer* default_container()
	{
		static AuxGeometryContainer* c = AuxGeometryDrawPass::instance().create_container();
		return c;
	}

	AuxGeometryContainer* create_container()
	{
		AuxGeometryContainer* p = new AuxGeometryContainer();
		aux_containers.push_back(p);
		return p;
	}

	bool destroy_container(AuxGeometryContainer* p)
	{
		for(auto it=aux_containers.begin(); it!=aux_containers.end(); ++it) {
			if((*it)==p) {
				aux_containers.erase(it);
				delete p;
				return true;
			}
		}

		return false;
	}

	void destroy_all_containers()
	{
		for(auto it=aux_containers.begin(); it!=aux_containers.end(); ++it) {
			delete (*it);
		}

		aux_containers.clear();
	}

	void begin_draw(const float4x4& camera_viewproj, dxRenderTargetView render_target, dxDepthStencilView depth_stencil)
	{
		if(!enabled)
			return;

		LawnEngineUtil::setRenderTargets(render_target, depth_stencil);

		mat_vp->set_matrix(camera_viewproj);
		cb_per_scene.update_to_device();

		RenderSystem::instance().deviceContext->IASetInputLayout(NULL);
		RenderSystem::instance().deviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINELIST);

		tech_line->get_pass(0).bind_states(shader_params);
	}

	void draw_all()
	{
		if(!enabled)
			return;

		for(auto it = aux_containers.begin(); it!=aux_containers.end(); ++it)
		{
			const AuxGeometryContainer* container = (*it);

			for(auto it_line = container->lines.begin(); it_line!=container->lines.end(); ++it_line)
			{
				const AuxLine& line = (*it_line);

				draw_line(line);
			}
		}
	}

	void draw_line(const AuxLine& line)
	{
		if(!enabled)
			return;

		float3 v1, v2;

		if(line.v1_ptr!=NULL)
			v1 = *line.v1_ptr;
		else
			v1 = line.v1;

		if(line.v2_ptr!=NULL)
			v2 = *line.v2_ptr;
		else
			v2 = line.v2;

		if(line.is_v2_direction)
		{
			float3 dir = v2;

			float old_length = dir.length();
			float new_length = old_length * line.v2_direction_length_scale + line.v2_direction_length_bias;
			dir *= (new_length / old_length);

			v2 = v1 + dir;
		}

		line_pt1->set_vector3(v1);
		line_pt2->set_vector3(v2);

		line_color->set_vector4(line.line_color);

		cb_per_line.update_to_device();

		RenderSystem::instance().deviceContext->Draw(2, 0);
	}
	
	void end_draw()
	{
		if(!enabled)
			return;

		LawnEngineUtil::releaseRenderTargets(1);
	}
	
	bool get_enabled() const { return enabled; }
	void set_enabled(bool val) { enabled = val; }
};
