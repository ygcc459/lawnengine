#pragma once


class DrawSkyPass : public RenderPass
{
public:	
	GeometryPtr geometry;

	dxEffect effect;
	EffectTechnique technique;
	EffectResourceVariable* var_sky_texture;
	EffectVariable* var_matrix_wvp;

	TextureCubePtr sky_texture;

	float4x4 world_matrix;
	

	DrawSkyPass()
		:effect(NULL), world_matrix(float4x4::identity())
	{

	}

	virtual ~DrawSkyPass()
	{
		destroy();
	}

	void init(const char* sky_texture_file_name, const float3& centerPos, float distance)
	{
		//
		// create the effect
		//
		effect = LawnEngineUtil::compile_effect("DrawSkyPass.fx");

		technique.create_from_dx_effect(effect, "DrawSkyBox");

		var_sky_texture = effect->GetVariableByName("sky_texture")->AsShaderResource();
		var_matrix_wvp = effect->GetVariableByName("matrix_wvp")->AsMatrix();

		//
		// create the geomtry
		//
		geometry = boost::static_pointer_cast<Mesh>(create_skybox());

		//
		// load the texture
		//
		sky_texture = TextureCube::create_from_file(sky_texture_file_name);

		//
		// set world matrix
		//
		world_matrix = float4x4::createScaling(distance, distance, distance) * float4x4::createTranslation(centerPos);
	}

	void destroy()
	{
		SAFE_RELEASE(effect);
	}

	void render_skybox(Camera& camera, const Viewport& viewport)
	{
		RenderSystem::instance().device->RSSetViewports(1, &viewport);

		var_sky_texture->SetResource(sky_texture->pD3DShaderResView);

		float4x4 view, proj;
		camera.getViewMatrix(view);
		camera.getProjectionMatrix(proj, viewport);

		float4x4 matrix_wvp = world_matrix * view * proj;
		var_matrix_wvp->set_matrix_row_majored((float*)&matrix_wvp);

		technique->render(*geometry);
	}

public:
	static std::shared_ptr<Mesh> create_skybox()
	{
		const char* name = "DrawSkyPass::skybox";

		auto _ptr = boost::dynamic_pointer_cast<Mesh>(Resource<Mesh>::get_loaded_resource(name, false));
		if(_ptr)
			return _ptr;

		//
		//     7 *--------* 6
		//      /|       /|
		//     / |    5 / |
		//  4 *--+-----*  |
		//    |  *-----|--* 2
		//    | / 3    | /
		//    |/       |/
		//  0 *--------* 1
		//

		float3 pos_buffer[8] = {
			float3(-1.0f,-1.0f,-1.0f),
			float3( 1.0f,-1.0f,-1.0f),
			float3( 1.0f,-1.0f, 1.0f),
			float3(-1.0f,-1.0f, 1.0f),
			float3(-1.0f, 1.0f,-1.0f),
			float3( 1.0f, 1.0f,-1.0f),
			float3( 1.0f, 1.0f, 1.0f),
			float3(-1.0f, 1.0f, 1.0f),
		};

		short index_buffer[12*3] = {
			0,1,5, 0,4,5,
			0,1,2, 0,3,2,
			0,4,7, 0,3,7,
			6,5,4, 6,7,4,
			6,2,1, 6,5,1,
			6,2,3, 6,7,3,
		};

		std::vector<VertexDataComponentDesc> vdcd;
		vdcd.push_back(VertexDataComponentDesc(IE_POSITION, sizeof(float3), (byte*)pos_buffer));

		auto ptr = Mesh::create_by_triangleList(8, 12, index_buffer, vdcd);

		Resource<Mesh>::add_loaded_resource(name, boost::static_pointer_cast<Mesh>(ptr), false);

		return ptr;
	}
};
