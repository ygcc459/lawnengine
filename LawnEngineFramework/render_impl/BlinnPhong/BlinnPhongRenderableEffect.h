#pragma once


class BlinnPhongRenderableEffect : public RenderableEffect
{
	//LE_RENDERABLEEFFECT("blinn_phong", BlinnPhongRenderableEffect, "BlinnPhongRenderableEffect.fx")

public:
	BlinnPhongRenderableEffect()
	{
	}

	virtual ~BlinnPhongRenderableEffect()
	{
	}

	virtual void on_init()
	{
		u_mx_wvp = effect->get_variable("u_mx_wvp");
		u_mx_wv = effect->get_variable("u_mx_wv");
		u_shiness = effect->get_variable("u_shiness");
		
		u_mx_wvp2 = effect->get_variable("u_mx_wvp2");
		u_mx_wv2 = effect->get_variable("u_mx_wv2");
		u_material_diffuse = effect->get_variable("u_material_diffuse");
		u_material_specular = effect->get_variable("u_material_specular");
		
		u_albedo_texture = effect->get_resource_variable("u_albedo_texture");
		
		cb_gbuffer = effect->get_cbuffer("cb_gbuffer");
		cb_deferred_lighting = effect->get_cbuffer("cb_deferred_lighting");

		emptyTexture = Texture2D::create_from_file("emptyTexture.png");
	}

	virtual void render_objects(Renderable* renderables, uint renderable_count, SceneRenderPass& sceneRenderPass)
	{
		EffectTechnique* technique = effect->get_technique(sceneRenderPass.getRenderTechnique());
		if(technique==NULL) {
			vcpp::log::err("technique '%s' not found for RenderableEffect '%s'", sceneRenderPass.getRenderTechnique().c_str(), this->getEffectFileName());
			return;
		}

		float4x4 matView, matProj;
		float4x4 mat_view_proj;
		{
			Camera& camera = *sceneRenderPass.getCamera();
			const Viewport& viewport = sceneRenderPass.getViewport();
			
			camera.getViewMatrix(matView);
			camera.getProjectionMatrix(matProj, viewport);
			mat_view_proj = matView * matProj;
		}

		if(technique->name=="gbuffer")
			render_gbuffer(technique, renderables, renderable_count, sceneRenderPass, mat_view_proj, matView);
		else if(technique->name=="deferred_lighting")
			render_deferred_lighting(technique, renderables, renderable_count, sceneRenderPass, mat_view_proj, matView);
	}

protected:
	void render_gbuffer(EffectTechnique* technique, Renderable* renderables, uint renderable_count, 
		SceneRenderPass& sceneRenderPass, const float4x4& mat_view_proj, const float4x4& mat_view)
	{
		//
		// pre render scene
		//
		{
		}

		for(uint i=0; i<technique->pass_count(); i++)
		{
			EffectPass& pass = technique->get_pass(i);

			pass.bind_states();

			for(uint i=0; i<renderable_count; i++)
			{
				Renderable& obj = renderables[i];

				BasicMaterial* material = dynamic_cast<BasicMaterial*>(obj.material);
				
				//
				// pre-render object
				//
				{
					float4x4 mat_wvp = obj.world_matrix * mat_view_proj;
					u_mx_wvp->set_matrix_row_majored(mat_wvp);
					
					float4x4 mat_wv = obj.world_matrix * mat_view;
					u_mx_wv->set_matrix_row_majored(mat_wv);

					u_shiness->set_float(material->roughness);

					cb_gbuffer->update_to_device();
				}

				obj.mesh->pre_render();
				obj.mesh->render();
			}
		}

		//
		// post render scene
		//
		{
		}
	}

	void render_deferred_lighting(EffectTechnique* technique, Renderable* renderables, uint renderable_count, 
		SceneRenderPass& sceneRenderPass, const float4x4& mat_view_proj, const float4x4& mat_view)
	{
		//
		// pre render scene
		//
		{
		}

		for(uint i=0; i<technique->pass_count(); i++)
		{
			EffectPass& pass = technique->get_pass(i);

			pass.bind_states();

			for(uint i=0; i<renderable_count; i++)
			{
				Renderable& obj = renderables[i];

				BasicMaterial* material = dynamic_cast<BasicMaterial*>(obj.material);

				//
				// pre-render object
				//
				{
					float4x4 mat_wvp = obj.world_matrix * mat_view_proj;
					u_mx_wvp2->set_matrix_row_majored(mat_wvp);
					
					float4x4 mat_wv = obj.world_matrix * mat_view;
					u_mx_wv2->set_matrix_row_majored(mat_wv);

					u_material_diffuse->set_float3((float*)&material->diffuse);
					u_material_specular->set_float3(material->specular);

					cb_deferred_lighting->update_to_device();
					
					if(material->albedo_texture)
						u_albedo_texture->set_resource_view(material->albedo_texture->pD3DShaderResView);
					else
						u_albedo_texture->set_resource_view(emptyTexture->pD3DShaderResView);

					pass.rebind_resource_view(u_albedo_texture);
				}				

				obj.mesh->pre_render();
				obj.mesh->render();
			}
		}

		//
		// post render scene
		//
		{
			LawnEngineUtil::releasePixelShaderResources();
		}
	}

protected:
	EffectVariable* u_mx_wvp;
	EffectVariable* u_mx_wv;
	EffectVariable* u_shiness;

	EffectVariable* u_mx_wvp2;
	EffectVariable* u_mx_wv2;
	EffectVariable* u_material_diffuse;
	EffectVariable* u_material_specular;

	EffectResourceVariable* u_albedo_texture;

	EffectConstantBuffer* cb_gbuffer;
	EffectConstantBuffer* cb_deferred_lighting;

	Texture2DPtr emptyTexture;

};
