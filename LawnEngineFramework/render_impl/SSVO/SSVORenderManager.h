#pragma once

#include "../../engine/ToLinearDepthPass.h"
#include "../../engine/BilateralBlur.h"
#include "../DeferredShading/DeferredShading_LightingPass.h"
#include "SSVOPass.h"


class SSVORenderManager : public RenderPipeline
{
	DepthStencilRenderTarget depthStencilRenderTarget;

	ColorRenderTarget depth;
	ColorRenderTarget normal;
	ColorRenderTarget albedo;

	ColorRenderTarget linearDepth;

	ColorRenderTarget occlusionBuffer_temp1;
	ColorRenderTarget occlusionBuffer_temp2;

	SceneRenderPass scenePass;

	ToLinearDepthPass toLinearDepthPass;

	SSVOPass ssvoPass;

	BilateralBlurPass bilateralBlurPass;

	DeferredShading_LightingPass lightingPass;

	GenerateNoShadowMapPlugin generateNoShadowMapPlugin;

public:
	SSVORenderManager(SceneManager* sceneMgr, LightCollection* lightCollection, CameraPtr camera, RenderView* renderView)
		:RenderPipeline(sceneMgr, lightCollection, camera, renderView)
	{
		RenderableEffect::addPlugin(&generateNoShadowMapPlugin);

		// scenePass
		{
			scenePass.sceneMgr = sceneMgr;
			scenePass.lightCollection = lightCollection;
			scenePass.camera = camera;
			scenePass.renderTechnique = RenderableEffect::RenderTechnique::GBuffer;
		}

		toLinearDepthPass.init();

		bilateralBlurPass.init();
	}

	virtual ~SSVORenderManager()
	{
	}

	virtual void on_swap_chain_resized()
	{
		uint w = renderView->viewport.Width;
		uint h = renderView->viewport.Height;

		this->depth.init(w, h, DXGI_FORMAT_R32_FLOAT);
		this->normal.init(w, h, DXGI_FORMAT_R16G16_FLOAT);
		this->albedo.init(w, h, DXGI_FORMAT_R8G8B8A8_UNORM);
		this->depthStencilRenderTarget.init(w, h, DXGI_FORMAT_R32_TYPELESS, DXGI_FORMAT_D32_FLOAT, DXGI_FORMAT_R32_FLOAT);

		linearDepth.init(w, h, DXGI_FORMAT_R32_FLOAT);

		occlusionBuffer_temp1.init(w, h, DXGI_FORMAT_R8_UNORM);
		occlusionBuffer_temp2.init(w, h, DXGI_FORMAT_R8_UNORM);

		scenePass.viewport = renderView->viewport;
	}

	virtual void render()
	{
		dxDevice device = RenderSystem::instance().device;

		device->ClearDepthStencilView(depthStencilRenderTarget, D3D11_CLEAR_DEPTH, 1.0f, 0);

		device->ClearRenderTargetView(depth, float4(1.0f, 0.0f, 0.0f));
		device->ClearRenderTargetView(normal, float4(0.0f, 0.0, 0.0f));
		device->ClearRenderTargetView(albedo, float4(0.0f, 0.0f, 0.0f, 1.0f));

		LawnEngineUtil::setViewport(scenePass.viewport);

		scenePass.setRenderTargetsGBuffer(depth, normal, albedo, depthStencilRenderTarget);
		scenePass.render();
		scenePass.releaseRenderTargets();

		toLinearDepthPass.hardwareDepth_to_linearDepth(depthStencilRenderTarget, linearDepth, *scenePass.camera.get());

		float4x4 proj, invProj;
		scenePass.camera->getProjectionMatrix(proj, scenePass.viewport);
		proj.inverse(&invProj);

		device->ClearRenderTargetView(*renderView, float4(0.0f, 0.0f, 0.0f));

		lightingPass.setRenderTargets(*renderView, depthStencilRenderTarget);
		lightingPass.setInputs(depth, normal, albedo, scenePass.camera, scenePass.viewport);
		{
			for(PointLightIterator it=lightCollection->point_light_begin(); it!=lightCollection->point_light_end(); ++it)
				lightingPass.render_pointLight(*it);

			for(DirectionalLightIterator it=lightCollection->directional_light_begin(); it!=lightCollection->directional_light_end(); ++it)
				lightingPass.render_directionalLight(*it);
		}
		lightingPass.releaseInputs();
		lightingPass.releaseRenderTargets();

		ssvoPass.setRenderTargets(occlusionBuffer_temp1);
		ssvoPass.setInputs(linearDepth, normal, proj, invProj, 0.5f);
		ssvoPass.render();
		ssvoPass.releaseInputs();
		ssvoPass.releaseRenderTargets();

		bilateralBlurPass.setParams(5, 1.0f);
		//bilateralBlurPass.blur_x(occlusionBuffer_temp1, linearDepth, occlusionBuffer_temp2, renderView->viewport.Width, renderView->viewport.Height);
		//bilateralBlurPass.blur_y(occlusionBuffer_temp2, linearDepth, *renderView, renderView->viewport.Width, renderView->viewport.Height);

		bilateralBlurPass.blur_x(occlusionBuffer, linearDepth, *renderView, renderView->viewport.Width, renderView->viewport.Height);
	}
};
