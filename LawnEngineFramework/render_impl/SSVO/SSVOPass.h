#pragma once


class SSVOPass : public RenderPass
{
	EffectPtr effect;
	EffectTechnique* tech_ssvo;

	ShaderParamValues shader_params;

	ShaderResourceVariable* var_depth;
	ShaderResourceVariable* var_normal;
	ShaderVariable* var_invProj;
	ShaderVariable* var_proj;
	ShaderVariable* var_radius;
	ShaderVariable* var_ambientAmount;

	ShaderVariable* var_epsilon;
	ShaderVariable* var_depth_bias;
	ShaderVariable* var_reject_dist_sqr;
	ShaderVariable* var_intensity_scale;

public:
	SSVOPass()
	{
		struct on_effect_loaded {
			typedef void result_type;
			void operator () (SSVOPass* _this, EffectPtr effect)
			{
				_this->tech_ssvo = effect->get_technique("tech_ssvo");

				ShaderParamValues& shader_params = _this->shader_params;

				shader_params.bind_to_shader(effect);
				_this->var_depth = shader_params.get_resource_variable("depthTexture");
				_this->var_normal = shader_params.get_resource_variable("normalTexture");
				_this->var_invProj = shader_params.get_variable("invProj");
				_this->var_proj = shader_params.get_variable("proj");
				_this->var_radius = shader_params.get_variable("radius");
				_this->var_ambientAmount = shader_params.get_variable("ambientAmount");
				_this->var_epsilon = shader_params.get_variable("epsilon");
				_this->var_depth_bias = shader_params.get_variable("depth_bias");
				_this->var_reject_dist_sqr = shader_params.get_variable("reject_dist_sqr");
				_this->var_intensity_scale = shader_params.get_variable("intensity_scale");
			}
		};

		effect = Effect::create("SSVOPass", "SSVOPass.fx", Effect::EffectLoadedHandler(BOOST_BIND(on_effect_loaded(), this, _1)));
	}

	virtual ~SSVOPass(void)
	{
		
	}

	void setRenderTargets(dxRenderTargetView ao) {
		LawnEngineUtil::setRenderTargets(ao, (dxDepthStencilView)NULL);
	}

	void setParameters(float ambientAmount, float radius=0.5f, float intensity_scale=5.0f, float epsilon=0.0001f, float depth_bias=0.002f, float reject_dist_sqr=1.0f)
	{
		var_radius->set_float(radius);
		var_ambientAmount->set_float(ambientAmount);
		var_epsilon->set_float(epsilon);
		var_depth_bias->set_float(depth_bias);
		var_reject_dist_sqr->set_float(reject_dist_sqr);
		var_intensity_scale->set_float(intensity_scale);
	}
	
	void setInputs(dxShaderResourceView depth_texture, dxShaderResourceView normal_texture, const float4x4& cameraProjection, const float4x4& cameraInverseProjection)
	{
		var_depth->set_resource_view(depth_texture);
		var_normal->set_resource_view(normal_texture);
		var_invProj->set_matrix(cameraInverseProjection);
		var_proj->set_matrix(cameraProjection);
	}

	void render()
	{
		tech_ssvo->render(LawnEngineUtil::full_screen_quad_geometry(), shader_params);
	}

	void releaseInputs()
	{
		LawnEngineUtil::releasePixelShaderResources(2);
		LawnEngineUtil::releaseRenderTargets();
	}

};
