#pragma once


class TextureOperationPass : public RenderPass
{
	ShaderResourceVariable* var_source_texture;

	EffectTechnique* tech_add;
	EffectTechnique* tech_scatter_add;

	EffectPtr effect;

public:
	TextureOperationPass(void)
	{
		struct on_effect_loaded
		{
			typedef void result_type;

			TextureOperationPass* _this;

			on_effect_loaded(TextureOperationPass* _this)
				:_this(_this)
			{}

			void operator () (EffectPtr effect)
			{
				_this->tech_add = effect->get_technique("tech_add");

				ShaderParamValues& shader_params = _this->shader_params;

				shader_params.bind_to_shader(effect);
				_this->tech_scatter_add = shader_params.get_technique("tech_scatter_add");
				_this->var_source_texture = shader_params.get_resource_variable("source_texture");
			}
		};

		effect = Effect::create("TextureOperationPass", "TextureOperationPass.fx", on_effect_loaded(this));
	}

	virtual ~TextureOperationPass(void)
	{
	}

	void add_source_to_target(dxShaderResourceView src, dxRenderTargetView target)
	{
		var_source_texture->set_resource_view(src);
		LawnEngineUtil::setRenderTargets(target, (dxDepthStencilView)NULL);
		
		tech_add->render(LawnEngineUtil::full_screen_quad_geometry(), shader_params);

		LawnEngineUtil::releaseRenderTargets();
		LawnEngineUtil::releasePixelShaderResources(1);
	}

	void add_source_to_target_channelScattered(dxShaderResourceView src, dxRenderTargetView target)
	{
		var_source_texture->set_resource_view(src);
		LawnEngineUtil::setRenderTargets(target, (dxDepthStencilView)NULL);

		tech_scatter_add->render(LawnEngineUtil::full_screen_quad_geometry(), shader_params);

		LawnEngineUtil::releaseRenderTargets();
		LawnEngineUtil::releasePixelShaderResources(1);
	}
	
};
