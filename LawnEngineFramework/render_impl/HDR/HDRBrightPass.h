#pragma once


class HDRBrightPass : public RenderPass
{
public:
	EffectPtr effect;
	EffectTechnique* technique;

	ShaderResourceVariable* var_hdr_image;
	ShaderResourceVariable* var_greyed_hdr_image_1x1;
	ShaderVariable* var_bright_threshold;
	ShaderVariable* var_middle_gray;
	ShaderVariable* var_lum_white;

	ShaderParamValues shader_params;

	HDRBrightPass()
	{

	}

	virtual ~HDRBrightPass()
	{
		destroy();
	}

	void init()
	{
		struct on_effect_loaded
		{
			typedef void result_type;

			HDRBrightPass* _this;

			on_effect_loaded(HDRBrightPass* _this)
				:_this(_this)
			{}

			void operator () (EffectPtr effect)
			{
				_this->technique = effect->get_technique("HDRBrightPass");

				ShaderParamValues& shader_params = _this->shader_params;

				shader_params.bind_to_shader(effect);
				_this->var_hdr_image = shader_params.get_resource_variable("hdr_image");
				_this->var_greyed_hdr_image_1x1 = shader_params.get_resource_variable("greyed_hdr_image_1x1");
				_this->var_bright_threshold = shader_params.get_variable("bright_threshold");
				_this->var_middle_gray = shader_params.get_variable("middle_gray");
				_this->var_lum_white = shader_params.get_variable("lum_white");
			}
		};

		effect = Effect::create("HDRBrightPass", "HDRBrightPass.fx", on_effect_loaded(this));
	}

	void destroy()
	{
	}

	void set_parameters(float bright_threshold=0.5f, float middle_gray=0.72f, float lum_white=1.5f)
	{
		var_bright_threshold->set_float(bright_threshold);
		var_middle_gray->set_float(middle_gray);
		var_lum_white->set_float(lum_white);
	}

	void do_bright_pass(dxShaderResourceView hdr_image, dxShaderResourceView greyed_hdr_image_1x1, ColorRenderTarget& target)
	{
		var_hdr_image->set_resource_view(hdr_image);
		var_greyed_hdr_image_1x1->set_resource_view(greyed_hdr_image_1x1);

		LawnEngineUtil::setRenderTargets(target, NULL);

		LawnEngineUtil::setViewport(0, 0, target.w, target.h);
		technique->render(LawnEngineUtil::full_screen_quad_geometry(), shader_params);

		LawnEngineUtil::releasePixelShaderResources(2);
		LawnEngineUtil::releaseRenderTargets();
	}

};
