#pragma once

#include "HDRBrightPass.h"
#include "HDRFinalPass.h"


class HDRRenderManager : public RenderPipeline
{
	DepthStencilRenderTarget depthStencilRenderTarget;

	SceneRenderPass forwardLightingPass;

	ColorRenderTarget hdr_image;
	ColorRenderTarget greyed_hdr_image;
	ColorRenderTarget lighted_area;
	ColorRenderTarget half_blurred_lighted_area;
	ColorRenderTarget fully_blurred_lighted_area;
	WrappingColorRenderTarget renderViewRenderTarget;

	DrawSkyPass drawSkyPass;

	MeasureLuminancePass measureLuminancePass;
	DownSamplePass downSamplePass;
	HDRBrightPass brightPass;
	OneDirectionBlurPass blurPass;
	HDRFinalPass toneMappingPass;

	GenerateNoShadowMapPlugin generateNoShadowMapPlugin;

public:
	HDRRenderManager(SceneManager* sceneMgr, LightCollection* lightCollection, CameraPtr camera, RenderView* renderView, const char* sky_texture_file_name)
		:RenderPipeline(sceneMgr, lightCollection, camera, renderView)
	{
		RenderableEffect::addPlugin(&generateNoShadowMapPlugin);

		measureLuminancePass.init();

		downSamplePass.init();

		brightPass.init();

		blurPass.init();

		toneMappingPass.init();

		drawSkyPass.init(sky_texture_file_name, float3::zero(), camera->getZFar() / sqrt(3.0f));

		// forwardLightingPass
		{
			forwardLightingPass.sceneMgr = sceneMgr;
			forwardLightingPass.lightCollection = lightCollection;
			forwardLightingPass.camera = camera;
			forwardLightingPass.renderTechnique = RenderableEffect::RenderTechnique::ForwardLighting;
		}
	}

	virtual ~HDRRenderManager()
	{
	}

	virtual void on_swap_chain_resized()
	{
		uint w = renderView->viewport.Width;
		uint h = renderView->viewport.Height;

		renderViewRenderTarget.renderTargetView = renderView->renderTargetView;
		renderViewRenderTarget.w = w;
		renderViewRenderTarget.h = h;

		this->depthStencilRenderTarget.init(w, h, DXGI_FORMAT_R32_TYPELESS, DXGI_FORMAT_D32_FLOAT);

		occlusionBuffer.init(w, h, DXGI_FORMAT_R8_UNORM);
		occlusionBuffer_tmp.init(w, h, DXGI_FORMAT_R8_UNORM);

		this->hdr_image.init(w, h, DXGI_FORMAT_R16G16B16A16_FLOAT);
		this->greyed_hdr_image.init(w, h, DXGI_FORMAT_R16_FLOAT);

		this->lighted_area.init(w/3, h/3, DXGI_FORMAT_R8G8B8A8_UNORM);

		this->half_blurred_lighted_area.init(w/3, h/3, DXGI_FORMAT_R8G8B8A8_UNORM);
		this->fully_blurred_lighted_area.init(w/3, h/3, DXGI_FORMAT_R8G8B8A8_UNORM);

		downSamplePass.reset_source_texture(greyed_hdr_image.texture);

		forwardLightingPass.viewport = renderView->viewport;
	}

	virtual void render()
	{
		dxDevice device = RenderSystem::instance().device;

		device->ClearDepthStencilView(depthStencilRenderTarget, D3D11_CLEAR_DEPTH, 1.0f, 0);

		device->ClearRenderTargetView(hdr_image, float4(0.5f, 0.5f, 0.5f));

		forwardLightingPass.setRenderTargetsForwardLighting(hdr_image, depthStencilRenderTarget);
		
		drawSkyPass.render_skybox(*camera, renderView->viewport);
		
		forwardLightingPass.render();

		measureLuminancePass.measure_luminance_rgb(hdr_image, greyed_hdr_image);

		ColorRenderTarget& greyed_hdr_image_1x1 = downSamplePass.do_down_sample(greyed_hdr_image);

		brightPass.do_bright_pass(hdr_image, greyed_hdr_image_1x1, lighted_area);

		blurPass.blur_x(lighted_area, half_blurred_lighted_area, 10);
		blurPass.blur_y(half_blurred_lighted_area, fully_blurred_lighted_area, 10);

		toneMappingPass.do_final_pass(hdr_image, greyed_hdr_image_1x1, fully_blurred_lighted_area, renderViewRenderTarget);
	}
};
