#pragma once


class HDRFinalPass : public RenderPass
{
public:
	EffectPtr effect;
	EffectTechnique* technique;

	ShaderResourceVariable* var_hdr_image;
	ShaderResourceVariable* var_greyed_hdr_image_1x1;
	ShaderResourceVariable* var_bloomed_light_area;
	ShaderVariable* var_middle_gray;
	ShaderVariable* var_lum_white;

	ShaderParamValues shader_params;

	HDRFinalPass()
	{

	}

	virtual ~HDRFinalPass()
	{
		destroy();
	}

	void init()
	{
		struct on_effect_loaded
		{
			typedef void result_type;

			HDRFinalPass* _this;

			on_effect_loaded(HDRFinalPass* _this)
				:_this(_this)
			{}

			void operator () (EffectPtr effect)
			{
				_this->technique = effect->get_technique("HDRFinalPass");

				ShaderParamValues& shader_params = _this->shader_params;

				shader_params.bind_to_shader(effect);
				_this->var_hdr_image = shader_params.get_resource_variable("hdr_image");
				_this->var_greyed_hdr_image_1x1 = shader_params.get_resource_variable("greyed_hdr_image_1x1");
				_this->var_bloomed_light_area = shader_params.get_resource_variable("bloomed_light_area");
				_this->var_middle_gray = shader_params.get_variable("middle_gray");
				_this->var_lum_white = shader_params.get_variable("lum_white");
			}
		};

		effect = Effect::create("HDRFinalPass", "HDRFinalPass.fx", on_effect_loaded(this));
	}

	void destroy()
	{
	}

	void set_parameters(float middle_gray=0.72f, float lum_white=1.5f)
	{
		var_middle_gray->set_float(middle_gray);
		var_lum_white->set_float(lum_white);
	}

	void do_final_pass(dxShaderResourceView hdr_image, dxShaderResourceView greyed_hdr_image_1x1, dxShaderResourceView bloomed_light_area, 
		dxRenderTargetView renderTarget, Viewport targetViewport)
	{
		var_hdr_image->set_resource_view(hdr_image);
		var_greyed_hdr_image_1x1->set_resource_view(greyed_hdr_image_1x1);
		var_bloomed_light_area->set_resource_view(bloomed_light_area);

		LawnEngineUtil::setRenderTargets(renderTarget, NULL);

		LawnEngineUtil::setViewport(targetViewport);
		technique->render(LawnEngineUtil::full_screen_quad_geometry(), shader_params);

		LawnEngineUtil::releasePixelShaderResources(3);
		LawnEngineUtil::releaseRenderTargets();
	}

};
