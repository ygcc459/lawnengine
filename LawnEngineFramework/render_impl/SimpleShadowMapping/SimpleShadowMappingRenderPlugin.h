#pragma once

class SimpleShadowMappingRenderPlugin : public RenderPassPlugin
{
	EffectResourceVariable* var_shadowMap;
	EffectVariable* var_matViewInverse;
	EffectVariable* var_matLightViewProj;

public:

	virtual const char* get_file_name() {
		return "SimpleShadow.fx";
	}

	virtual void on_effect_compiled(dxEffect effect) {
		var_shadowMap = effect->GetVariableByName("shadowMap")->AsShaderResource();
		var_matViewInverse = effect->GetVariableByName("matViewInverse")->AsMatrix();
		var_matLightViewProj = effect->GetVariableByName("matLightViewProj")->AsMatrix();
	}

	void setInputs(dxShaderResourceView shadowMap, const float4x4& viewInverse, const float4x4& lightViewProj) {
		var_shadowMap->SetResource(shadowMap);
		var_matViewInverse->set_matrix_row_majored((float*)&viewInverse);
		var_matLightViewProj->set_matrix_row_majored((float*)&lightViewProj);
	}

	void releaseInputs() {
		var_shadowMap->SetResource(NULL);
	}

};
