#pragma once

#include "../DeferredShading/DeferredShading_LightingPass.h"
#include "SimpleShadowMappingRenderPlugin.h"

class SimpleShadowMappingRenderManager : public RenderPipeline
{
	DepthStencilRenderTarget depthStencilRenderTarget;

	SceneRenderPass sceneRenderPass;
	DeferredShading_LightingPass lightingPass;
	SimpleShadowMappingRenderPlugin lightingPass_shadowPlugin;
	GenerateNoShadowMapPlugin generateNoShadowMapPlugin;

	SpotLightIterator shadowLight;
	std::shared_ptr<RawCamera> shadowLightCamera;

	CameraPtr camera;

	ColorRenderTarget depth;
	ColorRenderTarget normal;
	ColorRenderTarget albedo;

	ColorRenderTarget shadowMap;

public:
	SimpleShadowMappingRenderManager(SceneManager* sceneMgr, LightCollection* lightCollection, CameraPtr camera, RenderView* renderView, SpotLightIterator shadowLight)
		:shadowLight(shadowLight), lightingPass(&lightingPass_shadowPlugin), RenderPipeline(sceneMgr, lightCollection, camera, renderView)
	{
		RenderableEffect::addPlugin(&generateNoShadowMapPlugin);

		shadowLightCamera = std::shared_ptr<RawCamera>(new RawCamera());

		// sceneRenderPass
		{
			sceneRenderPass.sceneMgr = sceneMgr;
			sceneRenderPass.lightCollection = lightCollection;
		}
	}

	virtual ~SimpleShadowMappingRenderManager()
	{
	}

	virtual void on_swap_chain_resized()
	{
		uint w = renderView->viewport.Width;
		uint h = renderView->viewport.Height;

		this->depthStencilRenderTarget.init(w, h, DXGI_FORMAT_D32_FLOAT, false);

		this->depth.init(w, h, DXGI_FORMAT_R32_FLOAT);
		this->normal.init(w, h, DXGI_FORMAT_R16G16B16A16_FLOAT);
		this->albedo.init(w, h, DXGI_FORMAT_R8G8B8A8_UNORM);

		this->shadowMap.init(w, h, DXGI_FORMAT_R32_FLOAT);

		sceneRenderPass.viewport = renderView->viewport;
	}

	virtual void render()
	{
		dxDevice device = RenderSystem::instance().device;

		device->ClearRenderTargetView(*renderView, float4(0.0f, 0.0f, 0.0f));

		device->ClearRenderTargetView(depth, float4(1.0f, 0.0f, 0.0f));
		device->ClearRenderTargetView(normal, float4(0.0f, 0.0, 0.0f));
		device->ClearRenderTargetView(albedo, float4(0.0f, 0.0f, 0.0f, 1.0f));

		device->ClearRenderTargetView(shadowMap, float4(1.0f, 0.0f, 0.0f));

		// render scene to GBuffer
		{
			device->ClearDepthStencilView(depthStencilRenderTarget, D3D11_CLEAR_DEPTH, 1.0f, 0);

			sceneRenderPass.camera = camera;
			sceneRenderPass.renderTechnique = RenderableEffect::RenderTechnique::GBuffer;
			sceneRenderPass.setRenderTargetsGBuffer(depth, normal, albedo, depthStencilRenderTarget);
			sceneRenderPass.render();
		}

		float4x4 lightView, lightProj;
		getLightViewProjMatrix(*shadowLight, renderView->viewport, lightView, lightProj);

		// render scene to shadow map
		{
			device->ClearDepthStencilView(depthStencilRenderTarget, D3D11_CLEAR_DEPTH, 1.0f, 0);

			sceneRenderPass.camera = boost::static_pointer_cast<Camera>(shadowLightCamera);
			shadowLightCamera->viewMatrix = lightView;
			shadowLightCamera->projectionMatrix = lightProj;
			sceneRenderPass.renderTechnique = RenderableEffect::RenderTechnique::DepthOnly;
			sceneRenderPass.setRenderTargetsDepthOnly(shadowMap, depthStencilRenderTarget);
			sceneRenderPass.render();
		}

		// render scene to RenderView
		{
			float4x4 viewInverse; camera->getViewMatrix(viewInverse); viewInverse.inverse();
			float4x4 lightViewProj = lightView * lightProj;

			lightingPass_shadowPlugin.setInputs(shadowMap, viewInverse, lightViewProj);

			lightingPass.setInputs(depth, normal, albedo, camera, renderView->viewport);
			lightingPass.setRenderTargets(*renderView, depthStencilRenderTarget);

			for(PointLightIterator it=lightCollection->point_light_begin(); it!=lightCollection->point_light_end(); ++it)
				this->lightingPass.render_pointLight(*it);

			for(DirectionalLightIterator it=lightCollection->directional_light_begin(); it!=lightCollection->directional_light_end(); ++it)
				this->lightingPass.render_directionalLight(*it);

			lightingPass_shadowPlugin.releaseInputs();

			lightingPass.releaseInputs();
		}
	}

	static void getLightViewProjMatrix(const SpotLight& light, const Viewport& viewport, float4x4& outViewMat, float4x4& outProjMat)
	{
		//FIXME: left will be invalid if light.direction is parallel to float3::up
		float3 left; float3::cross(left, light.direction, float3::up());
		float3 up; float3::cross(up, left, light.direction);
		up.normalize();

		float4x4::createLookAtLH(outViewMat, light.position, light.direction, up);

		float aspect = float(viewport.width) / viewport.height;
		float4x4::createPerspectiveFovLH(outProjMat, light.outerConeAngle, aspect, 1.0f, 10000.0f);
	}

};
