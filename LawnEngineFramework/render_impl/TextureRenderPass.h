#pragma once


class TextureRenderPass : public RenderPass
{
	EffectTechnique* technique;
	
	ShaderParamValues shader_params;
	ShaderConstantBuffer cb_global;
	ShaderResourceVariable* var_sourceTexture;
	ShaderVariable* var_color_mul;
	ShaderVariable* var_color_add;

public:
	TextureRenderPass()
	{

	}

	virtual ~TextureRenderPass()
	{
		destroy();
	}

	void init()
	{
		struct on_effect_loaded {
			typedef void result_type;
		
			TextureRenderPass* _this;
		
			on_effect_loaded(TextureRenderPass* _this)
				:_this(_this)
			{}
		
			void operator () (EffectPtr effect)
			{
				_this->technique = effect->get_technique("texture_render");
				
				ShaderParamValues& shader_params = _this->shader_params;
				
				shader_params.bind_to_shader(effect);
				_this->var_sourceTexture = shader_params.get_resource_variable("source_texture");
				_this->var_color_mul = shader_params.get_variable("color_mul");
				_this->var_color_add = shader_params.get_variable("color_add");
				_this->cb_global = shader_params.get_cbuffer("$Globals");
			}
		};
		
		effect = Effect::create("TextureRenderPass", "TextureRenderPass.fx", on_effect_loaded(this));
	}

	void destroy()
	{
		
	}

	void render_texture(dxShaderResourceView srv, 
		float range_min, float range_max, 
		bool red, bool green, bool blue, bool alpha)
	{
		float k = 1.0f / (range_max - range_min);
		
		float4 cmul(k, k, k, k);
		float4 cadd(-range_min/k, -range_min/k, -range_min/k, -range_min/k);
		
		float4 channel_mask((red?1.0f:0.0f), (green?1.0f:0.0f), (blue?1.0f:0.0f), (alpha?1.0f:0.0f));
		cmul = cmul.memberwiseMul(channel_mask);
		cadd = cadd.memberwiseMul(channel_mask);
		
		render_texture(srv, cmul, cadd);
	}

	void render_texture(dxShaderResourceView srv, const float4& color_mul, const float4& color_add)
	{
		var_color_mul->set_vector4(color_mul);
		var_color_add->set_vector4(color_add);
		cb_global.update_to_device();
		
		var_sourceTexture->set_resource_view(srv);
		
		technique->render(LawnEngineUtil::full_screen_quad_geometry(), shader_params);
		
		LawnEngineUtil::releasePixelShaderResources(1);
	}
	
};
