#pragma once

class NormalMappingPhongRenderableEffect : public RenderableEffect
{
public:
	NormalMappingPhongRenderableEffect()
	{
	}

	virtual ~NormalMappingPhongRenderableEffect()
	{
	}

	virtual const char* getEffectName() const {
		return "phong_normalMapping";
	}

	virtual const char* getEffectFileName() const {
		return "NormalMappingPhongRenderableEffect.lefx";
	}

	virtual void on_init()
	{
		h_lightDirection = effect->GetVariableByName("lightDirection")->AsVector();
		h_lightColor = effect->GetVariableByName("lightColor")->AsVector();
		h_MaterialAmbient = effect->GetVariableByName("materialAmbient")->AsVector();
		h_MaterialDiffuse = effect->GetVariableByName("materialDiffuse")->AsVector();
		h_MaterialSpecular = effect->GetVariableByName("materialSpecular")->AsVector();
		h_albedoTexture = effect->GetVariableByName("albedoTexture")->AsShaderResource();
		h_normalTexture = effect->GetVariableByName("normalTexture")->AsShaderResource();

		h_cameraPos = effect->GetVariableByName("cameraPos")->AsVector();

		h_View = effect->GetVariableByName("mView")->AsMatrix();
		h_Projection = effect->GetVariableByName("mProj")->AsMatrix();
		h_World = effect->GetVariableByName("mWorld")->AsMatrix();

		emptyTexture = Texture2D::create_from_file("emptyTexture.png");

		h_deferredLighting_lightingTexture = effect->GetVariableByName("deferredLighting_lightingTexture")->AsShaderResource();
	}

	virtual void render_objects(SubEntity* objects[], uint object_count, SceneRenderPass& sceneRenderPass)
	{
		EffectTechnique& technique = renderTechniques[int(sceneRenderPass.renderTechnique)];

		pre_render(sceneRenderPass);

		for(uint i=0; i<technique.pass_count(); i++)
		{
			const EffectPass& pass = technique.get_pass(i);

			for(uint i=0; i<object_count; i++)
			{
				pre_render_object(sceneRenderPass, objects[i]);
				pass.apply();

				GeometryPtr pGeometry = objects[i]->pGeometry;
				pGeometry->pre_render();
				pGeometry->render();
			}
		}
	}

	void pre_render(SceneRenderPass& sceneRenderPass)
	{
		Camera& camera = *sceneRenderPass.camera;

		float4x4 viewmat;
		camera.getViewMatrix(viewmat);
		h_View->set_matrix_row_majored(viewmat);

		float4x4 projmat;
		camera.getProjectionMatrix(projmat, sceneRenderPass.viewport);
		h_Projection->set_matrix_row_majored(projmat);

		if(sceneRenderPass.renderTechnique==RenderableEffect::ForwardLighting)
		{
			float3 cameraPos;
			camera.getPosition(OUT cameraPos);
			h_cameraPos->SetFloatVector((float*)&cameraPos);

			if(sceneRenderPass.lightCollection->directionalLights.empty()==false)
			{
				const DirectionalLight& dl = sceneRenderPass.lightCollection->directionalLights.front();

				h_lightDirection->SetFloatVector((float*)&dl.direction);
				h_lightColor->SetFloatVector((float*)&dl.color);
			}
			else
			{
				float3 lightDirection_zero(0.0f, 0.0f, 0.0f);
				h_lightDirection->SetFloatVector((float*)&lightDirection_zero);
				//no need to set h_lightColor
			}
		}
		else if(sceneRenderPass.renderTechnique==RenderableEffect::DeferredLighting)
		{
			h_deferredLighting_lightingTexture->SetResource(sceneRenderPass.getDeferredLightingLightTexture());
		}
	}

	void pre_render_object(SceneRenderPass& sceneRenderPass, SubEntity* se)
	{
		Material& material = *se->pMaterial;

		h_World->set_matrix_row_majored(se->absolute_matrix);

		if(material.diffuseMap)
			h_albedoTexture->SetResource(material.diffuseMap->pD3DShaderResView);
		else
			h_albedoTexture->SetResource(emptyTexture->pD3DShaderResView);

		h_MaterialDiffuse->SetFloatVector((float*)&material.diffuseColor);

		float specular[4] = {material.specularColor.x, material.specularColor.y, material.specularColor.z, material.specularPower};
		h_MaterialSpecular->SetFloatVector(specular);

		assert(material.detailMapType==DetailMapType::DetailMapType_NormalMapping);

		if(material.detailMap)
			h_normalTexture->SetResource(material.detailMap->pD3DShaderResView);
		else
			h_normalTexture->SetResource(emptyTexture->pD3DShaderResView);

		h_MaterialDiffuse->SetFloatVector((float*)&material.diffuseColor);
	}

protected:
	EffectVariable* h_lightDirection;
	EffectVariable* h_lightColor;
	EffectVariable* h_MaterialAmbient;
	EffectVariable* h_MaterialDiffuse;
	EffectVariable* h_MaterialSpecular;
	EffectVariable* h_cameraPos;
	EffectResourceVariable* h_albedoTexture;
	EffectResourceVariable* h_normalTexture;
	EffectResourceVariable* h_deferredLighting_lightingTexture;

	EffectVariable* h_View;
	EffectVariable* h_Projection;
	EffectVariable* h_World;

	Texture2DPtr emptyTexture;

public:
	RENDERABLEEFFECT_ENABLE_REQUIRE("phong_normalMapping", NormalMappingPhongRenderableEffect)

};
