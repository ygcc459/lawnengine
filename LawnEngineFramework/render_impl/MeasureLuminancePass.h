#pragma once


class MeasureLuminancePass : public RenderPass
{
public:
	EffectPtr effect;
	EffectTechnique* technique;

	ShaderParamValues shader_params;
	ShaderResourceVariable* var_sourceTexture;

	MeasureLuminancePass()
	{

	}

	virtual ~MeasureLuminancePass()
	{
		destroy();
	}

	void init()
	{
		struct on_effect_loaded {
			MeasureLuminancePass* _this;

			on_effect_loaded(MeasureLuminancePass* _this)
				:_this(_this)
			{}

			void operator () (EffectPtr effect)
			{
				_this->technique = effect->get_technique("MeasureLuminance");
				
				ShaderParamValues& shader_params = _this->shader_params;

				shader_params.bind_to_shader(effect);
				_this->var_sourceTexture = shader_params.get_resource_variable("sourceTexture");
			}
		};

		effect = Effect::create("MeasureLuminancePass", "MeasureLuminancePass.fx", on_effect_loaded(this));

	}

	void destroy()
	{
	}

	//down-sample the given source texture, and return the result texture
	void measure_luminance_rgb(dxShaderResourceView source, ColorRenderTarget& target)
	{
		var_sourceTexture->set_resource_view(source);
		LawnEngineUtil::setRenderTargets(target, NULL);

		LawnEngineUtil::setViewport(0, 0, target.w, target.h);
		technique->render(LawnEngineUtil::full_screen_quad_geometry(), shader_params);

		LawnEngineUtil::releasePixelShaderResources(1);
		LawnEngineUtil::releaseRenderTargets();
	}

};
