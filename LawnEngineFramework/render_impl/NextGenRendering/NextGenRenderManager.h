#pragma once

#include "../DeferredLighting/DeferredLighting_LightingPass.h"
#include "../VarianceShadowMapping/GenerateVarianceShadowMapPlugin.h"
#include "../VarianceShadowMapping/VarianceShadowMappingRenderPlugin.h"
#include "../ssvo/SSVOPass.h"
#include "../BilateralBlur.h"
#include "../ToLinearDepthPass.h"
#include "../DownSamplePass.h"
#include "../MeasureLuminancePass.h"
#include "../OneDirectionBlur.h"
#include "../TextureOperationPass.h"
#include "../hdr/HDRBrightPass.h"
#include "../hdr/HDRFinalPass.h"


class NextGenRenderManager : public RenderPipeline
{
public:
	SceneRenderPass pass_scene;
	DepthStencilRenderTarget depthStencilRenderTarget;

	float3 sceneBackgroundColor;

	// deferred lighting
	ColorRenderTarget depth;
	ColorRenderTarget normal;

	ColorRenderTarget light;

	ToLinearDepthPass pass_toLinearDepth;
	DeferredLighting_LightingPass pass_lighting;

	// VSM
	bool vsm_enabled;
	uint vsm_blur_length_x, vsm_blur_length_y;

	SceneRenderPass shadow_scene_pass;
	
	float shadowMinDistance, shadowMaxDistance;
	float vsm_lightBleeding_reduceAmount;
	float vsm_sceneDepthBias;
	uint shadowMapSize[2];

	GenerateVarianceShadowMapPlugin shadowGenPlugin;
	VarianceShadowMappingRenderPlugin shadowRenderPlugin;

	std::shared_ptr<RawCamera> shadowLightCamera;
	Viewport shadowLightViewport;
	
	DepthStencilRenderTarget shadowMapDepthStencil;
	ColorRenderTarget shadowMap;
	ColorRenderTarget shadowMap_blur_temp;

	// ssvo
	bool ssvo_enabled;
	bool show_ssvo_only;
	float ssvo_radius;
	float ssvo_ambient_amount;
	float ssvo_epsilon;
	float ssvo_depth_bias;
	float ssvo_reject_dist_sqr;
	float ssvo_intensity_scale;
	uint ssvo_blur_length_x, ssvo_blur_length_y;
	float ssvo_blur_mul_x, ssvo_blur_mul_y;
	
	ColorRenderTarget occlusionBuffer_temp1;
	ColorRenderTarget occlusionBuffer_temp2;

	SSVOPass ssvoPass;
	BilateralBlurPass bilateralBlurPass;
	TextureOperationPass textureOperationPass;

	// hdr
	bool hdr_enabled;
	uint hdr_light_blur_length_x, hdr_light_blur_length_y;

	float hdr_bright_threshold;
	float hdr_middle_gray;
	float hdr_lum_white;

	ColorRenderTarget hdr_image;
	ColorRenderTarget greyed_hdr_image;
	ColorRenderTarget lighted_area;
	ColorRenderTarget half_blurred_lighted_area;
	ColorRenderTarget fully_blurred_lighted_area;

	MeasureLuminancePass measureLuminancePass;
	DownSamplePass downSamplePass;
	HDRBrightPass brightPass;
	OneDirectionBlurPass blurPass;
	HDRFinalPass toneMappingPass;

	WrappingColorRenderTarget renderViewRenderTarget;

public:
	NextGenRenderManager(SceneManager* sceneMgr, LightCollection* lightCollection, CameraPtr camera, RenderView* renderView)
		:RenderPipeline(sceneMgr, lightCollection, camera, renderView), shadowLightCamera(new RawCamera())
	{
		sceneBackgroundColor = float3(0.5f, 0.5f, 0.5f);

		vsm_enabled = true;
		vsm_blur_length_x = vsm_blur_length_y = 5;
		vsm_lightBleeding_reduceAmount = 0.2f;
		vsm_sceneDepthBias = -0.015f;

		ssvo_enabled = true;
		show_ssvo_only = false;
		ssvo_radius = 0.5f;
		ssvo_ambient_amount = 0.1f;
		ssvo_epsilon = 0.0001f;
		ssvo_depth_bias = 0.002f;
		ssvo_reject_dist_sqr = 1.0f;
		ssvo_intensity_scale = 5.0f;
		ssvo_blur_length_x = 8;
		ssvo_blur_length_y = 4;
		ssvo_blur_mul_x = 1.0f;
		ssvo_blur_mul_y = 1.0f;

		hdr_enabled = true;
		hdr_light_blur_length_x = hdr_light_blur_length_y = 10;
		hdr_bright_threshold = 0.5f;
		hdr_middle_gray = 0.72f;
		hdr_lum_white = 1.5f;

		RenderableEffectPlugin* plugins[] = {&shadowGenPlugin};
		RenderableEffect::setup_plugins(plugins, sizeof(plugins) / sizeof(plugins[0]));

		pass_scene.sceneMgr = sceneMgr;
		pass_scene.lightCollection = lightCollection;
		pass_scene.camera = camera;

		pass_lighting.init(&shadowRenderPlugin);

		pass_toLinearDepth.init();

		bilateralBlurPass.init();

		measureLuminancePass.init();
		downSamplePass.init();
		brightPass.init();
		blurPass.init();
		toneMappingPass.init();

		shadow_scene_pass.sceneMgr = sceneMgr;
		shadow_scene_pass.camera = boost::static_pointer_cast<Camera>(shadowLightCamera);				
		shadow_scene_pass.renderTechnique = "generate_shadow";

		shadowMinDistance = 5.0f;
		shadowMaxDistance = 5000.0f;
		shadowMapSize[0] = 1024;
		shadowMapSize[1] = 1024;

		shadowLightViewport.TopLeftX = shadowLightViewport.TopLeftY = 0;
		shadowLightViewport.Width = shadowMapSize[0];
		shadowLightViewport.Height = shadowMapSize[1];
		shadowLightViewport.MinDepth = 0.0f;
		shadowLightViewport.MaxDepth = 1.0f;

		shadowMapDepthStencil.init(shadowMapSize[0], shadowMapSize[1], DXGI_FORMAT_D24_UNORM_S8_UINT);
		shadowMap.init(shadowMapSize[0], shadowMapSize[1], DXGI_FORMAT_R32G32_FLOAT);
		shadowMap_blur_temp.init(shadowMapSize[0], shadowMapSize[1], DXGI_FORMAT_R32G32_FLOAT);
	}

	virtual ~NextGenRenderManager()
	{
	}

	virtual void on_swap_chain_resized()
	{
		uint w = renderView->viewport.Width;
		uint h = renderView->viewport.Height;

		renderViewRenderTarget.renderTargetView = renderView->renderTargetView;
		renderViewRenderTarget.w = w;
		renderViewRenderTarget.h = h;

		depth.init(w, h, DXGI_FORMAT_R32_FLOAT);
		normal.init(w, h, DXGI_FORMAT_R16G16B16A16_FLOAT);
		light.init(w, h, DXGI_FORMAT_R16G16B16A16_FLOAT);

		depthStencilRenderTarget.init(w, h, DXGI_FORMAT_R32_TYPELESS, DXGI_FORMAT_D32_FLOAT, DXGI_FORMAT_R32_FLOAT);

		hdr_image.init(w, h, DXGI_FORMAT_R16G16B16A16_FLOAT);
		greyed_hdr_image.init(w, h, DXGI_FORMAT_R16_FLOAT);
		lighted_area.init(w/3, h/3, DXGI_FORMAT_R8G8B8A8_UNORM);
		half_blurred_lighted_area.init(w/3, h/3, DXGI_FORMAT_R8G8B8A8_UNORM);
		fully_blurred_lighted_area.init(w/3, h/3, DXGI_FORMAT_R8G8B8A8_UNORM);
		downSamplePass.reset_source_texture(greyed_hdr_image.texture);

		ssvoPass.viewport = renderView->viewport;
		ssvoPass.viewport.Width /= 2;
		ssvoPass.viewport.Height /= 2;
		occlusionBuffer_temp1.init(ssvoPass.viewport.Width, ssvoPass.viewport.Height, DXGI_FORMAT_R8_UNORM);
		occlusionBuffer_temp2.init(ssvoPass.viewport.Width, ssvoPass.viewport.Height, DXGI_FORMAT_R8_UNORM);

		pass_scene.viewport = renderView->viewport;
	}

	virtual void render()
	{
		dxDevice device = RenderSystem::instance().device;

		if(sceneMgr->get_enity_count()==0)
		{
			device->ClearRenderTargetView(*renderView, float4(sceneBackgroundColor, 1.0f));
			return;
		}

		float4x4 cameraView;
		camera->getViewMatrix(cameraView);

		float4x4 cameraInvView;
		float4x4::createInverse(cameraInvView, cameraView);

		float4x4 cameraProj;
		camera->getProjectionMatrix(cameraProj, renderView->viewport);

		float4x4 cameraInvProj;
		float4x4::createInverse(cameraInvProj, cameraProj);

		//
		// Deferred Lighting - GBuffer
		//
		device->ClearDepthStencilView(depthStencilRenderTarget, D3D11_CLEAR_DEPTH, 1.0f, 0);

		device->ClearRenderTargetView(depth, float4(1.0f, 0.0f, 0.0f));
		device->ClearRenderTargetView(normal, float4(0.0f, 0.0, 0.0f));

		LawnEngineUtil::setViewport(pass_scene.viewport);

		pass_scene.renderTechnique = "gbuffer";
		pass_scene.setRenderTargetsGBufferMin(normal, depthStencilRenderTarget);
		pass_scene.render();
		pass_scene.releaseRenderTargets();

		pass_toLinearDepth.hardwareDepth_to_linearDepth(depthStencilRenderTarget, depth, *camera);

		//
		// Deferred Lighting - Lights
		//
		device->ClearRenderTargetView(light, float4(0.0f, 0.0f, 0.0f));
		pass_lighting.setRenderTargets(light, depthStencilRenderTarget);
		pass_lighting.setInputs(depth, normal, pass_scene.camera, pass_scene.viewport);

		for(PointLightIterator it=lightCollection->point_light_begin(); it!=lightCollection->point_light_end(); ++it)
			this->pass_lighting.render_pointLight(*it);

		for(DirectionalLightIterator it=lightCollection->directional_light_begin(); it!=lightCollection->directional_light_end(); ++it)
			this->pass_lighting.render_directionalLight(*it);

		for(SpotLightIterator it=lightCollection->spot_light_begin(); it!=lightCollection->spot_light_end(); ++it)
		{
			const SpotLight& spotLight = (*it);
			if(vsm_enabled && spotLight.generateShadow)
			{
				// generate shadowMap
				device->ClearRenderTargetView(shadowMap, float4(shadowMaxDistance, shadowMaxDistance*shadowMaxDistance, 0.0f));
				
				device->ClearDepthStencilView(shadowMapDepthStencil, D3D11_CLEAR_DEPTH, 1.0f, 0);

				float4x4 lightView, lightProj;
				getLightViewProjMatrix(spotLight, shadowMinDistance, shadowMaxDistance, shadowLightViewport, lightView, lightProj);
				shadowLightCamera->viewMatrix = lightView;
				shadowLightCamera->projectionMatrix = lightProj;

				LawnEngineUtil::setViewport(shadowLightViewport);

				shadow_scene_pass.setRenderTargetsGenerateShadowMap(shadowMap, shadowMapDepthStencil);

				shadowGenPlugin.setInputs(spotLight.position);

				shadow_scene_pass.render();

				shadow_scene_pass.releaseRenderTargets();

				// blur shadowMap
				if(vsm_blur_length_x>0 || vsm_blur_length_y>0) {
					blurPass.blur_x(shadowMap, shadowMap_blur_temp, vsm_blur_length_x);
					blurPass.blur_y(shadowMap_blur_temp, shadowMap, vsm_blur_length_y);
				}

				// do lighting
				LawnEngineUtil::setViewport(pass_scene.viewport);
				pass_lighting.setRenderTargets(light, depthStencilRenderTarget);
				pass_lighting.setInputs(depth, normal, pass_scene.camera, pass_scene.viewport);
				shadowRenderPlugin.setInputs(shadowMap, cameraInvView, lightView*lightProj, spotLight.position, vsm_lightBleeding_reduceAmount, vsm_sceneDepthBias);
				pass_lighting.render_spotLight_shadowed(spotLight);
			}
			else
			{
				pass_lighting.render_spotLight(spotLight);
			}			
		}

		pass_lighting.releaseInputs();

		//
		// SSVO
		//
		if(ssvo_enabled && ssvoPass.is_ready())
		{
			LawnEngineUtil::setViewport(ssvoPass.viewport);

			ssvoPass.setParameters(ssvo_ambient_amount, ssvo_radius, ssvo_intensity_scale, ssvo_epsilon, ssvo_depth_bias, ssvo_reject_dist_sqr);

			ssvoPass.setRenderTargets(occlusionBuffer_temp1);
			ssvoPass.setInputs(depth, normal, cameraProj, cameraInvProj);
			ssvoPass.render();
			ssvoPass.releaseInputs();
			ssvoPass.releaseRenderTargets();

			bilateralBlurPass.setParams(ssvo_blur_length_x, ssvo_blur_mul_x);
			bilateralBlurPass.blur_x(occlusionBuffer_temp1, depth, occlusionBuffer_temp2, occlusionBuffer_temp1.w);
			
			bilateralBlurPass.setParams(ssvo_blur_length_y, ssvo_blur_mul_y);
			bilateralBlurPass.blur_y(occlusionBuffer_temp2, depth, occlusionBuffer_temp1, occlusionBuffer_temp2.h);

			LawnEngineUtil::setViewport(pass_scene.viewport);
			if(show_ssvo_only)
			{
				device->ClearRenderTargetView(*renderView, float4(0.0f, 0.0f, 0.0f, 1.0f));
				textureOperationPass.add_source_to_target_channelScattered(occlusionBuffer_temp1, *renderView);
				return;
			}
			else
			{				
				textureOperationPass.add_source_to_target_channelScattered(occlusionBuffer_temp1, light);
			}
		}

		//
		// Deferred Lighting - final
		//
		if(hdr_enabled)
		{
			device->ClearRenderTargetView(hdr_image, float4(sceneBackgroundColor, 1.0f));

			LawnEngineUtil::setViewport(pass_scene.viewport);

			pass_scene.renderTechnique = "deferred_lighting";
			pass_scene.setDeferredLightingLightTexture(light);
			pass_scene.setRenderTargetsDeferredLighting(hdr_image, depthStencilRenderTarget);
			pass_scene.render();
			pass_scene.releaseRenderTargets();
			pass_scene.unsetDeferredLightingLightTexture();
		}
		else
		{
			device->ClearRenderTargetView(*renderView, float4(sceneBackgroundColor, 1.0f));

			LawnEngineUtil::setViewport(pass_scene.viewport);

			pass_scene.renderTechnique = "deferred_lighting";
			pass_scene.setDeferredLightingLightTexture(light);
			pass_scene.setRenderTargetsDeferredLighting(*renderView, depthStencilRenderTarget);
			pass_scene.render();
			pass_scene.releaseRenderTargets();
			pass_scene.unsetDeferredLightingLightTexture();
		}

		//
		// HDR
		//
		if(hdr_enabled)
		{
			measureLuminancePass.measure_luminance_rgb(hdr_image, greyed_hdr_image);

			ColorRenderTarget& greyed_hdr_image_1x1 = downSamplePass.do_down_sample(greyed_hdr_image);

			brightPass.set_parameters(hdr_bright_threshold, hdr_middle_gray, hdr_lum_white);
			brightPass.do_bright_pass(hdr_image, greyed_hdr_image_1x1, lighted_area);

			blurPass.blur_x(lighted_area, half_blurred_lighted_area, hdr_light_blur_length_x);
			blurPass.blur_y(half_blurred_lighted_area, fully_blurred_lighted_area, hdr_light_blur_length_y);

			toneMappingPass.set_parameters(hdr_middle_gray, hdr_lum_white);
			toneMappingPass.do_final_pass(hdr_image, greyed_hdr_image_1x1, fully_blurred_lighted_area, renderViewRenderTarget);
		}
	}

public:
	bool get_ssvo_enabled() const { return ssvo_enabled; }
	void set_ssvo_enabled(bool val) { ssvo_enabled = val; }

protected:
	static void getLightViewProjMatrix(const SpotLight& light, float shadowMinDistance, float shadowMaxDistance, const Viewport& viewport, float4x4& outViewMat, float4x4& outProjMat)
	{
		//FIXME: left will be invalid if light.direction is parallel to float3::up
		float3 left; float3::cross(left, light.direction, float3::up());
		float3 up; float3::cross(up, left, light.direction);
		up.normalize();

		float4x4::createLookAtLH(outViewMat, light.position, light.position + light.direction, up);

		float aspect = float(viewport.width) / viewport.height;
		float4x4::createPerspectiveFovLH(outProjMat, light.outerConeAngle, aspect, shadowMinDistance, shadowMaxDistance);
	}
};
