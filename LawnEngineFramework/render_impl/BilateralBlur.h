#pragma once

class BilateralBlurPass : RenderPass
{
	EffectPtr effect;

	EffectResourceVariable* sourceTexture;
	EffectResourceVariable* diffTexture;

	EffectVariable* src_tex_size;
	EffectVariable* kernel_radius;

	EffectVariable* init_g;
	EffectVariable* blur_factor;
	EffectVariable* sharpness_factor;

	EffectTechnique* tech_blurX;
	EffectTechnique* tech_blurY;
	EffectTechnique* tech_blurX_add;
	EffectTechnique* tech_blurY_add;

public:
	BilateralBlurPass()
	{
		
	}

	virtual ~BilateralBlurPass()
	{
		destroy();
	}

	void init()
	{
		struct on_effect_loaded {
			typedef void result_type;
			void operator () (BilateralBlurPass* _this, EffectPtr effect)
			{
				_this->tech_blurX = effect->get_technique("BlurX");
				_this->tech_blurY = effect->get_technique("BlurY");
				_this->tech_blurX_add = effect->get_technique("BlurX_add");
				_this->tech_blurY_add = effect->get_technique("BlurY_add");

				_this->sourceTexture = effect->get_resource_variable("sourceTexture");
				_this->diffTexture = effect->get_resource_variable("diffTexture");
				_this->src_tex_size = effect->get_variable("src_tex_size");
				_this->kernel_radius = effect->get_variable("kernel_radius");
				_this->init_g = effect->get_variable("init_g");
				_this->blur_factor = effect->get_variable("blur_factor");
				_this->sharpness_factor = effect->get_variable("sharpness_factor");
			}
		};

		effect = Effect::create("BilateralBlurRenderPass", "BilateralBlurRenderPass.fx", Effect::EffectLoadedHandler(BOOST_BIND(on_effect_loaded(), this, _1)));
	}

	void destroy()
	{
	}

	void setParams(int kernel_radius, float multiplier)
	{
		this->kernel_radius->SetInt(kernel_radius);

		float rho = kernel_radius / 4.0f;
		init_g->set_float(multiplier / std::sqrt(2.0f * PI * rho * rho));

		float f = 1 / (2 * rho * rho);
		blur_factor->set_float(f);

		sharpness_factor->set_float(f);
	}

	void blur_x(dxShaderResourceView sourceTexture, dxShaderResourceView diffTexture, dxRenderTargetView target, uint sourceTextureWidth)
	{
		LawnEngineUtil::setRenderTargets(target, (dxDepthStencilView)NULL);

		this->sourceTexture->SetResource(sourceTexture);
		this->diffTexture->SetResource(diffTexture);

		float tex_size = float(sourceTextureWidth);
		this->src_tex_size->SetFloatVector(float2(tex_size, 1.0f / tex_size));

		tech_blurX->render(LawnEngineUtil::full_screen_quad_geometry());

		dxShaderResourceView srv[] = {NULL, NULL};
		RenderSystem::instance().device->PSSetShaderResources(0, sizeof(srv)/sizeof(srv[0]), srv);
	}

	void blur_y(dxShaderResourceView sourceTexture, dxShaderResourceView diffTexture, dxRenderTargetView target, uint sourceTextureHeight)
	{
		LawnEngineUtil::setRenderTargets(target, (dxDepthStencilView)NULL);

		this->sourceTexture->SetResource(sourceTexture);
		this->diffTexture->SetResource(diffTexture);

		float tex_size = float(sourceTextureHeight);
		this->src_tex_size->SetFloatVector(float2(tex_size, 1.0f / tex_size));

		tech_blurY->render(LawnEngineUtil::full_screen_quad_geometry());

		dxShaderResourceView srv[] = {NULL, NULL};
		RenderSystem::instance().device->PSSetShaderResources(0, sizeof(srv)/sizeof(srv[0]), srv);
	}

	void blur_x_add(dxShaderResourceView sourceTexture, dxShaderResourceView diffTexture, dxRenderTargetView target, uint sourceTextureWidth)
	{
		LawnEngineUtil::setRenderTargets(target, (dxDepthStencilView)NULL);

		this->sourceTexture->SetResource(sourceTexture);
		this->diffTexture->SetResource(diffTexture);

		float tex_size = float(sourceTextureWidth);
		this->src_tex_size->SetFloatVector(float2(tex_size, 1.0f / tex_size));

		tech_blurX_add->render(LawnEngineUtil::full_screen_quad_geometry());

		dxShaderResourceView srv[] = {NULL, NULL};
		RenderSystem::instance().device->PSSetShaderResources(0, sizeof(srv)/sizeof(srv[0]), srv);
	}

	void blur_y_add(dxShaderResourceView sourceTexture, dxShaderResourceView diffTexture, dxRenderTargetView target, uint sourceTextureHeight)
	{
		LawnEngineUtil::setRenderTargets(target, (dxDepthStencilView)NULL);

		this->sourceTexture->SetResource(sourceTexture);
		this->diffTexture->SetResource(diffTexture);

		float tex_size = float(sourceTextureHeight);
		this->src_tex_size->SetFloatVector(float2(tex_size, 1.0f / tex_size));

		tech_blurY_add->render(LawnEngineUtil::full_screen_quad_geometry());

		dxShaderResourceView srv[] = {NULL, NULL};
		RenderSystem::instance().device->PSSetShaderResources(0, sizeof(srv)/sizeof(srv[0]), srv);
	}

};
