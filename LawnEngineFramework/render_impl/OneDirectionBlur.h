#pragma once

class OneDirectionBlurPass : RenderPass
{
	EffectPtr effect;
	ShaderParamValues shader_params;

	ShaderResourceVariable* var_sourceTexture;
	ShaderVariable* var_blur_step;
	ShaderVariable* var_blur_length;
	ShaderConstantBuffer cb_global;

	EffectTechnique* tech_blur;

public:
	OneDirectionBlurPass()
	{
		
	}

	virtual ~OneDirectionBlurPass()
	{
		destroy();
	}

	void init()
	{
		struct on_effect_loaded
		{
			typedef void result_type;

			OneDirectionBlurPass* _this;

			on_effect_loaded(OneDirectionBlurPass* _this)
				:_this(_this)
			{}

			void operator () (EffectPtr effect)
			{
				_this->tech_blur = effect->get_technique("blur");

				ShaderParamValues& shader_params = _this->shader_params;

				shader_params.bind_to_shader(effect);
				_this->var_sourceTexture = shader_params.get_resource_variable("sourceTexture");
				_this->var_blur_step = shader_params.get_variable("blur_step");
				_this->var_blur_length = shader_params.get_variable("blur_length");

				_this->cb_global = shader_params.get_cbuffer("$Globals");
			}
		};

		effect = Effect::create("OneDirectionBlurRenderPass", "OneDirectionBlurRenderPass.fx", on_effect_loaded(this));
	}

	void destroy()
	{
	}

	void begin_blur()
	{
		
	}

	//
	// blur_direction = (1,0) means to blur along X axis
	//                = (0,1) means to blur along Y axis
	//
	void blur(dxShaderResourceView sourceTexture, ColorRenderTarget& target, const float2 blur_direction, uint blur_length)
	{
		LawnEngineUtil::setRenderTargets(target, (dxDepthStencilView)NULL);
		LawnEngineUtil::setViewport(0, 0, target.w, target.h);

		float2 blur_step(blur_direction.x / float(target.w), blur_direction.y / float(target.h));
		var_blur_step->set_vector2(blur_step);

		var_blur_length->set_float((float)blur_length);

		cb_global.update_to_device();

		var_sourceTexture->set_resource_view(sourceTexture);

		tech_blur->render(LawnEngineUtil::full_screen_quad_geometry(), shader_params);
		
		LawnEngineUtil::releasePixelShaderResources(1);
		LawnEngineUtil::releaseRenderTargets();
	}

	void end_blur()
	{
		
	}

};
