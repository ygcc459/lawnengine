#pragma once


class ToLinearDepthPass : public RenderPass
{
public:
	EffectPtr effect;

	EffectTechnique* technique;

	ShaderConstantBuffer cb_global;
	ShaderResourceVariable* var_sourceTexture;
	ShaderVariable* var_q;

	ShaderParamValues shader_params;

	ToLinearDepthPass()
	{

	}

	virtual ~ToLinearDepthPass()
	{
		destroy();
	}

	void init()
	{
		struct on_effect_loaded {
			typedef void result_type;

			ToLinearDepthPass* _this;

			on_effect_loaded(ToLinearDepthPass* _this)
				:_this(_this)
			{}

			void operator () (EffectPtr effect)
			{
				_this->technique = effect->get_technique("toLinearDepth");

				ShaderParamValues& shader_params = _this->shader_params;

				shader_params.bind_to_shader(effect);
				_this->var_sourceTexture = shader_params.get_resource_variable("depthTexture");
				_this->var_q = shader_params.get_variable("q");
				_this->cb_global = shader_params.get_cbuffer("$Globals");
			}
		};

		effect = Effect::create("ToLinearDepthPass", "ToLinearDepthPass.fx", on_effect_loaded(this));
	}

	void destroy()
	{
		
	}

	void hardwareDepth_to_linearDepth(dxShaderResourceView hardwareDepthBuffer, ColorRenderTarget& outLinearDepth, Camera& camera)
	{
		LawnEngineUtil::setRenderTargets(outLinearDepth, NULL);
		
		float q0 = camera.getZFar() / (camera.getZFar() - camera.getZNear());
		float2 q(camera.getZNear() * q0, q0);
		var_q->set_vector2(q);
		cb_global.update_to_device();

		var_sourceTexture->set_resource_view(hardwareDepthBuffer);

		technique->render(LawnEngineUtil::full_screen_quad_geometry(), shader_params);

		LawnEngineUtil::releasePixelShaderResources(1);
		LawnEngineUtil::releaseRenderTargets();
	}
	
};
