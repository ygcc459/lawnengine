#pragma once

#include "DeferredShading_LightingPass.h"


class DeferredShadingRenderManager : public RenderPipeline
{
	ColorRenderTarget depth;
	ColorRenderTarget normal;
	ColorRenderTarget albedo;

	DepthStencilRenderTarget depthStencilRenderTarget;

	SceneRenderPass pass_GBuffer;
	DeferredShading_LightingPass pass_lighting;

	GenerateNoShadowMapPlugin generateNoShadowMapPlugin;

public:
	DeferredShadingRenderManager(SceneManager* sceneMgr, LightCollection* lightCollection, CameraPtr camera, RenderView* renderView)
		:RenderPipeline(sceneMgr, lightCollection, camera, renderView)
	{
		RenderableEffect::addPlugin(&generateNoShadowMapPlugin);

		// GBuffer
		{
			pass_GBuffer.sceneMgr = sceneMgr;
			pass_GBuffer.lightCollection = lightCollection;
			pass_GBuffer.camera = camera;
			pass_GBuffer.renderTechnique = RenderableEffect::RenderTechnique::GBuffer;
		}
	}

	virtual ~DeferredShadingRenderManager()
	{
	}

	virtual void on_swap_chain_resized()
	{
		uint w = renderView->viewport.Width;
		uint h = renderView->viewport.Height;

		this->depth.init(w, h, DXGI_FORMAT_R32_FLOAT);
		this->normal.init(w, h, DXGI_FORMAT_R16G16_FLOAT);
		this->albedo.init(w, h, DXGI_FORMAT_R8G8B8A8_UNORM);
		this->depthStencilRenderTarget.init(w, h, DXGI_FORMAT_R32_TYPELESS, DXGI_FORMAT_D32_FLOAT);
		
		pass_GBuffer.viewport = renderView->viewport;
	}

	virtual void render()
	{
		dxDevice device = RenderSystem::instance().device;

		device->ClearRenderTargetView(*renderView, float4(0.0f, 0.0f, 0.0f));
		device->ClearDepthStencilView(depthStencilRenderTarget, D3D11_CLEAR_DEPTH, 1.0f, 0);

		device->ClearRenderTargetView(depth, float4(1.0f, 0.0f, 0.0f));
		device->ClearRenderTargetView(normal, float4(0.0f, 0.0, 0.0f));
		device->ClearRenderTargetView(albedo, float4(0.0f, 0.0f, 0.0f, 1.0f));

		LawnEngineUtil::setViewport(pass_GBuffer.viewport);

		pass_GBuffer.setRenderTargetsGBuffer(depth, normal, albedo, depthStencilRenderTarget);
		pass_GBuffer.render();

		pass_lighting.setRenderTargets(*renderView, depthStencilRenderTarget);
		pass_lighting.setInputs(depth, normal, albedo, pass_GBuffer.camera, pass_GBuffer.viewport);

		for(PointLightIterator it=lightCollection->point_light_begin(); it!=lightCollection->point_light_end(); ++it)
			this->pass_lighting.render_pointLight(*it);

		for(DirectionalLightIterator it=lightCollection->directional_light_begin(); it!=lightCollection->directional_light_end(); ++it)
			this->pass_lighting.render_directionalLight(*it);

		pass_lighting.releaseInputs();
	}
};
