#pragma once


class DeferredShading_LightingPass : public RenderPass
{
	EffectVariable* var_lightColor;

	EffectVariable* var_lightDirecion;

	EffectVariable* var_lightPosition;
	EffectVariable* var_lightRadius;

	EffectVariable* var_cameraInvProj;

	EffectVariable* var_materialDiffuse;
	EffectResourceVariable* var_depth;
	EffectResourceVariable* var_normal;
	EffectResourceVariable* var_albedo;

	EffectTechnique tech_directionalLight, tech_pointLight;

	EffectPtr effect;

	float4x4 cameraView, cameraInvProj;

	RenderPassPlugin* shadowRenderPlugin;

public:
	DeferredShading_LightingPass(RenderPassPlugin* shadowRenderPlugin=NULL)
		:cameraView(float4x4::identity()), cameraInvProj(float4x4::identity()), shadowRenderPlugin(shadowRenderPlugin)
	{
		struct on_effect_loaded {
			typedef void result_type;
			void operator () (DeferredShading_LightingPass* _this, EffectPtr effect)
			{
				_this->tech_directionalLight = effect->get_technique("directionalLight");
				_this->tech_pointLight = effect->get_technique("pointLight");

				_this->var_lightColor = effect->get_variable("lightColor");
				_this->var_materialDiffuse = effect->get_variable("materialDiffuse");

				_this->var_lightDirecion = effect->get_variable("lightDirection");

				_this->var_lightRadius = effect->get_variable("lightRadius");
				_this->var_lightPosition = effect->get_variable("lightPosition");

				_this->var_cameraInvProj = effect->get_variable("matInvProj");

				_this->var_depth = effect->get_resource_variable("depthTexture");
				_this->var_normal = effect->get_resource_variable("normalTexture");
				_this->var_albedo = effect->get_resource_variable("albedoTexture");
			}
		};

		if(shadowRenderPlugin)
		{
			const char* other_included_files[] = {shadowRenderPlugin->get_file_name()};

			effect = Effect::create("DeferredShading_LightingPass", "DeferredShading_LightingPass.fx", 
				Effect::EffectLoadedHandler(BOOST_BIND(on_effect_loaded(), this, _1)), 
				sizeof(other_included_files) / sizeof(other_included_files[0]), other_included_files);
		}
		else
		{
			effect = Effect::create("DeferredShading_LightingPass", "DeferredShading_LightingPass.fx", 
				Effect::EffectLoadedHandler(BOOST_BIND(on_effect_loaded(), this, _1)));
		}
	}

	virtual ~DeferredShading_LightingPass(void)
	{
		
	}

	void setRenderTargets(dxRenderTargetView lighting, dxDepthStencilView depth) {
		LawnEngineUtil::setRenderTargets(lighting, depth);
	}
	
	void setInputs(dxShaderResourceView depth_texture, dxShaderResourceView normal_texture, dxShaderResourceView albedo_texture, CameraPtr camera, const Viewport& viewport) {
		var_depth->SetResource(depth_texture);
		var_normal->SetResource(normal_texture);
		var_albedo->SetResource(albedo_texture);

		camera->getViewMatrix(cameraView);
		camera->getProjectionMatrix(cameraInvProj, viewport);
		cameraInvProj.inverse();
	}

	virtual void render_directionalLight(const DirectionalLight& directionalLight)
	{
		float3 lightDir;
		directionalLight.direction.transformNormal(cameraView, OUT &lightDir);
		var_lightDirecion->SetFloatVector((float*)&lightDir);

		var_lightColor->SetFloatVector((float*)&directionalLight.color);

		var_cameraInvProj->set_matrix_row_majored((float*)&cameraInvProj);

		tech_directionalLight->render(LawnEngineUtil::full_screen_quad_geometry());
	}

	virtual void render_pointLight(const PointLight& pointLight)
	{
		float3 lightPos;
		pointLight.position.transformCoord(cameraView, OUT &lightPos);
		var_lightPosition->SetFloatVector((float*)&lightPos);

		var_lightRadius->set_float(pointLight.range);

		var_lightColor->SetFloatVector((float*)&pointLight.color);

		var_cameraInvProj->set_matrix_row_majored((float*)&cameraInvProj);

		tech_pointLight->render(LawnEngineUtil::full_screen_quad_geometry());
	}

	void releaseInputs()
	{
		var_depth->SetResource(NULL);
		var_normal->SetResource(NULL);
		var_albedo->SetResource(NULL);
		tech_directionalLight.passes[0].apply();
		tech_pointLight.passes[0].apply();
	}

};
