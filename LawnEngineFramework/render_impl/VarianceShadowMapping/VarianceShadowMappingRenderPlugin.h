#pragma once

class VarianceShadowMappingRenderPlugin : public RenderPassPlugin
{
	EffectResourceVariable* var_shadowMap;
	EffectVariable* var_lightPosition;
	EffectVariable* var_matLight;
	EffectVariable* var_lightBleeding_reduce_amount;
	EffectVariable* var_sceneDepthBias;

public:
	virtual const char* get_file_name() {
		return "VSMShadow.fx";
	}

	virtual void on_effect_compiled(dxEffect effect) {
		var_shadowMap = effect->GetVariableByName("shadowMap")->AsShaderResource();
		var_matLight = effect->GetVariableByName("matLight")->AsMatrix();
		var_lightPosition = effect->GetVariableByName("lightPosition")->AsVector();
		var_lightBleeding_reduce_amount = effect->GetVariableByName("lightBleeding_reduce_amount")->AsScalar();
		var_sceneDepthBias = effect->GetVariableByName("sceneDepthBias")->AsScalar();
	}

	void setInputs(dxShaderResourceView shadowMap, const float4x4& viewInverse, const float4x4& lightViewProj, const float3& lightPosition, float lightBleeding_reduce_amount=0.2f, float sceneDepthBias=-0.015f) {
		var_shadowMap->SetResource(shadowMap);
		float4x4 matLight = viewInverse * lightViewProj;
		var_matLight->set_matrix_row_majored((float*)&matLight);
		var_lightPosition->SetFloatVector((float*)&lightPosition);
		var_lightBleeding_reduce_amount->set_float(lightBleeding_reduce_amount);
		var_sceneDepthBias->set_float(sceneDepthBias);
	}

	//void releaseInputs() {
	//	var_shadowMap->SetResource(NULL);
	//}

};
