#pragma once

class GenerateVarianceShadowMapPlugin : public RenderableEffectPlugin
{
	EffectVariable* var_lightPositioin;

public:
	virtual const char* get_file_name() {
		return "GenerateVarianceShadowMapPlugin.fx";
	}

	virtual void on_effect_compiled(dxEffect effect) {
		var_lightPositioin = effect->GetVariableByName("lightPosition")->AsVector();
	}

	void setInputs(const float3& lightPosition) {
		var_lightPositioin->SetFloatVector((float*)&lightPosition);
	}

	virtual void pre_render_scene(SceneRenderPass& currentSceneRenderPass) {

	}

};
