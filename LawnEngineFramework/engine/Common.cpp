#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "Common.h"
#include <chrono>

namespace Util
{

	bool DeserializeFromAsset(AssetPath path, ReflectiveObject& target)
	{
		std::unique_ptr<ReadStream> stream = path.OpenRead();
		if (!stream->CanRead())
		{
			return false;
		}

		Archive archive;
		if (archive.ReadFrom(stream.get()))
		{
			Deserializer deserializer(archive.Root());
			deserializer.DeserializeRootObject(&target, ObjectProperty<ReflectiveObject>());
			deserializer.InvokeAfterDeserialization();

			return true;
		}
		else
		{
			return false;
		}
	}

	bool SerializeToAsset(ReflectiveObject& source, AssetPath path)
	{
		std::unique_ptr<WriteStream> stream = path.OpenWrite();
		if (!stream->CanWrite())
		{
			return false;
		}

		Archive archive;
		archive.CreateNew();

		Serializer serializer(archive.Root());
		serializer.SerializeRootObject(&source, ObjectProperty<ReflectiveObject>());

		bool result = archive.WriteTo(stream.get());

		return result;
	}

	ReflectiveObject* CastPtr(ReflectiveObject* ptr, Type* toType)
	{
		if (ptr)
		{
			Type* fromType = ptr->GetType();

			if (!fromType->IsSameOrChildOf(toType))
			{
				return nullptr;
			}

			return ptr;
		}
		else
		{
			return nullptr;
		}
	}

	float GetCurrentTime()
	{
		auto t = std::chrono::system_clock::now().time_since_epoch();
		auto t2 = std::chrono::duration<float, std::ratio<1, 1>>(t);

		return t2.count();
	}

}
