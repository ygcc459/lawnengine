#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "LawnEngine.h"

#include "JobSystem/JobSystem.h"
#include "AssetSystem/AssetManager.h"
#include "InputSystem/OISInputManager.h"
#include "RenderSystem/RenderSystem.h"
#include "EntitySystem/World.h"
#include "RenderSystem/RenderView.h"
#include "UISystem/UISystem.h"

#include <vcpp/Timer.h>
#include "ImGUISystem.h"

//////////////////////////////////////////////////////////////////////////

LawnEngine::LawnEngine(void)
	: moduleInitFinished(false)
	, modulePreInitFinished(false)
	, currentFrame(0)
	, paused(false)
	, inputManager()
	, world()
	, uiSystem(nullptr)
{
	_KeepCommonTypeMetaSymbolsHack();
}

LawnEngine::~LawnEngine(void)
{
}

//////////////////////////////////////////////////////////////////////////

void LawnEngine::registerPlugin(LawnEnginePlugin * pModule)
{
	if (modulePreInitFinished)
		pModule->preInit();

	if (moduleInitFinished)
		pModule->init();

	modules.push_back(pModule);
}

void LawnEngine::unregisterPlugin(LawnEnginePlugin * pModule)
{
	for(auto it=modules.begin(); it!=modules.end(); ++it) {
		if((*it)==pModule) {
			modules.erase(it);
			break;
		}
	}
}

bool LawnEngine::isPluginRegistered(const LawnEnginePlugin * pModule) const
{
	for(auto it=modules.begin(); it!=modules.end(); ++it) {
		if((*it)==pModule)
			return true;
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////

bool LawnEngine::preInitModules()
{
	for (std::list<LawnEnginePlugin*>::iterator it = modules.begin(); it != modules.end(); it++)
		if ((*it)->preInit() == false)
			return false;

	modulePreInitFinished = true;

	return true;
}

bool LawnEngine::initModules()
{
	for (std::list<LawnEnginePlugin*>::iterator it = modules.begin(); it != modules.end(); it++)
		if ((*it)->init() == false)
			return false;

	moduleInitFinished = true;

	return true;
}

bool LawnEngine::startup(HWND windowHwnd)
{
	Log::Info("LawnEngine starting up...");

	this->windowHwnd = windowHwnd;

	if (!preInitModules())
		return false;

	JobSystem::CreateInstance();

	assetManager = new AssetManager();
	assetManager->SetCommonDirectory(CD_ProjectDir, this->projectPath);
	assetManager->SetCommonDirectory(CD_AssetsDir, AssetPath(AST_ProjectPath, "Assets"));
	assetManager->SetCommonDirectory(CD_RawAssetsDir, AssetPath(AST_ProjectPath, "Raw"));
	assetManager->SetCommonDirectory(CD_TempDir, AssetPath(AST_ProjectPath, "Temp"));

	if(RenderSystem::instance().startup()==false)
		return false;

	world = new World();

	inputManager = new OISInputManager(windowHwnd);

	uiSystem = new UISystem();

	assetManager->Refresh();

	ImGUI::Startup(windowHwnd);

	if (!initModules())
		return false;

	Log::Info("LawnEngine startup done.");

	return true;
}

void LawnEngine::stepOneFrame()
{
	if (inputManager) {
		inputManager->onNextFrame();
	}

	this->tick();

	this->render();
}

void LawnEngine::shutdown()
{
	Log::Info("LawnEngine shutting down...");

	for(std::list<LawnEnginePlugin*>::iterator it=modules.begin(); it!=modules.end(); it++)
		(*it)->destroy();

	ImGUI::Shutdown();

	SAFE_DELETE(uiSystem);
	
	SAFE_DELETE(world);

	SAFE_DELETE(inputManager);

	clearRenderViews();

	RenderSystem::instance().shutdown();

	SAFE_DELETE(assetManager);

	JobSystem::DestroyInstance();

	Log::Info("LawnEngine shutdown done.");

	//ReportLiveObjects();
}

//////////////////////////////////////////////////////////////////////////

void LawnEngine::render()
{
	if (world)
	{
		world->EditorDrawGizmo();
		world->PreRenderUpdate();
		world->UpdateRenderScene();
	}

	for(std::list<LawnEnginePlugin*>::iterator it=modules.begin(); it!=modules.end(); it++)
	{
		(*it)->onRender();
	}

	for (auto it = renderViews.begin(); it != renderViews.end(); ++it)
	{
		RenderViewPtr renderView = (*it);
		renderView->render();
	}
}

void LawnEngine::tick()
{
	double oldTime = this->currentTime;
	this->currentTime = vcpp::getProfilerTime() * 0.001;

	double elapsedTime = this->currentTime - oldTime;

	//calculate fps
	{
		oneSecframeCount++;
		oneSecondStep += elapsedTime;
		if(oneSecondStep>=1.0f)
		{
			this->fps = (float)(oneSecframeCount / oneSecondStep);
			oneSecframeCount = 0;
			oneSecondStep = 0.0;
		}

		currentFrame++;
	}

	this->elapsedTime = (float)elapsedTime;

	for(std::list<LawnEnginePlugin*>::iterator it=modules.begin(); it!=modules.end(); it++)
		(*it)->onUpdateScene(this->elapsedTime);

	this->triggerUpdateEvent();

	if (world)
	{
		world->Tick();
	}
}

//////////////////////////////////////////////////////////////////////////
// RenderView management

RenderViewPtr LawnEngine::createRenderView(const RenderViewDesc& desc)
{
	RenderViewPtr p(new RenderView(desc));

	//TODO: handle multiple-view input
	inputManager->onWindowResized(desc.width, desc.height);

	renderViews.push_back(p);

	return p;
}

void LawnEngine::removeRenderView(RenderViewPtr p)
{
	std::remove(renderViews.begin(), renderViews.end(), p);
}

void LawnEngine::clearRenderViews()
{
	renderViews.clear();
}

RenderViewPtr LawnEngine::getRenderView(HWND hwnd)
{
	RenderViewPtr targetRenderView;

	for (auto it = renderViews.begin(); it != renderViews.end(); ++it)
	{
		RenderViewPtr rv = (*it);
		if (rv->WindowHandle() == hwnd)
		{
			targetRenderView = rv;
			break;
		}
	}

	return targetRenderView;
}

bool LawnEngine::resizeRenderView(HWND hwnd, uint newWidth, uint newHeight)
{
	RenderViewPtr targetRenderView = getRenderView(hwnd);

	if (targetRenderView)
	{
		targetRenderView->resize(newWidth, newHeight);

		//TODO: handle multiple-view input
		inputManager->onWindowResized(newWidth, newHeight);

		return true;
	}
	else
	{
		return false;
	}
}

void LawnEngine::resizeRenderView(RenderViewPtr p, uint newWidth, uint newHeight)
{
	p->resize(newWidth, newHeight);
}

//////////////////////////////////////////////////////////////////////////

void LawnEngine::setFullScreenMode(bool newmode)
{
	this->fullScreenMode = newmode;
}
