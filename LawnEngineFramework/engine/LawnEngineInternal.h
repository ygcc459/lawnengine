#pragma once

//this header is for the engine internal to use, not for the app to use

#include "engine.h"

#include "EngineConfig.h"

//////////////////////////////////////////////////////////////////////////

#include <vcpp/StdioFile.h>

#include <vcpp/FileSystem.h>

//////////////////////////////////////////////////////////////////////////

//#pragma comment(lib, "winmm.lib")

//#pragma comment(lib, "d3dx9dt.lib")
//#pragma comment(lib, "d3d9.lib")

// #pragma comment(lib, "d3d9.lib")
// #pragma comment(lib, "d3dx9.lib")
// #pragma comment(lib, "d3dx9d.lib")

//#pragma comment(lib, "d3dx9.lib")
// #pragma comment(lib, "dxgi.lib")
// #pragma comment(lib, "d3d10.lib")
// #pragma comment(lib, "d3dx10.lib")
// #pragma comment(lib, "dxerr.lib")
// #pragma comment(lib, "dxguid.lib")

// #if defined(_DEBUG) || defined(DEBUG)
// #pragma comment(lib, "jsoncpp_Debug.lib")
// #else
// #pragma comment(lib, "jsoncpp_Release.lib")
// #endif

#pragma warning(disable:4503)  //warning C4503: ... too long name ...
#pragma warning(disable:4482)  //warning C4482: 使用了非标准扩展: 限定名中使用了枚举
