#pragma once

struct PODValueContainer
{
	struct PODItem
	{
		int offset;
		int byteSize;

		PODItem() {}
		PODItem(int offset, int byteSize) : offset(offset), byteSize(byteSize) {}
	};

	typedef std::string KeyType;
	typedef std::map<KeyType, PODItem> ItemMapType;

	std::vector<uint8> buffer;
	ItemMapType items;

	void Clear();

	void* Set(const KeyType& key, const void* data, int byteSize);

	void* Get(const KeyType& key, int* outByteSize = nullptr);

	const void* Get(const KeyType& key, int* outByteSize = nullptr) const;

private:
	int AllocateBuffer(int byteSize);
};
