#pragma once

#include "LawnEnginePlugin.h"

#include "UpdateEvent.h"

#include <list>

class InputManager;
class AssetManager;
class World;
class OISInputManager;
class UISystem;

struct RenderViewDesc;

class RenderView;
typedef std::shared_ptr<RenderView> RenderViewPtr;

class LawnEngine : public UpdateEventSource
{
public:
	LawnEngine(void);
	~LawnEngine(void);

public:
	bool startup(HWND windowHwnd);
	void stepOneFrame();
	void shutdown();

	void registerPlugin(LawnEnginePlugin* pModule);
	void unregisterPlugin(LawnEnginePlugin* pModule);
	bool isPluginRegistered(const LawnEnginePlugin* pModule) const;

	RenderViewPtr createRenderView(const RenderViewDesc& desc);
	void removeRenderView(RenderViewPtr p);
	void resizeRenderView(RenderViewPtr p, uint newWidth, uint newHeight);
	bool resizeRenderView(HWND hwnd, uint newWidth, uint newHeight);
	void clearRenderViews();
	RenderViewPtr getRenderView(HWND hwnd);
	std::list<RenderViewPtr>& getRenderViews() { return renderViews; }

	void setPaused(bool paused) {this->paused = paused;}
	bool isPaused() const {return paused;}

	bool isFullScreenMode() const {return fullScreenMode;}
	void setFullScreenMode(bool b);

	void setProjectPath(const AssetPath& v) { projectPath = v; }
	AssetPath getProjectPath() const { return projectPath; }

	uint32_t get_current_frame() const {
		return currentFrame;
	}

	float get_frame_elapsed_time() const {
		return elapsedTime;
	}

	double get_current_time() const {
		return currentTime;
	}

	float get_fps() const {
		return fps;
	}

	InputManager& getInputManager()
	{
		return *inputManager;
	}

	World& getWorld()
	{
		return *world;
	}

	AssetManager& getAssetManager()
	{
		return *assetManager;
	}

	UISystem& getUISystem()
	{
		return *uiSystem;
	}

	float fps;

protected:
	bool paused;

	uint32_t currentFrame;
	float elapsedTime;
	double currentTime;  //in seconds

	int oneSecframeCount;
	double oneSecondStep;

	HWND windowHwnd;

	std::list<LawnEnginePlugin*> modules;

	std::list<RenderViewPtr> renderViews;

	bool fullScreenMode;

	bool modulePreInitFinished;
	bool moduleInitFinished;

	AssetPath projectPath;

	InputManager* inputManager;
	World* world;
	AssetManager* assetManager;
	UISystem* uiSystem;

protected:
	void render();
	void tick();

	bool preInitModules();
	bool initModules();
	
};
