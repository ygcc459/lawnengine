#pragma once

//this header is for the app to use, not for engine internal use

#include <cstdlib>
#include <cmath>
#include <cassert>
#include <new>

#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include <memory>

#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

//#pragma warning(push)
//#pragma warning(disable:4005)
//#include <d3d11.h>
//#pragma warning(pop)

//////////////////////////////////////////////////////////////////////////
// boost

//#pragma warning(push)
//#pragma warning(disable:4819)  //warning C4819: 该文件包含不能在当前代码页(936)中表示的字符。请将该文件保存为 Unicode 格式以防止数据丢失
//
//#include <boost/shared_ptr.hpp>
//#include <boost/enable_shared_from_this.hpp>
//#include <boost/weak_ptr.hpp>
//
//#pragma warning(pop)

//////////////////////////////////////////////////////////////////////////
// vcpp
#include <vcpp/math.h>
#include <vcpp/la.h>

using namespace vcpp;
using namespace vcpp::la;
using namespace vcpp::la::lh;

#include <vcpp/platform.h>
#include <vcpp/basic_def.h>
#include <vcpp/util.h>
#include <vcpp/signal.h>

//////////////////////////////////////////////////////
// lawn engine forward declarations

class BaseObject;

class ReadStream;
class WriteStream;
class FileReadStream;
class FileWriteStream;
class MemoryReadStream;
class MemoryWriteStream;

class LawnEngine;
class LawnEnginePlugin;

class RenderableCollector;

class Camera;

class Shader;
class ShaderPass;
class ShaderTechnique;

class ShaderParamValues;
class GenericShaderParamValues;
class ShaderParamValueStack;

class Material;

class Mesh;

class Light;
class PointLight;
class DirectionalLight;
class SpotLight;

class Texture;
class Texture2D;
class TextureCube;

class Sampler;

class RenderView;
class Viewport;
class ColorRenderTarget;
class DepthStencilRenderTarget;
class RenderPass;
class RenderPipeline;

class SceneRenderPass;

//////////////////////////////////////////////////////
// lawn engine globals

extern LawnEngine g_lawnEngine;

//////////////////////////////////////////////////////
// lawn engine include files

#pragma warning(push)
#pragma warning(disable:4482)  //warning C4482: 使用了非标准扩展: 限定名中使用了枚举

//////////////////////////////////////////////
// base architecture

#define IN
#define OUT
#define INOUT

#ifndef SAFE_DELETE
#	define SAFE_DELETE(p)       { if (p) { delete (p);     (p)=NULL; } }
#endif    
#ifndef SAFE_DELETE_ARRAY
#	define SAFE_DELETE_ARRAY(p) { if (p) { delete[] (p);   (p)=NULL; } }
#endif    
#ifndef SAFE_RELEASE
#	define SAFE_RELEASE(p)      { if (p) { (p)->Release(); (p)=NULL; } }
#endif

#define Max(a, b) (((a) > (b)) ? (a) : (b))
#define Min(a, b) (((a) < (b)) ? (a) : (b))

#define PI (3.141592654f)

#include <stdarg.h>
#include <tchar.h>
#include <vector>
#include <string>
#include <map>
#include <functional>

typedef float RadianAngle;
typedef float DegreeAngle;

template <typename Base, typename Derived>
struct is_base {
	constexpr static bool check(Base*) { return true; }
	constexpr static bool check(...) { return false; }
	enum { value = check(static_cast<Derived*>(0)) };
};

template<typename T>
struct is_pointer { static const bool value = false; };

template<typename T>
struct is_pointer<T*> { static const bool value = true; };

template<typename T>
struct remove_pointer_type { typedef T type; };

template<typename T>
struct remove_pointer_type<T*> { typedef T type; };

template<typename T>
struct dereference_if_pointer
{
	static T& convert(T& v) { return v; }
	static const T& convert(const T& v) { return v; }
};

template<typename T>
struct dereference_if_pointer<T*>
{
	static T& convert(T* v) { return *v; }
	static const T& convert(const T* v) { return *v; }
};

template<class TClass, class TMember>
inline size_t GetMemberOffset(TMember TClass::* p)
{
	return (size_t)&(((TClass*)0)->*p);
}

#define HASH_ROTL32(x, r) (x << r) | (x >> (32 - r))

template<std::size_t Bits>
struct hash_combine
{
	template <typename SizeT>
	inline static SizeT combine(SizeT seed, SizeT value)
	{
		seed ^= value + 0x9e3779b9 + (seed << 6) + (seed >> 2);
		return seed;
	}
};

template<>
struct hash_combine<32>
{
	inline static uint32 combine(uint32 h1, uint32 k1)
	{
		const uint32 c1 = 0xcc9e2d51;
		const uint32 c2 = 0x1b873593;

		k1 *= c1;
		k1 = HASH_ROTL32(k1, 15);
		k1 *= c2;

		h1 ^= k1;
		h1 = HASH_ROTL32(h1, 13);
		h1 = h1 * 5 + 0xe6546b64;

		return h1;
	}
};

template<>
struct hash_combine<64>
{
	inline static uint64 combine(uint64 h, uint64 k)
	{
		const uint64 m = (uint64(0xc6a4a793) << 32) + 0x5bd1e995;
		const int r = 47;

		k *= m;
		k ^= k >> r;
		k *= m;

		h ^= k;
		h *= m;

		// Completely arbitrary number, to prevent 0's
		// from hashing to 0.
		h += 0xe6546b64;

		return h;
	}
};

// engine base
#include "engineConstants.h"
#include "Log.h"

#if VCPP_WINDOWS
	#define PLATFORM_DEBUG_BREAK() __debugbreak()
#else
	#define PLATFORM_DEBUG_BREAK() 
#endif

#define CHECK(condition) do { if (!(condition)) { Log::Error("Assertion check failed: %s", #condition); PLATFORM_DEBUG_BREAK(); } } while(0)

#include "FixedString.h"

#include "AssetSystem/FileSystem.h"

#include "Memory.h"

#include "StringUtil.h"

#include "Reflection/Reflection.h"

#include "BaseObject.h"

#include "AssetSystem/AssetPath.h"

#include "Common.h"

#include "LawnEngine.h"

//resource management
#include "AssetSystem/AssetManager.h"

#include "Console.h"

#pragma warning(pop)
