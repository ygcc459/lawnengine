#pragma once

#include "UICommon.h"
#include "EntitySystem/Component.h"
#include "RenderSystem/RenderSystemCommon.h"
#include "InputSystem/InputManager.h"
#include "UILayout.h"

struct UIComponentCacheData;
struct UILayout;

//
// container.width = max{chilren.width} + offset
// 
// container.height = sum{chilren.height} + offset
// 
// container.left = lerp(canvas.left, canvas.right, percent) + offset
// container.right = container.left + container.width
// 
// child[i].left = container.left + offset
// 
// container.top = canvas.top + offset
// container.bottom = container.top + container.height
// 
// child[0].top = container.top + offset
// child[1].top = child[0].bottom + offset
//

struct UILayoutItemDesc
{
	UIComponent* referenceComponent;

	UILayoutInstructionOp op;

	UILayoutDimension dimension1;
	UILayoutDimension dimension2;
	UILayoutDimension dimension3;

	float value1;
	float value2;

	UILayoutItemDesc()
		: referenceComponent(nullptr)
	{
		Nop();
	}

	void Nop()
	{
		op = UILayoutInstructionOp::Nop;
		referenceComponent = nullptr;
		dimension1 = UILayoutDimension::_Count;
		dimension2 = UILayoutDimension::_Count;
		dimension3 = UILayoutDimension::_Count;
		value1 = 0;
		value2 = 0;
	}

	void Assign(float value)
	{
		op = UILayoutInstructionOp::Assign;
		referenceComponent = nullptr;
		dimension1 = UILayoutDimension::_Count;
		dimension2 = UILayoutDimension::_Count;
		dimension3 = UILayoutDimension::_Count;
		value1 = value;
		value2 = 0;
	}

	void Add(UIComponent* op1Component, UILayoutDimension op1Dim, float op2)
	{
		op = UILayoutInstructionOp::Add;
		referenceComponent = op1Component;
		dimension1 = op1Dim;
		dimension2 = UILayoutDimension::_Count;
		dimension3 = UILayoutDimension::_Count;
		value1 = op2;
		value2 = 0;
	}

	void Lerp(UIComponent* opComponent, UILayoutDimension op1Dim, UILayoutDimension op2Dim, float lerpWeight, float bias)
	{
		op = UILayoutInstructionOp::Lerp;
		referenceComponent = opComponent;
		dimension1 = op1Dim;
		dimension2 = op2Dim;
		dimension3 = UILayoutDimension::_Count;
		value1 = lerpWeight;
		value2 = bias;
	}

	void SubtractMultiplyAdd(UIComponent* subOpComponent, UILayoutDimension subOp1Dim, UILayoutDimension subOp2Dim, float multiplyValue, UILayoutDimension addSelfDim, float addValue)
	{
		op = UILayoutInstructionOp::Lerp;
		referenceComponent = subOpComponent;
		dimension1 = subOp1Dim;
		dimension2 = subOp2Dim;
		dimension3 = addSelfDim;
		value1 = multiplyValue;
		value2 = addValue;
	}
};

class UIComponent : public Component
{
	DECL_ABSTRACT_CLASS_TYPE(UIComponent, Component);

protected:
	UILayoutItemDesc layoutItemDescs[(int)UILayoutDimension::_Count];

	edge4f edgeRelativePositions;
	edge4f edgePaddings;
	
	edge4f rect;  //final rect, in pixels
	float2 requiredSize;  //final required size, in pixels
	int32 depth;  //UI-depth

	float2 layoutMinSize;
	float2 layoutFillWeight;
	bool minSizeFollowsChildren;

	bool isVisible;

	int32 layoutComponentId;

	UICanvas* canvas;

public:
	UIComponent();
	virtual ~UIComponent();

	virtual void Reflect(Reflector& reflector) override;

	virtual void OnStartup() override;
	virtual void OnShutdown() override;

	UILayoutItemDesc& Layout(UILayoutDimension dim) { return layoutItemDescs[(int)dim]; }

	void SetAbsoluteLayout(edge4f values);
	void SetRelativePositionsAndPaddings(const edge4f& edgeRelativePositions, const edge4f& edgePaddings);

	void SetXAsLeftAlignedFixedSize(float xmin, float width);
	void SetXAsRightAlignedFixedSize(float paddingRight, float width);
	void SetXAsFilledWithPadding(float paddingLeft, float paddingRight);

	void SetYAsTopAlignedFixedSize(float ymin, float height);
	void SetYAsFilledWithPadding(float paddingTop, float paddingBottom);

	void SetLayoutMinSize(const float2& v);
	float2 GetLayoutMinSize() const { return layoutMinSize; }

	void SetLayoutFillWeight(const float2& v);
	float2 GetLayoutFillWeight() const { return layoutFillWeight; }

	//
	// hiearachy
	//	
	template<class T>
	T* AddUIChild(const std::string& name = "UISceneObject")
	{
		Type* type = &T::StaticType;
		return (T*)AddUIChild(type, name);
	}
	
	UIComponent* AddUIChild(Type* type, const std::string& name = "UISceneObject");

	//
	// layout
	//
	void MarkLayoutDirty();

	edge4f GetRect() const { return rect; }
	void SetRect(const edge4f& rect) { this->rect = rect; }

	int32 GetDepth() const { return depth; }

	virtual void UpdateSelfRect(const edge4f& parentRect);

	virtual void UpdateRequiredSize(UIComponentCacheData& cacheData);
	float2 GetRequiredSize() const { return requiredSize; }

	virtual void UpdateRect(UIComponentCacheData& cacheData, const edge4f& parentRect);

	virtual void OnParentHierarchyChanged(const HierarchyChangedEvent& ev) override;
	void UpdateParentHierarchyDependentData();

	void MarkLayoutInstructionsDirty();
	void SetLayoutComponentId(int32 id) { layoutComponentId = id; }
	int32 GetLayoutComponentId() const { return layoutComponentId; }
	virtual void PreLayout();
	virtual void GenerateLayoutInstructions(UILayoutInstructionBuilder& builder);

	//
	// rendering
	//
	void MarkMeshDirty();

	void SetVisible(bool v);
	bool IsVisibleSelf() const { return isVisible; }
	bool IsVisibleInHierarchy() const;

	virtual bool CanDraw() { return false; }

	virtual MaterialPtr GetMaterial() { return MaterialPtr(); }

	virtual Texture2DPtr GetTexture() { return Texture2DPtr(); }

	virtual void UpdateMesh(UIMeshBatch* batch) {}

	//
	// mouse event
	//
	virtual bool BlockRaycasts() { return true; }
	virtual bool HandleMouseEvents() { return false; }

	virtual void OnMouseEvent(const MouseEvent& ev, const MouseState& state) {}

	void SetAsMouseFocus();
	void CancelMouseFocus();
	bool HasMouseFocus() const;

	//
	// key event
	//
	virtual bool HandleKeyEvents() { return false; }

	virtual void OnKeyEvent(const KeyEvent& ev, void* customData) {}

	void SetAsKeyFocus();
	void CancelKeyFocus();
	bool HasKeyFocus() const;
};
