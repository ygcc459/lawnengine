#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "UICanvas.h"
#include "UIComponent.h"
#include "EntitySystem/SceneObject.h"
#include "EntitySystem/Transform.h"
#include "EntitySystem/Camera.h"
#include "RenderSystem/Material.h"
#include "EntitySystem/ComponentRegistry.h"
#include "RenderSystem/RenderScene.h"
#include "RenderSystem/Texture2D.h"
#include "RenderSystem/RenderCommands.h"
#include "RenderSystem/RenderView.h"

#include <deque>
#include <unordered_set>

//////////////////////////////////////////////////////////////////////////

ConsoleVariable<bool> CVarCanvasDumpLayoutInstructions("UICanvas.DumpLayoutInstructions", false);

//////////////////////////////////////////////////////////////////////////

UIMeshBatch::UIMeshBatch(int depth, MaterialPtr material, Texture2DPtr texture)
	: depth(depth), material(material), texture(texture), mesh(nullptr)
{
}

UIMeshBatch::~UIMeshBatch()
{
}

int UIMeshBatch::AddVertices(int count)
{
	int index = vertices.size();
	vertices.emplace_back();
	return index;
}

int UIMeshBatch::AddQuad(const rectf& pos_rect, const rectf& uv_rect, const float4& color)
{
	return AddQuad(
		pos_rect.xmin(), pos_rect.xmax(), pos_rect.ymin(), pos_rect.ymax(),
		uv_rect.xmin(), uv_rect.xmax(), uv_rect.ymin(), uv_rect.ymax(),
		color);
}

//
// 0   1
// *---*
// |  /|
// | / |
// |/  |
// *---*
// 2   3    
//
int UIMeshBatch::AddQuad(float x1, float x2, float y1, float y2, float u1, float u2, float v1, float v2, const float4& color)
{
	color32 c = vcpp::la::to_color32(color);

	float3 poses[] = 
	{
		float3(x1, y1, 0),
		float3(x2, y1, 0),
		float3(x1, y2, 0),
		float3(x2, y2, 0),
	};

	float2 uvs[] = 
	{
		float2(u1, v1),
		float2(u2, v1),
		float2(u1, v2),
		float2(u2, v2),
	};

	int index = vertices.size();

	vertices.emplace_back(poses[0], uvs[0], c);
	vertices.emplace_back(poses[2], uvs[2], c);
	vertices.emplace_back(poses[1], uvs[1], c);

	vertices.emplace_back(poses[1], uvs[1], c);
	vertices.emplace_back(poses[2], uvs[2], c);
	vertices.emplace_back(poses[3], uvs[3], c);

	return index;
}

//
// pos_rect_outer.ymin() -> *--*--------*--*
//                          |  |        |  |
// pos_rect_inner.ymin() -> *--*--------*--*
//                          |  | center |  |
// pos_rect_inner.ymax() -> *--*--------*--*
//                          |  |        |  |
// pos_rect_outer.ymax() -> *--*--------*--*
//                          ^  ^        ^  ^
//                          |  |        |  |
//       pos_rect_outer.xmin() |        |  |
//          pos_rect_inner.xmin()       |  |
//                   pos_rect_inner.xmax() |
//                      pos_rect_outer.xmax()
// 
int UIMeshBatch::AddNinePatchQuad(
	const rectf& pos_rect_outer, const rectf& pos_rect_inner,
	const rectf& uv_rect_outer, const rectf& uv_rect_inner,
	const float4& color)
{
	int index = vertices.size();

	AddQuad(pos_rect_outer.xmin(), pos_rect_inner.xmin(), pos_rect_outer.ymin(), pos_rect_inner.ymin(), uv_rect_outer.xmin(), uv_rect_inner.xmin(), uv_rect_outer.ymin(), uv_rect_inner.ymin(), color);
	AddQuad(pos_rect_inner.xmin(), pos_rect_inner.xmax(), pos_rect_outer.ymin(), pos_rect_inner.ymin(), uv_rect_inner.xmin(), uv_rect_inner.xmax(), uv_rect_outer.ymin(), uv_rect_inner.ymin(), color);
	AddQuad(pos_rect_inner.xmax(), pos_rect_outer.xmax(), pos_rect_outer.ymin(), pos_rect_inner.ymin(), uv_rect_inner.xmax(), uv_rect_outer.xmax(), uv_rect_outer.ymin(), uv_rect_inner.ymin(), color);

	AddQuad(pos_rect_outer.xmin(), pos_rect_inner.xmin(), pos_rect_inner.ymin(), pos_rect_inner.ymax(), uv_rect_outer.xmin(), uv_rect_inner.xmin(), uv_rect_inner.ymin(), uv_rect_inner.ymax(), color);
	AddQuad(pos_rect_inner.xmin(), pos_rect_inner.xmax(), pos_rect_inner.ymin(), pos_rect_inner.ymax(), uv_rect_inner.xmin(), uv_rect_inner.xmax(), uv_rect_inner.ymin(), uv_rect_inner.ymax(), color);
	AddQuad(pos_rect_inner.xmax(), pos_rect_outer.xmax(), pos_rect_inner.ymin(), pos_rect_inner.ymax(), uv_rect_inner.xmax(), uv_rect_outer.xmax(), uv_rect_inner.ymin(), uv_rect_inner.ymax(), color);

	AddQuad(pos_rect_outer.xmin(), pos_rect_inner.xmin(), pos_rect_inner.ymax(), pos_rect_outer.ymax(), uv_rect_outer.xmin(), uv_rect_inner.xmin(), uv_rect_inner.ymax(), uv_rect_outer.ymax(), color);
	AddQuad(pos_rect_inner.xmin(), pos_rect_inner.xmax(), pos_rect_inner.ymax(), pos_rect_outer.ymax(), uv_rect_inner.xmin(), uv_rect_inner.xmax(), uv_rect_inner.ymax(), uv_rect_outer.ymax(), color);
	AddQuad(pos_rect_inner.xmax(), pos_rect_outer.xmax(), pos_rect_inner.ymax(), pos_rect_outer.ymax(), uv_rect_inner.xmax(), uv_rect_outer.xmax(), uv_rect_inner.ymax(), uv_rect_outer.ymax(), color);

	return index;
}

void UIMeshBatch::UpdateMesh()
{
	if (!mesh)
		mesh.reset(new Mesh());

	auto m = mesh->BeginModifyOnEmptyData();

	m->SetPrimitiveTopology(PT_TriangleList);

	m->SetVertexCount(vertices.size());

	int stride = sizeof(UIMeshVertex);

	int vbIndex = m->AddVertexBuffer(stride, stride * vertices.size(), vertices.data());

	m->AddVertexComponent(InputElement::IE_POSITION, vbIndex, MeshVertexFormat(MeshVertexBaseFormat::MVBF_Float, 3));
	m->AddVertexComponent(InputElement::IE_TEXCOORD0, vbIndex, MeshVertexFormat(MeshVertexBaseFormat::MVBF_Float, 2));
	m->AddVertexComponent(InputElement::IE_VERTEXCOLOR, vbIndex, MeshVertexFormat(MeshVertexBaseFormat::MVBF_Byte, 4));

	mesh->EndModify();
}

void UIMeshBatch::Clear()
{
	mesh.reset();

	vertices.clear();
}

//////////////////////////////////////////////////////////////////////////

UIGrid::UIGrid()
{
}

UIGrid::~UIGrid()
{
	Clear();
}

int2 UIGrid::GetGrid(const float2& pos) const
{
	int x = math::floor_int(pos.x / (float)gridCellSize.x);
	int y = math::floor_int(pos.y / (float)gridCellSize.y);
	return int2(x, y);
}

void UIGrid::GetClampedGridRange(const rectf& rect, int2& outMin, int2& outMax) const
{
	int2 gridPoint1 = GetGrid(rect.min_point());
	int2 gridPoint2 = GetGrid(rect.max_point());

	outMin.x = std::min(gridPoint1.x, gridPoint2.x);
	outMin.y = std::min(gridPoint1.y, gridPoint2.y);
	outMax.x = std::max(gridPoint1.x, gridPoint2.x);
	outMax.y = std::max(gridPoint1.y, gridPoint2.y);

	outMin.x = std::max(outMin.x, 0);
	outMin.y = std::max(outMin.y, 0);
	outMax.x = std::min(outMax.x, depthStackGrid.size_x() - 1);
	outMax.y = std::min(outMax.y, depthStackGrid.size_y() - 1);
}

UIComponent* UIGrid::Raycast(const float2& pos) const
{
	int2 gridPos = GetGrid(pos);

	if (gridPos.x >= 0 && gridPos.x < spatialSlicesGrid.size_x()
		&& gridPos.y >= 0 && gridPos.y < spatialSlicesGrid.size_y())
	{
		auto& spatialSlices = spatialSlicesGrid(gridPos.x, gridPos.y);

		for (int i = spatialSlices.size() - 1; i >= 0; i--)
		{
			const Slice& slice = spatialSlices[i];

			UIComponent* component = slice.component;
			if (slice.fullRect.contains(pos) && component->BlockRaycasts())
			{
				return component;
			}
		}
	}

	return nullptr;
}

void UIGrid::PushRect(const rectf& rect, int depth, UIComponent* component)
{
	int2 gridMin, gridMax;
	GetClampedGridRange(rect, gridMin, gridMax);

	for (int y = gridMin.y; y <= gridMax.y; y++)
	{
		for (int x = gridMin.x; x <= gridMax.x; x++)
		{
			auto& depthStack = depthStackGrid(x, y);
			depthStack.push(depth);

			auto& spatialSlices = spatialSlicesGrid(x, y);
			spatialSlices.push_back(Slice(rect, depth, component));
		}
	}
}

int UIGrid::GetRectDepth(const rectf& rect)
{
	int2 gridMin, gridMax;
	GetClampedGridRange(rect, gridMin, gridMax);

	int maxDepth = 0;

	for (int y = gridMin.y; y <= gridMax.y; y++)
	{
		for (int x = gridMin.x; x <= gridMax.x; x++)
		{
			auto& depthStack = depthStackGrid(x, y);
			int depth = depthStack.empty() ? 0 : depthStack.top() + 1;
			maxDepth = std::max(depth, maxDepth);
		}
	}

	return maxDepth;
}

void UIGrid::PopRect(const rectf& rect, int depth, UIComponent* component)
{
	int2 gridMin, gridMax;
	GetClampedGridRange(rect, gridMin, gridMax);

	for (int y = gridMin.y; y <= gridMax.y; y++)
	{
		for (int x = gridMin.x; x <= gridMax.x; x++)
		{
			auto& depthStack = depthStackGrid(x, y);

			CHECK(!depthStack.empty());
			CHECK(depthStack.top() == depth);

			depthStack.pop();
		}
	}
}

void UIGrid::Resize(const int2& canvasSize)
{
	this->gridCellSize = int2(200, 200);
	this->canvasSize = canvasSize;

	int numCellsX = math::ceil_int(canvasSize.x / (float)gridCellSize.x);
	int numCellsY = math::ceil_int(canvasSize.y / (float)gridCellSize.y);

	this->depthStackGrid.reset(numCellsX, numCellsY);

	this->spatialSlicesGrid.reset(numCellsX, numCellsY);
}

void UIGrid::Clear()
{
	for (int y = 0; y < depthStackGrid.size_y(); ++y)
	{
		for (int x = 0; x < depthStackGrid.size_x(); ++x)
		{
			auto& depthStack = depthStackGrid(x, y);
			CHECK(depthStack.size() == 0);

			auto& spatialSlices = spatialSlicesGrid(x, y);
			spatialSlices.clear();
		}
	}
}

//////////////////////////////////////////////////////////////////////////

UIBatcher::UIBatcher()
{
}

UIBatcher::~UIBatcher()
{
	Clear();
}

UIMeshBatch* UIBatcher::GetMeshBatch(const UIMeshBatchKey& key)
{
	auto it = meshBatchKeyMapping.find(key);
	if (it != meshBatchKeyMapping.end())
	{
		return it->second;
	}
	else
	{
		return  nullptr;
	}
}

UIMeshBatch* UIBatcher::CreateMeshBatch(const UIMeshBatchKey& key, int depth, MaterialPtr material, Texture2DPtr texture)
{
	UIMeshBatch* batch = new UIMeshBatch(depth, material, texture);

	meshBatches.push_back(batch);
	meshBatchKeyMapping.insert(std::make_pair(key, batch));

	return batch;
}

void UIBatcher::UpdateMeshes()
{
	for (int i = 0; i < meshBatches.size(); i++)
	{
		meshBatches[i]->UpdateMesh();
	}
}

void UIBatcher::Clear()
{
	for (int i = 0; i < meshBatches.size(); i++)
	{
		delete meshBatches[i];
	}
	meshBatches.clear();

	meshBatchKeyMapping.clear();
}

//////////////////////////////////////////////////////////////////////////

class UICanvasRenderSceneObject : public RenderSceneCustomPassObject
{
public:
	UICanvasRenderSceneObject(UICanvas* renderer)
		: RenderSceneCustomPassObject(CustomPassObjectType::CustomPassObjectType_UI, 0)
		, renderer(renderer)
	{
		techniqueName = "UIRenderPass";
		UITexParamName = "UITex";
		UITexSizeName = "UITexSize";
		RenderTargetSizeName = "RenderTargetSize";
	}

	virtual ~UICanvasRenderSceneObject()
	{}

	virtual void OnUpdateMeshDrawCommands(RenderSceneCustomPassObjectContext& context) override
	{
		/*
		if (!renderView->IsUICanvasRegistered(renderer))
			return;

		const std::vector<UIMeshBatch*>& meshBatches = renderer->batcher.GetSortedMeshBatches();

		if (meshBatches.size() == 0)
			return;

		int totalCount = meshBatches.size();

		int range_begin = 0;
		UIMeshBatch* range_begin_object = meshBatches[range_begin];
		Material* range_begin_material = range_begin_object->material.get();

		int cur;
		for (cur = range_begin + 1; cur != totalCount; ++cur)
		{
			UIMeshBatch* cur_object = meshBatches[cur];
			Material* cur_material = cur_object->material.get();

			if (cur_material != range_begin_material)
			{
				RenderSameMaterialObjects(range_begin_material, meshBatches, range_begin, cur, renderResourcesManager, meshDrawCommands);

				range_begin = cur;
				range_begin_material = cur_material;
			}
		}

		RenderSameMaterialObjects(range_begin_material, meshBatches, range_begin, cur, renderResourcesManager, meshDrawCommands);
		*/
	}

	void RenderSameMaterialObjects(Material* material, const std::vector<UIMeshBatch*>& meshBatches, int begin, int end, RenderResourcesManager& renderResourcesManager, std::vector<MeshDrawCommand*>& meshDrawCommands)
	{
		/*
		ShaderPtr shaderPtr = material->GetShader();
		if (!shaderPtr)
			return;

		Shader& shader = *shaderPtr;

		ShaderTechnique* technique = shader.FindTechnique(techniqueName);
		if (!technique)
			return;

		ShaderVariantKeys variantKeys;
		variantKeys.BindCollection(&technique->variantCollection);  //use default variant

		ShaderVariant* variant = technique->FindVariant(variantKeys);
		if (!variant)
			return;

		// material params
		ShaderParamValues& materialParamValues = material->GetBoundParamValues();

		ShaderParamValues perObjectParamValues;

		for (size_t iPass = 0; iPass < variant->passes.size(); ++iPass)
		{
			ShaderPass* pass = variant->passes[iPass].get();
			if (!pass->isValid)
				continue;

			for (int iBatch = begin; iBatch != end; ++iBatch)
			{
				const UIMeshBatch* object = meshBatches[iBatch];

				MeshDrawCommand* meshDrawCommand = new MeshDrawCommand();

				pass->FillMeshDrawCommand(meshDrawCommand);

				// per-object params
				Texture2DPtr texPtr = object->texture;
				if (!texPtr)
					texPtr = Texture2D::GetWhiteTexture();

				perObjectParamValues.SetResource(UITexParamName, std::static_pointer_cast<Texture>(texPtr));
				perObjectParamValues.SetFloat2(UITexSizeName, float2((float)texPtr->width, (float)texPtr->height));
				perObjectParamValues.SetFloat2(RenderTargetSizeName, float2((float)renderer->canvasSize.x, (float)renderer->canvasSize.y));

				// composite and bind to shader
				ShaderParamValueStack shaderParamValueStack(pass);
				shaderParamValueStack.Push(&materialParamValues);
				shaderParamValueStack.Push(&perObjectParamValues);
				shaderParamValueStack.FillMeshDrawCommand(renderResourcesManager, meshDrawCommand);

				object->mesh->UpdateGpuBuffers(renderResourcesManager);
				object->mesh->FillMeshDrawCommand(pass, meshDrawCommand);

				meshDrawCommand->UploadBuffers(renderResourcesManager);

				// drawcall
				meshDrawCommands.push_back(meshDrawCommand);
			}
		}
		*/
	}

public:
	ShaderParamValues objectParams;

	UICanvas* renderer;

	FixedString techniqueName;
	FixedString UITexParamName;
	FixedString UITexSizeName;
	FixedString RenderTargetSizeName;
};

//////////////////////////////////////////////////////////////////////////

ConsoleVariable<bool> CVarCanvasForceUpdateEveryframe("Canvas.ForceUpdateEveryframe", true);

IMPL_CLASS_TYPE(UICanvas);

UICanvas::UICanvas()
	: canvasSize(0, 0)
	, layoutDirty(true)
	, meshDirty(true)
	, mouseFocus(nullptr)
	, keyFocus(nullptr)
	, renderSceneObject(nullptr)
	, layoutInstructionsDirty(true)
	, pointerDraggingComponent(nullptr)
{
}

UICanvas::~UICanvas()
{
}

void UICanvas::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);
}

void UICanvas::OnStartup()
{
	g_lawnEngine.getInputManager().addMouseEventListener(this);

	SetUpdateEnabled(true);

	if (GetRenderScene())
	{
		SetPendingUpdateRenderScene();
	}
}

void UICanvas::OnShutdown()
{
	g_lawnEngine.getInputManager().removeMouseEventListener(this);
}

void UICanvas::OnChildHierarchyChanged(const HierarchyChangedEvent& ev)
{
	MarkLayoutDirty();
}

void UICanvas::RegisterPreLayoutComponent(UIComponent* comp)
{
	preLayoutUiComponents.insert(comp);
}

void UICanvas::UnRegisterPreLayoutComponent(UIComponent* comp)
{
	preLayoutUiComponents.erase(comp);
}

UIComponent* UICanvas::AddUIChild(Type* type, const std::string& name)
{
	auto& so = GetSceneObject();
	
	auto* childSO = so.AddChild(name);
	Component* comp = childSO->AddComponent(type);

	return Util::CastPtrWithCheck<UIComponent>(comp);
}

void UICanvas::RegisterComponent(UIComponent* comp)
{
	uiComponents.insert(comp);
}

void UICanvas::UnRegisterComponent(UIComponent* comp)
{
	uiComponents.erase(comp);
}

void UICanvas::Resize(const int2& canvasSize)
{
	this->canvasSize = canvasSize;
	this->grid.Resize(canvasSize);
}

void UICanvas::OnUpdate()
{
	if (CVarCanvasForceUpdateEveryframe.GetValue())
	{
		layoutDirty = true;
		meshDirty = true;
	}

	if (layoutDirty)
	{
		UpdateLayout();
	}

	if (meshDirty)
	{
		UpdateMesh();
	}
}

void UICanvas::BuildCacheDatas()
{
	componentCacheDatas.clear();

	BuildCacheDatasRecursive(-1);

	componentCacheDatasRootCount = 0;
	for (size_t i = 0; i < componentCacheDatas.size(); i++)
	{
		if (componentCacheDatas[i].parentIndex >= 0)
		{
			componentCacheDatasRootCount = i;
			break;
		}
	}
}

void UICanvas::BuildCacheDatasRecursive(int32 curDataIndex)
{
	SceneObject* so;
	if (curDataIndex >= 0)
	{
		// UIComponentCacheData reference is put in this scope, to remind us that, componentCacheDatas.push_back will invalidate it
		UIComponentCacheData& curData = componentCacheDatas[curDataIndex];
		so = &curData.component->GetSceneObject();
	}
	else
	{
		so = &this->GetSceneObject();
	}

	//
	// 1. push child nodes to array
	//
	int32 childrenIndexBegin = (int32)componentCacheDatas.size();
	int32 childrenCount = 0;

	for (Transform* childTransform : so->GetTransform().children)
	{
		SceneObject& childSceneObject = childTransform->GetSceneObject();

		UIComponent* childComp = childSceneObject.GetComponent<UIComponent>();
		if (childComp && childComp->IsVisibleInHierarchy())
		{
			int32 childDataIndex = (int32)componentCacheDatas.size();

			childrenCount++;
			componentCacheDatas.push_back(UIComponentCacheData());

			auto& childData = componentCacheDatas[childDataIndex];
			childData.component = childComp;
			childData.parentIndex = curDataIndex;
		}
	}

	if (curDataIndex >= 0)
	{
		// UIComponentCacheData reference is put in this scope, to remind us that, componentCacheDatas.push_back will invalidate it
		UIComponentCacheData& curData = componentCacheDatas[curDataIndex];
		curData.childrenIndexBegin = childrenIndexBegin;
		curData.childrenCount = childrenCount;
	}

	//
	// 2. visit child nodes
	//
	for (int32 iChild = 0; iChild < childrenCount; iChild++)
	{
		int32 childDataIndex = iChild + childrenIndexBegin;
		BuildCacheDatasRecursive(childDataIndex);
	}
}

void UICanvas::UpdateLayout()
{
	for (auto it = preLayoutUiComponents.begin(); it != preLayoutUiComponents.end(); ++it)
	{
		UIComponent* comp = *it;
		comp->PreLayout();
	}

	UpdateLayoutInstructions();

	if (CVarCanvasDumpLayoutInstructions.GetValue())
		DumpLayoutInstructions();

	ExecuteLayoutInstructions();
	UpdateLayoutResultsToComponents();

/*
	BuildCacheDatas();

	rectf rootRect = rectf(0.0f, 0.0f, this->canvasSize.x, this->canvasSize.y);

	for (int32 iRoot = 0; iRoot < componentCacheDatasRootCount; iRoot++)
	{
		UIComponentCacheData& cacheData = componentCacheDatas[iRoot];

		UIComponent* uiComponent = cacheData.GetComponent();

		uiComponent->UpdateRequiredSize(cacheData);
	}

	for (int32 iRoot = 0; iRoot < componentCacheDatasRootCount; iRoot++)
	{
		UIComponentCacheData& cacheData = componentCacheDatas[iRoot];

		UIComponent* uiComponent = cacheData.GetComponent();

		uiComponent->UpdateRect(cacheData, rootRect);
	}

	UpdateGrid();
//*/


	layoutDirty = false;
	meshDirty = true;
}

void UICanvas::UpdateGrid()
{
	grid.Clear();
	componentsWithDepth.clear();

	for (int32 iRoot = 0; iRoot < componentCacheDatasRootCount; iRoot++)
	{
		UIComponentCacheData& cacheData = componentCacheDatas[iRoot];

		UpdateGridRecursive(cacheData);
	}
}

void UICanvas::UpdateGridRecursive(UIComponentCacheData& cacheData)
{
	UIComponent* uiComponent = cacheData.GetComponent();

	rectf rect = uiComponent->GetRect().to_rect();

	int depth = grid.GetRectDepth(rect);
	if (uiComponent->CanDraw())
	{
		componentsWithDepth.push_back(ComponentWithDepth(depth, uiComponent));
	}

	grid.PushRect(rect, depth, uiComponent);
	{
		for (int iChild = 0; iChild < cacheData.ChildCount(); iChild++)
		{
			UIComponentCacheData& childCacheData = cacheData.GetChild(*this, iChild);

			UpdateGridRecursive(childCacheData);
		}
	}
	grid.PopRect(rect, depth, uiComponent);
}

void UICanvas::UpdateMesh()
{
	batcher.Clear();

	for (auto it = sortedUiComponents.begin(); it != sortedUiComponents.end(); ++it)
	{
		UIComponent* component = *it;
		int32 depth = component->GetDepth();

		MaterialPtr material = component->GetMaterial();
		Texture2DPtr texture = component->GetTexture();
		if (material && texture)
		{
			//
			// gen mesh for this component
			//
			UIMeshBatchKey key(depth, material.get(), texture.get());

			UIMeshBatch* batch = batcher.GetMeshBatch(key);
			if (!batch)
				batch = batcher.CreateMeshBatch(key, depth, material, texture);

			if (batch)
			{
				component->UpdateMesh(batch);
			}
		}
	}

/*
	for (auto& componentWithDepth : componentsWithDepth)
	{
		UIComponent* component = componentWithDepth.component;
		int depth = componentWithDepth.depth;

		MaterialPtr material = component->GetMaterial();
		Texture2DPtr texture = component->GetTexture();
		if (material && texture)
		{
			//
			// gen mesh for this component
			//
			UIMeshBatchKey key(depth, material.get(), texture.get());

			UIMeshBatch* batch = batcher.GetMeshBatch(key);
			if (!batch)
				batch = batcher.CreateMeshBatch(key, depth, material, texture);

			if (batch)
			{
				component->UpdateMesh(batch);
			}
		}
	}
//*/
	batcher.UpdateMeshes();

	meshDirty = false;
}

float UICanvas::GetMouseRaycastPriority()
{
	return 1;
}

BaseObject* UICanvas::OnMouseRaycast(const MouseEvent& ev, const MouseState& state)
{
	if (layoutDirty)
	{
		UpdateLayout();
	}

	float2 pos = state.PositionXY();

	std::vector<UIComponent*> visitStack;
	
	for (int iChild = 0; iChild < GetSceneObject().GetChildCount(); iChild++)
	{
		UIComponent* uiComponent = GetSceneObject().GetChild(iChild)->GetComponent<UIComponent>();
		if (uiComponent)
			visitStack.push_back(uiComponent);
	}

	while (!visitStack.empty())
	{
		UIComponent* cur = visitStack.back();
		visitStack.pop_back();

		if (cur->GetRect().contains(pos))
		{
			bool handled = cur->HandleMouseEvents();
			if (handled)
			{
				return cur;
			}

			for (int iChild = 0; iChild < cur->GetSceneObject().GetChildCount(); iChild++)
			{
				UIComponent* child = cur->GetSceneObject().GetChild(iChild)->GetComponent<UIComponent>();
				if (child)
					visitStack.push_back(child);
			}
		}
	}

#if 0
	UIComponent* component = grid.Raycast(pos);
	while (component)
	{
		if (component->HandleMouseEvents())
		{
			return component;
		}
		else
		{
			SceneObject* parentSO = component->GetSceneObject().GetParent();
			component = parentSO ? parentSO->GetComponentInParents<UIComponent>() : nullptr;
		}
	}
#endif

	return nullptr;
}

void UICanvas::OnMouseEvent(BaseObject* raycastResult, const MouseEvent& ev, const MouseState& state)
{
	UIComponent* component = (UIComponent*)raycastResult;
	if (component)
	{
		component->OnMouseEvent(ev, state);
	}
}

void UICanvas::SetMouseFocus(UIComponent* component)
{
	if (component)
	{
		mouseFocus = component;
		g_lawnEngine.getInputManager().setMouseFocus(this, component);
	}
	else
	{
		mouseFocus = nullptr;
		g_lawnEngine.getInputManager().setMouseFocus(nullptr, nullptr);
	}
}

UIComponent* UICanvas::GetMouseFocus()
{
	return this->mouseFocus;
}

void UICanvas::SetKeyFocus(UIComponent* component)
{
	if (component)
	{
		keyFocus = component;
		g_lawnEngine.getInputManager().setKeyFocus(this, component);
	}
	else
	{
		keyFocus = nullptr;
		g_lawnEngine.getInputManager().setKeyFocus(nullptr, nullptr);
	}
}

UIComponent* UICanvas::GetKeyFocus()
{
	return this->keyFocus;
}

void UICanvas::OnKeyEvent(const KeyEvent& ev, void* customData)
{
	UIComponent* component = (UIComponent*)customData;
	component->OnKeyEvent(ev, customData);
}

void UICanvas::CreateRenderSceneObject()
{
	CHECK(renderSceneObject == nullptr);

	RenderScene* renderScene = GetRenderScene();

	if (renderScene)
	{
		renderSceneObject = new UICanvasRenderSceneObject(this);

		renderScene->AddCustomPassObject(renderSceneObject);
	}
}

void UICanvas::DestroyRenderSceneObject()
{
	RenderScene* renderScene = GetRenderScene();

	if (renderSceneObject != nullptr)
	{
		if (renderScene)
			renderScene->RemoveCustomPassObject(renderSceneObject);

		SAFE_DELETE(renderSceneObject);
	}
}

void UICanvas::UpdateLayoutInstructions()
{
	sortedUiComponents.reserve(uiComponents.size());
	sortedUiComponents.clear();

	int32 canvasRootComponentId = UILayoutInstructionBuilder::CanvasRootComponentId;

	int32 nextLayoutComponentId = canvasRootComponentId + 1;
	
	//
	// gather ui-components & assign layoutComponentId
	//
	for (int iChild = 0; iChild < GetSceneObject().GetChildCount(); iChild++)
	{
		UIComponent* uiComponent = GetSceneObject().GetChild(iChild)->GetComponent<UIComponent>();
		if (uiComponent)
			uiComponentsVisitQueue.push_back(uiComponent);
	}

	while (!uiComponentsVisitQueue.empty())
	{
		UIComponent* cur = uiComponentsVisitQueue.front();
		uiComponentsVisitQueue.pop_front();
		
		cur->SetLayoutComponentId(nextLayoutComponentId++);
		sortedUiComponents.push_back(cur);

		Transform& curTransform = cur->GetTransform();
		for (int iChild = 0; iChild < curTransform.children.size(); iChild++)
		{
			UIComponent* child = curTransform.children[iChild]->GetSceneObject().GetComponent<UIComponent>();
			if (child)
				uiComponentsVisitQueue.push_back(child);
		}
	}

	uiComponentsVisitQueue.clear();

	//
	// generate layout instructions
	//
	uint32 layoutItemCount = (1 + (uint32)sortedUiComponents.size()) * (uint32)UILayoutDimension::_Count;  //add one for setting canvas size

	instructionBuilder.canvas = this;
	instructionBuilder.layoutItemToInstructions.clear();
	instructionBuilder.layoutItemToInstructions.resize(layoutItemCount);

	// layout instructions for root canvas
	{
		instructionBuilder.Assign(canvasRootComponentId, UILayoutDimension::Left, 0);
		instructionBuilder.Assign(canvasRootComponentId, UILayoutDimension::Top, 0);
		instructionBuilder.Assign(canvasRootComponentId, UILayoutDimension::Right, (float)canvasSize.x);
		instructionBuilder.Assign(canvasRootComponentId, UILayoutDimension::Bottom, (float)canvasSize.y);
	}

	// layout instructions for UI components
	for (auto it = sortedUiComponents.begin(); it != sortedUiComponents.end(); ++it)
	{
		UIComponent* comp = *it;
		comp->GenerateLayoutInstructions(instructionBuilder);
	}
	
	//
	// sort instructions by dependency
	//
	layoutItemVisitStack.clear();
	for (int32 i = layoutItemCount - 1; i >= 0; i--)  //small-depth components gets calculated first, because most components will depend only on its parent
	{
		layoutItemVisitStack.push_back(i);  //layoutItem is encoded in a continus sequence
	}

	layoutItemsResolved.resize_uninitialized(layoutItemCount);
	layoutItemsResolved.set_all(false);

	sortedInstructions.clear();
	sortedInstructions.reserve(layoutItemCount);

	layoutItemSortedIndex.clear();
	layoutItemSortedIndex.resize(layoutItemCount);

	while (!layoutItemVisitStack.empty())
	{
		uint64 layoutItem = layoutItemVisitStack.back();

		if (layoutItemVisitStack.size() > 10000)
		{
			Log::Error("cyclic dependency??");
		}

		if (layoutItemsResolved.get(layoutItem))
		{
			layoutItemVisitStack.pop_back();
			continue;
		}

		bool dependencyResolved = true;

		UILayoutItemInstruction& ins = instructionBuilder.layoutItemToInstructions[layoutItem];

		if (ins.layoutItem1 != UILayoutInstructionBuilder::InvalidLayoutItem)
		{
			auto dependentItem = ins.layoutItem1;

			if (!layoutItemsResolved.get(dependentItem))
			{
				dependencyResolved = false;

				layoutItemVisitStack.push_back(dependentItem);
			}
		}

		if (ins.layoutItem2 != UILayoutInstructionBuilder::InvalidLayoutItem)
		{
			auto dependentItem = ins.layoutItem2;

			if (!layoutItemsResolved.get(dependentItem))
			{
				dependencyResolved = false;

				layoutItemVisitStack.push_back(dependentItem);
			}
		}

		if (ins.layoutItem3 != UILayoutInstructionBuilder::InvalidLayoutItem)
		{
			auto dependentItem = ins.layoutItem3;

			if (!layoutItemsResolved.get(dependentItem))
			{
				dependencyResolved = false;

				layoutItemVisitStack.push_back(dependentItem);
			}
		}

		if (dependencyResolved)
		{
			layoutItemsResolved.set(layoutItem, true);
			layoutItemVisitStack.pop_back();

			uint32 sortedIndex = (uint32)sortedInstructions.size();
			sortedInstructions.push_back(ins);
			layoutItemSortedIndex[layoutItem] = sortedIndex;
		}
	}
}

void UICanvas::DumpLayoutInstructions()
{
	Log::Info("UICanvas::DumpLayoutInstructions: %s", this->GetWorldPath().c_str());

	const std::vector<UILayoutItemInstruction>& instructions = sortedInstructions;
	const std::vector<float>& values = sortedValues;

	std::map<uint32, std::pair<UIComponent*, UILayoutDimension>> layoutItemReverseMapping;
	
	{
		int32 canvasRootComponentId = UILayoutInstructionBuilder::CanvasRootComponentId;

		uint32 layoutItem;
		uint32 sortedIndex;

		layoutItem = UILayoutInstructionBuilder::LayoutItem(canvasRootComponentId, UILayoutDimension::Left);
		sortedIndex = layoutItemSortedIndex[layoutItem];
		layoutItemReverseMapping.insert(std::make_pair(sortedIndex, std::make_pair(nullptr, UILayoutDimension::Left)));
		
		layoutItem = UILayoutInstructionBuilder::LayoutItem(canvasRootComponentId, UILayoutDimension::Top);
		sortedIndex = layoutItemSortedIndex[layoutItem];
		layoutItemReverseMapping.insert(std::make_pair(sortedIndex, std::make_pair(nullptr, UILayoutDimension::Top)));
		
		layoutItem = UILayoutInstructionBuilder::LayoutItem(canvasRootComponentId, UILayoutDimension::Right);
		sortedIndex = layoutItemSortedIndex[layoutItem];
		layoutItemReverseMapping.insert(std::make_pair(sortedIndex, std::make_pair(nullptr, UILayoutDimension::Right)));
		
		layoutItem = UILayoutInstructionBuilder::LayoutItem(canvasRootComponentId, UILayoutDimension::Bottom);
		sortedIndex = layoutItemSortedIndex[layoutItem];
		layoutItemReverseMapping.insert(std::make_pair(sortedIndex, std::make_pair(nullptr, UILayoutDimension::Bottom)));
	}

	for (auto it = sortedUiComponents.begin(); it != sortedUiComponents.end(); ++it)
	{
		UIComponent* comp = *it;
		int32 layoutComponentId = comp->GetLayoutComponentId();

		uint32 layoutItem;
		uint32 sortedIndex;

		layoutItem = UILayoutInstructionBuilder::LayoutItem(layoutComponentId, UILayoutDimension::Left);
		sortedIndex = layoutItemSortedIndex[layoutItem];
		layoutItemReverseMapping.insert(std::make_pair(sortedIndex, std::make_pair(comp, UILayoutDimension::Left)));

		layoutItem = UILayoutInstructionBuilder::LayoutItem(layoutComponentId, UILayoutDimension::Right);
		sortedIndex = layoutItemSortedIndex[layoutItem];
		layoutItemReverseMapping.insert(std::make_pair(sortedIndex, std::make_pair(comp, UILayoutDimension::Right)));

		layoutItem = UILayoutInstructionBuilder::LayoutItem(layoutComponentId, UILayoutDimension::Top);
		sortedIndex = layoutItemSortedIndex[layoutItem];
		layoutItemReverseMapping.insert(std::make_pair(sortedIndex, std::make_pair(comp, UILayoutDimension::Top)));

		layoutItem = UILayoutInstructionBuilder::LayoutItem(layoutComponentId, UILayoutDimension::Bottom);
		sortedIndex = layoutItemSortedIndex[layoutItem];
		layoutItemReverseMapping.insert(std::make_pair(sortedIndex, std::make_pair(comp, UILayoutDimension::Bottom)));
	}

	const char* layoutDimensionNames[] = 
	{
		"Left",
		"Right",
		"Top",
		"Bottom",
	};

	auto layoutItemToString = [&](uint32 layoutItemIndex) -> std::string
	{
		UIComponent* comp = layoutItemReverseMapping[layoutItemIndex].first;
		UILayoutDimension dim = layoutItemReverseMapping[layoutItemIndex].second;
		return formatString("%s.%s", (comp ? comp->GetWorldPath().c_str() : this->GetWorldPath().c_str()), layoutDimensionNames[(int)dim]);
	};

	for (uint32 index = 0; index < (uint32)instructions.size(); ++index)
	{
		const UILayoutItemInstruction& ins = instructions[index];
		float resultValue = values[index];

		switch (ins.op)
		{
			case UILayoutInstructionOp::Assign:
			{
				Log::Info("[%d] %s = assign(%f) = %f", index, layoutItemToString(index).c_str(), ins.value1, resultValue);
				break;
			}
			case UILayoutInstructionOp::Add:
			{
				Log::Info("[%d] %s = <%s> + %f = %f", index, layoutItemToString(index).c_str(), layoutItemToString(ins.layoutItem1).c_str(), ins.value1, resultValue);
				break;
			}
			case UILayoutInstructionOp::Lerp:
			{
				Log::Info("[%d] %s = lerp(<%s>, <%s>, %f) + %f = %f", index, layoutItemToString(index).c_str(), layoutItemToString(ins.layoutItem1).c_str(), layoutItemToString(ins.layoutItem2).c_str(), ins.value1, ins.value2, resultValue);
				break;
			}
			case UILayoutInstructionOp::SubtractMultiplyAdd:
			{
				Log::Info("[%d] %s = (<%s> - <%s>) * %f + <%s> + %f = %f", 
					index, 
					layoutItemToString(index).c_str(), 
					layoutItemToString(ins.layoutItem1).c_str(), 
					layoutItemToString(ins.layoutItem2).c_str(), 
					ins.value1, 
					layoutItemToString(ins.layoutItem3).c_str(), 
					ins.value2, 
					resultValue);
				break;
			}
			default:
				break;
		}
	}
}

void UICanvas::ExecuteLayoutInstructions()
{
	std::vector<UILayoutItemInstruction>& instructions = sortedInstructions;
	std::vector<float>& values = sortedValues;

	values.resize(instructions.size());
	memset(values.data(), 0, sizeof(values[0]) * values.size());

	for (uint32 index = 0; index < (uint32)instructions.size(); ++index)
	{
		UILayoutItemInstruction& ins = instructions[index];

		float dstValue = 0;
		switch (ins.op)
		{
			case UILayoutInstructionOp::Assign:
			{
				dstValue = ins.value1;
				break;
			}
			case UILayoutInstructionOp::Add:
			{
				uint32 layoutItem1Index = layoutItemSortedIndex[ins.layoutItem1];
				float v1 = values[layoutItem1Index];
				dstValue = v1 + ins.value1;
				break;
			}
			case UILayoutInstructionOp::Lerp:
			{
				uint32 layoutItem1Index = layoutItemSortedIndex[ins.layoutItem1];
				uint32 layoutItem2Index = layoutItemSortedIndex[ins.layoutItem2];
				float v1 = values[layoutItem1Index];
				float v2 = values[layoutItem2Index];
				dstValue = math::lerp(v1, v2, ins.value1) + ins.value2;
				break;
			}
			case UILayoutInstructionOp::SubtractMultiplyAdd:
			{
				uint32 layoutItem1Index = layoutItemSortedIndex[ins.layoutItem1];
				uint32 layoutItem2Index = layoutItemSortedIndex[ins.layoutItem2];
				uint32 layoutItem3Index = layoutItemSortedIndex[ins.layoutItem3];
				float v1 = values[layoutItem1Index];
				float v2 = values[layoutItem2Index];
				float v3 = values[layoutItem3Index];
				dstValue = (v1 - v2) * ins.value1 + v3 + ins.value2;
				break;
			}
			default:
				break;
		}

		values[index] = dstValue;
	}
}

void UICanvas::UpdateLayoutResultsToComponents()
{
	for (auto it = sortedUiComponents.begin(); it != sortedUiComponents.end(); ++it)
	{
		UIComponent* comp = *it;

		int32 layoutComponentId = comp->GetLayoutComponentId();

		uint32 layoutItem;
		uint32 sortedIndex;

		layoutItem = UILayoutInstructionBuilder::LayoutItem(layoutComponentId, UILayoutDimension::Left);
		sortedIndex = layoutItemSortedIndex[layoutItem];
		float valueL = sortedValues[sortedIndex];

		layoutItem = UILayoutInstructionBuilder::LayoutItem(layoutComponentId, UILayoutDimension::Right);
		sortedIndex = layoutItemSortedIndex[layoutItem];
		float valueR = sortedValues[sortedIndex];

		layoutItem = UILayoutInstructionBuilder::LayoutItem(layoutComponentId, UILayoutDimension::Top);
		sortedIndex = layoutItemSortedIndex[layoutItem];
		float valueT = sortedValues[sortedIndex];

		layoutItem = UILayoutInstructionBuilder::LayoutItem(layoutComponentId, UILayoutDimension::Bottom);
		sortedIndex = layoutItemSortedIndex[layoutItem];
		float valueB = sortedValues[sortedIndex];

		comp->SetRect(edge4f(valueL, valueT, valueR, valueB));
	}
}
