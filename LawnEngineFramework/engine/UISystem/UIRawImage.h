#pragma once

#include "UICommon.h"
#include "UIComponent.h"
#include "UICanvas.h"

class UIRawImage : public UIComponent
{
	DECL_CLASS_TYPE(UIRawImage, UIComponent);

private:
	Texture2DPtr texture;
	float4 color;
	MaterialPtr material;

public:
	UIRawImage();
	virtual ~UIRawImage();

	virtual void Reflect(Reflector& reflector) override;

	virtual bool CanDraw() override { return GetTexture() != nullptr; }

	void SetTexture(Texture2DPtr texture)
	{
		this->texture = texture;
		MarkMeshDirty();
	}

	void SetColor(const float4& color)
	{
		this->color = color;

		MarkMeshDirty();
	}
	
	void SetMaterial(MaterialPtr mat);
	virtual MaterialPtr GetMaterial() override;
	virtual Texture2DPtr GetTexture() override;
	virtual void UpdateMesh(UIMeshBatch* batch) override;
};
