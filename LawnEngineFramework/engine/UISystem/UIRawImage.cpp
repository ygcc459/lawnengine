#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "UIRawImage.h"
#include "RenderSystem/Sprite.h"
#include "UISystem.h"
#include "RenderSystem/Material.h"

IMPL_CLASS_TYPE(UIRawImage);

UIRawImage::UIRawImage()
	: color(float4(1, 1, 1, 1))
{
}

UIRawImage::~UIRawImage()
{
}

void UIRawImage::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("texture", &Self::texture);
	reflector.AddMember("color", &Self::color);
	reflector.AddMember("material", &Self::material);

	reflector.AddFunction("SetTexture", &Self::SetTexture);
	reflector.AddFunction("SetColor", &Self::SetColor);
}

void UIRawImage::SetMaterial(MaterialPtr mat)
{
	this->material = mat;

	MarkMeshDirty();
}

MaterialPtr UIRawImage::GetMaterial()
{
	if (material)
		return material;
	else
		return g_lawnEngine.getUISystem().GetDefaultImageMaterial();
}

Texture2DPtr UIRawImage::GetTexture()
{
	return texture;
}

void UIRawImage::UpdateMesh(UIMeshBatch* batch)
{
	if (texture)
	{
		const rectf& rect = GetRect().to_rect();

		rectf spriteRect(0.0f, 0.0f, 1.0f, 1.0f);

		batch->AddQuad(rect, spriteRect, color);
	}
}
