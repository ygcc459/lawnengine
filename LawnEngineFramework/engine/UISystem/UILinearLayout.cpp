#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "UISystem.h"
#include "UILinearLayout.h"
#include "EntitySystem/SceneObject.h"
#include "EntitySystem/Transform.h"

//////////////////////////////////////////////////////////////////////////

struct LinearLayoutDirectionEnumType : public EnumObjectType<LinearLayoutDirection>
{
	LinearLayoutDirectionEnumType() : EnumObjectType<LinearLayoutDirection>("LinearLayoutDirection")
	{
		Add("Horizontal", LinearLayoutDirection::Horizontal);
		Add("Vertical", LinearLayoutDirection::Vertical);
	}
};
IMPL_ENUM_TYPE(LinearLayoutDirection, LinearLayoutDirectionEnumType)

//////////////////////////////////////////////////////////////////////////

IMPL_CLASS_TYPE(UILinearLayout);

UILinearLayout::UILinearLayout()
	: UIComponent()
	, direction(LinearLayoutDirection::Vertical)
	, fillHorizontal(true)
	, fillVertical(true)
	, itemSpacing(0)
	, itemMinSize(0)
{
}

UILinearLayout::~UILinearLayout()
{
}

void UILinearLayout::OnStartup()
{
	Super::OnStartup();

	if (canvas)
		canvas->RegisterPreLayoutComponent(this);
}

void UILinearLayout::OnShutdown()
{
	if (canvas)
		canvas->UnRegisterPreLayoutComponent(this);

	Super::OnShutdown();
}

void UILinearLayout::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("direction", &Self::direction);
	reflector.AddMember("fillHorizontal", &Self::fillHorizontal);
	reflector.AddMember("fillVertical", &Self::fillVertical);
	reflector.AddMember("itemSpacing", &Self::itemSpacing);
	reflector.AddMember("itemMinSize", &Self::itemMinSize);
}

void UILinearLayout::UpdateRequiredSize(UIComponentCacheData& cacheData)
{
	if (direction == LinearLayoutDirection::Horizontal)
	{
		float totalWidth = 0;

		for (int iChild = 0; iChild < cacheData.ChildCount(); iChild++)
		{
			UIComponentCacheData& childCacheData = cacheData.GetChild(*canvas, iChild);

			UIComponent* uiComponent = childCacheData.GetComponent();

			uiComponent->UpdateRequiredSize(childCacheData);
			float2 childRequiredSize = uiComponent->GetRequiredSize();
			float requiredWidth = std::max(0.0f, std::max(childRequiredSize.x, itemMinSize));

			totalWidth += requiredWidth;
		}

		this->requiredSize = float2(totalWidth, 0);
	}
	else
	{
		float totalHeight = 0;

		for (int iChild = 0; iChild < cacheData.ChildCount(); iChild++)
		{
			UIComponentCacheData& childCacheData = cacheData.GetChild(*canvas, iChild);

			UIComponent* uiComponent = childCacheData.GetComponent();

			uiComponent->UpdateRequiredSize(childCacheData);
			float2 childRequiredSize = uiComponent->GetRequiredSize();
			float requiredHeight = std::max(0.0f, std::max(childRequiredSize.y, itemMinSize));

			totalHeight += requiredHeight;
		}

		this->requiredSize = float2(0, totalHeight);
	}

	this->requiredSize = max(this->requiredSize, GetLayoutMinSize());
}

void UILinearLayout::UpdateRect(UIComponentCacheData& cacheData, const edge4f& parentRect)
{
	UpdateSelfRect(parentRect);

	rectf rect = GetRect().to_rect();

	if (direction == LinearLayoutDirection::Horizontal)
	{
		float totalFillWeight = 0.00001f;
		float totalRequiredSize = 0;
		float totalExtendSize = 0;

		if (fillHorizontal)
		{
			for (int iChild = 0; iChild < cacheData.ChildCount(); iChild++)
			{
				UIComponentCacheData& childCacheData = cacheData.GetChild(*canvas, iChild);

				UIComponent* uiComponent = childCacheData.GetComponent();

				totalFillWeight += uiComponent->GetLayoutFillWeight().x;
				totalRequiredSize += uiComponent->GetRequiredSize().x;
			}

			totalExtendSize = rect.w - totalRequiredSize;
		}
		else
		{
			totalExtendSize = 0;
		}

		float cur = rect.x;

		for (int iChild = 0; iChild < cacheData.ChildCount(); iChild++)
		{
			UIComponentCacheData& childCacheData = cacheData.GetChild(*canvas, iChild);

			UIComponent* uiComponent = childCacheData.GetComponent();

			float2 childRequiredSize = uiComponent->GetRequiredSize();
			float2 fillWeight = uiComponent->GetLayoutFillWeight();

			float extendSize = totalExtendSize * (fillWeight.x / totalFillWeight);
			float finalWidth = std::max(0.0f, std::max(childRequiredSize.x + extendSize, itemMinSize));

			rectf itemRect = rectf(cur, rect.y, finalWidth, rect.h);
			uiComponent->UpdateRect(childCacheData, edge4f(itemRect));

			cur += itemRect.w;
		}
	}
	else if (direction == LinearLayoutDirection::Vertical)
	{
		float totalFillWeight = 0.00001f;
		float totalRequiredSize = 0;
		float totalExtendSize = 0;

		if (fillVertical)
		{
			for (int iChild = 0; iChild < cacheData.ChildCount(); iChild++)
			{
				UIComponentCacheData& childCacheData = cacheData.GetChild(*canvas, iChild);

				UIComponent* uiComponent = childCacheData.GetComponent();

				totalFillWeight += uiComponent->GetLayoutFillWeight().y;
				totalRequiredSize += uiComponent->GetRequiredSize().y;
			}

			totalExtendSize = rect.h - totalRequiredSize;
		}
		else
		{
			totalExtendSize = 0;
		}

		float cur = rect.y;

		for (int iChild = 0; iChild < cacheData.ChildCount(); iChild++)
		{
			UIComponentCacheData& childCacheData = cacheData.GetChild(*canvas, iChild);

			UIComponent* uiComponent = childCacheData.GetComponent();

			float2 childRequiredSize = uiComponent->GetRequiredSize();
			float2 fillWeight = uiComponent->GetLayoutFillWeight();

			float extendSize = totalExtendSize * (fillWeight.y / totalFillWeight);
			float finalHeight = std::max(0.0f, std::max(childRequiredSize.y + extendSize, itemMinSize));

			rectf itemRect = rectf(rect.x, cur, rect.w, finalHeight);
			uiComponent->UpdateRect(childCacheData, edge4f(itemRect));

			cur += itemRect.h;
		}
	}
}

void UILinearLayout::PreLayout()
{
	UpdateChildrenLayoutItemDescs();
}

void UILinearLayout::UpdateChildrenLayoutItemDescs()
{
	auto& so = GetSceneObject();

	if (direction == LinearLayoutDirection::Horizontal)
	{
		float totalFillWeight = 0.00001f;
		float totalRequiredSize = 0;
		
		if (fillHorizontal)
		{
			for (int iChild = 0; iChild < so.GetChildCount(); iChild++)
			{
				UIComponent* uiComponent = so.GetChild(iChild)->GetComponent<UIComponent>();

				totalFillWeight += uiComponent->GetLayoutFillWeight().x;
				totalRequiredSize += uiComponent->GetRequiredSize().x;
			}
		}

		UIComponent* lastUiComponent = nullptr;
		
		for (int iChild = 0; iChild < so.GetChildCount(); iChild++)
		{
			UIComponent* uiComponent = so.GetChild(iChild)->GetComponent<UIComponent>();
			
			float requiredSize = uiComponent->GetRequiredSize().x;

			if (fillHorizontal)
			{
				float fillWeight = uiComponent->GetLayoutFillWeight().x;
				float normalizedWeight = fillWeight / totalFillWeight;

				//uiComponent.width = extendSize + requiredSize
				// => uiComponent.width = (this.width - totalRequiredSize) * normalizedWeight + requiredSize
				// => uiComponent.width = this.width * normalizedWeight - totalRequiredSize * normalizedWeight + requiredSize
				// => uiComponent.right = (this.right - this.left) * normalizedWeight + uiComponent.left - totalRequiredSize * normalizedWeight + requiredSize
				// => uiComponent.right = (<this.right> - <this.left>) * <normalizedWeight> + <uiComponent.left> + < -totalRequiredSize * normalizedWeight + requiredSize>

				uiComponent->Layout(UILayoutDimension::Right).SubtractMultiplyAdd(this, 
					UILayoutDimension::Right, 
					UILayoutDimension::Left, 
					normalizedWeight,
					UILayoutDimension::Left, 
					-totalRequiredSize * normalizedWeight + requiredSize);
			}
			else
			{
				float finalSize = std::max(0.0f, std::max(requiredSize, itemMinSize));
				uiComponent->Layout(UILayoutDimension::Right).Add(uiComponent, UILayoutDimension::Left, finalSize);
			}

			if (lastUiComponent)
			{
				uiComponent->Layout(UILayoutDimension::Left).Add(lastUiComponent, UILayoutDimension::Right, itemSpacing);
			}
			else
			{
				uiComponent->Layout(UILayoutDimension::Left).Add(this, UILayoutDimension::Left, itemSpacing);
			}

			if (fillVertical)
			{
				uiComponent->Layout(UILayoutDimension::Top).Add(this, UILayoutDimension::Top, itemSpacing);
				uiComponent->Layout(UILayoutDimension::Bottom).Add(this, UILayoutDimension::Bottom, itemSpacing);
			}

			lastUiComponent = uiComponent;
		}
	}
	else if (direction == LinearLayoutDirection::Vertical)
	{
		float totalFillWeight = 0.00001f;
		float totalRequiredSize = 0;
		
		if (fillVertical)
		{
			for (int iChild = 0; iChild < so.GetChildCount(); iChild++)
			{
				UIComponent* uiComponent = so.GetChild(iChild)->GetComponent<UIComponent>();

				totalFillWeight += uiComponent->GetLayoutFillWeight().y;
				totalRequiredSize += uiComponent->GetRequiredSize().y;
			}
		}

		UIComponent* lastUiComponent = nullptr;
		
		for (int iChild = 0; iChild < so.GetChildCount(); iChild++)
		{
			UIComponent* uiComponent = so.GetChild(iChild)->GetComponent<UIComponent>();
			
			float requiredSize = uiComponent->GetRequiredSize().y;

			if (fillVertical)
			{
				float fillWeight = uiComponent->GetLayoutFillWeight().y;
				float normalizedWeight = fillWeight / totalFillWeight;

				//uiComponent.height = extendSize + requiredSize
				// => uiComponent.height = (this.height - totalRequiredSize) * normalizedWeight + requiredSize
				// => uiComponent.height = this.height * normalizedWeight - totalRequiredSize * normalizedWeight + requiredSize
				// => uiComponent.bottom = (this.bottom - this.top) * normalizedWeight + uiComponent.top - totalRequiredSize * normalizedWeight + requiredSize
				// => uiComponent.bottom = (<this.bottom> - <this.top>) * <normalizedWeight> + <uiComponent.left> + < -totalRequiredSize * normalizedWeight + requiredSize>

				uiComponent->Layout(UILayoutDimension::Bottom).SubtractMultiplyAdd(this, 
					UILayoutDimension::Bottom, 
					UILayoutDimension::Top, 
					normalizedWeight,
					UILayoutDimension::Top, 
					-totalRequiredSize * normalizedWeight + requiredSize);
			}
			else
			{
				float finalSize = std::max(0.0f, std::max(requiredSize, itemMinSize));
				uiComponent->Layout(UILayoutDimension::Bottom).Add(uiComponent, UILayoutDimension::Top, finalSize);
			}

			if (lastUiComponent)
			{
				uiComponent->Layout(UILayoutDimension::Top).Add(lastUiComponent, UILayoutDimension::Bottom, itemSpacing);
			}
			else
			{
				uiComponent->Layout(UILayoutDimension::Top).Add(this, UILayoutDimension::Top, itemSpacing);
			}
			
			if (fillHorizontal)
			{
				uiComponent->Layout(UILayoutDimension::Left).Add(this, UILayoutDimension::Left, itemSpacing);
				uiComponent->Layout(UILayoutDimension::Right).Add(this, UILayoutDimension::Right, itemSpacing);
			}

			lastUiComponent = uiComponent;
		}
	}
}
