#pragma once

#include "UICommon.h"
#include "RenderSystem/RenderSystemCommon.h"

class UISystem
{
public:
	UISystem();
	~UISystem();

	MaterialPtr GetDefaultImageMaterial() { return defaultImageMaterial; }
	MaterialPtr GetDefaultTextMaterial() { return defaultTextMaterial; }

private:
	MaterialPtr defaultImageMaterial;
	MaterialPtr defaultTextMaterial;

};
