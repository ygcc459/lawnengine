#pragma once

#include "UICommon.h"
#include "UIComponent.h"
#include "UICanvas.h"
#include "RenderSystem/TextRenderer.h"

class UIText : public UIComponent
{
	DECL_CLASS_TYPE(UIText, UIComponent);

private:
	std::string text;
	int fontSize;
	TextAlignmentHorizontal alignmentHorizontal;
	TextAlignmentVertical alignmentVertical;

	bool minSizeFollowsContent;

	float4 color;
	MaterialPtr material;

public:
	std::function<void(const MouseEvent& ev, const MouseState& state)> MouseEventHandler;

public:
	UIText();
	virtual ~UIText();

	virtual void OnStartup() override;

	virtual void Reflect(Reflector& reflector) override;

	virtual void UpdateRequiredSize(UIComponentCacheData& cacheData);

	void SetText(const std::string& text)
	{
		this->text = text;
		MarkMeshDirty();
	}

	void SetFontSize(int fontSize)
	{
		this->fontSize = fontSize;
		MarkMeshDirty();
	}

	void SetAlignment(TextAlignmentHorizontal alignmentHorizontal, TextAlignmentVertical alignmentVertical)
	{
		this->alignmentHorizontal = alignmentHorizontal;
		this->alignmentVertical = alignmentVertical;
		MarkMeshDirty();
	}

	void SetColor(const float4& color)
	{
		this->color = color;
		MarkMeshDirty();
	}

	void SetMinSizeFollowsContent(bool minSizeFollowsContent)
	{
		this->minSizeFollowsContent = minSizeFollowsContent;
		MarkLayoutDirty();
	}

	void Measure(std::vector<rectf>& outCharRects);

	virtual bool CanDraw() { return fontSize > 0 && text.size() != 0; }

	virtual MaterialPtr GetMaterial() override;
	virtual Texture2DPtr GetTexture() override;
	virtual void UpdateMesh(UIMeshBatch* batch) override;

	virtual bool HandleMouseEvents() override  { return (bool)MouseEventHandler; }
	virtual void OnMouseEvent(const MouseEvent& ev, const MouseState& state) override;
};
