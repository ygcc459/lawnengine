#pragma once

#include "UICommon.h"
#include "UIComponent.h"
#include "UICanvas.h"
#include "UISelectableText.h"

class UIImage;
class UILinearLayout;
class UIButton;

class UISceneHierarchyView : public UIComponent
{
	DECL_CLASS_TYPE(UISceneHierarchyView, UIComponent);

public:
	UISceneHierarchyView();
	virtual ~UISceneHierarchyView();

	virtual void OnStartup() override;
	virtual void OnShutdown() override;

	virtual bool CanDraw() override { return false; }
	virtual bool HandleMouseEvents() { return false; }

private:
	struct SceneObjectInfo
	{
		SceneObject* targetSO;

		int depth;
		bool selected;
		bool expanded;

		SceneObject* UISO;
		UILinearLayout* layout;
		UIImage* backgroundImage;
		UISelectableText* expandButton;
		UISelectableText* nameButton;

		SceneObjectInfo()
			: targetSO(nullptr)
			, depth(-1)
			, selected(false)
			, expanded(true)
			, UISO(nullptr)
			, layout(nullptr)
			, backgroundImage(nullptr)
			, expandButton(nullptr)
			, nameButton(nullptr)
		{}
	};

	std::map<SceneObject*, std::unique_ptr<SceneObjectInfo>> sceneObjectInfoMap;

	SceneObject* containerSO;

	SpriteAtlasPtr atlas;

	SceneObjectInfo* GetSceneObjectInfo(SceneObject* so, bool autoCreate = true);

	void CreateSceneObjectUIRecursive(SceneObject* so);

	void UpdateUIVisiblityForExpandRecursive(SceneObject* so, bool parentExpanded);

	void OnSceneObjectDestroy(SceneObject* so);
	void OnSceneObjectSwitchExpand(SceneObject* so);
	void OnSceneObjectSelect(SceneObject* so);
};
