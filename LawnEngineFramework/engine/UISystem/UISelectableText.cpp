#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "UISelectableText.h"
#include "RenderSystem/Sprite.h"
#include "UISystem.h"
#include "RenderSystem/Material.h"
#include "EntitySystem/SceneObject.h"
#include "UIImage.h"

IMPL_CLASS_TYPE(UISelectableText);

UISelectableText::UISelectableText()
	: normalColor(float4(1, 1, 1, 1))
	, hoverColor(float4(0.50f, 0.85f, 1.0f, 1.0f))
	, pressColor(float4(0.15f, 0.60f, 1.0f, 1.0f))
{
}

UISelectableText::~UISelectableText()
{
}

void UISelectableText::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("normalColor", &Self::normalColor);
	reflector.AddMember("hoverColor", &Self::hoverColor);
	reflector.AddMember("pressColor", &Self::pressColor);

	reflector.AddFunction("SetColors", &Self::SetColors);
}

void UISelectableText::OnMouseEvent(const MouseEvent& ev, const MouseState& state)
{
	if (ev.type == MET_Enter)
	{
		SetColor(hoverColor);
	}
	else if (ev.type == MET_Leave)
	{
		SetColor(normalColor);
	}
	else if (ev.type == MET_ButtonDown)
	{
		SetColor(pressColor);
	}
	else if (ev.type == MET_ButtonUp)
	{
		SetColor(hoverColor);

		if (ev.button == MB_Left)
		{
			if (OnLeftClick)
				OnLeftClick();
		}
		else if (ev.button == MB_Right)
		{
			if (OnRightClick)
				OnRightClick();
		}
	}
}
