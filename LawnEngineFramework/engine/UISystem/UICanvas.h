#pragma once

#include "UICommon.h"
#include "RenderSystem/Mesh.h"
#include "EntitySystem/Component.h"
#include "EntitySystem/RenderSceneObjectBridge.h"
#include "InputSystem/InputManager.h"
#include "UILayout.h"
#include <vcpp/ArrayView.h>
#include <vcpp/array2d.h>
#include <vcpp/dynamic_bitset.h>
#include <deque>

class Mesh;
class Material;
class UICanvas;
class UICanvasRenderSceneObject;

struct UIMeshVertex
{
	float3 pos;	
	float2 uv;
	vcpp::la::color32 color;

	UIMeshVertex()
	{}

	UIMeshVertex(const float3& pos, const float2& uv, vcpp::la::color32 color)
		:pos(pos), uv(uv), color(color)
	{}
};

struct UIMeshBatchKey
{
	int depth;
	void* material;
	void* texture;

	UIMeshBatchKey(int depth, void* material, void* texture)
		:depth(depth), material(material), texture(texture)
	{}

	bool operator == (const UIMeshBatchKey& other) const
	{
		return depth == other.depth && material == other.material;
	}

	bool operator < (const UIMeshBatchKey& other) const
	{
		if (depth != other.depth)
			return depth < other.depth;
		else if (texture != other.texture)
			return texture < other.texture;
		else if (material != other.material)
			return material < other.material;
		else
			return false;
	}
};

class UIMeshBatch
{
public:
	int depth;
	MaterialPtr material;
	Texture2DPtr texture;
	MeshPtr mesh;

	std::vector<UIMeshVertex> vertices;

public:
	UIMeshBatch(int depth, MaterialPtr material, Texture2DPtr texture);
	~UIMeshBatch();

	int AddVertices(int count);
	UIMeshVertex& GetVertex(int index) { return vertices[index]; }

	int AddQuad(const rectf& pos_rect, const rectf& uv_rect, const float4& color);

	int AddQuad(float x1, float x2, float y1, float y2, float u1, float u2, float v1, float v2, const float4& color);

	int AddNinePatchQuad(
		const rectf& pos_rect_outer, const rectf& pos_rect_inner, 
		const rectf& uv_rect_outer, const rectf& uv_rect_inner,
		const float4& color);

	void UpdateMesh();

	void Clear();
};

class UIGrid
{
public:
	UIGrid();
	~UIGrid();

	void Resize(const int2& canvasSize);
	void Clear();

	void PushRect(const rectf& rect, int depth, UIComponent* component);
	int GetRectDepth(const rectf& rect);
	void PopRect(const rectf& rect, int depth, UIComponent* component);

	int2 GetGrid(const float2& pos) const;
	void GetClampedGridRange(const rectf& rect, int2& outMin, int2& outMax) const;

	UIComponent* Raycast(const float2& pos) const;

public:
	int2 gridCellSize;  //cell's size in pixels
	int2 canvasSize;  //in pixels

	// a grid of UIComponent stacks
	// each element in stack is sorted by render-order from back to front
	vcpp::array2d<std::stack<int>> depthStackGrid;

	struct Slice
	{
		rectf fullRect;
		int depth;
		UIComponent* component;

		Slice(const rectf& fullRect, int depth, UIComponent* component)
			: fullRect(fullRect), depth(depth), component(component)
		{}
	};

	// a grid of UIComponent stacks
	// each element in stack is sorted by render-order from back to front
	vcpp::array2d<std::vector<Slice>> spatialSlicesGrid;
};

class UIBatcher
{
public:
	UIBatcher();
	~UIBatcher();

	UIMeshBatch* GetMeshBatch(const UIMeshBatchKey& key);
	UIMeshBatch* CreateMeshBatch(const UIMeshBatchKey& key, int depth, MaterialPtr material, Texture2DPtr texture);

	void Clear();

	const std::vector<UIMeshBatch*>& GetSortedMeshBatches() { return meshBatches; }

	void UpdateMeshes();

private:
	// sorted in render draw call order, i.e. depth order asc
	// as we iterate the mesh in UpdateMeshRecursive in DFS, and adding new batches along the way,
	// meshBatches should already be in the correct order after that
	std::vector<UIMeshBatch*> meshBatches;

	std::map<UIMeshBatchKey, UIMeshBatch*> meshBatchKeyMapping;
};

struct UIComponentCacheData
{
	UIComponent* component;  //never null
	int32 childrenIndexBegin;  //index into componentCacheDatas
	int32 childrenCount;  //index into componentCacheDatas
	int32 parentIndex;  //index into componentCacheDatas

	UIComponentCacheData()
		: component(nullptr)
		, childrenIndexBegin(-1)
		, childrenCount(0)
		, parentIndex(-1)
	{
	}

	UIComponent* GetComponent() { return component; }

	int32 ChildCount() const { return childrenCount; }

	inline UIComponentCacheData& GetChild(UICanvas& canvas, int32 i);

	bool HasParent() const { return parentIndex != -1; }

	inline UIComponentCacheData& GetParent(UICanvas& canvas);
};

//
// currently only support rendering to one camera
//
class UICanvas : public RenderSceneObjectBridge, public MouseEventListener, public KeyEventListener
{
	DECL_CLASS_TYPE(UICanvas, RenderSceneObjectBridge);

public:
	UICanvas();
	virtual ~UICanvas();

	virtual void CreateRenderSceneObject() override;
	virtual void DestroyRenderSceneObject() override;

	virtual void Reflect(Reflector& reflector) override;

	void Resize(const int2& canvasSize);
	int2 GetCanvasSize() const { return canvasSize; };  //in pixels

	virtual void OnUpdate() override;

	void SetMouseFocus(UIComponent* component);
	UIComponent* GetMouseFocus();

	void SetKeyFocus(UIComponent* component);
	UIComponent* GetKeyFocus();

	virtual float GetMouseRaycastPriority() override;
	virtual BaseObject* OnMouseRaycast(const MouseEvent& ev, const MouseState& state) override;
	virtual void OnMouseEvent(BaseObject* raycastResult, const MouseEvent& ev, const MouseState& state);

	virtual void OnKeyEvent(const KeyEvent& ev, void* customData) override;

	virtual void OnStartup() override;
	virtual void OnShutdown() override;

	virtual void OnChildHierarchyChanged(const HierarchyChangedEvent& ev) override;

	void MarkLayoutDirty()
	{
		layoutDirty = true; 
		MarkLayoutInstructionsDirty();
	}
	void MarkMeshDirty() { meshDirty = true; }

	void MarkLayoutInstructionsDirty() { layoutInstructionsDirty = true; }
		
	//
	// hiearachy
	//	
	template<class T = UIImage>
	T* AddUIChild(const std::string& name = "UISceneObject")
	{
		Type* type = &T::StaticType;
		return (T*)AddUIChild(type, name);
	}
	
	UIComponent* AddUIChild(Type* type, const std::string& name = "UISceneObject");
	
	void RegisterComponent(UIComponent* comp);
	void UnRegisterComponent(UIComponent* comp);
	
	void RegisterPreLayoutComponent(UIComponent* comp);
	void UnRegisterPreLayoutComponent(UIComponent* comp);

private:
	struct ComponentCacheData;

	void BuildCacheDatas();
	void BuildCacheDatasRecursive(int32 curDataIndex);
	void UpdateLayout();
	void UpdateMesh();
	void UpdateGrid();
	void UpdateGridRecursive(UIComponentCacheData& cacheData);

	void UpdateLayoutInstructions();
	void ExecuteLayoutInstructions();
	void DumpLayoutInstructions();
	void UpdateLayoutResultsToComponents();

public:
	RegistryList<UICanvas*>::Handle renderViewRegistryHandle;

private:
	int2 canvasSize;  //in pixels

	bool layoutDirty;
	bool layoutInstructionsDirty;
	bool meshDirty;

	UIGrid grid;
	UIBatcher batcher;
	UILayoutInstructionBuilder instructionBuilder;

	UIComponent* mouseFocus;

	UIComponent* keyFocus;

private:
	friend struct UIComponentCacheData;
	friend struct UILayoutInstructionBuilder;

	std::set<UIComponent*> uiComponents;
	std::set<UIComponent*> preLayoutUiComponents;

	// ordered in breath-first-order, this is *NOT* the drawing-order
	// only store visible nodes
	std::vector<UIComponentCacheData> componentCacheDatas;
	int32 componentCacheDatasRootCount;

	struct ComponentWithDepth
	{
		int32 depth;
		UIComponent* component;

		ComponentWithDepth()
		{}

		ComponentWithDepth(int32 depth, UIComponent* component)
			: depth(depth), component(component)
		{}
	};

	// ordered in depth-first-order, this is also the drawing-order
	// i.e. child is placed after parent, so that child-UI is drawn after parent-UI
	std::vector<ComponentWithDepth> componentsWithDepth;

	std::deque<UIComponent*> uiComponentsVisitQueue;  //reusable buffer
	
	// ordered in drawing-order: 
	//   parent is drawed before children
	//   first-sibling is drawed before last-sibling
	//   breath first, not depth-first
	std::vector<UIComponent*> sortedUiComponents;

	UICanvasRenderSceneObject* renderSceneObject;
	friend class UICanvasRenderSceneObject;

	std::vector<uint64> layoutItemVisitStack;
	vcpp::dynamic_bitset layoutItemsResolved;

	std::vector<uint32> layoutItemSortedIndex;
	std::vector<UILayoutItemInstruction> sortedInstructions;
	std::vector<float> sortedValues;

	UIComponent* pointerDraggingComponent;
};

UIComponentCacheData& UIComponentCacheData::GetChild(UICanvas& canvas, int32 i)
{
	return canvas.componentCacheDatas[childrenIndexBegin + i];
}

UIComponentCacheData& UIComponentCacheData::GetParent(UICanvas& canvas)
{
	return canvas.componentCacheDatas[parentIndex];
}
