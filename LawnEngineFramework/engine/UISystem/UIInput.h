#pragma once

#include "UICommon.h"
#include "UIText.h"

class UIInput : public UIComponent
{
	DECL_CLASS_TYPE(UIInput, UIComponent);

public:
	UIText* textComponent;
	UIImage* caretImage;

	std::string text;
	std::vector<rectf> charRects;
	bool charRectsDirty;

	int selectionBegin;
	int selectionEnd;

public:
	std::function<void(void)> OnEnter;

public:
	UIInput();
	virtual ~UIInput();

	virtual void OnStartup() override;
	virtual void OnShutdown() override;

	virtual void Reflect(Reflector& reflector) override;

	void SetText(const std::string& text);
	const std::string& GetText() const { return text; }

	void SetCaret(int pos);

	void SetSelection(int begin, int end);
	int GetSelectionLength() const;

	void SetSelctionText(const std::string& v);

	virtual bool HandleMouseEvents() override { return true; }
	virtual void OnMouseEvent(const MouseEvent& ev, const MouseState& state) override;

	virtual bool HandleKeyEvents() override { return true; }
	virtual void OnKeyEvent(const KeyEvent& ev, void* customData) override;

	int GetCharPosition(const float2& mousePos);
	void UpdateCharRects(bool force = false);

};
