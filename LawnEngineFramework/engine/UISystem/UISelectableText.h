#pragma once

#include "UICommon.h"
#include "UIComponent.h"
#include "UICanvas.h"
#include "UIText.h"

class UIImage;

class UISelectableText : public UIText
{
	DECL_CLASS_TYPE(UISelectableText, UIText);

public:
	float4 normalColor;
	float4 hoverColor;
	float4 pressColor;

	std::function<void(void)> OnLeftClick;
	std::function<void(void)> OnRightClick;

public:
	UISelectableText();
	virtual ~UISelectableText();

	virtual void Reflect(Reflector& reflector) override;

	void SetColors(const float4& normalColor, const float4& hoverColor, const float4& pressColor)
	{
		this->normalColor = normalColor;
		this->hoverColor = hoverColor;
		this->pressColor = pressColor;

		MarkMeshDirty();
	}

	virtual bool HandleMouseEvents() { return true; }
	virtual void OnMouseEvent(const MouseEvent& ev, const MouseState& state) override;
};
