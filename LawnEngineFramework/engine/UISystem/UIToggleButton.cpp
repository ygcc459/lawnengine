#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "UIToggleButton.h"
#include "RenderSystem/Sprite.h"
#include "UISystem.h"
#include "RenderSystem/Material.h"
#include "EntitySystem/SceneObject.h"
#include "UIImage.h"
#include "UIText.h"

ConsoleVariable<AssetPath> CVarUIDefaultToggleButtonAtlas("UIDefault.ToggleButton.Image.Atlas", AssetPath(AST_ProjectPath, "DefaultUIAtlas.SpriteAtlas"));

ConsoleVariable<std::string> CVarUIDefaultToggleButtonOffNormalSprite("UIDefault.ToggleButton.OffNormal.Sprite", "ToggleOffNormal");
ConsoleVariable<std::string> CVarUIDefaultToggleButtonOnNormalSprite("UIDefault.ToggleButton.OnNormal.Sprite", "ToggleOnNormal");
ConsoleVariable<std::string> CVarUIDefaultToggleButtonOffHoverSprite("UIDefault.ToggleButton.OffHover.Sprite", "ToggleOffHover");
ConsoleVariable<std::string> CVarUIDefaultToggleButtonOnHoverSprite("UIDefault.ToggleButton.OnHover.Sprite", "ToggleOnHover");
ConsoleVariable<std::string> CVarUIDefaultToggleButtonOffPressedSprite("UIDefault.ToggleButton.OffPressed.Sprite", "ToggleOffPressed");
ConsoleVariable<std::string> CVarUIDefaultToggleButtonOnPressedSprite("UIDefault.ToggleButton.OnPressed.Sprite", "ToggleOnPressed");

ConsoleVariable<float4> CVarUIDefaultToggleButtonOffNormalColor("UIDefault.ToggleButton.OffNormal.Color", float4(1, 1, 1, 1));
ConsoleVariable<float4> CVarUIDefaultToggleButtonOnNormalColor("UIDefault.ToggleButton.OnNormal.Color", float4(1, 1, 1, 1));
ConsoleVariable<float4> CVarUIDefaultToggleButtonOffHoverColor("UIDefault.ToggleButton.OffHover.Color", float4(1, 1, 1, 1));
ConsoleVariable<float4> CVarUIDefaultToggleButtonOnHoverColor("UIDefault.ToggleButton.OnHover.Color", float4(1, 1, 1, 1));
ConsoleVariable<float4> CVarUIDefaultToggleButtonOffPressedColor("UIDefault.ToggleButton.OffPressed.Color", float4(1, 1, 1, 1));
ConsoleVariable<float4> CVarUIDefaultToggleButtonOnPressedColor("UIDefault.ToggleButton.OnPressed.Color", float4(1, 1, 1, 1));

IMPL_CLASS_TYPE(UIToggleButton);

UIToggleButton::UIToggleButton()
	: targetImage(nullptr)
	, targetText(nullptr)
	, offNormalColor(float4(1, 1, 1, 1))
	, onNormalColor(float4(1, 1, 1, 1))
	, offHoverColor(float4(1, 1, 1, 1))
	, onHoverColor(float4(1, 1, 1, 1))
	, offPressedColor(float4(1, 1, 1, 1))
	, onPressedColor(float4(1, 1, 1, 1))
	, value(false)
{
}

UIToggleButton::~UIToggleButton()
{
}

void UIToggleButton::OnStartup()
{
	Super::OnStartup();

	AssetManager& assetManager = g_lawnEngine.getAssetManager();
	SpriteAtlasPtr atlas = assetManager.Load<SpriteAtlas>(CVarUIDefaultToggleButtonAtlas.GetValue());

	SceneObject* bgSO = GetSceneObject().AddChild("Background");
	UIImage* bgImage = bgSO->AddComponent<UIImage>();

	SceneObject* textSO = GetSceneObject().AddChild("Text");
	UIText* text1 = textSO->AddComponent<UIText>();
	text1->SetAlignment(TextAlignmentHorizontal_Center, TextAlignmentVertical_Center);

	offNormalColor = CVarUIDefaultToggleButtonOffNormalColor.GetValue();
	onNormalColor = CVarUIDefaultToggleButtonOnNormalColor.GetValue();
	offHoverColor = CVarUIDefaultToggleButtonOffHoverColor.GetValue();
	onHoverColor = CVarUIDefaultToggleButtonOnHoverColor.GetValue();
	offPressedColor = CVarUIDefaultToggleButtonOffPressedColor.GetValue();
	onPressedColor = CVarUIDefaultToggleButtonOnPressedColor.GetValue();

	SetSprites(atlas, 
		CVarUIDefaultToggleButtonOffNormalSprite.GetValue(),
		CVarUIDefaultToggleButtonOnNormalSprite.GetValue(),
		CVarUIDefaultToggleButtonOffHoverSprite.GetValue(),
		CVarUIDefaultToggleButtonOnHoverSprite.GetValue(),
		CVarUIDefaultToggleButtonOffPressedSprite.GetValue(),
		CVarUIDefaultToggleButtonOnPressedSprite.GetValue());
	SetTargetImage(bgImage);
	SetTargetText(text1);
}

void UIToggleButton::OnShutdown()
{
	Super::OnShutdown();
}

void UIToggleButton::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("spriteAtlas", &Self::spriteAtlas);
	reflector.AddMember("targetImage", &Self::targetImage);
	reflector.AddMember("targetText", &Self::targetText);

	reflector.AddMember("offNormalSpriteName", &Self::offNormalSpriteName);
	reflector.AddMember("onNormalSpriteName", &Self::onNormalSpriteName);
	reflector.AddMember("offHoverSpriteName", &Self::offHoverSpriteName);
	reflector.AddMember("onHoverSpriteName", &Self::onHoverSpriteName);
	reflector.AddMember("offPressedSpriteName", &Self::offPressedSpriteName);
	reflector.AddMember("onPressedSpriteName", &Self::onPressedSpriteName);
	
	reflector.AddMember("offNormalColor", &Self::offNormalColor);
	reflector.AddMember("onNormalColor", &Self::onNormalColor);
	reflector.AddMember("offHoverColor", &Self::offHoverColor);
	reflector.AddMember("onHoverColor", &Self::onHoverColor);
	reflector.AddMember("offPressedColor", &Self::offPressedColor);
	reflector.AddMember("onPressedColor", &Self::onPressedColor);
}

MaterialPtr UIToggleButton::GetMaterial()
{
	return MaterialPtr();
}

Texture2DPtr UIToggleButton::GetTexture()
{
	return Texture2DPtr();
}

void UIToggleButton::UpdateMesh(UIMeshBatch* batch)
{
}

void UIToggleButton::SetTargetImage(UIImage* targetImage)
{
	this->targetImage = targetImage;

	if (targetImage)
	{
		targetImage->SetSprite(spriteAtlas, GetValue() ? onNormalSpriteName : offNormalSpriteName);
		targetImage->SetColor(GetValue() ? onNormalColor : offNormalColor);
	}

	MarkMeshDirty();
}

void UIToggleButton::SetTargetText(UIText* targetText)
{
	this->targetText = targetText;
}

void UIToggleButton::SetValue(bool v, bool triggerValueChangedEvent /*= true*/)
{
	if (this->value != v)
	{
		this->value = v;

		if (triggerValueChangedEvent)
		{
			if (OnValueChanged)
			{
				OnValueChanged();
			}
		}

		if (targetImage)
		{
			targetImage->SetSprite(spriteAtlas, GetValue() ? onNormalSpriteName : offNormalSpriteName);
			targetImage->SetColor(GetValue() ? onNormalColor : offNormalColor);
		}
	}
}

void UIToggleButton::OnMouseEvent(const MouseEvent& ev, const MouseState& state)
{
	if (ev.type == MET_Enter)
	{
		if (targetImage)
		{
			targetImage->SetSprite(spriteAtlas, GetValue() ? onHoverSpriteName : offHoverSpriteName);
			targetImage->SetColor(GetValue() ? onHoverColor : offHoverColor);
		}
	}
	else if (ev.type == MET_Leave)
	{
		if (targetImage)
		{
			targetImage->SetSprite(spriteAtlas, GetValue() ? onNormalSpriteName : offNormalSpriteName);
			targetImage->SetColor(GetValue() ? onNormalColor : offNormalColor);
		}
	}
	else if (ev.type == MET_ButtonDown)
	{
		if (targetImage)
		{
			targetImage->SetSprite(spriteAtlas, GetValue() ? onPressedSpriteName : offPressedSpriteName);
			targetImage->SetColor(GetValue() ? onPressedColor : offPressedColor);
		}
	}
	else if (ev.type == MET_ButtonUp)
	{
		Toggle();  //image&color already updated in Toggle()
	}
}
