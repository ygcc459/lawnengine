#pragma once

#include "UICommon.h"
#include "UIComponent.h"
#include "UICanvas.h"

class UIImage;

class UIToggleButton : public UIComponent
{
	DECL_CLASS_TYPE(UIToggleButton, UIComponent);

public:
	SpriteAtlasPtr spriteAtlas;

	UIImage* targetImage;
	UIText* targetText;

	std::string offNormalSpriteName;
	std::string onNormalSpriteName;
	std::string offHoverSpriteName;
	std::string onHoverSpriteName;
	std::string offPressedSpriteName;
	std::string onPressedSpriteName;
	
	float4 offNormalColor;
	float4 onNormalColor;
	float4 offHoverColor;
	float4 onHoverColor;
	float4 offPressedColor;
	float4 onPressedColor;

	bool value;

	std::function<void(void)> OnValueChanged;

public:
	UIToggleButton();
	virtual ~UIToggleButton();

	virtual void OnStartup() override;
	virtual void OnShutdown() override;

	virtual void Reflect(Reflector& reflector) override;

	void SetValue(bool v, bool triggerValueChangedEvent = true);
	bool GetValue() const { return value; }
	void Toggle() { SetValue(!GetValue()); }

	void SetTargetImage(UIImage* targetImage);
	void SetTargetText(UIText* targetText);

	void SetSprites(
		SpriteAtlasPtr spriteAtlas, 	
		const std::string& offNormalSpriteName, 
		const std::string& onNormalSpriteName, 
		const std::string& offHoverSpriteName, 
		const std::string& onHoverSpriteName, 
		const std::string& offPressedSpriteName, 
		const std::string& onPressedSpriteName)
	{
		this->spriteAtlas = spriteAtlas;
		this->offNormalSpriteName = offNormalSpriteName;
		this->onNormalSpriteName = onNormalSpriteName;
		this->offHoverSpriteName = offHoverSpriteName;
		this->onHoverSpriteName = onHoverSpriteName;
		this->offPressedSpriteName = offPressedSpriteName;
		this->onPressedSpriteName = onPressedSpriteName;

		MarkMeshDirty();
	}

	void SetColors(
		const float4& offNormalColor,
		const float4& onNormalColor,
		const float4& offHoverColor,
		const float4& onHoverColor,
		const float4& offPressedColor,
		const float4& onPressedColor)
	{
		this->offNormalColor = offNormalColor;
		this->onNormalColor = onNormalColor;
		this->offHoverColor = offHoverColor;
		this->onHoverColor = onHoverColor;
		this->offPressedColor = offPressedColor;
		this->onPressedColor = onPressedColor;

		MarkMeshDirty();
	}

	virtual bool CanDraw() override { return false; }

	virtual MaterialPtr GetMaterial() override;
	virtual Texture2DPtr GetTexture() override;
	virtual void UpdateMesh(UIMeshBatch* batch) override;

	virtual bool HandleMouseEvents() { return true; }
	virtual void OnMouseEvent(const MouseEvent& ev, const MouseState& state) override;
};
