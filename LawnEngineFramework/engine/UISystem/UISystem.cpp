#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "UISystem.h"
#include "RenderSystem/Shader.h"
#include "RenderSystem/Material.h"

UISystem::UISystem()
{
	AssetManager& assetManager = g_lawnEngine.getAssetManager();

	ShaderPtr imageShader = assetManager.Load<Shader>(assetManager.ProjectDir() / "Shaders/UIStandard.shader");
	defaultImageMaterial.reset(new Material());
	defaultImageMaterial->BindShader(imageShader);

	ShaderPtr textShader = assetManager.Load<Shader>(assetManager.ProjectDir() / "Shaders/UIText.shader");
	defaultTextMaterial.reset(new Material());
	defaultTextMaterial->BindShader(textShader);
}

UISystem::~UISystem()
{
}
