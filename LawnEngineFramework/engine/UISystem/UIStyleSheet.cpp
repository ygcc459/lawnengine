#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "UIStyleSheet.h"
#include "EntitySystem/World.h"
#include "EntitySystem/Transform.h"
#include "UISystem/UISystem.h"

IMPL_CLASS_TYPE(UIStyleSheet);

UIStyleSheet::UIStyleSheet()
{
}

UIStyleSheet::~UIStyleSheet()
{
}

bool UIStyleSheet::OnLoad(Archive& archive)
{
	ArchiveNode* root = archive.Root();

	for (int i = 0; i < root->ChildCount(); i++)
	{
		ArchiveNode* typeNode = root->GetChild(i);
		if (typeNode->GetName() == "TargetType")
		{
			std::string typeName = typeNode->GetValue<std::string>();

			Type* type = Type::FindByName(typeName);
			if (type != nullptr)
			{
				auto it = typeToStyleItemsMap.insert(std::make_pair(type, StyleItemsForType())).first;

				StyleItemsForType* styleItemsForType = &it->second;

				for (int j = 0; j < typeNode->ChildCount(); j++)
				{
					ArchiveNode* childNode = typeNode->GetChild(j);
					if (childNode->GetName() == "Member")
					{
						std::string memberName = childNode->GetChildValue<std::string>("Name");
						std::string memberValue = childNode->GetChildValue<std::string>("Value");

						bool found = false;
						for (auto& member : styleItemsForType->members)
						{
							if (member.memberName == memberName)
							{
								found = true;
								break;
							}
						}

						if (!found)
						{
							styleItemsForType->members.push_back(StyleMemberItem(memberName, memberValue));
						}
					}
					else if (childNode->GetName() == "Function")
					{
						std::string functionName = childNode->GetChildValue<std::string>("Name");

						// add function if not exist
						bool found = false;
						for (auto& func : styleItemsForType->functions)
						{
							if (func.functionName == functionName)
							{
								found = true;
								break;
							}
						}

						if (!found)
						{
							styleItemsForType->functions.push_back(StyleFunctionItem(functionName));

							StyleFunctionItem& styleFunctionItem = styleItemsForType->functions.back();

							// add params
							for (int k = 0; k < childNode->ChildCount(); k++)
							{
								ArchiveNode* paramNode = childNode->GetChild(k);
								if (paramNode->GetName() == "Param")
								{
									std::string paramValue = paramNode->GetValue<std::string>();
									styleFunctionItem.paramValues.push_back(paramValue);
								}
							}
						}
					}
				}
			}
			else
			{
				Log::Error("Can not find type '%s' while parsing '%s'", typeName.c_str(), GetAssetPath().ToString().c_str());
			}
		}
	}

	return false;
}

void UIStyleSheet::Apply(SceneObject* so, bool recursive /*= true*/)
{
	if (recursive)
	{
		for (size_t i = 0; i < so->transform->children.size(); i++)
		{
			SceneObject& child = so->transform->children[i]->GetSceneObject();
			Apply(&child, recursive);
		}
	}

	for (size_t i = 0; i < so->components.size(); i++)
	{
		Component* component = so->components[i];

		Type* type = component->GetType();

		auto it = typeToStyleItemsMap.find(type);
		if (it != typeToStyleItemsMap.end())
		{
			StyleItemsForType& styleItemsForType = it->second;

			Reflector reflector;
			component->Reflect(reflector);

			for (StyleMemberItem& smi : styleItemsForType.members)
			{
				ObjectMember* member = reflector.GetMember(smi.memberName.c_str());
				if (member)
				{
					const BaseProperty* prop = member->GetProperty();
					if (prop->valueType)
					{
						prop->valueType->FromString(smi.memberValue, member->GetAddr(component));
					}
				}
			}

			for (StyleFunctionItem& sfi : styleItemsForType.functions)
			{
				ObjectFunction* func = reflector.GetFunction(sfi.functionName.c_str());
				if (func)
				{
					if (func->parameters.size() == sfi.paramValues.size())
					{
						FunctionCall call(*func);
						call.targetObject = component;

						for (size_t iParam = 0; iParam < call.parameters.size(); iParam++)
						{
							const BaseProperty* prop = func->parameters[i].get();
							if (prop->valueType)
							{
								void* paramValueAddr = call.parameters[iParam];
								prop->valueType->FromString(sfi.paramValues[i], paramValueAddr);
							}
						}

						call.Invoke();
					}
					else
					{
						Log::Error("Expecting %llu parameters, but %llu provided, for function call %s", func->parameters.size(), sfi.paramValues.size(), sfi.functionName.c_str());
					}
				}
			}
		}
	}
}
