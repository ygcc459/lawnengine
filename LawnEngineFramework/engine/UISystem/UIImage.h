#pragma once

#include "UICommon.h"
#include "UIComponent.h"
#include "UICanvas.h"

class Sprite;
typedef std::shared_ptr<Sprite> SpritePtr;

class SpriteAtlas;
typedef std::shared_ptr<SpriteAtlas> SpriteAtlasPtr;

class UIImage : public UIComponent
{
	DECL_CLASS_TYPE(UIImage, UIComponent);

private:
	SpriteAtlasPtr spriteAtlas;
	std::string spriteName;
	float4 color;
	MaterialPtr material;

	Sprite* sprite;

public:
	UIImage();
	virtual ~UIImage();

	virtual void OnStartup() override;

	virtual void Reflect(Reflector& reflector) override;

	virtual bool CanDraw() override { return GetTexture() && sprite != nullptr; }

	void SetSprite(SpriteAtlasPtr spriteAtlas, const std::string& spriteName)
	{
		this->spriteAtlas = spriteAtlas;
		this->spriteName = spriteName;
		FetchSprite();

		MarkMeshDirty();
	}

	void SetColor(const float4& color)
	{
		this->color = color;

		MarkMeshDirty();
	}

	virtual MaterialPtr GetMaterial() override;
	virtual Texture2DPtr GetTexture() override;
	virtual void UpdateMesh(UIMeshBatch* batch) override;

	void FetchSprite();
};
