#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "UIImage.h"
#include "RenderSystem/Sprite.h"
#include "UISystem.h"
#include "RenderSystem/Material.h"

ConsoleVariable<AssetPath> CVarUIDefaultImageAtlas("UIDefault.Image.Atlas", AssetPath(AST_ProjectPath, "DefaultUIAtlas.SpriteAtlas"));
ConsoleVariable<std::string> CVarUIDefaultImageSprite("UIDefault.Image.Sprite", "Panel");

IMPL_CLASS_TYPE(UIImage);

UIImage::UIImage()
	: color(float4(1, 1, 1, 1)), sprite(nullptr)
{
}

UIImage::~UIImage()
{
}

void UIImage::OnStartup()
{
	Super::OnStartup();

	AssetManager& assetManager = g_lawnEngine.getAssetManager();
	SpriteAtlasPtr atlas = assetManager.Load<SpriteAtlas>(CVarUIDefaultImageAtlas.GetValue());

	SetSprite(atlas, CVarUIDefaultImageSprite.GetValue());
}

void UIImage::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("spriteAtlas", &Self::spriteAtlas);
	reflector.AddMember("spriteName", &Self::spriteName);
	reflector.AddMember("color", &Self::color);
	reflector.AddMember("material", &Self::material);

	reflector.AddFunction("SetSprite", &Self::SetSprite);
	reflector.AddFunction("SetColor", &Self::SetColor);
}

MaterialPtr UIImage::GetMaterial()
{
	if (material)
		return material;
	else
		return g_lawnEngine.getUISystem().GetDefaultImageMaterial();
}

Texture2DPtr UIImage::GetTexture()
{
	if (spriteAtlas)
		return spriteAtlas->texture;
	else
		return Texture2DPtr();
}

void UIImage::UpdateMesh(UIMeshBatch* batch)
{
	if (sprite && spriteAtlas && spriteAtlas->texture)
	{
		const rectf& rect = GetRect().to_rect();

		const rectf& spriteRect = sprite->rect;

		Texture2DPtr texPtr = spriteAtlas->texture;
		float2 textureSize = float2((float)texPtr->width + 0.001f, (float)texPtr->height + 0.001f);

		if (sprite->isNinePatch)
		{
			const edge4f& paddings = sprite->ninePatchPaddings;

			rectf rectInner;
			rectInner.x = rect.x + paddings.left;
			rectInner.y = rect.y + paddings.top;
			rectInner.set_right(rect.right() - paddings.right);
			rectInner.set_bottom(rect.bottom() - paddings.bottom);

			rectf spriteRectInner;
			spriteRectInner.x = spriteRect.x + paddings.left;
			spriteRectInner.y = spriteRect.y + paddings.top;
			spriteRectInner.set_right(spriteRect.right() - paddings.right);
			spriteRectInner.set_bottom(spriteRect.bottom() - paddings.bottom);
			
			rectf spriteRectNormalized;
			spriteRectNormalized.x = spriteRect.x / textureSize.x;
			spriteRectNormalized.y = spriteRect.y / textureSize.y;
			spriteRectNormalized.w = spriteRect.w / textureSize.x;
			spriteRectNormalized.h = spriteRect.h / textureSize.y;
			
			rectf spriteRectInnerNormalized;
			spriteRectInnerNormalized.x = spriteRectInner.x / textureSize.x;
			spriteRectInnerNormalized.y = spriteRectInner.y / textureSize.y;
			spriteRectInnerNormalized.w = spriteRectInner.w / textureSize.x;
			spriteRectInnerNormalized.h = spriteRectInner.h / textureSize.y;

			batch->AddNinePatchQuad(rect, rectInner, spriteRectNormalized, spriteRectInnerNormalized, color);
		}
		else
		{
			rectf spriteRectNormalized;
			spriteRectNormalized.x = spriteRect.x / textureSize.x;
			spriteRectNormalized.y = spriteRect.y / textureSize.y;
			spriteRectNormalized.w = spriteRect.w / textureSize.x;
			spriteRectNormalized.h = spriteRect.h / textureSize.y;

			batch->AddQuad(rect, spriteRectNormalized, color);
		}
	}
}

void UIImage::FetchSprite()
{
	if (spriteAtlas)
	{
		sprite = spriteAtlas->FindSprite(spriteName);
		if (sprite == nullptr)
		{
			Log::Error("Can not find sprite '%s' on atlas '%s'", spriteName.c_str(), spriteAtlas->assetPath.ToString().c_str());
		}
	}
	else
	{
		sprite = nullptr;
	}
}
