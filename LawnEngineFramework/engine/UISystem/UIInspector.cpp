#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "UIInspector.h"
#include "EntitySystem/World.h"
#include "UISystem/UISystem.h"
#include "UISystem/UILinearLayout.h"
#include "UISystem/UIText.h"
#include "UISystem/UIInput.h"
#include "UISystem/UIImage.h"
#include "UISystem/UIButton.h"

ConsoleVariable<AssetPath> CVarUIDefaultObjectInspectorContentBackgroundAtlas("UIDefault.ObjectInspector.Content.Background.Atlas", AssetPath(AST_ProjectPath, "DefaultUIAtlas.SpriteAtlas"));
ConsoleVariable<std::string> CVarUIDefaultObjectInspectorContentBackgroundSprite("UIDefault.ObjectInspector.Content.Background.Sprite", "Panel");

IMPL_CLASS_TYPE(UIObjectInspector);

UIObjectInspector::UIObjectInspector()
	: UIComponent(), targetObject(nullptr)
{
}

UIObjectInspector::~UIObjectInspector()
{
}

void UIObjectInspector::SetTargetObject(BaseObject* targetObject)
{
	NavigationClearAll();
	NavigationPushObject(targetObject);
}

void UIObjectInspector::OnShutdown()
{
	NavigationClearAll();
}

void UIObjectInspector::OnTargetObjectDestroyed()
{
	NavigationClearAll();
}

void UIObjectInspector::NavigationClearAll()
{
	while (!navigationStack.empty())
	{
		navigationStack.pop();
	}

	SetTargetObjectInternal(nullptr);
}

void UIObjectInspector::NavigationPushObject(BaseObject* obj)
{
	navigationStack.push(obj);

	SetTargetObjectInternal(navigationStack.top());
}

void UIObjectInspector::NavigationPopObject()
{
	navigationStack.pop();
	
	if (!navigationStack.empty())
	{
		SetTargetObjectInternal(navigationStack.top());
	}
	else
	{
		SetTargetObjectInternal(nullptr);
	}
}

void UIObjectInspector::SetTargetObjectInternal(BaseObject* newTargetObject)
{
	if (newTargetObject != this->targetObject)
	{
		if (this->targetObject)
		{
			DetachFromTargetObject();
		}

		this->targetObject = newTargetObject;

		if (this->targetObject)
		{
			AttachToTargetObject();
		}
	}
}

void UIObjectInspector::AttachToTargetObject()
{
	targetObject->destructionEvent.AddListener(this, [=](BaseObject*)
		{
			OnTargetObjectDestroyed();
		});

	reflector.reset(new Reflector());
	targetObject->Reflect(*reflector);

	inspectorContainerSO = this->GetSceneObject().AddChild("InspectorContainer");
	{
		UILinearLayout* layout = inspectorContainerSO->AddComponent<UILinearLayout>();
		layout->SetDirection(LinearLayoutDirection::Vertical);
		layout->SetItemMinSize(24);
	}

	if (navigationStack.size() > 1)
	{
		SceneObject* backButtonSO = inspectorContainerSO->AddChild("GoBack");

		UIButton* button = backButtonSO->AddComponent<UIButton>();
		button->GetSceneObject().GetComponentInChildren<UIText>()->SetText("<");
		button->OnClick = [=]()
		{
			this->NavigationPopObject();
		};
	}

	for (size_t i = 0; i < reflector->members.size(); i++)
	{
		ObjectMember& member = reflector->members[i];

		SceneObject* memberInspectorSO = inspectorContainerSO->AddChild(formatString("Inspector_%s", member.memberName.c_str()));

		UILinearLayout* layout = memberInspectorSO->AddComponent<UILinearLayout>();
		layout->SetDirection(LinearLayoutDirection::Horizontal);
		layout->SetItemSpacing(1);
		layout->SetLayoutMinSize(float2(0, 24));

		SceneObject* labelSO = memberInspectorSO->AddChild("Label");

		UIText* labelText = labelSO->AddComponent<UIText>();
		labelText->SetXAsLeftAlignedFixedSize(0, 1);
		labelText->SetYAsFilledWithPadding(1, 1);
		labelText->SetLayoutMinSize(float2(64, 0));
		labelText->SetLayoutFillWeight(float2(0.5f, 0));
		labelText->SetText(member.memberName);
		labelText->SetFontSize(24);
		labelText->SetAlignment(TextAlignmentHorizontal_Left, TextAlignmentVertical_Center);

		SceneObject* contentSO = memberInspectorSO->AddChild("Content");

		CreateInspectorUI(contentSO, member.GetAddr(targetObject), member.GetProperty());
	}
}

void UIObjectInspector::DetachFromTargetObject()
{
	targetObject->destructionEvent.RemoveListener(this);

	reflector.reset();

	if (inspectorContainerSO)
	{
		GetWorld()->DestroySceneObject(inspectorContainerSO);
		inspectorContainerSO = nullptr;
	}
}

void UIObjectInspector::CreateInspectorUI(SceneObject* so, void* obj, const BaseProperty* prop)
{
	if (prop->type == PropertyType::Primitive)
	{
		const BasePrimitiveProperty* primProp = reinterpret_cast<const BasePrimitiveProperty*>(prop);
		CreateInspectorUI(so, obj, primProp);
	}
	else if (prop->type == PropertyType::Object)
	{
		const BaseObjectProperty* objProp = reinterpret_cast<const BaseObjectProperty*>(prop);
		CreateInspectorUI(so, obj, objProp);
	}
	else if (prop->type == PropertyType::Array)
	{
		const BaseArrayProperty* arrayProp = reinterpret_cast<const BaseArrayProperty*>(prop);
		CreateInspectorUI(so, obj, arrayProp);
	}
	else if (prop->type == PropertyType::Map)
	{
		const BaseMapProperty* mapProp = reinterpret_cast<const BaseMapProperty*>(prop);
		CreateInspectorUI(so, obj, mapProp);
	}
	else if (prop->type == PropertyType::RawPointer)
	{
		const BaseRawPointerProperty* ptrProp = reinterpret_cast<const BaseRawPointerProperty*>(prop);
		CreateInspectorUI(so, obj, ptrProp);
	}
	else if (prop->type == PropertyType::SharedPointer)
	{
		const BaseSharedPointerProperty* ptrProp = reinterpret_cast<const BaseSharedPointerProperty*>(prop);
		CreateInspectorUI(so, obj, ptrProp);
	}
	else if (prop->type == PropertyType::Object)
	{
		const BaseObjectProperty* objProp = reinterpret_cast<const BaseObjectProperty*>(prop);
		CreateInspectorUI(so, obj, objProp);
	}
	else
	{
		CHECK(false);
	}
}

void UIObjectInspector::CreateInspectorUI(SceneObject* so, void* obj, const BasePrimitiveProperty* prop)
{
	UIInput* input = so->AddComponent<UIInput>();
	input->SetLayoutMinSize(float2(32, 24));
	input->SetText(prop->ToString(obj));
	input->textComponent->SetMinSizeFollowsContent(false);

	input->OnEnter = [=]()
	{
		std::string str = input->GetText();
		prop->FromString(obj, str);
		input->SetSelection(0, str.size());
	};
}

void UIObjectInspector::CreateInspectorUI(SceneObject* so, void* obj, const BaseObjectProperty* prop)
{
	ReflectiveObject* reflectiveObject = prop->GetPointer(obj);

	Type* type = reflectiveObject->GetType();

	if (type->IsSameOrChildOf(GetStaticType<BaseObject>()))
	{
		BaseObject* baseObject = reinterpret_cast<BaseObject*>(reflectiveObject);

		UILinearLayout* layout = so->AddComponent<UILinearLayout>();
		layout->SetDirection(LinearLayoutDirection::Horizontal);
		layout->SetItemSpacing(2);

		UIText* text = so->AddChild("Text")->AddComponent<UIText>();
		text->SetText(type->TypeName());
		text->SetAlignment(TextAlignmentHorizontal_Left, TextAlignmentVertical_Center);

		UIButton* button = so->AddChild("Button")->AddComponent<UIButton>();
		button->SetLayoutFillWeight(float2(0.2f, 0));
		button->SetLayoutMinSize(float2(64, 0));
		button->GetSceneObject().GetComponentInChildren<UIText>()->SetText(">");
		button->OnClick = [=]()
		{
			this->NavigationPushObject(baseObject);
		};
	}
}

void UIObjectInspector::CreateInspectorUI(SceneObject* so, void* obj, const BaseArrayProperty* arrayProp)
{

}

void UIObjectInspector::CreateInspectorUI(SceneObject* so, void* obj, const BaseMapProperty* mapProp)
{

}

void UIObjectInspector::CreateInspectorUI(SceneObject* so, void* obj, const BaseRawPointerProperty* ptrProp)
{

}

void UIObjectInspector::CreateInspectorUI(SceneObject* so, void* obj, const BaseSharedPointerProperty* ptrProp)
{

}
