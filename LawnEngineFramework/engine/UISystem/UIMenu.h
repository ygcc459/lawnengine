#pragma once

#include "UICommon.h"
#include "UIComponent.h"

class UIText;

class UIMenu : public UIComponent
{
	DECL_CLASS_TYPE(UIMenu, UIComponent);

public:
	
public:
	UIMenu();
	virtual ~UIMenu();

	virtual void Reflect(Reflector& reflector) override;

	virtual bool CanDraw() override { return false; }
};
