#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "UIButton.h"
#include "RenderSystem/Sprite.h"
#include "UISystem.h"
#include "RenderSystem/Material.h"
#include "EntitySystem/SceneObject.h"
#include "UIImage.h"
#include "UIText.h"

ConsoleVariable<AssetPath> CVarUIDefaultButtonNormalAtlas("UIDefault.Button.Image.Atlas", AssetPath(AST_ProjectPath, "DefaultUIAtlas.SpriteAtlas"));
ConsoleVariable<std::string> CVarUIDefaultButtonNormalSprite("UIDefault.Button.Normal.Sprite", "ButtonNormal");
ConsoleVariable<std::string> CVarUIDefaultButtonHoverSprite("UIDefault.Button.Hover.Sprite", "ButtonHover");
ConsoleVariable<std::string> CVarUIDefaultButtonPressedSprite("UIDefault.Button.Pressed.Sprite", "ButtonPressed");

IMPL_CLASS_TYPE(UIButton);

UIButton::UIButton()
	: targetImage(nullptr)
	, targetText(nullptr)
	, normalColor(float4(1, 1, 1, 1))
	, hoverColor(float4(1, 1, 1, 1))
	, pressColor(float4(1, 1, 1, 1))
{
}

UIButton::~UIButton()
{
}

void UIButton::OnStartup()
{
	Super::OnStartup();

	AssetManager& assetManager = g_lawnEngine.getAssetManager();
	SpriteAtlasPtr atlas = assetManager.Load<SpriteAtlas>(CVarUIDefaultButtonNormalAtlas.GetValue());

	SceneObject* bgSO = GetSceneObject().AddChild("Background");
	UIImage* bgImage = bgSO->AddComponent<UIImage>();

	SceneObject* textSO = GetSceneObject().AddChild("Text");
	UIText* text1 = textSO->AddComponent<UIText>();
	text1->SetAlignment(TextAlignmentHorizontal_Center, TextAlignmentVertical_Center);

	SetSprites(atlas, CVarUIDefaultButtonNormalSprite.GetValue(), CVarUIDefaultButtonHoverSprite.GetValue(), CVarUIDefaultButtonPressedSprite.GetValue());
	SetTargetImage(bgImage);
	SetTargetText(text1);
}

void UIButton::OnShutdown()
{
	Super::OnShutdown();
}

void UIButton::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("spriteAtlas", &Self::spriteAtlas);
	reflector.AddMember("targetImage", &Self::targetImage);
	reflector.AddMember("normalSpriteName", &Self::normalSpriteName);
	reflector.AddMember("normalColor", &Self::normalColor);
	reflector.AddMember("hoverSpriteName", &Self::hoverSpriteName);
	reflector.AddMember("hoverColor", &Self::hoverColor);
	reflector.AddMember("pressSpriteName", &Self::pressSpriteName);
	reflector.AddMember("pressColor", &Self::pressColor);

	reflector.AddFunction("SetSprites", &Self::SetSprites);
	reflector.AddFunction("SetColors", &Self::SetColors);
}

MaterialPtr UIButton::GetMaterial()
{
	return MaterialPtr();
}

Texture2DPtr UIButton::GetTexture()
{
	return Texture2DPtr();
}

void UIButton::UpdateMesh(UIMeshBatch* batch)
{
}

void UIButton::SetTargetImage(UIImage* targetImage)
{
	this->targetImage = targetImage;

	if (targetImage)
	{
		targetImage->SetSprite(spriteAtlas, normalSpriteName);
		targetImage->SetColor(normalColor);
	}

	MarkMeshDirty();
}

void UIButton::SetTargetText(UIText* targetText)
{
	this->targetText = targetText;
}

void UIButton::OnMouseEvent(const MouseEvent& ev, const MouseState& state)
{
	if (ev.type == MET_Enter)
	{
		if (targetImage)
		{
			targetImage->SetSprite(spriteAtlas, hoverSpriteName);
			targetImage->SetColor(hoverColor);
		}
	}
	else if (ev.type == MET_Leave)
	{
		if (targetImage)
		{
			targetImage->SetSprite(spriteAtlas, normalSpriteName);
			targetImage->SetColor(normalColor);
		}
	}
	else if (ev.type == MET_ButtonDown)
	{
		if (targetImage)
		{
			targetImage->SetSprite(spriteAtlas, pressSpriteName);
			targetImage->SetColor(pressColor);
		}
	}
	else if (ev.type == MET_ButtonUp)
	{
		if (targetImage)
		{
			targetImage->SetSprite(spriteAtlas, hoverSpriteName);
			targetImage->SetColor(hoverColor);
		}

		if (OnClick)
		{
			OnClick();
		}
	}
}
