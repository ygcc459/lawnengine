#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "UISceneHierarchyView.h"
#include "EntitySystem/World.h"
#include "EntitySystem/SceneObject.h"
#include "EntitySystem/Transform.h"
#include "UISystem/UISystem.h"
#include "UISystem/UILinearLayout.h"
#include "UISystem/UIText.h"
#include "UISystem/UIInput.h"
#include "UISystem/UIImage.h"
#include "UISystem/UIButton.h"

ConsoleVariable<AssetPath> CVarUIDefaultSceneHierarchyItemBackgroundAtlas("UIDefault.SceneHierarchy.Item.Background.Atlas", AssetPath(AST_ProjectPath, "DefaultUIAtlas.SpriteAtlas"));
ConsoleVariable<std::string> CVarUIDefaultSceneHierarchyItemBackgroundSprite("UIDefault.SceneHierarchy.Item.Background.Sprite", "White");

IMPL_CLASS_TYPE(UISceneHierarchyView);

UISceneHierarchyView::UISceneHierarchyView()
	: UIComponent(), containerSO(nullptr)
{
}

UISceneHierarchyView::~UISceneHierarchyView()
{
}

void UISceneHierarchyView::OnStartup()
{
	Super::OnStartup();

	AssetManager& assetManager = g_lawnEngine.getAssetManager();
	this->atlas = assetManager.Load<SpriteAtlas>(CVarUIDefaultSceneHierarchyItemBackgroundAtlas.GetValue());

	containerSO = GetSceneObject().AddChild("Container");
	UILinearLayout* layout = containerSO->AddComponent<UILinearLayout>();
	layout->SetDirection(LinearLayoutDirection::Vertical);
	layout->SetItemMinSize(24);
	layout->SetFillVertical(false);
	layout->SetFillHorizontal(true);

	World& world = g_lawnEngine.getWorld();
	std::vector<SceneObject*>& sceneObjects = world.GetSceneObjects();
	for (size_t i = 0; i < sceneObjects.size(); i++)
	{
		SceneObject* so = sceneObjects[i];
		if (so->GetParent() == nullptr)
		{
			CreateSceneObjectUIRecursive(so);
		}
	}
}

void UISceneHierarchyView::OnShutdown()
{
	Super::OnShutdown();
}

UISceneHierarchyView::SceneObjectInfo* UISceneHierarchyView::GetSceneObjectInfo(SceneObject* so, bool autoCreate /*= true*/)
{
	auto it = sceneObjectInfoMap.find(so);
	if (it != sceneObjectInfoMap.end())
	{
		return it->second.get();
	}
	else if (autoCreate)
	{
		// create parent info if not exist
		SceneObjectInfo* parentInfo = (so->GetParent() ? GetSceneObjectInfo(so->GetParent(), true) : nullptr);

		SceneObjectInfo* info = new SceneObjectInfo();
		info->targetSO = so;
		info->depth = (so->GetParent() ? (parentInfo->depth + 1) : 0);

		so->destructionEvent.AddListener(this, [=](BaseObject* obj)
			{
				//TODO: when destructionEvent is called, SceneObject is already destructed
				//      GetType() only calls BaseObject::GetType() and thus will return BaseObject type
				//CHECK(obj->GetType()->IsSameOrChildOf(GetStaticType<SceneObject>()));
				this->OnSceneObjectDestroy(reinterpret_cast<SceneObject*>(obj));
			});

		sceneObjectInfoMap.insert(std::make_pair(so, std::unique_ptr<SceneObjectInfo>(info)));

		return info;
	}
	else
	{
		return nullptr;
	}
}

void UISceneHierarchyView::CreateSceneObjectUIRecursive(SceneObject* so)
{
	// exclude UISceneHierarchyView UIs
	if (so == &this->GetSceneObject())
	{
		return;
	}

	SceneObjectInfo* info = GetSceneObjectInfo(so, true);
	if (info == nullptr)
	{
		return;
	}

	// UI already created
	if (info->UISO == nullptr)
	{
		info->UISO = containerSO->AddChild(formatString("UI: %s", so->name.c_str()));
		info->backgroundImage = info->UISO->AddComponent<UIImage>();
 		info->backgroundImage->SetSprite(nullptr, "");

		SceneObject* itemLayoutSO = info->UISO->AddChild("ItemLayout");
		info->layout = itemLayoutSO->AddComponent<UILinearLayout>();
		info->layout->SetXAsFilledWithPadding(info->depth * 24, 0);
		info->layout->SetDirection(LinearLayoutDirection::Horizontal);
		info->layout->SetItemMinSize(0);
		info->layout->SetItemSpacing(0);

		SceneObject* expandButtonSO = itemLayoutSO->AddChild("Expand");
		info->expandButton = expandButtonSO->AddComponent<UISelectableText>();
		info->expandButton->SetLayoutMinSize(float2(24, 0));
		info->expandButton->SetLayoutFillWeight(float2(0, 0));
		info->expandButton->SetMinSizeFollowsContent(false);
		info->expandButton->SetAlignment(TextAlignmentHorizontal_Center, TextAlignmentVertical_Center);
		if (so->transform->children.size() != 0)  //have children
		{
			info->expandButton->SetText("-");
			info->expandButton->OnLeftClick = [=]()
			{
				OnSceneObjectSwitchExpand(so);
			};
		}
		else  //have no children
		{
		}

		SceneObject* nameButtonSO = itemLayoutSO->AddChild("Name");
		info->nameButton = nameButtonSO->AddComponent<UISelectableText>();
		info->nameButton->SetLayoutMinSize(float2(64, 0));
		info->nameButton->SetLayoutFillWeight(float2(1, 0));
		info->nameButton->SetXAsFilledWithPadding(4, 4);
		info->nameButton->SetText(so->name);
		info->nameButton->SetMinSizeFollowsContent(false);
		info->nameButton->SetAlignment(TextAlignmentHorizontal_Left, TextAlignmentVertical_Center);
		info->nameButton->OnLeftClick = [=]()
		{
			OnSceneObjectSelect(so);
		};
	}

	// create UI for children
	for (size_t iChild = 0; iChild < so->transform->children.size(); iChild++)
	{
		SceneObject& childSO = so->transform->children[iChild]->GetSceneObject();
		CreateSceneObjectUIRecursive(&childSO);
	}
}

void UISceneHierarchyView::OnSceneObjectDestroy(SceneObject* so)
{
	sceneObjectInfoMap.erase(so);
}

void UISceneHierarchyView::OnSceneObjectSwitchExpand(SceneObject* so)
{
	SceneObjectInfo* info = GetSceneObjectInfo(so, false);
	if (info == nullptr)
	{
		return;
	}

	info->expanded = !info->expanded;
	
	info->expandButton->SetText(info->expanded ? "-" : "+");

	World& world = g_lawnEngine.getWorld();
	std::vector<SceneObject*>& sceneObjects = world.GetSceneObjects();
	for (size_t i = 0; i < sceneObjects.size(); i++)
	{
		SceneObject* so = sceneObjects[i];
		if (so->GetParent() == nullptr)
		{
			UpdateUIVisiblityForExpandRecursive(so, true);
		}
	}
}

void UISceneHierarchyView::UpdateUIVisiblityForExpandRecursive(SceneObject* so, bool parentExpandedFinal)
{
	SceneObjectInfo* info = GetSceneObjectInfo(so, false);
	if (info == nullptr)
	{
		return;
	}

	UIComponent* uiComp = info->UISO->GetComponent<UIComponent>();
	if (uiComp)
	{
		uiComp->SetVisible(parentExpandedFinal);
	}

	bool curExpandedFinal = parentExpandedFinal && info->expanded;

	for (size_t iChild = 0; iChild < so->transform->children.size(); iChild++)
	{
		SceneObject& childSO = so->transform->children[iChild]->GetSceneObject();

		UpdateUIVisiblityForExpandRecursive(&childSO, curExpandedFinal);
	}
}

void UISceneHierarchyView::OnSceneObjectSelect(SceneObject* so)
{

}
