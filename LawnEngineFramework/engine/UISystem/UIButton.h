#pragma once

#include "UICommon.h"
#include "UIComponent.h"
#include "UICanvas.h"

class UIImage;

class UIButton : public UIComponent
{
	DECL_CLASS_TYPE(UIButton, UIComponent);

public:
	SpriteAtlasPtr spriteAtlas;

	UIImage* targetImage;
	UIText* targetText;

	std::string normalSpriteName;
	float4 normalColor;

	std::string hoverSpriteName;
	float4 hoverColor;

	std::string pressSpriteName;
	float4 pressColor;

	std::function<void(void)> OnClick;

public:
	UIButton();
	virtual ~UIButton();

	virtual void OnStartup() override;
	virtual void OnShutdown() override;

	virtual void Reflect(Reflector& reflector) override;

	void SetTargetImage(UIImage* targetImage);
	void SetTargetText(UIText* targetText);

	void SetSprites(SpriteAtlasPtr spriteAtlas, const std::string& normalSpriteName, const std::string& hoverSpriteName, const std::string& pressSpriteName)
	{
		this->spriteAtlas = spriteAtlas;
		this->normalSpriteName = normalSpriteName;
		this->hoverSpriteName = hoverSpriteName;
		this->pressSpriteName = pressSpriteName;

		MarkMeshDirty();
	}

	void SetColors(const float4& normalColor, const float4& hoverColor, const float4& pressColor)
	{
		this->normalColor = normalColor;
		this->hoverColor = hoverColor;
		this->pressColor = pressColor;

		MarkMeshDirty();
	}

	virtual bool CanDraw() override { return false; }

	virtual MaterialPtr GetMaterial() override;
	virtual Texture2DPtr GetTexture() override;
	virtual void UpdateMesh(UIMeshBatch* batch) override;

	virtual bool HandleMouseEvents() { return true; }
	virtual void OnMouseEvent(const MouseEvent& ev, const MouseState& state) override;
};
