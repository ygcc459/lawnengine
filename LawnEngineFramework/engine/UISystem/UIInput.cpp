#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "UIInput.h"
#include "EntitySystem/SceneObject.h"
#include "UIImage.h"
#include <cctype>

ConsoleVariable<AssetPath> CVarUIDefaultInputBackgroundAtlas("UIDefault.Input.Background.Atlas", AssetPath(AST_ProjectPath, "DefaultUIAtlas.SpriteAtlas"));
ConsoleVariable<std::string> CVarUIDefaultInputBackgroundSprite("UIDefault.Input.Background.Sprite", "Panel");

ConsoleVariable<AssetPath> CVarUIDefaultInputCaretAtlas("UIDefault.Input.Caret.Atlas", AssetPath(AST_ProjectPath, "DefaultUIAtlas.SpriteAtlas"));
ConsoleVariable<std::string> CVarUIDefaultInputCaretSprite("UIDefault.Input.Caret.Sprite", "White");

ConsoleVariable<int> CVarUIDefaultInputTextAtlas("UIDefault.Input.Text.FontSize", 40);

IMPL_CLASS_TYPE(UIInput);

UIInput::UIInput()
	: textComponent(nullptr)
	, caretImage(nullptr)
	, selectionBegin(0)
	, selectionEnd(0)
	, charRectsDirty(false)
{
}

UIInput::~UIInput()
{
}

void UIInput::OnStartup()
{
	Super::OnStartup();

	AssetManager& assetManager = g_lawnEngine.getAssetManager();
	SpriteAtlasPtr bgAtlas = assetManager.Load<SpriteAtlas>(CVarUIDefaultInputBackgroundAtlas.GetValue());
	SpriteAtlasPtr caretAtlas = assetManager.Load<SpriteAtlas>(CVarUIDefaultInputCaretAtlas.GetValue());

	SceneObject* bgSO = GetSceneObject().AddChild("Background");
	UIImage* bgImage = bgSO->AddComponent<UIImage>();
	bgImage->SetSprite(bgAtlas, CVarUIDefaultInputBackgroundSprite.GetValue());

	SceneObject* caretSO = GetSceneObject().AddChild("Caret");
	UIImage* caretImage = caretSO->AddComponent<UIImage>();
	caretImage->SetSprite(caretAtlas, CVarUIDefaultInputCaretSprite.GetValue());

	SceneObject* textSO = GetSceneObject().AddChild("Text");
	UIText* text1 = textSO->AddComponent<UIText>();
	text1->SetRelativePositionsAndPaddings(edge4f(0, 0, 1, 1), edge4f(2, 2, 2, 2));
	text1->SetMinSizeFollowsContent(false);
	text1->SetFontSize(CVarUIDefaultInputTextAtlas.GetValue());
	text1->SetAlignment(TextAlignmentHorizontal_Left, TextAlignmentVertical_Center);

	this->textComponent = text1;
	this->caretImage = caretImage;

	SetText(this->text);
	SetSelection(this->selectionBegin, this->selectionEnd);
}

void UIInput::OnShutdown()
{
	Super::OnShutdown();
}

void UIInput::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);
}

void UIInput::SetText(const std::string& text)
{
	this->text = text;

	this->charRectsDirty = true;

	if (textComponent)
	{
		textComponent->SetText(text);
	}
}

void UIInput::SetCaret(int pos)
{
	SetSelection(pos, pos);
}

void UIInput::SetSelection(int begin, int end)
{
	this->selectionBegin = math::clamp(begin, 0, text.size());
	this->selectionEnd = math::clamp(end, 0, text.size());

	if (caretImage)
	{
		UpdateCharRects();

		rectf wholeTextRect = this->textComponent->GetRect().to_rect();

		if (GetSelectionLength() == 0)
		{
			float caretX;
			if (selectionBegin >= charRects.size())
			{
				caretX = (charRects.size() > 0) ? (charRects.back().xmax() - wholeTextRect.xmin()) : 0;
				caretX += 2;
			}
			else
			{
				caretX = charRects[selectionBegin].xmin() - wholeTextRect.xmin();
			}

			caretImage->SetXAsLeftAlignedFixedSize(caretX, 2);
			caretImage->SetYAsFilledWithPadding(6, 6);
			caretImage->SetColor(float4(1, 1, 1, 0.75f));
		}
		else
		{
			float caretBeginX;
			if (selectionBegin >= charRects.size())
			{
				caretBeginX = (charRects.size() > 0) ? (charRects.back().xmax() - wholeTextRect.xmin()) : 0;
			}
			else
			{
				caretBeginX = charRects[selectionBegin].xmin() - wholeTextRect.xmin();
			}

			float caretEndX;
			if (selectionEnd >= charRects.size())
			{
				caretEndX = (charRects.size() > 0) ? (charRects.back().xmax() - wholeTextRect.xmin()) : 0;
				caretEndX += 2;
			}
			else
			{
				caretEndX = charRects[selectionEnd].xmin() - wholeTextRect.xmin();
			}

			caretImage->SetXAsLeftAlignedFixedSize(caretBeginX, caretEndX - caretBeginX + 2);
			caretImage->SetYAsFilledWithPadding(6, 6);
			caretImage->SetColor(float4(0, 0, 1, 0.5f));
		}
	}
}

int UIInput::GetSelectionLength() const
{
	return math::abs(selectionEnd - selectionBegin);
}

int UIInput::GetCharPosition(const float2& mousePos)
{
	if (!textComponent)
	{
		return -1;
	}

	rectf wholeTextRect = this->textComponent->GetRect().to_rect();
	if (!wholeTextRect.contains(mousePos))
	{
		return -1;
	}

	UpdateCharRects();

	for (int i = 0; i < charRects.size(); i++)
	{
		const rectf& rect = charRects[i];

		float judgeX = math::lerp(rect.xmin(), rect.xmax(), 0.75f);

		if (rect.xmin() <= mousePos.x && mousePos.x <= judgeX)
		{
			return i;
		}
		else if (judgeX <= mousePos.x && mousePos.x <= rect.xmax())
		{
			return i + 1;
		}
	}

	float textXMax = (charRects.size() > 0) ? charRects.back().xmax() : wholeTextRect.xmin();
	if (mousePos.x >= textXMax)
	{
		return charRects.size();
	}

	return -1;
}


void UIInput::UpdateCharRects(bool force /*= false*/)
{
	if (charRectsDirty || force)
	{
		charRectsDirty = false;

		charRects.clear();
		this->textComponent->Measure(charRects);
	}
}

void UIInput::SetSelctionText(const std::string& v)
{
	if (GetSelectionLength() == 0)
	{
		text.insert(text.begin() + selectionBegin, v.begin(), v.end());
	}
	else
	{
		int selectionMin = std::min(selectionBegin, selectionEnd);
		int selectionMax = std::max(selectionBegin, selectionEnd);

		text.replace(text.begin() + selectionMin, text.begin() + selectionMax, v.begin(), v.end());
	}

	SetText(text);
}

void UIInput::OnMouseEvent(const MouseEvent& ev, const MouseState& state)
{
	if (ev.type == MET_ButtonDown && ev.button == MB_Left)
	{
		int pos = GetCharPosition(state.PositionXY());
		if (pos >= 0)
		{
			SetCaret(pos);
		}

		SetAsMouseFocus();
		SetAsKeyFocus();
	}
	else if (ev.type == MET_Move)
	{
		if (HasMouseFocus())
		{
			int pos = GetCharPosition(state.PositionXY());
			if (pos >= 0)
			{
				SetSelection(selectionBegin, pos);
			}
		}
	}
	else if (ev.type == MET_ButtonUp && ev.button == MB_Left)
	{
		if (HasMouseFocus())
		{
			CancelMouseFocus();
		}
	}
}

void UIInput::OnKeyEvent(const KeyEvent& ev, void* customData)
{
	if (ev.type == KET_CharacterInput)
	{
		char ch = (char)ev.text;

		if (!::iscntrl(ch))
		{
			char cc[2] = { ch, 0 };
			std::string inputText = cc;

			SetSelctionText(inputText);
			SetCaret(selectionBegin + 1);
		}
	}
	else if (ev.type == KET_KeyPressed)
	{
		switch (ev.keycode)
		{
		case KC_LEFT:
		{
			if (GetSelectionLength() == 0)
				SetCaret(selectionBegin - 1);
			else
				SetCaret(selectionBegin);
			break;
		}
		case KC_RIGHT:
		{
			if (GetSelectionLength() == 0)
				SetCaret(selectionBegin + 1);
			else
				SetCaret(selectionEnd);
			break;
		}
		case KC_RETURN:
		{
			if (OnEnter)
			{
				OnEnter();
			}
			break;
		}
		case KC_DELETE:
		{
			if (GetSelectionLength() == 0)
			{
				int charIndexToDelete = selectionBegin;
				if (charIndexToDelete >= 0 && charIndexToDelete < text.size())
				{
					text.erase(charIndexToDelete, 1);
					SetText(text);
					SetCaret(charIndexToDelete);
				}
			}
			else
			{
				int oldSelectionBegin = selectionBegin;
				SetSelctionText("");
				SetCaret(oldSelectionBegin);
			}
			break;
		}
		case KC_BACK:
		{
			if (GetSelectionLength() == 0)
			{
				int charIndexToDelete = selectionBegin - 1;
				if (charIndexToDelete >= 0 && charIndexToDelete < text.size())
				{
					text.erase(charIndexToDelete, 1);
					SetText(text);
					SetCaret(charIndexToDelete);
				}
			}
			else
			{
				int oldSelectionBegin = selectionBegin;
				SetSelctionText("");
				SetCaret(oldSelectionBegin);
			}
			break;
		}
		}
	}
}
