#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "UIText.h"
#include "UISystem.h"
#include "RenderSystem/Material.h"

ConsoleVariable<int> CVarUIDefaultTextFontSize("UIDefault.Text.FontSize", 24);

IMPL_CLASS_TYPE(UIText);

UIText::UIText()
	: UIComponent()
	, color(float4(1, 1, 1, 1))
	, fontSize(24)
	, alignmentHorizontal(TextAlignmentHorizontal_Left)
	, alignmentVertical(TextAlignmentVertical_Top)
	, minSizeFollowsContent(true)
{
}

UIText::~UIText()
{
}

void UIText::OnStartup()
{
	Super::OnStartup();

	fontSize = CVarUIDefaultTextFontSize.GetValue();
}

void UIText::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("text", &Self::text);
	reflector.AddMember("fontSize", &Self::fontSize);
	reflector.AddMember("alignmentHorizontal", &Self::alignmentHorizontal);
	reflector.AddMember("alignmentVertical", &Self::alignmentVertical);
	reflector.AddMember("color", &Self::color);
	reflector.AddMember("minSizeFollowsContent", &Self::minSizeFollowsContent);
	reflector.AddMember("material", &Self::material);
}

void UIText::UpdateRequiredSize(UIComponentCacheData& cacheData)
{
	Super::UpdateRequiredSize(cacheData);

	if (this->minSizeFollowsContent)
	{
		std::vector<rectf> charRects;
		Measure(charRects);

		rectf textRect;
		for (rectf charRect : charRects)
			textRect = textRect.union_with(charRect);

		float2 textSize = textRect.size() + float2(edgePaddings.left + edgePaddings.right, edgePaddings.top + edgePaddings.bottom);

		this->requiredSize = max(this->requiredSize, textSize);
	}
}

MaterialPtr UIText::GetMaterial()
{
	if (material)
		return material;
	else
		return g_lawnEngine.getUISystem().GetDefaultTextMaterial();
}

Texture2DPtr UIText::GetTexture()
{
	return TextRenderer::GetInstance()->GetTexture();
}

void UIText::UpdateMesh(UIMeshBatch* batch)
{
	rectf rect = GetRect().to_rect();

	TextRenderParams params;
	params.fontSize = this->fontSize;
	params.maxSize.x = rect.w;
	params.maxSize.y = rect.h;
	params.alignX = alignmentHorizontal;
	params.alignY = alignmentVertical;
	params.color = this->color;
	TextRenderer::GetInstance()->Render(batch, rect, text, params);
}

void UIText::OnMouseEvent(const MouseEvent& ev, const MouseState& state)
{
	if (MouseEventHandler)
		MouseEventHandler(ev, state);
}

void UIText::Measure(std::vector<rectf>& outCharRects)
{
	rectf rect = GetRect().to_rect();

	TextRenderParams params;
	params.fontSize = this->fontSize;
	params.maxSize.x = rect.w;
	params.maxSize.y = rect.h;
	params.alignX = alignmentHorizontal;
	params.alignY = alignmentVertical;
	params.color = this->color;
	TextRenderer::GetInstance()->Measure(outCharRects, rect, text, params);
}
