#pragma once

#include <vcpp/la/color32.h>

class UICanvas;
class UIComponent;
class UIMeshBatch;
class UIImage;
class UIText;

class Sprite;
typedef std::shared_ptr<Sprite> SpritePtr;

class SpriteAtlas;
typedef std::shared_ptr<SpriteAtlas> SpriteAtlasPtr;
