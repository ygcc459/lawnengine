#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "UIMenu.h"

IMPL_CLASS_TYPE(UIMenu);

UIMenu::UIMenu()
	: UIComponent()
{
}

UIMenu::~UIMenu()
{
}

void UIMenu::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);
}
