#pragma once

#include "UICommon.h"
#include "UIComponent.h"
#include "BaseObject.h"

class UIStyleSheet : public BaseObject
{
	DECL_CLASS_TYPE(UIStyleSheet, BaseObject);

public:
	UIStyleSheet();
	virtual ~UIStyleSheet();

	virtual bool OnLoad(Archive& archive) override;

	void Apply(SceneObject* so, bool recursive = true);

private:
	struct StyleMemberItem
	{
		std::string memberName;
		std::string memberValue;

		StyleMemberItem(const std::string& memberName, const std::string& memberValue)
			: memberName(memberName), memberValue(memberValue)
		{}
	};
	
	struct StyleFunctionItem
	{
		std::string functionName;
		std::vector<std::string> paramValues;

		StyleFunctionItem(const std::string& functionName)
			: functionName(functionName)
		{}
	};

	struct StyleItemsForType
	{
		Type* targetType;
		std::vector<StyleMemberItem> members;
		std::vector<StyleFunctionItem> functions;
	};

	std::map<Type*, StyleItemsForType> typeToStyleItemsMap;
};
