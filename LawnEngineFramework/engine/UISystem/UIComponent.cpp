#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "UIComponent.h"
#include "EntitySystem/Transform.h"
#include "EntitySystem/SceneObject.h"
#include "UICanvas.h"

IMPL_ABSTRACT_CLASS_TYPE(UIComponent);

UIComponent::UIComponent()
	: edgeRelativePositions(0.0f, 0.0f, 1.0f, 1.0f)
	, edgePaddings(0.0f, 0.0f, 0.0f, 0.0f)
	, rect(0.0f, 0.0f, 1.0f, 1.0f)
	, layoutMinSize(0, 0)
	, layoutFillWeight(1, 1)
	, canvas(nullptr)
	, isVisible(true)
	, requiredSize(0, 0)
	, minSizeFollowsChildren(true)
	, layoutComponentId(-1)
{
	SetRelativePositionsAndPaddings(edge4f(0, 0, 1, 1), edge4f(2, 2, 2, 2));
}

UIComponent::~UIComponent()
{
}

void UIComponent::OnStartup()
{
	UpdateParentHierarchyDependentData();

	canvas = GetSceneObject().GetComponentInParents<UICanvas>();
	if (canvas)
	{
		canvas->RegisterComponent(this);
	}
}

void UIComponent::OnShutdown()
{
	CancelMouseFocus();
	CancelKeyFocus();

	if (canvas)
	{
		canvas->UnRegisterComponent(this);

		layoutComponentId = -1;

		canvas = nullptr;
	}
}

void UIComponent::SetAbsoluteLayout(edge4f values)
{
	Layout(UILayoutDimension::Left).Assign(values.left);
	Layout(UILayoutDimension::Right).Assign(values.right);
	Layout(UILayoutDimension::Top).Assign(values.top);
	Layout(UILayoutDimension::Bottom).Assign(values.bottom);
	MarkLayoutDirty();
}

void UIComponent::SetRelativePositionsAndPaddings(const edge4f& edgeRelativePositions, const edge4f& edgePaddings)
{
	Layout(UILayoutDimension::Left).Lerp(nullptr, UILayoutDimension::Left, UILayoutDimension::Right, edgeRelativePositions.left, edgePaddings.left);
	Layout(UILayoutDimension::Right).Lerp(nullptr, UILayoutDimension::Left, UILayoutDimension::Right, edgeRelativePositions.right, -edgePaddings.right);
	Layout(UILayoutDimension::Top).Lerp(nullptr, UILayoutDimension::Top, UILayoutDimension::Bottom, edgeRelativePositions.top, edgePaddings.top);
	Layout(UILayoutDimension::Bottom).Lerp(nullptr, UILayoutDimension::Top, UILayoutDimension::Bottom, edgeRelativePositions.bottom, -edgePaddings.bottom);

	this->edgeRelativePositions = edgeRelativePositions;
	this->edgePaddings = edgePaddings;
	MarkLayoutDirty();
}

void UIComponent::SetXAsLeftAlignedFixedSize(float xmin, float width)
{
	Layout(UILayoutDimension::Left).Add(nullptr, UILayoutDimension::Left, xmin);
	Layout(UILayoutDimension::Right).Add(this, UILayoutDimension::Left, width);

	this->edgeRelativePositions.left = 0;
	this->edgeRelativePositions.right = 0;
	this->edgePaddings.left = xmin;
	this->edgePaddings.right = -(xmin + width);
	MarkLayoutDirty();
}

void UIComponent::SetXAsRightAlignedFixedSize(float paddingRight, float width)
{
	Layout(UILayoutDimension::Right).Add(nullptr, UILayoutDimension::Right, -paddingRight);
	Layout(UILayoutDimension::Left).Add(this, UILayoutDimension::Right, -width);

	this->edgeRelativePositions.left = 1;
	this->edgeRelativePositions.right = 1;
	this->edgePaddings.left = -(paddingRight + width);
	this->edgePaddings.right = -paddingRight;
	MarkLayoutDirty();
}

void UIComponent::SetXAsFilledWithPadding(float paddingLeft, float paddingRight)
{
	Layout(UILayoutDimension::Left).Add(nullptr, UILayoutDimension::Left, paddingLeft);
	Layout(UILayoutDimension::Right).Add(nullptr, UILayoutDimension::Right, -paddingRight);

	this->edgeRelativePositions.left = 0;
	this->edgeRelativePositions.right = 1;
	this->edgePaddings.left = paddingLeft;
	this->edgePaddings.right = paddingRight;
	MarkLayoutDirty();
}

void UIComponent::SetYAsTopAlignedFixedSize(float ymin, float height)
{
	Layout(UILayoutDimension::Top).Add(nullptr, UILayoutDimension::Top, ymin);
	Layout(UILayoutDimension::Bottom).Add(this, UILayoutDimension::Top, height);

	this->edgeRelativePositions.top = 0;
	this->edgeRelativePositions.bottom = 0;
	this->edgePaddings.top = ymin;
	this->edgePaddings.bottom = -(ymin + height);
	MarkLayoutDirty();
}

void UIComponent::SetYAsFilledWithPadding(float paddingTop, float paddingBottom)
{
	Layout(UILayoutDimension::Top).Add(nullptr, UILayoutDimension::Top, paddingTop);
	Layout(UILayoutDimension::Bottom).Add(nullptr, UILayoutDimension::Bottom, -paddingBottom);

	this->edgeRelativePositions.top = 0;
	this->edgeRelativePositions.bottom = 1;
	this->edgePaddings.top = paddingTop;
	this->edgePaddings.bottom = paddingBottom;
	MarkLayoutDirty();
}

void UIComponent::SetLayoutMinSize(const float2& v)
{
	layoutMinSize = v;
	MarkLayoutDirty();
}

void UIComponent::SetLayoutFillWeight(const float2& v)
{
	layoutFillWeight = v;
	MarkLayoutDirty();
}

void UIComponent::MarkLayoutDirty()
{
	if (canvas)
	{
		canvas->MarkLayoutDirty();
	}
}

void UIComponent::MarkMeshDirty()
{
	if (canvas)
	{
		canvas->MarkMeshDirty();
	}
}

void UIComponent::SetVisible(bool v)
{
	isVisible = v;
	MarkLayoutDirty();
}

bool UIComponent::IsVisibleInHierarchy() const
{
	if (!IsVisibleSelf())
		return false;

	UIComponent* parentUIComp = GetSceneObject().GetComponentInParents<UIComponent>(false);
	bool parentIsVisible = parentUIComp ? parentUIComp->IsVisibleInHierarchy() : true;
	if (!parentIsVisible)
		return false;

	return true;
}

void UIComponent::SetAsMouseFocus()
{
	canvas->SetMouseFocus(this);
}

void UIComponent::CancelMouseFocus()
{
	if (HasMouseFocus())
	{
		canvas->SetMouseFocus(nullptr);
	}
}

bool UIComponent::HasMouseFocus() const
{
	return (canvas->GetMouseFocus() == this);
}

void UIComponent::SetAsKeyFocus()
{
	canvas->SetKeyFocus(this);
}

void UIComponent::CancelKeyFocus()
{
	if (HasKeyFocus())
	{
		canvas->SetKeyFocus(nullptr);
	}
}

bool UIComponent::HasKeyFocus() const
{
	return (canvas->GetKeyFocus() == this);
}

void UIComponent::UpdateSelfRect(const edge4f& parentRect)
{
	rect.left = math::lerp(parentRect.xmin(), parentRect.xmax(), edgeRelativePositions.left) + edgePaddings.left;
	rect.top = math::lerp(parentRect.ymin(), parentRect.ymax(), edgeRelativePositions.top) + edgePaddings.top;
	rect.right = math::lerp(parentRect.xmin(), parentRect.xmax(), edgeRelativePositions.right) - edgePaddings.right;
	rect.bottom = math::lerp(parentRect.ymin(), parentRect.ymax(), edgeRelativePositions.bottom) - edgePaddings.bottom;
}

void UIComponent::UpdateRequiredSize(UIComponentCacheData& cacheData)
{
	float2 childrenRequiredSize = float2(0, 0);

	if (minSizeFollowsChildren)
	{
		for (int iChild = 0; iChild < cacheData.ChildCount(); iChild++)
		{
			UIComponentCacheData& childCacheData = cacheData.GetChild(*canvas, iChild);

			UIComponent* uiComponent = childCacheData.GetComponent();

			uiComponent->UpdateRequiredSize(childCacheData);

			childrenRequiredSize = max(uiComponent->GetRequiredSize(), childrenRequiredSize);
		}

		childrenRequiredSize += float2(edgePaddings.left + edgePaddings.right, edgePaddings.top + edgePaddings.bottom);
	}

	this->requiredSize = max(GetLayoutMinSize(), childrenRequiredSize);
}

void UIComponent::UpdateRect(UIComponentCacheData& cacheData, const edge4f& parentRect)
{
	UpdateSelfRect(parentRect);

	for (int iChild = 0; iChild < cacheData.ChildCount(); iChild++)
	{
		UIComponentCacheData& childCacheData = cacheData.GetChild(*canvas, iChild);

		UIComponent* uiComponent = childCacheData.GetComponent();
			
		uiComponent->UpdateRect(childCacheData, rect);
	}
}

void UIComponent::OnParentHierarchyChanged(const HierarchyChangedEvent& ev)
{
	UpdateParentHierarchyDependentData();
}

void UIComponent::UpdateParentHierarchyDependentData()
{
	SceneObject* so = this->GetSceneObject().GetParent();
	while (so)
	{
		UIComponent* uicomp = so->GetComponent<UIComponent>();
		if (uicomp)
		{
			depth = uicomp->depth + 1;
			canvas = uicomp->canvas;
			return;
		}

		UICanvas* uicanvas = so->GetComponent<UICanvas>();
		if (uicanvas)
		{
			depth = 0;
			canvas = uicanvas;
			return;
		}

		so = so->GetParent();
	}

	depth = -1;
	canvas = nullptr;
}

void UIComponent::MarkLayoutInstructionsDirty()
{
	if (canvas)
	{
		canvas->MarkLayoutDirty();
	}
}
	
void UIComponent::PreLayout()
{
}

void UIComponent::GenerateLayoutInstructions(UILayoutInstructionBuilder& builder)
{
	UIComponent* parent = GetSceneObject().GetComponentInParents<UIComponent>(false);

	for (int iDstDim = 0; iDstDim < (int)UILayoutDimension::_Count; iDstDim++)
	{
		UILayoutDimension dstDim = (UILayoutDimension)iDstDim;

		UILayoutItemDesc& desc = layoutItemDescs[iDstDim];

		int32 refComponentId;
		if (desc.referenceComponent)
			refComponentId = desc.referenceComponent->GetLayoutComponentId();
		else if (parent)
			refComponentId = parent->GetLayoutComponentId();
		else
			refComponentId = UILayoutInstructionBuilder::CanvasRootComponentId;
		
		UILayoutItemInstruction& instruction = builder.AddInstruction(this->layoutComponentId, dstDim);
		instruction.op = desc.op;
		instruction.layoutItem1 = (desc.dimension1 != UILayoutDimension::_Count) ? builder.LayoutItem(refComponentId, desc.dimension1) : UILayoutInstructionBuilder::InvalidLayoutItem;
		instruction.layoutItem2 = (desc.dimension2 != UILayoutDimension::_Count) ? builder.LayoutItem(refComponentId, desc.dimension2) : UILayoutInstructionBuilder::InvalidLayoutItem;
		instruction.layoutItem3 = (desc.dimension3 != UILayoutDimension::_Count) ? builder.LayoutItem(this->layoutComponentId, desc.dimension3) : UILayoutInstructionBuilder::InvalidLayoutItem;
		instruction.value1 = desc.value1;
		instruction.value2 = desc.value2;
	}
}

void UIComponent::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("edgeRelativePositions", &Self::edgeRelativePositions);
	reflector.AddMember("edgePaddings", &Self::edgePaddings);
	reflector.AddMember("layoutMinSize", &Self::layoutMinSize);
	reflector.AddMember("layoutFillWeight", &Self::layoutFillWeight);
}

UIComponent* UIComponent::AddUIChild(Type* type, const std::string& name /*= "UISceneObject"*/)
{
	auto& so = GetSceneObject();
	
	auto* childSO = so.AddChild(name);
	Component* comp = childSO->AddComponent(type);

	return Util::CastPtrWithCheck<UIComponent>(comp);
}
