#pragma once

#include "UICommon.h"
#include "UIComponent.h"
#include "UICanvas.h"

class UIImage;

class UIObjectInspector : public UIComponent
{
	DECL_CLASS_TYPE(UIObjectInspector, UIComponent);

public:
	BaseObject* targetObject;

private:
	std::unique_ptr<Reflector> reflector;
	SceneObject* inspectorContainerSO;
	
	std::stack<BaseObject*> navigationStack;

public:
	UIObjectInspector();
	virtual ~UIObjectInspector();

	void SetTargetObject(BaseObject* targetObject);

	virtual void OnShutdown() override;

	virtual bool CanDraw() override { return false; }
	virtual bool HandleMouseEvents() { return false; }

private:
	void NavigationPushObject(BaseObject* obj);
	void NavigationPopObject();
	void NavigationClearAll();

	void SetTargetObjectInternal(BaseObject* targetObject);

	void OnTargetObjectDestroyed();
	void AttachToTargetObject();
	void DetachFromTargetObject();

	void CreateInspectorUI(SceneObject* so, void* obj, const BaseProperty* prop);
	void CreateInspectorUI(SceneObject* so, void* obj, const BasePrimitiveProperty* prop);
	void CreateInspectorUI(SceneObject* so, void* obj, const BaseObjectProperty* prop);
	void CreateInspectorUI(SceneObject* so, void* obj, const BaseArrayProperty* arrayProp);
	void CreateInspectorUI(SceneObject* so, void* obj, const BaseMapProperty* mapProp);
	void CreateInspectorUI(SceneObject* so, void* obj, const BaseRawPointerProperty* ptrProp);
	void CreateInspectorUI(SceneObject* so, void* obj, const BaseSharedPointerProperty* ptrProp);

};
