#pragma once

#include "UICommon.h"
#include "UIComponent.h"
#include "UICanvas.h"

enum class LinearLayoutDirection
{
	Horizontal,
	Vertical,
};

DECL_ENUM_TYPE(LinearLayoutDirection);

class UILinearLayout : public UIComponent
{
	DECL_CLASS_TYPE(UILinearLayout, UIComponent);

public:
	LinearLayoutDirection direction;
	bool fillHorizontal;
	bool fillVertical;
	float itemSpacing;
	float itemMinSize;

public:
	UILinearLayout();
	virtual ~UILinearLayout();

	virtual void Reflect(Reflector& reflector) override;
	
	virtual void OnStartup() override;
	virtual void OnShutdown() override;

	void SetDirection(LinearLayoutDirection dir)
	{
		this->direction = dir;
		MarkLayoutDirty();
	}
	
	void SetFillHorizontal(bool v)
	{
		this->fillHorizontal = v;
		MarkLayoutDirty();
	}
	
	void SetFillVertical(bool v)
	{
		this->fillVertical = v;
		MarkLayoutDirty();
	}

	void SetItemSpacing(float spacing)
	{
		this->itemSpacing = spacing;
		MarkLayoutDirty();
	}

	void SetItemMinSize(float minSize)
	{
		this->itemMinSize = minSize;
		MarkLayoutDirty();
	}

	void UpdateChildrenLayoutItemDescs();

	virtual bool CanDraw() override { return false; }

	virtual bool HandleMouseEvents() { return false; }

	virtual void UpdateRequiredSize(UIComponentCacheData& cacheData) override;
	virtual void UpdateRect(UIComponentCacheData& cacheData, const edge4f& parentRect) override;
	
	virtual void PreLayout() override;

};
