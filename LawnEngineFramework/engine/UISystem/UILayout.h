#pragma once

#include "UICommon.h"
#include <unordered_map>

enum class UILayoutDimension : uint8
{
	Left = 0,
	Right,
	Top,
	Bottom,

	_Count,
};

//
// Nop:         None
// Assign:      dst.left = v1
// Add:         dst.top = ref.top + v1
// Lerp:        dst.bottom = lerp(ref.top, ref.bottom, v1) + v2
// SubtractMultiplyAdd: dst.right = (ref.right - ref.left) * v1 + dst.left + v2
//
enum class UILayoutInstructionOp : uint8
{
	Nop = 0,
	Assign,
	Add,
	Lerp,
	SubtractMultiplyAdd,
};

struct UILayoutItemInstruction
{
	UILayoutInstructionOp op;

	uint32 layoutItem1;
	uint32 layoutItem2;
	uint32 layoutItem3;

	float value1;
	float value2;
};

struct UILayoutInstructionBuilder
{
	UICanvas* canvas;

	std::vector<UILayoutItemInstruction> layoutItemToInstructions;

	enum
	{
		InvalidLayoutItem = 0xFFFFFFFF,
		CanvasRootComponentId = 0,
	};

	static uint32 LayoutItem(int32 componentId, UILayoutDimension dim)
	{
		uint32 key = (((uint32)componentId) << 2) | (uint32)dim;
		return key;
	}
	static int32 LayoutItemGetComponentId(uint32 layoutItem)
	{
		return (int32)(layoutItem >> 2);
	}
	static UILayoutDimension LayoutItemGetLayoutDimension(uint32 layoutItem)
	{
		return (UILayoutDimension)(layoutItem & 0x3);
	}

	UILayoutItemInstruction& AddInstruction(int dstComponent, UILayoutDimension dstDim)
	{
		uint32 key = LayoutItem(dstComponent, dstDim);

		return layoutItemToInstructions[key];
	}

	void Assign(int dstComponent, UILayoutDimension dstDim, float value)
	{
		uint32 key = LayoutItem(dstComponent, dstDim);

		UILayoutItemInstruction i;
		i.op = UILayoutInstructionOp::Assign;
		i.layoutItem1 = InvalidLayoutItem;
		i.layoutItem2 = InvalidLayoutItem;
		i.layoutItem3 = InvalidLayoutItem;
		i.value1 = value;
		i.value2 = 0;
		layoutItemToInstructions[key] = i;
	}
	void Add(int dstComponent, UILayoutDimension dstDim, int32 op1Component, UILayoutDimension op1Dim, float bias)
	{
		uint32 key = LayoutItem(dstComponent, dstDim);

		UILayoutItemInstruction i;
		i.op = UILayoutInstructionOp::Add;
		i.layoutItem1 = LayoutItem(op1Component, op1Dim);
		i.layoutItem2 = InvalidLayoutItem;
		i.layoutItem3 = InvalidLayoutItem;
		i.value1 = bias;
		i.value2 = 0;
		layoutItemToInstructions[key] = i;
	}
	void Lerp(int dstComponent, UILayoutDimension dstDim, int32 op1Component, UILayoutDimension op1Dim, int32 op2Component, UILayoutDimension op2Dim, float lerpWeight, float bias)
	{
		uint32 key = LayoutItem(dstComponent, dstDim);

		UILayoutItemInstruction i;
		i.op = UILayoutInstructionOp::Lerp;
		i.layoutItem1 = LayoutItem(op1Component, op1Dim);
		i.layoutItem2 = LayoutItem(op2Component, op2Dim);
		i.layoutItem3 = InvalidLayoutItem;
		i.value1 = lerpWeight;
		i.value2 = bias;
		layoutItemToInstructions[key] = i;
	}
	void SubtractMultiplyAdd(int dstComponent, UILayoutDimension dstDim, int32 subOp1Component, UILayoutDimension subOp1Dim, int32 subOp2Component, UILayoutDimension subOp2Dim, float multiplyValue, int32 addComponent, UILayoutDimension addDim, float addValue)
	{
		uint32 key = LayoutItem(dstComponent, dstDim);

		UILayoutItemInstruction i;
		i.op = UILayoutInstructionOp::Lerp;
		i.layoutItem1 = LayoutItem(subOp1Component, subOp1Dim);
		i.layoutItem2 = LayoutItem(subOp2Component, subOp2Dim);
		i.layoutItem3 = LayoutItem(addComponent, addDim);
		i.value1 = multiplyValue;
		i.value2 = addValue;
		layoutItemToInstructions[key] = i;
	}
};
