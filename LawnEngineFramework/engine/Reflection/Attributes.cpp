#include "fast_compile.h"
#include "Attributes.h"

IMPL_ABSTRACT_CLASS_TYPE(BaseAttribute);

IMPL_CLASS_TYPE(SkipSerializationAttribute);

IMPL_CLASS_TYPE(HideInInspectorAttribute);

IMPL_CLASS_TYPE(FloatRangeAttribute);

IMPL_CLASS_TYPE(AssetSelectorAttribute);
