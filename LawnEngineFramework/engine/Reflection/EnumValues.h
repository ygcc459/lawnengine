#pragma once

#include <string>
#include "../FixedString.h"

class EnumValues
{
public:
	typedef int ValueType;

	std::map<FixedString, ValueType> nameToValueMap;
	std::map<ValueType, FixedString> valueToNameMap;

	struct Initializer
	{
		std::vector<std::pair<FixedString, ValueType>> allValuePairs;

		Initializer& AddMember(const char* s, ValueType v)
		{
			FixedString fixedString = s;
			allValuePairs.push_back(std::make_pair(s, v));
			return *this;
		}
	};

	EnumValues() {}
	EnumValues(const Initializer& initializer)
	{
		for (size_t i = 0; i < initializer.allValuePairs.size(); i++)
		{
			auto pair = initializer.allValuePairs[i];
			Add(pair.first, pair.second);
		}
	}

	template<class T>
	void Add(FixedString name, T value)
	{
		Add(name, (ValueType)value);
	}

	void Add(FixedString name, ValueType value)
	{
		nameToValueMap.insert(std::make_pair(name, value));
		valueToNameMap.insert(std::make_pair(value, name));
	}

	void GetAllNames(std::vector<FixedString>& outNames) const
	{
		for (auto it = nameToValueMap.begin(); it != nameToValueMap.end(); ++it)
		{
			outNames.push_back(it->first);
		}
	}

	template<typename T>
	void GetAllValuesTyped(std::vector<T>& outValues) const
	{
		for (auto it = nameToValueMap.begin(); it != nameToValueMap.end(); ++it)
		{
			ValueType value = it->second;
			outValues.push_back((T)value);
		}
	}

	FixedString GetName(ValueType v) const
	{
		auto it = valueToNameMap.find(v);
		if (it != valueToNameMap.end())
		{
			return it->second;
		}
		else
		{
			return FixedString();
		}
	}

	ValueType GetValue(FixedString name) const
	{
		auto it = nameToValueMap.find(name);
		if (it != nameToValueMap.end())
		{
			return it->second;
		}
		else
		{
			return ValueType();
		}
	}

	template<typename T>
	T GetValueTyped(FixedString name) const
	{
		auto it = nameToValueMap.find(name);
		if (it != nameToValueMap.end())
		{
			return (T)it->second;
		}
		else
		{
			return T();
		}
	}

	bool ToString(ValueType v, FixedString& s) const
	{
		auto it = valueToNameMap.find(v);
		if (it != valueToNameMap.end())
		{
			s = it->second;
			return true;
		}
		else
		{
			return false;
		}
	}
	bool ToString(ValueType v, std::string& s) const
	{
		FixedString fixedString;
		bool result = ToString(v, fixedString);
		s = fixedString.str();
		return result;
	}

	bool FromString(FixedString s, ValueType& v) const
	{
		auto it = nameToValueMap.find(s);
		if (it != nameToValueMap.end())
		{
			v = it->second;
			return true;
		}
		else
		{
			return false;
		}
	}
	bool FromString(const std::string& s, ValueType& v) const
	{
		FixedString fixedString = s;
		return FromString(fixedString, v);
	}

};
