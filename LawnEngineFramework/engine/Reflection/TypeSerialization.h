#pragma once

#include "Type.h"
#include "Serializer.h"

template<typename DeclType>
inline bool PrimitiveObjectType<DeclType>::Serialize(Serializer& serializer, void* p) const
{
	std::string sv;
	ToString(p, sv);

	serializer.CurrentNode()->SetValueString(sv);

	return true;
}

template<typename DeclType>
inline bool PrimitiveObjectType<DeclType>::Deserialize(Deserializer& deserializer, void* p) const
{
	std::string sv = deserializer.CurrentNode()->GetValue<std::string>();

	return FromString(sv, p);
}

template<typename DeclType>
inline bool EnumObjectType<DeclType>::Serialize(Serializer& serializer, void* p) const
{
	std::string sv;
	ToString(p, sv);

	serializer.CurrentNode()->SetValueString(sv);

	return true;
}

template<typename DeclType>
inline bool EnumObjectType<DeclType>::Deserialize(Deserializer& deserializer, void* p) const
{
	std::string sv = deserializer.CurrentNode()->GetValue<std::string>();

	return FromString(sv, p);
}
