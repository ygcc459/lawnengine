#pragma once

#include <string>
#include <vector>
#include <map>

class Type;
class ReflectiveObject;
class Serializer;
class Deserializer;

struct BaseProperty;

enum class TypeCategory
{
	Primitive,
	Enum,
	Object,
};

class TypeRegistry
{
public:
	std::vector<Type*> allTypes;
	std::map<std::string, Type*> nameToTypeMap;

	int GetNextTypeID() const
	{
		return (int)allTypes.size();
	}

	void RegisterType(const std::string& name, Type* type)
	{
		allTypes.push_back(type);
		nameToTypeMap.insert(std::make_pair(name, type));
	}

	Type* FindTypeByName(const std::string& name)
	{
		auto it = nameToTypeMap.find(name);
		if (it != nameToTypeMap.end())
		{
			return it->second;
		}
		else
		{
			return nullptr;
		}
	}
};

//
// Type
//
class Type
{
protected:
	TypeCategory category;
	int typeId;
	const std::string name;
	int byteSize;

	Type* parentType;

public:
	Type(const char* name, Type* parentType, int byteSize, TypeCategory category)
		:name(name), parentType(parentType), typeId(-1), byteSize(byteSize), category(category)
	{
		TypeRegistry& typeRegistry = GetTypeRegistry();
		typeId = typeRegistry.GetNextTypeID();
		typeRegistry.RegisterType(this->name, this);
	}

	TypeCategory Category() const { return category; }

	int TypeID() const { return typeId; }

	const std::string& TypeName() const
	{
		return name;
	}

	virtual void* CreateInstance() const
	{
		return nullptr;
	}

	virtual void ToString(const void* p, std::string& s) const
	{
	}

	virtual bool FromString(const std::string& s, void* p) const
	{
		return false;
	}

	virtual bool Serialize(Serializer& serializer, void* p) const
	{
		return false;
	}

	virtual bool Deserialize(Deserializer& deserializer, void* p) const
	{
		return false;
	}

	Type* ParentType()
	{
		return parentType;
	}

	bool IsRootType() const
	{
		return parentType == nullptr;
	}

	bool IsSameOrChildOf(const Type* other) const
	{
		return (this == other) || IsChildOf(other);
	}

	bool IsChildOf(const Type* other) const
	{
		if (other == nullptr)
		{
			return false;
		}

		Type* p = parentType;
		while (p != nullptr)
		{
			if(p == other)
				return true;
			else
				p = p->parentType;
		}

		return false;
	}

	static Type* FindByName(const std::string& name)
	{
		return GetTypeRegistry().FindTypeByName(name);
	}

protected:
	static TypeRegistry* typeRegistry;

	static TypeRegistry& GetTypeRegistry()
	{
		if (typeRegistry == nullptr)
		{
			typeRegistry = new TypeRegistry();
		}

		return *typeRegistry;
	}
};

//
// ClassObjectType for abstract objects
//
class BaseClassObjectType : public Type
{
public:
	BaseClassObjectType(const char* name, Type* parentType, int byteSize)
		: Type(name, parentType, byteSize, TypeCategory::Object)
	{
	}

	virtual void ToString(const void* p, std::string& s) const override
	{
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		return false;
	}
};

Type* GetDynamicType(const ReflectiveObject& v);

template<typename T>
Type* GetStaticType()
{
	return &T::StaticType;
}

#define DECL_ABSTRACT_CLASS_TYPE(type_name, parent_type_name)  \
public: \
	virtual Type* GetType() const { return &type_name::StaticType; } \
	static BaseClassObjectType StaticType; \
	typedef type_name Self; \
	typedef parent_type_name Super

#define IMPL_ABSTRACT_CLASS_ROOTTYPE(type_name) BaseClassObjectType type_name::StaticType(#type_name, nullptr, sizeof(type_name))

#define IMPL_ABSTRACT_CLASS_TYPE(type_name) BaseClassObjectType type_name::StaticType(#type_name, &type_name::Super::StaticType, sizeof(type_name))

//
// ClassObjectType for normal objects
//
template<typename DeclType>
class ClassObjectType : public BaseClassObjectType
{
public:
	ClassObjectType(const char* name, Type* parentType)
		: BaseClassObjectType(name, parentType, sizeof(DeclType))
	{
	}

	virtual bool Serialize(Serializer& serializer, void* p) const override
	{
		DeclType* ptr = reinterpret_cast<DeclType*>(p);
		return ptr->Serialize(serializer);
	}

	virtual bool Deserialize(Deserializer& deserializer, void* p) const override
	{
		DeclType* ptr = reinterpret_cast<DeclType*>(p);
		return ptr->Deserialize(deserializer);
	}

	virtual void* CreateInstance() const override
	{
		return new DeclType();
	}

	DeclType* CreateInstanceTyped() const
	{
		return new DeclType();
	}
};

#define DECL_CLASS_TYPE(type_name, parent_type_name)  \
public: \
	virtual Type* GetType() const { return &type_name::StaticType; } \
	static ClassObjectType<type_name> StaticType; \
	typedef type_name Self; \
	typedef parent_type_name Super

#define DECL_ROOT_CLASS_TYPE(type_name)  \
public: \
	virtual Type* GetType() const { return &type_name::StaticType; } \
	static ClassObjectType<type_name> StaticType; \
	typedef type_name Self; \
	typedef void Super

#define IMPL_CLASS_TYPE(type_name) ClassObjectType<type_name> type_name::StaticType(#type_name, &type_name::Super::StaticType)

#define IMPL_ROOT_CLASS_TYPE(type_name) ClassObjectType<type_name> type_name::StaticType(#type_name, nullptr)

//
// PrimitiveObjectType
//
template<typename DeclType>
class PrimitiveObjectType : public Type
{
public:
	typedef DeclType ValueType;

	PrimitiveObjectType(const char* name)
		: Type(name, nullptr, sizeof(ValueType), TypeCategory::Primitive)
	{
	}

	const ValueType& GetValue(const void* p) const
	{
		return *reinterpret_cast<const ValueType*>(p);
	}

	ValueType& GetValue(void* p) const
	{
		return *reinterpret_cast<ValueType*>(p);
	}

	void SetValue(void* p, const ValueType& newValue) const
	{
		*reinterpret_cast<ValueType*>(p) = newValue;
	}

	virtual void* CreateInstance() const override
	{
		return new ValueType();
	}

	virtual void ToString(const void* p, std::string& s) const override
	{
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		return false;
	}

	virtual bool Serialize(Serializer& serializer, void* p) const override;

	virtual bool Deserialize(Deserializer& deserializer, void* p) const override;
};

#define DECL_PRIMITIVE_TYPE(type_name)  \
	Type* GetDynamicType(const type_name& v); \
	template<> Type* GetStaticType<type_name>();

#define IMPL_PRIMITIVE_TYPE(type_name, type_class)  \
	type_class _##type_class##_##StaticType; \
	Type* GetDynamicType(const type_name& v) { return &_##type_class##_##StaticType; } \
	template<> Type* GetStaticType<type_name>() { return &_##type_class##_##StaticType; }

//
// EnumObjectType
//
class BaseEnumObjectType : public Type
{
	typedef int ValueType;

	std::vector<FixedString> enumNames;
	std::vector<int> enumValues;

public:
	BaseEnumObjectType(const char* name, int byteSize)
		: Type(name, nullptr, byteSize, TypeCategory::Enum)
	{
	}

	const std::vector<FixedString>& GetNames() const
	{
		return enumNames;
	}

	const std::vector<int>& GetIntValues() const
	{
		return enumValues;
	}

	void Add(FixedString name, int intValue)
	{
		enumNames.push_back(name);
		enumValues.push_back(intValue);
	}

	bool GetIntValueFromName(FixedString name, int& outValue) const
	{
		for (size_t i = 0; i < enumNames.size(); i++)
		{
			if (enumNames[i] == name)
			{
				outValue = enumValues[i];
				return true;
			}
		}
		return false;
	}

	FixedString GetNameFromIntValue(int v) const
	{
		for (size_t i = 0; i < enumValues.size(); i++)
		{
			if (enumValues[i] == v)
			{
				return enumNames[i];
			}
		}
		return {};
	}

	virtual int GetIntValue(const void* p) const = 0;

	virtual void SetIntValue(void* p, int newValue) const = 0;

};

template<typename DeclType>
class EnumObjectType : public BaseEnumObjectType
{
public:
	typedef DeclType ValueType;

public:
	EnumObjectType(const char* name)
		: BaseEnumObjectType(name, sizeof(ValueType))
	{
	}

	void Add(FixedString name, ValueType value)
	{
		BaseEnumObjectType::Add(name, (int)value);
	}

	bool GetValueFromName(FixedString name, ValueType& outValue) const
	{
		int outIntValue;
		if (BaseEnumObjectType::GetIntValueFromName(name, outIntValue))
		{
			outValue = (ValueType)outIntValue;
			return true;
		}
		else
			return false;
	}

	FixedString GetNameFromValue(ValueType v) const
	{
		return BaseEnumObjectType::GetNameFromIntValue((int)v);
	}

	virtual int GetIntValue(const void* p) const override
	{
		return (int)GetValue(p);
	}

	virtual void SetIntValue(void* p, int newValue) const override
	{
		SetValue(p, (ValueType)newValue);
	}

	const ValueType& GetValue(const void* p) const
	{
		return *reinterpret_cast<const ValueType*>(p);
	}

	ValueType& GetValue(void* p) const
	{
		return *reinterpret_cast<ValueType*>(p);
	}

	void SetValue(void* p, const ValueType& newValue) const
	{
		*reinterpret_cast<ValueType*>(p) = newValue;
	}

	virtual void* CreateInstance() const override
	{
		return new ValueType();
	}

	virtual void ToString(const void* p, std::string& s) const override
	{
		ValueType v = GetValue(p);
		FixedString name = GetNameFromValue(v);
		s = name.str();
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		ValueType v;
		if (GetValueFromName(s, v))
		{
			SetValue(p, v);
			return true;
		}
		else
			return false;
	}

	virtual bool Serialize(Serializer& serializer, void* p) const override;

	virtual bool Deserialize(Deserializer& deserializer, void* p) const override;
};

#define DECL_ENUM_TYPE(type_name)  \
	Type* GetDynamicType(const type_name& v); \
	template<> Type* GetStaticType<type_name>();

#define IMPL_ENUM_TYPE(type_name, type_class)  \
	type_class _##type_class##_##StaticType; \
	Type* GetDynamicType(const type_name& v) { return &_##type_class##_##StaticType; } \
	template<> Type* GetStaticType<type_name>() { return &_##type_class##_##StaticType; }

//
// ArrayObjectType
//
// class ArrayObjectType : public Type
// {
// public:
// 	Type* elementType;
// 
// 	ArrayObjectType(const char* name, Type* elementType, size_t byteSize)
// 		: Type(name, nullptr, byteSize, TypeCategory::Primitive)
// 	{
// 	}
// 
// 	virtual void* GetElementAddress(void* obj, size_t index) const = 0;
// 
// 	virtual size_t GetSize(void* obj) const = 0;
// };
// 
// template<typename T>
// struct StdVectorType : public BaseArrayProperty
// {
// 	StdVectorType(BaseProperty* elementProperty)
// 		: BaseArrayProperty(elementProperty)
// 	{
// 	}
// 
// 	virtual void* GetElementAddress(void* obj, size_t index) const override
// 	{
// 		std::vector<T>* vec = reinterpret_cast<std::vector<T>*>(obj);
// 		return vec->data() + index;
// 	}
// 
// 	virtual size_t GetSize(void* obj) const override
// 	{
// 		std::vector<T>* vec = reinterpret_cast<std::vector<T>*>(obj);
// 		return vec->size();
// 	}
// 
// 	virtual void* CreateInstance() const override
// 	{
// 		return new std::vector<T>();
// 	}
// 
// 	virtual bool Serialize(Serializer& serializer, void* p) const override
// 	{
// 		
// 	}
// 
// 	virtual bool Deserialize(Deserializer& deserializer, void* p) const override
// 	{
// 		return false;
// 	}
// };
