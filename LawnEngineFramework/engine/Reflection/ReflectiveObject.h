#pragma once

#include <string>
#include "Type.h"
#include "Property.h"

class Serializer;
class Deserializer;
class Reflector;
struct ObjectMember;

class ReflectiveObject
{
	DECL_ROOT_CLASS_TYPE(ReflectiveObject);

public:
	ReflectiveObject()
	{}

	virtual ~ReflectiveObject()
	{}

	virtual bool Serialize(Serializer& serializer);
	virtual bool Deserialize(Deserializer& deserializer);
	virtual void Reflect(Reflector& reflector);

	virtual void AfterDeserialization();

	virtual void OnPropertyChangedByReflection(ObjectMember& member);
};
