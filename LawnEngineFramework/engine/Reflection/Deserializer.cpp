#include "fast_compile.h"
#include "Deserializer.h"

Deserializer::Deserializer(ArchiveNode* node)
	: rootNode(node), rootObjectPtr(nullptr)
{
	nodeStack.push(rootNode);

	for (int i = 0; i < rootNode->ChildCount(); ++i)
	{
		ArchiveNode* node = rootNode->GetChild(i);

		int id = node->GetChildValue<int>("_Id", -1);
		if (id != -1)
		{
			serializedObjectNodes.insert(std::make_pair(id, node));
		}
	}
}

Deserializer::~Deserializer()
{
}

void Deserializer::DeserializeRootObject(ReflectiveObject* obj, const BaseProperty* prop)
{
	rootObjectPtr = obj;

	int rootObjectId = 0;

	auto itObjectNode = serializedObjectNodes.find(rootObjectId);
	if (itObjectNode != serializedObjectNodes.end())
	{
		ArchiveNode* objectNode = itObjectNode->second;
		nodeStack.push(objectNode);

		deserializedObjects.insert(std::make_pair(rootObjectId, DeserializedObject(obj, prop->type)));

		Deserialize(obj, prop);

		nodeStack.pop();
	}
}

void Deserializer::DeserializeRootObject(std::shared_ptr<ReflectiveObject> obj, const BaseProperty* prop)
{
	CHECK(false);
}

void Deserializer::InvokeAfterDeserialization()
{
	for (auto it = deserializedReflectiveObjectSet.begin(); it != deserializedReflectiveObjectSet.end(); ++it)
	{
		ReflectiveObject* ptr = (*it);
		ptr->AfterDeserialization();
	}
}

std::string Deserializer::GetRootObjectTypeName()
{
	int rootObjectId = 0;

	auto itObjectNode = serializedObjectNodes.find(rootObjectId);
	if (itObjectNode != serializedObjectNodes.end())
	{
		ArchiveNode* objectNode = itObjectNode->second;
		nodeStack.push(objectNode);

		std::string typeName;
		if (DeserializeMember("_Type", &typeName, PrimitiveProperty<std::string>()))
		{
			return typeName;
		}

		nodeStack.pop();
	}

	return "";
}

bool Deserializer::Deserialize(void* obj, const BaseProperty* prop)
{
	if (prop->type == PropertyType::Primitive)
	{
		const BasePrimitiveProperty* primProp = reinterpret_cast<const BasePrimitiveProperty*>(prop);
		return Deserialize(obj, primProp);
	}
	else if (prop->type == PropertyType::Object)
	{
		const BaseObjectProperty* objProp = reinterpret_cast<const BaseObjectProperty*>(prop);
		return Deserialize(obj, objProp);
	}
	else if (prop->type == PropertyType::Array)
	{
		const BaseArrayProperty* arrayProp = reinterpret_cast<const BaseArrayProperty*>(prop);
		return Deserialize(obj, arrayProp);
	}
	else if (prop->type == PropertyType::Map)
	{
		const BaseMapProperty* mapProp = reinterpret_cast<const BaseMapProperty*>(prop);
		return Deserialize(obj, mapProp);
	}
	else if (prop->type == PropertyType::RawPointer)
	{
		const BaseRawPointerProperty* ptrProp = reinterpret_cast<const BaseRawPointerProperty*>(prop);
		return Deserialize(obj, ptrProp);
	}
	else if (prop->type == PropertyType::SharedPointer)
	{
		const BaseSharedPointerProperty* ptrProp = reinterpret_cast<const BaseSharedPointerProperty*>(prop);
		return Deserialize(obj, ptrProp);
	}
	else if (prop->type == PropertyType::Object)
	{
		const BaseObjectProperty* objProp = reinterpret_cast<const BaseObjectProperty*>(prop);
		return Deserialize(obj, objProp);
	}
	else
	{
		CHECK(false);
		return false;
	}
}

bool Deserializer::Deserialize(void* obj, const BasePrimitiveProperty* prop)
{
	Type* type = prop->valueType;
	return type->Deserialize(*this, obj);
}

bool Deserializer::Deserialize(void* obj, const BaseObjectProperty* prop)
{
	Type* type = prop->valueType;
	CHECK(type->Category() == TypeCategory::Object);
	CHECK(type->IsSameOrChildOf(GetStaticType<ReflectiveObject>()));
	
	ReflectiveObject* reflectiveObject = reinterpret_cast<ReflectiveObject*>(obj);
	deserializedReflectiveObjectSet.insert(reflectiveObject);

	return type->Deserialize(*this, obj);
}

bool Deserializer::Deserialize(void* obj, const BaseArrayProperty* arrayProp)
{
	const BaseProperty* elementProp = arrayProp->GetElementProperty();

	arrayProp->Clear(obj);

	ArchiveNode* childnode = CurrentNode()->GetChild("Array");
	if (childnode)
	{
		arrayProp->Resize(obj, childnode->ChildCount());

		nodeStack.push(childnode);
		for (int i = 0; i < childnode->ChildCount(); ++i)
		{
			ArchiveNode* elementNode = childnode->GetChild(i);
			CHECK(elementNode->GetName() == "Element");

			nodeStack.push(elementNode);
			{
				void* elementAddr = arrayProp->GetElementAddress(obj, i);
				Deserialize(elementAddr, elementProp);
			}
			nodeStack.pop();
		}
		nodeStack.pop();

		return true;
	}
	else
	{
		return false;
	}
}

bool Deserializer::Deserialize(void* obj, const BaseMapProperty* mapProp)
{
	const BaseProperty* keyProp = mapProp->GetKeyProperty();
	const BaseProperty* valueProp = mapProp->GetValueProperty();

	mapProp->Clear(obj);

	ArchiveNode* childnode = CurrentNode()->GetChild("Map");
	if (childnode)
	{
		nodeStack.push(childnode);
		for (int i = 0; i < childnode->ChildCount(); ++i)
		{
			ArchiveNode* elementNode = childnode->GetChild(i);
			CHECK(elementNode->GetName() == "Element");

			nodeStack.push(elementNode);
			{
				void* keyPtr = keyProp->CreateInstance();
				void* valuePtr = valueProp->CreateInstance();

				if (DeserializeMember("Key", keyPtr, keyProp)
					&& DeserializeMember("Value", valuePtr, valueProp))
				{
					mapProp->Insert(obj, keyPtr, valuePtr);
				}

				delete keyPtr;
				delete valuePtr;
			}
			nodeStack.pop();
		}
		nodeStack.pop();

		return true;
	}
	else
	{
		return false;
	}
}

bool Deserializer::Deserialize(void* obj, const BaseRawPointerProperty* ptrProp)
{
	const BaseProperty* elementProp = ptrProp->GetDereferencedProperty();

	ptrProp->SetPtr(obj, NULL);

	ArchiveNode* pointerNode = CurrentNode()->GetChild("Pointer");
	if (pointerNode)
	{
		nodeStack.push(pointerNode);

		AssetPath assetPath;
		if (DeserializeMember("_AssetPath", &assetPath, PrimitiveProperty<AssetPath>()))
		{
			BaseObjectPtr assetPtr = g_lawnEngine.getAssetManager().LoadRaw(assetPath);

			//NOTE: converting shared-ptr to raw-ptr is allowed, although the life-cycle of raw-ptr is not permitted
			BaseObject* rawPtr = assetPtr.get();

			ptrProp->SetPtr(obj, rawPtr);
		}
		else if (pointerNode->HasValue())
		{
			int id = pointerNode->GetValue<int>();

			auto it = deserializedObjects.find(id);
			if (it != deserializedObjects.end())  //if object already deserialized, get its pointer directly
			{
				void* elementPtr = it->second.addr;
				ptrProp->SetPtr(obj, elementPtr);
			}
			else if (deserializedSharedObjects.find(id) != deserializedSharedObjects.end())
			{
				CHECK(false);  //the same object is used for both shared-ptr and raw-ptr, this is not allowed yet
			}
			else  //haven't deserialized, create object and deserialize
			{
				// find the ArchiveNode by id
				auto itObjectNode = serializedObjectNodes.find(id);
				if (itObjectNode != serializedObjectNodes.end())
				{
					ArchiveNode* objectNode = itObjectNode->second;
					nodeStack.push(objectNode);

					void* elementPtr = CreateObjectFromDeserialize(elementProp);

					//NOTE: here elementPtr can be nullptr
					ptrProp->SetPtr(obj, elementPtr);
					deserializedObjects.insert(std::make_pair(id, DeserializedObject(elementPtr, elementProp->type)));

					if (elementPtr)
					{
						Deserialize(elementPtr, elementProp);
					}

					nodeStack.pop();
				}
			}
		}

		nodeStack.pop();

		return true;
	}
	else
	{
		return false;
	}
}

bool Deserializer::Deserialize(void* obj, const BaseSharedPointerProperty* ptrProp)
{
	const BaseProperty* elementProp = ptrProp->GetDereferencedProperty();

	ptrProp->SetSharedPtr(obj, std::shared_ptr<void>());

	ArchiveNode* pointerNode = CurrentNode()->GetChild("Pointer");
	if (pointerNode)
	{
		nodeStack.push(pointerNode);

		AssetPath assetPath;
		if (DeserializeMember("_AssetPath", &assetPath, PrimitiveProperty<AssetPath>()))
		{
			BaseObjectPtr assetPtr = g_lawnEngine.getAssetManager().LoadRaw(assetPath);

			if (elementProp->type == PropertyType::Object)
			{
				std::shared_ptr<ReflectiveObject> reflectiveObjectPtr = std::static_pointer_cast<ReflectiveObject>(assetPtr);
				ptrProp->SetSharedPtr(obj, reflectiveObjectPtr);
			}
			else
			{
				CHECK(false);  //assetPtr should always be a BaseObject type
			}
		}
		else if (pointerNode->HasValue())
		{
			int id = pointerNode->GetValue<int>();

			auto it = deserializedSharedObjects.find(id);
			if (it != deserializedSharedObjects.end())  //if object already deserialized, get its pointer directly
			{
				std::shared_ptr<void> elementSharedPtr = it->second.ptr;
				ptrProp->SetSharedPtr(obj, elementSharedPtr);
			}
			else if (deserializedObjects.find(id) != deserializedObjects.end())
			{
				CHECK(false);  //the same object is used for both shared-ptr and raw-ptr, this is not allowed yet
			}
			else  //haven't deserialized, create object and deserialize
			{
				// find the ArchiveNode by id
				auto itObjectNode = serializedObjectNodes.find(id);
				if (itObjectNode != serializedObjectNodes.end())
				{
					ArchiveNode* objectNode = itObjectNode->second;
					nodeStack.push(objectNode);

					//NOTE: elementPtr could be nullptr
					void* elementPtr = CreateObjectFromDeserialize(elementProp);

					ptrProp->SetSharedPtrFromTypelesRawPtr(obj, elementPtr);

					std::shared_ptr<void> elementSharedPtr = ptrProp->GetSharedPtr(obj);

					deserializedSharedObjects.insert(std::make_pair(id, DeserializedSharedObject(elementSharedPtr, elementProp->type)));

					if (elementPtr)
					{
						Deserialize(elementPtr, elementProp);
					}

					nodeStack.pop();
				}
			}
		}

		nodeStack.pop();

		return true;
	}
	else
	{
		return false;
	}
}

void* Deserializer::CreateObjectFromDeserialize(const BaseProperty* prop)
{
	bool isReflectiveObject = (prop->type == PropertyType::Object);

	// 1. try to create the object from serialized object-type
	std::string typeName;
	if (DeserializeMember("_Type", &typeName, PrimitiveProperty<std::string>()))
	{
		Type* serializedType = Type::FindByName(typeName);
		if (serializedType == nullptr)
		{
			return nullptr;  //type not found
		}

		if (isReflectiveObject && createReflectiveObjectCallback)
		{
			return createReflectiveObjectCallback(prop, serializedType);
		}
		else
		{
			// check type is found and is assignable
			if (serializedType->IsSameOrChildOf(prop->valueType))
			{
				return serializedType->CreateInstance();
			}
			else
			{
				return nullptr;
			}
		}
	}

	// 2. try to create the object from declared-type
	if (isReflectiveObject && createReflectiveObjectCallback)
	{
		CHECK(prop->valueType);
		return createReflectiveObjectCallback(prop, prop->valueType);
	}
	else
	{
		return prop->CreateInstance();
	}
}
