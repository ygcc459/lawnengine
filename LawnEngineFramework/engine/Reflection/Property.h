#pragma once

#include "Type.h"
#include <memory>  //for std::unique_ptr

enum class PropertyType
{
	Primitive,
	Object,
	Array,
	Map,
	RawPointer,
	SharedPointer,
	RefCountedPointer,
};

struct BaseProperty
{
	PropertyType type;
	Type* valueType;

	BaseProperty(PropertyType type, Type* valueType)
		: type(type), valueType(valueType)
	{}

	virtual ~BaseProperty()
	{}

	virtual void* CreateInstance() const
	{
		return nullptr;
	}

	virtual void DestroyInstance(void* addr) const
	{
	}
};

//
// Primitive
//
struct BasePrimitiveProperty : public BaseProperty
{
	BasePrimitiveProperty(Type* valueType)
		: BaseProperty(PropertyType::Primitive, valueType)
	{}

	template<class T>
	T& GetValue(void* addr) const
	{
		return *reinterpret_cast<T*>(addr);
	}

	template<class T> const
	void SetValue(void* addr, const T& newValue)
	{
		*reinterpret_cast<T*>(addr) = newValue;
	}
	
	virtual std::string ToString(void* obj) const = 0;
	virtual bool FromString(void* addr, const std::string& str) const = 0;
};

template<typename T>
struct PrimitiveProperty : public BasePrimitiveProperty
{
	PrimitiveProperty()
		: BasePrimitiveProperty(GetStaticType<T>())
	{}

	virtual void* CreateInstance() const override
	{
		return new T();
	}

	virtual void DestroyInstance(void* addr) const override
	{
		T* p = reinterpret_cast<T*>(addr);
		delete p;
	}

	virtual std::string ToString(void* addr) const override
	{
		T& obj = GetValue<T>(addr);
		std::string s;
		GetStaticType<T>()->ToString(&obj, s);
		return s;
	}

	virtual bool FromString(void* addr, const std::string& str) const override
	{
		T& obj = GetValue<T>(addr);
		return GetStaticType<T>()->FromString(str, &obj);
	}
};

//
// Array
//
struct BaseArrayProperty : public BaseProperty
{
	std::unique_ptr<BaseProperty> elementProperty;

	BaseArrayProperty(BaseProperty* elementProperty)
		: BaseProperty(PropertyType::Array, nullptr), elementProperty(elementProperty)
	{}

	BaseProperty* GetElementProperty() const
	{
		return elementProperty.get();
	}

	virtual void* GetElementAddress(void* obj, size_t index) const = 0;

	virtual size_t GetSize(void* obj) const = 0;

	virtual void Resize(void* obj, size_t newsize) const = 0;

	virtual void Clear(void* obj) const = 0;
};

template<typename T>
struct StdVectorProperty : public BaseArrayProperty
{
	StdVectorProperty(BaseProperty* elementProperty)
		: BaseArrayProperty(elementProperty)
	{
	}

	virtual void* CreateInstance() const override
	{
		return new std::vector<T>();
	}

	virtual void DestroyInstance(void* addr) const override
	{
		std::vector<T>* p = reinterpret_cast<std::vector<T>*>(addr);
		delete p;
	}

	virtual void* GetElementAddress(void* obj, size_t index) const override
	{
		std::vector<T>* vec = reinterpret_cast<std::vector<T>*>(obj);
		return vec->data() + index;
	}

	virtual size_t GetSize(void* obj) const override
	{
		std::vector<T>* vec = reinterpret_cast<std::vector<T>*>(obj);
		return vec->size();
	}

	virtual void Resize(void* obj, size_t newsize) const override
	{
		std::vector<T>* vec = reinterpret_cast<std::vector<T>*>(obj);
		return vec->resize(newsize);
	}

	virtual void Clear(void* obj) const override
	{
		std::vector<T>* vec = reinterpret_cast<std::vector<T>*>(obj);
		return vec->clear();
	}
};

//
// Map
//
struct BaseMapProperty : public BaseProperty
{
	std::unique_ptr<BaseProperty> keyProperty;
	std::unique_ptr<BaseProperty> valueProperty;

	struct Iterator
	{
		virtual bool IsValid() = 0;
		virtual void Next() = 0;
		virtual void* GetKeyAddress() = 0;
		virtual void* GetValueAddress() = 0;
	};

	BaseMapProperty(BaseProperty* keyProperty, BaseProperty* valueProperty)
		: BaseProperty(PropertyType::Map, nullptr), keyProperty(keyProperty), valueProperty(valueProperty)
	{}

	BaseProperty* GetKeyProperty() const
	{
		return keyProperty.get();
	}

	BaseProperty* GetValueProperty() const
	{
		return valueProperty.get();
	}

	virtual Iterator* GetIterator(void* obj) const = 0;

	virtual size_t GetSize(void* obj) const = 0;

	virtual void Clear(void* obj) const = 0;

	virtual bool Insert(void* obj, const void* keyAddr, const void* valueAddr) const = 0;

	virtual bool Find(void* obj, const void* keyAddr, void** outValueAddr) const = 0;
};

template<typename TKey, typename TValue>
struct StdMapProperty : public BaseMapProperty
{
	typedef std::map<TKey, TValue> MapType;

	struct StdMapIterator : public BaseMapProperty::Iterator
	{
		MapType& map;
		typename MapType::iterator it;

		StdMapIterator(MapType& map)
			: map(map), it(map.begin())
		{}

		virtual bool IsValid() override
		{
			return it != map.end();
		}

		virtual void Next() override
		{
			++it;
		}

		virtual void* GetKeyAddress() override
		{
			const TKey& k = it->first;
			return const_cast<TKey*>(&k);
		}

		virtual void* GetValueAddress() override
		{
			return &it->second;
		}
	};

	StdMapProperty(BaseProperty* keyProperty, BaseProperty* valueProperty)
		: BaseMapProperty(keyProperty, valueProperty)
	{
	}

	virtual void* CreateInstance() const override
	{
		return new MapType();
	}

	virtual void DestroyInstance(void* addr) const override
	{
		MapType* p = reinterpret_cast<MapType*>(addr);
		delete p;
	}

	virtual BaseMapProperty::Iterator* GetIterator(void* obj) const override
	{
		MapType* m = reinterpret_cast<MapType*>(obj);
		return new StdMapIterator(*m);
	}

	virtual size_t GetSize(void* obj) const override
	{
		MapType* m = reinterpret_cast<MapType*>(obj);
		return m->size();
	}

	virtual void Clear(void* obj) const override
	{
		MapType* m = reinterpret_cast<MapType*>(obj);
		m->clear();
	}

	virtual bool Insert(void* obj, const void* keyAddr, const void* valueAddr) const override
	{
		MapType* m = reinterpret_cast<MapType*>(obj);
		const TKey& k = *reinterpret_cast<const TKey*>(keyAddr);
		const TValue& v = *reinterpret_cast<const TValue*>(valueAddr);

		bool inserted = m->insert(std::make_pair(k, v)).second;
		
		return inserted;
	}

	virtual bool Find(void* obj, const void* keyAddr, void** outValueAddr) const override
	{
		MapType* m = reinterpret_cast<MapType*>(obj);

		const TKey& k = *reinterpret_cast<const TKey*>(keyAddr);

		typename MapType::iterator it = m->find(k);
		if (it != m->end())
		{
			(*outValueAddr) = &it->second;
			return true;
		}
		else
		{
			(*outValueAddr) = NULL;
			return false;
		}
	}
};

//
// Object
//
struct BaseObjectProperty : public BaseProperty
{
	BaseObjectProperty(Type* type)
		: BaseProperty(PropertyType::Object, type)
	{}

	ReflectiveObject& GetReference(void* addr) const
	{
		ReflectiveObject* p = reinterpret_cast<ReflectiveObject*>(addr);
		return *p;
	}

	ReflectiveObject* GetPointer(void* addr) const
	{
		ReflectiveObject* p = reinterpret_cast<ReflectiveObject*>(addr);
		return p;
	}
};

template<class T>
struct AbstractObjectProperty : public BaseObjectProperty
{
	AbstractObjectProperty()
		: BaseObjectProperty(GetStaticType<T>())
	{}
};

template<class T>
struct ObjectProperty : public AbstractObjectProperty<T>
{
	ObjectProperty()
	{}

	virtual void* CreateInstance() const override
	{
		return new T();
	}

	virtual void DestroyInstance(void* addr) const override
	{
		T* p = reinterpret_cast<T*>(addr);
		delete p;
	}
};

//
// RawPointer
//
struct BaseRawPointerProperty : public BaseProperty
{
	std::unique_ptr<BaseProperty> elementProperty;

	BaseRawPointerProperty(BaseProperty* elementProperty)
		: BaseProperty(PropertyType::RawPointer, nullptr), elementProperty(elementProperty)
	{}
	
	BaseProperty* GetDereferencedProperty() const
	{
		return elementProperty.get();
	}

	virtual void* GetPtr(void* obj) const = 0;
	virtual void SetPtr(void* obj, void* v) const = 0;
};

template<class T>
struct RawPointerProperty : public BaseRawPointerProperty
{
	typedef T* PointerType;

	RawPointerProperty(BaseProperty* elementProperty)
		: BaseRawPointerProperty(elementProperty)
	{}

	virtual void* CreateInstance() const override
	{
		return new PointerType();
	}

	virtual void DestroyInstance(void* addr) const override
	{
		PointerType* p = reinterpret_cast<PointerType*>(addr);
		delete p;
	}

	virtual void* GetPtr(void* obj) const override
	{
		PointerType& p = *reinterpret_cast<PointerType*>(obj);
		return p;
	}

	virtual void SetPtr(void* obj, void* v) const override
	{
		PointerType& p = *reinterpret_cast<PointerType*>(obj);
		T* newvalue = reinterpret_cast<T*>(v);
		p = newvalue;
	}
};

//
// SharedPointer
//
struct BaseSharedPointerProperty : public BaseProperty
{
	std::unique_ptr<BaseProperty> elementProperty;

	BaseSharedPointerProperty(BaseProperty* elementProperty)
		: BaseProperty(PropertyType::SharedPointer, nullptr), elementProperty(elementProperty)
	{}

	BaseProperty* GetDereferencedProperty() const
	{
		return elementProperty.get();
	}

	virtual std::shared_ptr<void> GetSharedPtr(void* obj) const = 0;

	virtual void SetSharedPtr(void* obj, const std::shared_ptr<void>& ptr) const = 0;

	virtual void SetSharedPtrFromTypelesRawPtr(void* obj, void* ptr) const = 0;
};

template<class T>
struct StdSharedPointerProperty : public BaseSharedPointerProperty
{
	typedef std::shared_ptr<T> PointerType;

	StdSharedPointerProperty(BaseProperty* elementProperty)
		: BaseSharedPointerProperty(elementProperty)
	{}

	virtual void* CreateInstance() const override
	{
		return new PointerType();
	}

	virtual void DestroyInstance(void* addr) const override
	{
		PointerType* p = reinterpret_cast<PointerType*>(addr);
		delete p;
	}

	virtual std::shared_ptr<void> GetSharedPtr(void* obj) const override
	{
		PointerType& p = *reinterpret_cast<PointerType*>(obj);
		return std::reinterpret_pointer_cast<void>(p);
	}

	virtual void SetSharedPtr(void* obj, const std::shared_ptr<void>& ptr) const override
	{
		PointerType& p = *reinterpret_cast<PointerType*>(obj);
		p = std::reinterpret_pointer_cast<T>(ptr);
	}

	virtual void SetSharedPtrFromTypelesRawPtr(void* obj, void* ptr) const override
	{
		PointerType& p = *reinterpret_cast<PointerType*>(obj);
		p.reset(reinterpret_cast<T*>(ptr));
	}
};

//
// RefCountedPointer
//
struct BaseRefCountedPointerProperty : public BaseProperty
{
	std::unique_ptr<BaseProperty> elementProperty;

	BaseRefCountedPointerProperty(BaseProperty* elementProperty)
		: BaseProperty(PropertyType::RefCountedPointer, nullptr), elementProperty(elementProperty)
	{}

	BaseProperty* GetDereferencedProperty() const
	{
		return elementProperty.get();
	}

	virtual RefCountedPtr<RefCountedObject> GetSharedPtr(void* obj) const = 0;

	virtual void SetSharedPtr(void* obj, const RefCountedPtr<RefCountedObject>& ptr) const = 0;
};

template<class T>
struct RefCountedPointerProperty : public BaseRefCountedPointerProperty
{
	typedef RefCountedPtr<T> PointerType;

	RefCountedPointerProperty(BaseProperty* elementProperty)
		: BaseRefCountedPointerProperty(elementProperty)
	{}

	virtual void* CreateInstance() const override
	{
		return new PointerType();
	}

	virtual void DestroyInstance(void* addr) const override
	{
		PointerType* p = reinterpret_cast<PointerType*>(addr);
		delete p;
	}

	virtual RefCountedPtr<RefCountedObject> GetSharedPtr(void* obj) const override
	{
		PointerType& p = *reinterpret_cast<PointerType*>(obj);
		return RefCountedPtr<RefCountedObject>(p.GetRefCountedObject());
	}

	virtual void SetSharedPtr(void* obj, const RefCountedPtr<RefCountedObject>& ptr) const override
	{
		PointerType& p = *reinterpret_cast<PointerType*>(obj);
		p = ptr.ReinterpretCast<T>();
	}
};

////////////////////////////////////////////////////////////////////////////////////

class PropertyCreator
{
private:
	//
	// inner
	// 
	template<bool isReflectiveObject, bool isAbstract, typename T>
	struct CreatePropertyInnerImpl  //primitive type
	{
		static BaseProperty* CreateProperty()
		{
			return new PrimitiveProperty<T>();
		}
	};

	template<typename T>
	struct CreatePropertyInnerImpl<true, false, T>  //object type, non-abstract
	{
		static BaseProperty* CreateProperty()
		{
			return new ObjectProperty<T>();
		}
	};

	template<typename T>
	struct CreatePropertyInnerImpl<true, true, T>  //object type, abstract
	{
		static BaseProperty* CreateProperty()
		{
			return new AbstractObjectProperty<T>();
		}
	};

	template<typename T>
	struct CreatePropertyInner
	{
		static BaseProperty* CreateProperty()
		{
			return CreatePropertyInnerImpl<
				is_base<ReflectiveObject, T>::value, 
				std::is_abstract<T>::value,
				T>::CreateProperty();
		}
	};

	//
	// outer
	// 
	template<typename T>
	struct CreatePropertyOuter  //non-container non-pointer type
	{
		static BaseProperty* CreateProperty()
		{
			BaseProperty* objProp = CreatePropertyInner<std::remove_cv<T>::type>::CreateProperty();

			return objProp;
		}
	};

	template<typename T>
	struct CreatePropertyOuter<T&>  //reference type
	{
		static BaseProperty* CreateProperty()
		{
			CHECK(false);
			return nullptr;
		}
	};

	template<typename T>
	struct CreatePropertyOuter<T*>  //pointer type
	{
		static BaseProperty* CreateProperty()
		{
			BaseProperty* objProp = CreatePropertyOuter<T>::CreateProperty();

			BaseProperty* prop = new RawPointerProperty<T>(objProp);

			return prop;
		}
	};

	template<typename T>
	struct CreatePropertyOuter<std::shared_ptr<T>>  //shared-pointer type
	{
		static BaseProperty* CreateProperty()
		{
			BaseProperty* objProp = CreatePropertyOuter<T>::CreateProperty();

			BaseProperty* prop = new StdSharedPointerProperty<T>(objProp);

			return prop;
		}
	};

	template<typename T>
	struct CreatePropertyOuter<RefCountedPtr<T>>  //RefCountedPtr type
	{
		static BaseProperty* CreateProperty()
		{
			BaseProperty* objProp = CreatePropertyOuter<T>::CreateProperty();

			BaseProperty* prop = new StdSharedPointerProperty<T>(objProp);

			return prop;
		}
	};

	template<typename T>
	struct CreatePropertyOuter<std::vector<T>>  //vector type
	{
		static BaseProperty* CreateProperty()
		{
			BaseProperty* elementProp = CreatePropertyOuter<T>::CreateProperty();

			BaseProperty* prop = new StdVectorProperty<T>(elementProp);

			return prop;
		}
	};

	template<typename TKey, typename TValue>
	struct CreatePropertyOuter<std::map<TKey, TValue>>  //map type
	{
		static BaseProperty* CreateProperty()
		{
			BaseProperty* keyProp = CreatePropertyOuter<TKey>::CreateProperty();

			BaseProperty* valueProp = CreatePropertyOuter<TValue>::CreateProperty();

			BaseProperty* prop = new StdMapProperty<TKey, TValue>(keyProp, valueProp);

			return prop;
		}
	};

public:
	//
	// property creators
	// 
	template<typename T>
	static BaseProperty* CreateProperty()
	{
		BaseProperty* prop = CreatePropertyOuter<T>::CreateProperty();
		return prop;
	}
};
