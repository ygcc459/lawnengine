#pragma once

#include "ToString.h"
#include "Type.h"
#include "ReflectiveObject.h"
#include "../AssetSystem/Archive.h"
#include <stack>

struct AssetPath;

class Serializer
{
	std::stack<ArchiveNode*> nodeStack;
	std::map<void*, int> serializedObjectIds;
	ArchiveNode* rootNode;
	void* rootObjectPtr;

public:
	std::function<bool(ReflectiveObject*)> canSerializeReflectiveObjectCallback;

public:
	Serializer(ArchiveNode* node);
	~Serializer();

	void SerializeRootObject(void* obj, const BaseProperty* prop);

	void SerializeRootObject(void* obj, const BaseProperty& prop)
	{
		SerializeRootObject(obj, &prop);
	}

	template<class T>
	void SerializeMemberAuto(const char* name, const T& member)
	{
		std::unique_ptr<BaseProperty> prop(PropertyCreator::CreateProperty<T>());
		const void* addr = &member;
		SerializeMember(name, const_cast<void*>(addr), prop.get());
	}

	void SerializeMember(const char* name, void* obj, const BaseProperty* prop)
	{
		ArchiveNode* childnode = CurrentNode()->AddChild(name);
		nodeStack.push(childnode);
		Serialize(obj, prop);
		nodeStack.pop();
	}

	void SerializeMember(const char* name, void* obj, const BaseProperty& prop)
	{
		SerializeMember(name, obj, &prop);
	}

	void SerializeMemberRawData(const char* name, void* data, size_t size)
	{
		ArchiveNode* childnode = CurrentNode()->AddChild(name);
		childnode->SetValueBinary((const char*)data, size);
	}

	template<class T>
	void SerializeMemberRawData(const char* name, const std::vector<T>& v)
	{
		ArchiveNode* childnode = CurrentNode()->AddChild(name);
		childnode->SetValueBinary((const char*)v.data(), v.size() * sizeof(T));
	}

	bool IsRootObject(void* ptr) const
	{
		return ptr == rootObjectPtr;
	}

	ArchiveNode* CurrentNode()
	{
		return nodeStack.top();
	}

	ArchiveNode* RootNode()
	{
		return rootNode;
	}

private:
	void Serialize(void* obj, const BaseProperty* prop);

	void Serialize(void* obj, const BasePrimitiveProperty* prop);

	void Serialize(void* obj, const BaseObjectProperty* prop);

	void Serialize(void* obj, const BaseArrayProperty* arrayProp);

	void Serialize(void* obj, const BaseMapProperty* mapProp);

	void Serialize(void* obj, const BaseRawPointerProperty* ptrProp);

	void Serialize(void* obj, const BaseSharedPointerProperty* ptrProp);

	bool CanSerializePointer(const BaseProperty* elementProp, void* elementAddr);

	bool ShouldPointerSerializeAsAssetPath(const BaseProperty* elementProp, void* elementAddr, AssetPath& outAssetPath);

	void SerializeToRootNode(int id, void* obj, const BaseProperty* prop);

	bool CanSerialize(ReflectiveObject* objPtr);

};
