#include "fast_compile.h"
#include "Type.h"

TypeRegistry* Type::typeRegistry = nullptr;

Type* GetDynamicType(const ReflectiveObject& v)
{
	return v.GetType();
}
