#include "fast_compile.h"
#include "CommonTypeMetas.h"

void _KeepCommonTypeMetaSymbolsHack()
{ 
}

IMPL_PRIMITIVE_TYPE(bool, BoolType);
IMPL_PRIMITIVE_TYPE(int8, Int8Type);
IMPL_PRIMITIVE_TYPE(uint8, UInt8Type);
IMPL_PRIMITIVE_TYPE(int32, Int32Type);
IMPL_PRIMITIVE_TYPE(int64, Int64Type);
IMPL_PRIMITIVE_TYPE(uint32, UInt32Type);
IMPL_PRIMITIVE_TYPE(uint64, UInt64Type);
IMPL_PRIMITIVE_TYPE(float, FloatType);
IMPL_PRIMITIVE_TYPE(double, DoubleType);
IMPL_PRIMITIVE_TYPE(std::string, StdStringType);
IMPL_PRIMITIVE_TYPE(FixedString, FixedStringType);
IMPL_PRIMITIVE_TYPE(float2, Float2Type);
IMPL_PRIMITIVE_TYPE(float3, Float3Type);
IMPL_PRIMITIVE_TYPE(float4, Float4Type);
IMPL_PRIMITIVE_TYPE(float4x4, Float4x4Type);
IMPL_PRIMITIVE_TYPE(quaternionf, QuaternionfType);
IMPL_PRIMITIVE_TYPE(rectf, RectfType);
IMPL_PRIMITIVE_TYPE(edge4f, Edge4fType);
IMPL_PRIMITIVE_TYPE(sh2, Sh2Type);
IMPL_PRIMITIVE_TYPE(sh3, Sh3Type);
IMPL_PRIMITIVE_TYPE(sh2_color3, Sh2Color3Type);
IMPL_PRIMITIVE_TYPE(sh3_color3, Sh3Color3Type);
