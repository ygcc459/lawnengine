#include "fast_compile.h"
#include "ReflectiveObject.h"

IMPL_ROOT_CLASS_TYPE(ReflectiveObject);

bool ReflectiveObject::Serialize(Serializer& serializer)
{
	return true;
}

bool ReflectiveObject::Deserialize(Deserializer& deserializer)
{
	return true;
}

void ReflectiveObject::Reflect(Reflector& reflector)
{
}

void ReflectiveObject::AfterDeserialization()
{
}

void ReflectiveObject::OnPropertyChangedByReflection(ObjectMember& member)
{
}
