#pragma once

#include "Type.h"
#include <memory>  //for std::unique_ptr

struct ObjectMember
{
	std::string memberName;
	size_t memberOffset;
	std::unique_ptr<BaseProperty> memberProperty;

	typedef std::function<void* (void* obj)> MemberAddressGetterFn;
	MemberAddressGetterFn memberAddressGetter;

	std::vector<std::unique_ptr<BaseAttribute>> attributes;

	ObjectMember(const std::string& memberName, BaseProperty* memberProperty, size_t memberOffset)
		: memberName(memberName), memberOffset(memberOffset), memberProperty(memberProperty)
	{}

	ObjectMember(const std::string& memberName, BaseProperty* memberProperty, const MemberAddressGetterFn& memberAddressGetter)
		: memberName(memberName), memberOffset(0), memberProperty(memberProperty), memberAddressGetter(memberAddressGetter)
	{}

	BaseProperty* GetProperty()
	{
		return memberProperty.get();
	}

	void* GetAddr(void* obj)
	{
		if (memberAddressGetter)
		{
			void* addr = memberAddressGetter(obj);
			return addr;
		}
		else
		{
			return reinterpret_cast<uint8*>(obj) + memberOffset;
		}
	}

	ObjectMember& AddAttribute(BaseAttribute* attr)
	{
		attributes.emplace_back(std::unique_ptr<BaseAttribute>(attr));
		return *this;
	}

	template<class TAttr>
	const TAttr* GetAttribute() const
	{
		for (size_t i = 0; i < attributes.size(); i++)
		{
			BaseAttribute* attr = attributes[i].get();
			if (attr->GetType()->IsSameOrChildOf(GetStaticType<TAttr>()))
				return reinterpret_cast<TAttr*>(attr);
		}
		return nullptr;
	}

	template<class TAttr>
	bool HasAttribute() const
	{
		for (size_t i = 0; i < attributes.size(); i++)
		{
			BaseAttribute* attr = attributes[i].get();
			if (attr->GetType()->IsSameOrChildOf(GetStaticType<TAttr>()))
				return true;
		}
		return false;
	}
};

struct FunctionCall;

struct ObjectFunction
{
	std::string name;

	std::vector<std::unique_ptr<BaseProperty>> parameters;
	std::unique_ptr<BaseProperty> returnValue;

	std::function<void(FunctionCall&)> functionInvoker;

	ObjectFunction()
	{}
};

struct FunctionCall
{
	ObjectFunction& func;

	void* targetObject;  //i.e. 'this' pointer
	std::vector<void*> parameters;
	void* returnValue;

	FunctionCall(ObjectFunction& func)
		: func(func), targetObject(nullptr), returnValue(nullptr)
	{
		if (func.returnValue)
		{
			returnValue = func.returnValue->CreateInstance();
		}

		parameters.resize(func.parameters.size());
		for (size_t i = 0; i < func.parameters.size(); i++)
		{
			parameters[i] = func.parameters[i]->CreateInstance();
		}
	}

	~FunctionCall()
	{
		if (func.returnValue)
		{
			func.returnValue->DestroyInstance(returnValue);
		}

		parameters.resize(func.parameters.size());
		for (size_t i = 0; i < func.parameters.size(); i++)
		{
			func.parameters[i]->DestroyInstance(parameters[i]);
		}
	}

	void Invoke()
	{
		func.functionInvoker(*this);
	}
};

class Reflector
{
public:
	std::vector<ObjectMember> members;
	std::vector<ObjectFunction> functions;

	Reflector()
	{}

public:
	void Clear()
	{
		members.clear();
		functions.clear();
	}

	ObjectMember* GetMember(const char* name)
	{
		for (ObjectMember& om : members)
		{
			if (om.memberName == name)
				return &om;
		}
		return nullptr;
	}

	ObjectFunction* GetFunction(const char* name)
	{
		for (ObjectFunction& of : functions)
		{
			if (of.name == name)
				return &of;
		}
		return nullptr;
	}

	//
	// add member
	//
	template<class TClass, class TMember>
	ObjectMember& AddMember(const char* name, TMember TClass::* p)
	{
		size_t offset = GetMemberOffset(p);
		BaseProperty* prop = PropertyCreator::CreateProperty<TMember>();
		members.emplace_back(name, prop, offset);
		return members.back();
	}

	//
	// add custom member
	//
	template<class TMember>
	ObjectMember& AddMemberCustom(const char* name, const ObjectMember::MemberAddressGetterFn& memberAddressGetter)
	{
		BaseProperty* prop = PropertyCreator::CreateProperty<TMember>();
		members.emplace_back(name, prop, memberAddressGetter);
		return members.back();
	}

	//
	// add function no-param
	//
	template<class TClass>
	ObjectFunction& AddFunction(const char* name, void(TClass::* fp)())
	{
		functions.emplace_back();
		ObjectFunction& objectFunction = functions.back();
		objectFunction.name = name;
		objectFunction.functionInvoker = [=](FunctionCall& call)
		{
			CHECK(call.targetObject);
			CHECK(call.parameters.size() == 0);
			CHECK(call.returnValue == nullptr);

			TClass* obj = reinterpret_cast<TClass*>(call.targetObject);

			(obj->*fp)();
		};
		return objectFunction;
	}

	template<class TClass, class TRet>
	ObjectFunction& AddFunction(const char* name, TRet(TClass::* fp)())
	{
		functions.emplace_back();
		ObjectFunction& objectFunction = functions.back();
		objectFunction.name = name;
		objectFunction.returnValue.reset(PropertyCreator::CreateProperty<TRet>());
		objectFunction.functionInvoker = [=](FunctionCall& call)
		{
			CHECK(call.targetObject);
			CHECK(call.parameters.size() == 0);
			CHECK(call.returnValue);

			TClass* obj = reinterpret_cast<TClass*>(call.targetObject);

			TRet& returnValue = *reinterpret_cast<TRet*>(call.returnValue);
			returnValue = (obj->*fp)();
		};
		return objectFunction;
	}

	//
	// add function one-param
	//
	template<class TClass, class TParam1>
	ObjectFunction& AddFunction(const char* name, void(TClass::* fp)(TParam1))
	{
		typedef std::remove_reference<TParam1>::type TParam1NoRef;

		// allowing T and const T&, but not allow T&
		static_assert(!std::is_reference_v<TParam1> || std::is_const_v<TParam1NoRef>, "T& is not allowed, use T* or T or const T& instead");

		functions.emplace_back();
		ObjectFunction& objectFunction = functions.back();
		objectFunction.name = name;
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam1NoRef>()));
		objectFunction.functionInvoker = [=](FunctionCall& call)
		{
			CHECK(call.targetObject);
			CHECK(call.parameters.size() == 1);
			CHECK(call.returnValue == nullptr);

			TClass* obj = reinterpret_cast<TClass*>(call.targetObject);
			TParam1NoRef& param1 = *reinterpret_cast<TParam1NoRef*>(call.parameters[0]);

			(obj->*fp)(param1);
		};
		return objectFunction;
	}

	template<class TClass, class TRet, class TParam1>
	ObjectFunction& AddFunction(const char* name, TRet(TClass::* fp)(TParam1))
	{
		typedef std::remove_reference<TParam1>::type TParam1NoRef;

		// allowing T and const T&, but not allow T&
		static_assert(!std::is_reference_v<TParam1> || std::is_const_v<TParam1NoRef>, "T& is not allowed, use T* or T or const T& instead");

		functions.emplace_back();
		ObjectFunction& objectFunction = functions.back();
		objectFunction.name = name;
		objectFunction.returnValue.reset(PropertyCreator::CreateProperty<TRet>());
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam1NoRef>()));
		objectFunction.functionInvoker = [=](FunctionCall& call)
		{
			CHECK(call.targetObject);
			CHECK(call.parameters.size() == 1);
			CHECK(call.returnValue);

			TClass* obj = reinterpret_cast<TClass*>(call.targetObject);
			TParam1NoRef& param1 = *reinterpret_cast<TParam1NoRef*>(call.parameters[0]);

			TRet& returnValue = *reinterpret_cast<TRet*>(call.returnValue);
			returnValue = (obj->*fp)(param1);
		};
		return objectFunction;
	}

	//
	// add function two-params
	//
	template<class TClass, class TParam1, class TParam2>
	ObjectFunction& AddFunction(const char* name, void(TClass::* fp)(TParam1, TParam2))
	{
		typedef std::remove_reference<TParam1>::type TParam1NoRef;
		typedef std::remove_reference<TParam2>::type TParam2NoRef;

		// allowing T and const T&, but not allow T&
		static_assert(!std::is_reference_v<TParam1> || std::is_const_v<TParam1NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam2> || std::is_const_v<TParam2NoRef>, "T& is not allowed, use T* or T or const T& instead");

		functions.emplace_back();
		ObjectFunction& objectFunction = functions.back();
		objectFunction.name = name;
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam1NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam2NoRef>()));
		objectFunction.functionInvoker = [=](FunctionCall& call)
		{
			CHECK(call.targetObject);
			CHECK(call.parameters.size() == 2);
			CHECK(call.returnValue == nullptr);

			TClass* obj = reinterpret_cast<TClass*>(call.targetObject);
			TParam1NoRef& param1 = *reinterpret_cast<TParam1NoRef*>(call.parameters[0]);
			TParam2NoRef& param2 = *reinterpret_cast<TParam2NoRef*>(call.parameters[1]);

			(obj->*fp)(param1, param2);
		};
		return objectFunction;
	}

	template<class TClass, class TRet, class TParam1, class TParam2>
	ObjectFunction& AddFunction(const char* name, TRet(TClass::* fp)(TParam1, TParam2))
	{
		typedef std::remove_reference<TParam1>::type TParam1NoRef;
		typedef std::remove_reference<TParam2>::type TParam2NoRef;

		// allowing T and const T&, but not allow T&
		static_assert(!std::is_reference_v<TParam1> || std::is_const_v<TParam1NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam2> || std::is_const_v<TParam2NoRef>, "T& is not allowed, use T* or T or const T& instead");

		functions.emplace_back();
		ObjectFunction& objectFunction = functions.back();
		objectFunction.name = name;
		objectFunction.returnValue.reset(PropertyCreator::CreateProperty<TRet>());
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam1NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam2NoRef>()));
		objectFunction.functionInvoker = [=](FunctionCall& call)
		{
			CHECK(call.targetObject);
			CHECK(call.parameters.size() == 2);
			CHECK(call.returnValue);

			TClass* obj = reinterpret_cast<TClass*>(call.targetObject);
			TParam1NoRef& param1 = *reinterpret_cast<TParam1NoRef*>(call.parameters[0]);
			TParam2NoRef& param2 = *reinterpret_cast<TParam2NoRef*>(call.parameters[1]);

			TRet& returnValue = *reinterpret_cast<TRet*>(call.returnValue);
			returnValue = (obj->*fp)(param1, param2);
		};
		return objectFunction;
	}

	//
	// add function three-params
	//
	template<class TClass, class TParam1, class TParam2, class TParam3>
	ObjectFunction& AddFunction(const char* name, void(TClass::* fp)(TParam1, TParam2, TParam3))
	{
		typedef std::remove_reference<TParam1>::type TParam1NoRef;
		typedef std::remove_reference<TParam2>::type TParam2NoRef;
		typedef std::remove_reference<TParam3>::type TParam3NoRef;

		// allowing T and const T&, but not allow T&
		static_assert(!std::is_reference_v<TParam1> || std::is_const_v<TParam1NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam2> || std::is_const_v<TParam2NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam3> || std::is_const_v<TParam3NoRef>, "T& is not allowed, use T* or T or const T& instead");

		functions.emplace_back();
		ObjectFunction& objectFunction = functions.back();
		objectFunction.name = name;
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam1NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam2NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam3NoRef>()));
		objectFunction.functionInvoker = [=](FunctionCall& call)
		{
			CHECK(call.targetObject);
			CHECK(call.parameters.size() == 3);
			CHECK(call.returnValue == nullptr);

			TClass* obj = reinterpret_cast<TClass*>(call.targetObject);
			TParam1NoRef& param1 = *reinterpret_cast<TParam1NoRef*>(call.parameters[0]);
			TParam2NoRef& param2 = *reinterpret_cast<TParam2NoRef*>(call.parameters[1]);
			TParam3NoRef& param3 = *reinterpret_cast<TParam3NoRef*>(call.parameters[2]);

			(obj->*fp)(param1, param2, param3);
		};
		return objectFunction;
	}

	template<class TClass, class TRet, class TParam1, class TParam2, class TParam3>
	ObjectFunction& AddFunction(const char* name, TRet(TClass::* fp)(TParam1, TParam2, TParam3))
	{
		typedef std::remove_reference<TParam1>::type TParam1NoRef;
		typedef std::remove_reference<TParam2>::type TParam2NoRef;
		typedef std::remove_reference<TParam3>::type TParam3NoRef;

		// allowing T and const T&, but not allow T&
		static_assert(!std::is_reference_v<TParam1> || std::is_const_v<TParam1NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam2> || std::is_const_v<TParam2NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam3> || std::is_const_v<TParam3NoRef>, "T& is not allowed, use T* or T or const T& instead");

		functions.emplace_back();
		ObjectFunction& objectFunction = functions.back();
		objectFunction.name = name;
		objectFunction.returnValue.reset(PropertyCreator::CreateProperty<TRet>());
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam1NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam2NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam3NoRef>()));
		objectFunction.functionInvoker = [=](FunctionCall& call)
		{
			CHECK(call.targetObject);
			CHECK(call.parameters.size() == 3);
			CHECK(call.returnValue);

			TClass* obj = reinterpret_cast<TClass*>(call.targetObject);
			TParam1NoRef& param1 = *reinterpret_cast<TParam1NoRef*>(call.parameters[0]);
			TParam2NoRef& param2 = *reinterpret_cast<TParam2NoRef*>(call.parameters[1]);
			TParam3NoRef& param3 = *reinterpret_cast<TParam3NoRef*>(call.parameters[2]);

			TRet& returnValue = *reinterpret_cast<TRet*>(call.returnValue);
			returnValue = (obj->*fp)(param1, param2, param3);
		};
		return objectFunction;
	}
	
	//
	// add function four-params
	//
	template<class TClass, class TParam1, class TParam2, class TParam3, class TParam4>
	ObjectFunction& AddFunction(const char* name, void(TClass::* fp)(TParam1, TParam2, TParam3, TParam4))
	{
		typedef std::remove_reference<TParam1>::type TParam1NoRef;
		typedef std::remove_reference<TParam2>::type TParam2NoRef;
		typedef std::remove_reference<TParam3>::type TParam3NoRef;
		typedef std::remove_reference<TParam4>::type TParam4NoRef;

		// allowing T and const T&, but not allow T&
		static_assert(!std::is_reference_v<TParam1> || std::is_const_v<TParam1NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam2> || std::is_const_v<TParam2NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam3> || std::is_const_v<TParam3NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam4> || std::is_const_v<TParam4NoRef>, "T& is not allowed, use T* or T or const T& instead");

		functions.emplace_back();
		ObjectFunction& objectFunction = functions.back();
		objectFunction.name = name;
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam1NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam2NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam3NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam4NoRef>()));
		objectFunction.functionInvoker = [=](FunctionCall& call)
		{
			CHECK(call.targetObject);
			CHECK(call.parameters.size() == 4);
			CHECK(call.returnValue == nullptr);

			TClass* obj = reinterpret_cast<TClass*>(call.targetObject);
			TParam1NoRef& param1 = *reinterpret_cast<TParam1NoRef*>(call.parameters[0]);
			TParam2NoRef& param2 = *reinterpret_cast<TParam2NoRef*>(call.parameters[1]);
			TParam3NoRef& param3 = *reinterpret_cast<TParam3NoRef*>(call.parameters[2]);
			TParam4NoRef& param4 = *reinterpret_cast<TParam4NoRef*>(call.parameters[3]);

			(obj->*fp)(param1, param2, param3, param4);
		};
		return objectFunction;
	}

	template<class TClass, class TRet, class TParam1, class TParam2, class TParam3, class TParam4>
	ObjectFunction& AddFunction(const char* name, TRet(TClass::* fp)(TParam1, TParam2, TParam3, TParam4))
	{
		typedef std::remove_reference<TParam1>::type TParam1NoRef;
		typedef std::remove_reference<TParam2>::type TParam2NoRef;
		typedef std::remove_reference<TParam3>::type TParam3NoRef;
		typedef std::remove_reference<TParam4>::type TParam4NoRef;

		// allowing T and const T&, but not allow T&
		static_assert(!std::is_reference_v<TParam1> || std::is_const_v<TParam1NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam2> || std::is_const_v<TParam2NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam3> || std::is_const_v<TParam3NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam4> || std::is_const_v<TParam4NoRef>, "T& is not allowed, use T* or T or const T& instead");

		functions.emplace_back();
		ObjectFunction& objectFunction = functions.back();
		objectFunction.name = name;
		objectFunction.returnValue.reset(PropertyCreator::CreateProperty<TRet>());
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam1NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam2NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam3NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam4NoRef>()));
		objectFunction.functionInvoker = [=](FunctionCall& call)
		{
			CHECK(call.targetObject);
			CHECK(call.parameters.size() == 4);
			CHECK(call.returnValue);

			TClass* obj = reinterpret_cast<TClass*>(call.targetObject);
			TParam1NoRef& param1 = *reinterpret_cast<TParam1NoRef*>(call.parameters[0]);
			TParam2NoRef& param2 = *reinterpret_cast<TParam2NoRef*>(call.parameters[1]);
			TParam3NoRef& param3 = *reinterpret_cast<TParam3NoRef*>(call.parameters[2]);
			TParam4NoRef& param4 = *reinterpret_cast<TParam4NoRef*>(call.parameters[3]);

			TRet& returnValue = *reinterpret_cast<TRet*>(call.returnValue);
			returnValue = (obj->*fp)(param1, param2, param3, param4);
		};
		return objectFunction;
	}
	
	//
	// add function five-params
	//
	template<class TClass, class TParam1, class TParam2, class TParam3, class TParam4, class TParam5>
	ObjectFunction& AddFunction(const char* name, void(TClass::* fp)(TParam1, TParam2, TParam3, TParam4, TParam5))
	{
		typedef std::remove_reference<TParam1>::type TParam1NoRef;
		typedef std::remove_reference<TParam2>::type TParam2NoRef;
		typedef std::remove_reference<TParam3>::type TParam3NoRef;
		typedef std::remove_reference<TParam4>::type TParam4NoRef;
		typedef std::remove_reference<TParam4>::type TParam5NoRef;

		// allowing T and const T&, but not allow T&
		static_assert(!std::is_reference_v<TParam1> || std::is_const_v<TParam1NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam2> || std::is_const_v<TParam2NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam3> || std::is_const_v<TParam3NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam4> || std::is_const_v<TParam4NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam5> || std::is_const_v<TParam5NoRef>, "T& is not allowed, use T* or T or const T& instead");

		functions.emplace_back();
		ObjectFunction& objectFunction = functions.back();
		objectFunction.name = name;
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam1NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam2NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam3NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam4NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam5NoRef>()));
		objectFunction.functionInvoker = [=](FunctionCall& call)
		{
			CHECK(call.targetObject);
			CHECK(call.parameters.size() == 5);
			CHECK(call.returnValue == nullptr);

			TClass* obj = reinterpret_cast<TClass*>(call.targetObject);
			TParam1NoRef& param1 = *reinterpret_cast<TParam1NoRef*>(call.parameters[0]);
			TParam2NoRef& param2 = *reinterpret_cast<TParam2NoRef*>(call.parameters[1]);
			TParam3NoRef& param3 = *reinterpret_cast<TParam3NoRef*>(call.parameters[2]);
			TParam4NoRef& param4 = *reinterpret_cast<TParam4NoRef*>(call.parameters[3]);
			TParam4NoRef& param5 = *reinterpret_cast<TParam4NoRef*>(call.parameters[4]);

			(obj->*fp)(param1, param2, param3, param4, param5);
		};
		return objectFunction;
	}

	template<class TClass, class TRet, class TParam1, class TParam2, class TParam3, class TParam4, class TParam5>
	ObjectFunction& AddFunction(const char* name, TRet(TClass::* fp)(TParam1, TParam2, TParam3, TParam4, TParam5))
	{
		typedef std::remove_reference<TParam1>::type TParam1NoRef;
		typedef std::remove_reference<TParam2>::type TParam2NoRef;
		typedef std::remove_reference<TParam3>::type TParam3NoRef;
		typedef std::remove_reference<TParam4>::type TParam4NoRef;
		typedef std::remove_reference<TParam4>::type TParam5NoRef;

		// allowing T and const T&, but not allow T&
		static_assert(!std::is_reference_v<TParam1> || std::is_const_v<TParam1NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam2> || std::is_const_v<TParam2NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam3> || std::is_const_v<TParam3NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam4> || std::is_const_v<TParam4NoRef>, "T& is not allowed, use T* or T or const T& instead");
		static_assert(!std::is_reference_v<TParam5> || std::is_const_v<TParam5NoRef>, "T& is not allowed, use T* or T or const T& instead");

		functions.emplace_back();
		ObjectFunction& objectFunction = functions.back();
		objectFunction.name = name;
		objectFunction.returnValue.reset(PropertyCreator::CreateProperty<TRet>());
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam1NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam2NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam3NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam4NoRef>()));
		objectFunction.parameters.push_back(std::unique_ptr<BaseProperty>(PropertyCreator::CreateProperty<TParam5NoRef>()));
		objectFunction.functionInvoker = [=](FunctionCall& call)
		{
			CHECK(call.targetObject);
			CHECK(call.parameters.size() == 5);
			CHECK(call.returnValue);

			TClass* obj = reinterpret_cast<TClass*>(call.targetObject);
			TParam1NoRef& param1 = *reinterpret_cast<TParam1NoRef*>(call.parameters[0]);
			TParam2NoRef& param2 = *reinterpret_cast<TParam2NoRef*>(call.parameters[1]);
			TParam3NoRef& param3 = *reinterpret_cast<TParam3NoRef*>(call.parameters[2]);
			TParam4NoRef& param4 = *reinterpret_cast<TParam4NoRef*>(call.parameters[3]);
			TParam5NoRef& param5 = *reinterpret_cast<TParam5NoRef*>(call.parameters[4]);

			TRet& returnValue = *reinterpret_cast<TRet*>(call.returnValue);
			returnValue = (obj->*fp)(param1, param2, param3, param4, param5);
		};
		return objectFunction;
	}
};
