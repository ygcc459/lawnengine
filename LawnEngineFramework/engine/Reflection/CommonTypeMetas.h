#pragma once

// There must be more than one symbol in CommonTypeMeta.a that are referenced elsewhere
// Otherwise, compiler will optimize out all symbols in CommonTypeMeta.a
// and thus these types will not be registered in TypeRegistry
void _KeepCommonTypeMetaSymbolsHack();

class BoolType : public PrimitiveObjectType<bool>
{
public:
	BoolType()
		: PrimitiveObjectType<bool>("bool")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		bool v = GetValue(p);

		s = v ? "true" : "false";
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		if (s == "true" || s == "True" || s == "1")
			SetValue(p, true);
		else
			SetValue(p, false);

		return true;
	}
};
DECL_PRIMITIVE_TYPE(bool)

class Int8Type : public PrimitiveObjectType<int8>
{
public:
	Int8Type()
		: PrimitiveObjectType<int8>("int8")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		int8 v = GetValue(p);

		char buffer[32];
		sprintf(buffer, "%d", (int32)v);
		s = buffer;
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		int32 v32;
		if (vcpp::parsers::parseInt32(s.c_str(), v32))
		{
			SetValue(p, v32);
			return true;
		}
		else
		{
			SetValue(p, 0);
			return false;
		}
	}
};
DECL_PRIMITIVE_TYPE(int8)

class UInt8Type : public PrimitiveObjectType<uint8>
{
public:
	UInt8Type()
		: PrimitiveObjectType<uint8>("uint8")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		uint8 v = GetValue(p);

		char buffer[32];
		sprintf(buffer, "%u", (uint32)v);
		s = buffer;
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		uint32 v32;
		if (vcpp::parsers::parseUInt32(s.c_str(), v32))
		{
			SetValue(p, (uint8)v32);
			return true;
		}
		else
		{
			SetValue(p, 0);
			return false;
		}
	}
};
DECL_PRIMITIVE_TYPE(uint8)

class Int32Type : public PrimitiveObjectType<int32>
{
public:
	Int32Type()
		: PrimitiveObjectType<int32>("int32")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		int32 v = GetValue(p);

		char buffer[32];
		sprintf(buffer, "%d", v);
		s = buffer;
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		int32 v32;
		if (vcpp::parsers::parseInt32(s.c_str(), v32))
		{
			SetValue(p, v32);
			return true;
		}
		else
		{
			SetValue(p, 0);
			return false;
		}
	}
};
DECL_PRIMITIVE_TYPE(int32)

class Int64Type : public PrimitiveObjectType<int64>
{
public:
	Int64Type()
		: PrimitiveObjectType<int64>("int64")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		int64 v = GetValue(p);

		char buffer[64];
		sprintf(buffer, "%lld", v);
		s = buffer;
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		int64 v64;
		if (vcpp::parsers::parseInt64(s.c_str(), v64))
		{
			SetValue(p, v64);
			return true;
		}
		else
		{
			SetValue(p, 0);
			return false;
		}
	}
};
DECL_PRIMITIVE_TYPE(int64)

class UInt32Type : public PrimitiveObjectType<uint32>
{
public:
	UInt32Type()
		: PrimitiveObjectType<uint32>("uint32")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		uint32 v = GetValue(p);

		char buffer[32];
		sprintf(buffer, "%u", v);
		s = buffer;
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		uint32 v32;
		if (vcpp::parsers::parseUInt32(s.c_str(), v32))
		{
			SetValue(p, v32);
			return true;
		}
		else
		{
			SetValue(p, 0);
			return false;
		}
	}
};
DECL_PRIMITIVE_TYPE(uint32)

class UInt64Type : public PrimitiveObjectType<uint64>
{
public:
	UInt64Type()
		: PrimitiveObjectType<uint64>("uint64")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		uint64 v = GetValue(p);

		char buffer[64];
		sprintf(buffer, "%llu", v);
		s = buffer;
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		uint64 v64;
		if (vcpp::parsers::parseUInt64(s.c_str(), v64))
		{
			SetValue(p, v64);
			return true;
		}
		else
		{
			SetValue(p, 0);
			return false;
		}
	}
};
DECL_PRIMITIVE_TYPE(uint64)

class FloatType : public PrimitiveObjectType<float>
{
public:
	FloatType()
		: PrimitiveObjectType<float>("float")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		float v = GetValue(p);

		char buffer[32];
		sprintf(buffer, "%f", v);
		s = buffer;
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		float v;
		if (vcpp::parsers::parseFloat(s.c_str(), v))
		{
			SetValue(p, v);
			return true;
		}
		else
		{
			SetValue(p, 0);
			return false;
		}
	}
};
DECL_PRIMITIVE_TYPE(float)

class DoubleType : public PrimitiveObjectType<double>
{
public:
	DoubleType()
		: PrimitiveObjectType<double>("double")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		double v = GetValue(p);

		char buffer[64];
		sprintf(buffer, "%lf", v);
		s = buffer;
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		double v;
		if (vcpp::parsers::parseDouble(s.c_str(), v))
		{
			SetValue(p, v);
			return true;
		}
		else
		{
			SetValue(p, 0);
			return false;
		}
	}
};
DECL_PRIMITIVE_TYPE(double)

class StdStringType : public PrimitiveObjectType<std::string>
{
public:
	StdStringType()
		: PrimitiveObjectType<std::string>("std::string")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		s = GetValue(p);
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		SetValue(p, s);
		return true;
	}
};
DECL_PRIMITIVE_TYPE(std::string)

class FixedStringType : public PrimitiveObjectType<FixedString>
{
public:
	FixedStringType()
		: PrimitiveObjectType<FixedString>("FixedString")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		FixedString v = GetValue(p);
		s = v.str();
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		FixedString v = s;
		SetValue(p, v);
		return true;
	}
};
DECL_PRIMITIVE_TYPE(FixedString)

class Float2Type : public PrimitiveObjectType<float2>
{
public:
	Float2Type()
		: PrimitiveObjectType<float2>("float2")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		const float2& v = GetValue(p);

		char buffer[64];
		sprintf(buffer, "%f %f", v.x, v.y);
		s = buffer;
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		float2 v;

		const char* readPtr = s.c_str();
		bool success = true;
		success &= vcpp::parsers::parseFloat(&readPtr, v.x);
		success &= vcpp::parsers::parseFloat(&readPtr, v.y);

		SetValue(p, v);

		return success;
	}
};
DECL_PRIMITIVE_TYPE(float2)

class Float3Type : public PrimitiveObjectType<float3>
{
public:
	Float3Type()
		: PrimitiveObjectType<float3>("float3")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		const float3& v = GetValue(p);

		char buffer[96];
		sprintf(buffer, "%f %f %f", v.x, v.y, v.z);
		s = buffer;
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		float3 v;

		const char* readPtr = s.c_str();
		bool success = true;
		success &= vcpp::parsers::parseFloat(&readPtr, v.x);
		success &= vcpp::parsers::parseFloat(&readPtr, v.y);
		success &= vcpp::parsers::parseFloat(&readPtr, v.z);

		SetValue(p, v);

		return success;
	}
};
DECL_PRIMITIVE_TYPE(float3)

class Float4Type : public PrimitiveObjectType<float4>
{
public:
	Float4Type()
		: PrimitiveObjectType<float4>("float4")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		const float4& v = GetValue(p);

		char buffer[128];
		sprintf(buffer, "%f %f %f %f", v.x, v.y, v.z, v.w);
		s = buffer;
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		float4 v;

		const char* readPtr = s.c_str();
		bool success = true;
		success &= vcpp::parsers::parseFloat(&readPtr, v.x);
		success &= vcpp::parsers::parseFloat(&readPtr, v.y);
		success &= vcpp::parsers::parseFloat(&readPtr, v.z);
		success &= vcpp::parsers::parseFloat(&readPtr, v.w);

		SetValue(p, v);

		return success;
	}
};
DECL_PRIMITIVE_TYPE(float4)

class Float4x4Type : public PrimitiveObjectType<float4x4>
{
public:
	Float4x4Type()
		: PrimitiveObjectType<float4x4>("float4x4")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		const float4x4& v = GetValue(p);

		char buffer[32 * 16];
		sprintf(buffer,
			"%f %f %f %f "
			"%f %f %f %f "
			"%f %f %f %f "
			"%f %f %f %f",
			v.f[0], v.f[1], v.f[2], v.f[3],
			v.f[4], v.f[5], v.f[6], v.f[7],
			v.f[8], v.f[9], v.f[10], v.f[11],
			v.f[12], v.f[13], v.f[14], v.f[15]);

		s = buffer;
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		float4x4 v;

		const char* readPtr = s.c_str();
		bool success = true;
		for (int i = 0; i < 16; ++i)
		{
			success &= vcpp::parsers::parseFloat(&readPtr, v.f[i]);
		}

		SetValue(p, v);

		return success;
	}
};
DECL_PRIMITIVE_TYPE(float4x4)

class QuaternionfType : public PrimitiveObjectType<quaternionf>
{
public:
	QuaternionfType()
		: PrimitiveObjectType<quaternionf>("quaternionf")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		const quaternionf& v = GetValue(p);

		char buffer[128];
		sprintf(buffer, "%f %f %f %f", v.x, v.y, v.z, v.w);
		s = buffer;
	}

	// can read euler angles in degrees, e.g. "euler 90.0 12.3 0.0"
	// and also quaternion raw values, e.g. "-0.803 0 0 0.52"
	virtual bool FromString(const std::string& s, void* p) const override
	{
		quaternionf v;

		const char* readPtr = s.c_str();

		const char* ptrPastEuler;
		if (vcpp::parsers::startsWith(readPtr, readPtr + s.size(), "euler", &ptrPastEuler))
		{
			readPtr = ptrPastEuler;

			float3 eulerAngles;

			bool success = true;
			success &= vcpp::parsers::parseFloat(&readPtr, eulerAngles.x);
			success &= vcpp::parsers::parseFloat(&readPtr, eulerAngles.y);
			success &= vcpp::parsers::parseFloat(&readPtr, eulerAngles.z);

			eulerAngles.x = math::degreeToRadian(eulerAngles.x);
			eulerAngles.y = math::degreeToRadian(eulerAngles.y);
			eulerAngles.z = math::degreeToRadian(eulerAngles.z);

			v = quaternion_from_euler(eulerAngles);

			SetValue(p, v);

			return success;
		}
		else
		{
			bool success = true;
			success &= vcpp::parsers::parseFloat(&readPtr, v.x);
			success &= vcpp::parsers::parseFloat(&readPtr, v.y);
			success &= vcpp::parsers::parseFloat(&readPtr, v.z);
			success &= vcpp::parsers::parseFloat(&readPtr, v.w);

			SetValue(p, v);

			return success;
		}
	}
};
DECL_PRIMITIVE_TYPE(quaternionf)

class RectfType : public PrimitiveObjectType<rectf>
{
public:
	RectfType()
		: PrimitiveObjectType<rectf>("rectf")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		const rectf& v = GetValue(p);

		char buffer[128];
		sprintf(buffer, "%f %f %f %f", v.x, v.y, v.w, v.h);
		s = buffer;
	}

	// can read euler angles in degrees, e.g. "euler 90.0 12.3 0.0"
	// and also quaternion raw values, e.g. "-0.803 0 0 0.52"
	virtual bool FromString(const std::string& s, void* p) const override
	{
		rectf v;

		const char* readPtr = s.c_str();
		bool success = true;
		success &= vcpp::parsers::parseFloat(&readPtr, v.x);
		success &= vcpp::parsers::parseFloat(&readPtr, v.y);
		success &= vcpp::parsers::parseFloat(&readPtr, v.w);
		success &= vcpp::parsers::parseFloat(&readPtr, v.h);

		SetValue(p, v);

		return success;
	}
};
DECL_PRIMITIVE_TYPE(rectf)

class Edge4fType : public PrimitiveObjectType<edge4f>
{
public:
	Edge4fType()
		: PrimitiveObjectType<edge4f>("edge4f")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		const edge4f& v = GetValue(p);

		char buffer[128];
		sprintf(buffer, "%f %f %f %f", v.left, v.top, v.right, v.bottom);
		s = buffer;
	}

	// can read euler angles in degrees, e.g. "euler 90.0 12.3 0.0"
	// and also quaternion raw values, e.g. "-0.803 0 0 0.52"
	virtual bool FromString(const std::string& s, void* p) const override
	{
		edge4f v;

		const char* readPtr = s.c_str();
		bool success = true;
		success &= vcpp::parsers::parseFloat(&readPtr, v.left);
		success &= vcpp::parsers::parseFloat(&readPtr, v.top);
		success &= vcpp::parsers::parseFloat(&readPtr, v.right);
		success &= vcpp::parsers::parseFloat(&readPtr, v.bottom);

		SetValue(p, v);

		return success;
	}
};
DECL_PRIMITIVE_TYPE(edge4f)

class Sh2Type : public PrimitiveObjectType<sh2>
{
public:
	Sh2Type()
		: PrimitiveObjectType<sh2>("sh2")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		const sh2& v = GetValue(p);

		s = "";
		for (size_t i = 0; i < v.size(); i++)
		{
			s += std::to_string(v[i]);
			if (i != v.size() - 1)
				s += ' ';
		}
	}

	// can read euler angles in degrees, e.g. "euler 90.0 12.3 0.0"
	// and also quaternion raw values, e.g. "-0.803 0 0 0.52"
	virtual bool FromString(const std::string& s, void* p) const override
	{
		sh2 v;

		const char* readPtr = s.c_str();
		bool success = true;
		for (size_t i = 0; success && i < v.size(); i++)
		{
			success &= vcpp::parsers::parseFloat(&readPtr, v[i]);
		}

		SetValue(p, v);

		return success;
	}
};
DECL_PRIMITIVE_TYPE(sh2)

class Sh3Type : public PrimitiveObjectType<sh3>
{
public:
	Sh3Type()
		: PrimitiveObjectType<sh3>("sh3")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		const sh3& v = GetValue(p);

		s = "";
		for (size_t i = 0; i < v.size(); i++)
		{
			s += std::to_string(v[i]);
			if (i != v.size() - 1)
				s += ' ';
		}
	}

	// can read euler angles in degrees, e.g. "euler 90.0 12.3 0.0"
	// and also quaternion raw values, e.g. "-0.803 0 0 0.52"
	virtual bool FromString(const std::string& s, void* p) const override
	{
		sh3 v;

		const char* readPtr = s.c_str();
		bool success = true;
		for (size_t i = 0; success && i < v.size(); i++)
		{
			success &= vcpp::parsers::parseFloat(&readPtr, v[i]);
		}

		SetValue(p, v);

		return success;
	}
};
DECL_PRIMITIVE_TYPE(sh3)

class Sh2Color3Type : public PrimitiveObjectType<sh2_color3>
{
public:
	Sh2Color3Type()
		: PrimitiveObjectType<sh2_color3>("sh2_color3")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		const sh2_color3& v = GetValue(p);

		s = "";
		for (size_t i = 0; i < v.num_coeff(); i++)
		{
			s += std::to_string(v.get_coeff(i));
			if (i != v.num_coeff() - 1)
				s += ' ';
		}
	}

	// can read euler angles in degrees, e.g. "euler 90.0 12.3 0.0"
	// and also quaternion raw values, e.g. "-0.803 0 0 0.52"
	virtual bool FromString(const std::string& s, void* p) const override
	{
		sh2_color3 v;

		const char* readPtr = s.c_str();
		bool success = true;
		for (size_t i = 0; success && i < v.num_coeff(); i++)
		{
			float coeff;
			success &= vcpp::parsers::parseFloat(&readPtr, coeff);
			v.set_coeff(i, coeff);
		}

		SetValue(p, v);

		return success;
	}
};
DECL_PRIMITIVE_TYPE(sh2_color3)

class Sh3Color3Type : public PrimitiveObjectType<sh3_color3>
{
public:
	Sh3Color3Type()
		: PrimitiveObjectType<sh3_color3>("sh3_color3")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		const sh3_color3& v = GetValue(p);

		s = "";
		for (size_t i = 0; i < v.num_coeff(); i++)
		{
			s += std::to_string(v.get_coeff(i));
			if (i != v.num_coeff() - 1)
				s += ' ';
		}
	}

	// can read euler angles in degrees, e.g. "euler 90.0 12.3 0.0"
	// and also quaternion raw values, e.g. "-0.803 0 0 0.52"
	virtual bool FromString(const std::string& s, void* p) const override
	{
		sh3_color3 v;

		const char* readPtr = s.c_str();
		bool success = true;
		for (size_t i = 0; success && i < v.num_coeff(); i++)
		{
			float coeff;
			success &= vcpp::parsers::parseFloat(&readPtr, coeff);
			v.set_coeff(i, coeff);
		}

		SetValue(p, v);

		return success;
	}
};
DECL_PRIMITIVE_TYPE(sh3_color3)
