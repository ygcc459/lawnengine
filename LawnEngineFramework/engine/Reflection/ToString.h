#pragma once

#include <string>
#include "ReflectiveObject.h"

namespace ToStringDetail
{
	//
	// ToString2: for ReflectiveObject subclasses, call their own member functions
	//
	template<bool IsBaseType, typename T>
	struct ToString2
	{
		static void ToString(const T& v, std::string& s)
		{
			GetStaticType<T>()->ToString(&v, s);
		}
	};

	template<typename T>
	struct ToString2<true, T>
	{
		static void ToString(const T& v, std::string& s)
		{
			v.ToString(s);
		}
	};

	//
	// ToString1: convert pointer types to their basic types
	//
	template<bool IsPointer, typename T>
	struct ToString1
	{
		static void ToString(const T& v, std::string& s)
		{
			ToString2<is_base<ReflectiveObject, T>::value, T>::ToString(v, s);
		}
	};

	template<typename T>
	struct ToString1<true, T>
	{
		static void ToString(const T& v, std::string& s)
		{
			typedef remove_pointer_type<T>::type converted_type;
			ToString2<is_base<ReflectiveObject, converted_type>::value, converted_type>::ToString(*v, s);
		}
	};
}

//
// ToString: convert to string, considering pointer types, and ReflectiveObject subclasses
//
template<class T>
std::string ToString(const T& v)
{
	std::string s;
	ToStringDetail::ToString1<is_pointer<T>::value, T>::ToString(v, s);
	return s;
}
