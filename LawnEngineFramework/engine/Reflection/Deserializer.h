#pragma once

#include "ToString.h"
#include "ReflectiveObject.h"
#include "../AssetSystem/Archive.h"
#include <stack>
#include <set>

class Deserializer
{
	ArchiveNode* rootNode;
	void* rootObjectPtr;

	std::stack<ArchiveNode*> nodeStack;
	std::map<int, ArchiveNode*> serializedObjectNodes;

	struct DeserializedObject
	{
		void* addr;
		PropertyType type;

		DeserializedObject()
			: addr(nullptr), type(PropertyType::Primitive)
		{}

		DeserializedObject(void* ptr, PropertyType type)
			: addr(ptr), type(type)
		{}
	};
	std::map<int, DeserializedObject> deserializedObjects;

	struct DeserializedSharedObject
	{
		std::shared_ptr<void> ptr;
		PropertyType type;

		DeserializedSharedObject()
			: ptr(nullptr), type(PropertyType::Primitive)
		{}

		DeserializedSharedObject(const std::shared_ptr<void>& ptr, PropertyType type)
			: ptr(ptr), type(type)
		{}
	};
	std::map<int, DeserializedSharedObject> deserializedSharedObjects;

	std::set<ReflectiveObject*> deserializedReflectiveObjectSet;

public:
	std::function<ReflectiveObject* (const BaseProperty* prop, Type* serializedType)> createReflectiveObjectCallback;

public:
	Deserializer(ArchiveNode* node);
	~Deserializer();

	// only valid if root object is ReflectiveObject, because we only serialize type-name for ReflectiveObjects
	std::string GetRootObjectTypeName();

	void DeserializeRootObject(ReflectiveObject* obj, const BaseProperty* prop);

	void DeserializeRootObject(ReflectiveObject* obj, const BaseProperty& prop)
	{
		DeserializeRootObject(obj, &prop);
	}

	void DeserializeRootObject(std::shared_ptr<ReflectiveObject> obj, const BaseProperty* prop);

	template<class T>
	bool DeserializeMemberAuto(const char* name, T& member)
	{
		std::unique_ptr<BaseProperty> prop(PropertyCreator::CreateProperty<T>());
		void* addr = &member;
		return DeserializeMember(name, addr, prop.get());
	}

	bool DeserializeMember(const char* name, void* obj, const BaseProperty* prop)
	{
		ArchiveNode* childnode = CurrentNode()->GetChild(name);
		if (childnode)
		{
			nodeStack.push(childnode);
			Deserialize(obj, prop);
			nodeStack.pop();

			return true;
		}
		else
		{
			return false;
		}
	}

	bool DeserializeMember(const char* name, void* obj, const BaseProperty& prop)
	{
		return DeserializeMember(name, obj, &prop);
	}

	void InvokeAfterDeserialization();

	template<class T>
	bool DeserializeMemberRawData(const char* name, std::vector<T>& v)
	{
		ArchiveNode* childnode = CurrentNode()->GetChild(name);
		if (childnode)
		{
			ArchiveString value = childnode->GetValue();
			if (value.IsValid())
			{
				if (value.Size() % sizeof(T) == 0)
				{
					v.resize(value.Size() / sizeof(T));
					memcpy(v.data(), value.Data(), value.Size());
					return true;
				}
				else
				{
					v.clear();
					return false;
				}
			}
		}

		v.clear();
		return false;
	}

	bool DeserializeMemberRawData(const char* name, byte** outBufferPtr, size_t* outSize)
	{
		ArchiveNode* childnode = CurrentNode()->GetChild(name);
		if (childnode)
		{
			ArchiveString value = childnode->GetValue();
			if (value.IsValid())
			{
				(*outBufferPtr) = (byte*)value.Data();
				(*outSize) = value.Size();
				return true;
			}
		}

		(*outBufferPtr) = nullptr;
		(*outSize) = 0;
		return false;
	}

	ArchiveNode* CurrentNode()
	{
		return nodeStack.top();
	}

	ArchiveNode* RootNode()
	{
		return rootNode;
	}

	bool IsRootObject(void* ptr) const
	{
		return ptr == rootObjectPtr;
	}

private:
	bool Deserialize(void* obj, const BaseProperty* prop);

	bool Deserialize(void* obj, const BasePrimitiveProperty* prop);

	bool Deserialize(void* obj, const BaseObjectProperty* prop);

	bool Deserialize(void* obj, const BaseArrayProperty* arrayProp);

	bool Deserialize(void* obj, const BaseMapProperty* mapProp);

	bool Deserialize(void* obj, const BaseRawPointerProperty* ptrProp);

	bool Deserialize(void* obj, const BaseSharedPointerProperty* ptrProp);

	void* CreateObjectFromDeserialize(const BaseProperty* prop);

};
