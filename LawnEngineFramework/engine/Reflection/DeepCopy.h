#pragma once

#include "ReflectiveObject.h"
#include "../AssetSystem/Archive.h"

//
// it's very hard to make a fast deep-copier
// cause it needs to iterate two objects simultaneously
// currently, we only serialize src, and then deserialize it !
//
class DeepCopier
{
	ReflectiveObject* src;
	ReflectiveObject* dst;

	Archive archive;

public:
	DeepCopier(ReflectiveObject* src, ReflectiveObject* dst)
		: src(src), dst(dst)
	{}

	~DeepCopier()
	{}

	void DoCopy()
	{
		archive.CreateNew();

		Serializer serializer(archive.Root());
		serializer.SerializeRootObject(src, ObjectProperty<ReflectiveObject>());

		Deserializer deserializer(archive.Root());
		deserializer.DeserializeRootObject(dst, ObjectProperty<ReflectiveObject>());

		deserializer.InvokeAfterDeserialization();
	}

	template<typename T>
	static std::shared_ptr<T> CopyAsShared(T* src)
	{
		std::shared_ptr<T> dst(new T());
		DeepCopier copier(src, dst.get());
		copier.DoCopy();
		return dst;
	}

	template<typename T>
	static T* Copy(T* src)
	{
		T* dst = new T();
		DeepCopier copier(src, dst);
		copier.DoCopy();
		return dst;
	}
};
