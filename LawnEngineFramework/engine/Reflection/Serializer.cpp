#include "fast_compile.h"
#include "Serializer.h"

Serializer::Serializer(ArchiveNode* node)
	: rootNode(node), rootObjectPtr(nullptr)
{
	nodeStack.push(node);
}

Serializer::~Serializer()
{
}

void Serializer::SerializeRootObject(void* obj, const BaseProperty* prop)
{
	rootObjectPtr = obj;

	int id = serializedObjectIds.size();
	serializedObjectIds.insert(std::make_pair(obj, id));
	CHECK(id == 0);

	SerializeToRootNode(id, obj, prop);
}

void Serializer::Serialize(void* obj, const BaseProperty* prop)
{
	if (prop->type == PropertyType::Primitive)
	{
		const BasePrimitiveProperty* primProp = reinterpret_cast<const BasePrimitiveProperty*>(prop);
		Serialize(obj, primProp);
	}
	else if (prop->type == PropertyType::Object)
	{
		const BaseObjectProperty* objProp = reinterpret_cast<const BaseObjectProperty*>(prop);
		Serialize(obj, objProp);
	}
	else if (prop->type == PropertyType::Array)
	{
		const BaseArrayProperty* arrayProp = reinterpret_cast<const BaseArrayProperty*>(prop);
		Serialize(obj, arrayProp);
	}
	else if (prop->type == PropertyType::Map)
	{
		const BaseMapProperty* mapProp = reinterpret_cast<const BaseMapProperty*>(prop);
		Serialize(obj, mapProp);
	}
	else if (prop->type == PropertyType::RawPointer)
	{
		const BaseRawPointerProperty* ptrProp = reinterpret_cast<const BaseRawPointerProperty*>(prop);
		Serialize(obj, ptrProp);
	}
	else if (prop->type == PropertyType::SharedPointer)
	{
		const BaseSharedPointerProperty* ptrProp = reinterpret_cast<const BaseSharedPointerProperty*>(prop);
		Serialize(obj, ptrProp);
	}
	else if (prop->type == PropertyType::Object)
	{
		const BaseObjectProperty* objProp = reinterpret_cast<const BaseObjectProperty*>(prop);
		Serialize(obj, objProp);
	}
	else
	{
		CHECK(false);
	}
}

void Serializer::Serialize(void* obj, const BasePrimitiveProperty* prop)
{
	Type* type = prop->valueType;
	type->Serialize(*this, obj);
}

void Serializer::Serialize(void* obj, const BaseObjectProperty* prop)
{
	Type* staticType = prop->valueType;
	CHECK(staticType->Category() == TypeCategory::Object);
	CHECK(staticType->IsSameOrChildOf(GetStaticType<ReflectiveObject>()));

	ReflectiveObject* reflectiveObject = reinterpret_cast<ReflectiveObject*>(obj);

	Type* dynamicType = GetDynamicType(*reflectiveObject);

	std::string typeName = dynamicType->TypeName();
	SerializeMember("_Type", &typeName, PrimitiveProperty<std::string>());

	dynamicType->Serialize(*this, obj);
}

void Serializer::Serialize(void* obj, const BaseArrayProperty* arrayProp)
{
	const BaseProperty* elementProp = arrayProp->GetElementProperty();
	size_t size = arrayProp->GetSize(obj);

	ArchiveNode* childnode = CurrentNode()->AddChild("Array");
	nodeStack.push(childnode);

	for (size_t i = 0; i < size; i++)
	{
		void* elementAddr = arrayProp->GetElementAddress(obj, i);

		SerializeMember("Element", elementAddr, elementProp);
	}

	nodeStack.pop();
}

void Serializer::Serialize(void* obj, const BaseMapProperty* mapProp)
{
	const BaseProperty* keyProp = mapProp->GetKeyProperty();
	const BaseProperty* valueProp = mapProp->GetValueProperty();

	ArchiveNode* childnode = CurrentNode()->AddChild("Map");
	nodeStack.push(childnode);

	for (BaseMapProperty::Iterator* it = mapProp->GetIterator(obj); it->IsValid(); it->Next())
	{
		nodeStack.push(CurrentNode()->AddChild("Element"));
		{
			SerializeMember("Key", it->GetKeyAddress(), keyProp);

			SerializeMember("Value", it->GetValueAddress(), valueProp);
		}
		nodeStack.pop();
	}

	nodeStack.pop();
}

void Serializer::Serialize(void* obj, const BaseRawPointerProperty* ptrProp)
{
	const BaseProperty* elementProp = ptrProp->GetDereferencedProperty();

	ArchiveNode* pointerNode = CurrentNode()->AddChild("Pointer");
	nodeStack.push(pointerNode);

	void* elementAddr = ptrProp->GetPtr(obj);
	if (elementAddr)
	{
		bool canSerialize = CanSerializePointer(elementProp, elementAddr);
		if (canSerialize)
		{
			AssetPath serializeAssetPath;
			bool serializeAsAssetPath = ShouldPointerSerializeAsAssetPath(elementProp, elementAddr, serializeAssetPath);

			if (serializeAsAssetPath)  // serialize asset path
			{
				SerializeMember("_AssetPath", &serializeAssetPath, PrimitiveProperty<AssetPath>());
			}
			else  // serialize object content
			{
				auto it = serializedObjectIds.find(elementAddr);
				if (it != serializedObjectIds.end())
				{
					int id = it->second;
					pointerNode->SetValue(id);
				}
				else
				{
					int id = serializedObjectIds.size();
					serializedObjectIds.insert(std::make_pair(elementAddr, id));

					pointerNode->SetValue(id);

					SerializeToRootNode(id, elementAddr, elementProp);
				}
			}
		}
		else  // skipped in serialize
		{
		}
	}
	else
	{
		// if null-pointer, will leave the node value empty
	}

	nodeStack.pop();
}

void Serializer::Serialize(void* obj, const BaseSharedPointerProperty* ptrProp)
{
	const BaseProperty* elementProp = ptrProp->GetDereferencedProperty();

	ArchiveNode* pointerNode = CurrentNode()->AddChild("Pointer");
	nodeStack.push(pointerNode);

	std::shared_ptr<void> elementSharedPtr = ptrProp->GetSharedPtr(obj);

	void* elementAddr = elementSharedPtr.get();
	if (elementAddr)
	{
		bool canSerialize = CanSerializePointer(elementProp, elementAddr);
		if (canSerialize)
		{
			AssetPath serializeAssetPath;
			bool serializeAsAssetPath = ShouldPointerSerializeAsAssetPath(elementProp, elementAddr, serializeAssetPath);

			if (serializeAsAssetPath)  // serialize asset path
			{
				SerializeMember("_AssetPath", &serializeAssetPath, PrimitiveProperty<AssetPath>());
			}
			else  // serialize object content
			{
				auto it = serializedObjectIds.find(elementAddr);
				if (it != serializedObjectIds.end())
				{
					int id = it->second;
					pointerNode->SetValue(id);
				}
				else
				{
					int id = serializedObjectIds.size();
					serializedObjectIds.insert(std::make_pair(elementAddr, id));

					pointerNode->SetValue(id);

					SerializeToRootNode(id, elementAddr, elementProp);
				}
			}
		}
		else  // skipped in serialize
		{
		}
	}
	else
	{
		// if null-pointer, will leave the node value empty
	}

	nodeStack.pop();
}

bool Serializer::CanSerializePointer(const BaseProperty* elementProp, void* elementAddr)
{
	if (elementProp->type == PropertyType::Object)
	{
		Type* type = elementProp->valueType;
		CHECK(type->Category() == TypeCategory::Object);
		CHECK(type->IsSameOrChildOf(GetStaticType<ReflectiveObject>()));

		ReflectiveObject* reflectiveObject = reinterpret_cast<ReflectiveObject*>(elementAddr);
		if (!CanSerialize(reflectiveObject))
		{
			return false;
		}
	}

	return true;
}

bool Serializer::ShouldPointerSerializeAsAssetPath(const BaseProperty* elementProp, void* elementAddr, AssetPath& outAssetPath)
{
	if (elementProp->type == PropertyType::Object)
	{
		Type* type = elementProp->valueType;
		if (type->IsSameOrChildOf(GetStaticType<BaseObject>()))
		{
			BaseObject* baseObject = reinterpret_cast<BaseObject*>(elementAddr);
			if (baseObject->IsAsset() && !IsRootObject(baseObject))
			{
				outAssetPath = baseObject->GetAssetPath();
				return true;
			}
		}
	}

	return false;
}

void Serializer::SerializeToRootNode(int id, void* obj, const BaseProperty* prop)
{
	ArchiveNode* childnode = RootNode()->AddChild("Object");
	childnode->SetChildValue<int>("_Id", id);

	nodeStack.push(childnode);
	{
		Serialize(obj, prop);
	}
	nodeStack.pop();
}

bool Serializer::CanSerialize(ReflectiveObject* objPtr)
{
	if (canSerializeReflectiveObjectCallback)
	{
		bool result = canSerializeReflectiveObjectCallback(objPtr);
		return result;
	}
	else
	{
		return true;
	}
}
