#pragma once

#include "ReflectiveObject.h"

class BaseAttribute : public ReflectiveObject
{
	DECL_ABSTRACT_CLASS_TYPE(BaseAttribute, ReflectiveObject);

public:
	BaseAttribute() {}
	virtual ~BaseAttribute() {}

};

class SkipSerializationAttribute : public BaseAttribute
{
	DECL_CLASS_TYPE(SkipSerializationAttribute, BaseAttribute);

public:
	SkipSerializationAttribute() {}
	virtual ~SkipSerializationAttribute() {}
};

class HideInInspectorAttribute : public BaseAttribute
{
	DECL_CLASS_TYPE(HideInInspectorAttribute, BaseAttribute);

public:
	HideInInspectorAttribute() {}
	virtual ~HideInInspectorAttribute() {}
};

class FloatRangeAttribute : public BaseAttribute
{
	DECL_CLASS_TYPE(FloatRangeAttribute, BaseAttribute);

public:
	float minValue;
	float maxValue;

	FloatRangeAttribute()
	{}

	FloatRangeAttribute(float minValue, float maxValue)
		: minValue(minValue), maxValue(maxValue)
	{}

	virtual ~FloatRangeAttribute() {}
};

class AssetSelectorAttribute : public BaseAttribute
{
	DECL_CLASS_TYPE(AssetSelectorAttribute, BaseAttribute);

public:
	AssetSelectorAttribute()
	{}
};
