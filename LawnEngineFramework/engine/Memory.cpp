#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "Memory.h"

Memory::TrackedObjectsContainer Memory::trackedObjects;

void Memory::TrackObject(TrackType type, void* obj, const std::string& name, const std::string& resourceName, void* parentObj)
{
	TrackedObjectInfo info;
	info.type = type;
	info.obj = obj;
	info.name = name;
	info.resourceName = resourceName;
	info.parentObj = parentObj;

	switch(type)
	{
	case TT_New:
	case TT_NewArray:
	case TT_Ctor:
	case TT_ComObjCreate:
	//case TT_ComObjAddRef:
		{
			auto iterSucceedPair = trackedObjects.insert(TrackedObjectsContainer::value_type(obj, info));
			if (iterSucceedPair.second == false)  // already have one
			{
				Log::Error("[Memory] already have an object at address '%08X', which may not have been released properly.", obj);
				LogSingleTrackedObject("[Memory]   the former may-be-unreleased object:", iterSucceedPair.first->second);
				assert(0);
			}
			break;
		}
	case TT_Delete:
	case TT_DeleteArray:
	case TT_Dtor:
	case TT_ComObjRelease:
		{
			TrackedObjectsContainer::iterator it = trackedObjects.find(obj);
			if (it == trackedObjects.end())
			{
				Log::Error("[Memory] object '%08X' released before it was created.", obj);
				LogSingleTrackedObject("[Memory]   the object:", info);
				DebugBreak();
			}
			else
			{
				auto type0 = it->second.type;
				if ((type == TT_Delete && type0 != TT_New) ||
					(type == TT_DeleteArray && type0 != TT_NewArray) ||
					(type == TT_Dtor && type0 != TT_Ctor) ||
					(type == TT_ComObjRelease && type0 != TT_ComObjAddRef))
				{
					Log::Error("[Memory] object '%08X' released in a different way as it was created.", obj);
					LogSingleTrackedObject("[Memory]   the object:", info);
					DebugBreak();
				}
				else
				{
					trackedObjects.erase(it);
				}
			}
			break;
		}
	}
}

void Memory::ReportTrackedObjects()
{
	Log::Info("[Memory] Reporting alive tracked objects -----------------------------------");

	TrackedObjectsContainer::iterator end = trackedObjects.end();
	for (TrackedObjectsContainer::iterator it = trackedObjects.begin(); it != end; ++it)
	{
		const TrackedObjectInfo& info = it->second;
		LogSingleTrackedObject("  ", info);
	}
	
	Log::Info("[Memory] Report over -----------------------------------");
}

void Memory::LogSingleTrackedObject(const char* prefix, const TrackedObjectInfo& info)
{
	Log::Info("%s {address:'%08X', name:'%s', resourceName:'%s', parentObj:'%08X'}", 
		prefix, info.obj, info.name.c_str(), info.resourceName.c_str(), info.parentObj);
}

void Memory::DebugBreak()
{
	assert(0);
}
