#pragma once

#include <string>
#include <map>
#include <atomic>

struct RefCountedObject
{
	std::atomic<uint32> refCounter;

	RefCountedObject()
		:refCounter(0)
	{}

	virtual ~RefCountedObject()
	{}

	void AddRef()
	{
		refCounter++;
	}

	void Release()
	{
		refCounter--;

		if (refCounter <= 0)
		{
			delete this;
		}
	}
};

template<class T>  //T must inherit from RefCountedObject
struct RefCountedPtr
{
	T* obj;

	static_assert(std::is_base_of<RefCountedObject, T>::value == true);

public:
	RefCountedPtr()
		: obj(nullptr)
	{}

	RefCountedPtr(std::nullptr_t)
		: obj(nullptr)
	{
	}

	explicit RefCountedPtr(T* obj)
		: obj(obj)
	{
		if (obj)
			obj->AddRef();
	}

	RefCountedPtr(const RefCountedPtr& other)
		: obj(other.obj)
	{
		if (obj)
			obj->AddRef();
	}

	~RefCountedPtr()
	{
		if (obj)
			obj->Release();
	}

	T* get()
	{
		return obj;
	}

	const T* get() const
	{
		return obj;
	}

	RefCountedObject* GetRefCountedObject()
	{
		return static_cast<RefCountedObject*>(obj);
	}

	T& operator * () const
	{
		return *obj;
	}

	T* operator -> () const
	{
		return obj;
	}

	explicit operator T* () const
	{
		return obj;
	}

	template<class TBaseClass, 
		typename std::enable_if<std::is_base_of<TBaseClass, T>::value, bool>::type = true>
	operator RefCountedPtr<TBaseClass>() const
	{
		TBaseClass* ptr = static_cast<TBaseClass*>(obj);
		return RefCountedPtr<TBaseClass>(ptr);
	}

	template<class TTarget>
	RefCountedPtr<TTarget> StaticCast() const
	{
		TTarget* ptr = static_cast<TTarget*>(obj);
		return RefCountedPtr<TTarget>(ptr);
	}

	template<class TTarget>
	RefCountedPtr<TTarget> ReinterpretCast() const
	{
		TTarget* ptr = reinterpret_cast<TTarget*>(obj);
		return RefCountedPtr<TTarget>(ptr);
	}

	template<class TTarget>
	RefCountedPtr<TTarget> DynamicCast() const
	{
		TTarget* ptr = dynamic_cast<TTarget*>(obj);
		return RefCountedPtr<TTarget>(ptr);
	}

	template<class TTarget>
	RefCountedPtr<TTarget> ConstCast() const
	{
		TTarget* ptr = const_cast<TTarget*>(obj);
		return RefCountedPtr<TTarget>(ptr);
	}

	void reset(T* newObj = nullptr)
	{
		(*this) = newObj;
	}

	RefCountedPtr<T> operator = (T* newObj)
	{
		if (obj != newObj)
		{
			if (obj)
				obj->Release();

			obj = newObj;

			if (obj)
				obj->AddRef();
		}

		return *this;
	}

	RefCountedPtr<T> operator = (const RefCountedPtr<T>& other)
	{
		if (obj != other.obj)
		{
			if (obj)
				obj->Release();

			obj = other.obj;

			if (obj)
				obj->AddRef();
		}

		return *this;
	}

	explicit operator bool () const
	{
		return obj != nullptr;
	}

	bool operator == (T* other) const
	{
		return obj == other;
	}

	bool operator == (const RefCountedPtr<T>& other) const
	{
		return obj == other.obj;
	}

	bool operator != (T* other) const
	{
		return obj != other;
	}

	bool operator != (const RefCountedPtr<T>& other) const
	{
		return obj != other.obj;
	}

	bool operator < (const RefCountedPtr<T>& other) const
	{
		return obj < other.obj;
	}

	bool operator > (const RefCountedPtr<T>& other) const
	{
		return obj > other.obj;
	}

	bool operator < (T* other) const
	{
		return obj < other;
	}

	bool operator > (T* other) const
	{
		return obj > other;
	}
};

enum TrackType
{
	TT_New, 
	TT_NewArray,
	TT_Delete,
	TT_DeleteArray,
	TT_Ctor,
	TT_Dtor,
	TT_ComObjCreate,
	TT_ComObjAddRef = TT_ComObjCreate,
	TT_ComObjRelease,
};

struct TrackedObjectInfo
{
	TrackType type;
	void* obj;
	std::string name;
	std::string resourceName;
	void* parentObj;
};

class Memory
{
	typedef std::map<void*, TrackedObjectInfo> TrackedObjectsContainer;
	static TrackedObjectsContainer trackedObjects;

public:
	static void TrackObject(TrackType type, void* obj, const std::string& name)
	{
		TrackObject(type, obj, name, "", NULL);
	}

	static void TrackObject(TrackType type, void* obj, const std::string& name, const std::string& resourceName, void* parentObj);
	
	static void ReportTrackedObjects();

private:
	static void LogSingleTrackedObject(const char* prefix, const TrackedObjectInfo& info);
	static void DebugBreak();
};
