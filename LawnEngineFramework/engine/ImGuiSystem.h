#pragma once

#include "UISystem/imgui/imgui.h"

class ImGUI
{
public:
	static void Startup(void* windowHandle);
	static void Shutdown();

	static void BeginFrame();
	static void Render();
	static void EndFrame();

};
