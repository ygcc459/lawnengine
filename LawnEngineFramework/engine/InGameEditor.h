#pragma once

#include "UISystem/imgui/imgui.h"
#include "UISystem/imgui/imgui_stdlib.h"
#include "InputSystem/InputManager.h"

class SceneObject;
class InspectorWindow;
class UICanvas;
class UIRawImage;
class Texture;
typedef std::shared_ptr<Texture> TexturePtr;
class Material;
typedef std::shared_ptr<Material> MaterialPtr;

class TranslationHandle : public BaseObject, public MouseEventListener
{
public:
	TranslationHandle();
	~TranslationHandle();

	void Render();

	virtual float GetMouseRaycastPriority() override;
	virtual BaseObject* OnMouseRaycast(const MouseEvent& ev, const MouseState& state) override;
	virtual void OnMouseEvent(BaseObject* raycastResult, const MouseEvent& ev, const MouseState& state) override;
};

class SceneHierarchyWindow : public BaseObject
{
	bool show;

public:
	SceneHierarchyWindow();
	~SceneHierarchyWindow();

	void Startup();
	void Shutdown();
	void Render();

	void SetSelectedObject(SceneObject* obj);

private:
	ImGuiTreeNodeFlags baseNodeFlags;

	SceneObject* clickingSceneObject;
	
	struct SceneObjectNode
	{
		bool selected;

		SceneObjectNode()
			: selected(false)
		{}
	};

	std::map<SceneObject*, std::unique_ptr<SceneObjectNode>> nodes;

	SceneObjectNode* GetOrCreateSceneObjectNode(SceneObject* so);
	void OnWorldSceneObjectDestroyed(SceneObject* so);

	void RenderSceneObjectNode(SceneObject* so);
	void ClearSceneObjectNodeSelection();
};

class InspectorWindow : BaseObject
{
	bool show;

public:
	InspectorWindow();
	~InspectorWindow();

	void Startup();
	void Shutdown();
	void Render();

	void SetTarget(BaseObject* target);

private:
	ImGuiTableFlags tableFlags;

	std::vector<BaseObject*> navigationStack;
	int navigationCurrentIndex;

	void RenderSceneObjectInspector(const std::string& name, SceneObject* so);
	void RenderMaterialInspector(const std::string& name, Material* mat);

	void NavigationClear();
	void NavigationPush(BaseObject* obj);
	BaseObject* NavigationGetCurrent();
	bool NavigationCanGoBack();
	void NavigationGoBack();
	bool NavigationCanGoForward();
	void NavigationGoForward();
	void NavigationRemove(BaseObject* obj);

	void RenderReflectiveObjectMembers(const std::string& name, ReflectiveObject* reflectiveObject, const BaseObjectProperty* prop = nullptr, const BaseProperty* parentProp = nullptr, void* parentObj = nullptr);
	
	template<typename PointerType>
	PointerType RenderBaseObjectPicker(PointerType value, Type* valueType);

	void RenderBaseProperty(const std::string& name, void* obj, const BaseProperty* prop, const BaseProperty* parentProp, void* parentObj, ReflectiveObject* ownerObject, ObjectMember* member);
	void RenderBasePrimitiveProperty(const std::string& name, void* obj, const BasePrimitiveProperty* prop, const BaseProperty* parentProp, void* parentObj, ReflectiveObject* ownerObject, ObjectMember* member);
	void RenderBaseObjectProperty(const std::string& name, void* obj, const BaseObjectProperty* prop, const BaseProperty* parentProp, void* parentObj, ReflectiveObject* ownerObject, ObjectMember* member);	
	void RenderBaseArrayProperty(const std::string& name, void* obj, const BaseArrayProperty* arrayProp, const BaseProperty* parentProp, void* parentObj, ReflectiveObject* ownerObject, ObjectMember* member);
	void RenderBaseMapProperty(const std::string& name, void* obj, const BaseMapProperty* mapProp, const BaseProperty* parentProp, void* parentObj, ReflectiveObject* ownerObject, ObjectMember* member);
	void RenderBaseRawPointerProperty(const std::string& name, void* obj, const BaseRawPointerProperty* ptrProp, const BaseProperty* parentProp, void* parentObj, ReflectiveObject* ownerObject, ObjectMember* member);
	void RenderBaseSharedPointerProperty(const std::string& name, void* obj, const BaseSharedPointerProperty* ptrProp, const BaseProperty* parentProp, void* parentObj, ReflectiveObject* ownerObject, ObjectMember* member);
};

class AssetsWindow : public BaseObject
{
	bool show;

	AssetPath rootPath;
	AssetPath currentPath;

	std::set<AssetPath> selectedPaths;

	std::vector<AssetPath> files;

	AssetPath clickingPath;

public:
	AssetsWindow();
	~AssetsWindow();

	void Startup();
	void Shutdown();
	void Render();

	void RenderTreeNode(const AssetPath& path);
	void RenderFilesView();
	void RenderFileContextMenu(const AssetPath& path);

	void SetSelection(const AssetPath& path);
	void SetSelection(const std::vector<std::string>& pathParts);

	AssetPath GetSelection();
};

class UIRawImage;

class TexturePreviewWindow : public BaseObject
{
	bool show;

	UICanvas* uiCanvas;
	UIRawImage* uiRawImage;
	TexturePtr texture;
	MaterialPtr uiRawImageInspectorMaterial;

	bool channelEnabled[4];

public:
	TexturePreviewWindow();
	~TexturePreviewWindow();

	void SetEnabled(bool b);

	void Startup();
	void Shutdown();
	void Render();

	void SetTexture(TexturePtr texture);

	void OnChannelEnabledChanged();
};

struct GizmoGroup;

class InGameEditor : public PointerEventListener, public BaseObject
{
public:
	static InGameEditor instance;

	SceneHierarchyWindow sceneHierarchyWindow;
	InspectorWindow inspectorWindow;
	AssetsWindow assetsWindow;
	TexturePreviewWindow texturePreviewWindow;

	BaseObject* selectedObject;

	GizmoGroup* axisGizmoGroup;
	GizmoGroup* gridGizmoGroup;

public:
	InGameEditor();
	virtual ~InGameEditor();

	void Startup();
	void Shutdown();
	void Render();

	static InGameEditor& GetInstance() { return instance; }

	void SetSelectedObject(BaseObject* obj);
	BaseObject* GetSelectedObject() { return selectedObject; }

	virtual float GetPointerEventPriority() override;
	virtual bool OnPointerClick(PointerState& pointer) override;
	virtual bool OnPointerBeginDrag(PointerState& pointer) override;
	virtual bool OnPointerDragging(PointerState& pointer) override;
	virtual bool OnPointerEndDrag(PointerState& pointer) override;
};
