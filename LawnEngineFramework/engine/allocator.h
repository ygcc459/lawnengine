#pragma once

template<typename TElement>
class DefaultAllocator
{
public:
	typedef size_t size_type;
	typedef TElement element_type;

	static element_type* Allocate(size_type byteSize)
	{
		element_type* p = ::malloc(byteSize);
		return p;
	}

	static void Free(element_type* p)
	{
		::free(p);
	}
};
