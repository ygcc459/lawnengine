
#include "Renderer.h"

#include "RenderSystem/Mesh.h"
#include "RenderSystem/Material.h"

class MeshRendererSceneObject;

class MeshRenderer : public Renderer
{
	DECL_CLASS_TYPE(MeshRenderer, Renderer);
	
public:
	MeshRenderer();
	virtual ~MeshRenderer();

	virtual void OnStartup() override;
	virtual void OnShutdown() override;

	virtual void Reflect(Reflector& reflector) override;

	virtual float OnEditorPick(const EditorPickInput& input) override;
	virtual void OnEditorDrawGizmo() override;

	void SetMaterial(MaterialPtr material);
	MaterialPtr GetMaterial();

	void SetMesh(MeshPtr mesh);
	MeshPtr GetMesh() { return mesh; }

	virtual AxisAlignedBox3<float> GetWorldBoundingBox() const override;

protected:
	virtual void CreateRenderSceneObject() override;
	virtual void DestroyRenderSceneObject() override;

protected:
	MeshPtr mesh;

	MaterialPtr material;

	MeshRendererSceneObject* renderSceneObject;
};
