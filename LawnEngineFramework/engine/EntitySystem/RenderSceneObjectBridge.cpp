#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "EntitySystemInternal.h"
#include "RenderSceneObjectBridge.h"
#include "SceneObject.h"
#include "../RenderSystem/RenderScene.h"

IMPL_ABSTRACT_CLASS_TYPE(RenderSceneObjectBridge);

RenderSceneObjectBridge::RenderSceneObjectBridge()
{
}

RenderSceneObjectBridge::~RenderSceneObjectBridge()
{
}

void RenderSceneObjectBridge::OnStartup()
{
	Super::OnStartup();

	GetSceneObject().GetTransform().transformChangedEvent.AddListener(this, [=](Transform*) { SetPendingUpdateRenderScene(); });
}

void RenderSceneObjectBridge::OnShutdown()
{
	Super::OnShutdown();

	GetSceneObject().GetTransform().transformChangedEvent.RemoveListener(this);
}

void RenderSceneObjectBridge::SetPendingUpdateRenderScene()
{
	if (!pendingUpdateRenderSceneHandle.IsRegistered())
	{
		World* world = GetWorld();
		if (world)
		{
			world->PendingUpdateRenderSceneRenderers().Register(this, pendingUpdateRenderSceneHandle);
		}
	}
}

void RenderSceneObjectBridge::UpdateRenderScene()
{
	DestroyRenderSceneObject();
	CreateRenderSceneObject();
}

void RenderSceneObjectBridge::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);
}

void RenderSceneObjectBridge::OnPropertyChangedByReflection(ObjectMember& member)
{
	SetPendingUpdateRenderScene();
}
