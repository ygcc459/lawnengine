#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "EntitySystemInternal.h"
#include "SceneSettings.h"

#include "SceneObject.h"

#include "RenderSystem/RenderScene.h"

IMPL_CLASS_TYPE(SceneSettings);

SceneSettings::SceneSettings()
	: Super()
{
	renderSettings = new RenderSceneSettings();

	renderSettings->eventPropertyChangedByReflection.AddListener(this, [=]()
		{
			this->SetPendingUpdateRenderScene();
		});
}

SceneSettings::~SceneSettings()
{
	delete renderSettings;
}

void SceneSettings::OnStartup()
{
	Super::OnStartup();

	if (GetRenderScene())
	{
		SetPendingUpdateRenderScene();
	}
}

void SceneSettings::OnShutdown()
{
	Super::OnShutdown();
}

void SceneSettings::CreateRenderSceneObject()
{
	RenderScene* renderScene = GetRenderScene();
	if (renderScene)
	{
		renderScene->UpdateRenderSceneSettings(*renderSettings);
	}
}

void SceneSettings::DestroyRenderSceneObject()
{
}

void SceneSettings::UpdateRenderScene()
{
	RenderScene* renderScene = GetRenderScene();
	if (renderScene)
	{
		renderScene->UpdateRenderSceneSettings(*renderSettings);
	}
}

void SceneSettings::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("renderSettings", &Self::renderSettings);
}
