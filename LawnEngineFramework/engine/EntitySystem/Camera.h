#pragma once

#include "Component.h"
#include "RenderSystem/RenderSystemCommon.h"
#include "EntitySystem/RenderSceneObjectBridge.h"
#include "RenderSystem/Texture2D.h"
#include <vcpp/la/rect.h>

class RenderView;
class RenderSceneCamera;

class Camera : public RenderSceneObjectBridge
{
	DECL_CLASS_TYPE(Camera, RenderSceneObjectBridge);

	RenderViewPtr targetRenderView;

	float fov;
	float znear;
	float zfar;
	
	vcpp::la::rect<float> relativeViewport;  //0 to 1
	vcpp::la::rect<uint> actualViewport;  //in pixels
		
public:
	Camera();
	virtual ~Camera();

	virtual void OnStartup();
	virtual void OnShutdown();

	virtual void CreateRenderSceneObject() override;
	virtual void DestroyRenderSceneObject() override;
	virtual void UpdateRenderScene() override;

	virtual void Reflect(Reflector& reflector) override;

	void Render();

	void SetRenderTarget(RenderViewPtr targetRenderView);
	RenderViewPtr GetRenderTarget() { return targetRenderView; }

	void OnRenderTargetResized(uint w, uint h);
	
	float GetAspectRatio() { return actualViewport.w / (float)actualViewport.h; }
	float GetFov() { return fov; }
	float GetZNear() { return znear; }
	float GetZFar() { return zfar; }

	float4x4 GetViewMatrix();
	float4x4 GetProjectionMatrix();

	//screenPoint: left-top=(0, 0), right-bottom=(viewport.w, viewport.h)
	float2 ScreenPointToViewport(const float2& screenPoint);

	//viewportPoint: left-top=(-1, -1), right-bottom=(1, 1)
	float3 ViewportPointToRayDir(const float2& viewportPoint);

	//screenPoint: left-top=(0, 0), right-bottom=(viewport.w, viewport.h)
	float3 ScreenPointToRayDir(const float2& screenPoint)
	{
		return ViewportPointToRayDir(ScreenPointToViewport(screenPoint));
	}

	void SetFov(float v)
	{
		fov = v;
	}

	void SetZNear(float v)
	{
		znear = v;
	}

	void SetZFar(float v)
	{
		zfar = v;
	}

protected:
	void UpdateViewport(uint w, uint h);

	RenderSceneCamera* renderSceneCamera;
};
