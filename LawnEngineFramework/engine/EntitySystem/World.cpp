#include "../fast_compile.h"
#include "LawnEngineInternal.h"
#include "EntitySystemInternal.h"
#include "World.h"
#include "../RenderSystem/RenderScene.h"
#include "WorldQuery.h"

IMPL_CLASS_TYPE(World);

World::World()
	: CreateSceneObjectEvent(this)
	, DestroySceneObjectEvent(this)
{
	worldQuery = new WorldQuery(*this);

	renderScene = new RenderScene();
}

World::~World()
{
	OnDestroy();
}

void World::OnDestroy()
{
	DestroyAllSceneObjects();

	SAFE_DELETE(worldQuery);

	SAFE_DELETE(renderScene);  //TODO: enqueue render command

	BaseObject::OnDestroy();
}

void World::Tick()
{
	TriggerComponentUpdates();
}

void World::PreRenderUpdate()
{
	// make a copy, cause the list can be modified during updating
	updatingComponents = componentRegistry.GetComponents(CRT_PreRenderUpdate);

	for (int i = 0; i < updatingComponents.size(); i++)
	{
		Component* c = updatingComponents[i];
		c->OnPreRenderUpdate();
	}
}

void World::EditorDrawGizmo()
{
	// make a copy, cause the list can be modified during updating
	updatingComponents = componentRegistry.GetComponents(CRT_EditorDrawGizmo);

	for (int i = 0; i < updatingComponents.size(); i++)
	{
		Component* c = updatingComponents[i];
		c->OnEditorDrawGizmo();
	}
}

void World::CreateSceneObjectInternal(SceneObject* p, bool startup /*= true*/)
{
	if (p->started)
	{
		Log::Error("World::CreateSceneObjectInternal must be called before SceneObject Startup");
		return;
	}

	sceneObjects.push_back(p);
	p->world = this;

	if (startup)
	{
		p->Startup(true);
	}

	CreateSceneObjectEvent.Trigger(p);
}

void World::DestroySceneObject(SceneObject* so)
{
	std::vector<SceneObject*> sceneObjectsToDelete;
	CollectSceneObjectsDepthFirst(so, sceneObjectsToDelete);

	for (size_t i = 0; i < sceneObjectsToDelete.size(); i++)
	{
		DestroySceneObjectInternal(sceneObjectsToDelete[i]);
	}
}

void World::CollectSceneObjectsDepthFirst(SceneObject* so, std::vector<SceneObject*>& outSceneObjects)
{
	for (int i = 0; i < so->transform->children.size(); i++)
	{
		SceneObject& child = so->transform->children[i]->GetSceneObject();
		CollectSceneObjectsDepthFirst(&child, outSceneObjects);
	}

	outSceneObjects.push_back(so);
}

void World::DestroySceneObjectInternal(SceneObject* so)
{
	DestroySceneObjectEvent.Trigger(so);

	// 1.children must been destroyed first
	CHECK(so->transform->children.size() == 0);

	// 2.shutdown components
	//   must happen before detach from parent
	so->Shutdown(true);

	// 3.detach from parent, without sending any events
	so->transform->SetParent(nullptr, false);

	// 4.destroy components
	so->DestroyAllComponentsInternal();

	// 5.detach from world
	for (size_t i = 0; i < sceneObjects.size(); i++)
	{
		if (sceneObjects[i] == so)
		{
			sceneObjects.erase(sceneObjects.begin() + i);
		}
	}

	// 6.destroy scene object
	delete so;
}

void World::DestroyAllSceneObjects()
{
	std::vector<SceneObject*> sceneObjectsToDelete;

	for (auto so : sceneObjects)
	{
		if (so->transform->parent == nullptr)  //only collect from root scene objects
		{
			CollectSceneObjectsDepthFirst(so, sceneObjectsToDelete);
		}
	}

	for (size_t i = 0; i < sceneObjectsToDelete.size(); i++)
	{
		DestroySceneObjectInternal(sceneObjectsToDelete[i]);
	}
}

Component* World::GetSingletonComponent(Type* type, bool autoCreate /*= true*/)
{
	auto it = managerComponents.find(type);
	if (it != managerComponents.end())
	{
		Component* c = it->second;
		return c;
	}
	else
	{
		if (autoCreate)
		{
			Component* c = CreateSingletonComponentInternal(type);
			managerComponents.insert(std::make_pair(type, c));
			return c;
		}
		else
		{
			return nullptr;
		}
	}
}

Component* World::CreateSingletonComponentInternal(Type* type)
{
	SceneObject* so = CreateSceneObject();
	so->name = type->TypeName();
	
	Component* comp = so->AddComponent(type);

	managerComponents.insert(std::make_pair(type, comp));

	return comp;
}

void World::OnSingletonComponentDestroy(Type* type)
{
	auto it = managerComponents.find(type);
	if (it != managerComponents.end())
	{
		managerComponents.erase(it);
	}
}

void World::UpdateRenderScene()
{
	for (int32 i = 0; i < pendingUpdateRenderSceneRenderers.GetSize(); i++)
	{
		RenderSceneObjectBridge* renderer = pendingUpdateRenderSceneRenderers[i];
		renderer->UpdateRenderScene();
	}

	pendingUpdateRenderSceneRenderers.UnregisterAll();
}

void World::TriggerComponentUpdates()
{
	// make a copy, cause the list can be modified during updating
	updatingComponents = componentRegistry.GetComponents(CRT_Update);

	for (int i = 0; i < updatingComponents.size(); i++)
	{
		Component* c = updatingComponents[i];
		c->OnUpdate();
	}
}

void World::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("sceneObjects", &Self::sceneObjects);
}

void World::GetComponents(Type* type, std::vector<Component*>& outComponents)
{
	for (SceneObject* obj : sceneObjects)
	{
		obj->GetComponentsInChildren(type, outComponents);
	}
}

void World::PrintHierarchy()
{
	Log::Info("====================== World::PrintHierarchy begin ======================");

	for (auto so : sceneObjects)
	{
		if (so->GetParent() == nullptr)
			PrintHierarchy(so, "");
	}

	Log::Info("====================== World::PrintHierarchy end  ======================");
}

void World::PrintHierarchy(SceneObject* so, const std::string& indent)
{
	Type* soType = so->GetType();
	Log::Info("%s[%s]%s (%08p):", indent.c_str(), soType->TypeName().c_str(), so->name.c_str(), so);

	for (auto i = 0; i < so->components.size(); i++)
	{
		Component* c = so->components[i];

		Type* type = c->GetType();

		Log::Info("%s    [%s] (%08p)", indent.c_str(), type->TypeName().c_str(), c);
	}

	for (size_t iChild = 0; iChild < so->transform->children.size(); iChild++)
	{
		SceneObject& childSO = so->transform->children[iChild]->GetSceneObject();
		PrintHierarchy(&childSO, indent + "    ");
	}
}
