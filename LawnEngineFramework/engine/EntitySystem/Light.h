#pragma once

#include "Component.h"
#include "RenderSceneObjectBridge.h"
#include "../RenderSystem/RenderSystemCommon.h"

class RenderSceneLight;

class Light : public RenderSceneObjectBridge
{
	DECL_ABSTRACT_CLASS_TYPE(Light, RenderSceneObjectBridge);

public:
	LightType type;

	float3 color;
	float intensity;

	bool generateShadow;

	Light(LightType type);
	virtual ~Light();

	virtual void OnStartup() override;
	virtual void OnShutdown() override;

	virtual void Reflect(Reflector& reflector) override;

	virtual void OnPropertyChangedByReflection(ObjectMember& member) override;

	virtual void CreateRenderSceneObject() override;
	virtual void DestroyRenderSceneObject() override;

	virtual void FillRenderSceneLight(RenderSceneLight* light) = 0;

private:
	RenderSceneLight* renderSceneLight;
};

class DirectionalLight : public Light
{
	DECL_CLASS_TYPE(DirectionalLight, Light);

public:
	DirectionalLight();
	virtual ~DirectionalLight();

	virtual void Reflect(Reflector& reflector) override;
	virtual void FillRenderSceneLight(RenderSceneLight* light) override;
};

class PointLight : public Light
{
	DECL_CLASS_TYPE(PointLight, Light);

public:
	PointLight();
	virtual ~PointLight();

	virtual void Reflect(Reflector& reflector) override;
	virtual void FillRenderSceneLight(RenderSceneLight* light) override;
};

class SpotLight : public Light
{
	DECL_CLASS_TYPE(SpotLight, Light);

public:
	RadianAngle innerConeAngle;  //ranges [0, PI), should be smaller than outerConeAngle
	RadianAngle outerConeAngle;  //ranges [0, PI)
	float falloff;  //Decrease in illumination between the edge of the inner cone and the outer cone, usually 1.0f

	SpotLight();
	virtual ~SpotLight();

	virtual void Reflect(Reflector& reflector) override;
	virtual void FillRenderSceneLight(RenderSceneLight* light) override;
};
