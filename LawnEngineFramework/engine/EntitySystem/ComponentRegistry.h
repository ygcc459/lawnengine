#pragma once

#include <vector>
#include <map>

class Component;

enum ComponentRegistryType
{
	CRT_EarlyUpdate,
	CRT_Update,
	CRT_LateUpdate,
	CRT_PreRenderUpdate,
	CRT_Cameras,
	CRT_Lights,
	CRT_EditorPickable,
	CRT_EditorDrawGizmo,

	CRT_Count,
};

class ComponentRegistry
{
public:
	ComponentRegistry()
	{
		registries = new SingleRegistry[CRT_Count];
	}

	~ComponentRegistry()
	{
		delete[] registries;
	}

	void Register(ComponentRegistryType type, Component* comp)
	{
		SingleRegistry& reg = registries[type];

		auto it = reg.componentToIndex.find(comp);		
		if (it == reg.componentToIndex.end())
		{
			int index = (int)reg.components.size();
			reg.components.push_back(comp);
			reg.componentToIndex.insert(std::make_pair(comp, index));
		}
	}

	void Unregister(ComponentRegistryType type, Component* comp)
	{
		SingleRegistry& reg = registries[type];

		auto it = reg.componentToIndex.find(comp);
		if (it != reg.componentToIndex.end())
		{
			int index = it->second;

			// swap with the last element
			if (index != reg.components.size() - 1)
			{
				Component* lastElement = reg.components[reg.components.size() - 1];
				
				reg.components[index] = lastElement;
				reg.componentToIndex[lastElement] = index;
			}

			reg.components.erase(reg.components.end() - 1);  // remove the last element
			reg.componentToIndex.erase(it);
		}
	}

	void Unregister(Component* comp)
	{
		for (int i = 0; i < CRT_Count; i++)
		{
			ComponentRegistryType type = (ComponentRegistryType)i;
			Unregister(type, comp);
		}
	}

	bool IsRegistered(ComponentRegistryType type, Component* comp) const
	{
		SingleRegistry& reg = registries[type];

		auto it = reg.componentToIndex.find(comp);
		return (it != reg.componentToIndex.end());
	}

	std::vector<Component*>& GetComponents(ComponentRegistryType type)
	{
		SingleRegistry& reg = registries[type];

		return reg.components;
	}

	template<class T>
	void GetComponents(ComponentRegistryType type, OUT T* & outArray, OUT int& outSize)
	{
		SingleRegistry& reg = registries[type];

		outArray = reinterpret_cast<T*>(reg.components.data());
		outSize = reg.components.size();
	}

protected:
	struct SingleRegistry
	{
		std::vector<Component*> components;
		std::map<Component*, int> componentToIndex;
	};

	SingleRegistry* registries;

};
