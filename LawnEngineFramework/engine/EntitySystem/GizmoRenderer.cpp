#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "EntitySystemInternal.h"
#include "GizmoRenderer.h"
#include "EntitySystemInternal.h"
#include "RenderSystem/RenderScene.h"
#include "RenderSystem/RenderCommands.h"

struct GizmoVertex
{
	float3 pos;
	color32 color;

	GizmoVertex()
	{}

	GizmoVertex(const float3& pos, color32 color)
		: pos(pos), color(color)
	{}
};

class GizmoRenderSceneObject : public RenderSceneObject
{
public:
	GizmoRenderSceneObject(GizmoRenderer* renderer, MaterialRenderDataPtr materialRenderData)
		: RenderSceneObject()
		, renderer(renderer)
		, materialRenderData(materialRenderData)
	{
	}

	virtual ~GizmoRenderSceneObject()
	{}

	void UpdateLinesMesh()
	{
	}

	virtual void OnUpdateMeshDrawCommands(RenderSceneCustomPassObjectContext& context) override
	{
		UpdateLinesMesh();

		renderer->linesMesh->UpdateGpuBuffers(renderResourcesManager);
		renderer->sphereMesh->UpdateGpuBuffers(renderResourcesManager);

		//
		// pass params
		//
		ShaderParamValues passParams;
		passParams.SetFloat4x4("viewMatrix", camera->viewMatrix);
		passParams.SetFloat4x4("projMatrix", camera->projectionMatrix);
		passParams.SetFloat4x4("viewProjMatrix", camera->viewProjectionMatrix);
		passParams.SetFloat3("cameraPos", camera->position);

		ShaderParamValueStack shaderParamValueStack(pass);
		shaderParamValueStack.Push(&passParams);

		//
		// lines
		//
		size_t meshDrawCommandIndex = 0;
		if (renderer->linesMesh->vertexCount > 0)
		{
			MeshDrawCommand* meshDrawCommand = new MeshDrawCommand();

			meshDrawCommand->BuildGraphicsState(renderer->linesMesh, *pass);

			float4x4 world_matrix = matrix_identity_4x4();
			objectParams.SetFloat4x4("worldMatrix", world_matrix);
			objectParams.SetFloat4("tintColor", float4(1, 1, 1, 1));

			ShaderParamValueStack stack(pass);
			stack.Push(&passParams);
			stack.Push(&material->GetBoundParamValues());
			stack.Push(&objectParams);
			stack.FillMeshDrawCommand(renderResourcesManager, meshDrawCommand);

			meshDrawCommand->UploadBuffers(renderResourcesManager);
			meshDrawCommands.push_back(meshDrawCommand);
		}

		//
		// spheres
		//
		for (int j = 0; j < allSpheres.size(); j++)
		{
			GizmoSphere& sphere = allSpheres[j];

			MeshDrawCommand* meshDrawCommand = new MeshDrawCommand();

			pass->FillMeshDrawCommand(meshDrawCommand);

			float4x4 world_matrix = mul(matrix_scaling(sphere.radius), matrix_translation(sphere.center));
			objectParams.SetFloat4x4("worldMatrix", world_matrix);
			objectParams.SetFloat4("tintColor", to_vector4<float>(sphere.color));

			ShaderParamValueStack stack(pass);
			stack.Push(&passParams);
			stack.Push(&material->GetBoundParamValues());
			stack.Push(&objectParams);
			stack.FillMeshDrawCommand(renderResourcesManager, meshDrawCommand);

			renderer->sphereMesh->FillMeshDrawCommand(pass, meshDrawCommand);

			meshDrawCommand->UploadBuffers(renderResourcesManager);
			meshDrawCommands.push_back(meshDrawCommand);
		}

		//
		// triangle meshes
		//
		for (int j = 0; j < allTriangleMeshes.size(); j++)
		{
			GizmoTriangleMesh& mesh = allTriangleMeshes[j];

			MeshDrawCommand* meshDrawCommand = new MeshDrawCommand();

			pass->FillMeshDrawCommand(meshDrawCommand);

			float4x4 world_matrix = matrix_identity_4x4();
			objectParams.SetFloat4x4("worldMatrix", world_matrix);
			objectParams.SetFloat4("tintColor", float4(1, 1, 1, 1));

			ShaderParamValueStack stack(pass);
			stack.Push(&passParams);
			stack.Push(&material->GetBoundParamValues());
			stack.Push(&objectParams);
			stack.FillMeshDrawCommand(renderResourcesManager, meshDrawCommand);

			meshDrawCommand->primitiveTopology = PrimitiveTopology::PT_TriangleList;

			meshDrawCommand->vertexComponents.push_back(MeshVertexComponent(InputElement::IE_POSITION, 0, MeshVertexFormat(MVBF_Float, 3), 0));
			meshDrawCommand->vertexComponents.push_back(MeshVertexComponent(InputElement::IE_VERTEXCOLOR, 1, MeshVertexFormat(MVBF_Byte, 4), 0));

			RHIBufferPtr vb1 = renderResourcesManager.rhiDevice->CreateBuffer(BufferDesc(sizeof(float3) * mesh.positions.size(), BBF_VERTEX_BUFFER), mesh.positions.data());
			meshDrawCommand->vertexBuffers.push_back(vb1);
			meshDrawCommand->vertexBufferStrides.push_back(sizeof(float3));
			meshDrawCommand->vertexBufferOffsets.push_back(0);

			RHIBufferPtr vb2 = renderResourcesManager.rhiDevice->CreateBuffer(BufferDesc(sizeof(color32) * mesh.colors.size(), BBF_VERTEX_BUFFER), mesh.colors.data());
			meshDrawCommand->vertexBuffers.push_back(vb2);
			meshDrawCommand->vertexBufferStrides.push_back(sizeof(color32));
			meshDrawCommand->vertexBufferOffsets.push_back(0);

			meshDrawCommand->vertexCount = mesh.positions.size();
			meshDrawCommand->indexCount = 0;

			meshDrawCommand->UploadBuffers(renderResourcesManager);
			meshDrawCommands.push_back(meshDrawCommand);
		}
		//*/
	}

public:
	ShaderParamValues objectParams;
	GizmoRenderer* renderer;

	std::vector<GizmoLine> allLines;
	std::vector<GizmoSphere> allSpheres;
	std::vector<GizmoTriangleMesh> allTriangleMeshes;

	MaterialRenderDataPtr materialRenderData;
};


class GizmoRendererSceneObject : public RenderSceneObject
{
public:
	GizmoRendererSceneObject(MaterialRenderDataPtr materialRenderData, MeshPtr lineMeshRenderData)
		: RenderSceneObject()
		, materialRenderData(materialRenderData)
		, worldBoundingBox(worldBounds)
		, meshRenderData(meshRenderData)
		, cullingInputHandle(-1)
	{
		objectParamsCpuBuffer.worldMatrix = worldMatrix;
	}

	virtual ~GizmoRendererSceneObject()
	{}

	virtual void FillProperties(RenderSceneObjectProperties& prop) override
	{
		prop.isDynamic = false;
	}

	SceneObjectRenderQueueType GetRenderQueueType() const
	{
		if (materialRenderData->GetBlendMode() == BlendMode::BlendMode_Opaque)
		{
			return SceneObjectRenderQueueType::Opaque;
		}
		else if (materialRenderData->GetBlendMode() == BlendMode::BlendMode_Opaque)
		{
			return SceneObjectRenderQueueType::Opaque;
		}
		else
		{
			assert(false);
			return SceneObjectRenderQueueType::Opaque;
		}
	}

	virtual void PrepareDrawCommands(PrepareDrawCommandsContext& context) override
	{
		RHIDevice* rhiDevice = RenderSystem::instance().rhiDevice;

		const ShaderParamValues* objectParams = &objectShaderParamValues;
		const ShaderParamValues* materialParams = &materialRenderData->GetShaderParamValues();

		ShaderPtr shader = materialRenderData->GetShader();

		if (cullingInputHandle == context.scene->cullingInputs.invalid_handle())
		{
			DrawCommandCullingInput cullingInput;
			cullingInput.boundingBoxCenter = worldBoundingBox.center();
			cullingInput.boundingBoxExtent = worldBoundingBox.size() * 0.5f;

			cullingInputHandle = context.scene->AddCullingInput(cullingInput);
		}

		for (uint32 scenePassIndex = 0; scenePassIndex < context.scenePassesCount; scenePassIndex++)
		{
			SceneRenderPass* scenePass = context.scenePasses[scenePassIndex];
			RenderPassCommandPtr scenePassCommandPtr = context.scenePassCommands[scenePassIndex];

			const ShaderParamValues* passParams = scenePass->GetPassParams();

			FixedString techniqueName = scenePass->GetShaderTechniqueName();
			ShaderTechnique* shaderTechnique = shader->FindTechnique(techniqueName);
			if (!shaderTechnique)
			{
				continue;
			}

			ShaderVariantKeys variantKeys;
			variantKeys.BindCollection(&shaderTechnique->variantCollection);
			for (auto it = passParams->variantKeys.begin(); it != passParams->variantKeys.end(); it++)
			{
				variantKeys.SetValue(it->first, it->second);
			}
			for (auto it = materialParams->variantKeys.begin(); it != materialParams->variantKeys.end(); it++)
			{
				variantKeys.SetValue(it->first, it->second);
			}
			for (auto it = objectParams->variantKeys.begin(); it != objectParams->variantKeys.end(); it++)
			{
				variantKeys.SetValue(it->first, it->second);
			}

			ShaderVariant* shaderVariant = shaderTechnique->FindVariant(variantKeys);
			if (!shaderVariant)
			{
				continue;
			}

			ShaderPassPtr shaderPass = shaderVariant->passes[0];
			if (!shaderPass || !shaderPass->isValid)
			{
				continue;
			}

			if (!objectParamsRHIBuffer)
			{
				BufferDesc bufferDesc(sizeof(ObjectParamsCpuBuffer), BufferBindFlag::BBF_CONSTANT_BUFFER, BufferUsage::BU_DEFAULT, CpuAccessFlag::CAF_NONE);
				objectParamsRHIBuffer = rhiDevice->CreateBuffer(bufferDesc, &objectParamsCpuBuffer);
			}

			RHIBufferPtr passParamsRHIBuffer;
			scenePass->GetOrCreatePassParamsRHIBuffer(shaderPass, rhiDevice, passParamsRHIBuffer);

			RHIBufferPtr materialParamsRHIBuffer;
			materialRenderData->GetOrCreateMaterialParamsRHIBuffer(shaderPass, rhiDevice, materialParamsRHIBuffer);

			const ShaderParamValues* shaderParamValues[(int)ShaderParamGroup::Count];
			shaderParamValues[(int)ShaderParamGroup::ObjectParams] = objectParams;
			shaderParamValues[(int)ShaderParamGroup::MaterialParams] = materialParams;
			shaderParamValues[(int)ShaderParamGroup::PassParams] = passParams;

			MeshDrawCommandPtr drawCommand(new MeshDrawCommand(scenePassCommandPtr));
			drawCommand->BuildGraphicsState(meshRenderData, shaderPass, shaderParamValues, (int)ShaderParamGroup::Count, objectParamsRHIBuffer, materialParamsRHIBuffer, passParamsRHIBuffer);

			SceneObjectRenderQueueType queueType = GetRenderQueueType();
			DrawCommandSortingKey sortingKey(queueType, 0);

			DrawCommandBatchingKey batchingKey(shaderPass->GetShaderPassID(), materialRenderData->GetMaterialRenderDataID(), meshRenderData->GetMeshRenderDataID());

			context.scene->AddDrawCommand(scenePassIndex, drawCommand, cullingInputHandle, batchingKey);
		}
	}

public:
	struct ObjectParamsCpuBuffer
	{
		float4x4 worldMatrix;
	};

	ObjectParamsCpuBuffer objectParamsCpuBuffer;
	RHIBufferPtr objectParamsRHIBuffer;

	ShaderParamValues objectShaderParamValues;

	AxisAlignedBox3<float> worldBoundingBox;
	MeshRenderDataPtr meshRenderData;
	MaterialRenderDataPtr materialRenderData;

	CullingInputHandle cullingInputHandle;
};

//////////////////////////////////////////////////////////////////////////

IMPL_CLASS_TYPE(GizmoRenderer);

GizmoRenderer::GizmoRenderer()
	: renderSceneObject(nullptr), defaultGroup(nullptr)
{
	SetAsSingltonComponent();
}

GizmoRenderer::~GizmoRenderer()
{
}

GizmoGroup* GizmoRenderer::DefaultGroup()
{
	CHECK(groups.size() > 0);
	return groups[0];
}

GizmoGroup* GizmoRenderer::AddGroup()
{
	GizmoGroup* group = new GizmoGroup();
	groups.push_back(group);
	return group;
}

void GizmoRenderer::RemoveGroup(GizmoGroup* group)
{
	for (int i = 0; i < groups.size(); i++)
	{
		if (groups[i] == group)
		{
			groups.erase(groups.begin() + i);
		}
	}
}

void GizmoRenderer::OnPreRenderUpdate()
{
	SetPendingUpdateRenderScene();  //update render scene in every frame
}

void GizmoRenderer::OnStartup()
{
	AssetManager& assetManager = g_lawnEngine.getAssetManager();

	ShaderPtr shader = assetManager.Load<Shader>(assetManager.ProjectDir() / "Shaders/Gizmo.shader");

	material = MaterialPtr(new Material());
	material->BindShader(shader);

	linesMesh = MeshPtr(new Mesh());

	sphereMesh = Mesh::CreateSphere(float3(0, 0, 0), 1.0f, 16, 16);
	MeshDataWrittablePtr sphereMeshWrittable = sphereMesh->BeginModify();
	{
		std::vector<color32> vertexColors;
		vertexColors.resize(sphereMeshWrittable->GetVertexCount(), color32(255, 255, 255, 255));

		int vertexColorVB = sphereMeshWrittable->AddVertexBuffer(sizeof(color32), vertexColors.size() * sizeof(color32), vertexColors.data());
		sphereMeshWrittable->AddVertexComponent(InputElement::IE_VERTEXCOLOR, vertexColorVB, MeshVertexFormat(MVBF_Byte, 4));
	}
	sphereMesh->EndModify();

	CHECK(defaultGroup == nullptr);
	defaultGroup = AddGroup();

	SetPreRenderUpdateEnabled(true);

	if (material && GetRenderScene())
	{
		SetPendingUpdateRenderScene();
	}
}

void GizmoRenderer::OnShutdown()
{
	SetPreRenderUpdateEnabled(false);

	RenderSceneObjectBridge::OnShutdown();
}

void GizmoRenderer::CreateRenderSceneObject()
{
	CHECK(renderSceneObject == nullptr);

	RenderScene* renderScene = GetRenderScene();

	if (material && renderScene)
	{
		renderSceneObject = new GizmoRenderSceneObject(this);

		renderScene->AddCustomPassObject(renderSceneObject);
	}
}

void GizmoRenderer::DestroyRenderSceneObject()
{
	RenderScene* renderScene = GetRenderScene();

	if (renderSceneObject != nullptr)
	{
		if (renderScene)
			renderScene->RemoveCustomPassObject(renderSceneObject);

		SAFE_DELETE(renderSceneObject);
	}
}

void GizmoRenderer::UpdateMeshes()
{
	int numLines = 0;
	int numSpheres = 0;
	int numTriangleMeshes = 0;
	for (GizmoGroup* group : groups)
	{
		numLines += group->lines.size();
		numSpheres += group->spheres.size();
		numTriangleMeshes += group->triangleMeshes.size();
	}

	MeshDataWrittablePtr linesMeshWrittable = linesMesh->BeginModifyOnEmptyData();
	{
		int lineVertexCount = numLines * 2;

		linesMeshWrittable->SetVertexCount(lineVertexCount);
		linesMeshWrittable->SetPrimitiveTopology(PT_LineList);

		std::vector<GizmoVertex> lineVerticies;
		lineVerticies.reserve(lineVertexCount);

		for (GizmoGroup* group : groups)
		{
			for (GizmoLine& line : group->lines)
			{
				lineVerticies.push_back(GizmoVertex(line.start, line.color));
				lineVerticies.push_back(GizmoVertex(line.end, line.color));
			}
		}

		int vertexBufferIndex = linesMeshWrittable->AddVertexBuffer(sizeof(GizmoVertex), lineVerticies.size() * sizeof(GizmoVertex), lineVerticies.data());

		linesMeshWrittable->AddVertexComponent(InputElement::IE_POSITION, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 3));
		linesMeshWrittable->AddVertexComponent(InputElement::IE_VERTEXCOLOR, vertexBufferIndex, MeshVertexFormat(MVBF_Byte, 4));
	}
	linesMesh->EndModify();
}

void GizmoRenderer::UpdateRenderScene()
{
	RenderScene* renderScene = GetRenderScene();
	if (!renderScene)
		return;

	if (!renderSceneObject)
	{
		renderSceneObject = new GizmoRenderSceneObject(this);
		renderScene->AddCustomPassObject(renderSceneObject);
	}

	UpdateGizmos();
}

void GizmoRenderer::UpdateGizmos()
{
	for (GizmoGroup* group : groups)
	{
		//
		// update lines
		//
		tempLines.reserve(group->lines.size());

		for (int i = 0; i < group->lines.size(); i++)
		{
			GizmoLine& line = group->lines[i];

			if (line.lifeCountdown != GIZMO_FOREVER)
			{
				line.lifeCountdown -= g_lawnEngine.get_frame_elapsed_time();
				if (line.lifeCountdown > 0)
				{
					tempLines.push_back(line);
				}
			}
			else
			{
				tempLines.push_back(line);
			}
		}

		group->lines.swap(tempLines);
		tempLines.clear();

		//
		// update spheres
		//
		tempSpheres.reserve(group->spheres.size());

		for (int i = 0; i < group->spheres.size(); i++)
		{
			GizmoSphere& sphere = group->spheres[i];

			if (sphere.lifeCountdown != GIZMO_FOREVER)
			{
				sphere.lifeCountdown -= g_lawnEngine.get_frame_elapsed_time();
				if (sphere.lifeCountdown > 0)
				{
					tempSpheres.push_back(sphere);
				}
			}
			else
			{
				tempSpheres.push_back(sphere);
			}
		}

		group->spheres.swap(tempSpheres);
		tempSpheres.clear();

		//
		// update triangle meshes
		//
		tempTriangleMeshes.reserve(group->triangleMeshes.size());

		for (int i = 0; i < group->triangleMeshes.size(); i++)
		{
			GizmoTriangleMesh& mesh = group->triangleMeshes[i];

			bool alive = false;

			if (mesh.lifeCountdown != GIZMO_FOREVER)
			{
				mesh.lifeCountdown -= g_lawnEngine.get_frame_elapsed_time();
				alive = mesh.lifeCountdown > 0;
			}
			else
			{
				alive = true;
			}

			if (alive)
			{
				tempTriangleMeshes.push_back(GizmoTriangleMesh());
				GizmoTriangleMesh& target = tempTriangleMeshes.back();
				mesh.Swap(target);
			}
		}

		group->triangleMeshes.swap(tempTriangleMeshes);
		tempTriangleMeshes.clear();
	}

	UpdateMeshes();
}

void GizmoRenderer::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddFunction("ClearDefaultGroup", &Self::ClearDefaultGroup);
}

void GizmoRenderer::ClearDefaultGroup()
{
	DefaultGroup()->Clear();
}
