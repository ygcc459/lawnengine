#pragma once

#include "SceneObject.h"
#include <vector>
#include "ComponentRegistry.h"
#include "RegistryList.h"

class RenderSceneObjectBridge;
class RenderScene;
class Camera;
class WorldQuery;

class World : public BaseObject
{
	DECL_CLASS_TYPE(World, BaseObject);

public:
	World();
	virtual ~World();

	virtual void Reflect(Reflector& reflector) override;

	virtual void OnDestroy() override;

	virtual void Tick();

	virtual void PreRenderUpdate();

	virtual void EditorDrawGizmo();

	template<class T = SceneObject>
	T* CreateSceneObject()
	{
		Type* type = &T::StaticType;
		return (T*)CreateSceneObject(type);
	}

	SceneObject* CreateSceneObject(Type* type, bool startup = true)
	{
		if (!type->IsSameOrChildOf(&SceneObject::StaticType))
		{
			Log::Error("CreateSceneObject: type must SceneObject or its sub-class, not %s", type->TypeName().c_str());
			return nullptr;
		}

		SceneObject* p = (SceneObject*)type->CreateInstance();
		CreateSceneObjectInternal(p, startup);
		return p;
	}

	void DestroySceneObject(SceneObject* so);

	void CollectSceneObjectsDepthFirst(SceneObject* so, std::vector<SceneObject*>& outSceneObjects);

	void DestroyAllSceneObjects();

	std::vector<SceneObject*>& GetSceneObjects() { return sceneObjects; }

	ComponentRegistry& GetComponentRegistry() { return componentRegistry; }

	RenderScene* GetRenderScene() { return renderScene; }

	RegistryList<RenderSceneObjectBridge*>& PendingUpdateRenderSceneRenderers() { return pendingUpdateRenderSceneRenderers; }
	
	void UpdateRenderScene();

	void TriggerComponentUpdates();

	template<class T>
	T* GetSingletonComponent(bool autoCreate = true)
	{
		Component* comp = GetSingletonComponent(&T::StaticType, autoCreate);
		T* ptr = Util::CastPtrWithCheck<T>(comp);
		return ptr;
	}

	Component* GetSingletonComponent(Type* type, bool autoCreate = true);

	void OnSingletonComponentDestroy(Type* type);

	template<class T>
	void GetComponents(std::vector<T*>& outComponents)
	{
		for (SceneObject* obj : sceneObjects)
		{
			obj->GetComponentsInChildren<T>(outComponents);
		}
	}

	void GetComponents(Type* type, std::vector<Component*>& outComponents);

	WorldQuery& GetQuery() { return *worldQuery; }

	void PrintHierarchy();

	Event<std::function<void(SceneObject*)>> CreateSceneObjectEvent;
	Event<std::function<void(SceneObject*)>> DestroySceneObjectEvent;

protected:
	void CreateSceneObjectInternal(SceneObject* p, bool startup = true);

	void DestroySceneObjectInternal(SceneObject* so);
	
	Component* CreateSingletonComponentInternal(Type* type);

	void PrintHierarchy(SceneObject* so, const std::string& indent);

protected:
	std::vector<SceneObject*> sceneObjects;

	ComponentRegistry componentRegistry;

	std::vector<Component*> updatingComponents;  //temporary buffer reused every frame

	std::map<Type*, Component*> managerComponents;

	RegistryList<RenderSceneObjectBridge*> pendingUpdateRenderSceneRenderers;

	RenderScene* renderScene;

	WorldQuery* worldQuery;
};
