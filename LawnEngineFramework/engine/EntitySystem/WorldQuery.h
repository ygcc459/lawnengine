#pragma once

class MeshRenderer;

struct RayQueryResult
{
	float distance;
	float3 triangleBarycentric;

	float3 intersectPoint;
	MeshRenderer* meshRenderer;
};

class World;

class WorldQuery
{
public:
	World& world;

public:
	WorldQuery(World& world);
	virtual ~WorldQuery();

	void Raycast(const float3& rayDirection, const float3& rayOrigin, float rayLength, std::vector<RayQueryResult>& outResults);
	bool RaycastFirst(const float3& rayDirection, const float3& rayOrigin, float rayLength, RayQueryResult& outResult);

};
