#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "EntitySystemInternal.h"
#include "MeshRenderer.h"
#include "EntitySystemInternal.h"
#include "RenderSystem/RenderScene.h"
#include "RenderSystem/RenderCommands.h"
#include "RenderSystem/SceneRenderPass.h"
#include "EntitySystem/GizmoRenderer.h"
#include "InGameEditor.h"

class MeshRendererSceneObject : public RenderSceneObject
{
public:
	MeshRendererSceneObject(const float4x4& worldMatrix, const AxisAlignedBox3<float>& worldBounds, MaterialRenderDataPtr materialRenderData, MeshRenderDataPtr meshRenderData)
		: RenderSceneObject()
		, materialRenderData(materialRenderData)
		, worldBoundingBox(worldBounds)
		, meshRenderData(meshRenderData)
		, cullingInputHandle(-1)
	{
		objectParamsCpuBuffer.worldMatrix = worldMatrix;
	}

	virtual ~MeshRendererSceneObject()
	{}

	virtual void FillProperties(RenderSceneObjectProperties& prop) override
	{
		prop.isDynamic = false;
	}

	SceneObjectRenderQueueType GetRenderQueueType() const
	{
		if (materialRenderData->GetBlendMode() == BlendMode::BlendMode_Opaque)
		{
			return SceneObjectRenderQueueType::Opaque;
		}
		else if (materialRenderData->GetBlendMode() == BlendMode::BlendMode_Opaque)
		{
			return SceneObjectRenderQueueType::Opaque;
		}
		else
		{
			assert(false);
			return SceneObjectRenderQueueType::Opaque;
		}
	}

	virtual void PrepareDrawCommands(PrepareDrawCommandsContext& context) override
	{
		RHIDevice* rhiDevice = RenderSystem::instance().rhiDevice;

		const ShaderParamValues* objectParams = &objectShaderParamValues;
		const ShaderParamValues* materialParams = &materialRenderData->GetShaderParamValues();

		ShaderPtr shader = materialRenderData->GetShader();

		if (cullingInputHandle == context.scene->cullingInputs.invalid_handle())
		{
			DrawCommandCullingInput cullingInput;
			cullingInput.boundingBoxCenter = worldBoundingBox.center();
			cullingInput.boundingBoxExtent = worldBoundingBox.size() * 0.5f;

			cullingInputHandle = context.scene->AddCullingInput(cullingInput);
		}

		for (uint32 scenePassIndex = 0; scenePassIndex < context.scenePassesCount; scenePassIndex++)
		{
			SceneRenderPass* scenePass = context.scenePasses[scenePassIndex];
			RenderPassCommandPtr scenePassCommandPtr = context.scenePassCommands[scenePassIndex];

			const ShaderParamValues* passParams = scenePass->GetPassParams();

			FixedString techniqueName = scenePass->GetShaderTechniqueName();
			ShaderTechnique* shaderTechnique = shader->FindTechnique(techniqueName);
			if (!shaderTechnique)
			{
				continue;
			}

			ShaderVariantKeys variantKeys;
			variantKeys.BindCollection(&shaderTechnique->variantCollection);
			for (auto it = passParams->variantKeys.begin(); it != passParams->variantKeys.end(); it++)
			{
				variantKeys.SetValue(it->first, it->second);
			}
			for (auto it = materialParams->variantKeys.begin(); it != materialParams->variantKeys.end(); it++)
			{
				variantKeys.SetValue(it->first, it->second);
			}
			for (auto it = objectParams->variantKeys.begin(); it != objectParams->variantKeys.end(); it++)
			{
				variantKeys.SetValue(it->first, it->second);
			}

			ShaderVariant* shaderVariant = shaderTechnique->FindVariant(variantKeys);
			if (!shaderVariant)
			{
				continue;
			}

			ShaderPassPtr shaderPass = shaderVariant->passes[0];
			if (!shaderPass || !shaderPass->isValid)
			{
				continue;
			}

			if (!objectParamsRHIBuffer)
			{
				BufferDesc bufferDesc(sizeof(ObjectParamsCpuBuffer), BufferBindFlag::BBF_CONSTANT_BUFFER, BufferUsage::BU_DEFAULT, CpuAccessFlag::CAF_NONE);
				objectParamsRHIBuffer = rhiDevice->CreateBuffer(bufferDesc, &objectParamsCpuBuffer);
			}

			RHIBufferPtr passParamsRHIBuffer;
			scenePass->GetOrCreatePassParamsRHIBuffer(shaderPass, rhiDevice, passParamsRHIBuffer);

			RHIBufferPtr materialParamsRHIBuffer;
			materialRenderData->GetOrCreateMaterialParamsRHIBuffer(shaderPass, rhiDevice, materialParamsRHIBuffer);

			const ShaderParamValues* shaderParamValues[(int)ShaderParamGroup::Count];
			shaderParamValues[(int)ShaderParamGroup::ObjectParams] = objectParams;
			shaderParamValues[(int)ShaderParamGroup::MaterialParams] = materialParams;
			shaderParamValues[(int)ShaderParamGroup::PassParams] = passParams;

			MeshDrawCommandPtr drawCommand(new MeshDrawCommand(scenePassCommandPtr));
			drawCommand->BuildGraphicsState(meshRenderData, shaderPass, shaderParamValues, (int)ShaderParamGroup::Count, objectParamsRHIBuffer, materialParamsRHIBuffer, passParamsRHIBuffer);

			SceneObjectRenderQueueType queueType = GetRenderQueueType();
			DrawCommandSortingKey sortingKey(queueType, 0);

			DrawCommandBatchingKey batchingKey(shaderPass->GetShaderPassID(), materialRenderData->GetMaterialRenderDataID(), meshRenderData->GetMeshRenderDataID());

			context.scene->AddDrawCommand(scenePassIndex, drawCommand, cullingInputHandle, batchingKey);
		}
	}

public:
	struct ObjectParamsCpuBuffer
	{
		float4x4 worldMatrix;
	};
	
	ObjectParamsCpuBuffer objectParamsCpuBuffer;
	RHIBufferPtr objectParamsRHIBuffer;

	ShaderParamValues objectShaderParamValues;

	AxisAlignedBox3<float> worldBoundingBox;
	MeshRenderDataPtr meshRenderData;
	MaterialRenderDataPtr materialRenderData;

	CullingInputHandle cullingInputHandle;
};

IMPL_CLASS_TYPE(MeshRenderer);

MeshRenderer::MeshRenderer()
	: renderSceneObject(nullptr)
{
}

MeshRenderer::~MeshRenderer()
{
}

void MeshRenderer::OnStartup()
{
	Super::OnStartup();

	RegisterToWorld(CRT_EditorPickable);
	RegisterToWorld(CRT_EditorDrawGizmo);

	if (mesh && material && GetRenderScene())
	{
		SetPendingUpdateRenderScene();
	}
}

void MeshRenderer::OnShutdown()
{
	Super::OnShutdown();

	this->material = nullptr;
	this->mesh = nullptr;
}

void MeshRenderer::SetMaterial(MaterialPtr material)
{
	if (this->material != material)
	{
		this->material = material;
		SetPendingUpdateRenderScene();
	}
}

MaterialPtr MeshRenderer::GetMaterial()
{
	return material;
}

void MeshRenderer::SetMesh(MeshPtr mesh)
{
	if (this->mesh != mesh)
	{
		this->mesh = mesh;
		SetPendingUpdateRenderScene();
	}
}

AxisAlignedBox3<float> MeshRenderer::GetWorldBoundingBox() const
{
	if (!mesh)
		return AxisAlignedBox3<float>(AxisAlignedBox3<float>::Empty);

	AxisAlignedBox3<float> localBounds = mesh->GetBoundingBox();

	float4x4 localToWorld = GetSceneObject().GetTransform().WorldMatrix();

	float3 localCorners[8] = 
	{
		float3(localBounds.xmin, localBounds.ymin, localBounds.zmin),
		float3(localBounds.xmin, localBounds.ymin, localBounds.zmax),
		float3(localBounds.xmin, localBounds.ymax, localBounds.zmin),
		float3(localBounds.xmin, localBounds.ymax, localBounds.zmax),
		float3(localBounds.xmax, localBounds.ymin, localBounds.zmin),
		float3(localBounds.xmax, localBounds.ymin, localBounds.zmax),
		float3(localBounds.xmax, localBounds.ymax, localBounds.zmin),
		float3(localBounds.xmax, localBounds.ymax, localBounds.zmax),
	};

	AxisAlignedBox3<float> worldBounds;
	for (int i = 0; i < 8; i++)
	{
		float3 worldCorner = mul(float4(localCorners[i], 1), localToWorld).xyz();
		worldBounds.unionWith(worldCorner);
	}

	return worldBounds;
}

void MeshRenderer::CreateRenderSceneObject()
{
	CHECK(renderSceneObject == nullptr);

	RenderScene* renderScene = GetRenderScene();

	if (mesh && material && renderScene)
	{
		MeshRenderDataPtr meshRenderData = mesh->GetMeshRenderData();
		MaterialRenderDataPtr materialData = material->GetMaterialRenderData();

		renderSceneObject = new MeshRendererSceneObject(GetTransform().WorldMatrix(), GetWorldBoundingBox(), materialData, meshRenderData);

		renderScene->AddObject(renderSceneObject);
	}
}

void MeshRenderer::DestroyRenderSceneObject()
{
	RenderScene* renderScene = GetRenderScene();

	if (renderSceneObject != nullptr)
	{
		if (renderScene)
			renderScene->RemoveObject(renderSceneObject);

		SAFE_DELETE(renderSceneObject);
	}
}

void MeshRenderer::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("material", &Self::material).AddAttribute(new AssetSelectorAttribute());
	reflector.AddMember("mesh", &Self::mesh).AddAttribute(new AssetSelectorAttribute());
}

float MeshRenderer::OnEditorPick(const EditorPickInput& input)
{
	MeshPtr mesh = GetMesh();
	if (mesh)
	{
		Transform& transform = GetTransform();

		float4x4 localToWorld = transform.WorldMatrix();
		float4x4 worldToLocal = transform.InverseWorldMatrix();

		float3 localRayDir = normalize(mul(float4(input.rayDirection, 0), worldToLocal).xyz());
		float3 localRayOrigin = mul(float4(input.rayOrigin, 1), worldToLocal).xyz();

		std::vector<Mesh::RaycastResult> results;
		if (mesh->Raycast(localRayDir, localRayOrigin, std::numeric_limits<float>::max(), results))
		{
			float3 localHitVec = results[0].distance * localRayDir;
			float3 worldHitVec = mul(float4(localHitVec, 0), localToWorld).xyz();
			float worldDistance = length(worldHitVec);

			return worldDistance;
		}
	}

	return -1;
}

void MeshRenderer::OnEditorDrawGizmo()
{
	if (GetWorld())
	{
		GizmoRenderer* gizmoRenderer = GetWorld()->GetSingletonComponent<GizmoRenderer>();

		AxisAlignedBox3<float> aabb = GetWorldBoundingBox();

		bool selected = 
			InGameEditor::GetInstance().GetSelectedObject() == this
			|| InGameEditor::GetInstance().GetSelectedObject() == &this->GetSceneObject();

		if (selected)
		{
			gizmoRenderer->DefaultGroup()->AddBoxWireframeForEditorSelectedComponent(aabb, selected);
		}
	}
}
