#pragma once

class SceneObject;
class World;
enum ComponentRegistryType;
class RenderScene;
class Transform;
struct HierarchyChangedEvent;

struct EditorPickInput
{
	float3 rayOrigin;
	float3 rayDirection;
};

class Component : public BaseObject
{
	DECL_CLASS_TYPE(Component, BaseObject);

protected:
	SceneObject* sceneObject;
	bool started;
	bool singleton;

public:
	Component();
	virtual ~Component();
	
	void Startup();
	void Shutdown();

	virtual void OnDestroy() override;

	virtual void Reflect(Reflector& reflector) override;
	virtual void AfterDeserialization() override;
	
	std::string GetWorldPath();

	void SetSceneObject(SceneObject* sceneObject);
	SceneObject& GetSceneObject() const { return *sceneObject; }
	Transform& GetTransform();
	World* GetWorld();
	RenderScene* GetRenderScene();

	virtual void OnUpdate() {}
	void SetUpdateEnabled(bool b);
	bool IsUpdateEnabled();

	virtual void OnPreRenderUpdate() {}
	void SetPreRenderUpdateEnabled(bool b);
	bool IsPreRenderUpdateEnabled();

	void RegisterToWorld(ComponentRegistryType type);
	void UnregisterFromWorld(ComponentRegistryType type);

	virtual void OnParentHierarchyChanged(const HierarchyChangedEvent& ev) {}
	virtual void OnChildHierarchyChanged(const HierarchyChangedEvent& ev) {}

	void SetAsSingltonComponent();
	void CheckSingltonComponent();

	// only enabled when registered as CRT_EditorPickable
	// return distance to hit point, a positive value means this component can be picked
	virtual float OnEditorPick(const EditorPickInput& input) { return -1.0f; }

	virtual void OnEditorDrawGizmo() {}

protected:
	virtual void OnStartup();
	virtual void OnShutdown();

};
