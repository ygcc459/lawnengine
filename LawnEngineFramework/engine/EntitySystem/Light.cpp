#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "EntitySystemInternal.h"
#include "Light.h"

#include "Transform.h"
#include "RenderSystem/RenderScene.h"

//////////////////////////////////////////////////////////////////////////

IMPL_ABSTRACT_CLASS_TYPE(Light);

Light::Light(LightType type)
	: type(type)
	, color(1, 1, 1)
	, intensity(1.0f)
	, generateShadow(false)
	, renderSceneLight(nullptr)
{

}

Light::~Light()
{

}

void Light::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("type", &Self::type);
	reflector.AddMember("color", &Self::color);

	reflector.AddMember("intensity", &Self::intensity)
		.AddAttribute(new FloatRangeAttribute(0.0f, 10.0f));

	reflector.AddMember("generateShadow", &Self::generateShadow);
}

void Light::OnPropertyChangedByReflection(ObjectMember& member)
{
	if (GetRenderScene())
	{
		SetPendingUpdateRenderScene();
	}
}

void Light::OnStartup()
{
	if (GetRenderScene())
	{
		SetPendingUpdateRenderScene();
	}
}

void Light::OnShutdown()
{
	RenderSceneObjectBridge::OnShutdown();
}

void Light::CreateRenderSceneObject()
{
	CHECK(renderSceneLight == nullptr);

	RenderScene* renderScene = GetRenderScene();

	renderSceneLight = new RenderSceneLight();
	renderSceneLight->type = this->type;
	renderSceneLight->color = this->color;
	renderSceneLight->intensity = this->intensity;
	renderSceneLight->generateShadow = this->generateShadow;
	FillRenderSceneLight(renderSceneLight);

	renderScene->AddLight(renderSceneLight);
}

void Light::DestroyRenderSceneObject()
{
	RenderScene* renderScene = GetRenderScene();

	if (renderSceneLight != nullptr)
	{
		if (renderScene)
			renderScene->RemoveLight(renderSceneLight);

		SAFE_DELETE(renderSceneLight);
	}
}

//////////////////////////////////////////////////////////////////////////

IMPL_CLASS_TYPE(DirectionalLight);

DirectionalLight::DirectionalLight()
	: Light(LightType::LT_DirectionalLight)
{

}

DirectionalLight::~DirectionalLight()
{
	
}

void DirectionalLight::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);
}

void DirectionalLight::FillRenderSceneLight(RenderSceneLight* light)
{
	light->direction = GetTransform().WorldForward();
}

//////////////////////////////////////////////////////////////////////////

IMPL_CLASS_TYPE(PointLight);

PointLight::PointLight()
	: Light(LightType::LT_PointLight)
{

}

PointLight::~PointLight()
{
	
}

void PointLight::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);
}

void PointLight::FillRenderSceneLight(RenderSceneLight* light)
{
	light->position = GetTransform().WorldPosition();
}

//////////////////////////////////////////////////////////////////////////

IMPL_CLASS_TYPE(SpotLight);

SpotLight::SpotLight()
	: Light(LightType::LT_SpotLight), innerConeAngle(math::degreeToRadian(30.0f)), outerConeAngle(math::degreeToRadian(60.0f)), falloff(1.0f)
{

}

SpotLight::~SpotLight()
{
	
}

void SpotLight::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("innerConeAngle", &Self::innerConeAngle);
	reflector.AddMember("outerConeAngle", &Self::outerConeAngle);
	reflector.AddMember("falloff", &Self::falloff);
}

void SpotLight::FillRenderSceneLight(RenderSceneLight* light)
{
	light->position = GetTransform().WorldPosition();
	light->direction = GetTransform().WorldForward();

	light->innerConeAngle = this->innerConeAngle;
	light->outerConeAngle = this->outerConeAngle;
	light->falloff = this->falloff;
}
