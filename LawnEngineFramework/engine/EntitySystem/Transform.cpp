#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "Transform.h"
#include "SceneObject.h"

IMPL_CLASS_TYPE(Transform);

Transform::Transform()
	: parent(nullptr),
	localPosition(0, 0, 0),
	localRotation(),
	localScale(1, 1, 1),
	transformChanged(true),
	transformChangedEvent(this)
{
}

Transform::~Transform()
{
	Shutdown();
}

void Transform::SetParent(Transform* newparent, bool sendEvents /*= true*/)
{
	if (newparent != this->parent)
	{
		Transform* oldparent = this->parent;

		if (oldparent)
		{
			for (int i = 0; i < oldparent->children.size(); i++)
			{
				if (oldparent->children[i] == this)
				{
					oldparent->children.erase(oldparent->children.begin() + i);
					break;
				}
			}

			if (sendEvents)
			{
				HierarchyChangedEvent detachEvent(HierarchyChangedEvent::TransformDetached, oldparent, this);

				this->GetSceneObject().TriggerParentHierarchyChangedEventRecursively(detachEvent);

				oldparent->GetSceneObject().TriggerChildHierarchyChangedEventRecursively(detachEvent);
			}
		}

		this->parent = newparent;

		if (newparent)
		{
			newparent->children.push_back(this);

			if (sendEvents)
			{
				HierarchyChangedEvent attachEvent(HierarchyChangedEvent::TransformAttached, newparent, this);

				this->GetSceneObject().TriggerParentHierarchyChangedEventRecursively(attachEvent);

				newparent->GetSceneObject().TriggerChildHierarchyChangedEventRecursively(attachEvent);
			}
		}

		MarkTransformChanged();
	}
}

void Transform::InverseTransformPosition(float3& v)
{
	if (parent)
	{
		parent->InverseTransformPosition(v);

		v = mul(quaternion_inverse(parent->localRotation), v - parent->localPosition) * inverse_scale(parent->localScale);
	}
	else
	{
		v = mul(quaternion_inverse(quaternion_identity()), v - float3(0, 0, 0)) * inverse_scale(float3(1, 1, 1));
	}
}

void Transform::SetWorldPosition(const float3& v)
{
	float3 tempv = v;
	InverseTransformPosition(tempv);

	SetLocalPosition(tempv);
}

void Transform::InverseTransformRotation(quaternionf& q)
{
	if (parent)
	{
		parent->InverseTransformRotation(q);

		q = mul(quaternion_inverse(parent->localRotation), q);
		q = quaternion_mul_scale(q, parent->localScale.x, parent->localScale.y, parent->localScale.z);
	}
	else
	{
		q = mul(quaternion_inverse(quaternion_identity()), q);
		q = quaternion_mul_scale(q, 1.0f, 1.0f, 1.0f);
	}
}

void Transform::SetWorldRotation(const quaternionf& q)
{
	quaternionf tempq = q;
	InverseTransformRotation(tempq);

	SetLocalRotation(tempq);
}

void Transform::MarkTransformChanged()
{
	transformChanged = true;

	transformChangedEvent.Trigger(this);
}

float3 Transform::WorldPosition()
{
	UpdateTransformIfChanged();
	return float3(worldMatrix._41, worldMatrix._42, worldMatrix._43);
}

quaternionf Transform::WorldRotation()
{
	quaternionf rotation = this->LocalRotation();
	
	Transform* cur = this->parent;

	while (cur != nullptr)
	{
		rotation = quaternion_mul_scale(rotation, cur->localScale.x, cur->localScale.y, cur->localScale.z);
		rotation = mul(cur->localRotation, rotation);

		cur = cur->parent;
	}

	return rotation;
}

void Transform::UpdateTransformIfChanged()
{
	if (transformChanged)
	{
		transformChanged = false;

		float4x4 translation = matrix_translation(localPosition);
		float4x4 rotation = quaternion_to_matrix4x4(localRotation);
		float4x4 scaling = matrix_scaling(localScale.x, localScale.y, localScale.z);

		if (parent)
		{
			float4x4 parentWorldMatrix = parent->WorldMatrix();
			this->worldMatrix = mul(mul(mul(parentWorldMatrix, scaling), rotation), translation);
		}
		else
		{
			this->worldMatrix = mul(mul(scaling, rotation), translation);
		}

		this->inverseWorldMatrix = inverse(this->worldMatrix);
	}
}

void Transform::SetWorldForward(const float3& v, const float3& up /*= float3(0, 1, 0)*/)
{
	float3x3 rotMat = matrix3x3_look(v, up);
	quaternionf q = quaternion_from_matrix3x3(rotMat);
	SetWorldRotation(q);
}

void Transform::SetLookAt(const float3& pos, const float3& up /*= float3(0, 1, 0)*/)
{
	SetWorldForward(pos - WorldPosition(), up);
}

void Transform::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("children", &Self::children);
	reflector.AddMember("parent", &Self::parent);

	reflector.AddMember("localPosition", &Self::localPosition);
	reflector.AddMember("localRotation", &Self::localRotation);
	reflector.AddMember("localScale", &Self::localScale);
}

void Transform::OnPropertyChangedByReflection(ObjectMember& member)
{
	MarkTransformChanged();
}
