#include "../fast_compile.h"
#include "LawnEngineInternal.h"
#include "EntitySystemInternal.h"
#include "FreeCameraController.h"
#include "InputSystem/InputManager.h"
#include "InGameEditor.h"
#include "Renderer.h"

//////////////////////////////////////////////////////////////////////////

IMPL_CLASS_TYPE(FreeCameraController);

FreeCameraController::FreeCameraController()
	: mode(Mode::None)
{
	panVector = float3(0, 0, 0);

	panSpeed = 4.0f;
	rotateSpeed = 0.65f;
	zoomSpeed = 0.030f;
	localPanCameraSpeed = 0.1f;

	orbitCenter = float3(0, 0, 0);
	orbitRadius = 0;
}

FreeCameraController::~FreeCameraController()
{
}

void FreeCameraController::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("panSpeed", &Self::panSpeed);
	reflector.AddMember("rotateSpeed", &Self::rotateSpeed);
}

void FreeCameraController::OnStartup()
{
	SetUpdateEnabled(true);

	g_lawnEngine.getInputManager().addKeyEventListener(this);
	g_lawnEngine.getInputManager().addPointerEventListener(this);
}

void FreeCameraController::OnShutdown()
{
	SetUpdateEnabled(false);

	g_lawnEngine.getInputManager().removeKeyEventListener(this);
	g_lawnEngine.getInputManager().removePointerEventListener(this);
}

void FreeCameraController::OnUpdate()
{
	Transform& transform = GetTransform();

	if (mode == Mode::None || mode == Mode::FpsCamera)
	{
		float3 moveVector = normalize(panVector.x * transform.WorldRight() + panVector.y * transform.WorldUp() + transform.WorldForward() * panVector.z);

		transform.SetWorldPosition(transform.WorldPosition() + moveVector * panSpeed * g_lawnEngine.get_frame_elapsed_time());
	}
}

void FreeCameraController::FocusToSelectedObject()
{
	BaseObject* selectedObj = InGameEditor::GetInstance().GetSelectedObject();
	if (selectedObj == nullptr)
		return;

	SceneObject* baseSceneObj = nullptr;
	if (selectedObj->GetType()->IsSameOrChildOf(GetStaticType<Component>()))
	{
		Component* component = Util::CastPtr<Component>(selectedObj);
		baseSceneObj = &component->GetSceneObject();
	}
	else if (selectedObj->GetType()->IsSameOrChildOf(GetStaticType<SceneObject>()))
	{
		SceneObject* sceneObj = Util::CastPtr<SceneObject>(selectedObj);
		baseSceneObj = sceneObj;
	}

	if (baseSceneObj)
	{
		std::vector<Renderer*> allRenderers;
		baseSceneObj->GetComponentsInChildren<Renderer>(allRenderers, true);

		if (allRenderers.size() > 0)
		{
			AxisAlignedBox3<float> totalBounds;
			for (auto* renderer : allRenderers)
			{
				auto aabb = renderer->GetWorldBoundingBox();
				totalBounds.unionWith(aabb);
			}

			orbitCenter = totalBounds.center();

			float3 halfSize = totalBounds.size() * 0.5f;
			orbitRadius = math::max(0.0f, length(halfSize)) * 2.0f;

			Transform& transform = GetTransform();
			transform.SetWorldPosition(orbitCenter - orbitRadius * transform.WorldForward());
		}
		else
		{
			orbitCenter = baseSceneObj->GetTransform().WorldPosition();

			Transform& transform = GetTransform();
			transform.SetLookAt(orbitCenter);
		}
	}
}

void FreeCameraController::SaveStartDragCameraTransform()
{
	startDragCameraPosition = GetTransform().WorldPosition();
	startDragCameraForward = GetTransform().WorldForward();
	startDragCameraRight = GetTransform().WorldRight();
	startDragCameraUp = GetTransform().WorldUp();
	startDragCameraRotation = GetTransform().WorldRotation();
}

//////////////////////////////////////////////////////////////////////////

float FreeCameraController::GetPointerEventPriority()
{
	return -1;
}

bool FreeCameraController::OnPointerClick(PointerState& pointer)
{
	if (pointer.pointerId == PointerId::MouseButtonLeft)
	{
		if (!g_lawnEngine.getInputManager().hasKeyFocus(this))
		{
			g_lawnEngine.getInputManager().setKeyFocus(this, nullptr);
			return true;
		}
	}

	return false;
}

bool FreeCameraController::OnPointerBeginDrag(PointerState& pointer)
{
	bool altDown = g_lawnEngine.getInputManager().isKeyDown(KeyCode::KC_LMENU) || g_lawnEngine.getInputManager().isKeyDown(KeyCode::KC_RMENU);

	if (pointer.pointerId == PointerId::MouseButtonLeft && altDown)
	{
		mode = Mode::OrbitCamera;
		SaveStartDragCameraTransform();
		return true;
	}
	else if (pointer.pointerId == PointerId::MouseButtonLeft && !altDown)
	{
		mode = Mode::LocalMoveRotateCamera;
		SaveStartDragCameraTransform();
		return true;
	}
	else if (pointer.pointerId == PointerId::MouseButtonRight && altDown)
	{
		mode = Mode::Zoom;
		SaveStartDragCameraTransform();
		return true;
	}
	else if (pointer.pointerId == PointerId::MouseButtonRight && !altDown)
	{
		mode = Mode::FpsCamera;
		SaveStartDragCameraTransform();
		return true;
	}
	else if (pointer.pointerId == PointerId::MouseButtonMiddle && altDown)
	{
	}
	else if (pointer.pointerId == PointerId::MouseButtonMiddle && !altDown)
	{
		mode = Mode::LocalPanCamera;
		SaveStartDragCameraTransform();
		return true;
	}

	return false;
}

bool FreeCameraController::OnPointerDragging(PointerState& pointer)
{
	if (mode == Mode::FpsCamera)
	{
		float2 rotation = (pointer.curPosition - pointer.startPosition) * rotateSpeed;

		Transform& transform = GetTransform();

		quaternionf q = startDragCameraRotation;
		q = mul(quaternion_from_axis_angle(float3(0, 1, 0), math::degreeToRadian(rotation.x)), q);
		q = mul(quaternion_from_axis_angle(transform.WorldRight(), math::degreeToRadian(rotation.y)), q);

		transform.SetWorldRotation(q);

		return true;
	}
	else if (mode == Mode::OrbitCamera)
	{
		float2 rotation = (pointer.curPosition - pointer.startPosition) * rotateSpeed;

		Transform& transform = GetTransform();

		float3 vec = startDragCameraPosition - orbitCenter;

		geovec3<float> geovec = direction_to_geovec_yup(vec);
		geovec.latitude += math::degreeToRadian(rotation.y);
		geovec.longitude -= math::degreeToRadian(rotation.x);

		geovec.latitude = math::clamp(geovec.latitude, latitude_min_radians<float>() + 0.001f, latitude_max_radians<float>() - 0.001f);

		float3 newvec = geovec_to_direction_yup(geovec);

		float3 newpos = orbitCenter + newvec;
		transform.SetWorldPosition(newpos);
		transform.SetLookAt(orbitCenter);

		return true;
	}
	else if (mode == Mode::Zoom)
	{
		float zoomFactor = -(pointer.curPosition.y - pointer.startPosition.y) * zoomSpeed;

		float3 newpos = startDragCameraPosition + zoomFactor * normalize(startDragCameraForward);

		Transform& transform = GetTransform();
		transform.SetWorldPosition(newpos);

		return true;
	}
	else if (mode == Mode::LocalPanCamera)
	{
		float2 delta = (pointer.curPosition - pointer.startPosition) * localPanCameraSpeed;

		float3 newpos = startDragCameraPosition + delta.x * normalize(startDragCameraRight) - delta.y * float3(0, 1, 0);

		Transform& transform = GetTransform();
		transform.SetWorldPosition(newpos);

		return true;
	}
	else if (mode == Mode::LocalMoveRotateCamera)
	{
		float deltapos = -(pointer.curDeltaPosition.y) * localPanCameraSpeed;
		float3 pandir = project_to_plane(startDragCameraForward, float3(0, 1, 0));
		float3 newpos = startDragCameraPosition + deltapos * normalize(pandir);

		float deltarot = (pointer.curDeltaPosition.x) * rotateSpeed;

		quaternionf newrot = mul(quaternion_from_axis_angle(float3(0, 1, 0), math::degreeToRadian(deltarot)), startDragCameraRotation);

		Transform& transform = GetTransform();
		transform.SetWorldPosition(newpos);
		transform.SetWorldRotation(newrot);

		SaveStartDragCameraTransform();

		return true;
	}

	return false;
}

bool FreeCameraController::OnPointerEndDrag(PointerState& pointer)
{
	if (mode == Mode::FpsCamera)
	{
		mode = Mode::None;
		return true;
	}
	else if (mode == Mode::OrbitCamera)
	{
		mode = Mode::None;
		return true;
	}
	else if (mode == Mode::Zoom)
	{
		mode = Mode::None;
		return true;
	}
	else if (mode == Mode::LocalPanCamera)
	{
		mode = Mode::None;
		return true;
	}
	else if (mode == Mode::LocalMoveRotateCamera)
	{
		mode = Mode::None;
		return true;
	}

	return false;
}

void FreeCameraController::OnKeyEvent(const KeyEvent& ev, void* customData)
{
	if (ev.type == KET_KeyPressed)
	{
		switch (ev.keycode)
		{
		case KC_1:
			panSpeed *= 1 / 2.0f;
			break;
		case KC_2:
			panSpeed *= 2.0f;
			break;
		case KC_3:
			rotateSpeed *= 1 / 2.0f;
			break;
		case KC_4:
			rotateSpeed *= 2.0f;
			break;
		case KC_W:
			panVector.z = 1;
			break;
		case KC_S:
			panVector.z = -1;
			break;
		case KC_A:
			panVector.x = -1;
			break;
		case KC_D:
			panVector.x = 1;
			break;
		case KC_E:
			panVector.y = 1;
			break;
		case KC_Q:
			panVector.y = -1;
			break;
		case KC_F:
			FocusToSelectedObject();
			break;
		}
	}
	else if (ev.type == KET_KeyReleased)
	{
		switch (ev.keycode)
		{
		case KC_W:
			panVector.z = 0;
			break;
		case KC_S:
			panVector.z = 0;
			break;
		case KC_A:
			panVector.x = 0;
			break;
		case KC_D:
			panVector.x = 0;
			break;
		case KC_E:
			panVector.y = 0;
			break;
		case KC_Q:
			panVector.y = 0;
			break;
		}
	}
	else if (ev.type == KET_LostFocus)
	{
		panVector = float3(0, 0, 0);
	}
}
