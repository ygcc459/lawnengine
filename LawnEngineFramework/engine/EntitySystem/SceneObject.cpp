#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "EntitySystemInternal.h"
#include "SceneObject.h"

IMPL_CLASS_TYPE(SceneObject);

SceneObject::SceneObject(World* world)
	: name("no-name-object"),
	world(world),
	started(false),
	transform(nullptr)
{
}

SceneObject::~SceneObject()
{
	Shutdown(true);
}

void SceneObject::Startup(bool recursive)
{
	if (!started)
	{
		started = true;

		if (transform == nullptr)
		{
			transform = AddComponent<Transform>();
		}

		for (auto c : components)
		{
			c->Startup();
		}

		if (recursive)
		{
			for (auto childTransform : transform->children)
			{
				SceneObject& child = childTransform->GetSceneObject();
				child.Startup(recursive);
			}
		}
	}
}

void SceneObject::Shutdown(bool recursive)
{
	if (started)
	{
		started = false;
		
		if (recursive)
		{
			for (auto childTransform : transform->children)
			{
				SceneObject& child = childTransform->GetSceneObject();
				child.Shutdown(recursive);
			}
		}

		for (auto comp : components)
		{
			comp->Shutdown();
		}
	}
}

std::string SceneObject::GetWorldPath()
{
	std::string path = formatString("%s", this->name.c_str());
	SceneObject* parent = GetParent();
	while (parent)
	{
		path = parent->name + "/" + path;

		parent = parent->GetParent();
	}

	return path;
}

SceneObject* SceneObject::AddChild(Type* type, const std::string& name /*= "SceneObject"*/)
{
	if (GetWorld())
	{
		SceneObject* sceneObject = GetWorld()->CreateSceneObject(type);
		sceneObject->name = name;
		sceneObject->SetParent(this);
		return sceneObject;
	}
	else
	{
		return nullptr;
	}
}

void SceneObject::SetParent(SceneObject* sceneObject)
{
	CHECK(transform != nullptr);

	if (sceneObject)
	{
		CHECK(sceneObject->transform != nullptr);

		transform->SetParent(&sceneObject->GetTransform());
	}
	else
	{
		transform->SetParent(nullptr);
	}
}

SceneObject* SceneObject::GetParent()
{
	CHECK(transform != nullptr);

	Transform* parentTransform = this->transform->parent;
	if (parentTransform)
		return &parentTransform->GetSceneObject();
	else
		return nullptr;
}

bool SceneObject::IsParentOf(SceneObject* other) const
{
	CHECK(transform != nullptr);

	Transform* p = other->transform->parent;
	while (p != nullptr)
	{
		if (p == this->transform)
			return true;

		p = p->parent;
	}

	return false;
}

size_t SceneObject::GetChildCount() const
{
	CHECK(transform != nullptr);

	return this->transform->children.size();
}

SceneObject* SceneObject::GetChild(size_t index)
{
	CHECK(transform != nullptr);

	Transform* p = this->transform->children[index];
	return (p ? &p->GetSceneObject() : nullptr);
}

void SceneObject::DestroyAllComponentsInternal()
{
	for (int32 i = (int32)components.size() - 1; i >= 0; i--)
	{
		DestroyComponent(components[i]);
	}
}

void SceneObject::DestroyComponent(Component* c)
{
	for (auto i = 0; i < components.size(); i++)
	{
		if (components[i] == c)
		{
			c->Shutdown();

			components.erase(components.begin() + i);

			delete c;
		}
	}
}

Component* SceneObject::GetComponent(Type* type)
{
	for (int i = 0; i < components.size(); i++)
	{
		Component* casted = (Component*)Util::CastPtr(components[i], type);
		if (casted)
		{
			return casted;
		}
	}

	return nullptr;
}

Component* SceneObject::GetComponentInParents(Type* type, bool includeSelf /*= true*/)
{
	SceneObject* so = includeSelf ? this : this->GetParent();

	while (so != nullptr)
	{
		Component* c = so->GetComponent(type);

		if (c != nullptr)
			return c;

		so = so->GetParent();
	}

	return nullptr;
}

Component* SceneObject::GetComponentInChildren(Type* type, bool includeSelf /*= true*/)
{
	if (includeSelf)
	{
		for (int i = 0; i < components.size(); i++)
		{
			Component* casted = (Component*)Util::CastPtr(components[i], type);
			if (casted)
			{
				return casted;
			}
		}
	}

	for (int i = 0; i < transform->children.size(); i++)
	{
		SceneObject& child = transform->children[i]->GetSceneObject();
		Component* c = child.GetComponentInChildren(type, true);
		if (c)
		{
			return c;
		}
	}

	return nullptr;
}

void SceneObject::GetComponents(Type* type, std::vector<Component*>& outComponents)
{
	for (int i = 0; i < components.size(); i++)
	{
		Component* casted = (Component*)Util::CastPtr(components[i], type);
		if (casted)
		{
			outComponents.push_back(casted);
		}
	}
}

void SceneObject::GetComponentsInChildren(Type* type, std::vector<Component*>& outComponents, bool includeSelf /*= true*/)
{
	if (includeSelf)
	{
		for (int i = 0; i < components.size(); i++)
		{
			Component* casted = (Component*)Util::CastPtr(components[i], type);
			if (casted)
			{
				outComponents.push_back(casted);
			}
		}
	}

	for (int i = 0; i < transform->children.size(); i++)
	{
		SceneObject& child = transform->children[i]->GetSceneObject();
		child.GetComponentsInChildren(type, outComponents, true);
	}
}

SceneObjectPtr SceneObject::CreateTemplate(SceneObject* srcObj)
{
	Archive archive;
	archive.CreateNew();

	Serializer serializer(archive.Root());
	serializer.canSerializeReflectiveObjectCallback = [=](ReflectiveObject* obj)
	{
		Type* type = obj->GetType();
		if (type->IsSameOrChildOf(&SceneObject::StaticType))
		{
			SceneObject* sceneObj = (SceneObject*)obj;

			if (sceneObj->GetWorld() == srcObj->GetWorld())
			{
				if (sceneObj == srcObj || srcObj->IsParentOf(sceneObj))
					return true;
			}

			return false;
		}
		else if (type->IsSameOrChildOf(&Component::StaticType))
		{
			Component* comp = (Component*)obj;
			SceneObject* sceneObj = &comp->GetSceneObject();

			if (sceneObj->GetWorld() == srcObj->GetWorld())
			{
				if (sceneObj == srcObj || srcObj->IsParentOf(sceneObj))
					return true;
			}

			return false;
		}
		else if (type->IsSameOrChildOf(&World::StaticType))
		{
			return false;
		}
		else
		{
			return true;
		}
	};
	serializer.SerializeRootObject(srcObj, ObjectProperty<BaseObject>());

	SceneObjectPtr dstObj(new SceneObject());

	Deserializer deserializer(archive.Root());
	deserializer.DeserializeRootObject(dstObj.get(), ObjectProperty<ReflectiveObject>());
	deserializer.InvokeAfterDeserialization();

	return dstObj;
}

SceneObject* SceneObject::Instanciate(SceneObjectPtr templateObj, World& world)
{
	if (!templateObj)
		return nullptr;

	Archive archive;
	archive.CreateNew();

	Serializer serializer(archive.Root());
	serializer.SerializeRootObject(templateObj.get(), ObjectProperty<BaseObject>());

	Type* sceneObjectType = templateObj->GetType();
	
	//don't startup during clone process
	SceneObject* dstObj = world.CreateSceneObject(sceneObjectType, false);

	Deserializer deserializer(archive.Root());
	deserializer.createReflectiveObjectCallback = [&](const BaseProperty* prop, Type* serializedType)
	{
		if (serializedType->IsSameOrChildOf(GetStaticType<SceneObject>()))
		{
			//don't startup during clone process
			return static_cast<ReflectiveObject*>(world.CreateSceneObject(serializedType, false));
		}
		else
		{
			return static_cast<ReflectiveObject*>(serializedType->CreateInstance());
		}
	};
	deserializer.DeserializeRootObject(dstObj, ObjectProperty<BaseObject>());

	deserializer.InvokeAfterDeserialization();

	dstObj->Startup(true);  //startup recursively

	return dstObj;
}

void SceneObject::TriggerChildHierarchyChangedEventRecursively(const HierarchyChangedEvent& ev)
{
	for (int i = 0; i < components.size(); i++)
	{
		Component* c = components[i];
		if (c)
		{
			c->OnChildHierarchyChanged(ev);
		}
	}

	SceneObject* parent = GetParent();
	if (parent)
	{
		parent->TriggerChildHierarchyChangedEventRecursively(ev);
	}
}

void SceneObject::TriggerParentHierarchyChangedEventRecursively(const HierarchyChangedEvent& ev)
{
	for (int i = 0; i < components.size(); i++)
	{
		Component* c = components[i];
		if (c)
		{
			c->OnParentHierarchyChanged(ev);
		}
	}

	for (int i = 0; i < transform->children.size(); i++)
	{
		SceneObject& child = transform->children[i]->GetSceneObject();
		child.TriggerParentHierarchyChangedEventRecursively(ev);
	}
}

Component* SceneObject::AddComponent(Type* type)
{
	ReflectiveObject* p = (ReflectiveObject*)type->CreateInstance();

	if (!p->GetType()->IsChildOf(&Component::StaticType))
	{
		Log::Error("AddComponent only accepts sub-classes of Component");
		return nullptr;
	}

	Component* comp = (Component*)p;

	AddComponentInternal(comp);
	return comp;
}

void SceneObject::AddComponentInternal(Component* c)
{
	c->SetSceneObject(this);

	this->components.push_back(c);

	if (this->started)
	{
		c->Startup();
	}
}

void SceneObject::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("world", &Self::world)
		.AddAttribute(new SkipSerializationAttribute());

	reflector.AddMember("name", &Self::name);
	reflector.AddMember("components", &Self::components);
	reflector.AddMember("transform", &Self::transform);
}

void SceneObject::AfterDeserialization()
{
}
