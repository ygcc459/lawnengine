#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "EntitySystemInternal.h"
#include "Camera.h"

#include "SceneObject.h"
#include "Transform.h"

#include "RenderSystem/RenderView.h"
#include "RenderSystem/RenderScene.h"

IMPL_CLASS_TYPE(Camera);

Camera::Camera()
	: Super()
	, renderSceneCamera(nullptr)
	, fov(45.0f)
	, znear(0.01f)
	, zfar(1000.0f)
	, relativeViewport(0.0f, 0.0f, 1.0f, 1.0f)
	, actualViewport(0, 0, 1, 1)
{
}

Camera::~Camera()
{
}

void Camera::OnStartup()
{
	Super::OnStartup();

	RegisterToWorld(ComponentRegistryType::CRT_Cameras);

	if (GetRenderScene())
	{
		SetPendingUpdateRenderScene();
	}
}

void Camera::OnShutdown()
{
	UnregisterFromWorld(ComponentRegistryType::CRT_Cameras);

	SetRenderTarget(nullptr);

	Super::OnShutdown();
}

void Camera::CreateRenderSceneObject()
{
	CHECK(renderSceneCamera == nullptr);

	RenderScene* renderScene = GetRenderScene();
	if (renderScene)
	{
		renderSceneCamera = new RenderSceneCamera();

		renderScene->AddCamera(renderSceneCamera);
	}
}

void Camera::DestroyRenderSceneObject()
{
	RenderScene* renderScene = GetRenderScene();
	if (renderSceneCamera != nullptr)
	{
		if (renderScene)
			renderScene->RemoveCamera(renderSceneCamera);

		SAFE_DELETE(renderSceneCamera);
	}
}

void Camera::UpdateRenderScene()
{
	RenderScene* renderScene = GetRenderScene();
	if (renderScene)
	{
		if (!renderSceneCamera)
			CreateRenderSceneObject();

		Transform& transform = GetTransform();

		renderSceneCamera->position = transform.WorldPosition();
		renderSceneCamera->direction = transform.WorldForward();
		renderSceneCamera->up = transform.WorldUp();
		renderSceneCamera->fov = this->fov;
		renderSceneCamera->znear = this->znear;
		renderSceneCamera->zfar = this->zfar;
		renderSceneCamera->viewport = this->actualViewport;
		renderSceneCamera->viewMatrix = GetViewMatrix();
		renderSceneCamera->projectionMatrix = GetProjectionMatrix();
		renderSceneCamera->viewProjectionMatrix = mul(renderSceneCamera->viewMatrix, renderSceneCamera->projectionMatrix);
	}
}

void Camera::Render()
{
	SetPendingUpdateRenderScene();  //update every frame

	RenderScene* renderScene = GetRenderScene();
	if (renderScene && this->targetRenderView && renderSceneCamera)
	{
		renderScene->Render(renderSceneCamera, targetRenderView);
	}
}

void Camera::SetRenderTarget(RenderViewPtr targetRenderView)
{
	if (this->targetRenderView)
	{
		this->targetRenderView->RemoveCameraInternal(this);
	}

	this->targetRenderView = targetRenderView;

	if (this->targetRenderView)
	{
		this->targetRenderView->AddCameraInternal(this);

		UpdateViewport(targetRenderView->Width(), targetRenderView->Height());
	}
}

void Camera::OnRenderTargetResized(uint w, uint h)
{
	UpdateViewport(w, h);
}

void Camera::UpdateViewport(uint w, uint h)
{
	vcpp::la::rect<uint> newViewport;
	newViewport.x = vcpp::math::round_uint(w * relativeViewport.x);
	newViewport.y = vcpp::math::round_uint(h * relativeViewport.y);
	newViewport.w = vcpp::math::round_uint(w * relativeViewport.w);
	newViewport.h = vcpp::math::round_uint(h * relativeViewport.h);

	actualViewport = newViewport;
}

void Camera::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("fov", &Self::fov);
	reflector.AddMember("znear", &Self::znear);
	reflector.AddMember("zfar", &Self::zfar);

	reflector.AddMember("relativeViewport", &Self::relativeViewport);
}

float4x4 Camera::GetViewMatrix()
{
	Transform& transform = GetTransform();
	return matrix_look(transform.WorldPosition(), transform.WorldForward(), transform.WorldUp());
}

float4x4 Camera::GetProjectionMatrix()
{
	return matrix_perspective_fov_y(fov, GetAspectRatio(), znear, zfar);
}

float3 Camera::ViewportPointToRayDir(const float2& viewportPoint)
{
	float px = 0.0f;
	float py = 0.0f;
	float pz = 1.0f;

	const vcpp::la::rect<uint>& vp = actualViewport;

	float4x4 proj = GetProjectionMatrix();
	px = viewportPoint.x / proj(0, 0);
	py = viewportPoint.y / proj(1, 1);

	float4x4 invView = GetViewMatrix();
	invView = inverse(invView);

	float3 ray;
	ray.x = px * invView._11 + py * invView._21 + pz * invView._31;
	ray.y = px * invView._12 + py * invView._22 + pz * invView._32;
	ray.z = px * invView._13 + py * invView._23 + pz * invView._33;

	ray = normalize(ray);

	return ray;
}

vcpp::la::float2 Camera::ScreenPointToViewport(const float2& screenPoint)
{
	const vcpp::la::rect<uint>& vp = actualViewport;

	return float2(
		((2.0f * screenPoint.x) / vp.w) - 1.0f,
		((-2.0f * screenPoint.y) / vp.h) + 1.0f);
}
