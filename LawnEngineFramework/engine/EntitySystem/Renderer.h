#pragma once

#include "RenderSceneObjectBridge.h"

class Renderer : public RenderSceneObjectBridge
{
	DECL_ABSTRACT_CLASS_TYPE(Renderer, RenderSceneObjectBridge);
	
public:
	Renderer();
	virtual ~Renderer();

	virtual AxisAlignedBox3<float> GetWorldBoundingBox() const = 0;

};
