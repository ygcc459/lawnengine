#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "Component.h"

#include "World.h"
#include "SceneObject.h"

IMPL_CLASS_TYPE(Component);

Component::Component()
	:sceneObject(nullptr), started(false), singleton(false)
{

}

Component::~Component()
{
}

void Component::Startup()
{
	if (!started)
	{
		CheckSingltonComponent();

		OnStartup();
		started = true;
	}
}

void Component::Shutdown()
{
	if (started)
	{
		OnShutdown();
		started = false;
	}
}

void Component::OnDestroy()
{
	if (singleton && GetWorld())
	{
		GetWorld()->OnSingletonComponentDestroy(this->GetType());
	}

	Shutdown();
}

void Component::AfterDeserialization()
{
}

std::string Component::GetWorldPath()
{
	std::string soPath = GetSceneObject().GetWorldPath();
	return formatString("%s::%s (%p)", soPath.c_str(), GetType()->TypeName().c_str(), this);
}

void Component::SetSceneObject(SceneObject* sceneObject)
{
	this->sceneObject = sceneObject;
}

Transform& Component::GetTransform()
{
	return sceneObject->GetTransform();
}

World* Component::GetWorld()
{
	return sceneObject ? sceneObject->GetWorld() : nullptr;
}

RenderScene* Component::GetRenderScene()
{
	if (sceneObject)
	{
		World* world = sceneObject->GetWorld();
		if (world)
		{
			return world->GetRenderScene();
		}
	}

	return nullptr;
}

void Component::SetUpdateEnabled(bool b)
{
	if (b)
	{
		RegisterToWorld(ComponentRegistryType::CRT_Update);
	}
	else
	{
		UnregisterFromWorld(ComponentRegistryType::CRT_Update);
	}
}

bool Component::IsUpdateEnabled()
{
	World* world = GetWorld();
	if (world)
	{
		return world->GetComponentRegistry().IsRegistered(ComponentRegistryType::CRT_Update, this);
	}
	else
	{
		return false;
	}
}

void Component::SetPreRenderUpdateEnabled(bool b)
{
	if (b)
	{
		RegisterToWorld(ComponentRegistryType::CRT_PreRenderUpdate);
	}
	else
	{
		UnregisterFromWorld(ComponentRegistryType::CRT_PreRenderUpdate);
	}
}

bool Component::IsPreRenderUpdateEnabled()
{
	World* world = GetWorld();
	if (world)
	{
		return world->GetComponentRegistry().IsRegistered(ComponentRegistryType::CRT_PreRenderUpdate, this);
	}
	else
	{
		return false;
	}
}

void Component::RegisterToWorld(ComponentRegistryType type)
{
	World* world = GetWorld();
	if (world)
	{
		world->GetComponentRegistry().Register(type, this);
	}
}

void Component::UnregisterFromWorld(ComponentRegistryType type)
{
	World* world = GetWorld();
	if (world)
	{
		world->GetComponentRegistry().Unregister(type, this);
	}
}

void Component::SetAsSingltonComponent()
{
	this->singleton = true;
}

void Component::CheckSingltonComponent()
{
	if (this->singleton)
	{
		if (GetWorld() == nullptr)
		{
			Log::Error("'%s' is a singleton component, and must put in a world", this->GetType()->TypeName().c_str());
			return;
		}

		Component* existedInstance = GetWorld()->GetSingletonComponent<Component>(false);
		if (existedInstance && existedInstance != this)
		{
			Log::Error("'%s' is a singleton component, but its instance already exists in this world.", this->GetType()->TypeName().c_str());
			return;
		}
	}
}

void Component::OnStartup()
{
}

void Component::OnShutdown()
{
	World* world = GetWorld();
	if (world)
	{
		world->GetComponentRegistry().Unregister(this);
	}
}

void Component::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("sceneObject", &Self::sceneObject)
		.AddAttribute(new HideInInspectorAttribute());
}
