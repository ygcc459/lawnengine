#pragma once

#include "Component.h"
#include "InputSystem/InputManager.h"

class FreeCameraController : public Component, public KeyEventListener, public PointerEventListener
{
	DECL_CLASS_TYPE(FreeCameraController, Component);

public:
	FreeCameraController();
	virtual ~FreeCameraController();

	virtual void OnStartup();
	virtual void OnShutdown();

	virtual void Reflect(Reflector& reflector) override;

	virtual void OnUpdate() override;

	virtual void OnKeyEvent(const KeyEvent& ev, void* customData) override;

	virtual float GetPointerEventPriority() override;
	virtual bool OnPointerClick(PointerState& pointer) override;
	virtual bool OnPointerBeginDrag(PointerState& pointer) override;
	virtual bool OnPointerDragging(PointerState& pointer) override;
	virtual bool OnPointerEndDrag(PointerState& pointer) override;

	void FocusToSelectedObject();

	void SaveStartDragCameraTransform();

private:
	enum Mode
	{
		None,
		FpsCamera,
		OrbitCamera,
		Zoom,
		LocalPanCamera,
		LocalMoveRotateCamera,
	};

	Mode mode;

	float3 tmpMovingPoint;

	float panSpeed;
	float rotateSpeed;
	float zoomSpeed;
	float localPanCameraSpeed;

	float orbitRadius;
	float3 orbitCenter;

	float3 startDragCameraPosition;
	float3 startDragCameraForward;
	float3 startDragCameraRight;
	float3 startDragCameraUp;
	quaternionf startDragCameraRotation;

protected:
	float3 panVector;

};

