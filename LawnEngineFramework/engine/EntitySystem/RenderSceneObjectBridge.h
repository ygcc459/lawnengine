#pragma once

#include "Component.h"
#include "../RegistryList.h"

class Material;
class Mesh;
class RenderSceneObject;

class RenderSceneObjectBridge : public Component
{
	DECL_ABSTRACT_CLASS_TYPE(RenderSceneObjectBridge, Component);
	
public:
	RenderSceneObjectBridge();
	virtual ~RenderSceneObjectBridge();

	virtual void OnStartup() override;
	virtual void OnShutdown() override;

	virtual void Reflect(Reflector& reflector) override;

	virtual void OnPropertyChangedByReflection(ObjectMember& member) override;

	void SetPendingUpdateRenderScene();

	virtual void CreateRenderSceneObject() = 0;
	virtual void DestroyRenderSceneObject() = 0;
	virtual void UpdateRenderScene();

private:
	RegistryList<RenderSceneObjectBridge*>::Handle pendingUpdateRenderSceneHandle;
};
