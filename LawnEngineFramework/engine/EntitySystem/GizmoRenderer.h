
#include "RenderSceneObjectBridge.h"

#include "RenderSystem/Mesh.h"
#include "RenderSystem/Material.h"

class GizmoRendererSceneObject;

#define GIZMO_ONE_FRAME (0)
#define GIZMO_FOREVER (-1)

struct GizmoLine
{
	float3 start;
	float3 end;
	color32 color;
	float lifeCountdown;

	GizmoLine()
		: lifeCountdown(GIZMO_ONE_FRAME)
	{}

	GizmoLine(const float3& start, const float3& end, color32 color, float lifeCountdown)
		: start(start), end(end), color(color), lifeCountdown(lifeCountdown)
	{}
};

struct GizmoSphere
{
	float3 center;
	float radius;
	color32 color;
	float lifeCountdown;

	GizmoSphere()
		: lifeCountdown(GIZMO_ONE_FRAME)
	{}

	GizmoSphere(const float3& center, float radius, color32 color, float lifeCountdown)
		: center(center), radius(radius), color(color), lifeCountdown(lifeCountdown)
	{}
};

struct GizmoTriangleMesh
{
	std::vector<float3> positions;
	std::vector<color32> colors;
	float lifeCountdown;

	GizmoTriangleMesh()
		: lifeCountdown(GIZMO_ONE_FRAME)
	{}

	GizmoTriangleMesh(const std::vector<float3>& positions, const std::vector<color32>& colors, float lifeCountdown)
		: positions(positions), colors(colors), lifeCountdown(lifeCountdown)
	{}
	
	GizmoTriangleMesh(float lifeCountdown)
		: lifeCountdown(lifeCountdown)
	{}

	void Clear()
	{
		positions.clear();
		colors.clear();
	}

	void Reserve(uint32 numTriangles)
	{
		positions.reserve(numTriangles * 3);
		colors.reserve(numTriangles * 3);
	}

	void AddTriangle(const float3& pos0, const float3& pos1, const float3& pos2, color32 color)
	{
		positions.push_back(pos0);
		positions.push_back(pos1);
		positions.push_back(pos2);
		colors.push_back(color);
		colors.push_back(color);
		colors.push_back(color);
	}

	void Swap(GizmoTriangleMesh& other)
	{
		positions.swap(other.positions);
		colors.swap(other.colors);
		std::swap(lifeCountdown, other.lifeCountdown);
	}
};

struct GizmoGroup
{
	std::vector<GizmoLine> lines;
	std::vector<GizmoSphere> spheres;
	std::vector<GizmoTriangleMesh> triangleMeshes;

	GizmoLine& AddLine(const float3& start, const float3& end, color32 color, float lifeCountdown = GIZMO_FOREVER)
	{
		lines.push_back(GizmoLine(start, end, color, lifeCountdown));
		return lines.back();
	}

	void AddCrossMarkLine(const float3& center, float size, color32 color, float lifeCountdown = GIZMO_FOREVER)
	{
		lines.push_back(GizmoLine(center - float3(-size, 0, 0), center - float3(size, 0, 0), color, lifeCountdown));
		lines.push_back(GizmoLine(center - float3(0, -size, 0), center - float3(0, size, 0), color, lifeCountdown));
		lines.push_back(GizmoLine(center - float3(0, 0, -size), center - float3(0, 0, size), color, lifeCountdown));
	}

	void AddBoxWireframeForEditorSelectedComponent(const AxisAlignedBox3<float>& aabb, bool selected)
	{
		color32 color = selected ? color32(255, 0, 0, 255) : color32(255, 255, 255, 255);
		AddBoxWireframe(aabb.minPt, aabb.maxPt, color, GIZMO_ONE_FRAME);
	}

	void AddBoxWireframe(const AxisAlignedBox3<float>& aabb, color32 color, float lifeCountdown = GIZMO_FOREVER)
	{
		AddBoxWireframe(aabb.minPt, aabb.maxPt, color, lifeCountdown);
	}

	void AddBoxWireframe(const float3& minpoint, const float3& maxpoint, color32 color, float lifeCountdown = GIZMO_FOREVER)
	{
		float3 corners[] =
		{
			float3(minpoint.x, minpoint.y, minpoint.z),
			float3(minpoint.x, minpoint.y, maxpoint.z),
			float3(minpoint.x, maxpoint.y, minpoint.z),
			float3(minpoint.x, maxpoint.y, maxpoint.z),
			float3(maxpoint.x, minpoint.y, minpoint.z),
			float3(maxpoint.x, minpoint.y, maxpoint.z),
			float3(maxpoint.x, maxpoint.y, minpoint.z),
			float3(maxpoint.x, maxpoint.y, maxpoint.z),
		};

		lines.reserve(lines.capacity() + 12);

		lines.push_back(GizmoLine(corners[0], corners[2], color, lifeCountdown));
		lines.push_back(GizmoLine(corners[2], corners[6], color, lifeCountdown));
		lines.push_back(GizmoLine(corners[6], corners[4], color, lifeCountdown));
		lines.push_back(GizmoLine(corners[4], corners[0], color, lifeCountdown));

		lines.push_back(GizmoLine(corners[1], corners[3], color, lifeCountdown));
		lines.push_back(GizmoLine(corners[3], corners[7], color, lifeCountdown));
		lines.push_back(GizmoLine(corners[7], corners[5], color, lifeCountdown));
		lines.push_back(GizmoLine(corners[5], corners[1], color, lifeCountdown));

		lines.push_back(GizmoLine(corners[0], corners[1], color, lifeCountdown));
		lines.push_back(GizmoLine(corners[2], corners[3], color, lifeCountdown));
		lines.push_back(GizmoLine(corners[4], corners[5], color, lifeCountdown));
		lines.push_back(GizmoLine(corners[6], corners[7], color, lifeCountdown));
	}

	void AddBox(const float3& minpoint, const float3& maxpoint, color32 color, float lifeCountdown = GIZMO_FOREVER)
	{

	}

	GizmoSphere& AddSphere(const float3& center, float radius, color32 color, float lifeCountdown = GIZMO_FOREVER)
	{
		spheres.push_back(GizmoSphere(center, radius, color, lifeCountdown));
		return spheres.back();
	}

	void Clear()
	{
		lines.clear();
		spheres.clear();
		triangleMeshes.clear();
	}
};

class GizmoRenderer : public Renderer
{
	DECL_CLASS_TYPE(GizmoRenderer, RenderSceneObjectBridge);

	friend class GizmoRenderSceneObject;
	
public:
	GizmoRenderer();
	virtual ~GizmoRenderer();

	virtual void OnStartup() override;
	virtual void OnShutdown() override;

	virtual void Reflect(Reflector& reflector) override;

	GizmoGroup* DefaultGroup();

	GizmoGroup* AddGroup();
	void RemoveGroup(GizmoGroup* group);

	virtual void OnPreRenderUpdate() override;

	void ClearDefaultGroup();

protected:
	virtual void CreateRenderSceneObject() override;
	virtual void DestroyRenderSceneObject() override;

	void UpdateMeshes();

protected:
	MaterialPtr material;

	MeshPtr linesMesh;
	MeshPtr sphereMesh;

	GizmoGroup* defaultGroup;
	std::vector<GizmoGroup*> groups;

	std::vector<GizmoLine> tempLines;
	std::vector<GizmoSphere> tempSpheres;
	std::vector<GizmoTriangleMesh> tempTriangleMeshes;

	GizmoRendererSceneObject* renderSceneObject;
};
