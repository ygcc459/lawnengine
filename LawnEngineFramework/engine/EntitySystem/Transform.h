#pragma once

#include "Component.h"

class Transform : public Component
{
	DECL_CLASS_TYPE(Transform, Component);

public:
	std::vector<Transform*> children;
	Transform* parent;

	float3 localPosition;
	quaternionf localRotation;
	float3 localScale;

	bool transformChanged;
	float4x4 worldMatrix;
	float4x4 inverseWorldMatrix;

	Event<std::function<void(Transform*)>> transformChangedEvent;

public:
	Transform();
	virtual ~Transform();

	virtual void Reflect(Reflector& reflector) override;

	virtual void OnPropertyChangedByReflection(ObjectMember& member) override;

	Transform* Parent() { return parent; }
	void SetParent(Transform* newparent, bool sendEvents = true);

	float3 LocalPosition() const { return localPosition; }
	quaternionf LocalRotation() const { return localRotation; }
	float3 LocalScale() const { return localScale; }

	void SetLocalPosition(const float3& v)
	{
		localPosition = v;
		MarkTransformChanged();
	}

	void SetLocalRotation(const quaternionf& v)
	{
		localRotation = v;
		MarkTransformChanged();
	}

	void SetLocalScale(const float3& v)
	{
		localScale = v;
		MarkTransformChanged();
	}

	void SetWorldPosition(const float3& v);

	void SetWorldRotation(const quaternionf& q);

	void MarkTransformChanged();

	float3 WorldPosition();
	quaternionf WorldRotation();

	float4x4 WorldMatrix()
	{
		UpdateTransformIfChanged();
		return worldMatrix;
	}

	float4x4 InverseWorldMatrix()
	{
		UpdateTransformIfChanged();
		return inverseWorldMatrix;
	}

	void UpdateTransformIfChanged();

	float3 WorldForward()
	{
		return mul(float4(0.0f, 0.0f, 1.0f, 0.0f), WorldMatrix()).xyz();
	}

	float3 WorldBackward()
	{
		return mul(float4(0.0f, 0.0f, -1.0f, 0.0f), WorldMatrix()).xyz();
	}

	float3 WorldUp()
	{
		return mul(float4(0.0f, 1.0f, 0.0f, 0.0f), WorldMatrix()).xyz();
	}

	float3 WorldDown()
	{
		return mul(float4(0.0f, -1.0f, 0.0f, 0.0f), WorldMatrix()).xyz();
	}

	float3 WorldLeft()
	{
		return mul(float4(-1.0f, 0.0f, 0.0f, 0.0f), WorldMatrix()).xyz();
	}

	float3 WorldRight()
	{
		return mul(float4(1.0f, 0.0f, 0.0f, 0.0f), WorldMatrix()).xyz();
	}

	void SetWorldForward(const float3& v, const float3& up = float3(0, 1, 0));
	void SetLookAt(const float3& pos, const float3& up = float3(0, 1, 0));

private:
	void InverseTransformPosition(float3& q);
	void InverseTransformRotation(quaternionf& q);
};
