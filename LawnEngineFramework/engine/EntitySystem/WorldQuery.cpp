#include "../fast_compile.h"
#include "LawnEngineInternal.h"
#include "WorldQuery.h"
#include "MeshRenderer.h"
#include "World.h"
#include "Transform.h"

WorldQuery::WorldQuery(World& world)
	:world(world)
{

}

WorldQuery::~WorldQuery()
{

}

void WorldQuery::Raycast(const float3& rayDirection, const float3& rayOrigin, float rayLength, std::vector<RayQueryResult>& outResults)
{
	std::vector<MeshRenderer*> meshRenderers;
	world.GetComponents<MeshRenderer>(meshRenderers);

	std::vector<Mesh::RaycastResult> meshLocalRaycastResults;

	for (MeshRenderer* mr : meshRenderers)
	{
		MeshPtr mesh = mr->GetMesh();
		if (mesh)
		{
			Transform& transform = mr->GetSceneObject().GetTransform();
			float4x4 localToWorld = transform.WorldMatrix();
			float4x4 worldToLocal = transform.InverseWorldMatrix();

			float3 localRayDir = mul(worldToLocal, float4(rayDirection, 0)).xyz();
			float3 localRayOrigin = mul(worldToLocal, float4(rayOrigin, 1)).xyz();

			meshLocalRaycastResults.clear();
			if (mesh->Raycast(localRayDir, localRayOrigin, rayLength, meshLocalRaycastResults))
			{
				for (int i = 0; i < meshLocalRaycastResults.size(); i++)
				{
					auto& localRaycastResult = meshLocalRaycastResults[i];

					float3 localHitVec = localRaycastResult.distance * localRayDir;
					float3 worldHitVec = mul(float4(localHitVec, 0), localToWorld).xyz();

					RayQueryResult result;
					result.distance = length(worldHitVec);
					result.intersectPoint = rayOrigin + worldHitVec;
					result.meshRenderer = mr;
					result.triangleBarycentric = localRaycastResult.triangleBarycentric;
					outResults.push_back(result);
				}
			}
		}
	}

	std::sort(outResults.begin(), outResults.end(), [](const RayQueryResult& a, const RayQueryResult& b)
	{
		return a.distance < b.distance;
	});
}

bool WorldQuery::RaycastFirst(const float3& rayDirection, const float3& rayOrigin, float rayLength, RayQueryResult& outResult)
{
	std::vector<RayQueryResult> results;
	Raycast(rayDirection, rayOrigin, rayLength, results);
	if (results.size() != 0)
	{
		outResult = results[0];
		return true;
	}
	else
	{
		return false;
	}
}
