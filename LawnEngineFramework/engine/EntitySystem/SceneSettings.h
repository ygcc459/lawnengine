#pragma once

#include "Component.h"
#include "RenderSystem/RenderSystemCommon.h"
#include "EntitySystem/RenderSceneObjectBridge.h"
#include "RenderSystem/Texture2D.h"
#include <vcpp/la/rect.h>

class RenderSceneSettings;

class SceneSettings : public RenderSceneObjectBridge
{
	DECL_CLASS_TYPE(SceneSettings, RenderSceneObjectBridge);

	RenderSceneSettings* renderSettings;
	
public:
	SceneSettings();
	virtual ~SceneSettings();

	virtual void OnStartup();
	virtual void OnShutdown();

	virtual void CreateRenderSceneObject() override;
	virtual void DestroyRenderSceneObject() override;
	virtual void UpdateRenderScene() override;

	virtual void Reflect(Reflector& reflector) override;
};
