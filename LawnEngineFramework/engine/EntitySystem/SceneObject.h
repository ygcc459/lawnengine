#pragma once

#include "Component.h"

class World;
class Transform;

class SceneObject;
typedef std::shared_ptr<SceneObject> SceneObjectPtr;

struct HierarchyChangedEvent
{
public:
	enum Action
	{
		TransformDetached,
		TransformAttached,
	};

	Action action;

	Transform* sourceTransform;

	Transform* movingTransform;

public:
	HierarchyChangedEvent()
		: sourceTransform(nullptr), movingTransform(nullptr)
	{}

	HierarchyChangedEvent(Action action, Transform* sourceTransform, Transform* movingTransform)
		: action(action), sourceTransform(sourceTransform), movingTransform(movingTransform)
	{}
};

class SceneObject : public BaseObject
{
	DECL_CLASS_TYPE(SceneObject, BaseObject);

public:
	World* world;
	std::string name;

	std::vector<Component*> components;
	bool started;
	
	Transform* transform;

public:
	SceneObject(World* world = nullptr);
	virtual ~SceneObject();

	void Startup(bool recursive);
	void Shutdown(bool recursive);

	virtual void Reflect(Reflector& reflector) override;

	void SetParent(SceneObject* sceneObject);
	SceneObject* GetParent();
	bool IsParentOf(SceneObject* other) const;

	std::string GetWorldPath();

	Transform& GetTransform() { CHECK(transform != nullptr); return *transform; }

	size_t GetChildCount() const;
	SceneObject* GetChild(size_t index);

	virtual void AfterDeserialization() override;

	World* GetWorld()
	{
		return world;
	}

	template<class T = SceneObject>
	T* AddChild(const std::string& name = "SceneObject")
	{
		Type* type = &T::StaticType;
		return (T*)AddChild(type, name);
	}

	SceneObject* AddChild(Type* type, const std::string& name = "SceneObject");

	template<class T>
	T* AddComponent()
	{
		Component* c = AddComponent(&T::StaticType);
		return (T*)c;
	}

	Component* AddComponent(Type* type);

	void DestroyAllComponentsInternal();
	void DestroyComponent(Component* c);

	template<class T>
	T* GetComponent()
	{
		return (T*)GetComponent(&T::StaticType);
	}

	template<class T>
	T* GetComponentInParents(bool includeSelf = true)
	{
		return (T*)GetComponentInParents(&T::StaticType, includeSelf);
	}

	template<class T>
	T* GetComponentInChildren()
	{
		return (T*)GetComponentInChildren(&T::StaticType);
	}

	template<class T>
	void GetComponentsInChildren(std::vector<T*>& outComponents, bool includeSelf = true)
	{
		if (includeSelf)
		{
			for (size_t i = 0; i < components.size(); i++)
			{
				T* casted = Util::CastPtr<T>(components[i]);
				if (casted)
				{
					outComponents.push_back(casted);
				}
			}
		}

		for (size_t i = 0; i < GetChildCount(); i++)
		{
			SceneObject* child = GetChild(i);
			child->GetComponentsInChildren<T>(outComponents, true);
		}
	}

	Component* GetComponent(Type* type);
	Component* GetComponentInParents(Type* type, bool includeSelf = true);
	Component* GetComponentInChildren(Type* type, bool includeSelf = true);
	std::vector<Component*>& GetComponents() { return components; }
	void GetComponents(Type* type, std::vector<Component*>& outComponents);
	
	template<class T>
	void GetComponentsOfType(std::vector<T*>& outComponents)
	{
		for (int i = 0; i < components.size(); i++)
		{
			T* casted = Util::CastPtr<T, Component>(components[i]);
			if (casted)
			{
				outComponents.push_back(casted);
			}
		}
	}

	void GetComponentsInChildren(Type* type, std::vector<Component*>& outComponents, bool includeSelf = true);

	static SceneObjectPtr CreateTemplate(SceneObject* srcObj);
	static SceneObject* Instanciate(SceneObjectPtr templateObj, World& world);

	// propagate up to parent recursively
	void TriggerChildHierarchyChangedEventRecursively(const HierarchyChangedEvent& ev);

	// propagate down to children recursively
	void TriggerParentHierarchyChangedEventRecursively(const HierarchyChangedEvent& ev);

private:
	void AddComponentInternal(Component* c);
};
