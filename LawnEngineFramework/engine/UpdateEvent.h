#pragma once


class Updatable;


class UpdateEventSource
{
public:
	UpdateEventSource()
	{}

	virtual ~UpdateEventSource()
	{}

	void triggerUpdateEvent();

	bool hasUpdateEventListener(Updatable* listener) const {
		return std::find(updateEventListeners.begin(), updateEventListeners.end(), listener)!=updateEventListeners.end();
	}

	void removeAllUpdateEventListeners();

private:
	friend class Updatable;

	//this is called by the UpdateEventListener to be connected
	void addUpdateEventListener(Updatable* listener) {
		updateEventListeners.push_back(listener);
	}
	
	//this is called by the UpdateEventListener to be disconnected
	void removeUpdateEventListener(Updatable* listener) {
		for (auto it = updateEventListeners.begin(); it != updateEventListeners.end(); )
		{
			if ((*it) == listener)
				it = updateEventListeners.erase(it);
			else
				it++;
		}
	}

	std::vector<Updatable*> updateEventListeners;
};


class Updatable
{
public:
	Updatable()
		:updateEventSource(0)
	{}

	virtual ~Updatable() {
		disableUpdate();
	}
	
	bool isUpdateEnabled() const {
		return updateEventSource!=NULL;
	}

	void disableUpdate() {
		if(updateEventSource) {
			updateEventSource->removeUpdateEventListener(this);
			updateEventSource = NULL;
		}
	}

	void enableUpdate(UpdateEventSource* updateEventSource) {
		assert(updateEventSource!=NULL);

		if(this->updateEventSource) {
			this->updateEventSource->removeUpdateEventListener(this);
			this->updateEventSource = updateEventSource;
		}

		updateEventSource->addUpdateEventListener(this);
	}

	virtual void update() = 0;

private:
	UpdateEventSource* updateEventSource;

protected:
	friend class UpdateEventSource;

};


inline void UpdateEventSource::triggerUpdateEvent() {
	for(auto it=updateEventListeners.begin(); it!=updateEventListeners.end(); ++it)
		(*it)->update();
}

inline void UpdateEventSource::removeAllUpdateEventListeners() {
	for(auto it=updateEventListeners.begin(); it!=updateEventListeners.end(); ++it) {
		Updatable* listener = (*it);
		listener->updateEventSource = NULL;
	}
}