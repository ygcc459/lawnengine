#pragma once

#include <string>
#include <map>
#include <vector>

//TODO: make it thread-safe
class FixedString
{
public:
	int index;

	FixedString()
	{
		SetAsEmpty();
	}

	FixedString(const char* s)
	{
		index = GetOrAdd(std::string(s));
	}

	FixedString(const std::string& s)
	{
		index = GetOrAdd(s);
	}

	FixedString(const FixedString& s)
	{
		index = s.index;
	}

	bool IsEmpty() const
	{
		return index == 0;
	}

	void SetAsEmpty()
	{
		Init();
		index = 0;
	}

	bool operator = (const FixedString& s)
	{
		return index = s.index;
	}

	bool operator == (const FixedString& s) const
	{
		return index == s.index;
	}

	bool operator != (const FixedString& s) const
	{
		return index != s.index;
	}

	bool operator < (const FixedString& s) const
	{
		return index < s.index;
	}

	const char* c_str() const
	{
		return (*allStrings)[index].c_str();
	}

	const std::string& str() const
	{
		return (*allStrings)[index];
	}

private:
	static std::unordered_map<std::string, int>* stringToIndexMap;
	static std::vector<std::string>* allStrings;

	static void Init();

	static int GetOrAdd(const std::string& str);
	static int Add(const std::string& str);
};
