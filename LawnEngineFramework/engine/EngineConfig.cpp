

#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "EngineConfig.h"


bool LawnEngineConfig::showMouseCursor = true;

int LawnEngineConfig::windowModeBackBufferWidth = 640;
int LawnEngineConfig::windowModeBackBufferHeight = 480;

uint LawnEngineConfig::fullScreenBackBufferWidth = 1024;
uint LawnEngineConfig::fullScreenBackBufferHeight = 768;

float4 LawnEngineConfig::sceneBackgroundColor = float4(1.0f, 0.5f, 0.5f, 1.0f);

float LawnEngineConfig::initialCameraPanSpeed = 1.0f;
float LawnEngineConfig::initialCameraRotateSpeed = 0.005f;

bool LawnEngineConfig::isMouseKeyEventEnabled = true;

bool LawnEngineConfig::wireframeMode = false;

uint LawnEngineConfig::UIHelper_batch_size = 256;
