#pragma once

#include <string>

std::string formatString(const char *fmt, ...);
void splitString(const std::string& str, const std::string& splitChars, std::vector<std::string>& result, bool allowZeroLength = true);

std::string stringToLower(const std::string& src);
std::string stringToUpper(const std::string& src);

template<class TIterator>
std::string stringJoin(TIterator begin, TIterator end, const std::string& separator)
{
	std::string str;
	bool first = true;
	while (begin != end)
	{
		if (!first)
			str.append(separator);
		str.append(*begin);

		++begin;
		first = false;
	}

	return str;
}
