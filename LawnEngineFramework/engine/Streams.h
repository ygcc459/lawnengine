#pragma once

#include <vector>

class WriteStream
{
public:
	WriteStream()
	{}

	virtual ~WriteStream()
	{}

	virtual bool CanWrite() = 0;

	virtual size_t Write(const void* data, size_t data_size) = 0;
};

class ReadStream
{
public:
	ReadStream()
	{}

	virtual ~ReadStream()
	{}

	virtual size_t Size() const = 0;

	virtual bool CanRead() const = 0;
	
	virtual size_t Read(void* outdata, size_t maxReadBytes) = 0;

	bool ReadAllData(std::vector<byte>& dst)
	{
		size_t totalSize = this->Size();

		dst.resize(totalSize);

		size_t offset = 0;
		while (offset < totalSize)
		{
			size_t readedSize = this->Read(dst.data() + offset, totalSize - offset);

			if (readedSize == 0)
			{
				break;
			}

			offset += readedSize;
		}

		return offset == totalSize;
	}

	std::string ReadAllString()
	{
		char* buffer = new char[this->Size()];

		size_t actualSize = this->Read(buffer, this->Size());

		std::string s = std::string(buffer, buffer + actualSize);

		delete [] buffer;

		return s;
	}
};

class MemoryWriteStream : public WriteStream
{
	std::vector<byte> buffer;
	size_t position;

public:
	MemoryWriteStream(size_t initialSize = 1024)
		:position(0)
	{
		buffer.reserve(initialSize);
	}

	virtual ~MemoryWriteStream()
	{
	}

	void EnsureCapacity(size_t requiredSize)
	{
		buffer.reserve(requiredSize);
	}

	size_t Size() const { return buffer.size(); }

	byte* Data() { return buffer.data(); }

	virtual bool CanWrite() { return true; }

	virtual size_t Write(const void* data, size_t data_size)
	{
		if (position + data_size > buffer.size())
		{
			buffer.resize(position + data_size);
		}

		memmove(buffer.data() + position, data, data_size);

		position += data_size;

		return data_size;
	}

	void SetPosition(size_t pos) { this->position = pos; }

	size_t GetPosition() const { return this->position; }
};

class MemoryReadStream : public ReadStream
{
	byte* buffer;
	bool bufferCopied;

	size_t size;

	size_t readPosition;  //the next position to read from

public:
	MemoryReadStream(byte* srcBuffer, size_t size, bool makeACopy)
		:bufferCopied(makeACopy), readPosition(0)
	{
		this->size = size;

		if (makeACopy)
		{
			this->buffer = new byte[size];
			memmove(this->buffer, srcBuffer, size);
		}
		else
		{
			this->buffer = srcBuffer;
		}
	}

	virtual ~MemoryReadStream()
	{
		if (bufferCopied)
		{
			delete[] this->buffer;
		}
	}

	const byte* Data() const { return buffer; }

	virtual size_t Size() const { return size; }
	
	virtual bool CanRead() const
	{
		return readPosition < this->size;
	}

	void Rewind()
	{
		SetPosition(0);
	}

	virtual size_t Read(void* outdata, size_t maxReadBytes)
	{
		size_t actualReadBytes;
		if (readPosition + maxReadBytes <= this->size)
			actualReadBytes = maxReadBytes;
		else if (CanRead())
			actualReadBytes = this->size - readPosition;
		else
			actualReadBytes = 0;

		if (actualReadBytes > 0)
		{
			memmove(outdata, buffer + readPosition, actualReadBytes);

			readPosition += actualReadBytes;
		}

		return actualReadBytes;
	}

	void SetPosition(size_t pos) { readPosition = pos; }

	size_t GetPosition() const { return readPosition; }
};

class FileWriteStream : public WriteStream
{
	FILE* file;

public:
	FileWriteStream(const char* path, bool text, bool truncatExisting = true)
	{
		char mode[3] = { 0, 0, 0 };
		mode[0] = (truncatExisting ? 'w' : 'a');
		mode[1] = (text ? '\0' : 'b');

		file = fopen(path, mode);
	}

	virtual ~FileWriteStream()
	{
		if (file)
		{
			fclose(file);
		}
	}

	virtual bool CanWrite() { return file != NULL; }

	virtual size_t Write(const void* data, size_t data_size)
	{
		if (file)
		{
			return fwrite(data, 1, data_size, file);
		}
		else
		{
			return 0;
		}
	}
};

class FileReadStream : public ReadStream
{
	FILE* file;
	size_t fileSize;

public:
	FileReadStream(const char* path, bool text)
	{
		char mode[3] = { 0, 0, 0 };
		mode[0] = 'r';
		mode[1] = (text ? '\0' : 'b');

		file = fopen(path, mode);

		if (file)
		{
			fseek(file, 0, SEEK_END);
			fileSize = ftell(file);
			fseek(file, 0, SEEK_SET);
		}
	}

	virtual ~FileReadStream()
	{
		if (file)
		{
			fclose(file);
		}
	}

	virtual size_t Size() const { return fileSize; }

	virtual bool CanRead() const
	{
		if (file && fileSize != 0)
			return !feof(file);
		else
			return false;
	}

	void Rewind()
	{
		if (file)
		{
			fseek(file, 0, SEEK_SET);
		}
	}

	virtual size_t Read(void* outdata, size_t maxReadBytes)
	{
		if (file)
		{
			return fread(outdata, 1, maxReadBytes, file);
		}
		else
		{
			return 0;
		}
	}
};
