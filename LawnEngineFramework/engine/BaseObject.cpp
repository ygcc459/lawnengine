#include "fast_compile.h"
#include "BaseObject.h"

IMPL_CLASS_TYPE(BaseObject);

BaseObject::BaseObject()
	: destructionEvent(this), dirtyEvent(this)
{

}

BaseObject::~BaseObject()
{
	OnDestroy();
}

bool BaseObject::Serialize(Serializer& serializer)
{
	if (!Super::Serialize(serializer))
		return false;

	Reflector reflector;
	Reflect(reflector);

	for (ObjectMember& member : reflector.members)
	{
		if (!member.HasAttribute<SkipSerializationAttribute>())
		{
			serializer.SerializeMember(member.memberName.c_str(), member.GetAddr(this), member.GetProperty());
		}
	}

	return true;
}

bool BaseObject::Deserialize(Deserializer& deserializer)
{
	if (!Super::Deserialize(deserializer))
		return false;

	Reflector reflector;
	Reflect(reflector);

	for (ObjectMember& member : reflector.members)
	{
		if (!member.HasAttribute<SkipSerializationAttribute>())
		{
			deserializer.DeserializeMember(member.memberName.c_str(), member.GetAddr(this), member.GetProperty());
		}
	}

	return true;
}

void BaseObject::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("assetPath", &Self::assetPath)
		.AddAttribute(new SkipSerializationAttribute())
		.AddAttribute(new HideInInspectorAttribute());
}

bool BaseObject::OnLoad(Archive& archive)
{
	Deserializer deserializer(archive.Root());
	deserializer.DeserializeRootObject(this, ObjectProperty<BaseObject>());
	deserializer.InvokeAfterDeserialization();
	return true;
}

bool BaseObject::OnSave(Archive& archive)
{
	Serializer serializer(archive.Root());
	serializer.SerializeRootObject(this, ObjectProperty<BaseObject>());
	return true;
}

void BaseObject::AfterDeserialization()
{
	ReflectiveObject::AfterDeserialization();
	SetDirty();
}

void BaseObject::OnPropertyChangedByReflection(ObjectMember& member)
{
	ReflectiveObject::OnPropertyChangedByReflection(member);
	SetDirty();
}

void BaseObject::SetDirty()
{
	dirtyEvent.Trigger(this);
}
