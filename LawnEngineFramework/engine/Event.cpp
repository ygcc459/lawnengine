#include "fast_compile.h"
#include "Event.h"

EventListener::EventListener()
{

}

EventListener::~EventListener()
{
	std::vector<BaseEvent*> backupListeningEvents;
	backupListeningEvents.swap(listeningEvents);

	for (auto it = backupListeningEvents.begin(); it != backupListeningEvents.end(); ++it)
	{
		BaseEvent* eventPtr = *it;
		eventPtr->OnEventListenerDestruction(this);
	}
}

bool EventListener::HasListeningEvent(BaseEvent* eventPtr) const
{
	for (auto it = listeningEvents.begin(); it != listeningEvents.end(); ++it)
	{
		if (*it == eventPtr)
		{
			return true;
		}
	}
	return false;
}

void EventListener::AddListeningEvent(BaseEvent* eventPtr)
{
	if (!HasListeningEvent(eventPtr))
	{
		listeningEvents.push_back(eventPtr);
	}
}

void EventListener::RemoveListeningEvent(BaseEvent* eventPtr)
{
	for (auto it = listeningEvents.begin(); it != listeningEvents.end(); ++it)
	{
		if (*it == eventPtr)
		{
			listeningEvents.erase(it);
			break;
		}
	}
}
