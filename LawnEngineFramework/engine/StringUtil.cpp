#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "String.h"

std::string formatString(const char *fmt, ...)
{
	std::string strResult = "";
	if (NULL != fmt)
	{
		va_list marker = NULL;
		va_start(marker, fmt);
		size_t nLength = _vscprintf(fmt, marker) + 1;
		std::vector<char> vBuffer(nLength, '\0');
		int nWritten = _vsnprintf_s(&vBuffer[0], vBuffer.size(), nLength, fmt, marker);
		if (nWritten > 0)
		{
			strResult = &vBuffer[0];
		}
		va_end(marker);
	}
	return strResult;
}

void splitString(const std::string& str, const std::string& splitChars, std::vector<std::string>& result, bool allowZeroLength /*= true*/)
{
	size_t pos = 0;
	while (pos < str.size())
	{
		size_t splitCharPos = str.find_first_of(splitChars, pos);
		if (splitCharPos != std::string::npos)
		{
			size_t partLen = splitCharPos - pos;
			if (partLen != 0 || allowZeroLength)
			{
				std::string part = str.substr(pos, partLen);
				result.push_back(part);
			}

			pos = splitCharPos + 1;
		}
		else
		{
			size_t partLen = str.size() - pos;
			if (partLen != 0 || allowZeroLength)
			{
				std::string part = str.substr(pos, partLen);
				result.push_back(part);
			}

			return;
		}
	}
}

std::string stringToLower(const std::string& src)
{
	std::string s;
	s.reserve(src.size());

	for (int i = 0; i < src.size(); i++)
	{
		char c = src[i];
		if (c >= 'A' && c <= 'Z')
			c = 'a' + c - 'A';
		s.push_back(c);
	}

	return s;
}

std::string stringToUpper(const std::string& src)
{
	std::string s;
	s.reserve(src.size());

	for (int i = 0; i < src.size(); i++)
	{
		char c = src[i];
		if (c >= 'a' && c <= 'z')
			c = 'A' + c - 'a';
		s.push_back(c);
	}

	return s;
}
