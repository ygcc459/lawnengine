#pragma once

#include <string>
#include "Reflection/Reflection.h"
#include "AssetSystem/AssetPath.h"
#include "Event.h"

struct BaseProperty;

class BaseObject : public ReflectiveObject, public EventListener
{
	DECL_CLASS_TYPE(BaseObject, ReflectiveObject);

public:
	AssetPath assetPath;  //only valid if object is an asset

	//ListeningEvents listeningEvents;

	Event<std::function<void(BaseObject*)>> destructionEvent;

	Event<std::function<void(BaseObject*)>> dirtyEvent;

public:
	BaseObject();
	virtual ~BaseObject();

	virtual void OnAwake()
	{
	}

	virtual void OnDestroy()
	{
		destructionEvent.Trigger(this);
	}

	virtual bool OnLoad(Archive& archive);
	virtual bool OnSave(Archive& archive);

	bool IsAsset() const { return assetPath.IsValid(); }
	const AssetPath& GetAssetPath() const { return assetPath; }
	void SetAssetPath(const AssetPath& path) { assetPath = path; }

	virtual bool Serialize(Serializer& serializer) override;
	virtual bool Deserialize(Deserializer& deserializer) override;
	virtual void Reflect(Reflector& reflector) override;

	virtual void AfterDeserialization() override;

	virtual void OnPropertyChangedByReflection(ObjectMember& member) override;

	virtual void SetDirty();
};

typedef std::shared_ptr<BaseObject> BaseObjectPtr;
//
//template<typename FnType>
//ListeningEvents& Event<FnType>::GetListeningEvents(BaseObject* obj)
//{
//	return obj->listeningEvents;
//}
