#pragma once

namespace Util
{
	bool DeserializeFromAsset(AssetPath path, ReflectiveObject& target);
	bool SerializeToAsset(ReflectiveObject& source, AssetPath path);

	template<typename T, typename A>
	bool Exists(const std::vector<T, A>& vec, const T& valueToFind)
	{
		for (size_t i = 0; i < vec.size(); i++)
		{
			if (vec[i] == valueToFind)
				return true;
		}

		return false;
	}

	template<typename T, typename A>
	size_t FindFirstIndex(const std::vector<T, A>& vec, const T& valueToFind)
	{
		for (size_t i = 0; i < vec.size(); i++)
		{
			if (vec[i] == valueToFind)
				return i;
		}

		return size_t(-1);
	}

	template<typename ToType, typename FromType>
	ToType* CastPtrWithCheck(FromType* ptr)
	{
		if (ptr)
		{
			Type* fromType = ptr->GetType();
			Type* toType = &ToType::StaticType;

			if (!fromType->IsSameOrChildOf(toType))
			{
				Log::Error("Type-cast failed, from '%s' to '%s'", fromType->TypeName().c_str(), toType->TypeName().c_str());
				return nullptr;
			}

			return (ToType*)ptr;
		}
		else
		{
			return nullptr;
		}
	}

	// on failure, will return nullptr without triggering any error
	template<typename ToType, typename FromType>
	ToType* CastPtr(FromType* ptr)
	{
		if (ptr)
		{
			Type* fromType = ptr->GetType();
			Type* toType = &ToType::StaticType;

			if (!fromType->IsSameOrChildOf(toType))
			{
				return nullptr;
			}

			return (ToType*)ptr;
		}
		else
		{
			return nullptr;
		}
	}

	// on failure, will return nullptr without triggering any error
	ReflectiveObject* CastPtr(ReflectiveObject* ptr, Type* toType);

	template<typename ToType, typename FromType>
	std::shared_ptr<ToType> CastPtrWithCheck(const std::shared_ptr<FromType>& ptr)
	{
		if (ptr)
		{
			const FromType* rawptr = ptr.get();

			Type* fromType = rawptr->GetType();
			Type* toType = &ToType::StaticType;

			if (!fromType->IsSameOrChildOf(toType))
			{
				Log::Error("Type-cast failed, from '%s' to '%s'", fromType->TypeName().c_str(), toType->TypeName().c_str());
				return std::shared_ptr<ToType>();
			}

			return std::static_pointer_cast<ToType>(ptr);
		}
		else
		{
			return std::shared_ptr<ToType>();
		}
	}

	// on failure, will return nullptr without triggering any error
	template<typename ToType, typename FromType>
	std::shared_ptr<ToType> CastPtr(const std::shared_ptr<FromType>& ptr)
	{
		if (ptr)
		{
			const FromType* rawptr = ptr.get();

			Type* fromType = rawptr->GetType();
			Type* toType = &ToType::StaticType;

			if (!fromType->IsSameOrChildOf(toType))
			{
				return std::shared_ptr<ToType>();
			}

			return std::static_pointer_cast<ToType>(ptr);
		}
		else
		{
			return std::shared_ptr<ToType>();
		}
	}

	// get current time in seconds
	float GetCurrentTime();
}
