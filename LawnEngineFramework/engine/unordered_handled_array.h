#pragma once

#include <vector>

//
// a linear & compact array, but unordered for fast add and remove
//
template<typename TElement>
class unordered_handled_array
{
public:
	typedef TElement element_type;
	typedef unsigned int handle_type;
	typedef unsigned int size_type;
	typedef element_type* iterator_type;
	typedef const element_type* const_iterator_type;

private:
	std::vector<element_type> _datas;  //_datas and _handles share the same index
	std::vector<handle_type> _handles;  //_datas and _handles share the same index

	std::vector<size_type> _handleToIndex;  //used as a LUT, key=Handle, value=index in _datas and _handles

	std::vector<handle_type> _freeHandles;

public:
	unordered_handled_array()
	{}

	~unordered_handled_array()
	{}

	void clear()
	{
		_datas.clear();
		_handles.clear();
		_handleToIndex.clear();
		_freeHandles.clear();
	}

	std::vector<element_type>& datas_array() { return _datas; }
	std::vector<handle_type>& handles_array() { return _handles; }
	std::vector<size_type>& handle_to_index_array() { return _handleToIndex; }

	static handle_type invalid_handle()
	{
		return (handle_type)(-1);
	}

	static size_type invalid_index()
	{
		return (size_type)(-1);
	}

	iterator_type begin() { return _datas.data(); }
	iterator_type end() { return _datas.data() + _datas.size(); }

	const_iterator_type cbegin() const { return _datas.data(); }
	const_iterator_type cend() const { return _datas.data() + _datas.size(); }

	size_type size() const { return (size_type)_datas.size(); }

	element_type* data() { return _datas.data(); }
	const element_type* data() const { return _datas.data(); }

	element_type& operator[] (const size_type i) noexcept
	{
		return _datas[i];
	}

	const element_type& operator[] (const size_type i) const noexcept
	{
		return _datas[i];
	}

	size_type handle_to_index(handle_type handle) const
	{
		if (handle != invalid_handle())
		{
			size_type index = _handleToIndex[handle];
			return index;
		}

		return invalid_index();
	}

	bool try_get(handle_type handle, element_type& out_element)
	{
		if (handle != invalid_handle())
		{
			size_type index = _handleToIndex[handle];
			if (index != invalid_index())
			{
				out_element = _datas[index];
				return true;
			}
		}

		return false;
	}

	handle_type add(const element_type& element)
	{
		if (_freeHandles.size() != 0)
		{
			handle_type freeHandle = _freeHandles.back();
			_freeHandles.pop_back();

			size_type index = (size_type)_datas.size();
			_datas.push_back(element);
			_handles.push_back(freeHandle);

			assert(_handleToIndex[freeHandle] == invalid_index());
			_handleToIndex[freeHandle] = index;

			return freeHandle;
		}
		else
		{
			handle_type freeHandle = (handle_type)_handleToIndex.size();
			size_type index = (size_type)_datas.size();

			_handleToIndex.push_back(index);
			_datas.push_back(element);
			_handles.push_back(freeHandle);

			return freeHandle;
		}
	}

	void remove(handle_type handle)
	{
		if (handle == invalid_handle())
			return;

		size_type removingElementIndex = _handleToIndex[handle];
		if (removingElementIndex != invalid_index())
		{
			size_type lastElementIndex = (size_type)_datas.size() - 1;
			if (removingElementIndex != lastElementIndex)
			{
				auto lastElementData = _datas[lastElementIndex];
				auto lastElementHandle = _handles[lastElementIndex];

				_datas[removingElementIndex] = lastElementData;
				_handles[removingElementIndex] = lastElementHandle;

				_handleToIndex[lastElementHandle] = removingElementIndex;
			}

			_datas.pop_back();
			_handles.pop_back();

			_handleToIndex[handle] = invalid_index();
			_freeHandles.push_back(handle);
		}
	}

};
