#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "FixedString.h"

std::unordered_map<std::string, int>* FixedString::stringToIndexMap = nullptr;
std::vector<std::string>* FixedString::allStrings = nullptr;

void FixedString::Init()
{
	if (stringToIndexMap == nullptr)
	{
		stringToIndexMap = new std::unordered_map<std::string, int>();
		allStrings = new std::vector<std::string>();

		Add("");
	}
}

int FixedString::GetOrAdd(const std::string& str)
{
	Init();

	auto it = stringToIndexMap->find(str);
	if (it != stringToIndexMap->end())
	{
		return it->second;
	}

	int index = Add(str);

	return index;
}

int FixedString::Add(const std::string& str)
{
	int index = allStrings->size();

	allStrings->push_back(str);
	stringToIndexMap->insert(std::make_pair(str, index));

	return index;
}
