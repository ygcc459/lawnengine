#pragma once

#include <vector>

class BitArray
{
public:
	typedef unsigned int size_type;

	typedef unsigned long long chunk_type;

	enum
	{
		BitsPerChunk = sizeof(chunk_type) * 8,
	};

protected:
	chunk_type* chunks;
	size_type chunkCount;
	size_type bitsCount;
	
public:
	BitArray()
		: bitsCount(0), chunkCount(0), chunks(nullptr)
	{}

	BitArray(size_type initialSize)
	{
		ResizeUninitialized(initialSize);
	}

	BitArray(size_type initialSize, bool initialValue)
	{
		Resize(initialSize, initialValue);
	}

	~BitArray()
	{
		Destroy();
	}

	void Destroy()
	{
		if (chunks)
		{
			delete[] chunks;
			chunks = nullptr;
		}

		bitsCount = 0;
		chunkCount = 0;
	}

	size_type BitsCount() const { return bitsCount; }

	size_type ChunkCount() const { return chunkCount; }

	//shrinked bits are set to zero
	//expanded bits are uninitialized (random value)
	void ResizeUninitialized(size_type bitsCountNew)
	{
		size_type chunkCountNew = (bitsCountNew + BitsPerChunk - 1) / BitsPerChunk;

		if (chunkCountNew != chunkCount)
		{
			auto chunksNew = new chunk_type[chunkCountNew];

			memcpy(chunksNew, chunks, ChunkCount() * sizeof(chunk_type));

			if (chunkCountNew < chunkCount)
			{
				//remove unused bits in last chunk
				size_type usedBitsInLastChunk = bitsCount % chunkCount;
				if (usedBitsInLastChunk != BitsPerChunk)
				{
					chunk_type cv = chunks[chunkCount - 1];
					chunks[chunkCount - 1] = cv & ((1 << usedBitsInLastChunk) - 1);
				}
			}

			if (chunks)
				delete[] chunks;

			chunks = chunksNew;
			chunkCount = chunkCountNew;
		}
		
		bitsCount = bitsCountNew;
	}

	//shrinked bits are set to zero
	//expanded bits are set to expandValue
	void Resize(size_type bitsCountNew, bool expandValue)
	{
		size_type chunkCountNew = (bitsCountNew + BitsPerChunk - 1) / BitsPerChunk;

		ResizeUninitialized(bitsCountNew);

		chunk_type cv = expandValue ? ((chunk_type)-1) : 0;
		for (size_type i = chunkCount; i < chunkCountNew; i++)
		{
			chunks[i] = cv;
		}
	}

	bool GetBit(size_type i) const
	{
		size_type index = i / BitsPerChunk;
		size_type offset = i % BitsPerChunk;
		chunk_type one = (chunk_type)1;
		chunk_type mask = one << offset;
		chunk_type prev = chunks[index];
		return prev & mask;
	}

	void SetBit(size_type i, bool v)
	{
		size_type index = i / BitsPerChunk;
		size_type offset = i % BitsPerChunk;
		chunk_type one = (chunk_type)1;
		chunk_type mask = one << offset;
		chunk_type prev = chunks[index];
		chunks[index] = (prev & (~mask)) | (v ? mask : 0);
	}

	void SetBitRange(size_type first, size_type last, bool v)
	{
		size_type first_index = first / BitsPerChunk;
		size_type first_offset = first % BitsPerChunk;
		size_type last_index = last / BitsPerChunk;
		size_type last_offset = last % BitsPerChunk;

		chunk_type one = (chunk_type)1;
		chunk_type left_mask = ~((one << first_offset) - 1);
		chunk_type right_mask = (one << last_offset) | ((one << last_offset) - 1);

		if (first_index != last_index)
		{
			chunks[first_index] = (chunks[first_index] & (~left_mask)) | (v ? left_mask : 0);

			chunks[last_index] = (chunks[last_index] & (~right_mask)) | (v ? right_mask : 0);

			for (size_type i = first_index + 1; i < last_index; ++i)
				chunks[i] = (chunk_type)(-1);
		}
		else
		{
			chunk_type mask = left_mask & right_mask;
			chunks[first_index] = (chunks[first_index] & (~mask)) | (v ? mask : 0);
		}
	}

	void SetAllBits(bool value)
	{
		chunk_type cv = value ? ((chunk_type)-1) : 0;
		for (size_type i = chunkCount; i < chunkCount; i++)
		{
			chunks[i] = cv;
		}
	}

	size_type ElementIndexToChunkIndex(size_type i) const
	{
		return i / BitsPerChunk;
	}

	chunk_type GetChunk(size_type chunkIndex) const
	{
		return chunks[chunkIndex];
	}

	void SetChunk(size_type chunkIndex, chunk_type chunkValue)
	{
		chunks[chunkIndex] = chunkValue;
	}
};
