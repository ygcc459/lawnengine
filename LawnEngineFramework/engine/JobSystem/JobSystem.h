#pragma once

#include <atomic>
#include <mutex>

enum class JobDispatchTarget
{
	MainThread = 0,
	RenderThreads,
	WorkerThreads,

	Count,
};

class JobQueue;

class Job;
typedef RefCountedPtr<Job> JobPtr;


struct JobLaunchParams
{
	uint32 loopCount = 1;
	uint32 batchSize = 1;
	JobDispatchTarget target = JobDispatchTarget::WorkerThreads;
	bool async = true;

	JobPtr* precedingJobs = nullptr;
	uint32 precedingJobsCount = 0;
};


class Job : public RefCountedObject
{
	friend class JobSystem;
	friend class JobQueue;

private:
	JobLaunchParams launchParams;

	std::atomic<uint32> unfinishedPrecedingJobs;
	std::atomic<uint32> unfinishedLoops;
	std::atomic<uint32> enqueued;

	std::mutex subsequentJobsMutex;
	std::vector<JobPtr> subsequentJobs;
	std::atomic<uint32> subsequentJobsNotified;

public:
	Job();
	virtual ~Job();

	virtual void OnExecute(uint32 loopIndex) = 0;

	void Wait();

	bool IsDone() const { return unfinishedLoops.load() == 0; }

private:
	void Startup(uint32 loopCount, uint32 batchSize = 1, JobPtr* precedingJobs = nullptr, uint32 numPrecedingJobs = 0, JobDispatchTarget target = JobDispatchTarget::WorkerThreads, bool async = true);
	void Execute(uint32 loopIndex);
	void Shutdown();
	void WaitUntilFinish();

	//return false if already done
	bool AddSubsequentJob(JobPtr subsequentJob);
	void OnPrecedingJobDone(JobPtr precedingJob);
	void NotifySubsequentJobs();
	void Enqueue();
};


class JobSystem
{
	friend class Job;

private:
	static JobSystem* instance;

	JobQueue* mainThreadJobQueue = nullptr;

	JobQueue* renderThreadsJobQueue = nullptr;

	JobQueue* workerThreadsJobQueue = nullptr;

	JobQueue* jobQueues[(int)JobDispatchTarget::Count] = {};

public:
	JobSystem() {}
	~JobSystem() {}

	static void CreateInstance();
	static void DestroyInstance();
	static JobSystem& GetInstance() { assert(instance != nullptr); return *instance; }

	static const size_t CacheLineSize = 64;

	void Startup();
	void Shutdown();

	void Dispatch(JobPtr job, uint32 loopCount, uint32 batchSize = 1, JobPtr* precedingJobs = nullptr, uint32 numPrecedingJobs = 0, JobDispatchTarget target = JobDispatchTarget::WorkerThreads, bool async = true);

	template<typename JobType>
	void Dispatch(RefCountedPtr<JobType> job, uint32 loopCount, uint32 batchSize = 1, JobPtr* precedingJobs = nullptr, uint32 numPrecedingJobs = 0, JobDispatchTarget target = JobDispatchTarget::WorkerThreads, bool async = true)
	{
		Dispatch(job.StaticCast<Job>(), loopCount, batchSize, precedingJobs, numPrecedingJobs, target, async);
	}

	void Wait(JobPtr job);

	bool IsDone(JobPtr job) const { return job->IsDone(); }

private:
	void ExecuteSync(JobPtr job);

	void Enqueue(JobPtr job);
};


void ParallelFor(uint32 count, const std::function<void(uint32)>& func, bool async = true);
