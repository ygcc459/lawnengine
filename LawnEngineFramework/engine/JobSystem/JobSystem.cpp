#include "fast_compile.h"
#include "JobSystem.h"
#include "concurrentqueue.h"
#include <semaphore>
#include <thread>

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//multiple producer, multiple consumer
class JobQueue
{
	struct JobWorker
	{
		std::string name;
		std::thread* thread;

		volatile int shouldExit;

		JobWorker(const std::string& name)
			: name(name), thread(nullptr), shouldExit(0)
		{}
	};

	struct JobData
	{
		JobPtr job;
		uint32 loopIndexBegin;
		uint32 loopIndexEnd;
	};

	std::vector<JobWorker*> workers;

	moodycamel::ConcurrentQueue<JobData> queue;
	moodycamel::ProducerToken queueProducerToken;

	std::counting_semaphore<> signalToWakeupWorkers;

public:
	JobQueue()
		: queueProducerToken(queue), signalToWakeupWorkers(0)
	{
	}

	~JobQueue()
	{
	}

	bool HasWorkerThreads() const { return workers.size() != 0; }

	uint32 GetMaxConcurrency() const { return HasWorkerThreads() ? (uint32)workers.size() : 1; }

	uint32 GetWorkerThreadsCount() const { return (uint32)workers.size(); }

	// Set numWorkerThreads to zero to reuse existing threads instead of launching new threads, e.g. main-thread-job-queue
	void Startup(uint32 numWorkerThreads)
	{
		workers.resize(numWorkerThreads, nullptr);
		for (uint32 i = 0; i < numWorkerThreads; i++)
		{
			std::string name = formatString("JobWorker%d", i);

			JobWorker* worker = new JobWorker(name);

			worker->thread = new std::thread(StaticWorkerThreadFunc, this, worker);

			workers.push_back(worker);
		}
	}

	void Shutdown()
	{
		for (size_t i = 0; i < workers.size(); i++)
		{
			JobWorker* worker = workers[i];
			worker->shouldExit = 1;
			worker->thread->join();
		}

		for (size_t i = 0; i < workers.size(); i++)
		{
			JobWorker* worker = workers[i];
			delete worker;
		}
		workers.clear();
	}

	static void StaticWorkerThreadFunc(JobQueue* jobQueue, JobWorker* worker)
	{
		jobQueue->WorkerThreadFunc(worker);
	}

	void WorkerThreadFunc(JobWorker* worker)
	{
		Log::Info("Worker thread '%s' launched", worker->name.c_str());

		uint32 emptyCounter = 0;

		while (worker->shouldExit == 0)
		{
			JobData jobData;
			if (queue.try_dequeue(jobData))
			{
				emptyCounter = 0;

				Execute(jobData);
			}
			else
			{
				emptyCounter++;

				if (emptyCounter < 64)
				{
					std::this_thread::yield();
				}
				else
				{
					signalToWakeupWorkers.acquire();
				}
			}
		}

		Log::Info("Worker thread '%s' ended", worker->name.c_str());
	}

	bool ExecuteOne()
	{
		JobData jobData;
		if (queue.try_dequeue(jobData))
		{
			Execute(jobData);
			return true;
		}
		else
		{
			return false;
		}
	}

	void ExecuteAll()
	{
		while (true)
		{
			JobData jobData;
			if (queue.try_dequeue(jobData))
			{
				Execute(jobData);
			}
			else
			{
				break;
			}
		}
	}

	void Enqueue(JobPtr job)
	{
		uint32 loopCount = job->launchParams.loopCount;
		uint32 batchSize = job->launchParams.batchSize;

		uint32 numBatches = (loopCount + batchSize - 1) / batchSize;
		uint32 loopIndexBegin = 0;

		for (uint32 i = 0; i < numBatches; i++)
		{
			JobData jobData;
			jobData.job = job;
			jobData.loopIndexBegin = loopIndexBegin;
			jobData.loopIndexEnd = math::min(loopIndexBegin + batchSize, loopCount);

			queue.enqueue(queueProducerToken, jobData);

			loopIndexBegin += batchSize;
		}

		if (HasWorkerThreads())
		{
			uint32 numWorkersToWakeup = math::min(numBatches, GetWorkerThreadsCount());
			signalToWakeupWorkers.release(numWorkersToWakeup);  //some worker might still working on other jobs, later causing a useless dequeue operation, this should be fine for this small engine
		}
	}

	void Execute(JobData& jobData)
	{
		JobPtr job = jobData.job;
		if (job)
		{
			for (uint32 i = jobData.loopIndexBegin; i < jobData.loopIndexEnd; i++)
			{
				job->Execute(i);
			}
		}
	}
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

Job::Job()
	: unfinishedLoops(0), unfinishedPrecedingJobs(0), subsequentJobsNotified(0)
{}

Job::~Job()
{
	assert(IsDone());

	delete[] launchParams.precedingJobs;
}

bool Job::AddSubsequentJob(JobPtr subsequentJob)
{
	std::lock_guard<std::mutex> lock(subsequentJobsMutex);

	if (Util::Exists(subsequentJobs, subsequentJob))
		return true;

	subsequentJobs.push_back(subsequentJob);

	if (subsequentJobsNotified.load() != 0)
	{
		JobPtr thisJob(this);
		subsequentJob->OnPrecedingJobDone(thisJob);
	}

	return true;
}

void Job::NotifySubsequentJobs()
{
	assert(IsDone());

	std::lock_guard<std::mutex> lock(subsequentJobsMutex);

	JobPtr thisJob(this);

	for (JobPtr job : subsequentJobs)
		job->OnPrecedingJobDone(thisJob);

	subsequentJobsNotified.store(1);
}

void Job::OnPrecedingJobDone(JobPtr precedingJob)
{
	if (unfinishedPrecedingJobs.fetch_sub(1) - 1 == 0)
	{
		Enqueue();
	}
}

void Job::Startup(uint32 loopCount, uint32 batchSize /*= 1*/, JobPtr* precedingJobs /*= nullptr*/, uint32 numPrecedingJobs /*= 0*/, JobDispatchTarget target /*= JobDispatchTarget::WorkerThreads*/, bool async /*= true*/)
{
	this->AddRef();  //keep alive during job execution

	launchParams.loopCount = loopCount;
	launchParams.batchSize = batchSize;
	launchParams.target = target;
	launchParams.async = async;
	launchParams.precedingJobs = new JobPtr[numPrecedingJobs];
	for (uint32 i = 0; i < numPrecedingJobs; i++)
		launchParams.precedingJobs[i] = precedingJobs[i];
	launchParams.precedingJobsCount = numPrecedingJobs;

	unfinishedLoops.store(launchParams.loopCount);
	unfinishedPrecedingJobs.store(launchParams.precedingJobsCount);

	if (launchParams.precedingJobsCount != 0)
	{
		JobPtr thisJob(this);

		for (uint32 i = 0; i < launchParams.precedingJobsCount; i++)
		{
			JobPtr precedingJob = launchParams.precedingJobs[i];
			if (precedingJob)
			{
				precedingJob->AddSubsequentJob(thisJob);
			}
		}
	}
	else
	{
		Enqueue();
	}
}

void Job::Enqueue()
{
	assert(unfinishedPrecedingJobs.load() == 0);
	assert(enqueued.load() == 0);

	enqueued.store(1);

	JobSystem::GetInstance().Enqueue(JobPtr(this));
}

void Job::Execute(uint32 loopIndex)
{
	OnExecute(loopIndex);

	const ptrdiff_t cur = unfinishedLoops.fetch_sub(1) - 1;
	if (cur == 0)
	{
		unfinishedLoops.notify_all();

		NotifySubsequentJobs();

		Shutdown();
	}
}

void Job::Shutdown()
{
	assert(unfinishedLoops.load() == 0);

	this->Release();  //keep alive during job execution
}

void Job::Wait()
{
	JobSystem::GetInstance().Wait(JobPtr(this));
}

void Job::WaitUntilFinish()
{
	for (;;)
	{
		uint32 value = unfinishedLoops.load();
		if (value <= 0)
			return;
		
		unfinishedLoops.wait(value, std::memory_order::relaxed);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

JobSystem* JobSystem::instance = nullptr;

void JobSystem::CreateInstance()
{
	instance = new JobSystem();
	instance->Startup();
}

void JobSystem::DestroyInstance()
{
	if (instance)
	{
		instance->Shutdown();
		delete instance;
		instance = nullptr;
	}
}

void JobSystem::Startup()
{
	mainThreadJobQueue = new JobQueue();
	renderThreadsJobQueue = new JobQueue();
	workerThreadsJobQueue = new JobQueue();

	jobQueues[(int)JobDispatchTarget::MainThread] = mainThreadJobQueue;
	jobQueues[(int)JobDispatchTarget::RenderThreads] = renderThreadsJobQueue;
	jobQueues[(int)JobDispatchTarget::WorkerThreads] = workerThreadsJobQueue;
}

void JobSystem::Shutdown()
{
	for (int i = 0; i < (int)JobDispatchTarget::Count; i++)
		jobQueues[i] = nullptr;

	delete mainThreadJobQueue;
	mainThreadJobQueue = nullptr;

	delete renderThreadsJobQueue;
	renderThreadsJobQueue = nullptr;

	delete workerThreadsJobQueue;
	workerThreadsJobQueue = nullptr;
}

void JobSystem::Dispatch(JobPtr job, uint32 loopCount, uint32 batchSize /*= 1*/, JobPtr* precedingJobs /*= nullptr*/, uint32 numPrecedingJobs /*= 0*/, JobDispatchTarget target /*= JobDispatchTarget::WorkerThreads*/, bool async /*= true*/)
{
	job->Startup(loopCount, batchSize, precedingJobs, numPrecedingJobs, target, async);
}

void JobSystem::Enqueue(JobPtr job)
{
	if (job->launchParams.async)
	{
		jobQueues[(int)job->launchParams.target]->Enqueue(job);
	}
	else
	{
		ExecuteSync(job);
	}
}

void JobSystem::ExecuteSync(JobPtr job)
{
	for (uint32 i = 0; i < job->launchParams.loopCount; i++)
		job->Execute(i);

	job->Shutdown();
}

void JobSystem::Wait(JobPtr job)
{
	while (!job->IsDone())
	{
		// simple job stealing: consume job queue if not empty, otherwise wait for it
		if (workerThreadsJobQueue->ExecuteOne())
		{
			std::this_thread::yield();
		}
		else
		{
			job->WaitUntilFinish();
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

class ParallelForJob : public Job
{
public:
	std::function<void(uint32)> func;

	ParallelForJob(const std::function<void(uint32)>& func)
		: func(func)
	{}

	virtual ~ParallelForJob() {}

	virtual void OnExecute(uint32 loopIndex)
	{
		func(loopIndex);
	}
};

void ParallelFor(uint32 count, const std::function<void(uint32)>& func, bool async /*= true*/)
{
	RefCountedPtr<Job> job(new ParallelForJob(func));

	JobSystem::GetInstance().Dispatch(job, count, 1, nullptr, 0, JobDispatchTarget::WorkerThreads, async);

	job->Wait();
}
