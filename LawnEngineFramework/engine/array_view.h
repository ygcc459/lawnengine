#pragma once

#include <assert.h>
#include <vector>

template<typename TElement, typename TSizeType = size_t>
class array_view
{
	typedef TSizeType size_type;
	typedef TElement element_type;
	typedef array_view<element_type> self_type;
	typedef element_type* iterator_type;
	typedef const element_type* const_iterator_type;

	element_type* _data;
	size_type _size;

public:
	array_view()
		: _data(nullptr), _size(0)
	{}

	array_view(element_type* data, size_type size)
		: _data(data), _size(size)
	{}

	array_view(element_type* begin, element_type* end)
		: _data(begin), _size(end - begin)
	{}

	template<typename allocator_type>
	array_view(std::vector<element_type, allocator_type>& v)
		: _data(v.data()), _size((size_type)v.size())
	{}

	~array_view()
	{}

	void reset(element_type* data, size_type size) { _data = data; _size = size; }

	void reset(element_type* begin, element_type* end) { _data = begin; _size = end - begin; }

	void reset(std::vector<element_type>& v) { _data = v.data(); _size = (size_type)v.size(); }

	iterator_type begin() { return _data; }
	iterator_type end() { return _data + _size; }

	const_iterator_type cbegin() const { return _data; }
	const_iterator_type cend() const { return _data + _size; }

	size_type size() const { return _size; }

	element_type* data() { return _data; }
	const element_type* data() const { return _data; }

	element_type& front() { assert(_size > 0); return _data[0]; }
	const element_type& front() const { assert(_size > 0); return _data[0]; }

	element_type& back() { assert(_size > 0); return _data[_size - 1]; }
	const element_type& back() const { assert(_size > 0); return _data[_size - 1]; }

	element_type& operator[] (const size_type i) noexcept
	{
		assert(i >= 0 && i < _size);
		return _data[i];
	}

	const element_type& operator[] (const size_type i) const noexcept
	{
		assert(i >= 0 && i < _size);
		return _data[i];
	}
};
