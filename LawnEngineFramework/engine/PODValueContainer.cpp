#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "PODValueContainer.h"

void PODValueContainer::Clear()
{
	items.clear();
	buffer.clear();
}

void* PODValueContainer::Set(const KeyType& key, const void* data, int byteSize)
{
	auto it = items.find(key);
	if (it != items.end())
	{
		PODItem& item = it->second;

		// if size grows larger, reallocate, leaving a hole in buffer
		// ok if size is less or equal
		if (byteSize > item.byteSize)
		{
			item.offset = AllocateBuffer(byteSize);
		}

		memcpy(buffer.data() + item.offset, data, byteSize);

		return buffer.data() + item.offset;
	}
	else
	{
		int offset = AllocateBuffer(byteSize);

		items.insert(std::make_pair(key, PODItem(offset, byteSize)));

		memcpy(buffer.data() + offset, data, byteSize);

		return buffer.data() + offset;
	}
}

void* PODValueContainer::Get(const KeyType& key, int* outByteSize /*= nullptr*/)
{
	auto it = items.find(key);
	if (it != items.end())
	{
		const PODItem& item = it->second;

		if (outByteSize)
		{
			*outByteSize = item.byteSize;
		}

		return buffer.data() + item.offset;
	}
	else
	{
		return nullptr;
	}
}

const void* PODValueContainer::Get(const KeyType& key, int* outByteSize /*= nullptr*/) const
{
	auto it = items.find(key);
	if (it != items.end())
	{
		const PODItem& item = it->second;

		if (outByteSize)
		{
			*outByteSize = item.byteSize;
		}

		return buffer.data() + item.offset;
	}
	else
	{
		return nullptr;
	}
}

int PODValueContainer::AllocateBuffer(int byteSize)
{
	int offset = buffer.size();
	buffer.resize(buffer.size() + byteSize);
	return offset;
}
