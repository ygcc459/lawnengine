#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "Log.h"

#include <vector>
#include <string>

#if VCPP_PLATFORM == VCPP_PLATFORM_WINDOWS
#include <Windows.h>
#include <stdio.h>
#elif VCPP_PLATFORM == VCPP_PLATFORM_ANDROID || VCPP_PLATFORM == VCPP_PLATFORM_IOS
#include <stdarg.h>
#include <android/log.h>
#else
#include <stdarg.h>
#endif

static std::string FormatString(const char* format, va_list argptr)
{
	std::string strResult = "";

	size_t nLength = _vscprintf(format, argptr) + 1;
	std::vector<char> vBuffer(nLength, '\0');
	int nWritten = _vsnprintf_s(&vBuffer[0], vBuffer.size(), nLength, format, argptr);
	if (nWritten>0)
	{
		strResult = &vBuffer[0];
	}

	return strResult;
}

bool Log::enableLogFile = true;
bool Log::enableStdOut = false;
bool Log::enablePlatformLog = true;
std::string Log::logFilePath = "";
LogCallback Log::logCallback = NULL;
Log::LogMutex Log::logMutex;

void Log::SetLogFilePath(const char* path)
{
	std::lock_guard<LogMutex> guard(logMutex);

	Log::logFilePath = path;
}

void Log::SetTarget(bool enableLogFile, bool enableStdOut, bool enablePlatformLog)
{
	std::lock_guard<LogMutex> guard(logMutex);

	Log::enableLogFile = enableLogFile;
	Log::enableStdOut = enableStdOut;
	Log::enablePlatformLog = enablePlatformLog;
}

void Log::PlatformLog(const char* format, ...)
{
	std::lock_guard<LogMutex> guard(logMutex);

	va_list argptr;
	va_start(argptr, format);
	std::string msg = FormatString(format, argptr);
	va_end(argptr);

	PlatformLogRaw(msg.c_str());
}

void Log::PlatformLogRaw(const char* msg)
{
	std::lock_guard<LogMutex> guard(logMutex);

#if VCPP_PLATFORM == VCPP_PLATFORM_WINDOWS
	OutputDebugStringA(msg);

#elif VCPP_PLATFORM == VCPP_PLATFORM_ANDROID
	__android_log_vprint(ANDROID_LOG_INFO, "LawnEngine", "%s", msg);

#elif VCPP_PLATFORM == VCPP_PLATFORM_IOS
	puts(msg);

#endif
}

void Log::Debug(const char *format, ...)
{
	std::lock_guard<LogMutex> guard(logMutex);

	va_list argptr;
	va_start(argptr, format);
	std::string msg = FormatString(format, argptr);
	va_end(argptr);

	WriteLog(LL_Debug, msg);
}

void Log::Info(const char *format, ...)
{
	std::lock_guard<LogMutex> guard(logMutex);

	va_list argptr;
	va_start(argptr, format);
	std::string msg = FormatString(format, argptr);
	va_end(argptr);

	WriteLog(LL_Info, msg);
}

void Log::Warning(const char *format, ...)
{
	std::lock_guard<LogMutex> guard(logMutex);

	va_list argptr;
	va_start(argptr, format);
	std::string msg = FormatString(format, argptr);
	va_end(argptr);

	WriteLog(LL_Warning, msg);
}

void Log::Error(const char *format, ...)
{
	std::lock_guard<LogMutex> guard(logMutex);

	va_list argptr;
	va_start(argptr, format);
	std::string msg = FormatString(format, argptr);
	va_end(argptr);

	WriteLog(LL_Error, msg);
}

void Log::WriteLog(LogLevel level, const std::string& msg)
{
	std::lock_guard<LogMutex> guard(logMutex);

	if (logCallback)
	{
		logCallback(level, msg.c_str());
	}

	static std::string newstr;

	newstr.clear();

	switch (level)
	{
	case LL_Debug:
		newstr += "[Debug] ";
		break;
	case LL_Info:
		newstr += "[Info] ";
		break;
	case LL_Warning:
		newstr += "[Warning] ";
		break;
	case LL_Error:
		newstr += "[Error] ";
		break;
	default:
		break;
	}

	newstr += msg;

	if (newstr.size() != 0)
	{
		if (enableLogFile && !logFilePath.empty())
		{
			FILE* f = fopen(logFilePath.c_str(), "a");
			fprintf(f, "%s\n", newstr.c_str());
			fclose(f);
		}

		if (enableStdOut)
		{
			printf("%s\n", newstr.c_str());
		}

		if (enablePlatformLog)
		{
#if VCPP_PLATFORM == VCPP_PLATFORM_WINDOWS
			newstr += "\n";  //work-around for line-breaking in windows platform output
#endif
			PlatformLogRaw(newstr.c_str());
		}
	}
}
