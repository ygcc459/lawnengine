#pragma once


/***********************************************************************
	Module Methods calling

	after app runs:
		init() for each module is called

	in each frame:
		first, onRender() for each module is called
		then onUpdateScene(fElapsedTime) for each module is called

		if device is lost: onDeviceLost() for each module is called
		and, after the lost device is reset, onDeviceRestored() for each module is called

		when win32 mouse events or win32 key events happens, onMouse* and onKey* methods is called

	just before app exits:
		destroy() for each module is called
***********************************************************************/

class LawnEnginePlugin
{
	friend class LawnEngine;

protected:
	LawnEnginePlugin() {}
	virtual ~LawnEnginePlugin() {}

public:
	virtual bool preInit() { return true; }
	virtual bool init() = 0;
	virtual bool destroy() = 0;

	virtual void onRender() = 0;
	virtual void onUpdateScene(float fElapsedTime) = 0;

};


