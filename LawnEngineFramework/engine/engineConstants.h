#pragma once

namespace EngineConstants
{
	enum {
		RenderTargetViewMax = 4,
		MaxSerializeTagLength = 256,
	};
}
