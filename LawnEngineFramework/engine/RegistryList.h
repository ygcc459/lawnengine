#pragma once

#include <vector>

//
// object container
// fast operations: register, unregister, random-visit, iterate
// limit: does not preserve order
//
template<typename T>
class RegistryList
{
public:
	struct Handle
	{
		RegistryList<T>* registryList;
		int32 index;

		Handle()
			: registryList(nullptr), index(-1)
		{}

		Handle(int32 index)
			: registryList(nullptr), index(index)
		{}

		~Handle()
		{
			Unregister();
		}

		bool IsRegistered() const
		{
			return registryList != nullptr && index >= 0;
		}

		void Unregister()
		{
			if (IsRegistered())
			{
				registryList->Unregister(*this);
			}
		}

	private:
		Handle(const Handle&)
		{}
	};

private:
	std::vector<Handle*> handles;
	std::vector<T> datas;

public:
	RegistryList()
	{
	}

	~RegistryList()
	{
		UnregisterAll();
	}

	void Register(const T& v, Handle& h)
	{
		CHECK(!h.IsRegistered());

		h.registryList = this;
		h.index = (int32)datas.size();

		handles.push_back(&h);
		datas.push_back(v);
	}

	void Unregister(Handle& h)
	{
		if (h.IsRegistered())
		{
			// swap with the last element
			if (h.index != datas.size() - 1)
			{
				int32 indexToSwap = h.index;

				datas[indexToSwap] = datas.back();
				handles[indexToSwap] = handles.back();
				handles[indexToSwap]->index = indexToSwap;
			}

			// remove the last element
			datas.pop_back();
			handles.pop_back();

			h.registryList = nullptr;
			h.index = -1;
		}
	}

	int32 GetSize() const
	{
		return (int32)datas.size();
	}

	T& operator [] (int32 index)
	{
		return datas[index];
	}

	const T& operator [] (int32 index) const
	{
		return datas[index];
	}

	bool Contains(Handle& h) const
	{
		return h.IsRegistered() && h.registryList == this;
	}

	void UnregisterAll()
	{
		for (int32 i = 0; i < GetSize(); i++)
		{
			Handle* h = handles[i];
			h->registryList = nullptr;
			h->index = -1;
		}

		handles.clear();
		datas.clear();
	}
};
