#pragma once


//#define EXIT_WHEN_RESOURCE_NOT_FIND


class LawnEngineConfig
{
private:
	LawnEngineConfig(void) {}
	~LawnEngineConfig(void) {}

public:
	static bool showMouseCursor;

	static int windowModeBackBufferWidth;
	static int windowModeBackBufferHeight;

	static uint fullScreenBackBufferWidth;
	static uint fullScreenBackBufferHeight;

	static float4 sceneBackgroundColor;

	static float initialCameraPanSpeed;
	static float initialCameraRotateSpeed;

	static bool isMouseKeyEventEnabled;

	static bool wireframeMode;

	static uint UIHelper_batch_size;

};

