#pragma once

#include <list>

struct BaseEvent;
struct EventListener;

struct BaseEvent
{
	virtual void OnEventListenerDestruction(EventListener* listener) = 0;
};

struct EventListener
{
private:
	std::vector<BaseEvent*> listeningEvents;

public:
	EventListener();
	~EventListener();

	bool HasListeningEvent(BaseEvent* eventPtr) const;
	void AddListeningEvent(BaseEvent* eventPtr);
	void RemoveListeningEvent(BaseEvent* eventPtr);
};

template<typename FnType>
struct Event : public BaseEvent
{
	struct EventListeningData
	{
		EventListener* listener;
		FnType callbackFunction;

		EventListeningData(EventListener* listener, FnType callbackFunction)
			:listener(listener), callbackFunction(callbackFunction)
		{}
	};

	std::list<EventListeningData> listeningDatas;
	BaseObject* eventSourceObject;

public:
	Event(BaseObject* eventSourceObject)
		: eventSourceObject(eventSourceObject)
	{}

	~Event()
	{
		OnEventSourceDestruction();
	}

	void OnEventSourceDestruction()
	{
		// notify each listener to remove me
		for (auto it = listeningDatas.begin(); it != listeningDatas.end(); ++it)
		{
			auto& listener = *it;
			listener.listener->RemoveListeningEvent(this);
		}

		listeningDatas.clear();
		eventSourceObject = nullptr;
	}

	virtual void OnEventListenerDestruction(EventListener* listener) override
	{
		RemoveListener(listener);
	}

	bool HasListener(EventListener* listener) const
	{
		for (auto it = listeningDatas.begin(); it != listeningDatas.end(); ++it)
		{
			const auto& curListener = *it;
			if (curListener.listener == listener)
			{
				return true;
			}
		}
		return false;
	}

	void AddListener(EventListener* listener, FnType callbackFunction)
	{
		if (!HasListener(listener))
		{
			this->listeningDatas.emplace_back(listener, callbackFunction);

			listener->AddListeningEvent(this);
		}
	}

	void RemoveListener(EventListener* listener)
	{
		// remove subscription from listener
		listener->RemoveListeningEvent(this);

		// remove subscription from source object
		for (auto it = listeningDatas.begin(); it != listeningDatas.end(); ++it)
		{
			auto& curListener = *it;
			if (curListener.listener == listener)
			{
				listeningDatas.erase(it);
				break;
			}
		}
	}

	void Trigger()
	{
		for (auto it = listeningDatas.begin(); it != listeningDatas.end(); ++it)
		{
			auto& listener = *it;
			listener.callbackFunction();
		}
	}

	template<typename T1>
	void Trigger(const T1& param1)
	{
		for (auto it = listeningDatas.begin(); it != listeningDatas.end(); ++it)
		{
			auto& listener = *it;
			listener.callbackFunction(param1);
		}
	}

	template<typename T1, typename T2>
	void Trigger(const T1& param1, const T1& param2)
	{
		for (auto it = listeningDatas.begin(); it != listeningDatas.end(); ++it)
		{
			auto& listener = *it;
			listener.callbackFunction(param1, param2);
		}
	}

	template<typename T1, typename T2, typename T3>
	void Trigger(const T1& param1, const T1& param2, const T1& param3)
	{
		for (auto it = listeningDatas.begin(); it != listeningDatas.end(); ++it)
		{
			auto& listener = *it;
			listener.callbackFunction(param1, param2, param3);
		}
	}

	template<typename T1, typename T2, typename T3, typename T4>
	void Trigger(const T1& param1, const T1& param2, const T1& param3, const T1& param4)
	{
		for (auto it = listeningDatas.begin(); it != listeningDatas.end(); ++it)
		{
			auto& listener = *it;
			listener.callbackFunction(param1, param2, param3, param4);
		}
	}

	template<typename T1, typename T2, typename T3, typename T4, typename T5>
	void Trigger(const T1& param1, const T1& param2, const T1& param3, const T1& param4, const T1& param5)
	{
		for (auto it = listeningDatas.begin(); it != listeningDatas.end(); ++it)
		{
			auto& listener = *it;
			listener.callbackFunction(param1, param2, param3, param4, param5);
		}
	}
};
