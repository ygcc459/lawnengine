#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "D3D11RHI.h"

#include "D3D11.h"
#include "D3D11RHIInputAssembler.h"
#include "D3D11RHIShaderCompiler.h"
#include "../RenderSystem/RenderSystem.h"

size_t RHI::getDXGIFormatSize(DXGI_FORMAT f)
{
	static const size_t sizes[] = {
		0xFF, //DXGI_FORMAT_UNKNOWN	                 = 0,
		16, //DXGI_FORMAT_R32G32B32A32_TYPELESS       = 1,
		16, //DXGI_FORMAT_R32G32B32A32_FLOAT          = 2,
		16, //DXGI_FORMAT_R32G32B32A32_UINT           = 3,
		16, //DXGI_FORMAT_R32G32B32A32_SINT           = 4,
		12, //DXGI_FORMAT_R32G32B32_TYPELESS          = 5,
		12, //DXGI_FORMAT_R32G32B32_FLOAT             = 6,
		12, //DXGI_FORMAT_R32G32B32_UINT              = 7,
		12, //DXGI_FORMAT_R32G32B32_SINT              = 8,
		8, //DXGI_FORMAT_R16G16B16A16_TYPELESS       = 9,
		8, //DXGI_FORMAT_R16G16B16A16_FLOAT          = 10,
		8, //DXGI_FORMAT_R16G16B16A16_UNORM          = 11,
		8, //DXGI_FORMAT_R16G16B16A16_UINT           = 12,
		8, //DXGI_FORMAT_R16G16B16A16_SNORM          = 13,
		8, //DXGI_FORMAT_R16G16B16A16_SINT           = 14,
		8, //DXGI_FORMAT_R32G32_TYPELESS             = 15,
		8, //DXGI_FORMAT_R32G32_FLOAT                = 16,
		8, //DXGI_FORMAT_R32G32_UINT                 = 17,
		8, //DXGI_FORMAT_R32G32_SINT                 = 18,
		8, //DXGI_FORMAT_R32G8X24_TYPELESS           = 19,
		8, //DXGI_FORMAT_D32_FLOAT_S8X24_UINT        = 20,
		8, //DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS    = 21,
		8, //DXGI_FORMAT_X32_TYPELESS_G8X24_UINT     = 22,
		4, //DXGI_FORMAT_R10G10B10A2_TYPELESS        = 23,
		4, //DXGI_FORMAT_R10G10B10A2_UNORM           = 24,
		4, //DXGI_FORMAT_R10G10B10A2_UINT            = 25,
		4, //DXGI_FORMAT_R11G11B10_FLOAT             = 26,
		4, //DXGI_FORMAT_R8G8B8A8_TYPELESS           = 27,
		4, //DXGI_FORMAT_R8G8B8A8_UNORM              = 28,
		4, //DXGI_FORMAT_R8G8B8A8_UNORM_SRGB         = 29,
		4, //DXGI_FORMAT_R8G8B8A8_UINT               = 30,
		4, //DXGI_FORMAT_R8G8B8A8_SNORM              = 31,
		4, //DXGI_FORMAT_R8G8B8A8_SINT               = 32,
		4, //DXGI_FORMAT_R16G16_TYPELESS             = 33,
		4, //DXGI_FORMAT_R16G16_FLOAT                = 34,
		4, //DXGI_FORMAT_R16G16_UNORM                = 35,
		4, //DXGI_FORMAT_R16G16_UINT                 = 36,
		4, //DXGI_FORMAT_R16G16_SNORM                = 37,
		4, //DXGI_FORMAT_R16G16_SINT                 = 38,
		4, //DXGI_FORMAT_R32_TYPELESS                = 39,
		4, //DXGI_FORMAT_D32_FLOAT                   = 40,
		4, //DXGI_FORMAT_R32_FLOAT                   = 41,
		4, //DXGI_FORMAT_R32_UINT                    = 42,
		4, //DXGI_FORMAT_R32_SINT                    = 43,
		4, //DXGI_FORMAT_R24G8_TYPELESS              = 44,
		4, //DXGI_FORMAT_D24_UNORM_S8_UINT           = 45,
		4, //DXGI_FORMAT_R24_UNORM_X8_TYPELESS       = 46,
		4, //DXGI_FORMAT_X24_TYPELESS_G8_UINT        = 47,
		2, //DXGI_FORMAT_R8G8_TYPELESS               = 48,
		2, //DXGI_FORMAT_R8G8_UNORM                  = 49,
		2, //DXGI_FORMAT_R8G8_UINT                   = 50,
		2, //DXGI_FORMAT_R8G8_SNORM                  = 51,
		2, //DXGI_FORMAT_R8G8_SINT                   = 52,
		2, //DXGI_FORMAT_R16_TYPELESS                = 53,
		2, //DXGI_FORMAT_R16_FLOAT                   = 54,
		2, //DXGI_FORMAT_D16_UNORM                   = 55,
		2, //DXGI_FORMAT_R16_UNORM                   = 56,
		2, //DXGI_FORMAT_R16_UINT                    = 57,
		2, //DXGI_FORMAT_R16_SNORM                   = 58,
		2, //DXGI_FORMAT_R16_SINT                    = 59,
		1, //DXGI_FORMAT_R8_TYPELESS                 = 60,
		1, //DXGI_FORMAT_R8_UNORM                    = 61,
		1, //DXGI_FORMAT_R8_UINT                     = 62,
		1, //DXGI_FORMAT_R8_SNORM                    = 63,
		1, //DXGI_FORMAT_R8_SINT                     = 64,
		1, //DXGI_FORMAT_A8_UNORM                    = 65,
		0xFF, //DXGI_FORMAT_R1_UNORM                    = 66,
		4, //DXGI_FORMAT_R9G9B9E5_SHAREDEXP          = 67,
		4, //DXGI_FORMAT_R8G8_B8G8_UNORM             = 68,
		4, //DXGI_FORMAT_G8R8_G8B8_UNORM             = 69,
		0xFF, //DXGI_FORMAT_BC1_TYPELESS                = 70,
		0xFF, //DXGI_FORMAT_BC1_UNORM                   = 71,
		0xFF, //DXGI_FORMAT_BC1_UNORM_SRGB              = 72,
		0xFF, //DXGI_FORMAT_BC2_TYPELESS                = 73,
		0xFF, //DXGI_FORMAT_BC2_UNORM                   = 74,
		0xFF, //DXGI_FORMAT_BC2_UNORM_SRGB              = 75,
		0xFF, //DXGI_FORMAT_BC3_TYPELESS                = 76,
		0xFF, //DXGI_FORMAT_BC3_UNORM                   = 77,
		0xFF, //DXGI_FORMAT_BC3_UNORM_SRGB              = 78,
		0xFF, //DXGI_FORMAT_BC4_TYPELESS                = 79,
		0xFF, //DXGI_FORMAT_BC4_UNORM                   = 80,
		0xFF, //DXGI_FORMAT_BC4_SNORM                   = 81,
		0xFF, //DXGI_FORMAT_BC5_TYPELESS                = 82,
		0xFF, //DXGI_FORMAT_BC5_UNORM                   = 83,
		0xFF, //DXGI_FORMAT_BC5_SNORM                   = 84,
		2, //DXGI_FORMAT_B5G6R5_UNORM                = 85,
		2, //DXGI_FORMAT_B5G5R5A1_UNORM              = 86,
		4, //DXGI_FORMAT_B8G8R8A8_UNORM              = 87,
		4, //DXGI_FORMAT_B8G8R8X8_UNORM              = 88,
		4, //DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM  = 89,
		4, //DXGI_FORMAT_B8G8R8A8_TYPELESS           = 90,
		4, //DXGI_FORMAT_B8G8R8A8_UNORM_SRGB         = 91,
		4, //DXGI_FORMAT_B8G8R8X8_TYPELESS           = 92,
		4, //DXGI_FORMAT_B8G8R8X8_UNORM_SRGB         = 93,
		0xFF, //DXGI_FORMAT_BC6H_TYPELESS               = 94,
		0xFF, //DXGI_FORMAT_BC6H_UF16                   = 95,
		0xFF, //DXGI_FORMAT_BC6H_SF16                   = 96,
		0xFF, //DXGI_FORMAT_BC7_TYPELESS                = 97,
		0xFF, //DXGI_FORMAT_BC7_UNORM                   = 98,
		0xFF, //DXGI_FORMAT_BC7_UNORM_SRGB              = 99,
	};
	return sizes[f];
}

DXGI_FORMAT RHI::GetNativeTextureFormat(PixelFormat v, TextureFlags flags)
{
	switch (v)
	{
	case PF_R8G8B8A8:
		if (flags & TF_SRGB)
			return DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
		else
			return DXGI_FORMAT_R8G8B8A8_UNORM;
	case PF_R8:
		return DXGI_FORMAT_R8_UNORM;
	case PF_R16G16B16A16_Float:
		return DXGI_FORMAT_R16G16B16A16_FLOAT;
	case PF_R32G32B32A32_Float:
		return DXGI_FORMAT_R32G32B32A32_FLOAT;
	case PF_Depth32:
		return DXGI_FORMAT_R32_TYPELESS;
	case PF_Depth24Stencil8:
		return DXGI_FORMAT_R24G8_TYPELESS;
	default:
		CHECK(false && "PixelFormat not supported");
		return DXGI_FORMAT_UNKNOWN;
	}
}

DXGI_FORMAT RHI::GetNativeShaderResourceViewFormat(DXGI_FORMAT textureFormat, TextureFlags flags)
{
	if (flags & TF_SRGB)
	{
		switch (textureFormat)
		{
			case DXGI_FORMAT_B8G8R8A8_TYPELESS:    return DXGI_FORMAT_B8G8R8A8_UNORM_SRGB;
			case DXGI_FORMAT_R8G8B8A8_TYPELESS:    return DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
			case DXGI_FORMAT_BC1_TYPELESS:         return DXGI_FORMAT_BC1_UNORM_SRGB;
			case DXGI_FORMAT_BC2_TYPELESS:         return DXGI_FORMAT_BC2_UNORM_SRGB;
			case DXGI_FORMAT_BC3_TYPELESS:         return DXGI_FORMAT_BC3_UNORM_SRGB;
			case DXGI_FORMAT_BC7_TYPELESS:         return DXGI_FORMAT_BC7_UNORM_SRGB;
		};
	}
	else
	{
		switch (textureFormat)
		{
			case DXGI_FORMAT_B8G8R8A8_TYPELESS: return DXGI_FORMAT_B8G8R8A8_UNORM;
			case DXGI_FORMAT_R8G8B8A8_TYPELESS: return DXGI_FORMAT_R8G8B8A8_UNORM;
			case DXGI_FORMAT_BC1_TYPELESS:      return DXGI_FORMAT_BC1_UNORM;
			case DXGI_FORMAT_BC2_TYPELESS:      return DXGI_FORMAT_BC2_UNORM;
			case DXGI_FORMAT_BC3_TYPELESS:      return DXGI_FORMAT_BC3_UNORM;
			case DXGI_FORMAT_BC7_TYPELESS:      return DXGI_FORMAT_BC7_UNORM;
		};
	}

	switch (textureFormat)
	{
		case DXGI_FORMAT_R24G8_TYPELESS:		return DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
		case DXGI_FORMAT_R32_TYPELESS:			return DXGI_FORMAT_R32_FLOAT;
		case DXGI_FORMAT_R16_TYPELESS:			return DXGI_FORMAT_R16_UNORM;
	}

	return textureFormat;
}

DXGI_FORMAT RHI::GetNativeRenderTargetViewFormat(DXGI_FORMAT textureFormat, TextureFlags flags)
{
	switch (textureFormat)
	{
	case DXGI_FORMAT_R24G8_TYPELESS:		return DXGI_FORMAT_D24_UNORM_S8_UINT;
	case DXGI_FORMAT_R32_TYPELESS:			return DXGI_FORMAT_D32_FLOAT;
	}

	return GetNativeShaderResourceViewFormat(textureFormat, flags);
}

D3D11_USAGE RHI::GetNativeUsage(TextureFlags flags)
{
	if (flags & TF_Dynamic)
		return D3D11_USAGE_DYNAMIC;
	else
		return D3D11_USAGE_DEFAULT;
}

D3D11_BIND_FLAG RHI::GetNativeBindFlag(TextureFlags flags)
{
	UINT result = 0;

	if (flags & TF_ShaderResource)
		result |= D3D11_BIND_SHADER_RESOURCE;

	if (flags & TF_RenderTarget)
		result |= D3D11_BIND_RENDER_TARGET;

	if (flags & TF_DepthStencil)
		result |= D3D11_BIND_DEPTH_STENCIL;

	return (D3D11_BIND_FLAG)result;
}

D3D11_CPU_ACCESS_FLAG RHI::GetCpuAccessFlag(TextureFlags flags)
{
	UINT result = 0;

	if (flags & TF_CPUReadable)
		result |= D3D11_CPU_ACCESS_READ;

	if (flags & TF_CPUWritable)
		result |= D3D11_CPU_ACCESS_WRITE;

	return (D3D11_CPU_ACCESS_FLAG)result;
}

DXGI_FORMAT RHI::MeshVertexFormatToRhiVertexFormat(MeshVertexFormat format)
{
	switch (format.baseFormat)
	{
		case MVBF_Float:
		{
			switch (format.count)
			{
				case 1: return DXGI_FORMAT_R32_FLOAT;
				case 2: return DXGI_FORMAT_R32G32_FLOAT;
				case 3: return DXGI_FORMAT_R32G32B32_FLOAT;
				case 4: return DXGI_FORMAT_R32G32B32A32_FLOAT;
			}
			break;
		}
		case MVBF_Byte:
		{
			switch (format.count)
			{
				case 1: return DXGI_FORMAT_R8_UNORM;
				case 4: return DXGI_FORMAT_R8G8B8A8_UNORM;
			}
			break;
		}
	}

	return DXGI_FORMAT_UNKNOWN;
}

dxPrimitiveTopology RHI::ToNativeType(PrimitiveTopology v)
{
	static dxPrimitiveTopology lookupTable[] = 
	{
		D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED,  //PT_Unknown
		D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,  //PT_PointList
		D3D11_PRIMITIVE_TOPOLOGY_LINELIST,  //PT_LineList
		D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP,  //PT_LineStrip
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,  //PT_TriangleList
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP,  //PT_TriangleStrip
	};

	CHECK((int)v < sizeof(lookupTable) / sizeof(lookupTable[0]));

	return lookupTable[(int)v];
}

D3D11_CULL_MODE RHI::ToNativeType(CullMode v)
{
	static D3D11_CULL_MODE lookupTable[] =
	{
		D3D11_CULL_NONE,  //CullMode_None
		D3D11_CULL_FRONT,  //CullMode_Front
		D3D11_CULL_BACK,  //CullMode_Back
	};

	CHECK((int)v < sizeof(lookupTable) / sizeof(lookupTable[0]));

	return lookupTable[(int)v];
}

void RHI::ToNativeType(RHIViewport v, dxViewport& result)
{
	result.TopLeftX = (float)v.x;
	result.TopLeftY = (float)v.y;
	result.Width = (float)v.width;
	result.Height = (float)v.height;
	result.MinDepth = v.min_depth;
	result.MaxDepth = v.max_depth;
}

UINT8 RHI::ToNativeRenderTargetWriteMask(RenderTargetWriteMask v)
{
	UINT8 result = 0;
	if (v & RTWM_RED)
		result |= D3D11_COLOR_WRITE_ENABLE_RED;
	if (v & RTWM_GREEN)
		result |= D3D11_COLOR_WRITE_ENABLE_GREEN;
	if (v & RTWM_BLUE)
		result |= D3D11_COLOR_WRITE_ENABLE_BLUE;
	if (v & RTWM_ALPHA)
		result |= D3D11_COLOR_WRITE_ENABLE_ALPHA;
	return result;
}

D3D11_COMPARISON_FUNC RHI::ToNativeType(DepthTestMethod v)
{
	static D3D11_COMPARISON_FUNC lookupTable[] = 
	{
		D3D11_COMPARISON_NEVER,  //DTM_Never
		D3D11_COMPARISON_LESS,  //DTM_Less
		D3D11_COMPARISON_EQUAL,  //DTM_Equal
		D3D11_COMPARISON_LESS_EQUAL,  //DTM_LessEqual
		D3D11_COMPARISON_GREATER,  //DTM_Greater
		D3D11_COMPARISON_NOT_EQUAL,  //DTM_NotEqual
		D3D11_COMPARISON_GREATER_EQUAL,  //DTM_GreaterEqual
		D3D11_COMPARISON_ALWAYS,  //DTM_Always
	};

	CHECK((int)v < sizeof(lookupTable) / sizeof(lookupTable[0]));

	return lookupTable[(int)v];
}

D3D11_BLEND_OP RHI::ToNativeType(BlendOperation v)
{
	static D3D11_BLEND_OP lookupTable[] = 
	{
		D3D11_BLEND_OP_ADD,  //BO_Add
		D3D11_BLEND_OP_SUBTRACT,  //BO_Sub
		D3D11_BLEND_OP_MIN,  //BO_Min
		D3D11_BLEND_OP_MAX,  //BO_Max
	};

	CHECK((int)v < sizeof(lookupTable) / sizeof(lookupTable[0]));

	return lookupTable[(int)v];
}

D3D11_BLEND RHI::ToNativeType(BlendMultiplier v)
{
	static D3D11_BLEND lookupTable[] = 
	{
		D3D11_BLEND_ZERO,  //BM_Zero
		D3D11_BLEND_ONE,  //BM_One
		D3D11_BLEND_SRC_COLOR,  //BM_SrcColor
		D3D11_BLEND_INV_SRC_COLOR,  //BM_InvSrcColor
		D3D11_BLEND_SRC_ALPHA,  //BM_SrcAlpha
		D3D11_BLEND_INV_SRC_ALPHA,  //BM_InvSrcAlpha
		D3D11_BLEND_DEST_COLOR,  //BM_DstColor
		D3D11_BLEND_INV_DEST_COLOR,  //BM_InvDstColor
		D3D11_BLEND_DEST_ALPHA,  //BM_DstAlpha
		D3D11_BLEND_INV_DEST_ALPHA,  //BM_InvDstAlpha
		D3D11_BLEND_BLEND_FACTOR,  //BM_BlendFactor
		D3D11_BLEND_INV_BLEND_FACTOR,  //BM_InvBlendFactor
	};

	CHECK((int)v < sizeof(lookupTable) / sizeof(lookupTable[0]));

	return lookupTable[(int)v];
}

D3D11_USAGE BufferUsageToNative(BufferUsage v)
{
	static_assert(BU_DEFAULT == D3D11_USAGE_DEFAULT);
	static_assert(BU_IMMUTABLE == D3D11_USAGE_IMMUTABLE);
	static_assert(BU_DYNAMIC == D3D11_USAGE_DYNAMIC);
	static_assert(BU_STAGING == D3D11_USAGE_STAGING);

	return (D3D11_USAGE)v;
};

D3D11_BIND_FLAG BufferBindFlagToNative(BufferBindFlag v)
{
	static_assert(BBF_VERTEX_BUFFER == D3D11_BIND_VERTEX_BUFFER);
	static_assert(BBF_INDEX_BUFFER == D3D11_BIND_INDEX_BUFFER);
	static_assert(BBF_CONSTANT_BUFFER == D3D11_BIND_CONSTANT_BUFFER);
	static_assert(BBF_SHADER_RESOURCE == D3D11_BIND_SHADER_RESOURCE);
	static_assert(BBF_STREAM_OUTPUT == D3D11_BIND_STREAM_OUTPUT);
	static_assert(BBF_RENDER_TARGET == D3D11_BIND_RENDER_TARGET);
	static_assert(BBF_DEPTH_STENCIL == D3D11_BIND_DEPTH_STENCIL);
	static_assert(BBF_UNORDERED_ACCESS == D3D11_BIND_UNORDERED_ACCESS);
	//static_assert(BBF_DECODER == D3D11_BIND_DECODER);
	//static_assert(BBF_VIDEO_ENCODER == D3D11_BIND_VIDEO_ENCODER);

	return (D3D11_BIND_FLAG)v;
};

D3D11_CPU_ACCESS_FLAG CpuAccessFlagToNative(CpuAccessFlag v)
{
	uint32 result = 0;

	if (v & CAF_READ)
		result |= D3D11_CPU_ACCESS_READ;
	if (v & CAF_WRITE)
		result |= D3D11_CPU_ACCESS_WRITE;

	return (D3D11_CPU_ACCESS_FLAG)result;
};

static D3D11RHIDevice* g_D3D11RHIDevice;

D3D11RHIDevice* RHI::CreateRHIDeviceD3D11()
{
	CHECK(g_D3D11RHIDevice == NULL);

	g_D3D11RHIDevice = new D3D11RHIDevice();

	return g_D3D11RHIDevice;
}

D3D11RHIDevice* RHI::GetRHIDeviceD3D11()
{
	return g_D3D11RHIDevice;
}

D3D11RHIDevice::D3D11RHIDevice()
	: device(NULL)
	, deviceContext(NULL)
	, dxGIFactory(NULL)
	, lastVertexBuffersCount(0)
	, inputLayoutManager(nullptr)
{
}

D3D11RHIDevice::~D3D11RHIDevice()
{
}

bool D3D11RHIDevice::Startup()
{
	HRESULT hr;

	UINT createDeviceFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_3,
		D3D_FEATURE_LEVEL_9_2,
		D3D_FEATURE_LEVEL_9_1,
	};
	UINT numFeatureLevels = ARRAYSIZE(featureLevels);

	D3D_DRIVER_TYPE driverType = D3D_DRIVER_TYPE_HARDWARE;

	hr = D3D11CreateDevice(NULL, driverType, NULL, createDeviceFlags, featureLevels, numFeatureLevels,
		D3D11_SDK_VERSION, &device, &featureLevel, &deviceContext);
	if (FAILED(hr))
	{
		Log::Error("D3D11CreateDevice failed");
		return false;
	}

	hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&dxGIFactory);
	if (FAILED(hr))
	{
		Log::Error("CreateDXGIFactory failed");
		return false;
	}

	inputLayoutManager = new D3D11RHIInputLayoutManager();
	shaderCompiler = new D3D11RHIShaderCompiler();

	return true;
}

void D3D11RHIDevice::Shutdown()
{
	SAFE_DELETE(inputLayoutManager);
	SAFE_DELETE(shaderCompiler);

	if (deviceContext)
		deviceContext->ClearState();

	SAFE_RELEASE(dxGIFactory);

	SAFE_RELEASE(deviceContext);

#if defined(DEBUG) || defined(_DEBUG)
	ID3D11Debug* d3dDebug = NULL;
	HRESULT hr = device->QueryInterface(__uuidof(ID3D11Debug), reinterpret_cast<void**>(&d3dDebug));

	SAFE_RELEASE(device);

	if (SUCCEEDED(hr))
		hr = d3dDebug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);

	SAFE_RELEASE(d3dDebug);
#else
	SAFE_RELEASE(device);
#endif
}

RHIDisplaySurfacePtr D3D11RHIDevice::CreateDisplaySurface(void* windowHandle, int w, int h, PixelFormat format)
{
	HRESULT hr;

	HWND hwnd = (HWND)windowHandle;

	DXGI_FORMAT nativeTextureFormat = RHI::GetNativeTextureFormat(format, TextureFlags::TF_None);
	DXGI_FORMAT renderTargetFormat = RHI::GetNativeRenderTargetViewFormat(nativeTextureFormat, TextureFlags::TF_None);

	DXGI_SWAP_CHAIN_DESC sd;
	{
		ZeroMemory(&sd, sizeof(sd));
		sd.BufferCount = 1;
		sd.BufferDesc.Width = w;
		sd.BufferDesc.Height = h;
		sd.BufferDesc.Format = nativeTextureFormat;
		sd.BufferDesc.RefreshRate.Numerator = 60;
		sd.BufferDesc.RefreshRate.Denominator = 1;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.OutputWindow = hwnd;
		sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
		sd.Windowed = TRUE;
	}

	dxSwapChain swapChain;
	hr = dxGIFactory->CreateSwapChain(device, &sd, &swapChain);
	if (FAILED(hr))
	{
		Log::Error("D3D11RHIDevice::CreateDisplaySurface failed");
		return nullptr;
	}

	dxTexture2D texture;
	if (FAILED(swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&texture)))
	{
		Log::Error("failed to get ID3D11Texture2D from swap chain when creating RenderView");
		return nullptr;
	}

	dxRenderTargetView renderTargetView;
	if (FAILED(device->CreateRenderTargetView(texture, NULL, &renderTargetView)))
	{
		Log::Error("failed to CreateRenderTargetView for ID3D11Texture2D when creating RenderView");
		return nullptr;
	}

	TextureDesc textureDesc;
	textureDesc.type = TT_Texture2D;
	textureDesc.width = w;
	textureDesc.height = h;
	textureDesc.depth = 1;
	textureDesc.mipmaps = 1;
	textureDesc.arraySize = 1;
	textureDesc.format = format;
	textureDesc.multiSampleCount = 1;
	textureDesc.multiSampleQuality = 0;
	textureDesc.flags = TF_RenderTarget;

	D3D11RHIDisplaySurfacePtr ptr(new D3D11RHIDisplaySurface());
	ptr->width = w;
	ptr->height = h;
	ptr->nativeObject = swapChain;
	ptr->texture.reset(new D3D11RHITexture2D());
	ptr->texture->desc = textureDesc;
	ptr->texture->nativeTexture2D = texture;
	ptr->texture->nativeRenderTarget = renderTargetView;

	return ptr.StaticCast<RHIDisplaySurface>();
}

void D3D11RHIDevice::UpdateDisplaySurface(RHIDisplaySurfacePtr p, int w, int h)
{
	auto ptr = p.StaticCast<D3D11RHIDisplaySurface>();

	deviceContext->ClearState();

	SAFE_RELEASE(ptr->texture->nativeRenderTarget);
	SAFE_RELEASE(ptr->texture->nativeTexture2D);

	DXGI_SWAP_CHAIN_DESC desc;
	ptr->nativeObject->GetDesc(&desc);

	ptr->nativeObject->ResizeBuffers(desc.BufferCount, w, h, desc.BufferDesc.Format, 0);

	dxTexture2D texture;
	if (FAILED(ptr->nativeObject->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&texture)))
	{
		Log::Error("failed to get ID3D11Texture2D from swap chain when creating RenderView");
		return;
	}

	dxRenderTargetView renderTargetView;
	if (FAILED(device->CreateRenderTargetView(texture, NULL, &renderTargetView)))
	{
		Log::Error("failed to CreateRenderTargetView for ID3D11Texture2D when creating RenderView");
		return;
	}

	ptr->texture->nativeTexture2D = texture;
	ptr->texture->nativeRenderTarget = renderTargetView;

	ptr->width = w;
	ptr->height = h;
}

void D3D11RHIDevice::ReleaseDisplaySurface(RHIDisplaySurfacePtr p)
{
	if (p)
	{
		auto ptr = p.StaticCast<D3D11RHIDisplaySurface>();

		SAFE_RELEASE(ptr->nativeObject);

		SAFE_RELEASE(ptr->texture->nativeRenderTarget);
		SAFE_RELEASE(ptr->texture->nativeTexture2D);
	}
}

void D3D11RHIDevice::PresentDisplaySurface(RHIDisplaySurfacePtr p)
{
	auto ptr = p.StaticCast<D3D11RHIDisplaySurface>();

	if (ptr->nativeObject)
	{
		ptr->nativeObject->Present(0, 0);
	}
}

RHISamplerPtr D3D11RHIDevice::CreateSampler(const SamplerDesc& desc)
{
	D3D11_SAMPLER_DESC rhiDesc;
	memset(&rhiDesc, 0, sizeof(rhiDesc));

	if (desc.filter == SF_Point)
		rhiDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	else if (desc.filter == SF_Bilinear)
		rhiDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	else
		CHECK(false && "not supported");

	if (desc.addressing == SA_Clamp)
	{
		rhiDesc.AddressU = D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_CLAMP;
		rhiDesc.AddressV = D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_CLAMP;
		rhiDesc.AddressW = D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_CLAMP;
	}
	else if (desc.addressing == SA_Repeat)
	{
		rhiDesc.AddressU = D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_WRAP;
		rhiDesc.AddressV = D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_WRAP;
		rhiDesc.AddressW = D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_WRAP;
	}
	else
	{
		CHECK(false && "not supported");
	}

	dxSamplerState rhiSampler;
	if (FAILED(device->CreateSamplerState(&rhiDesc, &rhiSampler)))
	{
		Log::Error("CreateSamplerState failed");
		return {};
	}

	D3D11RHISamplerPtr ptr(new D3D11RHISampler());
	ptr->desc = desc;
	ptr->nativeObject = rhiSampler;
	return ptr.StaticCast<RHISampler>();
}

void D3D11RHIDevice::ReleaseSampler(RHISamplerPtr p)
{
	auto ptr = p.StaticCast<D3D11RHISampler>();
	if (ptr)
	{
		SAFE_RELEASE(ptr->nativeObject);
	}
}

RHIBindingLayoutPtr D3D11RHIDevice::CreateBindingLayout(const BindingLayoutDesc& desc)
{
	D3D11RHIBindingLayoutPtr ptr(new D3D11RHIBindingLayout());
	ptr->desc = desc;

	return ptr.StaticCast<RHIBindingLayout>();
}

RHIBindingSetPtr D3D11RHIDevice::CreateBindingSet(const BindingSetDesc& desc)
{
	D3D11RHIBindingSetPtr ptr(new D3D11RHIBindingSet());
	ptr->desc = desc;

	RHIBindingLayoutPtr layout = desc.layout;
	const BindingLayoutDesc& layoutDesc = layout->desc;

	uint32 targetShaders = (uint32)layoutDesc.targetShaders;
	ptr->targetShaders = layoutDesc.targetShaders;

	const std::vector<BindingSetItem>& setItems = desc.items;

	for (uint32 iShaderType = 0; iShaderType < (uint32)GraphicsShaderType::Count; iShaderType++)  //for each shader-type
	{
		uint32 curShaderTypeMask = (((uint32)ShaderTypeMasks::Vertex) << iShaderType);
		if (targetShaders & curShaderTypeMask)
		{
			D3D11RHIBindingSet::PerShaderBindingData& perShaderBindingData = ptr->perShaderBindingDatas[iShaderType];

			for (size_t iSetItem = 0; iSetItem < setItems.size(); iSetItem++)  //collect each binding item
			{
				const BindingSetItem& setItem = setItems[iSetItem];

				switch (setItem.type)
				{
				case ResourceType::Texture_SRV:
				{
					D3D11RHITexturePtr texture = setItem.resource.ReinterpretCast<D3D11RHITexture>();
					perShaderBindingData.boundSlotAndSRVs[perShaderBindingData.numSRVs] = std::make_pair(setItem.slot, texture->nativeShaderResource);
					perShaderBindingData.numSRVs++;
					break;
				}
				case ResourceType::ConstantBuffer:
				case ResourceType::VolatileConstantBuffer:
				{
					D3D11RHIBufferPtr buffer = setItem.resource.ReinterpretCast<D3D11RHIBuffer>();
					perShaderBindingData.boundSlotAndCBuffers[perShaderBindingData.numCBuffers] = std::make_pair(setItem.slot, buffer->nativeBuffer);
					perShaderBindingData.numCBuffers++;
					break;
				}
				case ResourceType::Sampler:
				{
					D3D11RHISamplerPtr sampler = setItem.resource.ReinterpretCast<D3D11RHISampler>();
					perShaderBindingData.boundSlotAndSamplers[perShaderBindingData.numSamplers] = std::make_pair(setItem.slot, sampler->nativeObject);
					perShaderBindingData.numSamplers++;
					break;
				}
				}
			}
		}
	}

	return ptr.StaticCast<RHIBindingSet>();
}

RHIGraphicsPipelinePtr D3D11RHIDevice::CreateGraphicsPipeline(const GraphicsPipelineDesc& desc)
{
	D3D11RHIGraphicsPipelinePtr ptr(new D3D11RHIGraphicsPipeline());
	ptr->desc = desc;

	ptr->primitiveTopology = RHI::ToNativeType(desc.primitiveTopology);

	RHIInputLayoutPtr inputLayout = inputLayoutManager->GetOrCreateInputLayout(desc.vertexComponents, desc.numVertexComponents, desc.shaders[(int)GraphicsShaderType::Vertex]);
	ptr->inputLayout = inputLayout.ReinterpretCast<D3D11RHIInputLayout>();

	for (uint32 iShaderType = 0; iShaderType < (uint32)GraphicsShaderType::Count; iShaderType++)
	{
		ptr->shaders[iShaderType] = desc.shaders[iShaderType].ReinterpretCast<D3D11RHIShader>();
	}

	const RHIRenderStateDesc& renderStateDesc = desc.renderStateDesc;

	//
	// blend state
	//
	D3D11_BLEND_DESC rhiBlendDesc;
	memset(&rhiBlendDesc, 0, sizeof(rhiBlendDesc));

	for (uint32 i = 0; i < RHI::MaxBoundRenderTargets; i++)
	{
		rhiBlendDesc.RenderTarget[i].BlendEnable = renderStateDesc.renderTargetBlendStates[i].blendEnable;
		rhiBlendDesc.RenderTarget[i].SrcBlend = RHI::ToNativeType(renderStateDesc.renderTargetBlendStates[i].srcBlend);
		rhiBlendDesc.RenderTarget[i].DestBlend = RHI::ToNativeType(renderStateDesc.renderTargetBlendStates[i].dstBlend);
		rhiBlendDesc.RenderTarget[i].BlendOp = RHI::ToNativeType(renderStateDesc.renderTargetBlendStates[i].blendOp);
		rhiBlendDesc.RenderTarget[i].SrcBlendAlpha = RHI::ToNativeType(renderStateDesc.renderTargetBlendStates[i].srcBlendAlpha);
		rhiBlendDesc.RenderTarget[i].DestBlendAlpha = RHI::ToNativeType(renderStateDesc.renderTargetBlendStates[i].dstBlendAlpha);
		rhiBlendDesc.RenderTarget[i].BlendOpAlpha = RHI::ToNativeType(renderStateDesc.renderTargetBlendStates[i].blendOpAlpha);
		rhiBlendDesc.RenderTarget[i].RenderTargetWriteMask = RHI::ToNativeRenderTargetWriteMask(renderStateDesc.renderTargetWriteMasks[i]);
	}

	if (FAILED(device->CreateBlendState(&rhiBlendDesc, &ptr->blendState)))
	{
		Log::Error("CreateBlendState failed");
		return nullptr;
	}

	//
	// depth-stencil state
	//
	D3D11_DEPTH_STENCIL_DESC rhiDepthStencilDesc;
	memset(&rhiDepthStencilDesc, 0, sizeof(rhiDepthStencilDesc));

	rhiDepthStencilDesc.DepthEnable = renderStateDesc.depthEnable;
	rhiDepthStencilDesc.DepthWriteMask = renderStateDesc.depthWrite ? D3D11_DEPTH_WRITE_MASK_ALL : D3D11_DEPTH_WRITE_MASK_ZERO;
	rhiDepthStencilDesc.DepthFunc = RHI::ToNativeType(renderStateDesc.depthTestMethod);
	rhiDepthStencilDesc.StencilEnable = false;
	//TODO: stencil support

	if (FAILED(device->CreateDepthStencilState(&rhiDepthStencilDesc, &ptr->depthStencilState)))
	{
		Log::Error("CreateDepthStencilState failed");
		return nullptr;
	}

	//
	// Rasterizer
	//
	D3D11_RASTERIZER_DESC rhiRasterizerDesc;
	memset(&rhiRasterizerDesc, 0, sizeof(rhiRasterizerDesc));

	rhiRasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rhiRasterizerDesc.CullMode = RHI::ToNativeType(renderStateDesc.cullMode);
	rhiRasterizerDesc.FrontCounterClockwise = renderStateDesc.frontIsCCW;
	rhiRasterizerDesc.DepthBias = renderStateDesc.depthBias;
	rhiRasterizerDesc.DepthBiasClamp = renderStateDesc.depthBiasClamp;
	rhiRasterizerDesc.SlopeScaledDepthBias = renderStateDesc.slopeScaledDepthBias;
	rhiRasterizerDesc.DepthClipEnable = renderStateDesc.depthClipEnable;
	rhiRasterizerDesc.ScissorEnable = renderStateDesc.scissorEnable;
	rhiRasterizerDesc.MultisampleEnable = renderStateDesc.multisampleEnable;
	rhiRasterizerDesc.AntialiasedLineEnable = renderStateDesc.antialiasedLineEnable;

	if (FAILED(device->CreateRasterizerState(&rhiRasterizerDesc, &ptr->rasterizerState)))
	{
		Log::Error("CreateRasterizerState failed");
		return nullptr;
	}

	return ptr.StaticCast<RHIGraphicsPipeline>();
}

void D3D11RHIDevice::ReleaseGraphicsPipeline(RHIGraphicsPipelinePtr p)
{
	auto ptr = p.StaticCast<D3D11RHIGraphicsPipeline>();
	if (ptr)
	{
		SAFE_RELEASE(ptr->blendState);
		SAFE_RELEASE(ptr->depthStencilState);
		SAFE_RELEASE(ptr->rasterizerState);
	}
}

RHIShaderCompiler* D3D11RHIDevice::GetShaderCompiler()
{
	return shaderCompiler;
}

RHICommandListPtr D3D11RHIDevice::CreateCommandList()
{
	dxDeviceContext deferredContext;
	if (FAILED(device->CreateDeferredContext(0, &deferredContext)))
	{
		Log::Error("CreateDeferredContext failed");
		return {};
	}

	D3D11RHICommandListPtr result(new D3D11RHICommandList(D3D11RHIDevicePtr(this), deferredContext));
	return result.StaticCast<RHICommandList>();
}

void D3D11RHIDevice::ReleaseCommandList(RHICommandListPtr p)
{
	D3D11RHICommandListPtr d3d11CommandList = p.StaticCast<D3D11RHICommandList>();
	SAFE_RELEASE(d3d11CommandList->d3d11DeferredContext);
	SAFE_RELEASE(d3d11CommandList->d3d11CommandList);
}

void D3D11RHIDevice::ExecuteCommandList(RHICommandListPtr p)
{
	D3D11RHICommandListPtr d3d11CommandList = p.StaticCast<D3D11RHICommandList>();
	if (d3d11CommandList->IsRecordingFinished())
	{
		deviceContext->ExecuteCommandList(d3d11CommandList->GetD3D11CommandList(), FALSE);
	}
	else
	{
		Log::Error("ExecuteCommandList failed, recording is not finished");
	}
}

RHIBufferPtr D3D11RHIDevice::CreateBuffer(const BufferDesc& desc, const void* data)
{
	D3D11RHIBufferPtr result(new D3D11RHIBuffer());

	if (CreateNativeBuffer(result.get(), desc, data))
		return result.StaticCast<RHIBuffer>();
	else
		return nullptr;
}

void D3D11RHIDevice::ReleaseBuffer(RHIBufferPtr p)
{
	auto ptr = p.StaticCast<D3D11RHIBuffer>();
	ReleaseNativeBuffer(ptr.get());
}

bool D3D11RHIDevice::CreateNativeBuffer(D3D11RHIBuffer* p, const BufferDesc& desc, const void* data /*= NULL*/)
{
	UINT byteSize = std::max(desc.size, 1);  //d3d can not create zero sized buffer

	D3D11_BUFFER_DESC rhiDesc;
	rhiDesc.ByteWidth = byteSize;
	rhiDesc.BindFlags = BufferBindFlagToNative(desc.binding);
	rhiDesc.Usage = BufferUsageToNative(desc.usage);
	rhiDesc.CPUAccessFlags = CpuAccessFlagToNative(desc.cpuAccess);
	rhiDesc.MiscFlags = 0;
	rhiDesc.StructureByteStride = 0;

	dxBuffer nativeBuffer;

	if (data)
	{
		D3D11_SUBRESOURCE_DATA rhiResData;
		rhiResData.pSysMem = data;
		rhiResData.SysMemPitch = 0;
		rhiResData.SysMemSlicePitch = 0;

		if (FAILED(device->CreateBuffer(&rhiDesc, &rhiResData, &nativeBuffer)))
		{
			Log::Error("CreateBuffer with size %d failed", desc.size);
			return false;
		}
	}
	else
	{
		if (FAILED(device->CreateBuffer(&rhiDesc, NULL, &nativeBuffer)))
		{
			Log::Error("CreateBuffer with size %d failed", desc.size);
			return false;
		}
	}

	p->desc = desc;
	p->nativeBuffer = nativeBuffer;
	return true;
}

void D3D11RHIDevice::ReleaseNativeBuffer(D3D11RHIBuffer* p)
{
	SAFE_RELEASE(p->nativeBuffer);
}

RHITexturePtr D3D11RHIDevice::CreateTexture(const TextureDesc& desc, const void* init_data /*= NULL*/)
{
	if (desc.type == TT_Texture2D)
	{
		D3D11RHITexture2DPtr result(new D3D11RHITexture2D());
		if (CreateNativeTexture2D(result.get(), desc, init_data))
			return result.StaticCast<RHITexture>();
		else
			return nullptr;
	}
	else if (desc.type == TT_Texture3D)
	{
		D3D11RHITexture3DPtr result(new D3D11RHITexture3D());
		if (CreateNativeTexture3D(result.get(), desc, init_data))
			return result.StaticCast<RHITexture>();
		else
			return nullptr;
	}
	else
	{
		CHECK(false && "Texture type not supported");
		return nullptr;
	}
}

void D3D11RHIDevice::ReleaseTexture(RHITexturePtr p)
{
	if (p)
	{
		if (p->desc.type == TT_Texture2D)
		{
			auto ptr = p.StaticCast<D3D11RHITexture2D>();
			ReleaseNativeTexture2D(ptr.get());
		}
		else if (p->desc.type == TT_Texture3D)
		{
			auto ptr = p.StaticCast<D3D11RHITexture3D>();
			ReleaseNativeTexture3D(ptr.get());
		}
		else
		{
			CHECK(false && "Texture type not supported");
		}
	}
}

bool D3D11RHIDevice::CreateNativeTexture2D(D3D11RHITexture2D* target, const TextureDesc& desc, const void* init_data /*= NULL*/)
{
	CHECK(desc.type == TT_Texture2D);

	dxTexture2D nativeTexture2D = nullptr;
	dxShaderResourceView nativeShaderResource = nullptr;
	dxRenderTargetView nativeRenderTarget = nullptr;
	dxDepthStencilView nativeDepthStencilTarget = nullptr;

	DXGI_FORMAT nativeTextureFormat = RHI::GetNativeTextureFormat(desc.format, desc.flags);

	D3D11_TEXTURE2D_DESC rhiDesc;
	rhiDesc.Width = desc.width;
	rhiDesc.Height = desc.height;
	rhiDesc.MipLevels = desc.mipmaps;
	rhiDesc.ArraySize = desc.arraySize;
	rhiDesc.Format = nativeTextureFormat;
	rhiDesc.SampleDesc.Count = desc.multiSampleCount;
	rhiDesc.SampleDesc.Quality = desc.multiSampleQuality;
	rhiDesc.Usage = RHI::GetNativeUsage(desc.flags);
	rhiDesc.BindFlags = RHI::GetNativeBindFlag(desc.flags);
	rhiDesc.CPUAccessFlags = RHI::GetCpuAccessFlag(desc.flags);
	rhiDesc.MiscFlags = 0;

	std::vector<D3D11_SUBRESOURCE_DATA> subResourceDatas;
	if (init_data)
	{
		uint8* Data = (uint8*)init_data;

		// each mip of each array slice counts as a subresource
		subResourceDatas.resize(desc.mipmaps * desc.arraySize);
		memset(subResourceDatas.data(), 0, sizeof(subResourceDatas[0]) * subResourceDatas.size());

		uint32 SliceOffset = 0;
		for (uint32 ArraySliceIndex = 0; ArraySliceIndex < desc.arraySize; ++ArraySliceIndex)
		{
			uint32 MipOffset = 0;
			for (uint32 MipIndex = 0; MipIndex < desc.mipmaps; ++MipIndex)
			{
				uint32 DataOffset = SliceOffset + MipOffset;
				uint32 SubResourceIndex = ArraySliceIndex * desc.mipmaps + MipIndex;

				uint32 NumBlocksX = Max(1, desc.width >> MipIndex);
				uint32 NumBlocksY = Max(1, desc.height >> MipIndex);

				subResourceDatas[SubResourceIndex].pSysMem = Data + DataOffset;
				subResourceDatas[SubResourceIndex].SysMemPitch = NumBlocksX * GetPixelByteSize(desc.format);
				subResourceDatas[SubResourceIndex].SysMemSlicePitch = NumBlocksX * NumBlocksY * subResourceDatas[MipIndex].SysMemPitch;

				MipOffset += NumBlocksY * subResourceDatas[MipIndex].SysMemPitch;
			}
			SliceOffset += MipOffset;
		}
	}

	if (FAILED(device->CreateTexture2D(&rhiDesc, (init_data ? subResourceDatas.data() : NULL), &nativeTexture2D)))
	{
		Log::Error("CreateTexture2D failed");
		return false;
	}

	if (desc.flags & TF_ShaderResource)
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		srvDesc.Format = RHI::GetNativeShaderResourceViewFormat(nativeTextureFormat, desc.flags);
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MipLevels = rhiDesc.MipLevels;
		srvDesc.Texture2D.MostDetailedMip = 0;

		if (FAILED(device->CreateShaderResourceView(nativeTexture2D, &srvDesc, &nativeShaderResource)))
		{
			Log::Error("CreateShaderResourceView failed");
			SAFE_RELEASE(nativeTexture2D);
			return false;
		}
	}

	if (desc.flags & TF_RenderTarget)
	{
		D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
		rtvDesc.Format = RHI::GetNativeRenderTargetViewFormat(nativeTextureFormat, desc.flags);
		rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		rtvDesc.Texture2D.MipSlice = 0;

		if (FAILED(device->CreateRenderTargetView(nativeTexture2D, &rtvDesc, &nativeRenderTarget)))
		{
			Log::Error("CreateRenderTargetView failed");
			SAFE_RELEASE(nativeTexture2D);
			SAFE_RELEASE(nativeShaderResource);
			return false;
		}
	}

	if (desc.flags & TF_DepthStencil)
	{
		D3D11_DEPTH_STENCIL_VIEW_DESC dsvDesc;
		dsvDesc.Format = RHI::GetNativeRenderTargetViewFormat(nativeTextureFormat, desc.flags);
		dsvDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		dsvDesc.Flags = 0;
		dsvDesc.Texture2D.MipSlice = 0;

		if (FAILED(device->CreateDepthStencilView(nativeTexture2D, &dsvDesc, &nativeDepthStencilTarget)))
		{
			Log::Error("CreateDepthStencilView failed");
			SAFE_RELEASE(nativeTexture2D);
			SAFE_RELEASE(nativeShaderResource);
			SAFE_RELEASE(nativeRenderTarget);
			return false;
		}
	}

	target->desc = desc;
	target->nativeTexture2D = nativeTexture2D;
	target->nativeShaderResource = nativeShaderResource;
	target->nativeRenderTarget = nativeRenderTarget;
	target->nativeDepthStencilTarget = nativeDepthStencilTarget;
	return true;
}

bool D3D11RHIDevice::CreateNativeTexture3D(D3D11RHITexture3D* target, const TextureDesc& desc, const void* init_data /*= NULL*/)
{
	CHECK(desc.type == TT_Texture3D);

	dxTexture3D nativeTexture3D = nullptr;
	dxShaderResourceView nativeShaderResource = nullptr;

	DXGI_FORMAT nativeTextureFormat = RHI::GetNativeTextureFormat(desc.format, desc.flags);

	D3D11_TEXTURE3D_DESC rhiDesc;
	rhiDesc.Width = desc.width;
	rhiDesc.Height = desc.height;
	rhiDesc.Depth = desc.depth;
	rhiDesc.MipLevels = desc.mipmaps;
	rhiDesc.Format = nativeTextureFormat;
	rhiDesc.Usage = RHI::GetNativeUsage(desc.flags);
	rhiDesc.BindFlags = RHI::GetNativeBindFlag(desc.flags);
	rhiDesc.CPUAccessFlags = RHI::GetCpuAccessFlag(desc.flags);
	rhiDesc.MiscFlags = 0;

	std::vector<D3D11_SUBRESOURCE_DATA> subResourceDatas;
	if (init_data)
	{
		uint8* Data = (uint8*)init_data;

		// each mip counts as a subresource
		subResourceDatas.resize(desc.mipmaps);
		memset(subResourceDatas.data(), 0, sizeof(subResourceDatas[0]) * subResourceDatas.size());

		uint32 MipOffset = 0;
		for (uint32 MipIndex = 0; MipIndex < desc.mipmaps; ++MipIndex)
		{
			subResourceDatas[MipIndex].pSysMem = Data + MipOffset;
			subResourceDatas[MipIndex].SysMemPitch = Max(1, desc.width >> MipIndex) * GetPixelByteSize(desc.format);
			subResourceDatas[MipIndex].SysMemSlicePitch = Max(1, desc.height >> MipIndex) * subResourceDatas[MipIndex].SysMemPitch;
			MipOffset += Max(1, desc.depth >> MipIndex) * subResourceDatas[MipIndex].SysMemSlicePitch;
		}
	}

	if (FAILED(device->CreateTexture3D(&rhiDesc, (init_data ? subResourceDatas.data() : NULL), &nativeTexture3D)))
	{
		Log::Error("CreateTexture3D failed");
		return false;
	}

	if (desc.flags & TF_ShaderResource)
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		srvDesc.Format = RHI::GetNativeShaderResourceViewFormat(nativeTextureFormat, desc.flags);
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MipLevels = rhiDesc.MipLevels;
		srvDesc.Texture2D.MostDetailedMip = 0;

		if (FAILED(device->CreateShaderResourceView(nativeTexture3D, &srvDesc, &nativeShaderResource)))
		{
			Log::Error("CreateShaderResourceView failed, when creating ColorRenderTarget");
			SAFE_RELEASE(nativeTexture3D);
			return false;
		}
	}
	
	target->desc = desc;
	target->nativeTexture3D = nativeTexture3D;
	target->nativeShaderResource = nativeShaderResource;
	return true;
}

void D3D11RHIDevice::ReleaseNativeTexture2D(D3D11RHITexture2D* target)
{
	SAFE_RELEASE(target->nativeShaderResource);

	SAFE_RELEASE(target->nativeTexture2D);
	SAFE_RELEASE(target->nativeRenderTarget);
	SAFE_RELEASE(target->nativeDepthStencilTarget);
}

void D3D11RHIDevice::ReleaseNativeTexture3D(D3D11RHITexture3D* target)
{
	SAFE_RELEASE(target->nativeShaderResource);

	SAFE_RELEASE(target->nativeTexture3D);
}

RHIFramebufferPtr D3D11RHIDevice::CreateFramebuffer(const FramebufferDesc& desc)
{
	D3D11RHIFramebufferPtr obj(new D3D11RHIFramebuffer());
	obj->desc = desc;

	for (int i = 0; i < RHI::MaxBoundRenderTargets; ++i)
	{
		const FramebufferAttachmentDesc& rtDesc = desc.colorAttachments[i];
		RHITexturePtr rhiTexPtr = rtDesc.texture;
		if (rhiTexPtr)
		{
			CHECK(rhiTexPtr->desc.type == TT_Texture2D);
			CHECK(rhiTexPtr->desc.flags & TF_RenderTarget);

			auto d3d11RhiTex2DPtr = rhiTexPtr.StaticCast<D3D11RHITexture2D>();
			obj->renderTargetViews[i] = d3d11RhiTex2DPtr->nativeRenderTarget;
		}
		else
		{
			obj->renderTargetViews[i] = nullptr;
		}
	}

	{
		RHITexturePtr rhiTexPtr = desc.depthAttachment.texture;
		if (rhiTexPtr)
		{
			CHECK(rhiTexPtr->desc.type == TT_Texture2D);
			CHECK(rhiTexPtr->desc.flags & TF_DepthStencil);

			auto d3d11RhiTex2DPtr = rhiTexPtr.StaticCast<D3D11RHITexture2D>();
			obj->depthStencilView = d3d11RhiTex2DPtr->nativeDepthStencilTarget;
		}
	}

	return obj.StaticCast<RHIFramebuffer>();
}

void D3D11RHIDevice::ReleaseFramebuffer(RHIFramebufferPtr p)
{
	D3D11RHIFramebufferPtr obj = p.ReinterpretCast<D3D11RHIFramebuffer>();

	for (int i = 0; i < RHI::MaxBoundRenderTargets; ++i)
	{
		obj->desc.colorAttachments[i].texture.reset();

		obj->renderTargetViews[i] = nullptr;
	}

	{
		obj->desc.depthAttachment.texture.reset();

		obj->depthStencilView = nullptr;
	}
}

void D3D11RHICommandList::ClearRenderTarget(RHITexturePtr p, const float4& color)
{
	CHECK(p->desc.type == TT_Texture2D);
	CHECK(p->desc.flags & TF_RenderTarget);

	auto ptr = p.StaticCast<D3D11RHITexture2D>();

	dxDeviceContext deviceContext = d3d11Device->NativeDeviceContext();
	deviceContext->ClearRenderTargetView(ptr->nativeRenderTarget, (const float*)color);
}

void D3D11RHICommandList::ClearDepthStencilTarget(RHITexturePtr p, float depth /*= 1.0f*/, UINT8 stencil /*= 0x00*/)
{
	CHECK(p->desc.type == TT_Texture2D);
	CHECK(p->desc.flags & TF_DepthStencil);

	auto ptr = p.StaticCast<D3D11RHITexture2D>();

	dxDeviceContext deviceContext = d3d11Device->NativeDeviceContext();
	deviceContext->ClearDepthStencilView(ptr->nativeDepthStencilTarget, D3D10_CLEAR_DEPTH | D3D10_CLEAR_STENCIL, depth, stencil);
}

bool D3D11RHICommandList::UpdateBuffer(RHIBufferPtr p, const BufferDesc& desc, void* data, size_t size)
{
	dxDeviceContext deviceContext = d3d11Device->NativeDeviceContext();

	D3D11RHIBufferPtr ptr = p.StaticCast<D3D11RHIBuffer>();

	if (desc != ptr->desc)
	{
		d3d11Device->ReleaseNativeBuffer(ptr.get());

		return d3d11Device->CreateNativeBuffer(ptr.get(), desc, data);
	}
	else if (data)
	{
		deviceContext->UpdateSubresource(ptr->nativeBuffer, 0, nullptr, data, (UINT)size, 0);

		return true;
	}

	return false;
}

bool D3D11RHICommandList::UpdateBufferData(RHIBufferPtr p, void* data, size_t size)
{
	D3D11RHIBufferPtr ptr = p.StaticCast<D3D11RHIBuffer>();

	dxDeviceContext deviceContext = d3d11Device->NativeDeviceContext();
	deviceContext->UpdateSubresource(ptr->nativeBuffer, 0, nullptr, data, (UINT)size, 0);

	return true;
}

bool D3D11RHICommandList::UpdateTexture(RHITexturePtr p, const TextureDesc& newDesc, void* newData)
{
	auto ptr = p.StaticCast<D3D11RHITexture>();

	TextureDesc& desc = ptr->desc;
	if (newDesc != desc)  //different format, need to recreate native object, and re-upload data
	{
		CHECK(desc.type == newDesc.type);

		if (p->desc.type == TT_Texture2D)
		{
			auto ptr = p.StaticCast<D3D11RHITexture2D>();

			d3d11Device->ReleaseNativeTexture2D(ptr.get());

			return d3d11Device->CreateNativeTexture2D(ptr.get(), newDesc, newData);
		}
		else if (p->desc.type == TT_Texture3D)
		{
			auto ptr = p.StaticCast<D3D11RHITexture3D>();

			d3d11Device->ReleaseNativeTexture3D(ptr.get());

			return d3d11Device->CreateNativeTexture3D(ptr.get(), newDesc, newData);
		}
		else
		{
			CHECK(false && "Texture type not supported");
		}
	}
	else if (newData)  //format not changed, just re-upload data
	{
		uint8* data = (uint8*)newData;

		ID3D11Resource* pDstResource = NULL;
		if (p->desc.type == TT_Texture2D)
			pDstResource = p.StaticCast<D3D11RHITexture2D>()->nativeTexture2D;
		else if (p->desc.type == TT_Texture3D)
			pDstResource = p.StaticCast<D3D11RHITexture3D>()->nativeTexture3D;
		else
		{
			CHECK(false && "Texture type not supported");
			return false;
		}

		dxDeviceContext deviceContext = d3d11Device->NativeDeviceContext();

		uint32 SliceOffset = 0;
		for (uint32 ArraySliceIndex = 0; ArraySliceIndex < p->desc.arraySize; ++ArraySliceIndex)
		{
			uint32 MipOffset = 0;
			for (uint32 MipIndex = 0; MipIndex < p->desc.mipmaps; ++MipIndex)
			{
				uint32 DataOffset = SliceOffset + MipOffset;
				uint32 SubResourceIndex = ArraySliceIndex * p->desc.mipmaps + MipIndex;

				uint32 NumBlocksX = Max(1, p->desc.width >> MipIndex);
				uint32 NumBlocksY = Max(1, p->desc.height >> MipIndex);

				const void* SrcData = data + DataOffset;
				uint32 RowPitch = NumBlocksX * GetPixelByteSize(p->desc.format);
				uint32 DepthPitch = NumBlocksY * RowPitch;

				deviceContext->UpdateSubresource(pDstResource, SubResourceIndex, nullptr, SrcData, RowPitch, DepthPitch);

				MipOffset += DepthPitch;
			}
			SliceOffset += MipOffset;
		}

		return true;
	}

	return false;
}

void D3D11RHICommandList::SetGraphicsState(const GraphicsState& state)
{
	dxDeviceContext deviceContext = d3d11Device->NativeDeviceContext();

	D3D11RHIGraphicsPipelinePtr pipeline = state.pipeline.StaticCast<D3D11RHIGraphicsPipeline>();

	//
	// pipeline states
	//
	UINT stencilRef = 0;
	deviceContext->OMSetDepthStencilState(pipeline->depthStencilState, stencilRef);

	const float* blendFactor = reinterpret_cast<const float*>(&state.blendConstantColor);
	UINT sampleMask = 0xFFFFFFFF;
	deviceContext->OMSetBlendState(pipeline->blendState, blendFactor, sampleMask);

	deviceContext->RSSetState(pipeline->rasterizerState);

	//
	// frame buffers
	//
	if (state.framebuffer != lastFramebuffer)
	{
		CHECK(state.framebuffer);

		if (state.framebuffer)
		{
			D3D11RHIFramebufferPtr framebuffer = state.framebuffer.ReinterpretCast<D3D11RHIFramebuffer>();
			deviceContext->OMSetRenderTargets(RHI::MaxBoundRenderTargets, framebuffer->renderTargetViews, framebuffer->depthStencilView);
		}

		lastFramebuffer = state.framebuffer;
	}

	//
	// viewports
	//
	if (state.viewport != lastViewport)
	{
		RHIViewportSet& viewportSet = *state.viewport;

		for (uint32 i = 0; i < RHI::MaxBoundRenderTargets; i++)
		{
			RHI::ToNativeType(viewportSet.viewports[i], tempNativeViewports[i]);
		}
		deviceContext->RSSetViewports(RHI::MaxBoundRenderTargets, tempNativeViewports);

		lastViewport = state.viewport;
	}

	//
	// shader & bindings
	//
	for (uint32 iShaderType = 0; iShaderType < (uint32)GraphicsShaderType::Count; iShaderType++)
	{
		perShaderNativeDatas[iShaderType].Reset();
	}
	
	for (uint32 iBindingSet = 0; iBindingSet < state.numBindingSets; iBindingSet++)  // for each binding-set
	{
		D3D11RHIBindingSetPtr bindingSet = state.bindingSets[iBindingSet].ReinterpretCast<D3D11RHIBindingSet>();
		CHECK(bindingSet);
		
		uint32 targetShaders = (uint32)bindingSet->targetShaders;
		for (int iShaderType = 0; iShaderType < (int)GraphicsShaderType::Count; iShaderType++)  //for each shader-type
		{
			uint32 curShaderTypeMask = (((uint32)ShaderTypeMasks::Vertex) << iShaderType);
			if (!(targetShaders & curShaderTypeMask))
				continue;

			PerShaderNativeData& dstBindingData = perShaderNativeDatas[iShaderType];

			auto& srcBindingData = bindingSet->perShaderBindingDatas[iShaderType];

			for (uint32 iBindingItem = 0; iBindingItem < srcBindingData.numSRVs; iBindingItem++)
			{
				auto& bindingItem = srcBindingData.boundSlotAndSRVs[iBindingItem];
				dstBindingData.shaderResourceViews[bindingItem.first] = bindingItem.second;
			}

			for (uint32 iBindingItem = 0; iBindingItem < srcBindingData.numCBuffers; iBindingItem++)
			{
				auto& bindingItem = srcBindingData.boundSlotAndCBuffers[iBindingItem];
				dstBindingData.constantBuffers[bindingItem.first] = bindingItem.second;
			}

			for (uint32 iBindingItem = 0; iBindingItem < srcBindingData.numSamplers; iBindingItem++)
			{
				auto& bindingItem = srcBindingData.boundSlotAndSamplers[iBindingItem];
				dstBindingData.samplers[bindingItem.first] = bindingItem.second;
			}
		}
	}

	if (pipeline->GetVertexShader())
	{
		PerShaderNativeData& bindingData = perShaderNativeDatas[(int)GraphicsShaderType::Vertex];

		deviceContext->VSSetShader(pipeline->GetVertexShader()->nativeVertexShader, NULL, 0);

		deviceContext->VSSetShaderResources(0, RHI::MaxBoundShaderResrouceViews, bindingData.shaderResourceViews);
		deviceContext->VSSetConstantBuffers(0, RHI::MaxBoundConstantBuffers, bindingData.constantBuffers);
		deviceContext->VSSetSamplers(0, RHI::MaxBoundSamplers, bindingData.samplers);
	}

	if (pipeline->GetPixelShader())
	{
		PerShaderNativeData& bindingData = perShaderNativeDatas[(int)GraphicsShaderType::Pixel];

		deviceContext->PSSetShader(pipeline->GetPixelShader()->nativePixelShader, NULL, 0);

		deviceContext->PSSetShaderResources(0, RHI::MaxBoundShaderResrouceViews, bindingData.shaderResourceViews);
		deviceContext->PSSetConstantBuffers(0, RHI::MaxBoundConstantBuffers, bindingData.constantBuffers);
		deviceContext->PSSetSamplers(0, RHI::MaxBoundSamplers, bindingData.samplers);
	}

	//
	// geometry
	//
	this->primitiveTopology = pipeline->primitiveTopology;
	deviceContext->IASetPrimitiveTopology(this->primitiveTopology);

	D3D11RHIInputLayoutPtr inputLayout = pipeline->inputLayout;
	deviceContext->IASetInputLayout(inputLayout->nativeObject);

	//
	// vertex buffers
	//
	memset(tempNativeBuffers, 0, sizeof(dxBuffer) * RHI::MaxVertexBufferBindings);
	memset(tempBufferStrides, 0, sizeof(uint32) * RHI::MaxVertexBufferBindings);
	memset(tempBufferOffsets, 0, sizeof(uint32) * RHI::MaxVertexBufferBindings);

	for (uint32 i = 0; i < state.numVertexBufferBindings; i++)
	{
		const VertexBufferBinding& vertexBufferBinding = state.vertexBufferBindings[i];
		uint32 slot = vertexBufferBinding.slot;
		tempNativeBuffers[slot] = vertexBufferBinding.buffer ? vertexBufferBinding.buffer.ReinterpretCast<D3D11RHIBuffer>()->nativeBuffer : nullptr;
		tempBufferStrides[slot] = vertexBufferBinding.stride;
		tempBufferOffsets[slot] = vertexBufferBinding.offset;
	}
	deviceContext->IASetVertexBuffers(0, RHI::MaxVertexBufferBindings, tempNativeBuffers, tempBufferStrides, tempBufferOffsets);

	//
	// index buffer
	//
	if (state.indexBufferBinding.buffer)
	{
		const IndexBufferBinding& indexBufferBinding = state.indexBufferBinding;

		D3D11RHIBufferPtr indexBuffer = indexBufferBinding.buffer.ReinterpretCast<D3D11RHIBuffer>();
		DXGI_FORMAT indexBufferFormat = indexBufferBinding.format == IndexFormat::IF_UINT16 ? DXGI_FORMAT::DXGI_FORMAT_R16_UINT : DXGI_FORMAT::DXGI_FORMAT_R32_UINT;
		deviceContext->IASetIndexBuffer(indexBuffer->nativeBuffer, indexBufferFormat, indexBufferBinding.offset);
	}
	else
	{
		deviceContext->IASetIndexBuffer(nullptr, DXGI_FORMAT::DXGI_FORMAT_R32_UINT, 0);
	}
}

void D3D11RHICommandList::Draw(const DrawArguments& arg)
{
	dxDeviceContext deviceContext = d3d11Device->NativeDeviceContext();

	if (arg.indexed)
	{
		//D3D_PRIMITIVE_TOPOLOGY_POINTLIST = 1,
		//D3D_PRIMITIVE_TOPOLOGY_LINELIST = 2,
		//D3D_PRIMITIVE_TOPOLOGY_LINESTRIP = 3,
		//D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST = 4,
		//D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP = 5,
		//D3D_PRIMITIVE_TOPOLOGY_LINELIST_ADJ = 10,
		//D3D_PRIMITIVE_TOPOLOGY_LINESTRIP_ADJ = 11,
		//D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST_ADJ = 12,
		//D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP_ADJ = 13,

		//uint32 indexCount;
		//switch (this->primitiveTopology)
		//{
		//case D3D_PRIMITIVE_TOPOLOGY_POINTLIST:
		//	indexCount = arg.vertexCount;
		//	break;
		//case D3D_PRIMITIVE_TOPOLOGY_LINELIST:
		//	indexCount = arg.vertexCount / 2;
		//	break;
		//case D3D_PRIMITIVE_TOPOLOGY_LINESTRIP:
		//	indexCount = arg.vertexCount;
		//	break;
		//case D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST:
		//	indexCount = arg.vertexCount;
		//	break;
		//case D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP:
		//	indexCount = arg.vertexCount;
		//	break;
		//default:
		//	assert(false);
		//	return;
		//}

		deviceContext->DrawIndexed(arg.vertexCount, arg.startIndexLocation, arg.startVertexLocation);
	}
	else
	{
		deviceContext->Draw(arg.vertexCount, arg.startVertexLocation);
	}
}

void D3D11RHICommandList::FinishRecording()
{
	if (FAILED(d3d11DeferredContext->FinishCommandList(FALSE, &d3d11CommandList)))
	{
		Log::Error("FinishCommandList failed");
	}
}

void D3D11RHICommandList::UnbindShaderResources()
{
	dxDeviceContext deviceContext = d3d11Device->NativeDeviceContext();

	static dxShaderResourceView shaderResourceViews[D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT] = { 0 };

	deviceContext->VSSetShaderResources(0, D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, shaderResourceViews);
	deviceContext->PSSetShaderResources(0, D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, shaderResourceViews);
}

void D3D11RHICommandList::PerShaderNativeData::Reset()
{
	for (uint32 i = 0; i < RHI::MaxBoundShaderResrouceViews; i++)
		shaderResourceViews[i] = nullptr;
	
	for (uint32 i = 0; i < RHI::MaxBoundConstantBuffers; i++)
		constantBuffers[i] = nullptr;

	for (uint32 i = 0; i < RHI::MaxBoundSamplers; i++)
		samplers[i] = nullptr;
}
