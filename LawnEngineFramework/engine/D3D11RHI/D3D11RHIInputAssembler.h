#pragma once

#include "D3D11RHI.h"
#include "RHI/RHIInputAssembler.h"

void InputElementToSemantics(InputElement t, dxInputElementDesc& dst);

class D3D11RHIInputLayoutManager : public RHIInputLayoutManager
{
public:
	D3D11RHIInputLayoutManager();
	virtual ~D3D11RHIInputLayoutManager();

protected:
	virtual RHIInputLayoutPtr CreateInputLayout(const MeshVertexComponent* vertexComponents, uint32 numVertexComponents, RHIShaderPtr vertexShader) override;
	
	virtual void ReleaseInputLayout(RHIInputLayoutPtr p) override;

};
