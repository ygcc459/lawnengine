#pragma once

#include "D3D11RHI/D3D11RHI.h"
#include "RHI/RHIShader.h"

class D3D11RHIShaderCompiler : public RHIShaderCompiler
{
public:
	D3D11RHIShaderCompiler()
	{}

	virtual ~D3D11RHIShaderCompiler()
	{}

	virtual RHIShaderPtr CompileShader(
		const AssetPath& shaderPath,
		const std::string& shaderName,
		const std::string& source,
		const std::string& entryPoint,
		const std::map<std::string, std::string>& macros,
		ShaderFrequency freq) override;

private:
	bool GenerateShaderReflection(
		const std::string& shaderName, 
		ShaderFrequency freq,
		const std::vector<uint8>& bytecode, 
		RHIShaderReflection& outShaderReflection);

	bool CompileNativeShader(const std::string& shaderName, ShaderFrequency freq, D3D11RHIShader& rhiShader);
};

