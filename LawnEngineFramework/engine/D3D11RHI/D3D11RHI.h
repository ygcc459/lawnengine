#pragma once

#include "RHI/RHIDevice.h"

#include <DXGI.h>
#include <d3d11.h>

typedef ID3D11Device* dxDevice;
typedef ID3D11DeviceContext* dxDeviceContext;
typedef ID3D11CommandList* dxCommandList;
typedef IDXGISwapChain* dxSwapChain;

typedef ID3DBlob* dxBlob;

typedef ID3D11RenderTargetView* dxRenderTargetView;
typedef ID3D11DepthStencilView* dxDepthStencilView;

typedef ID3D11InputLayout* dxInputLayout;

typedef ID3D11Resource* dxResource;

typedef D3D11_PRIMITIVE_TOPOLOGY dxPrimitiveTopology;
typedef D3D11_INPUT_ELEMENT_DESC dxInputElementDesc;
typedef D3D11_BIND_FLAG dxBindFlag;

typedef ID3D11Buffer* dxBuffer;

typedef D3D11_VIEWPORT dxViewport;

typedef ID3D11Texture1D* dxTexture1D;
typedef ID3D11Texture2D* dxTexture2D;
typedef ID3D11Texture3D* dxTexture3D;

typedef ID3D11SamplerState* dxSamplerState;
typedef ID3D11ShaderResourceView* dxShaderResourceView;

typedef ID3D11VertexShader* dxVertexShader;
typedef ID3D11PixelShader* dxPixelShader;

typedef ID3D11RasterizerState* dxRasterizerState;
typedef ID3D11DepthStencilState* dxDepthStencilState;
typedef ID3D11BlendState* dxBlendState;

typedef D3D11_BLEND_DESC dxBlendDesc;
typedef D3D11_DEPTH_STENCIL_DESC dxDepthStencilDesc;
typedef D3D11_RASTERIZER_DESC dxRasterizerDesc;

typedef ID3DInclude* dxInclude;

namespace RHI
{
	size_t getDXGIFormatSize(DXGI_FORMAT f);

	DXGI_FORMAT MeshVertexFormatToRhiVertexFormat(MeshVertexFormat format);

	dxPrimitiveTopology ToNativeType(PrimitiveTopology v);

	D3D11_BLEND ToNativeType(BlendMultiplier v);
	D3D11_BLEND_OP ToNativeType(BlendOperation v);
	UINT8 ToNativeRenderTargetWriteMask(RenderTargetWriteMask v);
	D3D11_COMPARISON_FUNC ToNativeType(DepthTestMethod v);
	D3D11_CULL_MODE ToNativeType(CullMode v);

	void ToNativeType(RHIViewport v, dxViewport& result);

	DXGI_FORMAT GetNativeTextureFormat(PixelFormat v, TextureFlags flags);
	DXGI_FORMAT GetNativeShaderResourceViewFormat(DXGI_FORMAT textureFormat, TextureFlags flags);
	DXGI_FORMAT GetNativeRenderTargetViewFormat(DXGI_FORMAT textureFormat, TextureFlags flags);
	D3D11_USAGE GetNativeUsage(TextureFlags flags);
	D3D11_BIND_FLAG GetNativeBindFlag(TextureFlags flags);
	D3D11_CPU_ACCESS_FLAG GetCpuAccessFlag(TextureFlags flags);

	const uint32 MaxBoundShaderResrouceViews = 128;
	static_assert(MaxBoundShaderResrouceViews <= D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT);
	 
	const uint32 MaxBoundConstantBuffers = 14;
	static_assert(MaxBoundConstantBuffers <= D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT);

	const uint32 MaxBoundSamplers = 16;
	static_assert(MaxBoundSamplers <= D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT);
}

class D3D11RHIBuffer : public RHIBuffer
{
public:
	D3D11RHIBuffer()
		:RHIBuffer(), nativeBuffer(NULL)
	{}

	virtual ~D3D11RHIBuffer()
	{
		SAFE_RELEASE(nativeBuffer);
	}

	dxBuffer nativeBuffer;
};

typedef RefCountedPtr<D3D11RHIBuffer> D3D11RHIBufferPtr;

class D3D11RHIInputLayout : public RHIInputLayout
{
public:
	dxInputLayout nativeObject;

	D3D11RHIInputLayout()
		: RHIInputLayout(), nativeObject(nullptr)
	{}

	virtual ~D3D11RHIInputLayout()
	{
		SAFE_RELEASE(nativeObject);
	}
};

typedef RefCountedPtr<D3D11RHIInputLayout> D3D11RHIInputLayoutPtr;

class D3D11RHITexture : public RHITexture
{
public:
	dxShaderResourceView nativeShaderResource;

	D3D11RHITexture()
		: RHITexture(), nativeShaderResource(nullptr)
	{}

	virtual ~D3D11RHITexture()
	{
		SAFE_RELEASE(nativeShaderResource);
	}
};

typedef RefCountedPtr<D3D11RHITexture> D3D11RHITexturePtr;

class D3D11RHITexture2D : public D3D11RHITexture
{
public:
	dxTexture2D nativeTexture2D;

	dxRenderTargetView nativeRenderTarget;

	dxDepthStencilView nativeDepthStencilTarget;

	D3D11RHITexture2D()
		: D3D11RHITexture()
		, nativeTexture2D(nullptr)
		, nativeRenderTarget(nullptr)
		, nativeDepthStencilTarget(nullptr)
	{}

	virtual ~D3D11RHITexture2D()
	{
		SAFE_RELEASE(nativeTexture2D);
		SAFE_RELEASE(nativeRenderTarget);
		SAFE_RELEASE(nativeDepthStencilTarget);
	}
};

typedef RefCountedPtr<D3D11RHITexture2D> D3D11RHITexture2DPtr;

class D3D11RHITexture3D : public D3D11RHITexture
{
public:
	dxTexture3D nativeTexture3D;

	D3D11RHITexture3D()
		: D3D11RHITexture()
		, nativeTexture3D(nullptr)
	{}

	virtual ~D3D11RHITexture3D()
	{
		SAFE_RELEASE(nativeTexture3D);
	}
};

typedef RefCountedPtr<D3D11RHITexture3D> D3D11RHITexture3DPtr;

class D3D11RHIDisplaySurface : public RHIDisplaySurface
{
public:
	dxSwapChain nativeObject;
	D3D11RHITexture2DPtr texture;

	D3D11RHIDisplaySurface()
		: RHIDisplaySurface(), nativeObject(nullptr), texture(nullptr)
	{}

	virtual ~D3D11RHIDisplaySurface()
	{}

	virtual RHITexturePtr RenderTarget() override { return texture.StaticCast<RHITexture>(); }
};

typedef RefCountedPtr<D3D11RHIDisplaySurface> D3D11RHIDisplaySurfacePtr;

class D3D11RHISampler : public RHISampler
{
public:
	dxSamplerState nativeObject;

	D3D11RHISampler()
		: nativeObject(nullptr)
	{}

	virtual ~D3D11RHISampler()
	{}
};

typedef RefCountedPtr<D3D11RHISampler> D3D11RHISamplerPtr;


class D3D11RHIShader : public RHIShader
{
public:
	dxVertexShader nativeVertexShader;
	dxPixelShader nativePixelShader;

	std::vector<uint8> bytecode;

	D3D11RHIShader()
		: nativeVertexShader(nullptr), nativePixelShader(nullptr)
	{}

	virtual ~D3D11RHIShader()
	{}
};
typedef RefCountedPtr<D3D11RHIShader> D3D11RHIShaderPtr;


class D3D11RHIBindingLayout : public RHIBindingLayout
{
public:
	D3D11RHIBindingLayout()
	{}

	virtual ~D3D11RHIBindingLayout()
	{}
};
typedef RefCountedPtr<D3D11RHIBindingLayout> D3D11RHIBindingLayoutPtr;


class D3D11RHIGraphicsPipeline : public RHIGraphicsPipeline
{
public:
	dxPrimitiveTopology primitiveTopology;

	D3D11RHIInputLayoutPtr inputLayout;

	D3D11RHIShaderPtr shaders[(int)GraphicsShaderType::Count];

	dxDepthStencilState depthStencilState;
	dxBlendState blendState;
	dxRasterizerState rasterizerState;

	D3D11RHIGraphicsPipeline()
		: primitiveTopology(D3D_PRIMITIVE_TOPOLOGY_UNDEFINED)
		, depthStencilState(nullptr)
		, blendState(nullptr)
		, rasterizerState(nullptr)
	{}

	virtual ~D3D11RHIGraphicsPipeline()
	{}

	D3D11RHIShaderPtr GetVertexShader() { return shaders[(int)GraphicsShaderType::Vertex]; };
	D3D11RHIShaderPtr GetHullShader() { return shaders[(int)GraphicsShaderType::Hull]; };
	D3D11RHIShaderPtr GetDomainShader() { return shaders[(int)GraphicsShaderType::Domain]; };
	D3D11RHIShaderPtr GetGeometryShader() { return shaders[(int)GraphicsShaderType::Geometry]; };
	D3D11RHIShaderPtr GetPixelShader() { return shaders[(int)GraphicsShaderType::Pixel]; };
	D3D11RHIShaderPtr GetAmplificationShader() { return shaders[(int)GraphicsShaderType::Amplification]; };
	D3D11RHIShaderPtr GetMeshShader() { return shaders[(int)GraphicsShaderType::Mesh]; };
};
typedef RefCountedPtr<D3D11RHIGraphicsPipeline> D3D11RHIGraphicsPipelinePtr;


class D3D11RHIBindingSet : public RHIBindingSet
{
public:
	ShaderTypeMasks targetShaders;  //make a copy here for fast access

	struct PerShaderBindingData
	{
		std::pair<uint, dxShaderResourceView> boundSlotAndSRVs[RHI::MaxBoundShaderResrouceViews];
		uint32 numSRVs = 0;

		std::pair<uint, dxBuffer> boundSlotAndCBuffers[RHI::MaxBoundConstantBuffers];
		uint32 numCBuffers = 0;

		std::pair<uint, dxSamplerState> boundSlotAndSamplers[RHI::MaxBoundSamplers];
		uint32 numSamplers = 0;
	};
	PerShaderBindingData perShaderBindingDatas[(int)GraphicsShaderType::Count];

	D3D11RHIBindingSet()
	{}

	virtual ~D3D11RHIBindingSet()
	{}
};
typedef RefCountedPtr<D3D11RHIBindingSet> D3D11RHIBindingSetPtr;


class D3D11RHIFramebuffer : public RHIFramebuffer
{
public:
	dxRenderTargetView renderTargetViews[RHI::MaxBoundRenderTargets];

	dxDepthStencilView depthStencilView;

public:
	D3D11RHIFramebuffer()
		: depthStencilView(nullptr)
	{
		for (int i = 0; i < RHI::MaxBoundRenderTargets; i++)
		{
			renderTargetViews[i] = nullptr;
		}
	}

	virtual ~D3D11RHIFramebuffer()
	{}
};
typedef RefCountedPtr<D3D11RHIFramebuffer> D3D11RHIFramebufferPtr;


class D3D11RHIDevice : public RHIDevice
{
public:
	D3D11RHIDevice();
	virtual ~D3D11RHIDevice();

	virtual bool Startup() override;
	virtual void Shutdown() override;

	virtual RHIShaderCompiler* GetShaderCompiler() override;

	virtual RHICommandListPtr CreateCommandList() override;
	virtual void ReleaseCommandList(RHICommandListPtr p) override;

	virtual void ExecuteCommandList(RHICommandListPtr p) override;

	virtual RHIBufferPtr CreateBuffer(const BufferDesc& desc, const void* init_data = NULL) override;
	virtual void ReleaseBuffer(RHIBufferPtr p) override;

	bool CreateNativeBuffer(D3D11RHIBuffer* p, const BufferDesc& desc, const void* init_data = NULL);
	void ReleaseNativeBuffer(D3D11RHIBuffer* p);

	virtual RHITexturePtr CreateTexture(const TextureDesc& desc, const void* init_data = NULL) override;
	virtual void ReleaseTexture(RHITexturePtr p) override;

	bool CreateNativeTexture2D(D3D11RHITexture2D* target, const TextureDesc& desc, const void* init_data = NULL);
	bool CreateNativeTexture3D(D3D11RHITexture3D* target, const TextureDesc& desc, const void* init_data = NULL);
	void ReleaseNativeTexture2D(D3D11RHITexture2D* target);
	void ReleaseNativeTexture3D(D3D11RHITexture3D* target);

	virtual RHIFramebufferPtr CreateFramebuffer(const FramebufferDesc& desc) override;
	virtual void ReleaseFramebuffer(RHIFramebufferPtr p) override;

	virtual RHIDisplaySurfacePtr CreateDisplaySurface(void* windowHandle, int w, int h, PixelFormat format) override;
	virtual void UpdateDisplaySurface(RHIDisplaySurfacePtr p, int w, int h) override;
	virtual void ReleaseDisplaySurface(RHIDisplaySurfacePtr p) override;
	virtual void PresentDisplaySurface(RHIDisplaySurfacePtr p) override;

	virtual RHISamplerPtr CreateSampler(const SamplerDesc& desc) override;
	virtual void ReleaseSampler(RHISamplerPtr p) override;

	virtual RHIBindingLayoutPtr CreateBindingLayout(const BindingLayoutDesc& desc) override;
	virtual RHIBindingSetPtr CreateBindingSet(const BindingSetDesc& desc) override;

	virtual RHIGraphicsPipelinePtr CreateGraphicsPipeline(const GraphicsPipelineDesc& desc);
	virtual void ReleaseGraphicsPipeline(RHIGraphicsPipelinePtr p);

	dxDevice NativeDevice() { return device; }
	dxDeviceContext NativeDeviceContext() { return deviceContext; }

private:
	dxDevice device;
	dxDeviceContext deviceContext;
	D3D_FEATURE_LEVEL featureLevel;

	int lastVertexBuffersCount;

	IDXGIFactory* dxGIFactory;

	class D3D11RHIInputLayoutManager* inputLayoutManager;
	class D3D11RHIShaderCompiler* shaderCompiler;

	std::vector<dxRenderTargetView> tempRenderTargetViews;

	std::vector<dxShaderResourceView> tempShaderResourceViews;

	std::vector<dxSamplerState> tempSamplerStates;
};

typedef D3D11RHIDevice* D3D11RHIDevicePtr;


class D3D11RHICommandList : public RHICommandList
{
	friend class D3D11RHIDevice;

	D3D11RHIDevicePtr d3d11Device;
	dxDeviceContext d3d11DeferredContext;
	dxCommandList d3d11CommandList;

	dxPrimitiveTopology primitiveTopology;

	dxBuffer tempNativeBuffers[RHI::MaxVertexBufferBindings];
	uint32 tempBufferStrides[RHI::MaxVertexBufferBindings];
	uint32 tempBufferOffsets[RHI::MaxVertexBufferBindings];

	RHIFramebufferPtr lastFramebuffer;
	RHIViewportSetPtr lastViewport;

	dxViewport tempNativeViewports[RHI::MaxBoundRenderTargets];

	struct PerShaderNativeData
	{
		dxShaderResourceView shaderResourceViews[RHI::MaxBoundShaderResrouceViews];

		dxBuffer constantBuffers[RHI::MaxBoundConstantBuffers];

		dxSamplerState samplers[RHI::MaxBoundSamplers];

		void Reset();
	};

	PerShaderNativeData perShaderNativeDatas[(int)GraphicsShaderType::Count];

public:
	D3D11RHICommandList(D3D11RHIDevicePtr device, dxDeviceContext deferredContext)
		: RHICommandList(device), d3d11Device(device), d3d11DeferredContext(deferredContext), d3d11CommandList(nullptr)
	{}

	virtual ~D3D11RHICommandList()
	{}

	dxDeviceContext GetD3D11DeviceContext() { return d3d11DeferredContext; }
	dxCommandList GetD3D11CommandList() { return d3d11CommandList; }

	virtual void FinishRecording() override;

	bool IsRecordingFinished() const { return d3d11CommandList != nullptr; }

	void UnbindShaderResources();

	virtual void ClearRenderTarget(RHITexturePtr view, const float4& color);
	virtual void ClearDepthStencilTarget(RHITexturePtr view, float depth = 1.0f, UINT8 stencil = 0x00);

	virtual bool UpdateBuffer(RHIBufferPtr p, const BufferDesc& desc, void* data, size_t size);
	virtual bool UpdateBufferData(RHIBufferPtr p, void* data, size_t size);

	virtual bool UpdateTexture(RHITexturePtr p, const TextureDesc& desc, void* data);

	virtual void SetGraphicsState(const GraphicsState& state);
	virtual void Draw(const DrawArguments& arg);

};

typedef RefCountedPtr<D3D11RHICommandList> D3D11RHICommandListPtr;


namespace RHI
{
	D3D11RHIDevice* CreateRHIDeviceD3D11();
	D3D11RHIDevice* GetRHIDeviceD3D11();
}
