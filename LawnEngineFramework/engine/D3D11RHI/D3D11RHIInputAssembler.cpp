#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "D3D11RHIInputAssembler.h"

#include <functional>
#include <unordered_map>

void InputElementToSemantics(InputElement t, dxInputElementDesc& dst)
{
	static const char* semantic_map[] = {
		"POSITION", //IE_POSITION = 0,
		"NORMAL", //IE_NORMAL,
		"TEXCOORD", //IE_TEXCOORD0,
		"TEXCOORD", //IE_TEXCOORD1,
		"TEXCOORD", //IE_TEXCOORD2,
		"TEXCOORD", //IE_TEXCOORD3,
		"TEXCOORD", //IE_TEXCOORD4,
		"TEXCOORD", //IE_TEXCOORD5,
		"TEXCOORD", //IE_TEXCOORD6,
		"TEXCOORD", //IE_TEXCOORD7,
		"TANGENT", //IE_TANGENT,
		"BINORMAL", //IE_BINORMAL,
		"BLENDWEIGHT", //IE_BLENDWEIGHT,
		"BLENDINDICES", //IE_BLENDINDEXES,
		"PSIZE", //IE_PIXELSIZE,
		"COLOR", //IE_VERTEXCOLOR,
	};

	static UINT semantic_index_map[] = {
		0, //IE_POSITION = 0,
		0, //IE_NORMAL,
		0, //IE_TEXCOORD0,
		1, //IE_TEXCOORD1,
		2, //IE_TEXCOORD2,
		3, //IE_TEXCOORD3,
		4, //IE_TEXCOORD4,
		5, //IE_TEXCOORD5,
		6, //IE_TEXCOORD6,
		7, //IE_TEXCOORD7,
		0, //IE_TANGENT,
		0, //IE_BINORMAL,
		0, //IE_BLENDWEIGHT,
		0, //IE_BLENDINDEXES,
		0, //IE_PIXELSIZE,
		0, //IE_VERTEXCOLOR,
	};

	dst.SemanticName = semantic_map[int(t)];
	dst.SemanticIndex = semantic_index_map[int(t)];
}

D3D11RHIInputLayoutManager::D3D11RHIInputLayoutManager()
	: RHIInputLayoutManager()
{
}

D3D11RHIInputLayoutManager::~D3D11RHIInputLayoutManager()
{
}

RHIInputLayoutPtr D3D11RHIInputLayoutManager::CreateInputLayout(const MeshVertexComponent* vertexComponents, uint32 numVertexComponents, RHIShaderPtr vertexShader)
{
	std::vector<D3D11_INPUT_ELEMENT_DESC> rhiInputElements;
	rhiInputElements.reserve(numVertexComponents);

	for (uint32 i = 0; i < numVertexComponents; i++)
	{
		const MeshVertexComponent& vertexComponent = vertexComponents[i];

		D3D11_INPUT_ELEMENT_DESC& rhiInputElementDesc = rhiInputElements.emplace_back();
		InputElementToSemantics(vertexComponent.inputElement, rhiInputElementDesc);
		rhiInputElementDesc.Format = RHI::MeshVertexFormatToRhiVertexFormat(vertexComponent.format);
		rhiInputElementDesc.InputSlot = vertexComponent.vertexBufferIndex;
		rhiInputElementDesc.AlignedByteOffset = vertexComponent.offset;
		rhiInputElementDesc.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
		rhiInputElementDesc.InstanceDataStepRate = 0;
	}

	dxDevice device = RHI::GetRHIDeviceD3D11()->NativeDevice();

	auto nativeVertexShader = vertexShader.StaticCast<D3D11RHIShader>();

	auto& vsByteCode = nativeVertexShader->bytecode;

	dxInputLayout nativeObject = nullptr;
	if (FAILED(device->CreateInputLayout(rhiInputElements.data(), rhiInputElements.size(), vsByteCode.data(), vsByteCode.size(), &nativeObject)))
	{
		Log::Error("CreateInputLayout failed");
		return nullptr;
	}

	D3D11RHIInputLayoutPtr inputLayout(new D3D11RHIInputLayout());
	inputLayout->nativeObject = nativeObject;

	return inputLayout.StaticCast<RHIInputLayout>();
}

void D3D11RHIInputLayoutManager::ReleaseInputLayout(RHIInputLayoutPtr p)
{
	if (p)
	{
		auto ptr = p.StaticCast<D3D11RHIInputLayout>();

		SAFE_RELEASE(ptr->nativeObject);
	}
}
