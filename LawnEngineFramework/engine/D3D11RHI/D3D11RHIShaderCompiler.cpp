#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "D3D11RHIShaderCompiler.h"
#include "RenderSystem/Shader.h"

#include <d3dcompiler.h>
#include <d3d11shader.h>

class D3DCompilerAPI
{
public:
	D3DCompilerAPI()
		: mod_d3dcompiler_(NULL),
		DynamicD3DCompile_(NULL),
		DynamicD3DReflect_(NULL),
		DynamicD3DStripShader_(NULL)
	{
		LPCSTR d3dCompilerDll = D3DCOMPILER_DLL;
		//LPCSTR d3dCompilerDll = TEXT("d3dcompiler_47.dll");
		mod_d3dcompiler_ = LoadLibraryEx(d3dCompilerDll, NULL, 0);
		if (mod_d3dcompiler_)
		{
			DynamicD3DCompile_ = reinterpret_cast<D3DCompileFunc>(GetProcAddress(mod_d3dcompiler_, "D3DCompile"));
			DynamicD3DReflect_ = reinterpret_cast<D3DReflectFunc>(GetProcAddress(mod_d3dcompiler_, "D3DReflect"));
			DynamicD3DStripShader_ = reinterpret_cast<D3DStripShaderFunc>(GetProcAddress(mod_d3dcompiler_, "D3DStripShader"));
		}
		else
		{
			MessageBoxW(NULL, L"Can't load d3d compiler dll", L"Error", MB_OK);
		}
	}

	~D3DCompilerAPI()
	{
		if (mod_d3dcompiler_)
		{
			FreeLibrary(mod_d3dcompiler_);
		}
	}

	HRESULT D3DCompile(LPCVOID pSrcData, SIZE_T SrcDataSize, LPCSTR pSourceName,
		D3D_SHADER_MACRO const* pDefines, ID3DInclude* pInclude, LPCSTR pEntrypoint,
		LPCSTR pTarget, UINT Flags1, UINT Flags2, ID3DBlob** ppCode, ID3DBlob** ppErrorMsgs) const
	{
		return DynamicD3DCompile_(pSrcData, SrcDataSize, pSourceName, pDefines, pInclude, pEntrypoint,
			pTarget, Flags1, Flags2, ppCode, ppErrorMsgs);
	}

	HRESULT D3DReflect(LPCVOID pSrcData, SIZE_T SrcDataSize, void** ppReflector) const
	{
		REFIID pInterface = IID_ID3D11ShaderReflection;
		return DynamicD3DReflect_(pSrcData, SrcDataSize, pInterface, ppReflector);
	}

	HRESULT D3DStripShader(LPCVOID pShaderBytecode, SIZE_T BytecodeLength, UINT uStripFlags, ID3DBlob** ppStrippedBlob) const
	{
		return DynamicD3DStripShader_(pShaderBytecode, BytecodeLength, uStripFlags, ppStrippedBlob);
	}

private:
	typedef HRESULT(WINAPI* D3DCompileFunc)(LPCVOID pSrcData, SIZE_T SrcDataSize, LPCSTR pSourceName,
		D3D_SHADER_MACRO const* pDefines, ID3DInclude* pInclude, LPCSTR pEntrypoint,
		LPCSTR pTarget, UINT Flags1, UINT Flags2, ID3DBlob** ppCode, ID3DBlob** ppErrorMsgs);
	typedef HRESULT(WINAPI* D3DReflectFunc)(LPCVOID pSrcData, SIZE_T SrcDataSize, REFIID pInterface, void** ppReflector);
	typedef HRESULT(WINAPI* D3DStripShaderFunc)(LPCVOID pShaderBytecode, SIZE_T BytecodeLength, UINT uStripFlags, ID3DBlob** ppStrippedBlob);

private:
	HMODULE mod_d3dcompiler_;

	D3DCompileFunc DynamicD3DCompile_;
	D3DReflectFunc DynamicD3DReflect_;
	D3DStripShaderFunc DynamicD3DStripShader_;
};

D3DCompilerAPI g_D3DCompilerAPI;

//////////////////////////////////////////////////////////////////////////

ShaderVariableType GetShaderVariableType(ID3D11ShaderReflectionType* rhiVarType)
{
	D3D11_SHADER_TYPE_DESC rhiVarTypeDesc;
	rhiVarType->GetDesc(&rhiVarTypeDesc);

	switch (rhiVarTypeDesc.Class)
	{
	case D3D_SVC_SCALAR:
		if (rhiVarTypeDesc.Type == D3D_SVT_FLOAT)
		{
			return SVT_Float;
		}
		break;
	case D3D_SVC_VECTOR:
		if (rhiVarTypeDesc.Type == D3D_SVT_FLOAT)
		{
			if (rhiVarTypeDesc.Rows == 1 && rhiVarTypeDesc.Columns == 2)
				return SVT_Float2;
			else if (rhiVarTypeDesc.Rows == 1 && rhiVarTypeDesc.Columns == 3)
				return SVT_Float3;
			else if (rhiVarTypeDesc.Rows == 1 && rhiVarTypeDesc.Columns == 4)
				return SVT_Float4;
			else if (rhiVarTypeDesc.Rows == 4 && rhiVarTypeDesc.Columns == 4)
				return SVT_Float4x4;
		}
		break;
	case D3D_SVC_MATRIX_ROWS:
	case D3D_SVC_MATRIX_COLUMNS:
		if (rhiVarTypeDesc.Type == D3D_SVT_FLOAT)
		{
			if (rhiVarTypeDesc.Rows == 4 && rhiVarTypeDesc.Columns == 4)
				return SVT_Float4x4;
		}
		break;
	}

	return SVT_Unknown;
}

ShaderResourceType GetShaderResourceType(const D3D11_SHADER_INPUT_BIND_DESC& rhiResDesc)
{
	if (rhiResDesc.Dimension == D3D_SRV_DIMENSION_TEXTURE1D)
	{
		return SRT_Texture1D;
	}
	else if (rhiResDesc.Dimension == D3D_SRV_DIMENSION_TEXTURE2D)
	{
		return SRT_Texture2D;
	}
	else if (rhiResDesc.Dimension == D3D_SRV_DIMENSION_TEXTURE3D)
	{
		return SRT_Texture3D;
	}
	else if (rhiResDesc.Dimension == D3D_SRV_DIMENSION_TEXTURECUBE)
	{
		return SRT_TextureCube;
	}
	else
	{
		return SRT_Unknown;
	}
}

static int GetMeshVertexComponentCount(BYTE mask)
{
	int count = 0;
	if (mask & D3D_COMPONENT_MASK_X)
		count++;
	if (mask & D3D_COMPONENT_MASK_Y)
		count++;
	if (mask & D3D_COMPONENT_MASK_Z)
		count++;
	if (mask & D3D_COMPONENT_MASK_W)
		count++;

	return count;
}

MeshVertexFormat GetMeshVertexFormat(const D3D11_SIGNATURE_PARAMETER_DESC& vsParamDesc)
{
	int componentCount = GetMeshVertexComponentCount(vsParamDesc.Mask);

	switch (vsParamDesc.ComponentType)
	{
	case D3D_REGISTER_COMPONENT_FLOAT32:
		switch (componentCount)
		{
		case 1: return MeshVertexFormat(MVBF_Float, 1);
		case 2: return MeshVertexFormat(MVBF_Float, 2);
		case 3: return MeshVertexFormat(MVBF_Float, 3);
		case 4: return MeshVertexFormat(MVBF_Float, 4);
		}
		break;
	}

	return MeshVertexFormat();
}

//////////////////////////////////////////////////////////////////////////

class D3D11RHIShaderInclude : public ID3DInclude
{
public:
	AssetPath shaderPath;

public:
	D3D11RHIShaderInclude(const AssetPath& shaderPath)
		: shaderPath(shaderPath)
	{
	}

	HRESULT __stdcall Open(D3D_INCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID* ppData, UINT* pBytes)
	{
		AssetPath finalPath = Shader::FindShaderInclude(shaderPath, pFileName);

		if (finalPath.IsValid() && finalPath.Exists())
		{
			std::unique_ptr<ReadStream> stream = finalPath.OpenRead(true);
			std::string content = stream->ReadAllString();

			char* buf = new char[content.size()];
			memcpy(buf, content.data(), content.size());

			*ppData = buf;
			*pBytes = content.size();

			return S_OK;
		}
		else
		{
			Log::Error("Can not find include file: '%s' in shader '%s'", pFileName, shaderPath.ToString().c_str());

			*ppData = nullptr;
			*pBytes = 0;

			return E_FAIL;
		}
	}

	HRESULT __stdcall Close(LPCVOID pData)
	{
		// Here we must correctly free buffer created in Open.
		char* buf = (char*)pData;
		delete[] buf;
		return S_OK;
	}
};

//////////////////////////////////////////////////////////////////////////

RHIShaderPtr D3D11RHIShaderCompiler::CompileShader(
	const AssetPath& shaderPath,
	const std::string& shaderName,
	const std::string& source,
	const std::string& entryPoint,
	const std::map<std::string, std::string>& macros,
	ShaderFrequency freq)
{
	std::vector<D3D10_SHADER_MACRO> macroDefines;
	for (auto& it : macros)
	{
		D3D10_SHADER_MACRO m;
		m.Name = it.first.c_str();
		m.Definition = it.second.c_str();
		macroDefines.push_back(m);
	}

	D3D10_SHADER_MACRO m;
	m.Name = NULL;
	m.Definition = NULL;
	macroDefines.push_back(m);

	D3D11RHIShaderInclude include = D3D11RHIShaderInclude(shaderPath);

	dxInclude pInclude = &include;

	std::string target;
	if (freq == SF_VertexShader)
		target += "vs";
	else if (freq == SF_PixelShader)
		target += "ps";
	else
	{
		Log::Error("Unknown ShaderFrequency: %d, when compiling '%s'", freq, shaderName.c_str());
		return {};
	}

	ShaderFeatureLevel featureLevel = SFL_SM_5_0;

	target += "_";
	if (featureLevel == SFL_SM_5_0)
		target += "5_0";
	else
	{
		Log::Error("Unknown ShaderFeatureLevel: %d, when compiling '%s'", featureLevel, shaderName.c_str());
		return {};
	}

	UINT HLSLFlags = D3DCOMPILE_ENABLE_STRICTNESS;

#if defined(DEBUG) || defined(_DEBUG)
	HLSLFlags |= D3DCOMPILE_DEBUG;
#else
	HLSLFlags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
#endif

	UINT FXFlags = 0;

	dxBlob compileMsg = NULL;
	VCPP_AUTO_SAFE_RELEASE(compileMsg);

	dxBlob compiledBytecode = nullptr;

	HRESULT hr = g_D3DCompilerAPI.D3DCompile(source.data(), source.size(), shaderName.c_str(), macroDefines.data(), pInclude, entryPoint.c_str(), target.c_str(), HLSLFlags, FXFlags, &compiledBytecode, &compileMsg);

	if (compileMsg != NULL)
	{
		char* msg = (char*)compileMsg->GetBufferPointer();

		if (FAILED(hr))
		{
			Log::Error("Failed compiling shader: %s\n: %s", shaderName.c_str(), msg);
		}
		else
		{
			Log::Warning("Warning when compiling shader: %s\n: %s", shaderName.c_str(), msg);
		}
	}

	if (FAILED(hr))
	{
		Log::Error("Shader compile failed: %s", shaderName.c_str());
		return {};
	}

	D3D11RHIShaderPtr ptr(new D3D11RHIShader());
	ptr->name = shaderName;
	ptr->frequency = freq;
	ptr->reflection = new RHIShaderReflection();
	
	if (compiledBytecode)
	{
		ptr->bytecode.resize(compiledBytecode->GetBufferSize());
		memcpy(ptr->bytecode.data(), compiledBytecode->GetBufferPointer(), compiledBytecode->GetBufferSize());
	}

	if (!GenerateShaderReflection(shaderName, freq, ptr->bytecode, *ptr->reflection))
	{
		return {};
	}

	if (!CompileNativeShader(shaderName, freq, *ptr))
	{
		return {};
	}

	return ptr.StaticCast<RHIShader>();
}

bool D3D11RHIShaderCompiler::GenerateShaderReflection(
	const std::string& shaderNameForDebug, 
	ShaderFrequency freq,
	const std::vector<uint8>& bytecode, 
	RHIShaderReflection& outShaderReflection)
{
	ID3D11ShaderReflection* nativeShaderReflection = nullptr;
	VCPP_AUTO_SAFE_RELEASE(nativeShaderReflection);

	if (FAILED(g_D3DCompilerAPI.D3DReflect(bytecode.data(), bytecode.size(), (void**)&nativeShaderReflection)))
	{
		Log::Error("Reflect shader failed");
		return false;
	}

	D3D11_SHADER_DESC nativeShaderDesc;
	nativeShaderReflection->GetDesc(&nativeShaderDesc);

	//
	// vs-inputs
	//
	if (freq == SF_VertexShader)
	{
		int numVertexInputs = nativeShaderDesc.InputParameters;

		outShaderReflection.vertexInputs.reserve(numVertexInputs);

		for (int i = 0; i < numVertexInputs; i++)
		{
			D3D11_SIGNATURE_PARAMETER_DESC nativeVsInputDesc;
			nativeShaderReflection->GetInputParameterDesc(i, &nativeVsInputDesc);

			RHIShaderVertexInputDesc& vsInputDesc = outShaderReflection.vertexInputs.emplace_back();
			vsInputDesc.semanticName = nativeVsInputDesc.SemanticName;
			vsInputDesc.semanticIndex = nativeVsInputDesc.SemanticIndex;
			vsInputDesc.format = GetMeshVertexFormat(nativeVsInputDesc);
		}
	}

	//
	// cbuffers
	//
	for (int i = 0; i < nativeShaderDesc.ConstantBuffers; i++)
	{
		ID3D11ShaderReflectionConstantBuffer* nativeCB = nativeShaderReflection->GetConstantBufferByIndex(i);

		D3D11_SHADER_BUFFER_DESC nativeCBDesc;
		nativeCB->GetDesc(&nativeCBDesc);

		RHIShaderCBufferDesc& cbdesc = outShaderReflection.cbuffers.emplace_back();
		cbdesc.name = nativeCBDesc.Name;
		cbdesc.byteSize = nativeCBDesc.Size;

		for (int varIndex = 0; varIndex < nativeCBDesc.Variables; varIndex++)
		{
			ID3D11ShaderReflectionVariable* nativeVar = nativeCB->GetVariableByIndex(varIndex);

			D3D11_SHADER_VARIABLE_DESC nativeVarDesc;
			nativeVar->GetDesc(&nativeVarDesc);

			RHIShaderVariableDesc vardesc;
			vardesc.name = nativeVarDesc.Name;
			vardesc.type = GetShaderVariableType(nativeVar->GetType());
			vardesc.byteOffset = nativeVarDesc.StartOffset;
			vardesc.byteSize = nativeVarDesc.Size;

			if (nativeVarDesc.DefaultValue != nullptr)
			{
				cbdesc.variableDefaultValues.Set(vardesc.name.str(), nativeVarDesc.DefaultValue, vardesc.byteSize);
			}

			cbdesc.variables.push_back(vardesc);
		}
	}

	//
	// bindings and resources and samplers
	//
	for (int resIndex = 0; resIndex < nativeShaderDesc.BoundResources; resIndex++)
	{
		D3D11_SHADER_INPUT_BIND_DESC nativeResDesc;
		nativeShaderReflection->GetResourceBindingDesc(resIndex, &nativeResDesc);

		if (nativeResDesc.Type == D3D_SIT_CBUFFER)
		{
			RHIShaderCBufferBinding& binding = outShaderReflection.cbufferBindings.emplace_back();
			binding.cbufferName = nativeResDesc.Name;
			binding.bindPoint = nativeResDesc.BindPoint;
		}
		else if (nativeResDesc.Type == D3D_SIT_TEXTURE)
		{
			RHIShaderResourceBinding& binding = outShaderReflection.resourceBindings.emplace_back();
			binding.resourceName = nativeResDesc.Name;
			binding.bindPoint = nativeResDesc.BindPoint;

			if (outShaderReflection.FindResourceByName(binding.resourceName) < 0)
			{
				RHIShaderResourceDesc& resourceDesc = outShaderReflection.resources.emplace_back();
				resourceDesc.name = binding.resourceName;
				resourceDesc.type = GetShaderResourceType(nativeResDesc);
			}
		}
		else if (nativeResDesc.Type == D3D_SIT_SAMPLER)
		{
			RHIShaderSamplerBinding& binding = outShaderReflection.samplerBindings.emplace_back();
			binding.samplerName = nativeResDesc.Name;
			binding.bindPoint = nativeResDesc.BindPoint;

			if (outShaderReflection.FindSamplerByName(binding.samplerName) < 0)
			{
				RHIShaderSamplerDesc& samplerDesc = outShaderReflection.samplers.emplace_back();
				samplerDesc.name = binding.samplerName;
			}
		}
	}

	return true;
}

bool D3D11RHIShaderCompiler::CompileNativeShader(const std::string& shaderName, ShaderFrequency freq, D3D11RHIShader& rhiShader)
{
	dxDevice device = RHI::GetRHIDeviceD3D11()->NativeDevice();

	switch (freq)
	{
	case SF_VertexShader:
		if (FAILED(device->CreateVertexShader(rhiShader.bytecode.data(), rhiShader.bytecode.size(), NULL, &rhiShader.nativeVertexShader)))
		{
			Log::Error("CreateVertexShader failed");
			return false;
		}
		else
		{
			return true;
		}
	case SF_PixelShader:
		if (FAILED(device->CreatePixelShader(rhiShader.bytecode.data(), rhiShader.bytecode.size(), NULL, &rhiShader.nativePixelShader)))
		{
			Log::Error("CreatePixelShader failed");
			return false;
		}
		else
		{
			return true;
		}
	default:
		CHECK(false);
		return false;
	}
}
