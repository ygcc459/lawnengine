#pragma once

#include <memory>
#include "Memory.h"

class MeshDrawCommand;
class RHIDevice;
class RHICommandList;

//////////////////////////////////////////////////////////////////////////

namespace RHI
{
	const uint32 MaxBindingLayouts = 4;
	const uint32 MaxBoundRenderTargets = 8;
	const uint32 MaxBindingSets = 8;
	const uint32 MaxVertexBufferBindings = 8;
	const uint32 MaxVertexComponents = 16;
	const uint32 BindingSetMaxCBuffers = 3;
	const uint32 BindingSetMaxResources = 8;
	const uint32 BindingSetMaxSamplers = 8;
}

enum InputElement
{
	IE_POSITION = 0,
	IE_NORMAL = 1,
	IE_TEXCOORD0 = 2,
	IE_TEXCOORD1 = 3,
	IE_TEXCOORD2 = 4,
	IE_TEXCOORD3 = 5,
	IE_TEXCOORD4 = 6,
	IE_TEXCOORD5 = 7,
	IE_TEXCOORD6 = 8,
	IE_TEXCOORD7 = 9,
	IE_TANGENT = 10,
	IE_BITANGENT = 11,
	IE_BLENDWEIGHT = 12,
	IE_BLENDINDEXES = 13,
	IE_PIXELSIZE = 14,
	IE_VERTEXCOLOR = 15,

	_IE_COUNT,  //the number of all InputElements the engine support
	_IE_TEXCOORD,  //a place holder only, used in D3D11_INPUT_ELEMENT_DESC, indicating D3D11_INPUT_ELEMENT_DESC.SemanticIndex should be appended with this
	IE_UNKNOWN,
	_IE_FORCE_DWORD = 0xFFFFFFFF,
};
DECL_ENUM_TYPE(InputElement);

enum PrimitiveTopology
{
	PT_Unknown = 0,
	PT_PointList,
	PT_LineList,
	PT_LineStrip,
	PT_TriangleList,
	PT_TriangleStrip,
};
DECL_ENUM_TYPE(PrimitiveTopology);

enum MeshVertexBaseFormat
{
	MVBF_Unknown = 0,
	MVBF_Float,
	MVBF_Byte,
};
DECL_ENUM_TYPE(MeshVertexBaseFormat);

int GetByteSize(MeshVertexBaseFormat format);

// e.g. float2, half4, ...
struct MeshVertexFormat
{
	MeshVertexBaseFormat baseFormat;
	int count;

	MeshVertexFormat()
		:baseFormat(MVBF_Unknown), count(0)
	{}

	MeshVertexFormat(MeshVertexBaseFormat baseFormat, int count)
		:baseFormat(baseFormat), count(count)
	{}

	int ByteSize() const;

	bool operator == (const MeshVertexFormat& other) const
	{
		return baseFormat == other.baseFormat
			&& count == other.count;
	}

	bool operator != (const MeshVertexFormat& other) const
	{
		return !((*this) == other);
	}

	bool operator < (const MeshVertexFormat& other) const
	{
		if (baseFormat != other.baseFormat)
			return baseFormat < other.baseFormat;
		else if (count != other.count)
			return count < other.count;
		else
			return false;
	}
};
DECL_PRIMITIVE_TYPE(MeshVertexFormat)

bool IsCompatibleVertexFormat(MeshVertexFormat providedFormat, MeshVertexFormat reuqiredFormat);

struct MeshVertexComponent
{
	InputElement inputElement;
	int vertexBufferIndex;
	MeshVertexFormat format;
	int offset;

	MeshVertexComponent()
		:vertexBufferIndex(-1), offset(0)
	{}

	MeshVertexComponent(InputElement inputElement, int vertexBufferIndex, MeshVertexFormat format, int offset)
		: inputElement(inputElement), vertexBufferIndex(vertexBufferIndex), format(format), offset(offset)
	{}

	uint32 Pack() const
	{
		uint32 v = 0;

		CHECK(inputElement >= 0 && inputElement < 256);
		v |= (((uint32)inputElement) & 0xFF) << 24;

		CHECK(vertexBufferIndex >= 0 && vertexBufferIndex < 256);
		v |= (((uint32)vertexBufferIndex) & 0xFF) << 16;

		CHECK(offset >= 0 && offset < 256);
		v |= (((uint32)offset) & 0xFF) << 8;

		CHECK(format.baseFormat >= 0 && format.baseFormat < 16);
		v |= (((uint32)format.baseFormat) & 0x0F) << 4;

		CHECK(format.count >= 1 && format.count <= 16);
		v |= (((uint32)format.count - 1) & 0x0F) << 0;

		return v;
	}

	bool operator == (const MeshVertexComponent& other) const
	{
		return inputElement == other.inputElement
			&& vertexBufferIndex == other.vertexBufferIndex
			&& format == other.format
			&& offset == other.offset;
	}

	bool operator != (const MeshVertexComponent& other) const
	{
		return !((*this) == other);
	}

	bool operator < (const MeshVertexComponent& other) const
	{
		if (inputElement != other.inputElement)
			return inputElement < other.inputElement;
		else if (vertexBufferIndex != other.vertexBufferIndex)
			return vertexBufferIndex < other.vertexBufferIndex;
		else if (format != other.format)
			return format < other.format;
		else if (offset != other.offset)
			return offset < other.offset;
		else
			return false;
	}
};

class MeshVertexComponentType : public PrimitiveObjectType<MeshVertexComponent>
{
public:
	MeshVertexComponentType()
		: PrimitiveObjectType<MeshVertexComponent>("MeshVertexComponent")
	{}

	virtual bool Serialize(Serializer& serializer, void* p) const override
	{
		ValueType& obj = *reinterpret_cast<ValueType*>(p);

		serializer.SerializeMemberAuto("inputElement", obj.inputElement);
		serializer.SerializeMemberAuto("vertexBufferIndex", obj.vertexBufferIndex);
		serializer.SerializeMemberAuto("format", obj.format);
		serializer.SerializeMemberAuto("offset", obj.offset);

		return true;
	}

	virtual bool Deserialize(Deserializer& deserializer, void* p) const override
	{
		ValueType& obj = *reinterpret_cast<ValueType*>(p);

		deserializer.DeserializeMemberAuto("inputElement", obj.inputElement);
		deserializer.DeserializeMemberAuto("vertexBufferIndex", obj.vertexBufferIndex);
		deserializer.DeserializeMemberAuto("format", obj.format);
		deserializer.DeserializeMemberAuto("offset", obj.offset);

		return true;
	}
};
DECL_PRIMITIVE_TYPE(MeshVertexComponent)

enum ShaderFrequency
{
	SF_VertexShader = 0,
	SF_PixelShader,

	SF_Count,
};
DECL_ENUM_TYPE(ShaderFrequency);

enum ShaderFeatureLevel
{
	SFL_SM_5_0,
};

enum ShaderVariableType
{
	SVT_Unknown,

	SVT_Bool,
	SVT_Int,
	SVT_Float,

	SVT_Float2,
	SVT_Float3,
	SVT_Float4,

	SVT_Float4x4,
};
DECL_ENUM_TYPE(ShaderVariableType);

uint32 GetByteSize(ShaderVariableType type);

enum ShaderResourceType
{
	SRT_Unknown,
	SRT_Texture1D,
	SRT_Texture2D,
	SRT_Texture3D,
	SRT_TextureCube,
};

class RHIInputLayoutManager;

struct ShaderRequiredVertexComponentDesc
{
	InputElement inputElement;
	MeshVertexFormat format;
};

class IShaderVertexDataProvider
{
public:
	virtual bool FindAvailableVertexComponent(RHIInputLayoutManager* inputLayoutManager, const ShaderRequiredVertexComponentDesc& requiredComponent, uint& outInputSlot, uint& outByteOffset, MeshVertexFormat& outFormat) = 0;
	virtual void GetDummyVertexComponent(RHIInputLayoutManager* inputLayoutManager, uint& outInputSlot, uint& outByteOffset) = 0;
};

enum BufferUsage
{
	BU_DEFAULT = 0,
	BU_IMMUTABLE = 1,
	BU_DYNAMIC = 2,
	BU_STAGING = 3
};

enum BufferBindFlag
{
	BBF_VERTEX_BUFFER = 0x1L,
	BBF_INDEX_BUFFER = 0x2L,
	BBF_CONSTANT_BUFFER = 0x4L,
	BBF_SHADER_RESOURCE = 0x8L,
	BBF_STREAM_OUTPUT = 0x10L,
	BBF_RENDER_TARGET = 0x20L,
	BBF_DEPTH_STENCIL = 0x40L,
	BBF_UNORDERED_ACCESS = 0x80L,
	//BBF_DECODER = 0x200L,
	//BBF_VIDEO_ENCODER = 0x400L
};

enum CpuAccessFlag
{
	CAF_NONE = 0,
	CAF_WRITE = 1 << 0,
	CAF_READ = 1 << 1,
};

enum IndexFormat
{
	IF_UINT16,
	IF_UINT32,
};
DECL_ENUM_TYPE(IndexFormat)

enum PixelFormat
{
	PF_R8G8B8A8 = 0,
	PF_R8,
	PF_R16G16B16A16_Float,
	PF_R32G32B32A32_Float,
	PF_Depth32,
	PF_Depth24Stencil8,

	PF_Count,
};
DECL_ENUM_TYPE(PixelFormat);

uint32 GetPixelByteSize(PixelFormat format);

struct BufferDesc
{
	int size;
	BufferBindFlag binding;
	BufferUsage usage;
	CpuAccessFlag cpuAccess;
	
	BufferDesc()
		: size(0)
		, binding(BufferBindFlag::BBF_VERTEX_BUFFER)
		, usage(BU_DEFAULT)
		, cpuAccess(CAF_NONE)
	{}

	BufferDesc(int size, BufferBindFlag binding = BufferBindFlag::BBF_VERTEX_BUFFER, BufferUsage usage = BU_DEFAULT, CpuAccessFlag cpuAccess = CAF_NONE)
		: size(size)
		, binding(binding)
		, usage(usage)
		, cpuAccess(cpuAccess)
	{}

	bool operator == (BufferDesc& other) const
	{
		return this->size == other.size
			&& this->binding == other.binding
			&& this->usage == other.usage
			&& this->cpuAccess == other.cpuAccess;
	}

	bool operator != (BufferDesc& other) const
	{
		return !((*this) == other);
	}
};

enum TextureType
{
	TT_Texture2D,
	TT_Texture3D,
	TT_TextureCube,
};
DECL_ENUM_TYPE(TextureType);

enum TextureFlags
{
	TF_None = 0,

	TF_ShaderResource = 1 << 0,
	TF_RenderTarget = 1 << 1,
	TF_DepthStencil = 1 << 2,

	TF_SRGB = 1 << 3,

	TF_Dynamic = 1 << 4,

	TF_CPUReadable = 1 << 5,
	TF_CPUWritable = 1 << 6,
};

struct TextureDesc
{
	TextureType type;

	int width;
	int height;
	int depth;

	int mipmaps;
	int arraySize;

	PixelFormat format;
	
	int multiSampleCount;
	int multiSampleQuality;

	TextureFlags flags;

	TextureDesc()
		: type(TT_Texture2D)
		, width(1)
		, height(1)
		, depth(1)
		, mipmaps(1)
		, arraySize(1)
		, format(PF_R8G8B8A8)
		, multiSampleCount(1)
		, multiSampleQuality(0)
		, flags(TF_None)
	{}

	bool operator == (TextureDesc& other) const
	{
		return memcmp(this, &other, sizeof(TextureDesc)) == 0;
	}

	bool operator != (TextureDesc& other) const
	{
		return !((*this) == other);
	}
};

enum SamplerFilter
{
	SF_Point = 0,
	SF_Bilinear,
};

enum SamplerAddressing
{
	SA_Clamp = 0,
	SA_Repeat,
};

struct SamplerDesc
{
	SamplerFilter filter;
	SamplerAddressing addressing;

	SamplerDesc()
		: filter(SF_Bilinear), addressing(SA_Clamp)
	{}
};

enum BlendMultiplier
{
	BM_Zero = 0,
	BM_One,

	BM_SrcColor,
	BM_InvSrcColor,
	BM_SrcAlpha,
	BM_InvSrcAlpha,

	BM_DstColor,
	BM_InvDstColor,
	BM_DstAlpha,
	BM_InvDstAlpha,

	BM_BlendFactor,
	BM_InvBlendFactor,
};
DECL_ENUM_TYPE(BlendMultiplier);

enum BlendOperation
{
	BO_Add = 0,
	BO_Sub,
	BO_Min,
	BO_Max,
};
DECL_ENUM_TYPE(BlendOperation);

struct RenderTargetBlendState
{
	bool blendEnable;
	BlendMultiplier srcBlend;
	BlendMultiplier dstBlend;
	BlendOperation blendOp;
	BlendMultiplier srcBlendAlpha;
	BlendMultiplier dstBlendAlpha;
	BlendOperation blendOpAlpha;

	RenderTargetBlendState()
		: blendEnable(false)
		, srcBlend(BM_InvSrcAlpha)
		, dstBlend(BM_SrcAlpha)
		, blendOp(BO_Add)
		, srcBlendAlpha(BM_InvSrcAlpha)
		, dstBlendAlpha(BM_SrcAlpha)
		, blendOpAlpha(BO_Add)
	{}
};

enum RenderTargetWriteMask
{
	RTWM_RED = 1 << 0,
	RTWM_GREEN = 1 << 1,
	RTWM_BLUE = 1 << 2,
	RTWM_ALPHA = 1 << 3,

	RTWM_None = 0,
	RTWM_RGB = (((RTWM_RED | RTWM_GREEN) | RTWM_BLUE)),
	RTWM_RGBA = (((RTWM_RED | RTWM_GREEN) | RTWM_BLUE) | RTWM_ALPHA),
};

enum DepthTestMethod
{
	DTM_Never = 0,
	DTM_Less,
	DTM_Equal,
	DTM_LessEqual,
	DTM_Greater,
	DTM_NotEqual,
	DTM_GreaterEqual,
	DTM_Always,
};
DECL_ENUM_TYPE(DepthTestMethod);

enum CullMode
{
	CullMode_None = 0,
	CullMode_Front,
	CullMode_Back,
};
DECL_ENUM_TYPE(CullMode);

struct RHIViewport
{
public:
	int x;
	int y;
	uint width;
	uint height;
	float min_depth;
	float max_depth;

public:
	RHIViewport()
		: x(0), y(0), width(0), height(0), min_depth(0.0f), max_depth(1.0f)
	{
	}

	RHIViewport(int x, int y, uint w, uint h, float min_depth = 0.0f, float max_depth = 1.0f)
		: x(x), y(y), width(w), height(h), min_depth(min_depth), max_depth(max_depth)
	{
	}

	~RHIViewport(void)
	{
	}
};


//////////////////////////////////////////////////////////////////////////
