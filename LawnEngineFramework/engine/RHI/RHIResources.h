#pragma once

#include "RHICommon.h"

enum class ShaderTypeMasks : uint16_t
{
	None = 0x0000,

	Compute = 0x0020,

	Vertex = 0x0001,
	Hull = 0x0002,
	Domain = 0x0004,
	Geometry = 0x0008,
	Pixel = 0x0010,
	Amplification = 0x0040,
	Mesh = 0x0080,
	AllGraphics = 0x00FE,

	RayGeneration = 0x0100,
	AnyHit = 0x0200,
	ClosestHit = 0x0400,
	Miss = 0x0800,
	Intersection = 0x1000,
	Callable = 0x2000,
	AllRayTracing = 0x3F00,

	All = 0x3FFF,
};

enum class GraphicsShaderType
{
	Vertex = 0,
	Hull,
	Domain,
	Geometry,
	Pixel,
	Amplification,
	Mesh,

	Count,
};

enum class ResourceType : uint8_t
{
	None,
	Texture_SRV,
	Texture_UAV,
	TypedBuffer_SRV,
	TypedBuffer_UAV,
	StructuredBuffer_SRV,
	StructuredBuffer_UAV,
	RawBuffer_SRV,
	RawBuffer_UAV,
	ConstantBuffer,
	VolatileConstantBuffer,
	Sampler,
	RayTracingAccelStruct,
	PushConstants,

	Count,
};


class RHIResource : public RefCountedObject
{
public:
	RHIResource() {}
	virtual ~RHIResource() {}
};
typedef RefCountedPtr<RHIResource> RHIResourcePtr;


class RHIBuffer : public RHIResource
{
public:
	BufferDesc desc;

public:
	RHIBuffer() {}
	virtual ~RHIBuffer() {}

	const BufferDesc& GetDesc() const { return desc; }
};

typedef RefCountedPtr<RHIBuffer> RHIBufferPtr;


class RHIInputLayout : public RHIResource
{
public:
	bool needsDummyVertexBuffer;

	RHIInputLayout()
		:needsDummyVertexBuffer(false)
	{}

	virtual ~RHIInputLayout()
	{}
};

typedef RefCountedPtr<RHIInputLayout> RHIInputLayoutPtr;


class RHITexture : public RHIResource
{
public:
	TextureDesc desc;

	RHITexture() {}
	virtual ~RHITexture() {}

	const TextureDesc& GetDesc() const { return desc; }
};

typedef RefCountedPtr<RHITexture> RHITexturePtr;


class RHIDisplaySurface : public RHIResource
{
public:
	int width;
	int height;

	RHIDisplaySurface()
		: width(0), height(0)
	{}

	virtual ~RHIDisplaySurface()
	{}

	virtual RHITexturePtr RenderTarget() { return nullptr; }
};

typedef RefCountedPtr<RHIDisplaySurface> RHIDisplaySurfacePtr;


class RHISampler : public RHIResource
{
public:
	SamplerDesc desc;

	RHISampler()
	{}

	virtual ~RHISampler()
	{}
};

typedef RefCountedPtr<RHISampler> RHISamplerPtr;


struct RHIShaderReflection;

class RHIShader : public RHIResource
{
public:
	ShaderFrequency frequency;

	std::string name;

	RHIShaderReflection* reflection;

	RHIShader()
		: reflection(nullptr), frequency(ShaderFrequency::SF_VertexShader)
	{}

	virtual ~RHIShader();
};

typedef RefCountedPtr<RHIShader> RHIShaderPtr;


struct FramebufferAttachmentDesc
{
	RHITexturePtr texture = nullptr;

	uint32 baseMipLevel = 0;
	uint32 numMipLevels = 1;
	uint32 baseArraySlice = 0;
	uint32 numArraySlices = 1;

	bool isReadOnly = false;

	FramebufferAttachmentDesc()
	{}

	FramebufferAttachmentDesc(RHITexturePtr texture, bool isReadOnly)
		: texture(texture), isReadOnly(isReadOnly)
	{}

	FramebufferAttachmentDesc(RHITexturePtr texture, bool isReadOnly, uint32 baseMipLevel, uint32 numMipLevels, uint32 baseArraySlice, uint32 numArraySlices)
		: texture(texture), isReadOnly(isReadOnly), baseMipLevel(baseMipLevel), numMipLevels(numMipLevels), baseArraySlice(baseArraySlice), numArraySlices(numArraySlices)
	{}
};

struct FramebufferDesc
{
	FramebufferAttachmentDesc colorAttachments[RHI::MaxBoundRenderTargets];
	uint32 numColorAttachments = 0;

	FramebufferAttachmentDesc depthAttachment;
};

class RHIFramebuffer : public RHIResource
{
public:
	FramebufferDesc desc;

public:
	RHIFramebuffer()
	{}

	virtual ~RHIFramebuffer()
	{}
};

typedef RefCountedPtr<RHIFramebuffer> RHIFramebufferPtr;


struct RHIRenderStateDesc
{
	RenderTargetBlendState renderTargetBlendStates[RHI::MaxBoundRenderTargets];

	RenderTargetWriteMask renderTargetWriteMasks[RHI::MaxBoundRenderTargets];

	bool depthEnable = true;
	bool depthWrite = true;
	DepthTestMethod depthTestMethod = DepthTestMethod::DTM_LessEqual;
	//TODO: stencil support

	bool wireframe = false;

	CullMode cullMode = CullMode_Back;
	bool frontIsCCW = true;

	int depthBias = 0;
	float depthBiasClamp = 0.0f;
	float slopeScaledDepthBias = 0.0f;

	bool depthClipEnable = false;
	bool scissorEnable = false;
	bool multisampleEnable = false;
	bool antialiasedLineEnable = false;
};

struct BindingLayoutItem
{
	uint32 slot;

	ResourceType type : 8;
	uint8 unused : 8;

	uint16 size : 16;

	BindingLayoutItem()
		: type(ResourceType::None), slot(0), size(0)
	{}

	BindingLayoutItem(ResourceType type, uint32_t slot, uint16_t size)
		: type(type), slot(slot), size(size)
	{}

	bool operator ==(const BindingLayoutItem& b) const
	{
		return slot == b.slot && type == b.type && size == b.size;
	}

	bool operator !=(const BindingLayoutItem& b) const { return !(*this == b); }
};

struct BindingLayoutDesc
{
	ShaderTypeMasks targetShaders = ShaderTypeMasks::None;
	std::vector<BindingLayoutItem> items;
};

class RHIBindingLayout : public RHIResource
{
public:
	BindingLayoutDesc desc;
};
typedef RefCountedPtr<RHIBindingLayout> RHIBindingLayoutPtr;


struct GraphicsPipelineDesc
{
	PrimitiveTopology primitiveTopology;
	
	MeshVertexComponent vertexComponents[RHI::MaxVertexComponents];
	uint32 numVertexComponents = 0;

	RHIShaderPtr shaders[(int)GraphicsShaderType::Count];
	
	RHIRenderStateDesc renderStateDesc;

	RHIBindingLayoutPtr bindingLayouts[RHI::MaxBindingLayouts];
	uint32 numBindingLayouts = 0;
};

class RHIGraphicsPipeline : public RHIResource
{
public:
	GraphicsPipelineDesc desc;

	const GraphicsPipelineDesc& GetDesc() const { return desc; }
};
typedef RefCountedPtr<RHIGraphicsPipeline> RHIGraphicsPipelinePtr;


struct VertexBufferBinding
{
	RHIBufferPtr buffer;
	uint32 slot;
	uint32 stride;
	uint32 offset;

	bool operator ==(const VertexBufferBinding& b) const
	{
		return buffer == b.buffer && slot == b.slot && stride == b.stride && offset == b.offset;
	}
	bool operator !=(const VertexBufferBinding& b) const { return !(*this == b); }
};


struct IndexBufferBinding
{
	RHIBufferPtr buffer;
	IndexFormat format;
	uint32 offset;

	bool operator ==(const IndexBufferBinding& b) const
	{
		return buffer == b.buffer && format == b.format && offset == b.offset;
	}
	bool operator !=(const IndexBufferBinding& b) const { return !(*this == b); }
};


struct BindingSetItem
{
	ResourceType type;

	uint32 slot;

	RHIResourcePtr resource;

	BindingSetItem()
	{}

	BindingSetItem(const BindingSetItem& other)
		: type(other.type), slot(other.slot), resource(other.resource)
	{}

	BindingSetItem(ResourceType type, uint32 slot, RHIResourcePtr resource)
		: type(type), slot(slot), resource(resource)
	{}

    bool operator ==(const BindingSetItem& b) const
    {
        return resource == b.resource
            && slot == b.slot
            && type == b.type;
    }

    bool operator !=(const BindingSetItem& b) const
    {
        return !(*this == b);
    }
};

struct BindingSetDesc
{
	RHIBindingLayoutPtr layout;

	std::vector<BindingSetItem> items;
};

class RHIBindingSet : public RHIResource
{
public:
	BindingSetDesc desc;
};
typedef RefCountedPtr<RHIBindingSet> RHIBindingSetPtr;


class RHIViewportSet : public RHIResource
{
public:
	RHIViewport viewports[RHI::MaxBoundRenderTargets];
	uint32 numViewports = 0;

	bool AddViewport(const RHIViewport& v)
	{
		if (numViewports < RHI::MaxBoundRenderTargets)
		{
			viewports[numViewports++] = v;
			return true;
		}
		else
			return false;
	}
};
typedef RefCountedPtr<RHIViewportSet> RHIViewportSetPtr;


struct GraphicsState
{
	RHIGraphicsPipelinePtr pipeline;

	RHIFramebufferPtr framebuffer;
	RHIViewportSetPtr viewport;

	float4 blendConstantColor;

	RHIBindingSetPtr bindingSets[RHI::MaxBindingSets];
	uint32 numBindingSets = 0;

	VertexBufferBinding vertexBufferBindings[RHI::MaxVertexBufferBindings];
	uint32 numVertexBufferBindings = 0;

	IndexBufferBinding indexBufferBinding;

	bool AddBindingSet(RHIBindingSetPtr p)
	{
		if (numBindingSets < RHI::MaxBindingSets)
		{
			bindingSets[numBindingSets++] = p;
			return true;
		}
		else
			return false;
	}

	bool AddVertexBufferBinding(const VertexBufferBinding& b)
	{
		if (numVertexBufferBindings < RHI::MaxVertexBufferBindings)
		{
			vertexBufferBindings[numVertexBufferBindings++] = b;
			return true;
		}
		else
			return false;
	}
};

struct DrawArguments
{
	uint32 vertexCount = 0;
	uint32 instanceCount = 1;
	uint32 startIndexLocation = 0;
	uint32 startVertexLocation = 0;
	uint32 startInstanceLocation = 0;
	bool indexed = false;
};
