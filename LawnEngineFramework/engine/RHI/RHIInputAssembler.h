#pragma once

#include "RHICommon.h"
#include "RHIResources.h"

#include <unordered_map>

struct InputLayoutKey
{
	MeshVertexComponent* vertexComponents;
	uint32 numVertexComponents;
	void* vertexShader;

	InputLayoutKey(MeshVertexComponent* vertexComponents, uint32 numVertexComponents, void* vertexShader)
		: vertexComponents(vertexComponents), numVertexComponents(numVertexComponents), vertexShader(vertexShader)
	{
	}
};

struct InputLayoutKeyHash
{
	size_t operator()(const InputLayoutKey& k) const
	{
		size_t vsHash = std::hash<void*>()(k.vertexShader);

		uint32 componentsHash = 0;
		for (size_t i = 0; i < k.numVertexComponents; i++)
		{
			const MeshVertexComponent& c = k.vertexComponents[i];

			uint32 packedValue = c.Pack();
			
			componentsHash = hash_combine<32>::combine(componentsHash, packedValue);
		}

		size_t h = hash_combine<sizeof(size_t) * 8>::combine(vsHash, (size_t)componentsHash);

		return h;
	}
};

struct InputLayoutKeyEqual
{
	bool operator()(const InputLayoutKey& a, const InputLayoutKey& b) const
	{
		if (a.vertexShader != b.vertexShader)
			return false;

		if (a.vertexComponents == b.vertexComponents)
			return true;

		if (a.numVertexComponents != b.numVertexComponents)
			return false;

		for (size_t i = 0; i < a.numVertexComponents; i++)
		{
			const MeshVertexComponent& ca = a.vertexComponents[i];
			const MeshVertexComponent& cb = b.vertexComponents[i];
			if (ca != cb)
			{
				return false;
			}
		}

		return true;
	}
};

class RHIInputLayoutManager
{
public:
	RHIInputLayoutManager();
	virtual ~RHIInputLayoutManager();

	virtual RHIInputLayoutPtr GetOrCreateInputLayout(const MeshVertexComponent* vertexComponents, uint32 numVertexComponents, RHIShaderPtr vertexShader);

protected:
	virtual RHIInputLayoutPtr CreateInputLayout(const MeshVertexComponent* vertexComponents, uint32 numVertexComponents, RHIShaderPtr vertexShader) = 0;
	virtual void ReleaseInputLayout(RHIInputLayoutPtr p) = 0;
	
protected:
	typedef std::unordered_map<InputLayoutKey, RHIInputLayoutPtr, InputLayoutKeyHash, InputLayoutKeyEqual> InputLayoutMap;

	InputLayoutMap inputLayouts;
};
