#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RHICommon.h"

struct ShaderFrequencyType : public EnumObjectType<ShaderFrequency>
{
	ShaderFrequencyType() : EnumObjectType<ShaderFrequency>("ShaderFrequency")
	{
		Add("VertexShader", SF_VertexShader);
		Add("PixelShader", SF_PixelShader);
	}
};
IMPL_ENUM_TYPE(ShaderFrequency, ShaderFrequencyType)


struct PrimitiveTopologyType : public EnumObjectType<PrimitiveTopology>
{
	PrimitiveTopologyType() : EnumObjectType<PrimitiveTopology>("PrimitiveTopology")
	{
		Add("PT_Unknown", PT_Unknown);
		Add("PT_PointList", PT_PointList);
		Add("PT_LineList", PT_LineList);
		Add("PT_LineStrip", PT_LineStrip);
		Add("PT_TriangleList", PT_TriangleList);
		Add("PT_TriangleStrip", PT_TriangleStrip);
	}
};
IMPL_ENUM_TYPE(PrimitiveTopology, PrimitiveTopologyType)


struct MeshVertexBaseFormatType : public EnumObjectType<MeshVertexBaseFormat>
{
	MeshVertexBaseFormatType() : EnumObjectType<MeshVertexBaseFormat>("MeshVertexBaseFormat")
	{
		Add("MVBF_Unknown", MVBF_Unknown);
		Add("MVBF_Float", MVBF_Float);
		Add("MVBF_Byte", MVBF_Byte);
	}
};
IMPL_ENUM_TYPE(MeshVertexBaseFormat, MeshVertexBaseFormatType)


class MeshVertexFormatType : public PrimitiveObjectType<MeshVertexFormat>
{
public:
	MeshVertexFormatType()
		: PrimitiveObjectType<MeshVertexFormat>("MeshVertexFormat")
	{}

	virtual bool Serialize(Serializer& serializer, void* p) const override
	{
		ValueType& obj = *reinterpret_cast<ValueType*>(p);

		serializer.SerializeMemberAuto("baseFormat", obj.baseFormat);
		serializer.SerializeMemberAuto("count", obj.count);

		return true;
	}

	virtual bool Deserialize(Deserializer& deserializer, void* p) const override
	{
		ValueType& obj = *reinterpret_cast<ValueType*>(p);

		deserializer.DeserializeMemberAuto("baseFormat", obj.baseFormat);
		deserializer.DeserializeMemberAuto("count", obj.count);

		return true;
	}
};
IMPL_PRIMITIVE_TYPE(MeshVertexFormat, MeshVertexFormatType);


int GetByteSize(MeshVertexBaseFormat format)
{
	switch (format)
	{
	case MVBF_Float:
		return 4;
	case MVBF_Byte:
		return 1;
	default:
		return 0;
	}
}

uint32 GetByteSize(ShaderVariableType type)
{
	switch (type)
	{
	case SVT_Unknown:
		return 0;
	case SVT_Bool:
		return sizeof(bool);
	case SVT_Int:
		return sizeof(int32);
	case SVT_Float:
		return sizeof(float);
	case SVT_Float2:
		return sizeof(float) * 2;
	case SVT_Float3:
		return sizeof(float) * 3;
	case SVT_Float4:
		return sizeof(float) * 4;
	case SVT_Float4x4:
		return sizeof(float) * 4 * 4;
	default:
		return 0;
	}
}

bool IsCompatibleVertexFormat(MeshVertexFormat providedFormat, MeshVertexFormat reuqiredFormat)
{
	if (providedFormat.count < reuqiredFormat.count)
	{
		return false;
	}

	if (reuqiredFormat.baseFormat == MVBF_Float)
	{
		return providedFormat.baseFormat == MVBF_Float || providedFormat.baseFormat == MVBF_Byte;
	}
	else if (reuqiredFormat.baseFormat == MVBF_Byte)
	{
		return providedFormat.baseFormat == MVBF_Byte;
	}

	return false;
}

int MeshVertexFormat::ByteSize() const
{
	return GetByteSize(baseFormat) * count;
}


struct ShaderVariableTypeEnumType : public EnumObjectType<ShaderVariableType>
{
	ShaderVariableTypeEnumType() : EnumObjectType<ShaderVariableType>("ShaderVariableType")
	{
		Add("SVT_Unknown", SVT_Unknown);

		Add("SVT_Bool", SVT_Bool);
		Add("SVT_Int", SVT_Int);
		Add("SVT_Float", SVT_Float);

		Add("SVT_Float2", SVT_Float2);
		Add("SVT_Float3", SVT_Float3);
		Add("SVT_Float4", SVT_Float4);

		Add("SVT_Float4x4", SVT_Float4x4);
	}
};
IMPL_ENUM_TYPE(ShaderVariableType, ShaderVariableTypeEnumType)


struct InputElementEnumType : public EnumObjectType<InputElement>
{
	InputElementEnumType() : EnumObjectType<InputElement>("InputElement")
	{
		Add("IE_POSITION", IE_POSITION);
		Add("IE_NORMAL", IE_NORMAL);
		Add("IE_TEXCOORD0", IE_TEXCOORD0);
		Add("IE_TEXCOORD1", IE_TEXCOORD1);
		Add("IE_TEXCOORD2", IE_TEXCOORD2);
		Add("IE_TEXCOORD3", IE_TEXCOORD3);
		Add("IE_TEXCOORD4", IE_TEXCOORD4);
		Add("IE_TEXCOORD5", IE_TEXCOORD5);
		Add("IE_TEXCOORD6", IE_TEXCOORD6);
		Add("IE_TEXCOORD7", IE_TEXCOORD7);
		Add("IE_TANGENT", IE_TANGENT);
		Add("IE_BITANGENT", IE_BITANGENT);
		Add("IE_BLENDWEIGHT", IE_BLENDWEIGHT);
		Add("IE_BLENDINDEXES", IE_BLENDINDEXES);
		Add("IE_PIXELSIZE", IE_PIXELSIZE);
		Add("IE_VERTEXCOLOR", IE_VERTEXCOLOR);
	}
};
IMPL_ENUM_TYPE(InputElement, InputElementEnumType)


struct IndexFormatEnumType : public EnumObjectType<IndexFormat>
{
	IndexFormatEnumType() : EnumObjectType<IndexFormat>("IndexFormat")
	{
		Add("IF_UINT16", IF_UINT16);
		Add("IF_UINT32", IF_UINT32);
	}
};
IMPL_ENUM_TYPE(IndexFormat, IndexFormatEnumType)


struct PixelFormatEnumType : public EnumObjectType<PixelFormat>
{
	PixelFormatEnumType() : EnumObjectType<PixelFormat>("PixelFormat")
	{
		Add("PF_R8G8B8A8", PF_R8G8B8A8);
		Add("PF_R8", PF_R8);
		Add("PF_R16G16B16A16_Float", PF_R16G16B16A16_Float);
		Add("PF_R32G32B32A32_Float", PF_R32G32B32A32_Float);
		Add("PF_Depth32", PF_Depth32);
		Add("PF_Depth24Stencil8", PF_Depth24Stencil8);
	}
};
IMPL_ENUM_TYPE(PixelFormat, PixelFormatEnumType)


uint32 GetPixelByteSize(PixelFormat format)
{
	switch (format)
	{
	case PF_R8G8B8A8:
		return sizeof(byte) * 4;
	case PF_R8:
		return sizeof(byte);
	case PF_R16G16B16A16_Float:
		return sizeof(half) * 4;
	case PF_R32G32B32A32_Float:
		return sizeof(float) * 4;
	case PF_Depth32:
	case PF_Depth24Stencil8:
		return sizeof(uint32);
	default:
		CHECK(false && "PixelFormat not supported");
		return 0;
	}
}


struct TextureTypeEnumType : public EnumObjectType<TextureType>
{
	TextureTypeEnumType() : EnumObjectType<TextureType>("PixelFormat")
	{
		Add("Texture2D", TT_Texture2D);
		Add("Texture3D", TT_Texture3D);
		Add("TextureCube", TT_TextureCube);
	}
};
IMPL_ENUM_TYPE(TextureType, TextureTypeEnumType)


struct BlendMultiplierEnumType : public EnumObjectType<BlendMultiplier>
{
	BlendMultiplierEnumType() : EnumObjectType<BlendMultiplier>("BlendMultiplier")
	{
		Add("Zero", BM_Zero);
		Add("One", BM_One);
		Add("SrcColor", BM_SrcColor);
		Add("InvSrcColor", BM_InvSrcColor);
		Add("SrcAlpha", BM_SrcAlpha);
		Add("InvSrcAlpha", BM_InvSrcAlpha);
		Add("DstColor", BM_DstColor);
		Add("InvDstColor", BM_InvDstColor);
		Add("DstAlpha", BM_DstAlpha);
		Add("InvDstAlpha", BM_InvDstAlpha);
		Add("BlendFactor", BM_BlendFactor);
		Add("InvBlendFactor", BM_InvBlendFactor);
	}
};
IMPL_ENUM_TYPE(BlendMultiplier, BlendMultiplierEnumType)


struct BlendOperationEnumType : public EnumObjectType<BlendOperation>
{
	BlendOperationEnumType() : EnumObjectType<BlendOperation>("BlendOperation")
	{
		Add("Add", BO_Add);
		Add("Sub", BO_Sub);
		Add("Min", BO_Min);
		Add("Max", BO_Max);
	}
};
IMPL_ENUM_TYPE(BlendOperation, BlendOperationEnumType)


struct DepthTestMethodEnumType : public EnumObjectType<DepthTestMethod>
{
	DepthTestMethodEnumType() : EnumObjectType<DepthTestMethod>("DepthTestMethod")
	{
		Add("Never", DTM_Never);
		Add("Less", DTM_Less);
		Add("Equal", DTM_Equal);
		Add("LessEqual", DTM_LessEqual);
		Add("Greater", DTM_Greater);
		Add("NotEqual", DTM_NotEqual);
		Add("GreaterEqual", DTM_GreaterEqual);
		Add("Always", DTM_Always);
	}
};
IMPL_ENUM_TYPE(DepthTestMethod, DepthTestMethodEnumType)


struct CullModeEnumType : public EnumObjectType<CullMode>
{
	CullModeEnumType() : EnumObjectType<CullMode>("CullMode")
	{
		Add("CullNone", CullMode_None);
		Add("CullFront", CullMode_Front);
		Add("CullBack", CullMode_Back);
	}
};
IMPL_ENUM_TYPE(CullMode, CullModeEnumType)
