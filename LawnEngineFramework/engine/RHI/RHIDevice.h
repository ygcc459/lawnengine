#pragma once

#include "RHICommon.h"
#include "RHIResources.h"

class RHIShaderCompiler;

class RHICommandList;
typedef RefCountedPtr<RHICommandList> RHICommandListPtr;

class RHIDevice;
typedef RHIDevice* RHIDevicePtr;


class RHICommandList : public RefCountedObject
{
protected:
	RHIDevicePtr device;

public:
	RHICommandList(RHIDevicePtr device)
		: device(device)
	{}

	virtual ~RHICommandList()
	{}

	RHIDevicePtr GetDevice() { return device; }

	virtual void FinishRecording() {}

	virtual void ClearRenderTarget(RHITexturePtr view, const float4& color) {}
	virtual void ClearDepthStencilTarget(RHITexturePtr view, float depth = 1.0f, UINT8 stencil = 0x00) {}

	virtual bool UpdateBuffer(RHIBufferPtr p, const BufferDesc& desc, void* data, size_t size) { return false; }
	virtual bool UpdateBufferData(RHIBufferPtr p, void* data, size_t size) { return false; }

	virtual bool UpdateTexture(RHITexturePtr p, const TextureDesc& desc, void* data) { return false; }

	virtual void SetGraphicsState(const GraphicsState& state) {}
	virtual void Draw(const DrawArguments& arg) {}
};


class RHIDevice : public RefCountedObject
{
public:
	RHIDevice()
	{}

	virtual ~RHIDevice()
	{}

	virtual bool Startup() { return false; }
	virtual void Shutdown() {}

	virtual RHIShaderCompiler* GetShaderCompiler() { return nullptr; }

	virtual RHICommandListPtr CreateCommandList() { return nullptr; }
	virtual void ReleaseCommandList(RHICommandListPtr p) {}
	void SafeReleaseCommandList(RHICommandListPtr& p)
	{
		if (p)
		{
			ReleaseCommandList(p);
			p = nullptr;
		}
	}

	virtual void ExecuteCommandList(RHICommandListPtr p) {}

	virtual RHIInputLayoutPtr CreateInputLayout(const std::vector<MeshVertexComponent>& vertexComponents, RHIShaderPtr vertexShader) { return nullptr; }
	virtual void ReleaseInputLayout(RHIInputLayoutPtr p) {}
	void SafeReleaseInputLayout(RHIInputLayoutPtr& p)
	{
		if (p)
		{
			ReleaseInputLayout(p);
			p = nullptr;
		}
	}

	virtual RHIBufferPtr CreateBuffer(const BufferDesc& desc, const void* init_data = nullptr) { return nullptr; }
	virtual void ReleaseBuffer(RHIBufferPtr p) {}
	void SafeReleaseBuffer(RHIBufferPtr& p)
	{
		if (p)
		{
			ReleaseBuffer(p);
			p = nullptr;
		}
	}

	virtual RHITexturePtr CreateTexture(const TextureDesc& desc, const void* init_data = nullptr) { return nullptr; }
	virtual void ReleaseTexture(RHITexturePtr p) {}
	void SafeReleaseTexture(RHITexturePtr& p)
	{
		if (p)
		{
			ReleaseTexture(p);
			p = nullptr;
		}
	}

	virtual RHIFramebufferPtr CreateFramebuffer(const FramebufferDesc& desc) { return nullptr; }
	virtual void ReleaseFramebuffer(RHIFramebufferPtr p) {}
	void SafeReleaseFramebuffer(RHIFramebufferPtr& p)
	{
		if (p)
		{
			ReleaseFramebuffer(p);
			p = nullptr;
		}
	}

	virtual RHIDisplaySurfacePtr CreateDisplaySurface(void* windowHandle, int w, int h, PixelFormat format) { return nullptr; }
	virtual void UpdateDisplaySurface(RHIDisplaySurfacePtr p, int w, int h) {}
	virtual void ReleaseDisplaySurface(RHIDisplaySurfacePtr p) {}
	virtual void PresentDisplaySurface(RHIDisplaySurfacePtr p) {}

	virtual RHISamplerPtr CreateSampler(const SamplerDesc& desc) { return nullptr; }
	virtual void ReleaseSampler(RHISamplerPtr p) {}
	void SafeReleaseSampler(RHISamplerPtr& p)
	{
		if (p)
		{
			ReleaseSampler(p);
			p = nullptr;
		}
	}

	virtual RHIBindingLayoutPtr CreateBindingLayout(const BindingLayoutDesc& desc) { return nullptr; }
	virtual RHIBindingSetPtr CreateBindingSet(const BindingSetDesc& desc) { return nullptr; }

	virtual RHIGraphicsPipelinePtr CreateGraphicsPipeline(const GraphicsPipelineDesc& desc) { return nullptr; }
	virtual void ReleaseGraphicsPipeline(RHIGraphicsPipelinePtr p) {}
	void SafeReleaseGraphicsPipeline(RHIGraphicsPipelinePtr& p)
	{
		if (p)
		{
			ReleaseGraphicsPipeline(p);
			p = nullptr;
		}
	}
};
