#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RHIInputAssembler.h"

//////////////////////////////////////////////////////////////////////////

// const char* InputElementToSemantic(InputElement t)
// {
// 	static const char* map[] = {
// 		"POSITION", //IE_POSITION = 0,
// 		"NORMAL", //IE_NORMAL,
// 		"TEXCOORD0", //IE_TEXCOORD0,
// 		"TEXCOORD1", //IE_TEXCOORD1,
// 		"TEXCOORD2", //IE_TEXCOORD2,
// 		"TEXCOORD3", //IE_TEXCOORD3,
// 		"TEXCOORD4", //IE_TEXCOORD4,
// 		"TEXCOORD5", //IE_TEXCOORD5,
// 		"TEXCOORD6", //IE_TEXCOORD6,
// 		"TEXCOORD7", //IE_TEXCOORD7,
// 		"TANGENT", //IE_TANGENT,
// 		"BINORMAL", //IE_BINORMAL,
// 		"BLENDWEIGHT", //IE_BLENDWEIGHT,
// 		"BLENDINDICES", //IE_BLENDINDEXES,
// 		"PSIZE", //IE_PIXELSIZE,
// 		"COLOR", //IE_VERTEXCOLOR,
// 		"_IE_COUNT", //_IE_COUNT,  //this should never occur
// 		"_IE_TEXCOORD", //_IE_TEXCOORD,
// 	};
// 
// 	return map[int(t)];
// }

//////////////////////////////////////////////////////////////////////////

RHIInputLayoutManager::RHIInputLayoutManager()
{
}

RHIInputLayoutManager::~RHIInputLayoutManager()
{
}

RHIInputLayoutPtr RHIInputLayoutManager::GetOrCreateInputLayout(const MeshVertexComponent* vertexComponents, uint32 numVertexComponents, RHIShaderPtr vertexShader)
{
	InputLayoutKey key(const_cast<MeshVertexComponent*>(vertexComponents), numVertexComponents, vertexShader.get());

	auto it = inputLayouts.find(key);
	if (it != inputLayouts.end())
	{
		return it->second;
	}
	else
	{
		MeshVertexComponent* duplicatedVertexComponents = new MeshVertexComponent[numVertexComponents];
		memcpy(duplicatedVertexComponents, vertexComponents, numVertexComponents * sizeof(MeshVertexComponent));

		InputLayoutKey newkey(duplicatedVertexComponents, numVertexComponents, vertexShader.get());

		RHIInputLayoutPtr ptr = CreateInputLayout(duplicatedVertexComponents, numVertexComponents, vertexShader);

		inputLayouts.insert(std::make_pair(newkey, ptr));

		return ptr;
	}
}
