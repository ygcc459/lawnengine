#pragma once

#include "RHICommon.h"
#include "RHIResources.h"
#include "PODValueContainer.h"

struct RHIShaderVertexInputDesc
{
	FixedString semanticName;
	int semanticIndex;
	MeshVertexFormat format;
};

struct RHIShaderVariableDesc
{
	FixedString name;
	ShaderVariableType type;
	int byteOffset;  // Offset in constant buffer
	int byteSize;
};

struct RHIShaderCBufferDesc
{
	FixedString name;
	std::vector<RHIShaderVariableDesc> variables;
	int byteSize;
	PODValueContainer variableDefaultValues;
};

struct RHIShaderResourceDesc
{
	FixedString name;
	ShaderResourceType type;
};

struct RHIShaderSamplerDesc
{
	FixedString name;
};

struct RHIShaderCBufferBinding
{
	FixedString cbufferName;
	int bindPoint;
};

struct RHIShaderResourceBinding
{
	FixedString resourceName;
	int bindPoint;
};

struct RHIShaderSamplerBinding
{
	FixedString samplerName;
	int bindPoint;
};

struct RHIShaderReflection
{
	std::vector<RHIShaderVertexInputDesc> vertexInputs;
	std::vector<RHIShaderCBufferDesc> cbuffers;
	std::vector<RHIShaderResourceDesc> resources;
	std::vector<RHIShaderSamplerDesc> samplers;

	std::vector<RHIShaderCBufferBinding> cbufferBindings;
	std::vector<RHIShaderResourceBinding> resourceBindings;
	std::vector<RHIShaderSamplerBinding> samplerBindings;

	RHIShaderReflection()
	{}

	int FindCBufferByName(const FixedString& name) const
	{
		for (int i = 0; i < cbuffers.size(); i++)
			if (cbuffers[i].name == name)
				return i;
		return -1;
	}

	int FindResourceByName(const FixedString& name) const
	{
		for (int i = 0; i < resources.size(); i++)
			if (resources[i].name == name)
				return i;
		return -1;
	}

	int FindSamplerByName(const FixedString& name) const
	{
		for (int i = 0; i < samplers.size(); i++)
			if (samplers[i].name == name)
				return i;
		return -1;
	}
};


class RHIShaderCompiler
{
public:
	RHIShaderCompiler()
	{}

	virtual ~RHIShaderCompiler()
	{}

	virtual RHIShaderPtr CompileShader(
		const AssetPath& shaderPath,
		const std::string& shaderName,
		const std::string& source,
		const std::string& entryPoint,
		const std::map<std::string, std::string>& macros,
		ShaderFrequency freq)
	{
		return nullptr;
	}

};
