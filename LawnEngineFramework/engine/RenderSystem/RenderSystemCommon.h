#pragma once

enum LightType
{
	LT_PointLight = 0,
	LT_DirectionalLight = 1,
	LT_SpotLight = 2,
};

DECL_ENUM_TYPE(LightType);

enum BlendMode
{
	BlendMode_Opaque,
	BlendMode_Transparent,
};

DECL_ENUM_TYPE(BlendMode);

enum class SceneObjectRenderQueueType
{
	Opaque = 0,
	Skybox,
	Transparent,
	Gizmo,
	UI,

	_Count,
};

typedef uint32 SceneRenderPassMask;

#define MAX_SCENE_RENDER_PASSES (32)

enum class SceneRenderPassType
{
	ForwardShading = 0,
	DeferredShaing,
	GBuffer,
	DeferredLighting,
	ShadowCaster,

	_Count,
};

struct DrawCommandCullingInput
{
	float3 boundingBoxCenter;
	float3 boundingBoxExtent;
};

struct DrawCommandSortingKey
{
	uint64 value;

	DrawCommandSortingKey()
		: value(0)
	{}

	DrawCommandSortingKey(SceneObjectRenderQueueType queueType, int32 subOrder)
		: value(0)
	{
		value = (((uint64)queueType) << 32) + subOrder;
	}

	SceneObjectRenderQueueType QueueType() const { return (SceneObjectRenderQueueType)(value >> 32); }

	int32 SubOrder() const { return (int32)((int64)value - (int64)(value & 0xFFFFFFFF00000000)); }
};

struct DrawCommandBatchingKey
{
	uint32 shaderPassID;
	uint32 materialRenderDataID;
	uint32 meshRenderDataID;

	DrawCommandBatchingKey(uint32 shaderPassID, uint32 materialRenderDataID, uint32 meshRenderDataID)
		: shaderPassID(shaderPassID), materialRenderDataID(materialRenderDataID), meshRenderDataID(meshRenderDataID)
	{}

	bool operator < (const DrawCommandBatchingKey& other) const
	{
		if (shaderPassID != other.shaderPassID)
			return shaderPassID < other.shaderPassID;
		else if (materialRenderDataID != other.materialRenderDataID)
			return materialRenderDataID < other.materialRenderDataID;
		else
			return meshRenderDataID < other.meshRenderDataID;
	}
};

class ColorRenderTarget;
typedef std::shared_ptr<ColorRenderTarget> ColorRenderTargetPtr;

class DepthStencilRenderTarget;
typedef std::shared_ptr<DepthStencilRenderTarget> DepthStencilRenderTargetPtr;

class Mesh;
typedef std::shared_ptr<Mesh> MeshPtr;

class IRenderTarget;
typedef std::shared_ptr<IRenderTarget> IRenderTargetPtr;

class Sampler;
typedef std::shared_ptr<Sampler> SamplerPtr;

class Texture;
typedef std::shared_ptr<Texture> TexturePtr;

class Texture2D;
typedef std::shared_ptr<Texture2D> Texture2DPtr;

class Shader;
typedef std::shared_ptr<Shader> ShaderPtr;

class Material;
typedef std::shared_ptr<Material> MaterialPtr;

class RenderView;
typedef std::shared_ptr<RenderView> RenderViewPtr;

class TextureIBLData;
typedef std::shared_ptr<TextureIBLData> TextureIBLDataPtr;
