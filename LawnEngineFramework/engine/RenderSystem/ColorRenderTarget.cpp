#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "ColorRenderTarget.h"

IMPL_CLASS_TYPE(ColorRenderTarget);

ColorRenderTarget::ColorRenderTarget()
	: Texture(TT_Texture2D)
	, width(0)
	, height(0)
	, format(PF_R8G8B8A8)
	, sRGB(false)
{
}

ColorRenderTarget::~ColorRenderTarget()
{
}

void ColorRenderTarget::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("width", &Self::width);
	reflector.AddMember("height", &Self::height);
	reflector.AddMember("format", &Self::format);
	reflector.AddMember("sRGB", &Self::sRGB);
}

void ColorRenderTarget::Create(int width, int height, PixelFormat format, bool sRGB /*= false*/)
{
	this->width = width;
	this->height = height;
	this->format = format;
	this->sRGB = sRGB;
}

void ColorRenderTarget::FillTextureDesc(TextureDesc& desc)
{
	desc.type = TT_Texture2D;
	desc.width = this->width;
	desc.height = this->height;
	desc.format = this->format;
	desc.flags = (TextureFlags)((this->sRGB ? TF_SRGB : TF_None) | TF_ShaderResource | TF_RenderTarget);
}

bool ColorRenderTarget::CreateRHIResources(RHIDevice* device)
{
	TextureDesc desc;
	FillTextureDesc(desc);

	rhiTexture = device->CreateTexture(desc);
	return (rhiTexture != nullptr);
}

bool ColorRenderTarget::UpdateRHIResources(RHICommandList* rhiCommandList)
{
	if (!Texture::UpdateRHIResources(rhiCommandList))
		return false;

	if (!rhiTexture)
	{
		return CreateRHIResources(rhiCommandList->GetDevice());
	}
	else
	{
		TextureDesc desc;
		FillTextureDesc(desc);
		return rhiCommandList->UpdateTexture(rhiTexture, desc, nullptr);
	}
}

void ColorRenderTarget::DestroyRHIResources()
{
	rhiTexture.reset();
}

RHITexturePtr ColorRenderTarget::GetRHITexture()
{
	return rhiTexture;
}

RHITexturePtr ColorRenderTarget::GetRHIRenderTarget()
{
	return rhiTexture;
}
