#pragma once

#include "RenderSystemCommon.h"
#include "RHI/RHICommon.h"

#include <map>

class RHIBufferPool
{
public:
	RHIBufferPool();
	~RHIBufferPool();

	RHIBufferPtr Alloc(const BufferDesc& desc, bool exactSize = false);
	void Free(RHIBufferPtr p);

	void SafeFree(RHIBufferPtr& p)
	{
		if (p)
		{
			Free(p);
			p.reset();
		}
	}

private:
	struct BufferGroupKey
	{
		union
		{
			struct
			{
				BufferBindFlag binding : 16;
				BufferUsage usage : 8;
				CpuAccessFlag cpuAccess : 8;
			};

			uint32 packedValue;
		};

		BufferGroupKey()
			: binding(BBF_VERTEX_BUFFER), usage(BU_DEFAULT), cpuAccess(CAF_NONE)
		{}

		BufferGroupKey(BufferBindFlag binding, BufferUsage usage = BU_DEFAULT, CpuAccessFlag cpuAccess = CAF_NONE)
			: binding(binding), usage(usage), cpuAccess(cpuAccess)
		{}

		bool operator == (const BufferGroupKey& other) const
		{
			return this->packedValue == other.packedValue;
		}

		bool operator < (const BufferGroupKey& other) const
		{
			return this->packedValue < other.packedValue;
		}
	};

	struct RHIBufferKey
	{
		uint32 size;
		void* ptr;  //used to distinguish different buffers with same size

		RHIBufferKey()
			: size(0), ptr(0)
		{}

		RHIBufferKey(uint32 size, void* ptr)
			: size(size), ptr(ptr)
		{}

		bool operator < (const RHIBufferKey& other) const
		{
			if (size != other.size)
				return size < other.size;
			else
				return ptr < other.ptr;
		}
	};

	struct BufferGroup
	{
		std::map<RHIBufferKey, RHIBufferPtr> buffers;  //sorted by size
	};

	std::map<BufferGroupKey, BufferGroup> bufferGroups;
	
};
