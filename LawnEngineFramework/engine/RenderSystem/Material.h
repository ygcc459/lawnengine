#pragma once

#include "Shader.h"
#include "ShaderParamValues.h"

// read-only material render data, used across different threads
class MaterialRenderData : public RefCountedObject
{
	friend class Material;

protected:
	static std::atomic<uint32> nextMaterialRenderDataID;

	uint32 materialRenderDataID;

	ShaderPtr shader;
	ShaderParamValues shaderParamValues;

	struct MaterialParamsBuffer
	{
		ShaderPassPtr shaderPass;
		std::vector<uint8> cpuBuffer;
		RHIBufferPtr rhiBuffer;
	};
	std::vector<MaterialParamsBuffer*> materialParamsBuffers;

	std::unordered_map<uint64, MaterialParamsBuffer*> materialParamsBuffersLayoutHashMap;

public:
	MaterialRenderData();
	~MaterialRenderData();

	uint32 GetMaterialRenderDataID() const { return materialRenderDataID; }

	const ShaderParamValues& GetShaderParamValues() const { return shaderParamValues; }

	ShaderPtr GetShader() { return shader; }

	BlendMode GetBlendMode() const { return shader->GetBlendMode(); }

	void BuildMaterialParamsCpuData(ShaderCBufferSlot* materialParamsCBuffer, std::vector<uint8>& cpuBuffer);

	bool GetOrCreateMaterialParamsRHIBuffer(ShaderPassPtr shaderPass, RHIDevice* rhiDevice, RHIBufferPtr& outMaterialParamsRHICBuffer);

};

typedef RefCountedPtr<MaterialRenderData> MaterialRenderDataPtr;

// read-write material object, used by a one thread at a time
class Material : public Asset
{
	DECL_CLASS_TYPE(Material, Asset);

protected:
	ShaderPtr shader;
	ShaderParamValues shaderParamValues;

	MaterialRenderDataPtr renderData;
	bool renderDataDirty = false;

public:
	Material();
	virtual ~Material();

	virtual void Reflect(Reflector& reflector) override;

	virtual void OnDestroy() override;

	virtual void SetDirty() override;

	ShaderPtr GetShader() { return shader; }

	void Reset();

	virtual void OnCreateNewAsset();
	virtual bool OnLoadAsset();
	virtual bool OnSaveAsset();

	void BindShader(ShaderPtr shader, bool force = false);

	MaterialRenderDataPtr GetMaterialRenderData();

	ShaderParamValues& GetShaderParamValues() { return shaderParamValues; }

public:
	bool HasSetValue(FixedString name) const { return shaderParamValues.HasVariableValue(name); }

	float GetFloat(FixedString name) { return shaderParamValues.GetFloat(name); }
	float2 GetFloat2(FixedString name) { return shaderParamValues.GetFloat2(name); }
	float3 GetFloat3(FixedString name) { return shaderParamValues.GetFloat3(name); }
	float4 GetFloat4(FixedString name) { return shaderParamValues.GetFloat4(name); }
	float4x4 GetFloat4x4(FixedString name) { return shaderParamValues.GetFloat4x4(name); }

	void SetFloat(FixedString name, float value) { shaderParamValues.SetFloat(name, value); }
	void SetFloat2(FixedString name, const float2& value) { shaderParamValues.SetFloat2(name, value); }
	void SetFloat3(FixedString name, const float3& value) { shaderParamValues.SetFloat3(name, value); }
	void SetFloat4(FixedString name, const float4& value) { shaderParamValues.SetFloat4(name, value); }
	void SetFloat4x4(FixedString name, const float4x4& value) { shaderParamValues.SetFloat4x4(name, value); }

	bool HasSetResource(FixedString name) const { return shaderParamValues.HasResource(name); }

	void SetResource(FixedString name, TexturePtr value)
	{
		shaderParamValues.SetResource(name, value);
	}
	TexturePtr GetResource(FixedString name)
	{
		return shaderParamValues.GetResource(name);
	}
};

typedef std::shared_ptr<Material> MaterialPtr;
