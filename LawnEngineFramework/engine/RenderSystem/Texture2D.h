#pragma once

#include "Texture.h"

#include "Image2D.h"

class Texture2D : public Texture
{
	DECL_CLASS_TYPE(Texture2D, Texture);

public:
	int width;
	int height;
	int numMipmaps;
	PixelFormat pixelFormat;
	bool sRGB;

	Image2DMipmaps data;

	bool needUpdateRhiResources;
	RHITexturePtr rhiTexture;

public:
	Texture2D();
	virtual ~Texture2D(void);

	virtual void Reflect(Reflector& reflector) override;
	virtual void AfterDeserialization() override;

	virtual void OnDestroy();

	bool Create(const Image2DMipmaps& data);
	bool Create(int w, int h, int numMipmaps, PixelFormat pixelFormat);

	int2 Size() const { return int2(width, height); }

	Image2DMipmaps& Data()
	{
		return data;
	}
	const Image2DMipmaps& Data() const
	{
		return data;
	}

	void MarkDataDirty();

	void FillTextureDesc(TextureDesc& desc);
	virtual bool CreateRHIResources(RHIDevice* device) override;
	virtual bool UpdateRHIResources(RHICommandList* rhiCommandList) override;
	virtual void DestroyRHIResources() override;

	virtual RHITexturePtr GetRHITexture() override;

	static Texture2DPtr GetWhiteTexture() { return GetPureColorTexture(vcpp::la::color32(255, 255, 255, 255)); }
	static Texture2DPtr GetBlackTexture() { return GetPureColorTexture(vcpp::la::color32(0, 0, 0, 0)); }
	static Texture2DPtr GetFlatNormalTexture() { return GetPureColorTexture(vcpp::la::color32(128, 128, 128, 0)); }
	static Texture2DPtr GetPureColorTexture(vcpp::la::color32 color);

private:
	static std::map<vcpp::la::color32, Texture2DPtr> pureColorTextures;
};

typedef std::shared_ptr<Texture2D> Texture2DPtr;

