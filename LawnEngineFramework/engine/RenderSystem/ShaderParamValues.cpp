#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "ShaderParamValues.h"
#include "RenderSystem/RenderCommands.h"

IMPL_CLASS_TYPE(ShaderParamValues);

ShaderParamValues::ShaderParamValues()
{
}

ShaderParamValues::~ShaderParamValues()
{
	OnDestroy();
}

void ShaderParamValues::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	for (auto it = variables.begin(); it != variables.end(); it++)
	{
		FixedString varName = it->first;
		VariableInfo& varInfo = it->second;

		if (varInfo.type == ShaderVariableType::SVT_Float)
		{
			reflector.AddMemberCustom<float>(varName.c_str(), [=](void* obj)
				{
					return reinterpret_cast<ShaderParamValues*>(obj)->GetVariableValueAddrInternal(varName);
				}).AddAttribute(new SkipSerializationAttribute());
		}
		else if (varInfo.type == ShaderVariableType::SVT_Float2)
		{
			reflector.AddMemberCustom<float2>(varName.c_str(), [=](void* obj)
				{
					return reinterpret_cast<ShaderParamValues*>(obj)->GetVariableValueAddrInternal(varName);
				}).AddAttribute(new SkipSerializationAttribute());
		}
		else if (varInfo.type == ShaderVariableType::SVT_Float3)
		{
			reflector.AddMemberCustom<float3>(varName.c_str(), [=](void* obj)
				{
					return reinterpret_cast<ShaderParamValues*>(obj)->GetVariableValueAddrInternal(varName);
				}).AddAttribute(new SkipSerializationAttribute());
		}
		else if (varInfo.type == ShaderVariableType::SVT_Float4)
		{
			reflector.AddMemberCustom<float4>(varName.c_str(), [=](void* obj)
				{
					return reinterpret_cast<ShaderParamValues*>(obj)->GetVariableValueAddrInternal(varName);
				}).AddAttribute(new SkipSerializationAttribute());
		}
		else if (varInfo.type == ShaderVariableType::SVT_Float4x4)
		{
			reflector.AddMemberCustom<float4x4>(varName.c_str(), [=](void* obj)
				{
					return reinterpret_cast<ShaderParamValues*>(obj)->GetVariableValueAddrInternal(varName);
				}).AddAttribute(new SkipSerializationAttribute());
		}
		else
		{
		}
	}

	for (auto it = resources.begin(); it != resources.end(); it++)
	{
		FixedString resName = it->first;
		TexturePtr resValue = it->second;

		reflector.AddMemberCustom<TexturePtr>(resName.c_str(), [=](void* obj)
			{
				return reinterpret_cast<ShaderParamValues*>(obj)->GetResourceValueAddrInternal(resName);
			}).AddAttribute(new SkipSerializationAttribute());
	}
}

bool ShaderParamValues::Serialize(Serializer& serializer)
{
	if (!Super::Serialize(serializer))
		return false;

	ArchiveNode* node = serializer.CurrentNode();
	
	for (auto it = variables.begin(); it != variables.end(); it++)
	{
		ArchiveNode* variableNode = node->AddChild("variable");

		FixedString varName = it->first;
		VariableInfo& varInfo = it->second;

		variableNode->SetChildValue<std::string>("name", varName.str());
		variableNode->SetChildValue<ShaderVariableType>("type", varInfo.type);

		switch (varInfo.type)
		{
		case ShaderVariableType::SVT_Float:
			variableNode->SetChildValue<float>("value", GetFloat(varName));
			break;
		case ShaderVariableType::SVT_Float2:
			variableNode->SetChildValue<float2>("value", GetFloat2(varName));
			break;
		case ShaderVariableType::SVT_Float3:
			variableNode->SetChildValue<float3>("value", GetFloat3(varName));
			break;
		case ShaderVariableType::SVT_Float4:
			variableNode->SetChildValue<float4>("value", GetFloat4(varName));
			break;
		case ShaderVariableType::SVT_Float4x4:
			variableNode->SetChildValue<float4x4>("value", GetFloat4x4(varName));
			break;
		default:
			CHECK(false);
			break;
		}
	}

	for (auto it = resources.begin(); it != resources.end(); it++)
	{
		FixedString resName = it->first;
		TexturePtr resValue = it->second;

		ArchiveNode* resourceNode = node->AddChild("resource");
		resourceNode->SetChildValue<std::string>("name", resName.str());
		resourceNode->SetChildValue<AssetPath>("value", resValue->GetAssetPath());
	}

	return true;
}

bool ShaderParamValues::Deserialize(Deserializer& deserializer)
{
	if (!Super::Deserialize(deserializer))
		return false;

	ArchiveNode* node = deserializer.CurrentNode();

	for (int iChild = 0; iChild < node->ChildCount(); iChild++)
	{
		ArchiveNode* childNode = node->GetChild(iChild);
		if (childNode->GetName() == "variable")
		{
			FixedString varName = childNode->GetChildValue<std::string>("name");
			ShaderVariableType type = childNode->GetChildValue<ShaderVariableType>("type");

			switch (type)
			{
			case ShaderVariableType::SVT_Float:
				SetFloat(varName, childNode->GetChildValue<float>("value"));
				break;
			case ShaderVariableType::SVT_Float2:
				SetFloat2(varName, childNode->GetChildValue<float2>("value"));
				break;
			case ShaderVariableType::SVT_Float3:
				SetFloat3(varName, childNode->GetChildValue<float3>("value"));
				break;
			case ShaderVariableType::SVT_Float4:
				SetFloat4(varName, childNode->GetChildValue<float4>("value"));
				break;
			case ShaderVariableType::SVT_Float4x4:
				SetFloat4x4(varName, childNode->GetChildValue<float4x4>("value"));
				break;
			default:
				CHECK(false);
				break;
			}
		}
	}

	for (int iChild = 0; iChild < node->ChildCount(); iChild++)
	{
		ArchiveNode* childNode = node->GetChild(iChild);
		if (childNode->GetName() == "resource")
		{
			FixedString varName = childNode->GetChildValue<std::string>("name");
			AssetPath assetPath = childNode->GetChildValue<AssetPath>("value");

			TexturePtr resValue = g_lawnEngine.getAssetManager().Load<Texture>(assetPath);

			if (!resValue)
			{
				Log::Error("Failed to load '%s' texture at path '%s'", varName.c_str(), assetPath.ToString().c_str());
			}

			SetResource(varName, resValue);
		}
	}

	return true;
}

void ShaderParamValues::OnDestroy()
{
	Reset();
}

void ShaderParamValues::Reset()
{
	variantKeys.clear();
	variables.clear();
	variableValueBuffer.clear();
	resources.clear();
	samplers.clear();
}

void ShaderParamValues::CopyFrom(const ShaderParamValues& other)
{
	this->variantKeys = other.variantKeys;
	this->variables = other.variables;
	this->variableValueBuffer = other.variableValueBuffer;
	this->resources = other.resources;
	this->samplers = other.samplers;
}

sh3rgb ShaderParamValues::GetSH3RGB(FixedString name)
{
	PackedSH3RGB packed = *reinterpret_cast<PackedSH3RGB*>(GetVariableValueAddrInternal(name));
	return packed.ToSH3RGB();
}

void ShaderParamValues::SetSH3RGB(FixedString name, const sh3rgb& value)
{
	PackedSH3RGB packed(value);
	SetVariableValue(name, packed);
}

bool ShaderParamValues::SetVariantKey(FixedString name, FixedString value)
{
	variantKeys[name] = value;
	return true;
}

bool ShaderParamValues::HasVariantKey(FixedString name) const
{
	auto it = variantKeys.find(name);
	if (it != variantKeys.end())
		return true;
	else
		return false;
}

FixedString ShaderParamValues::GetVariantKey(FixedString name) const
{
	auto it = variantKeys.find(name);
	if (it != variantKeys.end())
		return it->second;
	else
		return FixedString();
}

bool ShaderParamValues::HasVariableValue(FixedString name) const
{
	auto it = variables.find(name);
	return (it != variables.end());
}

void* ShaderParamValues::GetVariableValueAddrInternal(FixedString name)
{
	auto it = variables.find(name);
	CHECK(it != variables.end());

	VariableInfo& info = it->second;

	return variableValueBuffer.data() + info.offset;
}

bool ShaderParamValues::GetVariableValue(FixedString name, void** outData, uint32* outByteSize, ShaderVariableType* outType)
{
	auto it = variables.find(name);
	if (it != variables.end())
	{
		VariableInfo& info = it->second;

		(*outData) = variableValueBuffer.data() + info.offset;
		(*outByteSize) = info.byteSize;
		(*outType) = info.type;

		return true;
	}
	else
	{
		return false;
	}
}

void* ShaderParamValues::GetVariableValueAddr(FixedString name)
{
	auto it = variables.find(name);
	if (it != variables.end())
	{
		const VariableInfo& info = it->second;
		return variableValueBuffer.data() + info.offset;
	}
	else
	{
		return nullptr;
	}
}

const void* ShaderParamValues::GetVariableValueAddr(FixedString name) const
{
	auto it = variables.find(name);
	if (it != variables.end())
	{
		const VariableInfo& info = it->second;
		return variableValueBuffer.data() + info.offset;
	}
	else
	{
		return nullptr;
	}
}

void ShaderParamValues::SetVariableValue(FixedString name, const void* data, uint32 byteSize, ShaderVariableType type)
{
	CHECK(byteSize != 0);

	auto it = variables.find(name);
	if (it == variables.end())
	{
		it = variables.insert(std::make_pair(name, VariableInfo())).first;
	}

	VariableInfo& info = it->second;

	uint32 newSize = byteSize;
	uint32 prevSize = info.byteSize;
	if (newSize > prevSize)  //allocate new space
	{
		info.offset = variableValueBuffer.size();

		variableValueBuffer.resize(variableValueBuffer.size() + newSize);
	}

	info.byteSize = newSize;
	info.type = type;
	memcpy(variableValueBuffer.data() + info.offset, data, newSize);
}

bool ShaderParamValues::SetResource(FixedString name, TexturePtr value)
{
	auto it = resources.find(name);
	if (it != resources.end())
		resources[name] = value;
	else
		resources.insert(std::make_pair(name, value));

	return true;
}

TexturePtr ShaderParamValues::GetResource(FixedString name) const
{
	auto it = resources.find(name);
	if (it != resources.end())
		return it->second;
	else
		return nullptr;
}

bool ShaderParamValues::SetSampler(FixedString name, SamplerPtr value)
{
	auto it = samplers.find(name);
	if (it != samplers.end())
		samplers[name] = value;
	else
		samplers.insert(std::make_pair(name, value));

	return true;
}

SamplerPtr ShaderParamValues::GetSampler(FixedString name) const
{
	auto it = samplers.find(name);
	if (it != samplers.end())
		return it->second;
	else
		return nullptr;
}

void* ShaderParamValues::GetResourceValueAddrInternal(FixedString name)
{
	auto it = resources.find(name);
	CHECK(it != resources.end());

	TexturePtr& ptr = it->second;

	return &ptr;
}

bool ShaderParamValues::HasResource(FixedString name) const
{
	auto it = resources.find(name);
	return it != resources.end();
}

//////////////////////////////////////////////////////////////////////////

#if 0
IMPL_CLASS_TYPE(GenericShaderParamValues);

GenericShaderParamValues::GenericShaderParamValues()
{
}

GenericShaderParamValues::~GenericShaderParamValues()
{
}

void GenericShaderParamValues::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);
}

void GenericShaderParamValues::OnDestroy()
{
}

void GenericShaderParamValues::BindShader(std::shared_ptr<Shader> shader, ShaderParamValues& outShaderParamValues) const
{
// 	for (auto it = variables.items.begin(); it != variables.items.end(); ++it)
// 	{
// 		FixedString name = it->first;
// 
// 		int dataSize;
// 		const void* data = variables.Get(name.str(), &dataSize);
// 
// 		CHECK(data);
// 
// 		outShaderParamValues.SetVariableValue(name, data, dataSize);
// 	}
// 
// 	for (auto it = resources.begin(); it != resources.end(); ++it)
// 	{
// 		FixedString name = it->first;
// 
// 		TexturePtr value = it->second;
// 
// 		outShaderParamValues.SetResource(name, value);
// 	}
}

void GenericShaderParamValues::Reset()
{
	variables.Clear();
	resources.clear();
}

bool GenericShaderParamValues::GetVariableValue(FixedString name, void* dstBuffer, int byteSize)
{
	void* srcdata = variables.Get(name.str(), &byteSize);
	if (srcdata)
	{
		memcpy(dstBuffer, srcdata, byteSize);
		return true;
	}
	else
	{
		return false;
	}
}

bool GenericShaderParamValues::SetVariableValue(FixedString name, const void* data, int byteSize)
{
	variables.Set(name.str(), data, byteSize);
	return true;
}

bool GenericShaderParamValues::SetResource(FixedString name, TexturePtr value)
{
	auto it = resources.find(name);
	if (it != resources.end())
		resources[name] = value;
	else
		resources.insert(std::make_pair(name, value));

	return true;
}

TexturePtr GenericShaderParamValues::GetResource(FixedString name)
{
	auto it = resources.find(name);
	if (it != resources.end())
		return it->second;
	else
		return nullptr;
}
#endif

//////////////////////////////////////////////////////////////////////////
/*
ShaderParamValueStack::ShaderParamValueStack(ShaderPass* pass)
	: pass(pass), stackSize(0)
{
	for (int i = 0; i < MAX_STACK_SIZE; i++)
		stack[i] = NULL;
}

ShaderParamValueStack::~ShaderParamValueStack()
{
	Clear();
}

void ShaderParamValueStack::Clear()
{
	stackSize = 0;
}

void ShaderParamValueStack::Push(ShaderParamValues* params)
{
	if (stackSize < MAX_STACK_SIZE)
	{
		stack[stackSize] = params;
		++stackSize;
	}
	else
	{
		CHECK(false && "Should set MAX_STACK_SIZE to a larger value");
	}
}

void ShaderParamValueStack::Pop()
{
	if (stackSize > 0)
	{
		--stackSize;
	}
}

void ShaderParamValueStack::SetSize(int size)
{
	CHECK(size >= 0 && size <= MAX_STACK_SIZE);
	stackSize = size;
}

// bind everything to RHI, also generate & upload cbuffers
void ShaderParamValueStack::FillMeshDrawCommand(RenderResourcesManager& renderResourcesManager, MeshDrawCommand* command)
{
	for (int i = 0; i < ShaderFrequency::SF_Count; i++)
	{
		FillMeshDrawCommandForShaderFrequency(renderResourcesManager, command, pass, (ShaderFrequency)i);
	}
}

void ShaderParamValueStack::FillMeshDrawCommandForShaderFrequency(RenderResourcesManager& renderResourcesManager, MeshDrawCommand* command, ShaderPass* pass, ShaderFrequency shaderFrequency)
{                          
	ShaderPass::ShaderFrequencyData& passShaderFreqData = pass->frequencyDatas[shaderFrequency];

	MeshDrawCommand::ShaderFrequencyData& targetShaderFreqData = command->shaderFrequencyDatas[shaderFrequency];

	//
	// buffer
	//
	targetShaderFreqData.rhiBuffers.resize(passShaderFreqData.cbufferSlots.size());
	targetShaderFreqData.cpuBuffers.resize(passShaderFreqData.cbufferSlots.size());
	targetShaderFreqData.rhiBuffersNeedUpload.resize(passShaderFreqData.cbufferSlots.size());

	for (int bindPoint = 0; bindPoint < passShaderFreqData.cbufferSlots.size(); bindPoint++)
	{
		ShaderCBufferSlot* slot = passShaderFreqData.cbufferSlots[bindPoint].get();

		auto& cpuData = targetShaderFreqData.cpuBuffers[bindPoint];
		cpuData.resize(slot->byteSize);
		for (size_t iVar = 0; iVar < slot->variables.size(); iVar++)
		{
			ShaderVariable* var = slot->variables[iVar];
			FixedString varName = var->name;
			void* varDstBuffer = cpuData.data() + var->byteOffset;

			bool found = false;

			for (int i = stackSize - 1; i >= 0; --i)
			{
				ShaderParamValues* params = stack[i];

				void* paramsVarData;
				uint32 paramsByteSize;
				ShaderVariableType paramsType;
				if (params->GetVariableValue(varName, &paramsVarData, &paramsByteSize, &paramsType))
				{
					uint32 copySize = std::min<uint32>(var->byteSize, paramsByteSize);
					memcpy(varDstBuffer, paramsVarData, copySize);
					found = true;
					break;
				}
			}

			if (!found)
			{
				void* defaultVarData = slot->defaultCBufferData.data() + var->byteOffset;
				int defaultVarDataSize = var->byteSize;
				memcpy(varDstBuffer, defaultVarData, defaultVarDataSize);
			}
		}

		BufferDesc bufferDesc = BufferDesc(cpuData.size(), BufferBindFlag::BBF_CONSTANT_BUFFER);
		
		RHIBufferPtr& rhiBuffer = targetShaderFreqData.rhiBuffers[bindPoint];
		renderResourcesManager.rhiDevice->CreateOrUpdateBuffer(rhiBuffer, bufferDesc, cpuData.data(), cpuData.size());

		targetShaderFreqData.rhiBuffersNeedUpload[bindPoint] = true;
	}

	targetShaderFreqData.rhiBuffersNeedRebind = true;

	//
	// resource & sampler
	//
	targetShaderFreqData.rhiResources.resize(passShaderFreqData.resourceSlots.size());
	targetShaderFreqData.cpuResources.resize(passShaderFreqData.resourceSlots.size());

	targetShaderFreqData.rhiSamplers.resize(passShaderFreqData.samplerSlots.size());
	targetShaderFreqData.cpuSamplers.resize(passShaderFreqData.samplerSlots.size());

	for (int bindPoint = 0; bindPoint < passShaderFreqData.resourceSlots.size(); bindPoint++)
	{
		ShaderResourceSlot* resourceSlot = passShaderFreqData.resourceSlots[bindPoint].get();
		FixedString varName = resourceSlot->name;

		TexturePtr ptr = TexturePtr();

		for (int i = stackSize - 1; i >= 0; --i)
		{
			ShaderParamValues* params = stack[i];

			ptr = params->GetResource(varName);
			if (ptr)
			{
				break;
			}
		}

		if (!ptr)
		{
			//Log::Error("Resource is empty for '%s', in technique '%s' of shader '%s'", slot->name.c_str(), pass->technique->name.c_str(), pass->technique->shader->assetPath.ToString().c_str());
			ptr = resourceSlot->defaultValue;
		}

		ptr->UpdateRHIResources();

		targetShaderFreqData.rhiResources[bindPoint] = ptr->GetRHITexture();
		targetShaderFreqData.cpuResources[bindPoint] = ptr;

		if (resourceSlot->samplerSlotIndex >= 0)
		{
			SamplerPtr sampler = ptr->GetSampler();			
			RHISamplerPtr rhiSampler = sampler ? sampler->GetRHISamplerState() : Sampler::LinearClampSampler->GetRHISamplerState();

			targetShaderFreqData.rhiSamplers[resourceSlot->samplerSlotIndex] = rhiSampler;
			targetShaderFreqData.cpuSamplers[resourceSlot->samplerSlotIndex] = sampler;
		}
	}

	for (int bindPoint = 0; bindPoint < passShaderFreqData.samplerSlots.size(); bindPoint++)
	{
		ShaderSamplerSlot* samplerSlot = passShaderFreqData.samplerSlots[bindPoint].get();

		if (!targetShaderFreqData.rhiSamplers[bindPoint])
		{
			CHECK(!targetShaderFreqData.cpuSamplers[bindPoint]);

			targetShaderFreqData.rhiSamplers[bindPoint] = Sampler::LinearClampSampler->GetRHISamplerState();
			targetShaderFreqData.cpuSamplers[bindPoint] = Sampler::LinearClampSampler;
		}
	}

	targetShaderFreqData.rhiResourcesNeedRebind = true;
	targetShaderFreqData.rhiSamplersNeedRebind = true;
}
*/
