#pragma once

#include "RenderScene.h"
#include "RenderPipeline.h"
#include "RenderPass.h"
#include "RenderSystem/ShaderParamValues.h"
#include "RenderCommands.h"
#include "JobSystem/JobSystem.h"

struct SceneRenderPassJobInput
{
	uint32* visibleObjectIndicies;
	std::atomic<uint32>* visibleObjectCount;

	JobPtr cullObjectJob;

	RenderSceneCamera* camera;

	RenderPassCommandPtr passCommand;
};

class SceneRenderPass : public RenderPass
{
protected:
	RenderPipeline* pipeline;
	RenderScene* scene;

	SceneRenderPassType type;

	FixedString shaderTechniqueName;

	ShaderParamValues passParams;

public:
	SceneRenderPass(SceneRenderPassType type, FixedString shaderTechniqueName, RenderPipeline* pipeline, RenderScene* scene)
		: type(type), shaderTechniqueName(shaderTechniqueName), pipeline(pipeline), scene(scene)
	{}

	virtual ~SceneRenderPass()
	{}

	SceneRenderPassType GetType() const { return type; }

	RenderPipeline* GetRenderPipeline() const { return pipeline; }

	RenderScene* GetScene() const { return scene; }

	FixedString GetShaderTechniqueName() const { return shaderTechniqueName; }

	virtual void Prepare(RenderSceneCamera* camera, RHICommandListPtr rhiCommandList) = 0;

	virtual void GetOrCreatePassParamsRHIBuffer(ShaderPassPtr shaderPass, RHIDevice* rhiDevice, RHIBufferPtr& outPassParamsRHICBuffer) = 0;

	const ShaderParamValues* GetPassParams() { return &passParams; }
};
