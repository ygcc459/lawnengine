#pragma once

#include "RenderSystemCommon.h"
#include "RHI/RHIDevice.h"

class RenderSystem
{
	RenderSystem();
	~RenderSystem();

public:
	static RenderSystem& instance() {
		return _instance;
	}

	bool startup();
	void shutdown();
	
	MeshPtr FullScreenQuadMesh();

public:
	RHIDevicePtr rhiDevice;
		
private:
	static RenderSystem _instance;

	MeshPtr fullScreenQuadMesh;
};
