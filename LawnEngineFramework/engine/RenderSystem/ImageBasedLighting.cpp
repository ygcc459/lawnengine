#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "ImageBasedLighting.h"
#include "ImageOperations.h"

IMPL_CLASS_TYPE(TextureIBLData);

TextureIBLData::TextureIBLData()
{
}

TextureIBLData::~TextureIBLData()
{
}

void TextureIBLData::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("diffuseRadianceSH", &Self::diffuseRadianceSH);
	reflector.AddMember("diffuseRadianceTexture", &Self::diffuseRadianceTexture);
	reflector.AddMember("specularReflectionTexture", &Self::specularReflectionTexture);
}

void TextureIBLData::InitFromImage(const Image2D& image)
{
	this->diffuseRadianceSH = ImageOperations::CalcDiffuseRadianceSH(image);

	this->diffuseRadianceTexture.reset(new Texture2D());

	Image2DMipmaps diffuseRadianceImage(64, 64, PixelFormat::PF_R16G16B16A16_Float, 1);
	ImageOperations::CalcDiffuseRadianceTexture(image, diffuseRadianceImage.GetMipmap(0));

	this->diffuseRadianceTexture->Create(diffuseRadianceImage);
	this->diffuseRadianceTexture->sRGB = true;
}
