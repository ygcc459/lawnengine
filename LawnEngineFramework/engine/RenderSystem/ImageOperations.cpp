#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "ImageOperations.h"
#include <random>
#include <cmath>
#include <chrono>

#include "EntitySystem/World.h"
#include "EntitySystem/GizmoRenderer.h"

template<PixelFormat pixelFormat>
static void ResampleImpl(const Image2D& src, Image2D& dst)
{
	typedef PixelFormatToType<pixelFormat>::Value PixelType;
	Image2DSamplerStatic<PixelType, pixelFormat, SamplerFilter::SF_Bilinear, SamplerAddressing::SA_Repeat> srcSampler;
	PixelAccess<pixelFormat> dstWriter;

	for (int y = 0; y < dst.height; y++)
	{
		float v = y / (float)(src.height - 1);

		for (int x = 0; x < dst.width; x++)
		{
			float u = x / (float)(src.width - 1);

			PixelType pixel = srcSampler.Sample(src, float2(u, v));
			dstWriter.Write(dst, x, y, pixel);
		}
	}
}

bool ImageOperations::Resample(const Image2D& src, Image2D& dst)
{
	if (src.width <= 1 || src.height <= 1)
	{
		Log::Error("ImageOperations::Downsample src image too small: w=%d h=%d", src.width, src.height);
		return false;
	}

	switch (src.pixelFormat)
	{
	case PF_R32G32B32A32_Float:
		ResampleImpl<PF_R32G32B32A32_Float>(src, dst);
		break;
	case PF_R8G8B8A8:
		ResampleImpl<PF_R8G8B8A8>(src, dst);
		break;
	case PF_R8:
		ResampleImpl<PF_R8>(src, dst);
		break;
	case PF_R16G16B16A16_Float:
		ResampleImpl<PF_R16G16B16A16_Float>(src, dst);
		break;
	default:
		Log::Error("ImageOperations::Downsample unsupported pixel format: %d", src.pixelFormat);
		return false;
	}

	return true;
}

bool ImageOperations::GenMipmaps(Image2DMipmaps& mipmaps)
{
	assert(mipmaps.numMipmaps > 0);

	Image2D& lastMip = mipmaps.GetMipmap(0);

	for (int mipmapIndex = 1; mipmapIndex < mipmaps.numMipmaps; mipmapIndex++)
	{
		Image2D& curMip = mipmaps.GetMipmap(mipmapIndex);
		if (!Resample(lastMip, curMip))
			return false;
	}

	return true;
}

template<PixelFormat pixelFormat>
sh3rgb CalcDiffuseRadianceSHImpl(const Image2D& src)
{
	const uint32 sh_samples_count = 128;
	const uint32 radiance_samples_count = 128;

	Image2DSamplerStatic<float4, pixelFormat, SamplerFilter::SF_Bilinear, SamplerAddressing::SA_Repeat> srcSampler;

	std::vector<std::pair<float3, float3>> samples;  //(normal direction, radiance)
	samples.resize(radiance_samples_count);

	GizmoRenderer* gizmoRenderer = g_lawnEngine.getWorld().GetSingletonComponent<GizmoRenderer>();

	auto sampleAtDirection = [&](const float3& direction)->float3
	{
		float3 N = direction;

		double3 irradiance = double3(0, 0, 0);

		// tangent space calculation from origin point
		float3 up = float3(0.0, 1.0, 0.0);
		float3 right = normalize(cross(up, N));
		up = normalize(cross(N, right));

		float sampleDelta = 0.025f;
		uint32 nrSamples = 0;
		for (float phi = 0.0; phi < 2.0 * PI; phi += sampleDelta)
		{
			for (float theta = 0.0; theta < 0.5 * PI; theta += sampleDelta)
			{
				float3 tangentSample = float3(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));

				float3 sampleVec = tangentSample.x * right + tangentSample.y * up + tangentSample.z * N;

				geovec2<float> gv = direction_to_geovec_zup(sampleVec).latitude_longitude();

				float2 uv;
				uv.x = math::saturate(math::lerp(gv.longitude, -math::pi<float>(), math::pi<float>(), 0.0f, 1.0f));
				uv.y = 1.0f - math::saturate(math::lerp(gv.latitude, latitude_min_radians(), latitude_max_radians(), 0.0f, 1.0f));

				float3 radiance = srcSampler.Sample(src, uv).xyz() * cos(theta) * sin(theta);

				irradiance += double3(radiance.x, radiance.y, radiance.z);
				nrSamples++;
			}
		}

		irradiance = PI * irradiance / float(nrSamples);

		return float3((float)irradiance.x, (float)irradiance.y, (float)irradiance.z);
	};

	sh_color3_projector_sphere_zup<float, 3> projector;

	sh3rgb diffuseRadianceSH = projector.project_function(sh_samples_count,
		[&](const float3& direction)->float3
		{
			float3 result = sampleAtDirection(direction);
			return result;
		});

	return diffuseRadianceSH;
}

sh3rgb ImageOperations::CalcDiffuseRadianceSH(const Image2D& src)
{
	switch (src.pixelFormat)
	{
	case PF_R32G32B32A32_Float:
		return CalcDiffuseRadianceSHImpl<PF_R32G32B32A32_Float>(src);
	case PF_R8G8B8A8:
		return CalcDiffuseRadianceSHImpl<PF_R8G8B8A8>(src);
	case PF_R16G16B16A16_Float:
		return CalcDiffuseRadianceSHImpl<PF_R16G16B16A16_Float>(src);
	default:
		Log::Error("ImageOperations::CalcDiffuseRadianceSH unsupported pixel format: %d", src.pixelFormat);
		return {};
	}
}

template<PixelFormat srcPixelFormat, PixelFormat dstPixelFormat>
void CalcDiffuseRadianceTextureImpl(const Image2D& src, Image2D& dst)
{
	const uint32 radiance_samples_count = 256;

	Image2DSamplerStatic<float4, srcPixelFormat, SamplerFilter::SF_Point, SamplerAddressing::SA_Repeat> srcSampler;

	std::vector<std::pair<float3, float3>> samples;  //(normal direction, radiance)
	samples.resize(radiance_samples_count);

	auto sampleAtDirection = [&](const float3& direction)->float3
	{
		float3 N = direction;

		double3 irradiance = double3(0, 0, 0);

		// tangent space calculation from origin point
		float3 up = float3(0.0, 1.0, 0.0);
		float3 right = normalize(cross(up, N));
		up = normalize(cross(N, right));

		float sampleDelta = 0.025f;
		uint32 nrSamples = 0;
		for (float phi = 0.0; phi < 2.0 * PI; phi += sampleDelta)
		{
			for (float theta = 0.0; theta < 0.5 * PI; theta += sampleDelta)
			{
				float3 tangentSample = float3(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));
				
				float3 sampleVec = tangentSample.x * right + tangentSample.y * up + tangentSample.z * N;

				geovec2<float> gv = direction_to_geovec_zup(sampleVec).latitude_longitude();

				float2 uv;
				uv.x = math::saturate(math::lerp(gv.longitude, -math::pi<float>(), math::pi<float>(), 0.0f, 1.0f));
				uv.y = 1.0f - math::saturate(math::lerp(gv.latitude, latitude_min_radians(), latitude_max_radians(), 0.0f, 1.0f));

				float3 radiance = srcSampler.Sample(src, uv).xyz() * cos(theta) * sin(theta);
				
				irradiance += double3(radiance.x, radiance.y, radiance.z);
				nrSamples++;
			}
		}

		irradiance = PI * irradiance / float(nrSamples);

		return float3((float)irradiance.x, (float)irradiance.y, (float)irradiance.z);
	};

	typedef PixelFormatToType<dstPixelFormat>::Value PixelType;
	PixelAccess<dstPixelFormat> dstWriter;

	for (int y = 0; y < dst.height; y++)
	{
		float v = y / (dst.height - 1.0f);

		for (int x = 0; x < dst.width; x++)
		{
			float u = x / (dst.width - 1.0f);

			float longitude = math::lerp(u, 0.0f, 1.0f , -math::pi<float>(), math::pi<float>());  //[0, 1] to [-pi, pi]
			float latitude = math::lerp(v, 0.0f, 1.0f, latitude_min_radians(), latitude_max_radians());

			float3 direction = geovec_to_direction_zup(geovec2(latitude, longitude));

			float4 color(sampleAtDirection(direction), 1);

			dstWriter.WriteWithConvert(dst, x, y, color);
		}
	}
}

void ImageOperations::CalcDiffuseRadianceTexture(const Image2D& src, Image2D& dst)
{
	if (dst.width < 2 || dst.height < 2)
	{
		Log::Error("ImageOperations::CalcDiffuseRadianceTexture dst image too small");
		return;
	}

	if (dst.pixelFormat == PF_R16G16B16A16_Float)
	{
		switch (src.pixelFormat)
		{
		case PF_R32G32B32A32_Float:
			CalcDiffuseRadianceTextureImpl<PF_R32G32B32A32_Float, PF_R16G16B16A16_Float>(src, dst);
			break;
		case PF_R8G8B8A8:
			CalcDiffuseRadianceTextureImpl<PF_R8G8B8A8, PF_R16G16B16A16_Float>(src, dst);
			break;
		case PF_R16G16B16A16_Float:
			CalcDiffuseRadianceTextureImpl<PF_R16G16B16A16_Float, PF_R16G16B16A16_Float>(src, dst);
			break;
		default:
			Log::Error("ImageOperations::CalcDiffuseRadianceTexture unsupported pixel format: %d", src.pixelFormat);
			return;
		}
	}
	else if (dst.pixelFormat == PF_R8G8B8A8)
	{
		switch (src.pixelFormat)
		{
		case PF_R32G32B32A32_Float:
			CalcDiffuseRadianceTextureImpl<PF_R32G32B32A32_Float, PF_R8G8B8A8>(src, dst);
			break;
		case PF_R8G8B8A8:
			CalcDiffuseRadianceTextureImpl<PF_R8G8B8A8, PF_R8G8B8A8>(src, dst);
			break;
		case PF_R16G16B16A16_Float:
			CalcDiffuseRadianceTextureImpl<PF_R16G16B16A16_Float, PF_R8G8B8A8>(src, dst);
			break;
		default:
			Log::Error("ImageOperations::CalcDiffuseRadianceTexture unsupported pixel format: %d", src.pixelFormat);
			return;
		}
	}
	else
	{
		Log::Error("ImageOperations::CalcDiffuseRadianceTexture unsupported dst pixel format: %d", dst.pixelFormat);
		return;
	}
}

#if 0
float DistributionGGX(float3 N, float3 H, float roughness)
{
	float a = roughness * roughness;
	float a2 = a * a;
	float NdotH = std::max(dot(N, H), 0.0f);
	float NdotH2 = NdotH * NdotH;

	float nom = a2;
	float denom = (NdotH2 * (a2 - 1.0) + 1.0);
	denom = PI * denom * denom;

	return nom / denom;
}

// http://holger.dammertz.org/stuff/notes_HammersleyOnHemisphere.html
// efficient VanDerCorpus calculation.
float RadicalInverse_VdC(uint bits)
{
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}

float2 Hammersley(uint i, uint N)
{
	return float2(float(i) / float(N), RadicalInverse_VdC(i));
}

float3 ImportanceSampleGGX(float2 Xi, float3 N, float roughness)
{
	float a = roughness * roughness;

	float phi = 2.0 * PI * Xi.x;
	float cosTheta = sqrt((1.0 - Xi.y) / (1.0 + (a * a - 1.0) * Xi.y));
	float sinTheta = sqrt(1.0 - cosTheta * cosTheta);

	// from spherical coordinates to cartesian coordinates - halfway vector
	float3 H;
	H.x = cos(phi) * sinTheta;
	H.y = sin(phi) * sinTheta;
	H.z = cosTheta;

	// from tangent-space H vector to world-space sample vector
	float3 up = abs(N.z) < 0.999 ? float3(0.0, 0.0, 1.0) : float3(1.0, 0.0, 0.0);
	float3 tangent = normalize(cross(up, N));
	float3 bitangent = cross(N, tangent);

	float3 sampleVec = tangent * H.x + bitangent * H.y + N * H.z;
	return normalize(sampleVec);
}

template<PixelFormat srcPixelFormat>
float3 CalcSpecularReflectionAtDirection(const Image2DMipmaps& srcImage, const float3& N, float roughness)
{
	Image2DSamplerStatic<float4, srcPixelFormat, SamplerFilter::SF_Point, SamplerAddressing::SA_Repeat> srcSampler;

	// make the simplifying assumption that V equals R equals the normal 
	float3 R = N;
	float3 V = R;

	const uint SAMPLE_COUNT = 1024u;

	float3 prefilteredColor = float3(0, 0, 0);
	float totalWeight = 0.0;

	for (uint i = 0u; i < SAMPLE_COUNT; ++i)
	{
		// generates a sample vector that's biased towards the preferred alignment direction (importance sampling).
		float2 Xi = Hammersley(i, SAMPLE_COUNT);
		float3 H = ImportanceSampleGGX(Xi, N, roughness);
		float3 L = normalize(2.0f * dot(V, H) * H - V);

		float NdotL = std::max(dot(N, L), 0.0f);
		if (NdotL > 0.0)
		{
			// sample from the environment's mip level based on roughness/pdf
			float D = DistributionGGX(N, H, roughness);
			float NdotH = std::max(dot(N, H), 0.0f);
			float HdotV = std::max(dot(H, V), 0.0f);
			float pdf = D * NdotH / (4.0 * HdotV) + 0.0001;

			float resolution = 512.0; // resolution of source cubemap (per face)
			float saTexel = 4.0 * PI / (6.0 * resolution * resolution);
			float saSample = 1.0 / (float(SAMPLE_COUNT) * pdf + 0.0001);

			float mipLevel = roughness == 0.0 ? 0.0 : 0.5 * log2(saSample / saTexel);

			prefilteredColor += srcSampler.SampleLod(srcImage, L, mipLevel).rgb * NdotL;
			totalWeight += NdotL;
		}
	}

	prefilteredColor = prefilteredColor / totalWeight;

	return prefilteredColor;
}

#endif

template<PixelFormat pixelFormat>
void CalcSpecularReflectionTextureImpl(const Image2D& src, Image2DMipmaps& outMipmaps)
{
	//TODO
}

void ImageOperations::CalcSpecularReflectionTexture(const Image2D& src, Image2DMipmaps& outMipmaps)
{
	switch (src.pixelFormat)
	{
	case PF_R32G32B32A32_Float:
		CalcSpecularReflectionTextureImpl<PF_R32G32B32A32_Float>(src, outMipmaps);
		break;
	case PF_R8G8B8A8:
		CalcSpecularReflectionTextureImpl<PF_R8G8B8A8>(src, outMipmaps);
		break;
	case PF_R16G16B16A16_Float:
		CalcSpecularReflectionTextureImpl<PF_R16G16B16A16_Float>(src, outMipmaps);
		break;
	default:
		Log::Error("ImageOperations::CalcSpecularReflectionTexture unsupported pixel format: %d", src.pixelFormat);
	}
}
