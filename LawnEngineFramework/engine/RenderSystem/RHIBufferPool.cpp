#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "RHIBufferPool.h"

RHIBufferPool::RHIBufferPool()
{
}

RHIBufferPool::~RHIBufferPool()
{
}

RHIBufferPtr RHIBufferPool::Alloc(const BufferDesc& desc, bool exactSize /*= false*/)
{
	BufferGroupKey key(desc.binding, desc.usage, desc.cpuAccess);

	auto it = bufferGroups.find(key);
	if (it == bufferGroups.end())
	{
		it = bufferGroups.insert(std::make_pair(key, BufferGroup())).first;
	}

	BufferGroup& bufferGroup = it->second;

	auto& buffers = bufferGroup.buffers;

	std::map<RHIBufferKey, RHIBufferPtr>::iterator bufferIt;
	if (exactSize)
		bufferIt = buffers.find(RHIBufferKey(desc.size, nullptr));
	else
		bufferIt = buffers.lower_bound(RHIBufferKey(desc.size, nullptr));

	if (bufferIt != buffers.end())
	{
		RHIBufferPtr result = bufferIt->second;

		buffers.erase(bufferIt);

		return result;
	}
	else
	{
		RHIBufferPtr result = RenderSystem::instance().rhiDevice->CreateBuffer(desc, NULL);

		return result;
	}
}

void RHIBufferPool::Free(RHIBufferPtr p)
{
	if (!p)
		return;

	const BufferDesc& desc = p->desc;

	BufferGroupKey key(desc.binding, desc.usage, desc.cpuAccess);

	auto it = bufferGroups.find(key);
	if (it == bufferGroups.end())
	{
		it = bufferGroups.insert(std::make_pair(key, BufferGroup())).first;
	}

	BufferGroup& bufferGroup = it->second;

	auto& buffers = bufferGroup.buffers;

	buffers.insert(std::make_pair(RHIBufferKey(desc.size, p.get()), p));
}
