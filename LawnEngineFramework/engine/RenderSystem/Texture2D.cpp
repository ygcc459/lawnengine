#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "Texture2D.h"
#include "../AssetSystem/TextureImporter.h"

#include "Streams.h"

IMPL_CLASS_TYPE(Texture2D);

std::map<vcpp::la::color32, Texture2DPtr> Texture2D::pureColorTextures;

Texture2D::Texture2D()
	:Texture(TT_Texture2D)
	, rhiTexture(nullptr)
	, width(0)
	, height(0)
	, numMipmaps(1)
	, pixelFormat(PF_R8G8B8A8)
	, needUpdateRhiResources(false)
	, sRGB(false)
{
}

Texture2D::~Texture2D(void)
{
}

void Texture2D::AfterDeserialization()
{
	MarkDataDirty();
}

void Texture2D::OnDestroy()
{
	this->rhiTexture.reset();

	this->needUpdateRhiResources = false;

	this->width = 0;
	this->height = 0;

	this->data.Destroy();

	BaseObject::OnDestroy();
}

bool Texture2D::Create(const Image2DMipmaps& data)
{
	this->OnDestroy();

	this->width = data.width;
	this->height = data.height;
	this->numMipmaps = data.numMipmaps;
	this->pixelFormat = data.pixelFormat;

	this->data.Assign(data, true);

	MarkDataDirty();

	return true;
}

bool Texture2D::Create(int w, int h, int numMipmaps, PixelFormat pixelFormat)
{
	this->OnDestroy();

	this->width = w;
	this->height = h;
	this->numMipmaps = numMipmaps;
	this->pixelFormat = pixelFormat;

	this->data.Assign(width, height, pixelFormat, numMipmaps);

	MarkDataDirty();

	return true;
}

void Texture2D::MarkDataDirty()
{
	this->needUpdateRhiResources = true;
}

void Texture2D::FillTextureDesc(TextureDesc& desc)
{
	desc.type = TT_Texture2D;
	desc.width = this->width;
	desc.height = this->height;
	desc.mipmaps = this->numMipmaps;
	desc.format = this->pixelFormat;
	desc.flags = TF_ShaderResource;

	if (sRGB)
		desc.flags = (TextureFlags)(desc.flags | TF_SRGB);
}

bool Texture2D::CreateRHIResources(RHIDevice* device)
{
	TextureDesc textureDesc;
	FillTextureDesc(textureDesc);

	rhiTexture = device->CreateTexture(textureDesc, this->data.buffer);
	return (rhiTexture != nullptr);
}

bool Texture2D::UpdateRHIResources(RHICommandList* rhiCommandList)
{
	if (!Texture::UpdateRHIResources(rhiCommandList))
		return false;

	if (needUpdateRhiResources)
	{
		needUpdateRhiResources = false;

		if (!rhiTexture)
		{
			return CreateRHIResources(rhiCommandList->GetDevice());
		}
		else
		{
			TextureDesc textureDesc;
			FillTextureDesc(textureDesc);
			return rhiCommandList->UpdateTexture(rhiTexture, textureDesc, this->data.buffer);
		}
	}

	return true;
}

void Texture2D::DestroyRHIResources()
{
	rhiTexture.reset();
}

RHITexturePtr Texture2D::GetRHITexture()
{
	return rhiTexture;
}

Texture2DPtr Texture2D::GetPureColorTexture(vcpp::la::color32 color)
{
	auto it = pureColorTextures.find(color);
	if (it != pureColorTextures.end())
	{
		return it->second;
	}
	else
	{
		vector4<byte> c = vcpp::la::to_vector4<byte>(color);

		Image2DMipmaps image(4, 4, PF_R8G8B8A8, 1);
		for (int y = 0; y < image.height; y++)
		{
			for (int x = 0; x < image.width; x++)
			{
				PixelAccess<PF_R8G8B8A8>::Write(image.GetMipmap(0), x, y, c);
			}
		}

		Texture2DPtr tex = std::make_shared<Texture2D>();
		tex->Create(image);

		pureColorTextures.insert(std::make_pair(color, tex));

		return tex;
	}
}

void Texture2D::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("width", &Self::width);
	reflector.AddMember("height", &Self::height);
	reflector.AddMember("pixelFormat", &Self::pixelFormat);
	reflector.AddMember("sRGB", &Self::sRGB);

	reflector.AddMember("data", &Self::data);
}
