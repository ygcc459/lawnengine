#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "Shader.h"
#include "AssetSystem/Archive.h"
#include "AssetSystem/AssetManager.h"
#include "Streams.h"
#include "ShaderParamValues.h"
#include "RenderCommands.h"

#include "RHI/RHIShader.h"

//////////////////////////////////////////////////////////////////////////

struct ShaderPropertyTypeEnumType : public EnumObjectType<ShaderPropertyType>
{
	ShaderPropertyTypeEnumType() : EnumObjectType<ShaderPropertyType>("ShaderPropertyType")
	{
		Add("VariantKey", SPT_VariantKey);
		Add("Color", SPT_Color);
		Add("Float", SPT_Float);
		Add("Float2", SPT_Float2);
		Add("Float3", SPT_Float3);
		Add("Float4", SPT_Float4);
		Add("Texture1D", SPT_Texture1D);
		Add("Texture2D", SPT_Texture2D);
		Add("Texture3D", SPT_Texture3D);
		Add("TextureCube", SPT_TextureCube);
	}
};
IMPL_ENUM_TYPE(ShaderPropertyType, ShaderPropertyTypeEnumType)

//////////////////////////////////////////////////////////////////////////

IMPL_CLASS_TYPE(Shader);

Shader::Shader()
	: blendMode(BlendMode_Opaque)
{
}

Shader::~Shader()
{
}

void Shader::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);
}

bool Shader::OnCreate()
{
	return true;
}

bool Shader::OnLoad(Archive& archive)
{
	return Compile(archive);
}

void Shader::OnDestroy()
{
}

ShaderTechnique* Shader::FindTechnique(FixedString name)
{
	for (int i = 0; i < this->techniques.size(); i++)
	{
		ShaderTechnique* tech = this->techniques[i].get();
		if (tech->name == name)
		{
			return tech;
		}
	}

	return nullptr;
}

ShaderTechnique* Shader::DefaultTechnique()
{
	if (this->techniques.size() > 0)
		return this->techniques[0].get();
	else
		return nullptr;
}

ShaderParamGroup Shader::GetCBufferShaderParamGroup(FixedString name)
{
	auto it = cbufferNameToGroupMapping.find(name);
	if (it != cbufferNameToGroupMapping.end())
		return it->second;
	else
		return ShaderParamGroup::PassParams;
}

ShaderParamGroup Shader::GetResourceShaderParamGroup(FixedString name)
{
	auto it = resourceNameToGroupMapping.find(name);
	if (it != resourceNameToGroupMapping.end())
		return it->second;
	else
		return ShaderParamGroup::PassParams;
}

ShaderParamGroup Shader::GetSamplerShaderParamGroup(FixedString name)
{
	auto it = samplerNameToGroupMapping.find(name);
	if (it != samplerNameToGroupMapping.end())
		return it->second;
	else
		return ShaderParamGroup::PassParams;
}

AssetPath Shader::FindShaderInclude(const AssetPath& shaderPath, const char* includeFileName)
{
	AssetManager& assetManager = g_lawnEngine.getAssetManager();

	AssetPath finalPath;

	finalPath = shaderPath.ToAbsolutePath().Parent() / includeFileName;
	if (!finalPath.Exists())
	{
		finalPath = assetManager.ProjectDir() / "Shaders" / includeFileName;
	}

	if (finalPath.Exists())
		return finalPath;
	else
		return AssetPath();
}

bool Shader::Compile(Archive& archive)
{
	Log::Info("Compiling shader: %s", this->GetAssetPath().ToString().c_str());

	std::string sourceCode = archive.Root()->GetChildValue<std::string>("source");
	
	if (sourceCode.empty())
	{
		Log::Error("Failed to find source code in source node: %s", this->GetAssetPath().ToString().c_str());
		return false;
	}

	if (!ParseShader(archive.Root(), sourceCode))
	{
		Log::Error("Failed to parse shader: %s", this->GetAssetPath().ToString().c_str());
		return false;
	}

	Log::Info("Compile shader success: %s", this->GetAssetPath().ToString().c_str());

	return true;
}

bool Shader::ParseShader(ArchiveNode* shaderNode, const std::string& source)
{
	blendMode = shaderNode->GetChildValue<BlendMode>("BlendMode");

	ParseShaderProperties(shaderNode);

	for (int i = 0; i < shaderNode->ChildCount(); ++i)
	{
		auto node = shaderNode->GetChild(i);

		if (node->GetName() == "technique")
		{
			ParseShaderTechnique(node, source);
		}
	}

	return true;
}

bool Shader::ParseShaderProperties(ArchiveNode* shaderNode)
{
	ArchiveNode* propertiesNode = shaderNode->GetChild("properties");
	if (propertiesNode)
	{
		for (int i = 0; i < propertiesNode->ChildCount(); ++i)
		{
			ArchiveNode* propertyNode = propertiesNode->GetChild(i);

			FixedString propName = propertyNode->GetName().ToStr();

			ShaderProperty* prop = new ShaderProperty();
			prop->name = propName;
			prop->type = propertyNode->GetChildValue<ShaderPropertyType>("Type");
			
			std::string defaultValueStr = propertyNode->GetChildValue<std::string>("Default", "");
			if (defaultValueStr.size() != 0)
			{
				prop->hasDefaultValue = true;

				switch (prop->type)
				{
				case SPT_VariantKey:
					prop->defaultValueVariantKey = defaultValueStr;
					break;
				case SPT_Color:
					GetStaticType<float4>()->FromString(defaultValueStr, &prop->defaultValueFloat4);
					break;
				case SPT_Float:
					GetStaticType<float>()->FromString(defaultValueStr, &prop->defaultValueFloat4);
					break;
				case SPT_Float2:
					GetStaticType<float2>()->FromString(defaultValueStr, &prop->defaultValueFloat4);
					break;
				case SPT_Float3:
					GetStaticType<float3>()->FromString(defaultValueStr, &prop->defaultValueFloat4);
					break;
				case SPT_Float4:
					GetStaticType<float4>()->FromString(defaultValueStr, &prop->defaultValueFloat4);
					break;
				case SPT_Texture1D:
					break;
				case SPT_Texture2D:
				{
					if (defaultValueStr.size() != 0)
					{
						const char* parsePtr = defaultValueStr.c_str();
						const char* strEnd = defaultValueStr.c_str() + defaultValueStr.size();

						if (vcpp::parsers::startsWith(parsePtr, strEnd, "color_texture", &parsePtr))
						{
							float4 colorValue = float4(0, 0, 0, 0);
							if (vcpp::parsers::parseFloat(&parsePtr, colorValue.x)
								&& vcpp::parsers::parseFloat(&parsePtr, colorValue.y)
								&& vcpp::parsers::parseFloat(&parsePtr, colorValue.z)
								&& vcpp::parsers::parseFloat(&parsePtr, colorValue.w))
							{
								color32 colorValue32 = vcpp::la::to_color32(colorValue);
								prop->defaultValueTexture = std::static_pointer_cast<Texture>(Texture2D::GetPureColorTexture(colorValue32));
							}
							else
							{
								Log::Error("Failed to parse color_texture '%s' in shader property '%s'", defaultValueStr.c_str(), propName.c_str());
							}
						}
						else
						{
							AssetPath defaultTexPath = AssetPath::FromString(defaultValueStr);

							prop->defaultValueTexture = g_lawnEngine.getAssetManager().Load<Texture>(defaultTexPath);
							if (!prop->defaultValueTexture)
							{
								Log::Error("Failed to load texture at '%s' in shader property '%s'", defaultTexPath.ToString().c_str(), propName.c_str());
							}
						}
					}

					if (!prop->defaultValueTexture)
					{
						prop->defaultValueTexture = Texture2D::GetPureColorTexture(color32(255, 255, 255, 255));
					}
					break;
				}
				case SPT_Texture3D:
					break;
				case SPT_TextureCube:
					break;
				}
			}

			std::string rangeValueStr = propertyNode->GetChildValue<std::string>("Range", "");
			if (rangeValueStr.size() != 0)
			{
				prop->ranged = true;

				switch (prop->type)
				{
				case SPT_Float:
					GetStaticType<float2>()->FromString(rangeValueStr, &prop->floatRange);
					break;
				}
			}

			this->properties.insert(std::make_pair(prop->name, std::unique_ptr<ShaderProperty>(prop)));
		}
	}

	return true;
}

bool Shader::ParseShaderTechnique(ArchiveNode* techniqueNode, const std::string& source)
{
	ShaderTechnique* technique = new ShaderTechnique(this);
	techniques.push_back(std::unique_ptr<ShaderTechnique>(technique));

	technique->name = techniqueNode->GetChildValue<std::string>("name", "default_technique");

	ParseShaderVariants(*technique, techniqueNode, source);

	ShaderVariantKeys tempKeys;
	tempKeys.BindCollection(&technique->variantCollection);
	CreateShaderVariants(*technique, tempKeys, 0, techniqueNode, source);

	return true;
}

bool Shader::ParseShaderVariants(ShaderTechnique& technique, ArchiveNode* techiniqueNode, const std::string& rawsource)
{
	ArchiveNode* variantsNode = techiniqueNode->GetChild("variants");
	if (variantsNode)  //parse dimensions
	{
		for (int i = 0; i < variantsNode->ChildCount(); ++i)
		{
			auto dimensionNode = variantsNode->GetChild(i);

			if (dimensionNode->GetName() == "dimension")
			{
				std::string dimensionName = dimensionNode->GetValue<std::string>();

				ShaderVariantDimension* dim = technique.variantCollection.AddDimension(dimensionName);

				dim->defaultValueIndex = 0;

				for (int j = 0; j < dimensionNode->ChildCount(); ++j)
				{
					auto node = dimensionNode->GetChild(j);
					if (node->GetName() == "value")
					{
						dim->AddValue(node->GetValue<std::string>());
					}
				}
			}
		}
	}
	else  //don't have variants node, add a default dimension
	{
		ShaderVariantDimension* dim = technique.variantCollection.AddDimension("defaultDimension");
		dim->defaultValueIndex = 0;
		dim->AddValue("0");
	}

	technique.variantCollection.BuildValueBitMasks();

	return true;
}

bool Shader::CreateShaderVariants(ShaderTechnique& technique, ShaderVariantKeys& keys, size_t dimensionIndex, ArchiveNode* techniqueNode, const std::string& source)
{
	ShaderVariantDimension* dim = technique.variantCollection.dimensions[dimensionIndex];

	for (size_t valueIndex = 0; valueIndex < dim->values.size(); valueIndex++)
	{
		keys.SetValue(dimensionIndex, valueIndex);

		if (dimensionIndex + 1 < technique.variantCollection.dimensions.size())
		{
			bool result = CreateShaderVariants(technique, keys, dimensionIndex + 1, techniqueNode, source);
			if (!result)
			{
				return false;
			}
		}
		else
		{
			if (!CreateShaderVariant(technique, keys, techniqueNode, source))
			{
				return false;
			}
		}
	}

	return true;
}

bool Shader::CreateShaderVariant(ShaderTechnique& technique, const ShaderVariantKeys& keys, ArchiveNode* techniqueNode, const std::string& source)
{
	ShaderVariant* variant = new ShaderVariant(&technique);
	variant->keys = keys;

	ShaderVariantKeysCompressed keyCompressed = variant->keys.GetCompressedKey();

	technique.variants.insert(std::make_pair(keyCompressed, std::unique_ptr<ShaderVariant>(variant)));

	std::string variantName = "";
	for (size_t iDimension = 0; iDimension < variant->keys.collection->dimensions.size(); iDimension++)
	{
		ShaderVariantDimension* dimension = variant->keys.collection->dimensions[iDimension];
		uint32 dimensionValueIndex = variant->keys.dimensionValueIndices[iDimension];

		FixedString dimensionName = dimension->name;
		FixedString dimensionValue = dimension->values[dimensionValueIndex];

		if (iDimension != 0)
			variantName += ",";
		variantName += formatString("%s=%s", dimensionName.c_str(), dimensionValue.c_str());		
	}

	variant->name = variantName;

	Log::Info(" Compiling variant: %s", variantName.c_str());

	for (int i = 0; i < techniqueNode->ChildCount(); ++i)
	{
		auto node = techniqueNode->GetChild(i);

		if (node->GetName() == "pass")
		{
			ParseShaderPass(*variant, node, source);
		}
	}

	return true;
}

bool Shader::ParseShaderPass(ShaderVariant& variant, ArchiveNode* passNode, const std::string& source)
{
	RHIDevicePtr rhiDevice = RenderSystem::instance().rhiDevice;
	RHIShaderCompiler* rhiShaderCompiler = rhiDevice->GetShaderCompiler();

	std::string shaderName = this->GetAssetPath().ToString();

	ShaderPassPtr pass(new ShaderPass(&variant));
	variant.passes.push_back(pass);

	std::map<std::string, std::string> macros;
	for (size_t iDimension = 0; iDimension < variant.keys.collection->dimensions.size(); iDimension++)
	{
		ShaderVariantDimension* dimension = variant.keys.collection->dimensions[iDimension];
		uint32 dimensionValueIndex = variant.keys.dimensionValueIndices[iDimension];

		FixedString dimensionName = dimension->name;
		std::string dimensionValueNumeric = formatString("%d", dimensionValueIndex);

		macros.insert(std::make_pair(dimensionName.str(), dimensionValueNumeric));

		for (size_t valueIndex = 0; valueIndex < dimension->values.size(); valueIndex++)
		{
			FixedString valueName = dimension->values[valueIndex];
			std::string numericValue = formatString("%d", valueIndex);
			macros.insert(std::make_pair(valueName.str(), numericValue));
		}
	}
	
	if (!ParseShaderParamGroups(*pass, passNode))
	{
		Log::Error("ParseShaderParamGroups failed");
		return false;
	}

	if (!ParseSamplerFromTexture(*pass, passNode))
	{
		Log::Error("ParseSamplerFromTexture failed");
		return false;
	}

	std::string entryPoints[] =
	{
		passNode->GetChildValue<std::string>("vs_entry", "vs_entry"),
		passNode->GetChildValue<std::string>("ps_entry", "ps_entry"),
	};

	for (int iFreq = 0; iFreq < ShaderFrequency::SF_Count; ++iFreq)
	{
		ShaderFrequency freq = (ShaderFrequency)iFreq;

		ShaderPass::ShaderFrequencyData& shaderFreqData = pass->frequencyDatas[freq];

		const std::string& entryPoint = entryPoints[freq];

		std::string shaderNameDetail = formatString("%s (%s: %s)", shaderName.c_str(), ToString<ShaderFrequency>(freq).c_str(), entryPoint.c_str());

		shaderFreqData.rhiShader = rhiShaderCompiler->CompileShader(this->GetAssetPath(), shaderNameDetail, source, entryPoint, macros, freq);

		if (!shaderFreqData.rhiShader)
		{
			Log::Error("Compile shader failed");
			return false;
		}

		if (!BuildResourcesAndBindings(freq, *pass))
		{
			Log::Error("BuildCBuffersAndResources failed");
			return false;
		}
	}

	//
	// input layout
	//
	RHIShaderPtr rhiVsShader = pass->frequencyDatas[ShaderFrequency::SF_VertexShader].rhiShader;
	ParseRequiredVertexComponents(*pass, rhiVsShader);
	
	//
	// pipeline state
	//
	ParseRenderStateDesc(*pass, passNode);

	pass->blendConstantColor = passNode->GetChildValue<float4>("BlendConstantColor", float4(0, 0, 0, 0));

	pass->isValid = true;

	return true;
}

bool Shader::ParseRenderStateDesc(ShaderPass& pass, ArchiveNode* node)
{
	RHIRenderStateDesc& desc = pass.renderStateDesc;

	//TODO: support multiple render targets
	for (int i = 0; i < RHI::MaxBoundRenderTargets; i++)
	{
		desc.renderTargetBlendStates[i].blendEnable = node->GetChildValue<bool>("BlendEnable", false);
		desc.renderTargetBlendStates[i].srcBlend = node->GetChildValue<BlendMultiplier>("SrcBlend", BM_SrcAlpha);
		desc.renderTargetBlendStates[i].dstBlend = node->GetChildValue<BlendMultiplier>("DstBlend", BM_InvSrcAlpha);
		desc.renderTargetBlendStates[i].blendOp = node->GetChildValue<BlendOperation>("BlendOp", BO_Add);
		desc.renderTargetBlendStates[i].srcBlendAlpha = node->GetChildValue<BlendMultiplier>("SrcBlendAlpha", BM_Zero);
		desc.renderTargetBlendStates[i].dstBlendAlpha = node->GetChildValue<BlendMultiplier>("DstBlendAlpha", BM_One);
		desc.renderTargetBlendStates[i].blendOpAlpha = node->GetChildValue<BlendOperation>("BlendOpAlpha", BO_Add);

		std::string RTMaskStr = stringToLower(node->GetChildValue<std::string>("WriteMask", "rgba"));
		RenderTargetWriteMask writeMask = RTWM_None;
		if (RTMaskStr.find('r') != std::string::npos) writeMask = (RenderTargetWriteMask)(writeMask | RTWM_RED);
		if (RTMaskStr.find('g') != std::string::npos) writeMask = (RenderTargetWriteMask)(writeMask | RTWM_GREEN);
		if (RTMaskStr.find('b') != std::string::npos) writeMask = (RenderTargetWriteMask)(writeMask | RTWM_BLUE);
		if (RTMaskStr.find('a') != std::string::npos) writeMask = (RenderTargetWriteMask)(writeMask | RTWM_ALPHA);
		desc.renderTargetWriteMasks[i] = writeMask;
	}

	desc.depthEnable = node->GetChildValue<bool>("DepthEnable", true);
	desc.depthWrite = node->GetChildValue<bool>("DepthWrite", true);
	desc.depthTestMethod = node->GetChildValue<DepthTestMethod>("DepthTest", DTM_LessEqual);

	desc.wireframe = false;

	desc.cullMode = node->GetChildValue<CullMode>("Cull", CullMode_Back);
	desc.frontIsCCW = node->GetChildValue<bool>("FrontIsCCW", true);

	desc.depthBias = node->GetChildValue<int>("DepthBias", 0);
	desc.depthBiasClamp = node->GetChildValue<float>("DepthBiasClamp", 0.0f);
	desc.slopeScaledDepthBias = node->GetChildValue<float>("SlopeScaledDepthBias", 0.0f);

	desc.depthClipEnable = node->GetChildValue<bool>("DepthClipEnable", true);
	desc.scissorEnable = false;
	desc.multisampleEnable = false;
	desc.antialiasedLineEnable = false;

	return true;
}

bool Shader::ParseShaderParamGroups(ShaderPass& pass, ArchiveNode* node)
{
	const char* shaderParamGroupNodeName[] =
	{
		"ObjectParams",
		"MaterialParams",
		"PassParams",
	};

	for (int iGroup = 0; iGroup < (int)ShaderParamGroup::Count; iGroup++)
	{
		ShaderParamGroup group = (ShaderParamGroup)iGroup;

		const char* groupName = shaderParamGroupNodeName[iGroup];

		ArchiveNode* groupNode = node->GetChild(groupName);
		if (groupNode)
		{
			for (int iChildNode = 0; iChildNode < groupNode->ChildCount(); iChildNode++)
			{
				ArchiveNode* childNode = groupNode->GetChild(iChildNode);

				if (childNode->GetName() == "cbuffer")
					cbufferNameToGroupMapping.insert(std::make_pair(FixedString(childNode->GetValue().ToStr()), group));
				else if (childNode->GetName() == "resource")
					resourceNameToGroupMapping.insert(std::make_pair(FixedString(childNode->GetValue().ToStr()), group));
				else if (childNode->GetName() == "sampler")
					samplerNameToGroupMapping.insert(std::make_pair(FixedString(childNode->GetValue().ToStr()), group));
			}
		}
	}

	return true;
}

bool Shader::ParseSamplerFromTexture(ShaderPass& pass, ArchiveNode* node)
{
	samplerToTextureMapping.clear();

	for (int iChildNode = 0; iChildNode < node->ChildCount(); iChildNode++)
	{
		ArchiveNode* childNode = node->GetChild(iChildNode);
		if (childNode->GetName() == "SamplerFromTexture")
		{
			std::string s = childNode->GetValue().ToStr();

			std::vector<std::string> args;
			splitString(s, " \n\t", args, false);

			FixedString samplerName = args[0];
			FixedString textureName = args[1];
			samplerToTextureMapping.insert(std::make_pair(samplerName, textureName));
		}		
	}

	//for (auto it = samplerToTextureMapping.begin(); it != samplerToTextureMapping.end(); it++)
	//{
	//	FixedString samplerName = it->first;
	//	FixedString resourceName = it->second;
	//
	//	ShaderResourceSlot* resourceSlot = FindResourceSlot(resourceName);
	//	if (!resourceSlot)
	//		Log::Error("Can not find resource named '%s' in SamplerFromTexture property in shader '%s'", resourceName.c_str(), this->GetAssetPath().ToString().c_str());
	//
	//	ShaderSamplerSlot* samplerSlot = FindSamplerSlot(samplerName);
	//	if (!samplerSlot)
	//		Log::Error("Can not find sampler named '%s' in SamplerFromTexture property in shader '%s'", samplerName.c_str(), this->GetAssetPath().ToString().c_str());
	//
	//	if (samplerSlot && resourceSlot)
	//	{
	//		samplerSlot->resourceSlotIndex = resourceSlot->index;
	//		resourceSlot->samplerSlotIndex = samplerSlot->index;
	//	}
	//}

	return true;
}

ShaderCBufferSlot* ShaderPass::CreateCBuffer(const RHIShaderCBufferDesc& rhiCBDesc)
{
	Shader* shader = variant->technique->shader;

	//
	// create cbuffer
	//
	ShaderCBufferSlot* cbuffer = new ShaderCBufferSlot();
	cbuffer->name = rhiCBDesc.name;
	cbuffer->byteSize = rhiCBDesc.byteSize;
	cbuffer->defaultCBufferData.resize(rhiCBDesc.byteSize, 0);
	cbuffer->index = (uint32)cbuffers.size();
	cbuffer->group = shader->GetCBufferShaderParamGroup(cbuffer->name);
	cbuffers.push_back(cbuffer);

	uint64 layoutHash = 0;

	//
	// create variables
	//
	for (int varIndex = 0; varIndex < rhiCBDesc.variables.size(); varIndex++)
	{
		const RHIShaderVariableDesc& rhiVarDesc = rhiCBDesc.variables[varIndex];

		ShaderVariable* variable = FindVariable(rhiVarDesc.name);
		if (!variable)
		{
			variable = new ShaderVariable();
			variable->name = rhiVarDesc.name;
			variable->index = varIndex;
			variable->type = rhiVarDesc.type;
			variable->cbuffer = cbuffer;
			variable->byteOffset = rhiVarDesc.byteOffset;
			variable->byteSize = rhiVarDesc.byteSize;

			bool defaultValueAssigned = false;

			auto propertyIt = shader->properties.find(variable->name);
			if (propertyIt != shader->properties.end())  //higher priority for shader property
			{
				ShaderProperty& prop = *propertyIt->second;
				if (prop.hasDefaultValue)
				{
					memcpy(cbuffer->defaultCBufferData.data() + variable->byteOffset, &prop.defaultValueFloat4, rhiVarDesc.byteSize);
					defaultValueAssigned = true;
				}
			}

			if (!defaultValueAssigned)
			{
				const void* defaultValueSrcPtr;
				if ((defaultValueSrcPtr = rhiCBDesc.variableDefaultValues.Get(variable->name.str())) != nullptr)  //lower priority for shader source code
				{
					memcpy(cbuffer->defaultCBufferData.data() + variable->byteOffset, defaultValueSrcPtr, rhiVarDesc.byteSize);
					defaultValueAssigned = true;
				}
			}

			variables.push_back(variable);

			cbuffer->variables.push_back(variable);

			layoutHash = hash_combine<64>::combine(layoutHash, (uint64)variable->name.index);
			layoutHash = hash_combine<64>::combine(layoutHash, (uint64)variable->type);
			layoutHash = hash_combine<64>::combine(layoutHash, (uint64)variable->byteOffset);
			layoutHash = hash_combine<64>::combine(layoutHash, (uint64)variable->byteSize);
		}
	}

	cbuffer->layoutHash = layoutHash;

	return cbuffer;
}

ShaderResourceSlot* ShaderPass::CreateResource(const RHIShaderResourceDesc& rhiResDesc)
{
	Shader* shader = variant->technique->shader;

	//
	// create resource
	//
	ShaderResourceSlot* resSlot = new ShaderResourceSlot();
	resSlot->name = rhiResDesc.name;
	resSlot->index = (uint32)resources.size();
	resSlot->type = rhiResDesc.type;
	resSlot->group = shader->GetResourceShaderParamGroup(resSlot->name);

	bool defaultValueAssigned = false;

	auto propertyIt = shader->properties.find(resSlot->name);
	if (propertyIt != shader->properties.end())
	{
		ShaderProperty& prop = *propertyIt->second;
		if (prop.hasDefaultValue)
		{
			resSlot->defaultValue = prop.defaultValueTexture;
			defaultValueAssigned = true;
		}
	}

	if (!defaultValueAssigned)
	{
		switch (resSlot->type)
		{
		case ShaderResourceType::SRT_Texture1D:
			CHECK(false);
			break;
		case ShaderResourceType::SRT_Texture2D:
			resSlot->defaultValue = Texture2D::GetPureColorTexture(color32(255, 255, 255, 255));
			break;
		case ShaderResourceType::SRT_Texture3D:
			CHECK(false);
			break;
		case ShaderResourceType::SRT_TextureCube:
			CHECK(false);
			break;
		}
	}

	resources.push_back(resSlot);

	return resSlot;
}

ShaderSamplerSlot* ShaderPass::CreateSampler(const RHIShaderSamplerDesc& rhiSamplerDesc)
{
	Shader* shader = variant->technique->shader;

	//
	// create sampler
	//
	ShaderSamplerSlot* samplerSlot = new ShaderSamplerSlot();
	samplerSlot->name = rhiSamplerDesc.name;
	samplerSlot->index = (uint32)samplers.size();
	samplerSlot->group = shader->GetSamplerShaderParamGroup(samplerSlot->name);

	auto it = shader->samplerToTextureMapping.find(samplerSlot->name);
	if (it != shader->samplerToTextureMapping.end())
	{
		FixedString textureName = it->second;

		samplerSlot->resourceSlot = FindResource(textureName);
		if (samplerSlot->resourceSlot == nullptr)
			Log::Error("ShaderPass::CreateSampler did not find resource with name '%s' for sampler '%s'", textureName.c_str(), samplerSlot->name.c_str());
	}

	samplers.push_back(samplerSlot);

	return samplerSlot;
}

ShaderCBufferSlot* ShaderPass::GetPassParamsCBuffer()
{
	if (passParamsCBufferIndex >= 0 && passParamsCBufferIndex < cbuffers.size())
	{
		ShaderCBufferSlot* passParamsCBuffer = cbuffers[passParamsCBufferIndex];
		return passParamsCBuffer;
	}
	else
	{
		return nullptr;
	}
}

ShaderCBufferSlot* ShaderPass::GetMaterialParamsCBuffer()
{
	if (materialParamsCBufferIndex >= 0 && materialParamsCBufferIndex < cbuffers.size())
	{
		ShaderCBufferSlot* materialParamsCBuffer = cbuffers[materialParamsCBufferIndex];
		return materialParamsCBuffer;
	}
	else
	{
		return nullptr;
	}
}

bool ShaderPass::IsRHIResourcesCreated() const
{
	for (int iFreq = 0; iFreq < ShaderFrequency::SF_Count; ++iFreq)
	{
		ShaderFrequency freq = (ShaderFrequency)iFreq;

		const ShaderPass::ShaderFrequencyData& shaderFreqData = frequencyDatas[freq];

		if (!shaderFreqData.rhiBindingLayout)
			return false;
	}

	return true;
}

bool ShaderPass::CreateRHIResources(RHIDevice* rhiDevice)
{
	for (int iFreq = 0; iFreq < ShaderFrequency::SF_Count; ++iFreq)
	{
		ShaderFrequency freq = (ShaderFrequency)iFreq;

		ShaderPass::ShaderFrequencyData& shaderFreqData = frequencyDatas[freq];

		std::vector<BindingLayoutItem> bindings;

		BindingLayoutDesc bindingLayoutDesc;
		switch (freq)
		{
		case SF_VertexShader:
			bindingLayoutDesc.targetShaders = ShaderTypeMasks::Vertex;
			break;
		case SF_PixelShader:
			bindingLayoutDesc.targetShaders = ShaderTypeMasks::Pixel;
			break;
		}

		bindingLayoutDesc.items.reserve(shaderFreqData.bindingItems.size());
		for (const ShaderBindingItem& item : shaderFreqData.bindingItems)
			bindingLayoutDesc.items.push_back(item.bindingLayout);

		RHIBindingLayoutPtr rhiBindingLayout = rhiDevice->CreateBindingLayout(bindingLayoutDesc);
		shaderFreqData.rhiBindingLayout = rhiBindingLayout;
	}

	return true;
}

bool ShaderPass::UpdateRHIResources(RHICommandList* rhiCommandList)
{
	if (rhiResourcesDirty)
	{
		rhiResourcesDirty = false;
		return CreateRHIResources(rhiCommandList->GetDevice());
	}

	return true;
}

void ShaderPass::DestroyRHIResources()
{
	for (int iFreq = 0; iFreq < ShaderFrequency::SF_Count; ++iFreq)
	{
		ShaderFrequency freq = (ShaderFrequency)iFreq;
		ShaderPass::ShaderFrequencyData& shaderFreqData = frequencyDatas[freq];
		shaderFreqData.rhiBindingLayout = nullptr;
	}
}

bool Shader::BuildResourcesAndBindings(ShaderFrequency freq, ShaderPass& pass)
{
	ShaderPass::ShaderFrequencyData& shaderFreqData = pass.frequencyDatas[freq];

	const RHIShaderReflection& rhiShaderReflection = *shaderFreqData.rhiShader->reflection;

	pass.passParamsCBufferIndex = (uint32)-1;
	pass.passParamsLayoutHash = 0;

	pass.materialParamsCBufferIndex = (uint32)-1;
	pass.materialParamsLayoutHash = 0;

	for (size_t i = 0; i < rhiShaderReflection.cbuffers.size(); i++)
	{
		const RHIShaderCBufferDesc& rhiCBDesc = rhiShaderReflection.cbuffers[i];

		ShaderCBufferSlot* cbuffer = pass.FindCBuffer(rhiCBDesc.name);
		if (!cbuffer)
			cbuffer = pass.CreateCBuffer(rhiCBDesc);

		BindingLayoutItem bindingLayoutItem(ResourceType::ConstantBuffer, i, cbuffer->byteSize);
		shaderFreqData.bindingItems.push_back(ShaderPass::ShaderBindingItem(cbuffer, bindingLayoutItem));

		if (cbuffer->group == ShaderParamGroup::PassParams)  //currently supports only one pass-params-cbuffer per shader
		{
			pass.passParamsCBufferIndex = cbuffer->index;
			pass.passParamsLayoutHash = cbuffer->layoutHash;
		}
		else if (cbuffer->group == ShaderParamGroup::MaterialParams)  //currently supports only one material-params-cbuffer per shader
		{
			pass.materialParamsCBufferIndex = cbuffer->index;
			pass.materialParamsLayoutHash = cbuffer->layoutHash;
		}
	}

	for (size_t i = 0; i < rhiShaderReflection.resources.size(); i++)
	{
		const RHIShaderResourceDesc& rhiResDesc = rhiShaderReflection.resources[i];

		ShaderResourceSlot* resSlot = pass.FindResource(rhiResDesc.name);
		if (!resSlot)
			resSlot = pass.CreateResource(rhiResDesc);

		BindingLayoutItem bindingLayoutItem(ResourceType::Texture_SRV, i, 0);
		shaderFreqData.bindingItems.push_back(ShaderPass::ShaderBindingItem(resSlot, bindingLayoutItem));
	}

	for (size_t i = 0; i < rhiShaderReflection.samplers.size(); i++)
	{
		const RHIShaderSamplerDesc& rhiSamplerDesc = rhiShaderReflection.samplers[i];

		ShaderSamplerSlot* samplerSlot = pass.FindSampler(rhiSamplerDesc.name);
		if (!samplerSlot)
			samplerSlot = pass.CreateSampler(rhiSamplerDesc);

		BindingLayoutItem bindingLayoutItem(ResourceType::Sampler, i, 0);
		shaderFreqData.bindingItems.push_back(ShaderPass::ShaderBindingItem(samplerSlot, bindingLayoutItem));
	}

	return true;
}

bool Shader::ParseRequiredVertexComponents(ShaderPass& pass, RHIShaderPtr rhiVsShader)
{
	const std::vector<RHIShaderVertexInputDesc>& rhiVertexInputDescs = rhiVsShader->reflection->vertexInputs;

	pass.requiredVertexComponents.reserve(rhiVertexInputDescs.size());

	for (const auto& rhiVertexInputDesc : rhiVertexInputDescs)
	{
		ShaderRequiredVertexComponentDesc desc;
		desc.inputElement = GetInputElement(rhiVertexInputDesc.semanticName, rhiVertexInputDesc.semanticIndex);
		desc.format = rhiVertexInputDesc.format;

		pass.requiredVertexComponents.push_back(desc);
	}

	return true;
}

InputElement Shader::GetInputElement(FixedString semanticName, int semanticIndex)
{
	struct MapObject
	{
		std::map<std::string, InputElement> map;

		MapObject()
		{
			map.insert(std::make_pair("POSITION0", IE_POSITION));
			map.insert(std::make_pair("NORMAL0", IE_NORMAL));
			map.insert(std::make_pair("TEXCOORD0", IE_TEXCOORD0));
			map.insert(std::make_pair("TEXCOORD1", IE_TEXCOORD1));
			map.insert(std::make_pair("TEXCOORD2", IE_TEXCOORD2));
			map.insert(std::make_pair("TEXCOORD3", IE_TEXCOORD3));
			map.insert(std::make_pair("TEXCOORD4", IE_TEXCOORD4));
			map.insert(std::make_pair("TEXCOORD5", IE_TEXCOORD5));
			map.insert(std::make_pair("TEXCOORD6", IE_TEXCOORD6));
			map.insert(std::make_pair("TEXCOORD7", IE_TEXCOORD7));
			map.insert(std::make_pair("TANGENT0", IE_TANGENT));
			map.insert(std::make_pair("BITANGENT0", IE_BITANGENT));
			map.insert(std::make_pair("BLENDWEIGHT0", IE_BLENDWEIGHT));
			map.insert(std::make_pair("BLENDINDICES0", IE_BLENDINDEXES));
			map.insert(std::make_pair("PSIZE0", IE_PIXELSIZE));
			map.insert(std::make_pair("COLOR0", IE_VERTEXCOLOR));
		}
	};
	static MapObject map_object;

	std::string str = formatString("%s%d", semanticName.c_str(), semanticIndex);

	auto it = map_object.map.find(str);
	if (it != map_object.map.end())
		return it->second;
	else
		return IE_UNKNOWN;
}

std::atomic<uint32> ShaderPass::nextShaderPassID(0);

ShaderPass::ShaderPass(ShaderVariant* variant)
	: isValid(false)
	, variant(variant)
	, shaderPassID(nextShaderPassID++)
{}

ShaderPass::~ShaderPass()
{}

void ShaderPass::Destroy()
{
	for (ShaderCBufferSlot* v : cbuffers)
		delete v;
	cbuffers.clear();

	for (ShaderVariable* v : variables)
		delete v;
	variables.clear();

	for (ShaderResourceSlot* v : resources)
		delete v;
	resources.clear();

	for (ShaderSamplerSlot* v : samplers)
		delete v;
	samplers.clear();
}

ShaderCBufferSlot* ShaderPass::FindCBuffer(FixedString name)
{
	for (auto v : cbuffers)
		if (v->name == name)
			return v;
	return nullptr;
}
ShaderVariable* ShaderPass::FindVariable(FixedString name)
{
	for (auto v : variables)
		if (v->name == name)
			return v;
	return nullptr;
}
ShaderResourceSlot* ShaderPass::FindResource(FixedString name)
{
	for (auto v : resources)
		if (v->name == name)
			return v;
	return nullptr;
}
int32 ShaderPass::FindResourceIndex(FixedString name)
{
	for (size_t i = 0; i < resources.size(); i++)
		if (resources[i]->name == name)
			return (int32)i;
	return -1;
}
ShaderSamplerSlot* ShaderPass::FindSampler(FixedString name)
{
	for (auto v : samplers)
		if (v->name == name)
			return v;
	return nullptr;
}

const std::vector<ShaderRequiredVertexComponentDesc>& ShaderPass::GetRequiredVertexComponents()
{
	return this->requiredVertexComponents;
}

ShaderVariant* ShaderTechnique::DefaultVariant()
{
	ShaderVariantKeys variantKeys;
	variantKeys.BindCollection(&variantCollection);

	ShaderVariant* variant = FindVariant(variantKeys);
	return variant;
}
