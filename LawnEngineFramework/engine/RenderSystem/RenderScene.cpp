#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderScene.h"
#include "RenderPipeline.h"
#include "ImageBasedLighting.h"
#include "Texture2D.h"
#include "ImageOperations.h"
#include "RenderSystem.h"
#include "RenderCommands.h"
#include "TextureCube.h"

struct DiffuseIndirectLightSourceType : public EnumObjectType<DiffuseIndirectLightSource>
{
	DiffuseIndirectLightSourceType() : EnumObjectType<DiffuseIndirectLightSource>("DiffuseIndirectLightSource")
	{
		Add("Color", DiffuseIndirectLightSource::Color);
		Add("SkyboxSH", DiffuseIndirectLightSource::SkyboxSH);
		Add("SkyboxTexture", DiffuseIndirectLightSource::SkyboxTexture);
	}
};
IMPL_ENUM_TYPE(DiffuseIndirectLightSource, DiffuseIndirectLightSourceType)

IMPL_CLASS_TYPE(RenderSceneSettings);

RenderSceneSettings::RenderSceneSettings()
	: skyboxIntensity(1)
	, skyboxRotation(0)
	, eventPropertyChangedByReflection(this)
	, diffuseIndirectLightSource(DiffuseIndirectLightSource::Color)
	, diffuseIndirectColor(1, 1, 1)
{
}

RenderSceneSettings::~RenderSceneSettings()
{
}

void RenderSceneSettings::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("skybox", &Self::skybox).AddAttribute(new AssetSelectorAttribute());
	reflector.AddMember("skyboxIntensity", &Self::skyboxIntensity).AddAttribute(new FloatRangeAttribute(0.0f, 2.0f));
	reflector.AddMember("skyboxRotation", &Self::skyboxRotation).AddAttribute(new FloatRangeAttribute(0.0f, 1.0f));

	reflector.AddMember("diffuseIndirectLightSource", &Self::diffuseIndirectLightSource);

	reflector.AddMember("diffuseIndirectColor", &Self::diffuseIndirectColor);
}

void RenderSceneSettings::AfterDeserialization()
{
	GenerateDiffuseIndirectLighting();
}

void RenderSceneSettings::OnPropertyChangedByReflection(ObjectMember& member)
{
	eventPropertyChangedByReflection.Trigger();

	if (member.memberName == "skybox" || member.memberName == "diffuseIndirectLightSource")
	{
		GenerateDiffuseIndirectLighting();
	}

	if (member.memberName == "skybox")
	{
		GenerateSpecularIndirectLighting();
	}
}

void RenderSceneSettings::GenerateDiffuseIndirectLighting()
{
	Log::Info("Generating diffuse indirect lighting");

	if (diffuseIndirectLightSource == DiffuseIndirectLightSource::SkyboxSH)
	{
		if (skybox)
			diffuseIndirectSH = ImageOperations::CalcDiffuseRadianceSH(skybox->Data().GetMipmap(0));
		else
			diffuseIndirectSH = sh3rgb();
	}
	else if (diffuseIndirectLightSource == DiffuseIndirectLightSource::SkyboxTexture)
	{
		if (skybox)
		{
			diffuseIndirectTexture.reset(new Texture2D());

			Image2DMipmaps diffuseRadianceImage(32, 32, PixelFormat::PF_R16G16B16A16_Float, 1);
			ImageOperations::CalcDiffuseRadianceTexture(skybox->Data().GetMipmap(0), diffuseRadianceImage.GetMipmap(0));

			diffuseIndirectTexture->Create(diffuseRadianceImage);
			diffuseIndirectTexture->SetSampler(Sampler::LinearRepeatSampler);
			diffuseIndirectTexture->sRGB = true;
		}
		else
			diffuseIndirectTexture = Texture2D::GetWhiteTexture();
	}
}

void SphericalMapToCubeMap(TexturePtr src, TextureCubePtr dst, std::vector<std::unique_ptr<RenderPassCommand>>& renderPassCommands)
{
// 	std::vector<RHITexturePtr> renderTargets;
// 	std::vector<RenderTargetAction> renderTargetActions;
// 
// 	RHITexturePtr depthStencilTarget;
// 	DepthStencilTargetAction depthStencilTargetAction;
// 
// 	RHIViewport viewport;
// 
// 	std::vector<MeshDrawCommand*> meshDrawCommands;
}

void RenderSceneSettings::GenerateSpecularIndirectLighting()
{
// 	Log::Info("Generating specular indirect lighting");
// 
// 	RHIDevice* device = RenderSystem::instance().rhiDevice.get();
// 
// 	std::vector<std::unique_ptr<RenderPassCommand>> renderPassCommands;
// 
// 	TextureCubePtr cubemap;
// 	SphericalMapToCubeMap(this->skybox, cubemap, *renderPassCommands.emplace_back(std::make_unique<RenderPassCommand>()));
// 
// 	device->FrameBegin();
// 
// 	for (size_t iPass = 0; iPass < renderPassCommands.size(); iPass++)
// 	{
// 		RenderPassCommand* renderPassCommand = renderPassCommands[iPass];
// 
// 		renderPassCommand->PassBegin(device);
// 
// 		for (auto i = 0; i < renderPassCommand->meshDrawCommands.size(); i++)
// 		{
// 			MeshDrawCommand* meshDrawCommand = renderPassCommand->meshDrawCommands[i];
// 			meshDrawCommand->BindPipelineStates(device);
// 			meshDrawCommand->Draw(device);
// 		}
// 
// 		renderPassCommand->PassEnd(device);
// 	}
// 
// 	device->FrameEnd();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

RenderScene::RenderScene()
{
	renderPipeline = new RenderPipeline(this);
}

RenderScene::~RenderScene()
{
	SAFE_DELETE(renderPipeline);
}

void RenderScene::Render(RenderSceneCamera* camera, RenderViewPtr renderView)
{
	if (renderPipeline)
	{
		renderPipeline->Render(camera, renderView);
	}
}

void RenderScene::UpdateObjectDatas(RenderSceneObject* p, uint32 atIndex)
{
	RenderSceneObjectProperties properties;
	p->FillProperties(properties);

	objects[atIndex] = p;

	SetObjectDynamic(p, properties.isDynamic);

	SetObjectPendingPrepare(p, true);
}

void RenderScene::SetObjectDynamic(RenderSceneObject* p, bool isDynamic)
{
	if (isDynamic)
	{
		dynamicObjects.insert(p);
	}
	else
	{
		dynamicObjects.erase(p);
	}
}

void RenderScene::SetObjectPendingPrepare(RenderSceneObject* p, bool pendingPrepare)
{
	if (pendingPrepare)
	{
		pendingPrepareObjects.insert(p);
	}
	else
	{
		pendingPrepareObjects.erase(p);
	}
}

void RenderScene::GetAndClearPendingPrepareObjects(std::vector<RenderSceneObject*>& outObjects)
{
	std::unordered_set<RenderSceneObject*> finalObjectsSet;  //remove duplicates

	finalObjectsSet.insert(dynamicObjects.begin(), dynamicObjects.end());

	finalObjectsSet.insert(pendingPrepareObjects.begin(), pendingPrepareObjects.end());
	pendingPrepareObjects.clear();

	outObjects.assign(finalObjectsSet.begin(), finalObjectsSet.end());
}

void RenderScene::AddObject(RenderSceneObject* p)
{
	CHECK(p->renderSceneContainerHandle == -1);

	int index = objects.size();
	p->renderSceneContainerHandle = index;

	size_t newSize = objects.size() + 1;
	objects.resize(newSize);
	UpdateObjectDatas(p, index);

	NotifyAddObject(p, index);
}

void RenderScene::AddLight(RenderSceneLight* p)
{
	CHECK(p->renderSceneContainerHandle == -1);

	int index = lights.size();
	p->renderSceneContainerHandle = index;
	lights.push_back(p);

	NotifyAddLight(p, index);
}

void RenderScene::AddCamera(RenderSceneCamera* p)
{
	CHECK(p->renderSceneContainerHandle == -1);

	int index = cameras.size();
	p->renderSceneContainerHandle = index;
	cameras.push_back(p);

	NotifyAddCamera(p, index);
}

void RenderScene::AddCustomPassObject(RenderSceneCustomPassObject* p)
{
	CHECK(p->renderSceneContainerHandle == -1);

	std::vector<RenderSceneCustomPassObject*>& container = customPassObjects[p->type];

	int index = container.size();
	p->renderSceneContainerHandle = index;
	container.push_back(p);
}

void RenderScene::RemoveObject(RenderSceneObject* p)
{
	int index = p->renderSceneContainerHandle;
	if (index >= 0)
	{
		if (index != objects.size() - 1)
		{
			RenderSceneObject* last = objects.back();
			
			objects[index] = last;

			last->renderSceneContainerHandle = index;
		}

		objects.pop_back();

		SetObjectDynamic(p, false);

		SetObjectPendingPrepare(p, false);

		p->renderSceneContainerHandle = -1;

		NotifyRemoveObject(p, index);
	}
}

void RenderScene::RemoveLight(RenderSceneLight* p)
{
	int index = p->renderSceneContainerHandle;
	if (index >= 0)
	{
		if (index != lights.size() - 1)
		{
			RenderSceneLight* last = lights.back();

			lights[index] = last;
			last->renderSceneContainerHandle = index;
		}

		lights.pop_back();
		p->renderSceneContainerHandle = -1;

		NotifyRemoveLight(p, index);
	}
}

void RenderScene::RemoveCamera(RenderSceneCamera* p)
{
	int index = p->renderSceneContainerHandle;
	if (index >= 0)
	{
		if (index != cameras.size() - 1)
		{
			RenderSceneCamera* last = cameras.back();

			cameras[index] = last;
			last->renderSceneContainerHandle = index;
		}

		cameras.pop_back();
		p->renderSceneContainerHandle = -1;

		NotifyRemoveCamera(p, index);
	}
}

void RenderScene::RemoveCustomPassObject(RenderSceneCustomPassObject* p)
{
	std::vector<RenderSceneCustomPassObject*>& container = customPassObjects[p->type];

	int index = p->renderSceneContainerHandle;
	if (index >= 0)
	{
		if (index != container.size() - 1)
		{
			RenderSceneCustomPassObject* last = container.back();

			container[index] = last;
			last->renderSceneContainerHandle = index;
		}

		container.pop_back();
		p->renderSceneContainerHandle = -1;
	}
}

void RenderScene::NotifyAddObject(RenderSceneObject* p, int32 atIndex)
{
}

void RenderScene::NotifyAddLight(RenderSceneLight* p, int32 atIndex)
{
	renderPipeline->OnAddLight(p, atIndex);
}

void RenderScene::NotifyAddCamera(RenderSceneCamera* p, int32 atIndex)
{
	renderPipeline->OnAddCamera(p, atIndex);
}

void RenderScene::NotifyRemoveObject(RenderSceneObject* p, int32 atIndex)
{
}

void RenderScene::NotifyRemoveLight(RenderSceneLight* p, int32 atIndex)
{
	renderPipeline->OnRemoveLight(p, atIndex);
}

void RenderScene::NotifyRemoveCamera(RenderSceneCamera* p, int32 atIndex)
{
	renderPipeline->OnRemoveCamera(p, atIndex);
}

void RenderScene::SetScenePassCount(uint32 scenePassCount)
{
	if (perScenePassDatas.size() != scenePassCount)
	{
		assert(perScenePassDatas.size() == 0);  //dynamic resize is not supported currently
		perScenePassDatas.resize(scenePassCount);
	}
}

DrawCommandHandle RenderScene::AddDrawCommand(uint32 scenePassIndex, MeshDrawCommandPtr drawcommand, CullingInputHandle cullingInputHandle, const DrawCommandBatchingKey& batchingKey)
{
	PerScenePassData& perScenePassData = perScenePassDatas[scenePassIndex];

	DrawCommandHandle handle = drawCommandHandleToIndex.size();
	DrawCommandIndex index(scenePassIndex, (uint32)perScenePassData.drawCommands.size());

	BatchID batchID = AddBatch(scenePassIndex, batchingKey);

	perScenePassData.drawCommands.push_back(drawcommand);
	perScenePassData.drawCommandCullingInputHandles.push_back(cullingInputHandle);
	perScenePassData.drawCommandSortingKeys.push_back(DrawCommandSortingKey());
	perScenePassData.drawCommandBatchIDs.push_back(batchID);

	perScenePassData.batches.batchingKeyUseCounts[batchID]++;

	drawCommandHandleToIndex.insert(std::make_pair(handle, index));

	return handle;
}

void RenderScene::RemoveDrawCommand(DrawCommandHandle handle)
{
	auto it = drawCommandHandleToIndex.find(handle);
	if (it != drawCommandHandleToIndex.end())
	{
		DrawCommandIndex index = it->second;

		PerScenePassData& perScenePassData = perScenePassDatas[index.ScenePassIndex()];

		uint32 removingIndex = index.ArrayIndex();
		int32 lastValidIndex = perScenePassData.drawCommands.size() - 1;

		auto batchID = perScenePassData.drawCommandBatchIDs[removingIndex];
		perScenePassData.batches.batchingKeyUseCounts[batchID]--;

		if (removingIndex != lastValidIndex)
		{
			auto lastDrawCommand = perScenePassData.drawCommands[lastValidIndex];
			auto lastCullingInputHandle = perScenePassData.drawCommandCullingInputHandles[lastValidIndex];
			auto lastSortingKey = perScenePassData.drawCommandSortingKeys[lastValidIndex];
			auto lastBatchID = perScenePassData.drawCommandBatchIDs[lastValidIndex];

			perScenePassData.drawCommands[removingIndex] = lastDrawCommand;
			perScenePassData.drawCommandCullingInputHandles[removingIndex] = lastCullingInputHandle;
			perScenePassData.drawCommandSortingKeys[removingIndex] = lastSortingKey;
			perScenePassData.drawCommandBatchIDs[removingIndex] = lastBatchID;

			drawCommandHandleToIndex[lastDrawCommandHandle] = removingIndex;
		}

		drawCommandHandleToIndex.erase(it);
		lastValidIndex--;

		int newLength = lastValidIndex + 1;
		perScenePassData.drawCommands.resize(newLength);
		perScenePassData.drawCommandCullingInputHandles.resize(newLength);
		perScenePassData.drawCommandSortingKeys.resize(newLength);
		perScenePassData.drawCommandBatchIDs.resize(newLength);
	}
}

CullingInputHandle RenderScene::AddCullingInput(const DrawCommandCullingInput& cullingInput)
{
	return cullingInputs.add(cullingInput);
}

void RenderScene::RemoveCullingInput(CullingInputHandle handle)
{
	cullingInputs.remove(handle);
}

BatchID RenderScene::AddBatch(uint32 scenePassIndex, const DrawCommandBatchingKey& batchingKey)
{
	PerScenePassData& perScenePassData = perScenePassDatas[scenePassIndex];

	BatchesContainer& batches = perScenePassData.batches;

	auto it = batches.batchingKeyToBatchID.find(batchingKey);
	if (it != batches.batchingKeyToBatchID.end())
	{
		uint32 batchID = it->second;
		return batchID;
	}
	else
	{
		uint32 batchID = batches.batchingKeys.size();

		batches.batchingKeys.push_back(batchingKey);
		batches.batchingKeyUseCounts.push_back(0);

		batches.batchingKeyToBatchID.insert(std::make_pair(batchingKey, batchID));

		return batchID;
	}
}

void RenderScene::RemoveBatch(uint32 scenePassIndex, BatchID batchID)
{
}
