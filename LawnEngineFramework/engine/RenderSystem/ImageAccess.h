#pragma once

#include "RenderSystemCommon.h"
#include "RHI/RHICommon.h"

class Image2D;

//////////////////////////////////////////////////////////////////////////

template<SamplerAddressing addressing>
struct Image2DAddressing
{
};

template<>
struct Image2DAddressing<SamplerAddressing::SA_Clamp>
{
	static int AddressingU(const Image2D& image, int u)
	{
		if (u < 0)
			return 0;
		else if (u >= image.width)
			return image.width - 1;
		else
			return u;
	}

	static int AddressingV(const Image2D& image, int v)
	{
		if (v < 0)
			return 0;
		else if (v >= image.height)
			return image.height - 1;
		else
			return v;
	}
};

template<>
struct Image2DAddressing<SamplerAddressing::SA_Repeat>
{
	static int AddressingU(const Image2D& image, int u)
	{
		return u % image.width;
	}

	static int AddressingV(const Image2D& image, int v)
	{
		return v % image.height;
	}
};

//////////////////////////////////////////////////////////////////////////

template<PixelFormat pixelFormat>
struct PixelAccess
{
	typedef typename PixelFormatToType<pixelFormat>::Value PixelTypeRaw;

	static PixelTypeRaw Read(const Image2D& image, int x, int y)
	{
		const PixelTypeRaw* p = reinterpret_cast<const PixelTypeRaw*>(image.PixelPointer(x, y));
		return *p;
	}

	template<typename PixelType>
	static PixelType ReadWithConvert(const Image2D& image, int x, int y)
	{
		const PixelTypeRaw* p = reinterpret_cast<const PixelTypeRaw*>(image.PixelPointer(x, y));
		PixelType result = PixelTypeConvert<PixelTypeRaw, PixelType>::Convert(*p);
		return result;
	}

	static void Write(Image2D& image, int x, int y, const PixelTypeRaw& v)
	{
		PixelTypeRaw* p = reinterpret_cast<PixelTypeRaw*>(image.PixelPointer(x, y));
		(*p) = v;
	}

	template<typename PixelType>
	static void WriteWithConvert(Image2D& image, int x, int y, const PixelType& v)
	{
		PixelTypeRaw r = PixelTypeConvert<PixelType, PixelTypeRaw>::Convert(v);
		PixelTypeRaw* p = reinterpret_cast<PixelTypeRaw*>(image.PixelPointer(x, y));
		(*p) = r;
	}
};

//////////////////////////////////////////////////////////////////////////

template<SamplerFilter filter, typename pixelType, PixelFormat pixelFormat, SamplerAddressing addressing>
struct Image2DFilter
{
};

template<typename pixelType, PixelFormat pixelFormat, SamplerAddressing addressing>
struct Image2DFilter<SamplerFilter::SF_Point, pixelType, pixelFormat, addressing>
{
	static pixelType Sample(const Image2D& image, const float2& uv)
	{
		float2 pixelCoord = uv * float2(image.width - 1.0f, image.height - 1.0f);

		int x = round(pixelCoord.x);
		int y = round(pixelCoord.y);

		x = Image2DAddressing<addressing>::AddressingU(image, x);
		y = Image2DAddressing<addressing>::AddressingV(image, y);

		pixelType v = PixelAccess<pixelFormat>::template ReadWithConvert<pixelType>(image, x, y);

		return v;
	}
};

template<typename pixelType, PixelFormat pixelFormat, SamplerAddressing addressing>
struct Image2DFilter<SamplerFilter::SF_Bilinear, pixelType, pixelFormat, addressing>
{
	typedef typename PixelFormatToType<pixelFormat>::Value PixelRawType;

	typedef typename PixelTypeToFloatType<PixelRawType>::Value PixelFloatType;

	static pixelType Sample(const Image2D& image, const float2& uv)
	{
		float2 pixelCoord = uv * float2((float)image.width - 1, (float)image.height - 1);

		int xmin = vcpp::math::floor_int(pixelCoord.x);
		int xmax = vcpp::math::ceil_int(pixelCoord.x);

		int ymin = vcpp::math::floor_int(pixelCoord.y);
		int ymax = vcpp::math::ceil_int(pixelCoord.y);

		float fracx = pixelCoord.x - xmin;
		float fracy = pixelCoord.y - ymin;

		xmin = Image2DAddressing<addressing>::AddressingU(image, xmin);
		xmax = Image2DAddressing<addressing>::AddressingU(image, xmax);

		ymin = Image2DAddressing<addressing>::AddressingV(image, ymin);
		ymax = Image2DAddressing<addressing>::AddressingV(image, ymax);

		PixelFloatType v00 = PixelAccess<pixelFormat>::template ReadWithConvert<PixelFloatType>(image, xmin, ymin);
		PixelFloatType v10 = PixelAccess<pixelFormat>::template ReadWithConvert<PixelFloatType>(image, xmax, ymin);
		PixelFloatType v01 = PixelAccess<pixelFormat>::template ReadWithConvert<PixelFloatType>(image, xmin, ymax);
		PixelFloatType v11 = PixelAccess<pixelFormat>::template ReadWithConvert<PixelFloatType>(image, xmax, ymax);

		PixelFloatType vtop = vcpp::math::lerp(v00, v10, fracx);
		PixelFloatType vbottom = vcpp::math::lerp(v01, v11, fracx);

		PixelFloatType v = vcpp::math::lerp(vtop, vbottom, fracy);

		pixelType result = PixelTypeConvert<PixelFloatType, pixelType>::Convert(v);

		return result;
	}
};

//////////////////////////////////////////////////////////////////////////

//
// By specifying the PixelFormat of image, we can make everything compile-time determined, removing any virtual functions
//
template<typename pixelType, PixelFormat pixelFormat, SamplerFilter filter, SamplerAddressing addressing>
struct Image2DSamplerStatic
{
	Image2DSamplerStatic()
	{}

	pixelType Sample(const Image2D& image, const float2& uv)
	{
		return Image2DFilter<filter, pixelType, pixelFormat, addressing>::Sample(image, uv);
	}

	pixelType SampleLod(const Image2DMipmaps& imageMipmaps, const float2& uv, int mipmap)
	{
		const Image2D& image = imageMipmaps.GetMipmap(mipmap);
		return Image2DFilter<filter, pixelType, pixelFormat, addressing>::Sample(image, uv);
	}
};

//
// Sometimes when we can not determine the PixelFormat at compile-time, some overhead should exist
//
template<typename pixelType, SamplerFilter filter, SamplerAddressing addressing>
struct Image2DSamplerDynamic
{
	const Image2D& image;

	typedef pixelType SampleFunc(const Image2D& image, const float2& uv);
	SampleFunc* sampleFunc;

	Image2DSamplerDynamic(const Image2D& image)
		: image(image)
	{
		switch (image.pixelFormat)
		{
		case PF_R8G8B8A8:
			sampleFunc = Image2DFilter<filter, pixelType, PF_R8G8B8A8, addressing>::Sample;
			break;
		case PF_R8:
			sampleFunc = Image2DFilter<filter, pixelType, PF_R8, addressing>::Sample;
			break;
		case PF_R16G16B16A16_Float:
			sampleFunc = Image2DFilter<filter, pixelType, PF_R16G16B16A16_Float, addressing>::Sample;
			break;
		case PF_R32G32B32A32_Float:
			sampleFunc = Image2DFilter<filter, pixelType, PF_R32G32B32A32_Float, addressing>::Sample;
			break;
		default:
			sampleFunc = nullptr;
			CHECK(false && "PixelFormat not supported");
			break;
		}
	}

	pixelType Sample(const float2& uv)
	{
		return sampleFunc(image, uv);
	}
};
