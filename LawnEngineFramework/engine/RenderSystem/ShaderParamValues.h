#pragma once

#include "Shader.h"
#include <atomic>

class RenderResourcesManager;
class ShaderPass;

struct PackedSH3RGB
{
	float4 r_packed0;
	float4 r_packed1;

	float4 g_packed0;
	float4 g_packed1;

	float4 b_packed0;
	float4 b_packed1;

	float3 rgb_packed2;
	float reserved;

	PackedSH3RGB()
	{}

	PackedSH3RGB(const sh3rgb& v)
	{
		FromSH3RGB(v);
	}

	void FromSH3RGB(const sh3rgb& v)
	{
		r_packed0.m[0] = v.r().coeff[0];
		r_packed0.m[1] = v.r().coeff[1];
		r_packed0.m[2] = v.r().coeff[2];
		r_packed0.m[3] = v.r().coeff[3];
		r_packed1.m[0] = v.r().coeff[4];
		r_packed1.m[1] = v.r().coeff[5];
		r_packed1.m[2] = v.r().coeff[6];
		r_packed1.m[3] = v.r().coeff[7];
		rgb_packed2.m[0] = v.r().coeff[8];

		g_packed0.m[0] = v.g().coeff[0];
		g_packed0.m[1] = v.g().coeff[1];
		g_packed0.m[2] = v.g().coeff[2];
		g_packed0.m[3] = v.g().coeff[3];
		g_packed1.m[0] = v.g().coeff[4];
		g_packed1.m[1] = v.g().coeff[5];
		g_packed1.m[2] = v.g().coeff[6];
		g_packed1.m[3] = v.g().coeff[7];
		rgb_packed2.m[1] = v.g().coeff[8];

		b_packed0.m[0] = v.b().coeff[0];
		b_packed0.m[1] = v.b().coeff[1];
		b_packed0.m[2] = v.b().coeff[2];
		b_packed0.m[3] = v.b().coeff[3];
		b_packed1.m[0] = v.b().coeff[4];
		b_packed1.m[1] = v.b().coeff[5];
		b_packed1.m[2] = v.b().coeff[6];
		b_packed1.m[3] = v.b().coeff[7];
		rgb_packed2.m[2] = v.b().coeff[8];
	}

	sh3rgb ToSH3RGB()
	{
		sh3rgb v;

		v.r().coeff[0] = r_packed0.m[0];
		v.r().coeff[1] = r_packed0.m[1];
		v.r().coeff[2] = r_packed0.m[2];
		v.r().coeff[3] = r_packed0.m[3];
		v.r().coeff[4] = r_packed1.m[0];
		v.r().coeff[5] = r_packed1.m[1];
		v.r().coeff[6] = r_packed1.m[2];
		v.r().coeff[7] = r_packed1.m[3];
		v.r().coeff[8] = rgb_packed2.m[0];

		v.g().coeff[0] = g_packed0.m[0];
		v.g().coeff[1] = g_packed0.m[1];
		v.g().coeff[2] = g_packed0.m[2];
		v.g().coeff[3] = g_packed0.m[3];
		v.g().coeff[4] = g_packed1.m[0];
		v.g().coeff[5] = g_packed1.m[1];
		v.g().coeff[6] = g_packed1.m[2];
		v.g().coeff[7] = g_packed1.m[3];
		v.g().coeff[8] = rgb_packed2.m[1];

		v.b().coeff[0] = b_packed0.m[0];
		v.b().coeff[1] = b_packed0.m[1];
		v.b().coeff[2] = b_packed0.m[2];
		v.b().coeff[3] = b_packed0.m[3];
		v.b().coeff[4] = b_packed1.m[0];
		v.b().coeff[5] = b_packed1.m[1];
		v.b().coeff[6] = b_packed1.m[2];
		v.b().coeff[7] = b_packed1.m[3];
		v.b().coeff[8] = rgb_packed2.m[2];

		return v;
	}
};

//
// generic container for shader params, not bound to any shader
//
class ShaderParamValues : public BaseObject
{
	DECL_CLASS_TYPE(ShaderParamValues, BaseObject);

public:
	std::map<FixedString, FixedString> variantKeys;

	struct VariableInfo
	{
		int offset;
		uint32 byteSize;
		ShaderVariableType type;

		VariableInfo()
			: offset(0), byteSize(0), type(SVT_Unknown)
		{}

		VariableInfo(int offset, uint32 byteSize, ShaderVariableType type)
			: offset(offset), byteSize(byteSize), type(type)
		{}
	};

	std::map<FixedString, VariableInfo> variables;

	std::vector<uint8> variableValueBuffer;

	std::map<FixedString, TexturePtr> resources;

	std::map<FixedString, SamplerPtr> samplers;

public:
	ShaderParamValues();
	~ShaderParamValues();

	virtual void Reflect(Reflector& reflector) override;
	virtual bool Serialize(Serializer& serializer) override;
	virtual bool Deserialize(Deserializer& deserializer) override;

	virtual void OnDestroy() override;

	void Reset();

	void CopyFrom(const ShaderParamValues& other);

	bool SetVariantKey(FixedString name, FixedString value);
	FixedString GetVariantKey(FixedString name) const;
	bool HasVariantKey(FixedString name) const;

	void SetVariableValue(FixedString name, const void* data, uint32 byteSize, ShaderVariableType type);
	bool GetVariableValue(FixedString name, void** outData, uint32* outByteSize, ShaderVariableType* outType);
	const void* GetVariableValueAddr(FixedString name) const;
	void* GetVariableValueAddr(FixedString name);
	bool HasVariableValue(FixedString name) const;

	float GetFloat(FixedString name) { return *reinterpret_cast<float*>(GetVariableValueAddrInternal(name)); }
	float2 GetFloat2(FixedString name) { return *reinterpret_cast<float2*>(GetVariableValueAddrInternal(name)); }
	float3 GetFloat3(FixedString name) { return *reinterpret_cast<float3*>(GetVariableValueAddrInternal(name)); }
	float4 GetFloat4(FixedString name) { return *reinterpret_cast<float4*>(GetVariableValueAddrInternal(name)); }
	float4x4 GetFloat4x4(FixedString name) { return *reinterpret_cast<float4x4*>(GetVariableValueAddrInternal(name)); }
	sh3rgb GetSH3RGB(FixedString name);

	void SetFloat(FixedString name, float value) { SetVariableValue(name, &value, (uint32)sizeof(float), SVT_Float); }
	void SetFloat2(FixedString name, const float2& value) { SetVariableValue(name, &value, (uint32)sizeof(float2), SVT_Float2); }
	void SetFloat3(FixedString name, const float3& value) { SetVariableValue(name, &value, (uint32)sizeof(float3), SVT_Float3); }
	void SetFloat4(FixedString name, const float4& value) { SetVariableValue(name, &value, (uint32)sizeof(float4), SVT_Float4); }
	void SetFloat4x4(FixedString name, const float4x4& value) { SetVariableValue(name, &value, (uint32)sizeof(float4x4), SVT_Float4x4); }
	void SetSH3RGB(FixedString name, const sh3rgb& value);

	template<typename T>
	void SetVariableValue(FixedString name, const T& value) { SetVariableValue(name, &value, (uint32)sizeof(T), SVT_Unknown); }

	template<typename T>
	bool GetVariableValue(FixedString name, T& outValue)
	{
		uint32 byteSize;
		ShaderVariableType type;
		if (GetVariableValue(name, &outValue, &byteSize, &type))
		{
			CHECK(sizeof(T) == byteSize);
			return true;
		}
		else
			return false;
	}

	bool SetResource(FixedString name, TexturePtr value);
	TexturePtr GetResource(FixedString name) const;
	bool HasResource(FixedString name) const;

	bool SetSampler(FixedString name, SamplerPtr value);
	SamplerPtr GetSampler(FixedString name) const;

private:
	void* GetVariableValueAddrInternal(FixedString name);
	void* GetResourceValueAddrInternal(FixedString name);
};

/*
class ShaderParamValueStack
{
	ShaderPass* pass;

	enum { MAX_STACK_SIZE = 4 };

	ShaderParamValues* stack[MAX_STACK_SIZE];
	int stackSize;

public:
	ShaderParamValueStack(ShaderPass* pass);
	~ShaderParamValueStack();

	void Clear();

	void Push(ShaderParamValues* params);
	void Pop();

	//void FillMeshDrawCommand(RenderResourcesManager& renderResourcesManager, MeshDrawCommand* command);

	int GetSize() const { return stackSize; }
	void SetSize(int size);

private:
	void FillMeshDrawCommandForShaderFrequency(RenderResourcesManager& renderResourcesManager, MeshDrawCommand* command, ShaderPass* pass, ShaderFrequency shaderFrequency);
};*/
