#pragma once

#include "../AssetSystem/AssetManager.h"
#include "RenderSystemCommon.h"
#include "Image2D.h"

class TextureIBLData : public Asset
{
	DECL_CLASS_TYPE(TextureIBLData, Asset);

public:
	sh3rgb diffuseRadianceSH;

	Texture2DPtr diffuseRadianceTexture;

	Texture2DPtr specularReflectionTexture;

public:
	TextureIBLData();
	virtual ~TextureIBLData();

	virtual void Reflect(Reflector& reflector) override;

	void InitFromImage(const Image2D& image);

};

typedef std::shared_ptr<TextureIBLData> TextureIBLDataPtr;
