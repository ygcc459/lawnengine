#pragma once

#include "RHI/RHICommon.h"
#include "RenderSystem/RenderScene.h"
#include "JobSystem/JobSystem.h"
#include "array_view.h"

class ColorRenderTarget;
typedef std::shared_ptr<ColorRenderTarget> ColorRenderTargetPtr;

class DepthStencilRenderTarget;
typedef std::shared_ptr<DepthStencilRenderTarget> DepthStencilRenderTargetPtr;

class CopyTexturePass;
class ForwardSceneRenderPass;

class RenderSceneObject;
class RenderSceneLight;
class RenderSceneCamera;
class RenderScene;

class RenderPassCommand;
class MeshDrawCommand;

#define MaxCullingPlanes (12)

struct PlanePacket4
{
	float4 nx;
	float4 ny;
	float4 nz;
	float4 d;

	// Store absolute values of plane normals to avoid recalculating per instance
	float4 nxAbs;
	float4 nyAbs;
	float4 nzAbs;

	PlanePacket4(float4* planes, int offset, int limit);
};

struct CullingViewData
{
	uint32 planePacketOffset;
	uint32 planePacketCount;
};

struct CullingView
{
	enum Type
	{
		Camera,
		Light,
	};

	float4 cullingPlanes[MaxCullingPlanes];
	uint32 numCullingPlanes;
	Type type;
};

struct RenderGraph
{
	std::vector<SceneRenderPass*> scenePasses;
	std::vector<RenderPassCommandPtr> scenePassCommands;
	std::vector<uint32> scenePassCullingViewIndices;

	std::vector<CullingView> cullingViews;

	uint32 AddScenePass(SceneRenderPass* pass, RenderPassCommandPtr passCommand, uint32 cullingViewIndex);

	uint32 AddCullingViewFromCamera(RenderSceneCamera* renderSceneCamera);

	bool CreateRHIResources(RHIDevice* rhiDevice);
	void DestroyRHIResources();

	void Clear();
};

struct BatchesDataPerPass
{
	std::vector<uint32> batchVisibleDrawCommandIndicies;

	std::vector<uint32> batchDrawCommandIndexBegin;
	std::vector<uint32> batchDrawCommandIndexCount;
};

struct RenderContext
{
	RHICommandListPtr prepareCommandlist;
	RHICommandListPtr postprocessCommandlist;
	std::vector<RHICommandListPtr> renderCommandlists;

	std::vector<JobPtr> prepareJobs;
	std::vector<RenderSceneObject*> pendingPrepareObjects;

	std::vector<JobPtr> cullJobs;
	std::vector<PlanePacket4> cullingPlanePackets;
	std::vector<CullingViewData> cullingViewDatas;
	std::vector<uint32> cullingInputPerViewVisibility;

	std::vector<JobPtr> batchingJobs;
	std::vector<BatchesDataPerPass> batchesDataPerPass;

	std::vector<JobPtr> renderJobs;

	std::vector<JobPtr> cleanupJobs;

	RenderContext()
	{}

	~RenderContext()
	{}
};

class RenderPipeline
{
public:
	RenderScene* scene;

	ForwardSceneRenderPass* forwardScenePass = nullptr;
	RenderPassCommandPtr forwardPassCommand;

	CopyTexturePass* copyTexturePass = nullptr;

	MeshPtr skyboxMesh;
	MaterialPtr skyboxMaterial;
	MeshDrawCommandPtr skyboxDrawCommand;

	float4 sceneBackgroundColor;

	RHIFramebufferPtr outputFramebuffer;
	RHIViewportSetPtr outputViewport;

	RenderGraph renderGraph;

	//
	// camera data
	//
	struct CameraData
	{
		RenderSceneCamera* renderSceneCamera;

		vcpp::la::rect<uint> viewport;  //in pixels

		ColorRenderTargetPtr basePassColorTarget;
		DepthStencilRenderTargetPtr basePassDepthStencilTarget;
	};

	std::vector<CameraData*> cameraDatas;  //same index as RenderScene.cameras

	//
	// light data
	//
	struct LightData
	{
		RenderSceneLight* renderSceneLight;

		//TODO: GenShadowPass
	};

	std::vector<LightData*> lightDatas;  //same index as RenderScene.lights

public:
	RenderPipeline(RenderScene* scene);
	virtual ~RenderPipeline();

	void UpdateRenderTargets(CameraData* cameraData, RenderViewPtr targetRenderView, RHIDevice* device);
	void OnRenderTargetResized(CameraData* targetCameraData, RenderViewPtr outputRenderView, RHIDevice* device);

	void DestroyRenderGraph(CameraData* targetCameraData, RenderViewPtr outputRenderView, RHIDevice* device);
	void BuildRenderGraph(CameraData* targetCameraData, RenderViewPtr outputRenderView, RHIDevice* device);

	CameraData* GetCameraData(RenderSceneCamera* camera);

	void OnAddLight(RenderSceneLight* p, int32 atIndex);
	void OnRemoveLight(RenderSceneLight* p, int32 atIndex);

	void OnAddCamera(RenderSceneCamera* p, int32 atIndex);
	void OnRemoveCamera(RenderSceneCamera* p, int32 atIndex);

	void Render(RenderSceneCamera* targetCamera, RenderViewPtr targetRenderView);

	void PrepareDrawCommands(RenderContext* renderContext, RHICommandListPtr rhiCommandList);

	void CreateCommandLists(RenderContext* renderContext, RHIDevice* device);

	void CullDrawCommands(RenderContext* renderContext);
	void BatchDrawCommands(RenderContext* renderContext);
	void RenderDrawCommands(RenderContext* renderContext);
	void Cleanup(RenderContext* renderContext);

};
