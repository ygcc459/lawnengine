#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "RenderPipeline.h"
#include "RenderSystem/RenderView.h"
#include "EntitySystem/Camera.h"
#include "EntitySystem/World.h"
#include "EntitySystem/Transform.h"
#include "ColorRenderTarget.h"
#include "DepthStencilRenderTarget.h"
#include "RenderSystem/RenderCommands.h"
#include "ForwardSceneRenderPass.h"
#include "ImGUISystem.h"

//////////////////////////////////////////////////////////////////////////

PlanePacket4::PlanePacket4(float4* planes, int offset, int limit)
{
	float4 p0 = planes[(offset + 0 < limit) ? (offset + 0) : limit];
	float4 p1 = planes[(offset + 1 < limit) ? (offset + 1) : limit];
	float4 p2 = planes[(offset + 2 < limit) ? (offset + 2) : limit];
	float4 p3 = planes[(offset + 3 < limit) ? (offset + 3) : limit];
	nx = float4(p0.x, p1.x, p2.x, p3.x);
	ny = float4(p0.y, p1.y, p2.y, p3.y);
	nz = float4(p0.z, p1.z, p2.z, p3.z);
	d = float4(p0.w, p1.w, p2.w, p3.w);
	nxAbs = vcpp::la::abs(nx);
	nyAbs = vcpp::la::abs(ny);
	nzAbs = vcpp::la::abs(nz);
}

//////////////////////////////////////////////////////////////////////////

RenderPipeline::RenderPipeline(RenderScene* scene)
	: scene(scene)
{
	skyboxMesh = Mesh::CreateSphere(float3(0, 0, 0), 1.0f, 16, 16);

	AssetManager& assetManager = g_lawnEngine.getAssetManager();

	ShaderPtr skyboxShader = assetManager.Load<Shader>(assetManager.ProjectDir() / "Shaders/Skybox.shader");
	skyboxMaterial.reset(new Material());
	skyboxMaterial->BindShader(skyboxShader);

	forwardScenePass = new ForwardSceneRenderPass(this, scene);
	forwardPassCommand = new RenderPassCommand();

	copyTexturePass = new CopyTexturePass();
}

RenderPipeline::~RenderPipeline()
{
	delete forwardScenePass;
	delete copyTexturePass;
}

void RenderPipeline::OnAddLight(RenderSceneLight* obj, int32 atIndex)
{
	LightData* data = new LightData();
	data->renderSceneLight = obj;

	CHECK(lightDatas.size() == atIndex);
	lightDatas.push_back(data);
}

void RenderPipeline::OnRemoveLight(RenderSceneLight* obj, int32 atIndex)
{
	CHECK(atIndex >= 0 && atIndex < lightDatas.size());

	LightData* data = lightDatas[atIndex];
	delete data;

	if (atIndex != lightDatas.size() - 1)
	{
		lightDatas[atIndex] = lightDatas.back();
	}
	lightDatas.pop_back();
}

void RenderPipeline::OnAddCamera(RenderSceneCamera* p, int32 atIndex)
{
	CameraData* data = new CameraData();
	data->renderSceneCamera = p;
	data->viewport = p->viewport;
	data->basePassColorTarget = std::make_shared<ColorRenderTarget>();
	data->basePassDepthStencilTarget = std::make_shared<DepthStencilRenderTarget>();

	CHECK(cameraDatas.size() == atIndex);
	cameraDatas.push_back(data);
}

void RenderPipeline::OnRemoveCamera(RenderSceneCamera* p, int32 atIndex)
{
	CHECK(atIndex >= 0 && atIndex < cameraDatas.size());

	CameraData* data = cameraDatas[atIndex];
	delete data;

	if (atIndex != cameraDatas.size() - 1)
	{
		cameraDatas[atIndex] = cameraDatas.back();
	}
	cameraDatas.pop_back();
}

RenderPipeline::CameraData* RenderPipeline::GetCameraData(RenderSceneCamera* camera)
{
	int index = camera->renderSceneContainerHandle;
	CHECK(index >= 0 && index < cameraDatas.size());

	CameraData* data = cameraDatas[index];
	CHECK(data->renderSceneCamera == camera);

	return data;
}

void RenderPipeline::UpdateRenderTargets(CameraData* cameraData, RenderViewPtr targetRenderView, RHIDevice* device)
{
	RenderSceneCamera* camera = cameraData->renderSceneCamera;

	if (cameraData->viewport != camera->viewport)
	{
		cameraData->viewport = camera->viewport;
		OnRenderTargetResized(cameraData, targetRenderView, device);
	}
}

void RenderPipeline::OnRenderTargetResized(CameraData* targetCameraData, RenderViewPtr outputRenderView, RHIDevice* device)
{
	DestroyRenderGraph(targetCameraData, outputRenderView, device);
	BuildRenderGraph(targetCameraData, outputRenderView, device);
}

uint32 RenderGraph::AddScenePass(SceneRenderPass* pass, RenderPassCommandPtr passCommand, uint32 cullingViewIndex)
{
	uint32 index = scenePasses.size();

	scenePasses.push_back(pass);
	scenePassCommands.push_back(passCommand);
	scenePassCullingViewIndices.push_back(cullingViewIndex);

	return index;
}

uint32 RenderGraph::AddCullingViewFromCamera(RenderSceneCamera* renderSceneCamera)
{
	uint32 index = cullingViews.size();

	CullingView& cameraCullingView = cullingViews.emplace_back();

	return index;
}

bool RenderGraph::CreateRHIResources(RHIDevice* rhiDevice)
{
	for (int i = 0; i < scenePassCommands.size(); i++)
	{
		RenderPassCommandPtr passCommand = scenePassCommands[i];
		passCommand->CreateRHIResources(rhiDevice);
	}
}

void RenderGraph::DestroyRHIResources()
{
	for (int i = 0; i < scenePassCommands.size(); i++)
	{
		RenderPassCommandPtr passCommand = scenePassCommands[i];
		passCommand->DestroyRHIResources();
	}
}

void RenderGraph::Clear()
{
	scenePasses.clear();
	scenePassCommands.clear();
	scenePassCullingViewIndices.clear();

	cullingViews.clear();
}

void RenderPipeline::BuildRenderGraph(CameraData* targetCameraData, RenderViewPtr outputRenderView, RHIDevice* device)
{
	const vcpp::la::rect<uint>& outputViewport = targetCameraData->renderSceneCamera->viewport;

	RHIViewport outputRHIViewport = RHIViewport(outputViewport.x, outputViewport.y, outputViewport.w, outputViewport.h, 0.0f, 1.0f);

	uint32 backbufferW = std::max<uint32>(outputViewport.w, 4);
	uint32 backbufferH = std::max<uint32>(outputViewport.h, 4);

	ColorRenderTargetPtr basePassColor = targetCameraData->basePassColorTarget;
	DepthStencilRenderTargetPtr basePassDepthStencil = targetCameraData->basePassDepthStencilTarget;

	basePassColor->Create(backbufferW, backbufferH, PF_R8G8B8A8);
	basePassColor->CreateRHIResources(device);

	basePassDepthStencil->Create(backbufferW, backbufferH, PF_Depth24Stencil8);
	basePassDepthStencil->CreateRHIResources(device);

	renderGraph.Clear();

	uint32 cameraCullingViewIndex = renderGraph.AddCullingViewFromCamera(targetCameraData->renderSceneCamera);

	//
	// forwardScenePass
	//
	{
		forwardPassCommand->SetColorAttachmentCount(1);
		forwardPassCommand->SetColorAttachment(0, FramebufferAttachmentDesc(basePassColor->GetRHIRenderTarget(), false), RenderTargetAction().ClearColor(sceneBackgroundColor));

		forwardPassCommand->SetDepthStencilAttachment(FramebufferAttachmentDesc(basePassDepthStencil->GetRHITexture(), false), DepthStencilTargetAction().ClearDepth(1.0f).ClearStencil(0));

		forwardPassCommand->SetViewportCount(1);
		forwardPassCommand->SetViewport(0, outputRHIViewport);

		renderGraph.AddScenePass(forwardScenePass, forwardPassCommand, cameraCullingViewIndex);
	}

	//
	// gen shadow passes
	//
	for (uint32 i = 0; i < lightDatas.size(); i++)
	{
	}

	//
	// CopyTexturePass
	//
	copyTexturePass->BuildRenderGraph(targetCameraData->basePassColorTarget, outputRenderView->GetRHIRenderTarget(), outputRHIViewport, device);

	//
	// others
	//
	renderGraph.CreateRHIResources(device);
}

void RenderPipeline::DestroyRenderGraph(CameraData* targetCameraData, RenderViewPtr outputRenderView, RHIDevice* device)
{
	renderGraph.DestroyRHIResources();
}

void RenderPipeline::CreateCommandLists(RenderContext* renderContext, RHIDevice* device)
{
	renderContext->prepareCommandlist = device->CreateCommandList();

	renderContext->postprocessCommandlist = device->CreateCommandList();

	renderContext->renderCommandlists.resize(renderGraph.scenePasses.size());
	for (uint32 i = 0; i < renderGraph.scenePasses.size(); i++)
		renderContext->renderCommandlists[i] = device->CreateCommandList();
}

void RenderPipeline::Render(RenderSceneCamera* camera, RenderViewPtr targetRenderView)
{
	RHIDevice* device = RenderSystem::instance().rhiDevice;

	CameraData* cameraData = GetCameraData(camera);
	UpdateRenderTargets(cameraData, targetRenderView, device);

	vcpp::la::rect<uint>& cameraViewport = camera->viewport;
	RHIViewport viewport = RHIViewport(cameraViewport.x, cameraViewport.y, cameraViewport.w, cameraViewport.h, 0.0f, 1.0f);

	RenderContext* renderContext = new RenderContext();
	CreateCommandLists(renderContext, device);

	//
	// scene passes
	//
	for (int iSceneRenderPass = 0; iSceneRenderPass < renderGraph.scenePasses.size(); iSceneRenderPass++)
	{
		SceneRenderPass* sceneRenderPass = renderGraph.scenePasses[iSceneRenderPass];
		if (sceneRenderPass)
		{
			sceneRenderPass->Prepare(camera, renderContext->prepareCommandlist);
		}
	}

	PrepareDrawCommands(renderContext, renderContext->prepareCommandlist);

	CullDrawCommands(renderContext);

	BatchDrawCommands(renderContext);

	RenderDrawCommands(renderContext);

	//
	// post-process passes
	//
	copyTexturePass->Render(renderContext->postprocessCommandlist);

	device->PresentDisplaySurface(targetRenderView->GetRhiDisplaySurface());
}

struct PrepareDrawCommandsJob : public Job
{
	RenderScene* scene;

	array_view<RenderSceneObject*> objects;

	array_view<SceneRenderPass*> scenePasses;

	array_view<RenderPassCommandPtr> scenePassCommands;

	virtual void OnExecute(uint32 i) override
	{
		RenderSceneObject* object = objects[i];

		PrepareDrawCommandsContext context;
		context.scene = scene;
		context.scenePassesCount = scenePasses.size();
		context.scenePasses = scenePasses.data();
		context.scenePassCommands = scenePassCommands.data();

		object->PrepareDrawCommands(context);
	}
};

void RenderPipeline::PrepareDrawCommands(RenderContext* renderContext, RHICommandListPtr rhiCommandList)
{
	scene->GetAndClearPendingPrepareObjects(renderContext->pendingPrepareObjects);

	RefCountedPtr<PrepareDrawCommandsJob> prepareJob(new PrepareDrawCommandsJob());
	prepareJob->objects.reset(renderContext->pendingPrepareObjects);
	prepareJob->scenePasses.reset(renderGraph.scenePasses);
	prepareJob->scenePassCommands.reset(renderGraph.scenePassCommands);

	JobSystem::GetInstance().Dispatch(prepareJob, scene->objects.size(), 1);
	renderContext->prepareJobs.push_back(prepareJob);
}

struct CullDrawCommandsJob : public Job
{
	static const uint32 NumObjectsPerLoop = 32;

	array_view<DrawCommandCullingInput> cullingInputs;

	array_view<CullingViewData> cullingViewDatas;

	array_view<PlanePacket4> cullingPlanePackets;

	array_view<uint32> outVisibilityMasks;

	virtual void OnExecute(uint32 loopIndex) override
	{
		uint32 beginIndex = loopIndex * NumObjectsPerLoop;
		uint32 endIndex = beginIndex + NumObjectsPerLoop;
		endIndex = (endIndex < cullingInputs.size()) ? endIndex : cullingInputs.size();

		uint32 visibleObjectIndiciesCount = 0;

		for (uint32 iObject = beginIndex; iObject < endIndex; iObject++)
		{
			const DrawCommandCullingInput& cullingInput = cullingInputs[iObject];

			const float3& center = cullingInput.boundingBoxCenter;
			const float3& extent = cullingInput.boundingBoxExtent;

			float4 cx = float4(center.x, center.x, center.x, center.x);
			float4 cy = float4(center.y, center.y, center.y, center.y);
			float4 cz = float4(center.z, center.z, center.z, center.z);

			float4 ex = float4(extent.x, extent.x, extent.x, extent.x);
			float4 ey = float4(extent.y, extent.y, extent.y, extent.y);
			float4 ez = float4(extent.z, extent.z, extent.z, extent.z);

			uint32 visibilityMask = 0;

			for (int32 viewIndex = 0; viewIndex < cullingViewDatas.size(); ++viewIndex)
			{
				CullingViewData splitInfo = cullingViewDatas[viewIndex];

				bool4 isCulled = new bool4(false);
				for (int32 i = 0; i < splitInfo.planePacketCount; ++i)
				{
					const PlanePacket4& p = cullingPlanePackets[splitInfo.planePacketOffset + i];
					float4 distances = p.nx * cx + p.ny * cy + p.nz * cz + p.d;
					float4 radii = p.nxAbs * ex + p.nyAbs * ey + p.nzAbs * ez;

					isCulled = isCulled | (distances + radii < float4::zero);
				}
				if (!math.any(isCulled))
					visibilityMask |= 1U << viewIndex;
			}

			outVisibilityMasks[iObject] =  visibilityMask;
		}
	}
};

void RenderPipeline::CullDrawCommands(RenderContext* renderContext)
{
	const uint32 PlanesPerPacket = 4;

	std::vector<PlanePacket4>& cullingPlanePackets = renderContext->cullingPlanePackets;
	std::vector<CullingViewData>& cullingViewDatas = renderContext->cullingViewDatas;
	std::vector<uint32>& cullingInputPerViewVisibility = renderContext->cullingInputPerViewVisibility;

	for (uint32 iCullingView = 0; iCullingView < renderGraph.cullingViews.size(); iCullingView++)
	{
		CullingView& cullingView = renderGraph.cullingViews[iCullingView];

		uint32 numPackets = (cullingView.numCullingPlanes + PlanesPerPacket - 1) / PlanesPerPacket;

		CullingViewData cullingViewData;
		cullingViewData.planePacketOffset = cullingPlanePackets.size();
		cullingViewData.planePacketCount = numPackets;

		cullingPlanePackets.reserve(cullingPlanePackets.capacity() + numPackets);

		for (uint32 iPacket = 0; iPacket < numPackets; iPacket++)
		{
			PlanePacket4 packet(cullingView.cullingPlanes, iPacket * PlanesPerPacket, cullingView.numCullingPlanes);
			cullingPlanePackets.push_back(packet);
		}

		cullingViewDatas.push_back(cullingViewData);
	}

	assert(renderGraph.cullingViews.size() < sizeof(cullingInputPerViewVisibility[0]) * 8);

	if (cullingInputPerViewVisibility.size() < scene->cullingInputs.size())
		cullingInputPerViewVisibility.resize(scene->cullingInputs.size());

	RefCountedPtr<CullDrawCommandsJob> cullingJob(new CullDrawCommandsJob());
	cullingJob->cullingInputs.reset(scene->cullingInputs.datas_array());
	cullingJob->cullingViewDatas.reset(cullingViewDatas);
	cullingJob->cullingPlanePackets.reset(cullingPlanePackets);
	cullingJob->outVisibilityMasks.reset(cullingInputPerViewVisibility);

	JobSystem::GetInstance().Dispatch(cullingJob, scene->cullingInputs.size(), CullDrawCommandsJob::NumObjectsPerLoop, renderContext->prepareJobs.data(), renderContext->prepareJobs.size());

	renderContext->cullJobs.push_back(cullingJob);
}

void RenderPipeline::BatchDrawCommands(RenderContext* renderContext)
{
	uint32 numScenePasses = renderGraph.scenePasses.size();

	renderContext->batchesDataPerPass.resize(numScenePasses);

	//
	// calc drawcommand layouts ignoring visibility
	//
	for (uint32 iScenePass = 0; iScenePass < numScenePasses; iScenePass++)  //TODO: jobify
	{
		RenderScene::PerScenePassData& scenePerPassData = scene->perScenePassDatas[iScenePass];
		BatchesDataPerPass& batchesPerPass = renderContext->batchesDataPerPass[iScenePass];
		BatchesContainer& batches = scenePerPassData.batches;

		uint32 numDrawCommands = scenePerPassData.drawCommands.size();
		uint32 numBatches = batches.batchingKeys.size();

		//
		// do allocations first
		//
		batchesPerPass.batchDrawCommandIndexBegin.clear();
		batchesPerPass.batchDrawCommandIndexBegin.resize(numBatches);

		batchesPerPass.batchDrawCommandIndexCount.clear();
		batchesPerPass.batchDrawCommandIndexCount.resize(numBatches, 0);

		batchesPerPass.batchVisibleDrawCommandIndicies.clear();
		batchesPerPass.batchVisibleDrawCommandIndicies.resize(numDrawCommands);

		//
		// calc batchDrawCommandIndexBegin
		//
		uint32 offset = 0;
		for (BatchID batchID = 0; batchID < numBatches; batchID++)
		{
			batchesPerPass.batchDrawCommandIndexBegin[batchID] = offset;

			uint32 drawCommandCount = batches.batchingKeyUseCounts[batchID];

			offset += drawCommandCount;
		}
		assert(offset == numDrawCommands);
	}

	//
	// calc batchDrawCommandIndexCount & fill visible-draw-commands, this step considers visibility
	//
	for (uint32 iScenePass = 0; iScenePass < numScenePasses; iScenePass++)
	{
		RenderScene::PerScenePassData& scenePerPassData = scene->perScenePassDatas[iScenePass];
		BatchesDataPerPass& batchesPerPass = renderContext->batchesDataPerPass[iScenePass];

		uint32 numDrawCommands = scenePerPassData.drawCommands.size();

		uint32 cullingViewIndex = renderGraph.scenePassCullingViewIndices[iScenePass];

		for (uint32 iDrawCommand = 0; iDrawCommand < numDrawCommands; iDrawCommand++)  //TODO: jobify
		{
			CullingInputHandle cullingInputHandle = scenePerPassData.drawCommandCullingInputHandles[iDrawCommand];

			auto cullingInputIndex = scene->cullingInputs.handle_to_index(cullingInputHandle);

			uint32 perViewVisibility = renderContext->cullingInputPerViewVisibility[cullingInputIndex];

			bool isVisible = perViewVisibility & (1 << cullingViewIndex);
			if (isVisible)
			{
				BatchID batchID = scenePerPassData.drawCommandBatchIDs[iDrawCommand];

				uint32 indexBegin = batchesPerPass.batchDrawCommandIndexBegin[batchID];
				uint32 fillIndex = batchesPerPass.batchDrawCommandIndexCount[batchID]++;

				batchesPerPass.batchVisibleDrawCommandIndicies[indexBegin + fillIndex] = iDrawCommand;
			}
		}
	}
}

struct RenderDrawCommandsJob : public Job
{
	static const uint32 NumObjectsPerLoop = 32;

	RHICommandListPtr commandlist;

	uint32 viewIndex;

	array_view<uint32> batchDrawCommandIndicies;

	array_view<uint32> batchDrawCommandIndexBegin;
	array_view<uint32> batchDrawCommandIndexCount;

	array_view<MeshDrawCommandPtr> drawCommands;

	virtual void OnExecute(uint32 loopIndex) override
	{
		uint32 batchIndex = loopIndex;

		uint32 drawCommandIndiciesBegin = batchDrawCommandIndexBegin[batchIndex];
		uint32 drawCommandIndiciesCount = batchDrawCommandIndexCount[batchIndex];

		for (uint32 i = drawCommandIndiciesBegin; i < drawCommandIndiciesBegin + drawCommandIndiciesCount; i++)
		{
			uint32 drawCommandIndex = batchDrawCommandIndicies[i];

			MeshDrawCommandPtr drawCommand = drawCommands[drawCommandIndex];

			drawCommand->Execute(commandlist);
		}
	}
};

void RenderPipeline::RenderDrawCommands(RenderContext* renderContext)
{
	for (uint32 iScenePass = 0; iScenePass < renderGraph.scenePasses.size(); iScenePass++)
	{
		RenderScene::PerScenePassData& sceneData = scene->perScenePassDatas[iScenePass];

		BatchesDataPerPass& passCtx = renderContext->batchesDataPerPass[iScenePass];

		RefCountedPtr<RenderDrawCommandsJob> renderJob(new RenderDrawCommandsJob());
		renderJob->commandlist = renderContext->renderCommandlists[iScenePass];
		renderJob->viewIndex = renderGraph.scenePassCullingViewIndices[iScenePass];
		renderJob->batchDrawCommandIndicies = passCtx.batchVisibleDrawCommandIndicies;
		renderJob->batchDrawCommandIndexBegin = passCtx.batchDrawCommandIndexBegin;
		renderJob->batchDrawCommandIndexCount = passCtx.batchDrawCommandIndexCount;
		renderJob->drawCommands = sceneData.drawCommands;

		JobSystem::GetInstance().Dispatch(renderJob, passCtx.batchDrawCommandIndexBegin.size(), RenderDrawCommandsJob::NumObjectsPerLoop, renderContext->batchingJobs.data(), renderContext->batchingJobs.size());

		renderContext->renderJobs.push_back(renderJob);
	}
}

struct RenderContextCleanupJob : public Job
{
	RenderContext* renderContext;

	virtual void OnExecute(uint32 i) override
	{
		delete renderContext;
	}
};

void RenderPipeline::Cleanup(RenderContext* renderContext)
{
	RefCountedPtr<RenderContextCleanupJob> cleanupJob(new RenderContextCleanupJob());
	cleanupJob->renderContext = renderContext;

	JobSystem::GetInstance().Dispatch(cleanupJob, 1, 1, renderContext->renderJobs.data(), renderContext->renderJobs.size());
	renderContext->cleanupJobs.push_back(cleanupJob);
}

/*
struct CustomPassObjectsSorter
{
	bool operator () (const RenderSceneCustomPassObject* a, const RenderSceneCustomPassObject* b)
	{
		return a->priority > b->priority;
	}
};

void RenderPipeline::RenderSkybox(RenderSceneCamera* camera, RenderPassCommandPtr targetRenderPass)
{
	RenderSceneSettings& renderSceneSettings = scene->settings;

	if (renderSceneSettings.skybox)
	{
		if (!skyboxDrawCommand)
			skyboxDrawCommand.reset(new MeshDrawCommand(targetRenderPass));

		ShaderPtr skyboxShader = skyboxMaterial->GetShader();

		ShaderPassPtr shaderPass = skyboxShader->DefaultTechnique()->DefaultVariant()->FirstPass();

		ShaderParamValues* shaderParamValues = &skyboxMaterial->GetShaderParamValues();
		
		renderSceneSettings.skybox->SetSampler(Sampler::LinearRepeatSampler);

		shaderParamValues->SetResource("skyboxTex", renderSceneSettings.skybox);
		shaderParamValues->SetFloat("skyboxIntensity", renderSceneSettings.skyboxIntensity);
		shaderParamValues->SetFloat("skyboxRotation", renderSceneSettings.skyboxRotation);
		
		shaderParamValues->SetFloat3("cameraPos", camera->position);
		shaderParamValues->SetFloat4x4("viewProjMatrix", camera->viewProjectionMatrix);

		const ShaderParamValues* shaderParamValuesArray[] = {shaderParamValues};
		
		skyboxDrawCommand->BuildGraphicsState(skyboxMesh->GetMeshRenderData(), shaderPass, shaderParamValuesArray, 1);
	}
}

struct RenderCustomPassObjectsJob : public Job
{
public:
	CustomPassObjectType type;

	RenderSceneCamera* camera;

	std::vector<RenderSceneCustomPassObject*> objects;  //make a copy to ease sorting

	RHICommandListPtr rhiCommandList;

protected:
	virtual void OnExecute(uint32) override
	{
		std::sort(objects.begin(), objects.end(), CustomPassObjectsSorter());

		std::vector<MeshDrawCommandPtr> drawCommandPtrs;

		for (uint32 i = 0; i < objects.size(); i++)
		{
			RenderSceneCustomPassObject* object = objects[i];

			object->PrepareDrawCommands(camera, drawCommandPtrs);

			for (uint32 j = 0; j < drawCommandPtrs.size(); j++)
			{
				MeshDrawCommandPtr drawCommand = drawCommandPtrs[j];
				drawCommand->Execute(rhiCommandList);
			}

			drawCommandPtrs.clear();
		}
	}
};

void RenderPipeline::PrepareCustomPassObjects(RenderSceneCamera* camera, RHICommandListPtr rhiCommandList, std::vector<JobPtr>& prepareJobs)
{
	RHIDevicePtr rhiDevice = RenderSystem::instance().rhiDevice;

	for (int iType = 0; iType < CustomPassObjectType_Count; iType++)
	{
		if (!customPassObjectCommandlists[iType])
		{
			customPassObjectCommandlists[iType] = rhiDevice->CreateCommandList();
		}
	}
}

void RenderPipeline::DispatchRenderCustomPassObjects(std::vector<JobPtr>& outJobs)
{
	for (int iType = 0; iType < CustomPassObjectType_Count; iType++)
	{
		RefCountedPtr<RenderCustomPassObjectsJob> job(new RenderCustomPassObjectsJob());
		job->type = (CustomPassObjectType)iType;
		job->objects = scene->customPassObjects[iType];  //make a copy
		job->rhiCommandList = customPassObjectCommandlists[iType];

		JobSystem::GetInstance().Dispatch(job, 1, 1);
		outJobs.push_back(job);
	}
}
*/
