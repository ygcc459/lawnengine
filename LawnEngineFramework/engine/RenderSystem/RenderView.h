#pragma once

#include "RHI/RHICommon.h"
#include "IRenderTarget.h"
#include "RegistryList.h"

class UICanvas;

struct RenderViewDesc
{
	uint width;
	uint height;
	void* outputWindow;
	PixelFormat format;

	RenderViewDesc()
		: width(0), height(0), outputWindow(NULL), format(PF_R8G8B8A8)
	{}

	RenderViewDesc(uint width, uint height, void* outputWindow, PixelFormat format = PF_R8G8B8A8)
		: width(width), height(height), outputWindow(outputWindow), format(format)
	{}
};

class RenderView : public IRenderTarget
{
public:
	RenderView(const RenderViewDesc& desc);
	virtual ~RenderView();

	virtual void UpdateRHIResources();
	virtual bool CreateRHIResources();
	virtual void DestroyRHIResources();

	void render();

	void resize(uint newWidth, uint newHeight);

	void AddCameraInternal(Camera* camera);
	void RemoveCameraInternal(Camera* camera);

	void AddUICanvas(UICanvas* v);
	void RemoveUICanvas(UICanvas* v);
	bool IsUICanvasRegistered(UICanvas* v) const;

	uint Width() const { return desc.width; }
	uint Height() const { return desc.height; }
	void* WindowHandle() const { return desc.outputWindow; }

	virtual RHITexturePtr GetRHIRenderTarget() override;
	RHIDisplaySurfacePtr GetRhiDisplaySurface() { return rhiDisplaySurface; }

	// params: void
	// returns: void
	vcpp::signal<void(void)> signal_before_window_resize;

	// params: uint new_window_width, uint new_window_height
	// returns: void
	vcpp::signal<void(uint,uint)> signal_after_window_resize;

protected:
	std::vector<Camera*> cameras;

	RegistryList<UICanvas*> canvases;

	RenderViewDesc desc;

	RHIDisplaySurfacePtr rhiDisplaySurface;

};

typedef std::shared_ptr<RenderView> RenderViewPtr;
