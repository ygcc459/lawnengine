#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderPass.h"

RenderPass::RenderPass()
{
	Log::Debug("RenderPass() %08X", this);
}

RenderPass::~RenderPass()
{
	Log::Debug("~RenderPass() %08X", this);
}
