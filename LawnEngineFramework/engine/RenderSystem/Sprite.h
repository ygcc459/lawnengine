#pragma once

#include "Texture2D.h"

#include "IntrusiveLinkedList.h"

class Sprite;
typedef std::shared_ptr<Sprite> SpritePtr;

class SpriteAtlas;
typedef std::shared_ptr<SpriteAtlas> SpriteAtlasPtr;

class Sprite : public BaseObject
{
	DECL_CLASS_TYPE(Sprite, BaseObject);

public:
	std::string name;
	rectf rect;  //in pixels

	bool isNinePatch;
	edge4f ninePatchPaddings;  //in pixels

public:
	Sprite();
	virtual ~Sprite();

	virtual void Reflect(Reflector& reflector) override;

};


class SpriteAtlas : public Asset
{
	DECL_CLASS_TYPE(SpriteAtlas, Asset);

public:
	Texture2DPtr texture;

	std::vector<Sprite*> sprites;

	std::map<std::string, int> nameToIndexMapping;

public:
	SpriteAtlas();
	virtual ~SpriteAtlas();

	virtual void Reflect(Reflector& reflector) override;

	Sprite* AddSprite(const std::string& name);
	Sprite* FindSprite(const std::string& name);

private:
	virtual void AfterDeserialization() override;
	
};

typedef std::shared_ptr<SpriteAtlas> SpriteAtlasPtr;


class DynamicTextureAtlas
{
private:
	struct Slot : public TIntrusiveLinkedList<Slot>
	{
		int32 x, y, w, h;

		Slot(int x, int y, int w, int h)
			: TIntrusiveLinkedList<Slot>(), x(x), y(y), w(w), h(h)
		{}
	};

	Slot* atlasUsedSlots;
	Slot* atlasEmptySlots;

	Texture2DPtr texture;

	int Padding;

public:
	DynamicTextureAtlas(int w, int h, PixelFormat pixelFormat, int Padding);
	virtual ~DynamicTextureAtlas();

	Texture2DPtr GetTexture() { return texture; }

	bool Alloc(const Image2D& image, recti& outRect);

private:
	bool AllocRectOnAtlas(const int2& requiredSize, recti& outRect);

};

typedef std::shared_ptr<SpriteAtlas> SpriteAtlasPtr;
