#include "../fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "Mesh.h"

#include <vcpp/la.h>

using namespace vcpp::la;

std::shared_ptr<Mesh> Mesh::CreateBox(const float3& minPoint, const float3& maxPoint)
{
	struct Vertex
	{
		float3 pos;
		float3 normal;
		float4 tangent;
		float2 uv;
	};

	Vertex vertices[] =
	{
		// +x
		{ float3(maxPoint.x, minPoint.y, minPoint.z), float3(1, 0, 0), float4(0, 1, 0, -1), float2(0, 0) },
		{ float3(maxPoint.x, minPoint.y, maxPoint.z), float3(1, 0, 0), float4(0, 1, 0, -1), float2(0, 1) },
		{ float3(maxPoint.x, maxPoint.y, maxPoint.z), float3(1, 0, 0), float4(0, 1, 0, -1), float2(1, 1) },
		{ float3(maxPoint.x, maxPoint.y, minPoint.z), float3(1, 0, 0), float4(0, 1, 0, -1), float2(1, 0) },

		// -x
		{ float3(minPoint.x, minPoint.y, minPoint.z), float3(-1, 0, 0), float4(0, 1, 0, -1), float2(0, 0) },
		{ float3(minPoint.x, maxPoint.y, minPoint.z), float3(-1, 0, 0), float4(0, 1, 0, -1), float2(1, 0) },
		{ float3(minPoint.x, maxPoint.y, maxPoint.z), float3(-1, 0, 0), float4(0, 1, 0, -1), float2(1, 1) },
		{ float3(minPoint.x, minPoint.y, maxPoint.z), float3(-1, 0, 0), float4(0, 1, 0, -1), float2(0, 1) },

		// +y
		{ float3(minPoint.x, maxPoint.y, minPoint.z), float3(0, 1, 0), float4(1, 0, 0, 1), float2(0, 0) },
		{ float3(maxPoint.x, maxPoint.y, minPoint.z), float3(0, 1, 0), float4(1, 0, 0, 1), float2(1, 0) },
		{ float3(maxPoint.x, maxPoint.y, maxPoint.z), float3(0, 1, 0), float4(1, 0, 0, 1), float2(1, 1) },
		{ float3(minPoint.x, maxPoint.y, maxPoint.z), float3(0, 1, 0), float4(1, 0, 0, 1), float2(0, 1) },

		// -y
		{ float3(minPoint.x, minPoint.y, minPoint.z), float3(0, -1, 0), float4(1, 0, 0, -1), float2(0, 0) },
		{ float3(minPoint.x, minPoint.y, maxPoint.z), float3(0, -1, 0), float4(1, 0, 0, -1), float2(0, 1) },
		{ float3(maxPoint.x, minPoint.y, maxPoint.z), float3(0, -1, 0), float4(1, 0, 0, -1), float2(1, 1) },
		{ float3(maxPoint.x, minPoint.y, minPoint.z), float3(0, -1, 0), float4(1, 0, 0, -1), float2(1, 0) },

		// +z
		{ float3(minPoint.x, minPoint.y, maxPoint.z), float3(0, 0, 1), float4(0, 1, 0, -1), float2(0, 0) },
		{ float3(minPoint.x, maxPoint.y, maxPoint.z), float3(0, 0, 1), float4(0, 1, 0, -1), float2(0, 1) },
		{ float3(maxPoint.x, maxPoint.y, maxPoint.z), float3(0, 0, 1), float4(0, 1, 0, -1), float2(1, 1) },
		{ float3(maxPoint.x, minPoint.y, maxPoint.z), float3(0, 0, 1), float4(0, 1, 0, -1), float2(1, 0) },

		// -z
		{ float3(minPoint.x, minPoint.y, minPoint.z), float3(0, 0, -1), float4(0, 1, 0, -1), float2(0, 0) },
		{ float3(maxPoint.x, minPoint.y, minPoint.z), float3(0, 0, -1), float4(0, 1, 0, -1), float2(1, 0) },
		{ float3(maxPoint.x, maxPoint.y, minPoint.z), float3(0, 0, -1), float4(0, 1, 0, -1), float2(1, 1) },
		{ float3(minPoint.x, maxPoint.y, minPoint.z), float3(0, 0, -1), float4(0, 1, 0, -1), float2(0, 1) },
	};

	uint16 indicies[] =
	{
		// +x
		0 + 0, 0 + 1, 0 + 2,
		0 + 0, 0 + 2, 0 + 3,

		// -x
		4 + 0, 4 + 1, 4 + 2,
		4 + 0, 4 + 2, 4 + 3,

		// +y
		8 + 0, 8 + 1, 8 + 2,
		8 + 0, 8 + 2, 8 + 3,

		// -y
		12 + 0, 12 + 1, 12 + 2,
		12 + 0, 12 + 2, 12 + 3,

		// +z
		16 + 0, 16 + 1, 16 + 2,
		16 + 0, 16 + 2, 16 + 3,

		// -z
		20 + 0, 20 + 1, 20 + 2,
		20 + 0, 20 + 2, 20 + 3,
	};

	int vertexCount = sizeof(vertices) / sizeof(vertices[0]);
	int indexCount = sizeof(indicies) / sizeof(indicies[0]);

	std::shared_ptr<Mesh> mesh;
	mesh.reset(new Mesh());

	auto meshModifier = mesh->BeginModifyOnEmptyData();
	{
		meshModifier->SetVertexCount(vertexCount);
		meshModifier->SetPrimitiveTopology(PT_TriangleList);

		int vertexBufferIndex = meshModifier->AddVertexBuffer(sizeof(Vertex), sizeof(Vertex) * vertexCount, (uint8*)&vertices);

		meshModifier->AddVertexComponent(InputElement::IE_POSITION, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 3));
		meshModifier->AddVertexComponent(InputElement::IE_NORMAL, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 3));
		meshModifier->AddVertexComponent(InputElement::IE_TANGENT, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 4));
		meshModifier->AddVertexComponent(InputElement::IE_TEXCOORD0, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 2));

		meshModifier->UpdateIndexBuffer(IF_UINT16, indexCount, (uint8*)&indicies);
	}
	mesh->EndModify();

	mesh->UpdateBoundingBox();

	return mesh;
}

std::shared_ptr<Mesh> Mesh::CreatePlane(const float3& origin, float lengthX, float lengthZ)
{
	struct Vertex
	{
		float3 pos;
		float3 normal;
		float4 tangent;
		float2 uv;
	};

	Vertex vertices[] =
	{
		{ origin + float3(0, 0, 0),				float3(0, 1, 0), float4(1, 0, 0, 1), float2(0, 0) },
		{ origin + float3(lengthX, 0, 0),		float3(0, 1, 0), float4(1, 0, 0, 1), float2(1, 0) },
		{ origin + float3(lengthX, 0, lengthZ), float3(0, 1, 0), float4(1, 0, 0, 1), float2(1, 1) },
		{ origin + float3(0, 0, lengthZ),		float3(0, 1, 0), float4(1, 0, 0, 1), float2(0, 1) },
	};

	uint16 indicies[] =
	{
		0, 1, 2,
		0, 2, 3,
	};

	int vertexCount = sizeof(vertices) / sizeof(vertices[0]);
	int indexCount = sizeof(indicies) / sizeof(indicies[0]);

	std::shared_ptr<Mesh> mesh;
	mesh.reset(new Mesh());

	auto meshModifier = mesh->BeginModifyOnEmptyData();
	{
		meshModifier->SetVertexCount(vertexCount);
		meshModifier->SetPrimitiveTopology(PT_TriangleList);

		int vertexBufferIndex = meshModifier->AddVertexBuffer(sizeof(Vertex), sizeof(Vertex) * vertexCount, (uint8*)&vertices);

		meshModifier->AddVertexComponent(InputElement::IE_POSITION, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 3));
		meshModifier->AddVertexComponent(InputElement::IE_NORMAL, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 3));
		meshModifier->AddVertexComponent(InputElement::IE_TANGENT, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 4));
		meshModifier->AddVertexComponent(InputElement::IE_TEXCOORD0, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 2));

		meshModifier->UpdateIndexBuffer(IF_UINT16, indexCount, (uint8*)&indicies);
	}
	mesh->EndModify();

	mesh->UpdateBoundingBox();

	return mesh;
}

std::shared_ptr<Mesh> Mesh::CreateQuad(const float2& minPoint, const float2& maxPoint, float z)
{
	struct Vertex
	{
		float3 pos;
		float3 normal;
		float2 uv;
	};

	Vertex vertices[] =
	{
		{ float3(minPoint.x, minPoint.y, z), float3(0, 0, 1), float2(0, 0) },
		{ float3(minPoint.x, maxPoint.y, z), float3(0, 0, 1), float2(0, 1) },
		{ float3(maxPoint.x, maxPoint.y, z), float3(0, 0, 1), float2(1, 1) },
		{ float3(maxPoint.x, minPoint.y, z), float3(0, 0, 1), float2(1, 0) },
	};

	uint16 indicies[] =
	{
		0, 1, 2,
		0, 2, 3,
	};

	int vertexCount = sizeof(vertices) / sizeof(vertices[0]);
	int indexCount = sizeof(indicies) / sizeof(indicies[0]);

	std::shared_ptr<Mesh> mesh;
	mesh.reset(new Mesh());

	auto meshModifier = mesh->BeginModifyOnEmptyData();
	{
		meshModifier->SetVertexCount(vertexCount);
		meshModifier->SetPrimitiveTopology(PT_TriangleList);

		int vertexBufferIndex = meshModifier->AddVertexBuffer(sizeof(Vertex), sizeof(Vertex) * vertexCount, (uint8*)&vertices);

		meshModifier->AddVertexComponent(InputElement::IE_POSITION, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 3));
		meshModifier->AddVertexComponent(InputElement::IE_NORMAL, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 3));
		meshModifier->AddVertexComponent(InputElement::IE_TEXCOORD0, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 2));

		meshModifier->UpdateIndexBuffer(IF_UINT16, indexCount, (uint8*)&indicies);
	}
	mesh->EndModify();

	mesh->UpdateBoundingBox();

	return mesh;
}

std::shared_ptr<Mesh> Mesh::CreateFullScreenQuad()
{
	struct Vertex
	{
		float2 pos;
		float2 uv;
	};

	Vertex vertices[] =
	{
		{ float2(-1, -1), float2(0, 1) },
		{ float2(-1,  1), float2(0, 0) },
		{ float2( 1,  1), float2(1, 0) },
		{ float2( 1, -1), float2(1, 1) },
	};

	uint16 indicies[] =
	{
		0, 1, 2,
		0, 2, 3,
	};

	int vertexCount = sizeof(vertices) / sizeof(vertices[0]);
	int indexCount = sizeof(indicies) / sizeof(indicies[0]);

	std::shared_ptr<Mesh> mesh;
	mesh.reset(new Mesh());

	auto meshModifier = mesh->BeginModifyOnEmptyData();
	{
		meshModifier->SetVertexCount(vertexCount);
		meshModifier->SetPrimitiveTopology(PT_TriangleList);

		int vertexBufferIndex = meshModifier->AddVertexBuffer(sizeof(Vertex), sizeof(Vertex) * vertexCount, (uint8*)&vertices);

		meshModifier->AddVertexComponent(InputElement::IE_POSITION, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 2), 0);
		meshModifier->AddVertexComponent(InputElement::IE_TEXCOORD0, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 2), 8);

		meshModifier->UpdateIndexBuffer(IF_UINT16, indexCount, (uint8*)&indicies);
	}
	mesh->EndModify();

	mesh->UpdateBoundingBox();

	return mesh;
}

std::shared_ptr<Mesh> Mesh::CreateSphere(const float3& center, float radius, uint segment_u, uint segment_v)
{
	struct Vertex
	{
		float3 pos;
		float3 normal;

		Vertex()
		{}

		Vertex(const float3& pos, const float3& normal)
			:pos(pos), normal(normal)
		{}
	};

	uint vertex_count = 2 + (segment_v - 1) * segment_u;

	float du = 2 * PI / segment_u;
	float dv = PI / segment_v;

	std::vector<Vertex> vertices;
	vertices.resize(vertex_count);

	vertices[0] = Vertex(center + float3(0.0f, radius, 0.0f), float3(0, 1, 0));
	vertices[1] = Vertex(center + float3(0.0f, -radius, 0.0f), float3(0, -1, 0));

#define map_uv_to_index(u, v) (((v) - 1) + (u) * (segment_v - 1) + 2)

	for(uint v = 1; v < segment_v; v++)
	{
		for(uint u = 0; u < segment_u; u++)
		{
			float lat = dv * v;
			float r = radius * sin(lat);

			float3 d = float3(cos(du * u) * r, cos(lat) * radius, sin(du * u) * r);

			vertices[map_uv_to_index(u, v)] = Vertex(center + d, d);
		}
	}
	
	uint triangle_count = segment_u * 2 + segment_u * (segment_v - 2) * 2;

	std::vector<uint> indicies;
	indicies.reserve(triangle_count * 3);

	for (uint u = 0; u < segment_u; u++) {
		indicies.push_back(0);
		indicies.push_back(map_uv_to_index(u, 1));
		indicies.push_back(map_uv_to_index((u + 1) % segment_u, 1));
	}

	for (uint u = 0; u < segment_u; u++) {
		for (uint v = 1; v <= segment_v - 2; v++) {
			indicies.push_back(map_uv_to_index(u, v));
			indicies.push_back(map_uv_to_index(u, v + 1));
			indicies.push_back(map_uv_to_index((u + 1) % segment_u, v));

			indicies.push_back(map_uv_to_index(u, v + 1));
			indicies.push_back(map_uv_to_index((u + 1) % segment_u, v + 1));
			indicies.push_back(map_uv_to_index((u + 1) % segment_u, v));
		}
	}

	for (uint u = 0; u < segment_u; u++) {
		indicies.push_back(map_uv_to_index(u, segment_v - 1));
		indicies.push_back(1);
		indicies.push_back(map_uv_to_index((u + 1) % segment_u, segment_v - 1));
	}

	int vertexCount = vertices.size();
	int indexCount = indicies.size();

	std::shared_ptr<Mesh> mesh;
	mesh.reset(new Mesh());

	auto meshModifier = mesh->BeginModifyOnEmptyData();
	{
		meshModifier->SetVertexCount(vertexCount);
		meshModifier->SetPrimitiveTopology(PT_TriangleList);

		int vertexBufferIndex = meshModifier->AddVertexBuffer(sizeof(Vertex), sizeof(Vertex) * vertexCount, (uint8*)vertices.data());

		meshModifier->AddVertexComponent(InputElement::IE_POSITION, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 3), 0);
		meshModifier->AddVertexComponent(InputElement::IE_NORMAL, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 3), 12);
		meshModifier->AddVertexComponent(InputElement::IE_TEXCOORD0, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 2), 24);

		meshModifier->UpdateIndexBuffer(indicies);
	}
	mesh->EndModify();

	mesh->UpdateBoundingBox();

	return mesh;
}

static float3 make_perpendicular_vector(const float3& v)
{
	float3 v1 = cross(v, float3(1, 0, 0));
	if (length_square(v1) <= FLT_EPSILON)
		v1 = cross(v, float3(0, 1, 0));

	return v1;
}

std::shared_ptr<Mesh> Mesh::CreateCone(const float3& apex_pos, const float3& base_center, float base_radius, uint nr_base_segments, bool cap)
{
	//
	// make positions
	//
	std::vector<float3> positions;
	positions.reserve(nr_base_segments + 2);

	float3 base_y = apex_pos - base_center;
	float3 base_x = make_perpendicular_vector(base_y); base_x = normalize(base_x); base_x *= base_radius;
	float3 base_z = cross(base_x, base_y); base_z = normalize(base_z); base_z *= base_radius;

	positions.push_back(apex_pos);

	float theta = 2 * PI / nr_base_segments;
	for (uint i = 0; i < nr_base_segments; i++)
	{
		positions.push_back(base_center + base_x * cos(theta * i) + base_z * sin(theta * i));
	}

	if (cap)
	{
		positions.push_back(base_center);
	}

	//
	// make indexes
	//
	std::vector<uint> indicies;

	for(uint i=0; i<nr_base_segments; i++)
	{
		indicies.push_back(i + 1);
		indicies.push_back(0);
		indicies.push_back(((i + 1) % nr_base_segments) + 1);
	}

	if(cap)
	{
		for(uint i=0; i<nr_base_segments; i++)
		{
			indicies.push_back(i + 1);
			indicies.push_back(((i + 1) % nr_base_segments) + 1);
			indicies.push_back(positions.size() - 1);
		}
	}

	int vertexCount = positions.size();
	int indexCount = indicies.size();

	std::shared_ptr<Mesh> mesh;
	mesh.reset(new Mesh());

	auto meshModifier = mesh->BeginModifyOnEmptyData();
	{
		meshModifier->SetVertexCount(vertexCount);
		meshModifier->SetPrimitiveTopology(PT_TriangleList);

		int vertexBufferIndex = meshModifier->AddVertexBuffer(sizeof(float3), sizeof(float3) * vertexCount, (uint8*)positions.data());

		meshModifier->AddVertexComponent(InputElement::IE_POSITION, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 3), 0);

		meshModifier->UpdateIndexBuffer(indicies);
	}
	mesh->EndModify();

	mesh->UpdateBoundingBox();

	return mesh;
}
