#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "ForwardSceneRenderPass.h"
#include "ColorRenderTarget.h"
#include "DepthStencilRenderTarget.h"
#include "EntitySystem/Camera.h"
#include "EntitySystem/Transform.h"
#include "JobSystem/JobSystem.h"

//////////////////////////////////////////////////////////////////////////
#if 0
struct DrawBatchKeyOpaque
{
	int renderQueue;
	ShaderPassPtr shaderPass;
	MaterialRenderDataPtr material;
	MeshRenderDataPtr mesh;

	bool operator < (const DrawBatchKeyOpaque& other) const
	{
		if (renderQueue < other.renderQueue)
			return true;
		else if (shaderPass < other.shaderPass)
			return true;
		else if (material < other.material)
			return true;
		else if (mesh < other.mesh)
			return true;
		else
			return false;
	}

	bool operator == (const DrawBatchKeyOpaque& other) const
	{
		return renderQueue == other.renderQueue
			&& shaderPass == other.shaderPass
			&& material == other.material
			&& mesh == other.mesh;
	}
};

template<>
struct std::hash<DrawBatchKeyOpaque>
{
	std::size_t operator()(const DrawBatchKeyOpaque& v) const noexcept
	{
		size_t hash = 0;
		hash = hash_combine<64>::combine(hash, (uint64)v.renderQueue);
		hash = hash_combine<64>::combine(hash, (uint64)v.shaderPass.get());
		hash = hash_combine<64>::combine(hash, (uint64)v.material.get());
		hash = hash_combine<64>::combine(hash, (uint64)v.mesh.get());
		return hash;
	}
};

struct DrawBatchKeyTransparent
{
	int renderQueue;
	float distance;

	bool operator < (const DrawBatchKeyTransparent& other) const
	{
		if (renderQueue < other.renderQueue)
			return true;
		else if (distance < other.distance)
			return true;
		else
			return false;
	}

	bool operator == (const DrawBatchKeyTransparent& other) const
	{
		return renderQueue == other.renderQueue
			&& distance == other.distance;
	}
};

template<>
struct std::hash<DrawBatchKeyTransparent>
{
	std::size_t operator()(const DrawBatchKeyTransparent& v) const noexcept
	{
		size_t hash = 0;
		hash = hash_combine<64>::combine(hash, (uint64)v.renderQueue);
		hash = hash_combine<64>::combine(hash, (uint64)v.distance);
		return hash;
	}
};
#endif
//////////////////////////////////////////////////////////////////////////

struct OpaqueObjectSorter
{
	MaterialRenderDataPtr* objectMaterials;
	MeshRenderDataPtr* objectMeshes;

	bool operator () (uint32 objectIndex1, uint32 objectIndex2)
	{
		MaterialRenderDataPtr mat1 = objectMaterials[objectIndex1];
		MaterialRenderDataPtr mat2 = objectMaterials[objectIndex1];

		ShaderPtr shader1 = mat1->GetShader();  //TODO: sort by shader variant, not shader
		ShaderPtr shader2 = mat2->GetShader();

		if (shader1 != shader2)
		{
			return shader1 < shader2;
		}

		if (mat1 != mat2)
		{
			return mat1 < mat2;
		}

		MeshRenderDataPtr mesh1 = objectMeshes[objectIndex1];
		MeshRenderDataPtr mesh2 = objectMeshes[objectIndex1];
		if (mesh1 != mesh2)
		{
			return mesh1 < mesh2;
		}

		return objectIndex1 < objectIndex2;
	}
};

struct TransparentObjectSorter
{
	float* objectDistances;

	bool operator () (uint32 objectIndex1, uint32 objectIndex2)
	{
		float distance1 = objectDistances[objectIndex1];
		float distance2 = objectDistances[objectIndex2];
		return distance1 > distance2;
	}
};

//////////////////////////////////////////////////////////////////////////

ForwardSceneRenderPass::ForwardSceneRenderPass(RenderPipeline* pipeline, RenderScene* scene)
	: SceneRenderPass(SceneRenderPassType::ForwardShading, "ForwardSceneRenderPass", pipeline, scene)
{
}

ForwardSceneRenderPass::~ForwardSceneRenderPass()
{
}

void ForwardSceneRenderPass::Prepare(RenderSceneCamera* camera, RHICommandListPtr rhiCommandList)
{
	PrepareCommandlists();
	PreparePassCBuffer(camera, rhiCommandList);
}

void ForwardSceneRenderPass::PrepareCommandlists()
{
	RHIDevicePtr rhiDevice = RenderSystem::instance().rhiDevice;

	for (int i = 0; i < (int)SceneObjectRenderQueueType::_Count; i++)
	{
		if (!commandlists[i])
			commandlists[i] = rhiDevice->CreateCommandList();
	}
}

void ForwardSceneRenderPass::GetOrCreatePassParamsRHIBuffer(ShaderPassPtr shaderPass, RHIDevice* rhiDevice, RHIBufferPtr& outPassParamsRHICBuffer)
{
	outPassParamsRHICBuffer = cbufferGpuData;
}

void ForwardSceneRenderPass::PreparePassCBuffer(RenderSceneCamera* camera, RHICommandListPtr rhiCommandList)
{
	passParams.Reset();

	cbufferCpuData.viewMatrix = camera->viewMatrix;
	cbufferCpuData.projMatrix = camera->projectionMatrix;
	cbufferCpuData.viewProjMatrix = camera->viewProjectionMatrix;

	cbufferCpuData.cameraPos = camera->position;

	float4 cameraParams = float4(camera->znear, camera->zfar, camera->fov, (camera->viewport.w / (float)camera->viewport.h));
	cbufferCpuData.cameraParams = cameraParams;

	RenderSceneLight* mainLight = scene->lights.size() != 0 ? scene->lights[0] : nullptr;
	if (mainLight)
	{
		cbufferCpuData.mainLightDir = normalize(-mainLight->direction);
		cbufferCpuData.mainLightColor = mainLight->color * mainLight->intensity;
	}
	else
	{
		cbufferCpuData.mainLightDir = float3(0, 0, 0);
		cbufferCpuData.mainLightColor = float3(0, 0, 0);
	}

	//passParams.SetFloat4x4("viewMatrix", camera->viewMatrix);
	//passParams.SetFloat4x4("projMatrix", camera->projectionMatrix);
	//passParams.SetFloat4x4("viewProjMatrix", camera->viewProjectionMatrix);
	//
	//passParams.SetFloat3("cameraPos", camera->position);
	//
	//passParams.SetFloat4("cameraParams", cameraParams);
	//
	//if (mainLight)
	//{
	//	RenderSceneLight* renderSceneLight = mainLight->renderSceneLight;
	//
	//	passParams.SetFloat3("mainLightDir", normalize(-renderSceneLight->direction));
	//	passParams.SetFloat3("mainLightColor", renderSceneLight->color * renderSceneLight->intensity);
	//}
	//else
	//{
	//	passParams.SetFloat3("mainLightDir", float3(0, 0, 0));
	//	passParams.SetFloat3("mainLightColor", float3(0, 0, 0));
	//}

	RenderSceneSettings& renderSceneSettings = scene->settings;

	cbufferCpuData.diffuseIndirectColor = renderSceneSettings.diffuseIndirectColor;
	//passParams.SetFloat3("diffuseIndirectColor", renderSceneSettings.diffuseIndirectColor);

	if (renderSceneSettings.diffuseIndirectLightSource == DiffuseIndirectLightSource::Color)
	{
		passParams.SetVariantKey("DiffuseIndirectType", "DiffuseIndirect_Color");
	}
	else if (renderSceneSettings.diffuseIndirectLightSource == DiffuseIndirectLightSource::SkyboxSH)
	{
		passParams.SetVariantKey("DiffuseIndirectType", "DiffuseIndirect_SH");

		cbufferCpuData.diffuseIndirectSH = renderSceneSettings.diffuseIndirectSH;
		//passParams.SetSH3RGB("diffuseIndirectSH", renderSceneSettings.diffuseIndirectSH);
	}
	else if (renderSceneSettings.diffuseIndirectLightSource == DiffuseIndirectLightSource::SkyboxTexture)
	{
		passParams.SetVariantKey("DiffuseIndirectType", "DiffuseIndirect_Texture");

		passParams.SetResource("diffuseIndirectTexture", renderSceneSettings.diffuseIndirectTexture);
	}

	if (cbufferGpuData)
	{
		rhiCommandList->UpdateBufferData(cbufferGpuData, &cbufferCpuData, sizeof(cbufferCpuData));
	}
	else
	{
		RHIDevicePtr rhiDevice = RenderSystem::instance().rhiDevice;
		BufferDesc bufferDesc(sizeof(cbufferCpuData), BufferBindFlag::BBF_CONSTANT_BUFFER, BufferUsage::BU_DEFAULT, CpuAccessFlag::CAF_NONE);
		cbufferGpuData = rhiDevice->CreateBuffer(bufferDesc, &cbufferCpuData);
	}
}

/*
bool ForwardSceneRenderPass::UpdateRHIResources(RHICommandList* rhiCommandList)
{
	for (PassParamsData* passParamsData : passParamsDatas)
	{
		ShaderCBufferSlot* passParamsCBuffer = passParamsData->shaderPass->GetPassParamsCBuffer();

		BuildPassParamsCpuData(passParamsCBuffer, passParamsData->cpuBuffer);

		rhiCommandList->UpdateBufferData(passParamsData->rhiBuffer, passParamsData->cpuBuffer.data(), passParamsData->cpuBuffer.size());
	}

	return true;
}

void ForwardSceneRenderPass::BuildPassParamsCpuData(ShaderCBufferSlot* passParamsCBuffer, std::vector<uint8>& cpuBuffer)
{
	cpuBuffer.resize(passParamsCBuffer->byteSize);

	for (uint32 iVariable = 0; iVariable < passParamsCBuffer->variables.size(); iVariable++)
	{
		ShaderVariable* variable = passParamsCBuffer->variables[iVariable];

		void* variableValueAddr = passParams.GetVariableValueAddr(variable->name);
		if (!variableValueAddr)
			variableValueAddr = passParamsCBuffer->defaultCBufferData.data() + variable->byteOffset;

		memcpy(cpuBuffer.data() + variable->byteOffset, variableValueAddr, variable->byteSize);
	}
}

void ForwardSceneRenderPass::GetOrCreatePassParamsRHIBuffer(ShaderPassPtr shaderPass, RHIDevice* rhiDevice, RHIBufferPtr& outPassParamsRHICBuffer)
{
	auto it = passParamsCBufferLayoutHashMap.find(shaderPass->passParamsLayoutHash);
	if (it != passParamsCBufferLayoutHashMap.end())
	{
		PassParamsData* passParamsData = it->second;
		outPassParamsRHICBuffer = passParamsData->rhiBuffer;
		return;
	}

	ShaderCBufferSlot* passParamsCBuffer = shaderPass->GetPassParamsCBuffer();
	if (passParamsCBuffer)
	{
		PassParamsData* passParamsData = new PassParamsData();
		passParamsData->shaderPass = shaderPass;

		BuildPassParamsCpuData(passParamsCBuffer, passParamsData->cpuBuffer);

		BufferDesc bufferDesc(passParamsCBuffer->byteSize, BufferBindFlag::BBF_CONSTANT_BUFFER, BufferUsage::BU_DEFAULT, CpuAccessFlag::CAF_NONE);
		passParamsData->rhiBuffer = rhiDevice->CreateBuffer(bufferDesc, passParamsData->cpuBuffer.data());

		passParamsDatas.push_back(passParamsData);
		passParamsCBufferLayoutHashMap.insert(std::make_pair(shaderPass->passParamsLayoutHash, passParamsData));

		outPassParamsRHICBuffer = passParamsData->rhiBuffer;
	}
	else
	{
		outPassParamsRHICBuffer = RHIBufferPtr();
	}
}
//*/