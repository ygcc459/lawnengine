#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "RenderSystem.h"
#include "TextRenderer.h"

#if VCPP_PLATFORM_FAMILY == VCPP_PLATFORM_FAMILY_WINDOWS
#	include "D3D11RHI/D3D11RHI.h"
#else
#	error RHI not supported!
#endif

RenderSystem RenderSystem::_instance;

RenderSystem::RenderSystem()
{
}

RenderSystem::~RenderSystem()
{
}

bool RenderSystem::startup()
{
	Log::Debug("RenderSystem::startup...");

#if VCPP_PLATFORM_FAMILY == VCPP_PLATFORM_FAMILY_WINDOWS
	rhiDevice = RHI::CreateRHIDeviceD3D11();
#else
#	error RHI not supported!
#endif

	if (!rhiDevice)
	{
		Log::Error("Create RHI Device failed");
		return false;
	}

	if (!rhiDevice->Startup())
	{
		Log::Error("RHIDevice Startup failed");
		return false;
	}

	Sampler::CreateDefaultSamplers();

	TextRenderer::CreateInstance();

	Log::Debug("RenderSystem::startup finished");

	return true;
}

void RenderSystem::shutdown()
{
	Log::Debug("RenderSystem::shutdown...");

	TextRenderer::DestroyInstance();

	Sampler::DestroyDefaultSamplers();

	if (rhiDevice)
	{
		rhiDevice->Shutdown();
	}
	rhiDevice = nullptr;

	Log::Debug("RenderSystem::shutdown finished");
}

MeshPtr RenderSystem::FullScreenQuadMesh()
{
	if (!fullScreenQuadMesh)
	{
		fullScreenQuadMesh = Mesh::CreateQuad(float2(-1.0f, 1.0f), float2(1.0f, -1.0f), 0.0f);
	}

	return fullScreenQuadMesh;
}
