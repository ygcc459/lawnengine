#include "../fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "Material.h"

//////////////////////////////////////////////////////////////////////////

IMPL_CLASS_TYPE(Material);

Material::Material()
{
}

Material::~Material()
{
}

void Material::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("shader", &Self::shader);
	reflector.AddMember("values", &Self::shaderParamValues);
}

void Material::OnDestroy()
{
	shaderParamValues.Reset();
	shader = nullptr;
}

void Material::SetDirty()
{
	renderDataDirty = true;
	Super::SetDirty();
}

void Material::Reset()
{
	shaderParamValues.Reset();
	shader = nullptr;
}

void Material::OnCreateNewAsset()
{

}

bool Material::OnLoadAsset()
{
	return false;
}

bool Material::OnSaveAsset()
{
	return false;
}

void Material::BindShader(ShaderPtr shader, bool force /*= false*/)
{
	if (shader != this->shader || force)
	{
		this->shader = shader;
	}
}

MaterialRenderDataPtr Material::GetMaterialRenderData()
{
	if (renderData && !renderDataDirty)
		return renderData;

	renderDataDirty = false;

	renderData = new MaterialRenderData();
	renderData->shader = this->shader;
	renderData->shaderParamValues.CopyFrom(this->shaderParamValues);

	return renderData;
}

//////////////////////////////////////////////////////////////////////////

std::atomic<uint32> MaterialRenderData::nextMaterialRenderDataID(0);

MaterialRenderData::MaterialRenderData()
	: materialRenderDataID(nextMaterialRenderDataID++)
{
}

MaterialRenderData::~MaterialRenderData()
{
	for (MaterialParamsBuffer* p : materialParamsBuffers)
		delete p;
	materialParamsBuffers.clear();

	materialParamsBuffersLayoutHashMap.clear();
}

void MaterialRenderData::BuildMaterialParamsCpuData(ShaderCBufferSlot* materialParamsCBuffer, std::vector<uint8>& cpuBuffer)
{
	cpuBuffer.resize(materialParamsCBuffer->byteSize);

	for (uint32 iVariable = 0; iVariable < materialParamsCBuffer->variables.size(); iVariable++)
	{
		ShaderVariable* variable = materialParamsCBuffer->variables[iVariable];

		void* variableValueAddr = shaderParamValues.GetVariableValueAddr(variable->name);
		if (!variableValueAddr)
			variableValueAddr = materialParamsCBuffer->defaultCBufferData.data() + variable->byteOffset;

		memcpy(cpuBuffer.data() + variable->byteOffset, variableValueAddr, variable->byteSize);
	}
}

bool MaterialRenderData::GetOrCreateMaterialParamsRHIBuffer(ShaderPassPtr shaderPass, RHIDevice* rhiDevice, RHIBufferPtr& outMaterialParamsRHICBuffer)
{
	auto it = materialParamsBuffersLayoutHashMap.find(shaderPass->materialParamsLayoutHash);
	if (it != materialParamsBuffersLayoutHashMap.end())
	{
		MaterialParamsBuffer* materialParamsBuffer = it->second;
		outMaterialParamsRHICBuffer = materialParamsBuffer->rhiBuffer;
		return true;
	}

	ShaderCBufferSlot* materialParamsCBuffer = shaderPass->GetMaterialParamsCBuffer();
	if (materialParamsCBuffer)
	{
		MaterialParamsBuffer* materialParamsBuffer = new MaterialParamsBuffer();
		materialParamsBuffer->shaderPass = shaderPass;

		BuildMaterialParamsCpuData(materialParamsCBuffer, materialParamsBuffer->cpuBuffer);

		BufferDesc bufferDesc(materialParamsCBuffer->byteSize, BufferBindFlag::BBF_CONSTANT_BUFFER, BufferUsage::BU_DEFAULT, CpuAccessFlag::CAF_NONE);
		materialParamsBuffer->rhiBuffer = rhiDevice->CreateBuffer(bufferDesc, materialParamsBuffer->cpuBuffer.data());

		materialParamsBuffers.push_back(materialParamsBuffer);
		materialParamsBuffersLayoutHashMap.insert(std::make_pair(shaderPass->passParamsLayoutHash, materialParamsBuffer));

		outMaterialParamsRHICBuffer = materialParamsBuffer->rhiBuffer;
		return true;
	}
	else
	{
		outMaterialParamsRHICBuffer = RHIBufferPtr();
		return false;
	}
}
