#pragma once

#include "RHI/RHICommon.h"
#include "RHI/RHIDevice.h"
#include "RenderSystem/RenderSystem.h"

#include "../AssetSystem/AssetManager.h"

#include "RHI/RHIInputAssembler.h"
#include "RHI/RHIResources.h"

#include <vcpp/ArrayView.h>

#include <mutex>

class MeshDrawCommand;
class ShaderPass;
class RenderResourcesManager;

struct MeshVertexBuffer
{
	int size;
	int stride;
	std::vector<uint8> cpuBuffer;

	MeshVertexBuffer()
	{
		Reset();
	}

	~MeshVertexBuffer()
	{
	}

	void Reset()
	{
		size = 0;
		stride = 0;
	}
};

class MeshVertexBufferType : public PrimitiveObjectType<MeshVertexBuffer>
{
public:
	MeshVertexBufferType()
		: PrimitiveObjectType<MeshVertexBuffer>("MeshVertexBuffer")
	{}

	virtual bool Serialize(Serializer& serializer, void* p) const override
	{
		ValueType& obj = *reinterpret_cast<ValueType*>(p);

		serializer.SerializeMemberAuto("size", obj.size);
		serializer.SerializeMemberAuto("stride", obj.stride);
		serializer.SerializeMemberRawData("cpuBuffer", obj.cpuBuffer);

		return true;
	}

	virtual bool Deserialize(Deserializer& deserializer, void* p) const override
	{
		ValueType& obj = *reinterpret_cast<ValueType*>(p);

		deserializer.DeserializeMemberAuto("size", obj.size);
		deserializer.DeserializeMemberAuto("stride", obj.stride);
		deserializer.DeserializeMemberRawData("cpuBuffer", obj.cpuBuffer);

		return true;
	}
};
DECL_PRIMITIVE_TYPE(MeshVertexBuffer)


struct MeshIndexBuffer
{
	IndexFormat indexFormat;
	uint indexCount;
	std::vector<uint8> cpuBuffer;

	MeshIndexBuffer()
	{
		Reset();
	}

	~MeshIndexBuffer()
	{
	}

	int stride() const { return indexFormat == IF_UINT16 ? 2 : 4; }

	int size() const { return indexCount * stride(); }

	void Reset()
	{
		indexFormat = IF_UINT16;
		indexCount = 0;
	}
};

class MeshIndexBufferType : public PrimitiveObjectType<MeshIndexBuffer>
{
public:
	MeshIndexBufferType()
		: PrimitiveObjectType<MeshIndexBuffer>("MeshIndexBuffer")
	{}

	virtual bool Serialize(Serializer& serializer, void* p) const override
	{
		ValueType& obj = *reinterpret_cast<ValueType*>(p);

		serializer.SerializeMemberAuto("indexFormat", obj.indexFormat);
		serializer.SerializeMemberAuto("indexCount", obj.indexCount);
		serializer.SerializeMemberRawData("cpuBuffer", obj.cpuBuffer);

		return true;
	}

	virtual bool Deserialize(Deserializer& deserializer, void* p) const override
	{
		ValueType& obj = *reinterpret_cast<ValueType*>(p);

		deserializer.DeserializeMemberAuto("indexFormat", obj.indexFormat);
		deserializer.DeserializeMemberAuto("indexCount", obj.indexCount);
		deserializer.DeserializeMemberRawData("cpuBuffer", obj.cpuBuffer);

		return true;
	}
};
DECL_PRIMITIVE_TYPE(MeshIndexBuffer)

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// read-only mesh data, used across different threads
class MeshData : public BaseObject, public RefCountedObject
{
	DECL_CLASS_TYPE(MeshData, BaseObject);

protected:
	std::vector<MeshVertexBuffer*> vertexBuffers;

	MeshIndexBuffer indexBuffer;

	std::vector<MeshVertexComponent> vertexComponents;

	uint32 vertexCount = 0;

	PrimitiveTopology primitiveTopology = PT_Unknown;

public:
	MeshData() {}
	~MeshData() {}

	virtual void Reflect(Reflector& reflector);

	void CopyTo(MeshData& target);

	uint32 GetVertexCount() const { return vertexCount; }

	PrimitiveTopology GetPrimitiveTopology() const { return primitiveTopology; };

	const std::vector<MeshVertexBuffer*> GetVertexBuffers() { return vertexBuffers; }
	const MeshIndexBuffer GetIndexBuffer() { return indexBuffer; }
	const std::vector<MeshVertexComponent>& GetVertexComponents() { return vertexComponents; }

	const MeshVertexComponent* FindVertexComponent(InputElement inputElement)
	{
		for (MeshVertexComponent& component : vertexComponents)
			if (component.inputElement == inputElement)
				return &component;
		return nullptr;
	}

	template<typename T>
	const vcpp::StridedArrayView<T> GetVertexDataView(int vertexBufferIndex)
	{
		MeshVertexBuffer* vertexBuffer = vertexBuffers[vertexBufferIndex];
		return GetVertexDataView<T>(vertexBuffer);
	}

	template<typename T>
	const vcpp::StridedArrayView<T> GetVertexDataView(InputElement inputElement)
	{
		const MeshVertexComponent* vertexComponent = FindVertexComponent(inputElement);
		return GetVertexDataView<T>(vertexComponent);
	}

	template<typename T>
	const vcpp::StridedArrayView<T> GetVertexDataView(const MeshVertexComponent* vertexComponent)
	{
		if (vertexComponent)
		{
			MeshVertexBuffer* vertexBuffer = vertexBuffers[vertexComponent->vertexBufferIndex];
			return vcpp::StridedArrayView<T>(vertexBuffer->cpuBuffer.data(), vertexCount, vertexComponent->offset, vertexBuffer->stride);
		}
		else
		{
			return vcpp::StridedArrayView<T>();
		}
	}

	template<typename T>
	const vcpp::StridedArrayView<T> GetVertexDataView(MeshVertexBuffer* vertexBuffer)
	{
		return vcpp::StridedArrayView<T>(vertexBuffer->cpuBuffer.data(), vertexCount, 0, sizeof(T));
	}

	template<typename T>
	const vcpp::ArrayView<T> GetIndexDataView() 
	{
		return vcpp::ArrayView<T>(indexBuffer.cpuBuffer.data(), indexBuffer.indexCount);
	}

	uint32 GetIndexCount() const { return indexBuffer.indexCount; }
	const std::vector<uint8>& GetIndexData() { return indexBuffer.cpuBuffer; }

	bool HasIndicies() const { return indexBuffer.indexCount > 0; }
	IndexFormat GetIndexFormat() const { return indexBuffer.indexFormat; }

	int CalcVertexBufferComponentsSize(int vertexBufferIndex) const;
};

typedef RefCountedPtr<MeshData> MeshDataPtr;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// the only thing that can modify a mesh, works in a single thread
class MeshDataWrittable : public MeshData
{
public:
	MeshDataWrittable() {}
	~MeshDataWrittable() {}

	void ClearVertexIndexData();
	void ClearAll();

	void SetVertexCount(uint32 vertexCount);
	void SetPrimitiveTopology(PrimitiveTopology primitiveTopology);

	int AddVertexBuffer(int stride = 0, int size = 0, const void* data = nullptr);
	void SetVertexBufferStride(int vertexBufferIndex, int stride);
	void UpdateVertexBuffer(int vertexBufferIndex, int size, const void* data = nullptr);

	int AddVertexComponent(
		InputElement inputElement,
		int vertexBufferIndex,
		MeshVertexFormat format,
		int offset = -1);

	// will write data to vertex buffer
	template<class T>
	int AddVertexComponent(
		InputElement inputElement,
		int vertexBufferIndex,
		MeshVertexFormat format,
		const vcpp::StridedArrayView<T>& data,
		int offset = -1);

	MeshVertexComponent* FindVertexComponent(InputElement inputElement);

	template<typename T>
	vcpp::StridedArrayView<T> GetVertexDataView(int vertexBufferIndex)
	{
		MeshVertexBuffer* vertexBuffer = vertexBuffers[vertexBufferIndex];
		return GetVertexDataView<T>(vertexBuffer);
	}

	template<typename T>
	vcpp::StridedArrayView<T> GetVertexDataView(InputElement inputElement)
	{
		MeshVertexComponent* vertexComponent = FindVertexComponent(inputElement);
		return GetVertexDataView<T>(vertexComponent);
	}

	template<typename T>
	vcpp::StridedArrayView<T> GetVertexDataView(MeshVertexComponent* vertexComponent)
	{
		if (vertexComponent)
		{
			MeshVertexBuffer* vertexBuffer = vertexBuffers[vertexComponent->vertexBufferIndex];
			return vcpp::StridedArrayView<T>(vertexBuffer->cpuBuffer.data(), vertexCount, vertexComponent->offset, vertexBuffer->stride);
		}
		else
		{
			return vcpp::StridedArrayView<T>();
		}
	}

	template<typename T>
	vcpp::StridedArrayView<T> GetVertexDataView(MeshVertexBuffer* vertexBuffer)
	{
		return vcpp::StridedArrayView<T>(vertexBuffer->cpuBuffer.data(), vertexCount, 0, sizeof(T));
	}

	template<typename T>
	vcpp::ArrayView<T> GetIndexDataView()
	{
		return vcpp::ArrayView<T>(indexBuffer.cpuBuffer.data(), indexBuffer.indexCount);
	}

	uint32 GetIndexCount() const
	{
		return indexBuffer.indexCount;
	}

	std::vector<uint8>& GetIndexData()
	{
		return indexBuffer.cpuBuffer;
	}

	// return first added vertex index
	uint AddVertices(int count = 1);
	void ClearVertices();

	void AddTriangle(uint32 index0, uint32 index1, uint32 index2)
	{
		if (indexBuffer.indexFormat == IF_UINT16)
		{
			uint16 indicies[3] = { (uint16)index0, (uint16)index1, (uint16)index2 };
			AddIndicies(vcpp::ArrayView<uint16>(indicies, sizeof(indicies) / sizeof(indicies[0])));
		}
		else if (indexBuffer.indexFormat == IF_UINT32)
		{
			uint32 indicies[3] = { index0, index1, index2 };
			AddIndicies(vcpp::ArrayView<uint32>(indicies, sizeof(indicies) / sizeof(indicies[0])));
		}
	}
	void AddIndicies(const vcpp::ArrayView<uint16>& indicies)
	{
		AppendIndexBuffer((uint8*)&indicies[0], indicies.size() * sizeof(uint16));
	}
	void AddIndicies(const vcpp::ArrayView<uint32>& indicies)
	{
		AppendIndexBuffer((uint8*)&indicies[0], indicies.size() * sizeof(uint32));
	}

	template<class T>
	void UpdateVertexComponent(MeshVertexComponent* vertexComponent, const vcpp::StridedArrayView<T>& data);

	void AppendIndexBuffer(uint8* data, uint32 byteSize);
	void UpdateIndexBuffer(IndexFormat indexFormat, uint indexCount, uint8* data);

	void UpdateIndexBuffer(const std::vector<uint16>& indicies)
	{
		UpdateIndexBuffer(IF_UINT16, (uint32)indicies.size(), (uint8*)indicies.data());
	}
	void UpdateIndexBuffer(const std::vector<uint32>& indicies)
	{
		UpdateIndexBuffer(IF_UINT32, (uint32)indicies.size(), (uint8*)indicies.data());
	}

	bool HasIndicies() const { return indexBuffer.indexCount > 0; }
	IndexFormat GetIndexFormat() const { return indexBuffer.indexFormat; }
};

template<class T>
int MeshDataWrittable::AddVertexComponent(InputElement inputElement, int vertexBufferIndex, MeshVertexFormat format, const vcpp::StridedArrayView<T>& data, int offset /*= -1*/)
{
	int componentIndex = AddVertexComponent(inputElement, vertexBufferIndex, format, offset);

	UpdateVertexComponent(&vertexComponents[componentIndex], data);

	return componentIndex;
}

template<class T>
void MeshDataWrittable::UpdateVertexComponent(MeshVertexComponent* vertexComponent, const vcpp::StridedArrayView<T>& data)
{
	MeshVertexBuffer* vertexBuffer = vertexBuffers[vertexComponent->vertexBufferIndex];

	CHECK(data.size() > 0);
	CHECK(vertexComponent->offset + vertexBuffer->stride * (data.size() - 1) + sizeof(T) <= vertexBuffer->cpuBuffer.size());

	uint8* writePtr = vertexBuffer->cpuBuffer.data() + vertexComponent->offset;

	for (int i = 0; i < data.size(); i++)
	{
		*reinterpret_cast<T*>(writePtr) = data[i];
		writePtr += vertexBuffer->stride;
	}
}

typedef RefCountedPtr<MeshDataWrittable> MeshDataWrittablePtr;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// mesh render data, used in a single render-thread
class MeshRenderData : public RefCountedObject
{
public:
	static std::atomic<uint32> nextMeshRenderDataID;

	uint32 meshRenderDataID;

	MeshDataPtr data;

	std::vector<RHIBufferPtr> rhiVertexBuffers;
	RHIBufferPtr rhiIndexBuffer;

	AxisAlignedBox3<float> boundingBox;

protected:
	static MeshVertexBuffer dummyVertexBuffer;
	static RHIBufferPtr rhiDummyVertexBuffer;

public:
	MeshRenderData();
	~MeshRenderData();

	uint32 GetMeshRenderDataID() const { return meshRenderDataID; }

	MeshDataPtr GetMeshData() { return data; }

	bool HasIndicies() const { return data->HasIndicies(); }
	uint32 GetVertexCount() const { return data->GetVertexCount(); }
	uint32 GetIndexCount() const { return data->GetIndexCount(); }
	IndexFormat GetIndexFormat() const { return data->GetIndexFormat(); }

	AxisAlignedBox3<float> GetBoundingBox() const { return boundingBox; }

	PrimitiveTopology GetPrimitiveTopology() const { return data->GetPrimitiveTopology(); };

	std::vector<RHIBufferPtr>& GetRHIVertexBuffers() { return rhiVertexBuffers; }
	RHIBufferPtr GetRHIIndexBuffer() { return rhiIndexBuffer; }

	//
	// rendering
	//
	bool CreateRHIResources(RHIDevice* device);
	void DestroyRHIResources();
	bool IsRHIResourcesCreated() const;

	//
	// DummyVertexBuffer
	//
	static void EnsureDummyVertexBufferSize(int byteSize, RHIDevice* rhiDevice);
	static RHIBufferPtr GetDummyVertexBuffer();

};

typedef RefCountedPtr<MeshRenderData> MeshRenderDataPtr;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Mesh : public Asset
{
	DECL_CLASS_TYPE(Mesh, Asset);

public:
	MeshDataPtr data;

	bool boundingBoxNeedUpdate = false;
	AxisAlignedBox3<float> boundingBox;

	bool renderDataDirty = false;
	MeshRenderDataPtr renderData;

	typedef std::mutex ModificationMutex;
	ModificationMutex modificationMutex;
	MeshDataWrittablePtr modifyingData = nullptr;

public:
	Mesh();
	virtual ~Mesh();

	virtual void Reflect(Reflector& reflector) override;

	virtual void OnDestroy() override;

	//virtual void SetDirty() override;

	MeshDataPtr GetMeshData() { return data; }

	MeshDataWrittablePtr BeginModify();
	MeshDataWrittablePtr BeginModifyOnEmptyData();
	void EndModify();

	MeshRenderDataPtr GetMeshRenderData();

	//bool HasIndicies() const { return indexBuffer.indexCount > 0; }
	//IndexFormat GetIndexFormat() const { return indexBuffer.indexFormat; }

	void UpdateBoundingBox();
	AxisAlignedBox3<float> GetBoundingBox();

	//
	// geometry
	//
	struct RaycastResult
	{
		float distance;
		float3 triangleBarycentric;

		bool operator < (const RaycastResult& other) const
		{
			return distance < other.distance;
		}
	};

	bool Raycast(const float3& rayDirection, const float3& rayOrigin, float rayLength, std::vector<RaycastResult>& outResults, bool sort = true);

	//
	// creation
	//
	static std::shared_ptr<Mesh> CreateBox(const float3& minPoint, const float3& maxPoint);
	static std::shared_ptr<Mesh> CreatePlane(const float3& origin, float lengthX, float lengthZ);
	static std::shared_ptr<Mesh> CreateQuad(const float2& minPoint, const float2& maxPoint, float z);
	static std::shared_ptr<Mesh> CreateFullScreenQuad();
	static std::shared_ptr<Mesh> CreateSphere(const float3& center, float radius, uint segment_u, uint segment_v);
	static std::shared_ptr<Mesh> CreateCone(const float3& apex_pos, const float3& base_center, float base_radius, uint nr_base_segments, bool cap);

};

typedef std::shared_ptr<Mesh> MeshPtr;
