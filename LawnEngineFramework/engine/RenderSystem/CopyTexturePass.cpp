#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "CopyTexturePass.h"
#include "ColorRenderTarget.h"
#include "RenderCommands.h"

CopyTexturePass::CopyTexturePass()
{
	AssetManager& assetManager = g_lawnEngine.getAssetManager();

	shader = assetManager.Load<Shader>(assetManager.ProjectDir() / "Shaders/CopyTexture.shader");

	mesh = Mesh::CreateFullScreenQuad();
}

CopyTexturePass::~CopyTexturePass()
{
}

void CopyTexturePass::BuildRenderGraph(TexturePtr source, RHITexturePtr target, const RHIViewport& targetViewport, RHIDevice* device)
{
	renderPassCommand.reset(new RenderPassCommand());
	renderPassCommand->name = "CopyTexturePass";
	renderPassCommand->SetColorAttachmentCount(1);
	renderPassCommand->SetColorAttachment(0, FramebufferAttachmentDesc(target, false), RenderTargetAction());
	renderPassCommand->SetViewportCount(1);
	renderPassCommand->SetViewport(0, targetViewport);
	renderPassCommand->CreateRHIResources(device);

	static FixedString techniqueName = "CopyTexture";

	ShaderTechnique* technique = shader->FindTechnique(techniqueName);
	if (!technique)
		return;

	ShaderVariantKeys variantKeys;
	variantKeys.BindCollection(&technique->variantCollection);  //use default variant

	ShaderVariant* variant = technique->FindVariant(variantKeys);
	if (!variant)
		return;

	ShaderPassPtr pass = variant->passes[0];
	if (!pass || !pass->isValid)
		return;

	shaderParamValues.SetResource("sourceTexture", source);
	const ShaderParamValues* shaderParamValuesArray[1] = { &shaderParamValues };

	meshDrawCommand.reset(new MeshDrawCommand(renderPassCommand));
	meshDrawCommand->BuildGraphicsState(mesh->GetMeshRenderData(), pass, shaderParamValuesArray, 1, RHIBufferPtr(), RHIBufferPtr(), RHIBufferPtr());
}

void CopyTexturePass::DestroyRenderGraph()
{
}

void CopyTexturePass::Render(RHICommandListPtr commandlist)
{
	if (meshDrawCommand)
	{
		meshDrawCommand->Execute(commandlist);
	}
}
