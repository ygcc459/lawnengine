#pragma once

#include "RHI/RHICommon.h"
#include "RHI/RHIResources.h"
#include "RHI/RHIDevice.h"
#include "DepthStencilRenderTarget.h"
#include "Shader.h"
#include <vector>
#include "Mesh.h"
#include "Material.h"

class RHIDevice;

class Sampler;
typedef std::shared_ptr<Sampler> SamplerPtr;

class Texture;
typedef std::shared_ptr<Texture> TexturePtr;

class Shader;
typedef std::shared_ptr<Shader> ShaderPtr;

class ShaderPass;
struct ShaderVariable;
class RenderResourcesManager;

class RenderPassCommand;
typedef RefCountedPtr<RenderPassCommand> RenderPassCommandPtr;

class MeshDrawCommand;
typedef RefCountedPtr<MeshDrawCommand> MeshDrawCommandPtr;

class RenderResourcesManager
{
public:
	RHIDevice* rhiDevice;
};

struct RenderTargetAction
{
	bool clearColor;
	float4 colorValue;

	RenderTargetAction()
		: clearColor(false), colorValue(0, 0, 0, 0)
	{}

	RenderTargetAction& ClearColor(const float4& colorValue)
	{
		this->clearColor = true;
		this->colorValue = colorValue;
		return *this;
	}
};

struct DepthStencilTargetAction
{
	bool clearDepthStencil;

	float depthValue;
	uint8 stencilValue;

	DepthStencilTargetAction()
		: clearDepthStencil(false), depthValue(1.0f), stencilValue(0x00)
	{}

	DepthStencilTargetAction& ClearDepth(float depthValue)
	{
		this->clearDepthStencil = true;
		this->depthValue = depthValue;
		return *this;
	}

	DepthStencilTargetAction& ClearStencil(uint8 stencilValue)
	{
		this->clearDepthStencil = true;
		this->stencilValue = stencilValue;
		return *this;
	}
};

class RenderPassCommand : public RefCountedObject
{
public:
	std::string name;

	FramebufferDesc framebufferDesc;
	RHIFramebufferPtr rhiFramebuffer;
	bool framebufferDirty = false;

	RenderTargetAction colorAttachmentActions[RHI::MaxBoundRenderTargets];
	DepthStencilTargetAction depthAttachmentAction;

	RHIViewportSetPtr rhiViewport;

public:
	RenderPassCommand();
	~RenderPassCommand();

	void SetColorAttachmentCount(int count);
	void SetColorAttachment(int index, const FramebufferAttachmentDesc& desc, const RenderTargetAction& action);

	void SetDepthStencilAttachment(const FramebufferAttachmentDesc& desc, const DepthStencilTargetAction& action);

	void SetViewportCount(int count);
	void SetViewport(int index, const RHIViewport& viewport);

	bool CreateRHIResources(RHIDevice* rhiDevice);
	bool UpdateRHIResources(RHICommandList* rhiCommandList);
	void DestroyRHIResources();

	void PassBegin(RHICommandListPtr commandList);
	void PassEnd(RHICommandListPtr commandList);
};

class MeshDrawCommand : public RefCountedObject
{
public:
	RenderPassCommandPtr renderPassCommand;

	MeshRenderDataPtr meshRenderData;

	GraphicsState graphicsState;

	DrawArguments drawArguments;

	ShaderPassPtr shaderPassPtr;

	std::vector<TexturePtr> referencedTextures;
	std::vector<SamplerPtr> referencedSamplers;

	struct CombinedConstantBuffer
	{
		bool dirty = false;
		std::vector<uint8> cpuData;
		RHIBufferPtr rhiBuffer;
		FixedString name;
	};

	std::vector<CombinedConstantBuffer> constantBuffers;

public:
	MeshDrawCommand(RenderPassCommandPtr renderPassCommand)
		: renderPassCommand(renderPassCommand)
	{}

	~MeshDrawCommand() {}

	void BuildGraphicsState(
		MeshRenderDataPtr meshRenderData, 
		ShaderPassPtr shaderPass, 
		const ShaderParamValues** shaderParamValues, 
		uint32 shaderParamValuesCount,
		RHIBufferPtr objectParamsRHIBuffer,
		RHIBufferPtr materialParamsRHIBuffer,
		RHIBufferPtr passParamsRHIBuffer);

	void Execute(RHICommandListPtr cmdlist);

	void UpdateReferencedResources(RHICommandListPtr cmdlist);

	template<typename T>
	void SetShaderVariable(FixedString variableName, const T& data)
	{
		ShaderVariable* variable = shaderPassPtr->FindVariable(variableName);
		if (variable)
		{
			if (sizeof(T) <= variable->byteSize)
			{
				uint32 constantBufferIndex = variable->cbuffer->index;

				CombinedConstantBuffer& constantBuffer = constantBuffers[constantBufferIndex];

				constantBuffer.dirty = true;

				T* dst = reinterpret_cast<T*>(constantBuffer.cpuData.data() + variable->byteOffset);
				(*dst) = data;
			}
		}
	}

private:
	void BuildConstantBuffers(RHIDevice* rhiDevice, ShaderPass& shaderPass, const ShaderParamValues** shaderParamValues, uint32 shaderParamValuesCount);

	void CalcVertexComponents(MeshRenderData* meshRenderData, ShaderPass& shaderPass,
		MeshVertexComponent* outVertexComponents, uint32& outCount,
		bool& outNeedDummyBuffer);

	void SetVertexBufferBindings(MeshRenderData* meshRenderData, bool needDummyVertexBuffer, RHIDevice* rhiDevice);
	void SetIndexBufferBinding(MeshRenderData* meshRenderData);

};

