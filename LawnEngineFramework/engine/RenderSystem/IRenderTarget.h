#pragma once

#include "RHI/RHIResources.h"

class IRenderTarget
{
public:
	IRenderTarget()
	{}

	virtual ~IRenderTarget()
	{}

	virtual RHITexturePtr GetRHIRenderTarget()
	{
		return nullptr;
	}
};

typedef std::shared_ptr<IRenderTarget> IRenderTargetPtr;
