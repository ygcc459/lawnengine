#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "Sampler.h"

std::shared_ptr<Sampler> Sampler::PointClampSampler;
std::shared_ptr<Sampler> Sampler::LinearClampSampler;
std::shared_ptr<Sampler> Sampler::PointRepeatSampler;
std::shared_ptr<Sampler> Sampler::LinearRepeatSampler;

void Sampler::CreateDefaultSamplers()
{
	PointClampSampler.reset(new Sampler(SF_Point, SA_Clamp));
	LinearClampSampler.reset(new Sampler(SF_Bilinear, SA_Clamp));
	PointRepeatSampler.reset(new Sampler(SF_Point, SA_Repeat));
	LinearRepeatSampler.reset(new Sampler(SF_Bilinear, SA_Repeat));
}

void Sampler::DestroyDefaultSamplers()
{
	PointClampSampler = nullptr;
	LinearClampSampler = nullptr;
	PointRepeatSampler = nullptr;
	LinearRepeatSampler = nullptr;
}

void Sampler::Destroy()
{
	DestroyRHIResources();
}

bool Sampler::UpdateRHIResources(RHICommandList* rhiCommandList)
{
	if (rhiSampler == nullptr)
	{
		return CreateRHIResources(rhiCommandList->GetDevice());
	}
	return true;
}

bool Sampler::CreateRHIResources(RHIDevice* device)
{
	DestroyRHIResources();

	SamplerDesc desc;
	desc.filter = this->filter;
	desc.addressing = this->addressing;

	rhiSampler = device->CreateSampler(desc);

	return true;
}

void Sampler::DestroyRHIResources()
{
	rhiSampler.reset();
}

RHISamplerPtr Sampler::GetOrCreateRHISamplerState(RHIDevice* rhiDevice)
{
	if (!rhiSampler)
		CreateRHIResources(rhiDevice);
	return rhiSampler;
}

