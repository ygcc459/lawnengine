#pragma once

#include "Sprite.h"

class UIMeshBatch;

typedef struct FT_LibraryRec_ *FT_Library;
typedef struct FT_FaceRec_*  FT_Face;

enum TextAlignmentHorizontal
{
	TextAlignmentHorizontal_Left = 0,
	TextAlignmentHorizontal_Center = 1,
	TextAlignmentHorizontal_Right = 2,
};

DECL_ENUM_TYPE(TextAlignmentHorizontal);

enum TextAlignmentVertical
{
	TextAlignmentVertical_Top = 0,
	TextAlignmentVertical_Center = 1,
	TextAlignmentVertical_Bottom = 2,
};

DECL_ENUM_TYPE(TextAlignmentVertical);

struct TextRenderParams
{
	uint16 fontSize;
	float2 maxSize;  //0=not limited
	TextAlignmentHorizontal alignX;
	TextAlignmentVertical alignY;
	float4 color;

	TextRenderParams()
		:fontSize(24), maxSize(0, 0), alignX(TextAlignmentHorizontal_Left), alignY(TextAlignmentVertical_Top), color(0, 0, 0, 1)
	{}
};

class TextRenderer
{
public:
	TextRenderer();
	~TextRenderer();

	void Render(UIMeshBatch* batch, const rectf& rect, const std::string& str, const TextRenderParams& params);

	void Measure(std::vector<rectf>& outCharRects, const rectf& rect, const std::string& str, const TextRenderParams& params);

	Texture2DPtr GetTexture();

	static TextRenderer* GetInstance() { return instance; }

	static void CreateInstance();
	static void DestroyInstance();

private:
	static TextRenderer* instance;

	FT_Library library;
	FT_Face face;

	struct CharacterCacheKey
	{
		union
		{
			struct
			{
				uint32 charcode;
				uint16 size;
				uint16 font;
			};

			uint64 packedValue;
		};

		CharacterCacheKey() {}

		CharacterCacheKey(uint32 charcode, uint16 size, uint16 font)
			: charcode(charcode), size(size), font(font)
		{}

		bool operator < (const CharacterCacheKey& other) const
		{
			return this->packedValue < other.packedValue;
		}

		bool operator == (const CharacterCacheKey& other) const
		{
			return this->packedValue == other.packedValue;
		}
	};

	struct CharacterCacheData
	{
		recti rect;

		CharacterCacheData() {}

		CharacterCacheData(const recti& rect)
			: rect(rect)
		{}
	};

	std::map<CharacterCacheKey, CharacterCacheData> characterCacheDatas;

	std::unique_ptr<DynamicTextureAtlas> atlas;
};
