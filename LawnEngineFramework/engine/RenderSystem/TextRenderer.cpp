#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "TextRenderer.h"
#include "UISystem/UICanvas.h"

#include <ft2build.h>
#include <freetype/freetype.h>

//////////////////////////////////////////////////////////////////////////

struct TextAlignmentHorizontalEnumType : public EnumObjectType<TextAlignmentHorizontal>
{
	TextAlignmentHorizontalEnumType() : EnumObjectType<TextAlignmentHorizontal>("TextAlignmentHorizontal")
	{
		Add("Left", TextAlignmentHorizontal_Left);
		Add("Center", TextAlignmentHorizontal_Center);
		Add("Right", TextAlignmentHorizontal_Right);
	}
};
IMPL_ENUM_TYPE(TextAlignmentHorizontal, TextAlignmentHorizontalEnumType)

struct TextAlignmentVerticalEnumType : public EnumObjectType<TextAlignmentVertical>
{
	TextAlignmentVerticalEnumType() : EnumObjectType<TextAlignmentVertical>("TextAlignmentVertical")
	{
		Add("Top", TextAlignmentVertical_Top);
		Add("Center", TextAlignmentVertical_Center);
		Add("Bottom", TextAlignmentVertical_Bottom);
	}
};
IMPL_ENUM_TYPE(TextAlignmentVertical, TextAlignmentVerticalEnumType)

//////////////////////////////////////////////////////////////////////////

TextRenderer* TextRenderer::instance = nullptr;

TextRenderer::TextRenderer()
	: library(nullptr), face(nullptr)
{
	atlas.reset(new DynamicTextureAtlas(4096, 4096, PF_R8, 1));

	const char* fontPath = "E:/programming/projects/lawnengine/TestProject/Consolas.ttf";

	FT_Error error;
	
	error = FT_Init_FreeType(&library);
	if (error)
	{
		Log::Error("FreeType error: %d, %s", error, FT_Error_String(error));
		return;
	}

	FT_Long face_index = 0;

	error = FT_New_Face(library, fontPath, face_index, &face);/* create face object */
	if (error)
	{
		Log::Error("FreeType error: %d, %s", error, FT_Error_String(error));
		return;
	}
}

TextRenderer::~TextRenderer()
{
	FT_Done_Face(face);
	FT_Done_FreeType(library);
}

void TextRenderer::CreateInstance()
{
	CHECK(instance == nullptr);
	instance = new TextRenderer();
}

void TextRenderer::DestroyInstance()
{
	CHECK(instance != nullptr);
	delete instance;
	instance = nullptr;
}

void TextRenderer::Render(UIMeshBatch* batch, const rectf& rect, const std::string& str, const TextRenderParams& params)
{
	uint16 fontSize = params.fontSize;

	FT_Error error;

	//float sizeInPoints = fontSize;
	//int dpi = 300;
	//error = FT_Set_Char_Size(face, 0, (int)(sizeInPoints * 64), dpi, dpi);

	error = FT_Set_Pixel_Sizes(face, fontSize, fontSize);
	if (error)
	{
		Log::Error("FreeType error: %d, %s", error, FT_Error_String(error));
		return;
	}

	/* set transformation */
	FT_Set_Transform(face, NULL, NULL);

	float2 advancingOrigin = float2(0, 0);
	edge4f bounds = edge4f(std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), -std::numeric_limits<float>::max(), -std::numeric_limits<float>::max());

	//
	// measure pass
	//
	for (int n = 0; n < str.size(); n++)
	{
		char cc = str[n];

		// You should use the UTF-32 representation form of Unicode; for example, if you want to load character U+1F028, use value 0x1F028 as the value for charcode.
		// return 0 if failed
		FT_UInt glyph_index = FT_Get_Char_Index(face, cc);

		error = FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT);

		error = FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);

		rectf charRect = rectf(
			advancingOrigin.x + face->glyph->bitmap_left,
			advancingOrigin.y - face->glyph->bitmap_top,
			face->glyph->bitmap.width,
			face->glyph->bitmap.rows);

		bounds.left = std::min(bounds.left, charRect.xmin());
		bounds.top = std::min(bounds.top, charRect.ymin());
		bounds.right = std::max(bounds.right, charRect.xmax());
		bounds.bottom = std::max(bounds.bottom, charRect.ymax());

		/* increment pen position */
		advancingOrigin.x += face->glyph->advance.x / 64;  //advance is in 1/64th pixels
		advancingOrigin.y += face->glyph->advance.y / 64;  //advance is in 1/64th pixels
	}

	float scale = 1.0f;

	if (params.maxSize.y > 0.0001f)
	{
		float scaleByMaxHeight = params.maxSize.y / bounds.height();
		scale = std::min(scale, scaleByMaxHeight);
	}
	if (params.maxSize.x > 0.0001f)
	{
		float scaleByMaxWidth = params.maxSize.x / bounds.width();
		scale = std::min(scale, scaleByMaxWidth);
	}

	float2 strSize = float2(bounds.width(), bounds.height()) * scale;

	//
	// do alignment
	//
	float2 topleftCorner;  //post-scale space
	if (params.alignX == TextAlignmentHorizontal_Left)
		topleftCorner.x = rect.x;
	else if (params.alignX == TextAlignmentHorizontal_Center)
		topleftCorner.x = rect.x + rect.w * 0.5f - strSize.x * 0.5f;
	else if (params.alignX == TextAlignmentHorizontal_Right)
		topleftCorner.x = rect.right() - strSize.x;

	if (params.alignY == TextAlignmentVertical_Top)
		topleftCorner.y = rect.y;
	else if (params.alignY == TextAlignmentVertical_Center)
		topleftCorner.y = rect.y + rect.h * 0.5f - strSize.y * 0.5f;
	else if (params.alignY == TextAlignmentVertical_Bottom)
		topleftCorner.y = rect.bottom() - strSize.y;

	// text baseline
	advancingOrigin.x = 0;
	advancingOrigin.y = (face->ascender / (float)face->units_per_EM) * fontSize;

	//
	// render pass
	//
	for (int n = 0; n < str.size(); n++)
	{
		char cc = str[n];

		uint32 charcode = cc;

		// You should use the UTF-32 representation form of Unicode; for example, if you want to load character U+1F028, use value 0x1F028 as the value for charcode.
		// return 0 if failed
		FT_UInt glyph_index = FT_Get_Char_Index(face, charcode);

		error = FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT);

		error = FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);

		CharacterCacheKey key = CharacterCacheKey(charcode, fontSize, 0);

		recti charImageRect;

		auto it = characterCacheDatas.find(key);
		if (it == characterCacheDatas.end())
		{
			Image2D charImage = Image2D(face->glyph->bitmap.buffer, face->glyph->bitmap.width, face->glyph->bitmap.rows, face->glyph->bitmap.pitch, PF_R8, false);

			if (!atlas->Alloc(charImage, charImageRect))
			{
				Log::Error("atlas alloc failed");
				return;
			}

			CharacterCacheData charData = CharacterCacheData(charImageRect);
			characterCacheDatas.insert(std::make_pair(key, charData));
		}
		else
		{
			CharacterCacheData& charData = it->second;
			charImageRect = charData.rect;
		}

		rectf dstQuadRect = rectf(
			advancingOrigin.x + face->glyph->bitmap_left,
			advancingOrigin.y - face->glyph->bitmap_top,
			face->glyph->bitmap.width,
			face->glyph->bitmap.rows);

		rectf postScaleRect = rectf(
			dstQuadRect.x * scale + topleftCorner.x,
			dstQuadRect.y * scale + topleftCorner.y,
			dstQuadRect.w * scale,
			dstQuadRect.h * scale
		);

		batch->AddQuad(postScaleRect, rectf(charImageRect.x, charImageRect.y, charImageRect.w, charImageRect.h), params.color);

		/* increment pen position */
		advancingOrigin.x += face->glyph->advance.x / 64;  //advance is in 1/64th pixels
		advancingOrigin.y += face->glyph->advance.y / 64;  //advance is in 1/64th pixels
	}
}

void TextRenderer::Measure(std::vector<rectf>& outCharRects, const rectf& rect, const std::string& str, const TextRenderParams& params)
{
	uint16 fontSize = params.fontSize;

	FT_Error error;

	//float sizeInPoints = fontSize;
	//int dpi = 300;
	//error = FT_Set_Char_Size(face, 0, (int)(sizeInPoints * 64), dpi, dpi);

	error = FT_Set_Pixel_Sizes(face, fontSize, fontSize);
	if (error)
	{
		Log::Error("FreeType error: %d, %s", error, FT_Error_String(error));
		return;
	}

	/* set transformation */
	FT_Set_Transform(face, NULL, NULL);

	float2 advancingOrigin = float2(0, 0);
	edge4f bounds = edge4f(std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), -std::numeric_limits<float>::max(), -std::numeric_limits<float>::max());

	//
	// measure pass
	//
	for (int n = 0; n < str.size(); n++)
	{
		char cc = str[n];

		// You should use the UTF-32 representation form of Unicode; for example, if you want to load character U+1F028, use value 0x1F028 as the value for charcode.
		// return 0 if failed
		FT_UInt glyph_index = FT_Get_Char_Index(face, cc);

		error = FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT);

		error = FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);

		rectf charRect = rectf(
			advancingOrigin.x + face->glyph->bitmap_left,
			advancingOrigin.y - face->glyph->bitmap_top,
			face->glyph->bitmap.width,
			face->glyph->bitmap.rows);

		bounds.left = std::min(bounds.left, charRect.xmin());
		bounds.top = std::min(bounds.top, charRect.ymin());
		bounds.right = std::max(bounds.right, charRect.xmax());
		bounds.bottom = std::max(bounds.bottom, charRect.ymax());

		/* increment pen position */
		advancingOrigin.x += face->glyph->advance.x / 64;  //advance is in 1/64th pixels
		advancingOrigin.y += face->glyph->advance.y / 64;  //advance is in 1/64th pixels
	}

	float scale = 1.0f;

	if (params.maxSize.y > 0.0001f)
	{
		float scaleByMaxHeight = params.maxSize.y / bounds.height();
		scale = std::min(scale, scaleByMaxHeight);
	}
	if (params.maxSize.x > 0.0001f)
	{
		float scaleByMaxWidth = params.maxSize.x / bounds.width();
		scale = std::min(scale, scaleByMaxWidth);
	}

	float2 strSize = float2(bounds.width(), bounds.height());

	//
	// do alignment
	//
	float2 topleftCorner;
	if (params.alignX == TextAlignmentHorizontal_Left)
		topleftCorner.x = rect.x;
	else if (params.alignX == TextAlignmentHorizontal_Center)
		topleftCorner.x = rect.x + rect.w * 0.5f - strSize.x * 0.5f;
	else if (params.alignX == TextAlignmentHorizontal_Right)
		topleftCorner.x = rect.right() - strSize.x;

	if (params.alignY == TextAlignmentVertical_Top)
		topleftCorner.y = rect.y;
	else if (params.alignY == TextAlignmentVertical_Center)
		topleftCorner.y = rect.y + rect.h * 0.5f - strSize.y * 0.5f;
	else if (params.alignY == TextAlignmentVertical_Bottom)
		topleftCorner.y = rect.bottom() - strSize.y;

	// text baseline
	advancingOrigin.x = 0;
	advancingOrigin.y = (face->ascender / (float)face->units_per_EM) * fontSize;

	//
	// render pass
	//
	for (int n = 0; n < str.size(); n++)
	{
		char cc = str[n];

		uint32 charcode = cc;

		// You should use the UTF-32 representation form of Unicode; for example, if you want to load character U+1F028, use value 0x1F028 as the value for charcode.
		// return 0 if failed
		FT_UInt glyph_index = FT_Get_Char_Index(face, charcode);

		error = FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT);

		error = FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);

		rectf dstQuadRect = rectf(
			advancingOrigin.x + face->glyph->bitmap_left,
			advancingOrigin.y - face->glyph->bitmap_top,
			face->glyph->bitmap.width,
			face->glyph->bitmap.rows);

		rectf postScaleRect = rectf(
			dstQuadRect.x * scale + topleftCorner.x,
			dstQuadRect.y * scale + topleftCorner.y,
			dstQuadRect.w * scale,
			dstQuadRect.h * scale
		);

		// fix zero width for prev character
		if (outCharRects.size() != 0 && outCharRects.back().w == 0)
		{
			outCharRects.back().w = postScaleRect.xmin() - outCharRects.back().xmin();
		}

		outCharRects.push_back(postScaleRect);

		/* increment pen position */
		advancingOrigin.x += face->glyph->advance.x / 64;  //advance is in 1/64th pixels
		advancingOrigin.y += face->glyph->advance.y / 64;  //advance is in 1/64th pixels
	}
}

Texture2DPtr TextRenderer::GetTexture()
{
	return atlas->GetTexture();
}
