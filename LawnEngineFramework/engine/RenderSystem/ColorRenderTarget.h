#pragma once

#include "IRenderTarget.h"
#include "RenderSystemCommon.h"
#include "RHI/RHIResources.h"

class ColorRenderTarget : public Texture, public IRenderTarget
{
	DECL_CLASS_TYPE(ColorRenderTarget, Texture);

public:
	int width;
	int height;
	PixelFormat format;
	bool sRGB;

	RHITexturePtr rhiTexture;

public:
	ColorRenderTarget();
	virtual ~ColorRenderTarget();

	virtual void Reflect(Reflector& reflector) override;

	void Create(int width, int height, PixelFormat format, bool sRGB = false);

	void FillTextureDesc(TextureDesc& desc);
	virtual bool CreateRHIResources(RHIDevice* device) override;
	virtual bool UpdateRHIResources(RHICommandList* rhiCommandList) override;
	virtual void DestroyRHIResources() override;

	virtual RHITexturePtr GetRHITexture() override;

	virtual RHITexturePtr GetRHIRenderTarget() override;
};

typedef std::shared_ptr<ColorRenderTarget> ColorRenderTargetPtr;
