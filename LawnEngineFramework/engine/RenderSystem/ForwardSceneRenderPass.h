#pragma once

#include "SceneRenderPass.h"

class RenderPipeline;

struct ForwardSceneRenderPassCBuffer
{
	float4x4 viewMatrix;
	float4x4 projMatrix;
	float4x4 viewProjMatrix;

	float3 cameraPos;

	float4 cameraParams;

	float3 mainLightDir;
	float3 mainLightColor;

	float3 diffuseIndirectColor;

	PackedSH3RGB diffuseIndirectSH;
};

class ForwardSceneRenderPass : public SceneRenderPass
{
public:
	ForwardSceneRenderPass(RenderPipeline* pipeline, RenderScene* scene);
	virtual ~ForwardSceneRenderPass();

	virtual void Prepare(RenderSceneCamera* camera, RHICommandListPtr rhiCommandList) override;

	virtual void GetOrCreatePassParamsRHIBuffer(ShaderPassPtr shaderPass, RHIDevice* rhiDevice, RHIBufferPtr& outPassParamsRHICBuffer) override;

private:
	void PreparePassCBuffer(RenderSceneCamera* camera, RHICommandListPtr rhiCommandList);
	void PrepareCommandlists();

private:
	ForwardSceneRenderPassCBuffer cbufferCpuData;
	RHIBufferPtr cbufferGpuData;

	RHICommandListPtr commandlists[(int)SceneObjectRenderQueueType::_Count];
};
