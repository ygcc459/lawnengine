#pragma once

#include "Texture.h"

class TextureCube : public Texture
{
	DECL_CLASS_TYPE(TextureCube, Texture);

public:
	int width;
	int height;
	int depth;
	int numMipmaps;
	PixelFormat pixelFormat;
	bool sRGB;
	
	bool asRenderTarget;
	bool needUpdateRhiResources;
	RHITexturePtr rhiTexture;

public:
	TextureCube();
	virtual ~TextureCube();

	virtual void Reflect(Reflector& reflector) override;
	virtual void AfterDeserialization() override;

	virtual void OnDestroy();

	void MarkDataDirty();

	bool Create(int width, int height, int depth, int numMipmaps, PixelFormat pixelFormat);

	int3 Size() const { return int3(width, height, depth); }

	void FillTextureDesc(TextureDesc& desc);
	virtual bool CreateRHIResources(RHIDevice* device) override;
	virtual bool UpdateRHIResources(RHICommandList* rhiCommandList) override;
	virtual void DestroyRHIResources() override;

	virtual RHITexturePtr GetRHITexture() override;
};

typedef std::shared_ptr<TextureCube> TextureCubePtr;
