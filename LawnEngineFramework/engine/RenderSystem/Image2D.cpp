#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "Image2D.h"

IMPL_PRIMITIVE_TYPE(Image2DMipmaps, Image2DMipmapsType);

Image2DMipmaps::~Image2DMipmaps()
{
	Destroy();
}

void Image2DMipmaps::Destroy()
{
	if (ownsBuffer)
	{
		delete buffer;
	}
	buffer = nullptr;

	width = 0;
	height = 0;
	numMipmaps = 0;

	pixelByteSize = 0;
	totalByteSize = 0;

	mipmapViews.clear();
}

void Image2DMipmaps::Assign(const Image2DMipmaps& src, bool makeACopy)
{
	Destroy();

	this->width = src.width;
	this->height = src.height;
	this->pixelFormat = src.pixelFormat;
	this->pixelByteSize = src.pixelByteSize;
	this->totalByteSize = src.totalByteSize;
	this->ownsBuffer = makeACopy;
	this->numMipmaps = src.numMipmaps;

	if (makeACopy)
	{
		this->buffer = new byte[totalByteSize];
		memcpy(this->buffer, src.buffer, totalByteSize);
	}
	else
	{
		this->buffer = src.buffer;
	}

	BuildMipmapViews();
}


void Image2DMipmaps::Assign(byte* buffer, int width, int height, PixelFormat pixelFormat, bool makeACopy, int maxNumMipmaps /*= -1*/)
{
	Destroy();

	this->width = width;
	this->height = height;
	this->pixelFormat = pixelFormat;
	this->pixelByteSize = GetPixelByteSize(pixelFormat);
	this->ownsBuffer = makeACopy;

	int numFullMipmaps = CalcNumFullMipmaps(width, height);
	this->numMipmaps = (maxNumMipmaps > 0 && maxNumMipmaps < numFullMipmaps) ? maxNumMipmaps : numFullMipmaps;

	this->totalByteSize = CalcTotalByteSize(width, height, numMipmaps, pixelByteSize);

	if (makeACopy)
	{
		this->buffer = new byte[totalByteSize];
		memcpy(this->buffer, buffer, totalByteSize);
	}
	else
	{
		this->buffer = buffer;
	}

	BuildMipmapViews();
}


void Image2DMipmaps::Assign(int width, int height, PixelFormat pixelFormat, int maxNumMipmaps /*= -1*/)
{
	Destroy();

	this->width = width;
	this->height = height;
	this->pixelFormat = pixelFormat;
	this->pixelByteSize = GetPixelByteSize(pixelFormat);
	this->ownsBuffer = true;

	int numFullMipmaps = CalcNumFullMipmaps(width, height);
	this->numMipmaps = (maxNumMipmaps > 0 && maxNumMipmaps < numFullMipmaps) ? maxNumMipmaps : numFullMipmaps;

	this->totalByteSize = CalcTotalByteSize(width, height, numMipmaps, pixelByteSize);

	this->buffer = new byte[totalByteSize];
	memset(this->buffer, 0, totalByteSize);

	BuildMipmapViews();
}

int Image2DMipmaps::CalcNumFullMipmaps(int width, int height)
{
	int widthMipmaps = Max(1, math::floor_uint(log2(width)) + 1);
	int heightMipmaps = Max(1, math::floor_uint(log2(height)) + 1);
	return Max(widthMipmaps, heightMipmaps);
}

int Image2DMipmaps::CalcTotalByteSize(int width, int height, int mipmaps, int pixelByteSize)
{
	size_t totalByteSize = 0;
	for (int mipmapIndex = 0; mipmapIndex < mipmaps; mipmapIndex++)
	{
		int mipmapWidth = Max(1, width >> mipmapIndex);
		int mipmapHeight = Max(1, height >> mipmapIndex);

		totalByteSize += mipmapWidth * mipmapHeight * pixelByteSize;
	}

	return totalByteSize;
}

void Image2DMipmaps::BuildMipmapViews()
{
	mipmapViews.clear();

	int mipmapByteOffset = 0;
	for (int mipmapIndex = 0; mipmapIndex < numMipmaps; mipmapIndex++)
	{
		int mipmapWidth = Max(1, width >> mipmapIndex);
		int mipmapHeight = Max(1, height >> mipmapIndex);

		byte* mipmapData = this->buffer + mipmapByteOffset;

		mipmapViews.emplace_back(mipmapData, mipmapWidth, mipmapHeight, pixelFormat, false);

		mipmapByteOffset += mipmapWidth * mipmapHeight * pixelByteSize;
	}
}
