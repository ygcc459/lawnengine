#pragma once

#include "RenderSystemCommon.h"
#include "RenderCommands.h"
#include "Shader.h"
#include "BitArray.h"

#include <unordered_set>
#include <shared_mutex>

#include "array_view.h"
#include "unordered_handled_array.h"

class MeshDrawCommand;
class RenderResourcesManager;
class RenderScene;

class RenderSceneLight
{
public:
	RenderSceneLight()
		: renderSceneContainerHandle(-1)
	{}

	virtual ~RenderSceneLight()
	{}

	void GetShadowCullingPlanes(float4* outPlanes, uint32& outCount);

public:
	int renderSceneContainerHandle;

	LightType type;

	float3 color;
	float intensity;

	bool generateShadow;

	float3 direction;

	float3 position;

	RadianAngle innerConeAngle;  //ranges [0, PI), should be smaller than outerConeAngle
	RadianAngle outerConeAngle;  //ranges [0, PI)
	float falloff;  //Decrease in illumination between the edge of the inner cone and the outer cone, usually 1.0f
};

class RenderSceneCamera
{
public:
	RenderSceneCamera()
		: renderSceneContainerHandle(-1)
	{}

	virtual ~RenderSceneCamera()
	{}

	void GetCullingPlanes(float4* outPlanes, uint32& outCount);

public:
	int renderSceneContainerHandle;

	float3 position;
	float3 direction;
	float3 up;

	float fov;
	float znear;
	float zfar;

	vcpp::la::rect<uint> viewport;  //in pixels

	float4x4 viewMatrix;
	float4x4 projectionMatrix;
	float4x4 viewProjectionMatrix;
};

//struct RenderSceneObjectCullingInput
//{
//	float3 boundingBoxCenter;
//	float3 boundingBoxExtent;
//};

struct RenderSceneObjectProperties
{
	bool isDynamic = false;
};

struct PrepareDrawCommandsContext
{
	RenderScene* scene;

	uint32 scenePassesCount;
	SceneRenderPass** scenePasses;
	RenderPassCommandPtr* scenePassCommands;
};

struct RenderSceneObjectDrawCommandsData
{
	std::vector<DrawCommandCullingInput> cullingInputs;
	std::vector<DrawCommandSortingKey> sortingKeys;

	std::vector<MeshDrawCommandPtr> drawCommandsPerScenePass;
	std::vector<DrawCommandBatchingKey> batchingKeysPerScenePass;
	
	std::vector<uint32> offsetsPerScenePass;

	uint32 scenePassesCount;

	void Reset(uint32 numScenePasses)
	{
		scenePassesCount = numScenePasses;

		cullingInputs.clear();
		sortingKeys.clear();

		drawCommandsPerScenePass.clear();
		batchingKeysPerScenePass.clear();
		offsetsPerScenePass.resize(numScenePasses + 1, 0);
	}

	uint32 Size() const
	{
		return (uint32)drawCommandsPerScenePass.size();
	}

	void SetRangeBegin(uint32 scenePassIndex, uint32 begin)
	{
		offsetsPerScenePass[scenePassIndex] = begin;
	}

	void SetRangeEnd(uint32 scenePassIndex, uint32 end)
	{
		offsetsPerScenePass[scenePassIndex + 1] = end;
	}

	uint32 GetRangeBegin(uint32 scenePassIndex) const
	{
		return offsetsPerScenePass[scenePassIndex];
	}

	uint32 GetRangeEnd(uint32 scenePassIndex) const
	{
		return offsetsPerScenePass[scenePassIndex + 1];
	}
};

class RenderSceneObject
{
public:
	RenderSceneObject()
		: renderSceneContainerHandle(-1)
	{}

	virtual ~RenderSceneObject()
	{}

	virtual void FillProperties(RenderSceneObjectProperties& prop) = 0;

	virtual void PrepareDrawCommands(PrepareDrawCommandsContext& context) = 0;

	RenderSceneObjectDrawCommandsData& GetDrawCommandsData() { return drawCommandsData; }

public:
	int renderSceneContainerHandle;

private:
	RenderSceneObjectDrawCommandsData drawCommandsData;
};

enum class DiffuseIndirectLightSource
{
	Color,
	SkyboxSH,
	SkyboxTexture,
};
DECL_ENUM_TYPE(DiffuseIndirectLightSource);

class RenderSceneSettings : public BaseObject
{
	DECL_CLASS_TYPE(RenderSceneSettings, BaseObject);

public:
	RenderSceneSettings();
	virtual ~RenderSceneSettings();

	virtual void Reflect(Reflector& reflector) override;

	virtual void AfterDeserialization() override;
	virtual void OnPropertyChangedByReflection(ObjectMember& member) override;

	void GenerateDiffuseIndirectLighting();
	void GenerateSpecularIndirectLighting();

public:
	Event<std::function<void()>> eventPropertyChangedByReflection;

	Texture2DPtr skybox;
	float skyboxIntensity;
	float skyboxRotation;

	DiffuseIndirectLightSource diffuseIndirectLightSource;

	float3 diffuseIndirectColor;
	sh3rgb diffuseIndirectSH;
	Texture2DPtr diffuseIndirectTexture;
	Texture2DPtr specularIndirectTexture;
};

enum CustomPassObjectType
{
	CustomPassObjectType_UI = 0,
	CustomPassObjectType_Gizmo,

	CustomPassObjectType_Count,
};

class RenderSceneCustomPassObject
{
public:
	RenderSceneCustomPassObject()
		: renderSceneContainerHandle(-1), type(type), priority(0)
	{}

	RenderSceneCustomPassObject(CustomPassObjectType type, float priority)
		: renderSceneContainerHandle(-1), type(type), priority(priority)
	{}

	virtual ~RenderSceneCustomPassObject()
	{}

	virtual void PrepareDrawCommands(RenderSceneCamera* camera, std::vector<MeshDrawCommandPtr>& outDrawCommandPtrs) = 0;

public:
	int renderSceneContainerHandle;

	CustomPassObjectType type;

	//objects of same type are sorted by priority
	float priority;
};

typedef uint32 DrawCommandHandle;

struct DrawCommandIndex
{
	uint32 value;

	enum
	{
		ScenePassIndexBits = 6,  //support at most 64 scene passes
		ArrayIndexBits = sizeof(value) * 8 - ScenePassIndexBits,  //support at most 67,108,864 drawcommands
	};

	DrawCommandIndex(uint32 scenePassIndex, uint32 arrayIndex)
	{
		uint32 scenePassIndexMask = (1 << ScenePassIndexBits) - 1;
		uint32 arrayIndexMask = ((1 << ArrayIndexBits) - 1);
		value = ((scenePassIndex & scenePassIndexMask) << ArrayIndexBits) | (arrayIndex & arrayIndexMask);
	}

	uint32 ScenePassIndex() const { return (value >> ArrayIndexBits); }
	uint32 ArrayIndex() const { return (value & ((1 << ArrayIndexBits) - 1)); }
};

struct DrawCommandBatchingKeySceneData
{
	uint32 referenceCounter = 0;
	DrawCommandBatchingKey batchingKey;
};

class BatchesContainer
{
public:
	typedef uint32 BatchID;

	std::vector<DrawCommandBatchingKey> batchingKeys;  //indexed by BatchID
	std::vector<uint32> batchingKeyUseCounts;  //indexed by BatchID, how many DrawCommand uses this batching key

	std::unordered_map<DrawCommandBatchingKey, BatchID> batchingKeyToBatchID;
};

typedef BatchesContainer::BatchID BatchID;

typedef unordered_handled_array<DrawCommandCullingInput> CullingInputContainer;
typedef CullingInputContainer::handle_type CullingInputHandle;

// one world have only one RenderScene
// each RenderScene can be accessed by multiple RenderPiplines
class RenderScene
{
public:
	RenderScene();
	virtual ~RenderScene();

	void Render(RenderSceneCamera* camera, RenderViewPtr renderView);

	void AddObject(RenderSceneObject* p);
	void AddLight(RenderSceneLight* p);
	void AddCamera(RenderSceneCamera* p);
	void AddCustomPassObject(RenderSceneCustomPassObject* p);

	void RemoveObject(RenderSceneObject* p);
	void RemoveLight(RenderSceneLight* p);
	void RemoveCamera(RenderSceneCamera* p);
	void RemoveCustomPassObject(RenderSceneCustomPassObject* p);

	void UpdateRenderSceneSettings(const RenderSceneSettings& newSettings) { settings = newSettings; }

	void GetAndClearPendingPrepareObjects(std::vector<RenderSceneObject*>& outObjects);

	struct PerScenePassData
	{
		std::vector<MeshDrawCommandPtr> drawCommands;
		std::vector<DrawCommandSortingKey> drawCommandSortingKeys;
		std::vector<CullingInputHandle> drawCommandCullingInputHandles;
		std::vector<BatchID> drawCommandBatchIDs;

		BatchesContainer batches;
	};

	void SetScenePassCount(uint32 scenePassCount);

	CullingInputHandle AddCullingInput(const DrawCommandCullingInput& cullingInput);
	void RemoveCullingInput(CullingInputHandle handle);

	DrawCommandHandle AddDrawCommand(uint32 scenePassIndex, MeshDrawCommandPtr drawcommand, CullingInputHandle cullingInputHandle, const DrawCommandBatchingKey& batchingKey);
	void RemoveDrawCommand(DrawCommandHandle handle);

protected:
	void NotifyAddObject(RenderSceneObject* p, int32 atIndex);
	void NotifyAddLight(RenderSceneLight* p, int32 atIndex);
	void NotifyAddCamera(RenderSceneCamera* p, int32 atIndex);
	void NotifyRemoveObject(RenderSceneObject* p, int32 atIndex);
	void NotifyRemoveLight(RenderSceneLight* p, int32 atIndex);
	void NotifyRemoveCamera(RenderSceneCamera* p, int32 atIndex);

	void UpdateObjectDatas(RenderSceneObject* p, uint32 atIndex);

	void SetObjectDynamic(RenderSceneObject* p, bool isDynamic);
	void SetObjectPendingPrepare(RenderSceneObject* p, bool pendingPrepare);

	BatchID AddBatch(uint32 scenePassIndex, const DrawCommandBatchingKey& batchingKey);
	void RemoveBatch(uint32 scenePassIndex, BatchID batchID);

public:
	std::vector<RenderSceneObject*> objects;
	//std::vector<RenderSceneObjectCullingInput> objectCullingInputs;
	//std::vector<RenderSceneObjectSortingKey> objectSortingKeys;
	//std::vector<MaterialRenderDataPtr> objectMaterials;
	//std::vector<MeshRenderDataPtr> objectMeshes;

	std::unordered_set<RenderSceneObject*> dynamicObjects;
	std::unordered_set<RenderSceneObject*> pendingPrepareObjects;

	std::unordered_map<DrawCommandHandle, DrawCommandIndex> drawCommandHandleToIndex;
	std::vector<PerScenePassData> perScenePassDatas;

	CullingInputContainer cullingInputs;

	std::vector<RenderSceneLight*> lights;
	std::vector<RenderSceneCamera*> cameras;
	std::vector<RenderSceneCustomPassObject*> customPassObjects[CustomPassObjectType_Count];

	RenderSceneSettings settings;

	RenderPipeline* renderPipeline;
};
