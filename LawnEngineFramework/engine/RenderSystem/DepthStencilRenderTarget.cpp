#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "DepthStencilRenderTarget.h"

IMPL_CLASS_TYPE(DepthStencilRenderTarget);

DepthStencilRenderTarget::DepthStencilRenderTarget()
	: Texture(TT_Texture2D)
	, width(0)
	, height(0)
	, format(PF_Depth32)
{
}

DepthStencilRenderTarget::~DepthStencilRenderTarget()
{
	DestroyRHIResources();
}

void DepthStencilRenderTarget::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("width", &Self::width);
	reflector.AddMember("height", &Self::height);
	reflector.AddMember("format", &Self::format);
}

void DepthStencilRenderTarget::Create(int width, int height, PixelFormat format)
{
	this->width = width;
	this->height = height;
	this->format = format;
}

void DepthStencilRenderTarget::FillTextureDesc(TextureDesc& desc)
{
	desc.type = TT_Texture2D;
	desc.width = this->width;
	desc.height = this->height;
	desc.format = this->format;
	desc.flags = (TextureFlags)(TF_ShaderResource | TF_DepthStencil);
}

bool DepthStencilRenderTarget::CreateRHIResources(RHIDevice* device)
{
	TextureDesc desc;
	FillTextureDesc(desc);

	rhiTexture = device->CreateTexture(desc);
	return (rhiTexture != nullptr);
}

bool DepthStencilRenderTarget::UpdateRHIResources(RHICommandList* rhiCommandList)
{
	if (!Texture::UpdateRHIResources(rhiCommandList))
		return false;

	if (!rhiTexture)
	{
		return CreateRHIResources(rhiCommandList->GetDevice());
	}
	else
	{
		TextureDesc desc;
		FillTextureDesc(desc);
		return rhiCommandList->UpdateTexture(rhiTexture, desc, nullptr);
	}
}

void DepthStencilRenderTarget::DestroyRHIResources()
{
	rhiTexture.reset();
}

RHITexturePtr DepthStencilRenderTarget::GetRHITexture()
{
	return rhiTexture;
}
