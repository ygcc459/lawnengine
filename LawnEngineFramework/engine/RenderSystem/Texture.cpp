#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "Texture.h"

IMPL_ABSTRACT_CLASS_TYPE(Texture);

bool Texture::CreateRHIResources(RHIDevice* device)
{
	return true;
}

bool Texture::UpdateRHIResources(RHICommandList* rhiCommandList)
{
	if (sampler)
	{
		if (!sampler->UpdateRHIResources(rhiCommandList))
			return false;
	}

	return true;
}

void Texture::DestroyRHIResources()
{
}

RHITexturePtr Texture::GetOrCreateRHITexture(RHIDevice* rhiDevice)
{
	RHITexturePtr rhiTex = GetRHITexture();
	if (!rhiTex)
	{
		CreateRHIResources(rhiDevice);
		rhiTex = GetRHITexture();
	}
	return rhiTex;
}

void Texture::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("type", &Self::type);
}
