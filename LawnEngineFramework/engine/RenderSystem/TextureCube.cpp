#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "TextureCube.h"

TextureCube::TextureCube()
	: Texture(TextureType::TT_TextureCube)
	, width(0)
	, height(0)
	, depth(0)
	, numMipmaps(0)
	, pixelFormat(PixelFormat::PF_R8G8B8A8)
	, sRGB(false)
	, needUpdateRhiResources(false)
	, asRenderTarget(false)
{
}

TextureCube::~TextureCube()
{
	this->rhiTexture.reset();

	this->needUpdateRhiResources = false;

	this->width = 0;
	this->height = 0;
	this->depth = 0;

	BaseObject::OnDestroy();
}

void TextureCube::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("width", &Self::width);
	reflector.AddMember("height", &Self::height);
	reflector.AddMember("depth", &Self::depth);
	reflector.AddMember("pixelFormat", &Self::pixelFormat);
	reflector.AddMember("sRGB", &Self::sRGB);
}

void TextureCube::AfterDeserialization()
{
	MarkDataDirty();
}

void TextureCube::OnDestroy()
{
}

void TextureCube::MarkDataDirty()
{
	this->needUpdateRhiResources = true;
}

bool TextureCube::Create(int width, int height, int depth, int numMipmaps, PixelFormat pixelFormat)
{
	this->OnDestroy();

	this->width = width;
	this->height = height;
	this->depth = depth;
	this->numMipmaps = numMipmaps;
	this->pixelFormat = pixelFormat;

	MarkDataDirty();

	return true;
}

void TextureCube::FillTextureDesc(TextureDesc& desc)
{
	desc.type = TextureType::TT_TextureCube;
	desc.width = this->width;
	desc.height = this->height;
	desc.depth = this->depth;
	desc.mipmaps = this->numMipmaps;
	desc.format = this->pixelFormat;
	desc.flags = TF_ShaderResource;

	if (asRenderTarget)
		desc.flags = (TextureFlags)(desc.flags | TF_RenderTarget);

	if (sRGB)
		desc.flags = (TextureFlags)(desc.flags | TF_SRGB);
}

bool TextureCube::CreateRHIResources(RHIDevice* device)
{
	TextureDesc textureDesc;
	FillTextureDesc(textureDesc);

	rhiTexture = device->CreateTexture(textureDesc, nullptr);
	return (rhiTexture != nullptr);
}

bool TextureCube::UpdateRHIResources(RHICommandList* rhiCommandList)
{
	if (!Texture::UpdateRHIResources(rhiCommandList))
		return false;

	if (needUpdateRhiResources || !rhiTexture)
	{
		needUpdateRhiResources = false;

		if (!rhiTexture)
		{
			return CreateRHIResources(rhiCommandList->GetDevice());
		}
		else
		{
			TextureDesc textureDesc;
			FillTextureDesc(textureDesc);

			return rhiCommandList->UpdateTexture(rhiTexture, textureDesc, nullptr);
		}
	}

	return true;
}

void TextureCube::DestroyRHIResources()
{
	rhiTexture.reset();
}

RHITexturePtr TextureCube::GetRHITexture()
{
	return rhiTexture;
}
