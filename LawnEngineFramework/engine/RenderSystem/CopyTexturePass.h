#pragma once

#include "RenderPipeline.h"
#include "RenderCommands.h"

class CopyTexturePass
{
	ShaderPtr shader;
	MeshPtr mesh;
	ShaderParamValues shaderParamValues;

	RenderPassCommandPtr renderPassCommand;
	MeshDrawCommandPtr meshDrawCommand;

public:
	CopyTexturePass();
	~CopyTexturePass();

	void DestroyRenderGraph();

	void BuildRenderGraph(TexturePtr source, RHITexturePtr target, const RHIViewport& targetViewport, RHIDevice* device);

	void Render(RHICommandListPtr commandlist);

};
