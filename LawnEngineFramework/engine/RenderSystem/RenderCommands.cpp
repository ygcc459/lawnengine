#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderCommands.h"
#include "RHI/RHIDevice.h"
#include "RenderSystem/Shader.h"
#include "RenderSystem/RenderPipeline.h"
#include "RHI/RHIInputAssembler.h"
#include "RenderSystem/Mesh.h"
#include "ShaderParamValues.h"
#include "SceneRenderPass.h"

RenderPassCommand::RenderPassCommand()
{
	rhiViewport.reset(new RHIViewportSet());
}

RenderPassCommand::~RenderPassCommand()
{
}

void RenderPassCommand::SetColorAttachmentCount(int count)
{
	framebufferDesc.numColorAttachments = count;

	framebufferDirty = true;
}
void RenderPassCommand::SetColorAttachment(int index, const FramebufferAttachmentDesc& desc, const RenderTargetAction& action)
{
	framebufferDesc.colorAttachments[index] = desc;
	colorAttachmentActions[index] = action;

	framebufferDirty = true;
}
void RenderPassCommand::SetDepthStencilAttachment(const FramebufferAttachmentDesc& desc, const DepthStencilTargetAction& action)
{
	framebufferDesc.depthAttachment = desc;
	depthAttachmentAction = action;

	framebufferDirty = true;
}

void RenderPassCommand::SetViewportCount(int count)
{
	rhiViewport->numViewports = count;
}
void RenderPassCommand::SetViewport(int index, const RHIViewport& value)
{
	rhiViewport->viewports[index] = value;
}

bool RenderPassCommand::CreateRHIResources(RHIDevice* rhiDevice)
{
	framebufferDirty = false;

	rhiFramebuffer = rhiDevice->CreateFramebuffer(framebufferDesc);

	return (rhiFramebuffer != nullptr);
}

bool RenderPassCommand::UpdateRHIResources(RHICommandList* rhiCommandList)
{
	if (!rhiFramebuffer)
	{
		return CreateRHIResources(rhiCommandList->GetDevice());
	}
	else if (framebufferDirty)
	{
		DestroyRHIResources();
		return CreateRHIResources(rhiCommandList->GetDevice());  //TODO: notify all MeshRenderers to rebuild
	}
	return true;
}

void RenderPassCommand::DestroyRHIResources()
{
	rhiFramebuffer.reset();
}

void RenderPassCommand::PassBegin(RHICommandListPtr commandList)
{
	for (uint32 i = 0; i < framebufferDesc.numColorAttachments; i++)
	{
		const auto& action = colorAttachmentActions[i];

		if (action.clearColor)
		{
			commandList->ClearRenderTarget(framebufferDesc.colorAttachments[i].texture, action.colorValue);
		}
	}

	if (depthAttachmentAction.clearDepthStencil)
	{
		commandList->ClearDepthStencilTarget(framebufferDesc.depthAttachment.texture, depthAttachmentAction.depthValue, depthAttachmentAction.stencilValue);
	}
}

void RenderPassCommand::PassEnd(RHICommandListPtr commandList)
{
}

void MeshDrawCommand::BuildGraphicsState(
	MeshRenderDataPtr meshRenderData,
	ShaderPassPtr shaderPassPtr,
	const ShaderParamValues** shaderParamValues,
	uint32 shaderParamValuesCount,
	RHIBufferPtr objectParamsRHIBuffer,
	RHIBufferPtr materialParamsRHIBuffer,
	RHIBufferPtr passParamsRHIBuffer)
{
	RHIDevice* rhiDevice = RenderSystem::instance().rhiDevice;

	this->meshRenderData = meshRenderData;
	if (!meshRenderData->IsRHIResourcesCreated())
		meshRenderData->CreateRHIResources(rhiDevice);

	this->shaderPassPtr = shaderPassPtr;
	if (!shaderPassPtr->IsRHIResourcesCreated())
		shaderPassPtr->CreateRHIResources(rhiDevice);

	ShaderPass& shaderPass = *shaderPassPtr;

	GraphicsPipelineDesc pipelineDesc;
	pipelineDesc.primitiveTopology = meshRenderData->GetPrimitiveTopology();

	bool needDummyVertexBuffer;
	CalcVertexComponents(meshRenderData.get(), shaderPass, pipelineDesc.vertexComponents, pipelineDesc.numVertexComponents, needDummyVertexBuffer);

	pipelineDesc.shaders[(int)GraphicsShaderType::Vertex] = shaderPass.frequencyDatas[(int)ShaderFrequency::SF_VertexShader].rhiShader;
	pipelineDesc.shaders[(int)GraphicsShaderType::Pixel] = shaderPass.frequencyDatas[(int)ShaderFrequency::SF_PixelShader].rhiShader;

	pipelineDesc.renderStateDesc = shaderPass.renderStateDesc;

	BuildConstantBuffers(rhiDevice, shaderPass, shaderParamValues, shaderParamValuesCount);

	RHIBindingSetPtr rhiBindingSets[RHI::MaxBindingLayouts];

	std::set<TexturePtr> referencedTextureSet;
	std::set<SamplerPtr> referencedSamplerSet;

	for (uint32 iFreq = 0; iFreq < (uint32)ShaderFrequency::SF_Count; ++iFreq)
	{
		ShaderFrequency freq = (ShaderFrequency)iFreq;
		ShaderPass::ShaderFrequencyData& shaderFreqData = shaderPass.frequencyDatas[freq];

		BindingSetDesc bindingSetDesc;
		bindingSetDesc.layout = shaderFreqData.rhiBindingLayout;

		for (uint32 i = 0; i < (uint32)shaderFreqData.bindingItems.size(); ++i)
		{
			ShaderPass::ShaderBindingItem& item = shaderFreqData.bindingItems[i];
			
			uint32 shaderParamValuesIndex = std::min((uint32)item.group, shaderParamValuesCount - 1);
			const ShaderParamValues* groupShaderParamValues = shaderParamValues[shaderParamValuesIndex];

			switch (item.bindingLayout.type)
			{
			case ResourceType::Texture_SRV:
			{
				ShaderResourceSlot* resourceSlot = item.resourceSlot;

				RHITexturePtr rhiTex;

				TexturePtr tex = groupShaderParamValues->GetResource(resourceSlot->name);
				if (tex)
				{
					referencedTextureSet.insert(tex);
					rhiTex = tex->GetOrCreateRHITexture(rhiDevice);
				}

				if (!rhiTex)
				{
					TexturePtr defaultTex = item.resourceSlot->defaultValue;
					assert(defaultTex);

					referencedTextureSet.insert(defaultTex);
					rhiTex = defaultTex->GetOrCreateRHITexture(rhiDevice);
				}

				bindingSetDesc.items.push_back(BindingSetItem(item.bindingLayout.type, item.bindingLayout.slot, rhiTex.StaticCast<RHIResource>()));
				break;
			}
			case ResourceType::ConstantBuffer:
			case ResourceType::VolatileConstantBuffer:
			{
				ShaderCBufferSlot* cbufferSlot = item.cbufferSlot;

				RHIBufferPtr rhiBuffer;
				if (item.group == ShaderParamGroup::ObjectParams && objectParamsRHIBuffer)
				{
					rhiBuffer = objectParamsRHIBuffer;
				}
				else if (item.group == ShaderParamGroup::MaterialParams && materialParamsRHIBuffer)
				{
					rhiBuffer = materialParamsRHIBuffer;
				}
				else if (item.group == ShaderParamGroup::PassParams && passParamsRHIBuffer)
				{
					rhiBuffer = passParamsRHIBuffer;
				}
				else
				{
					rhiBuffer = constantBuffers[cbufferSlot->index].rhiBuffer;
				}

				bindingSetDesc.items.push_back(BindingSetItem(item.bindingLayout.type, item.bindingLayout.slot, rhiBuffer.StaticCast<RHIResource>()));
				break;
			}
			case ResourceType::Sampler:
			{
				ShaderSamplerSlot* samplerSlot = item.samplerSlot;

				SamplerPtr sampler;

				ShaderResourceSlot* resourceSlot = samplerSlot->resourceSlot;
				if (resourceSlot)
				{
					TexturePtr tex = groupShaderParamValues->GetResource(resourceSlot->name);
					if (!tex)
						tex = resourceSlot->defaultValue;
					
					sampler = tex->GetSampler();
				}
				else
				{
					sampler = groupShaderParamValues->GetSampler(samplerSlot->name);
				}

				if (!sampler)
					sampler = Sampler::LinearRepeatSampler;

				referencedSamplerSet.insert(sampler);
				RHISamplerPtr rhiSampler = sampler->GetOrCreateRHISamplerState(rhiDevice);
				bindingSetDesc.items.push_back(BindingSetItem(item.bindingLayout.type, item.bindingLayout.slot, rhiSampler.StaticCast<RHIResource>()));
				break;
			}
			default:
			{
				Log::Error("MeshDrawCommand::BuildGraphicsState unsupported resource type %d", item.bindingLayout.type);
				break;
			}
			}
		}

		RHIBindingSetPtr rhiBindingSet = rhiDevice->CreateBindingSet(bindingSetDesc);

		pipelineDesc.bindingLayouts[pipelineDesc.numBindingLayouts] = shaderFreqData.rhiBindingLayout;

		rhiBindingSets[pipelineDesc.numBindingLayouts] = rhiBindingSet;
		pipelineDesc.numBindingLayouts++;
	}

	RHIGraphicsPipelinePtr pipeline = rhiDevice->CreateGraphicsPipeline(pipelineDesc);
	graphicsState.pipeline = pipeline;

	graphicsState.framebuffer = renderPassCommand->rhiFramebuffer;
	graphicsState.viewport = renderPassCommand->rhiViewport;

	graphicsState.blendConstantColor = shaderPass.blendConstantColor;

	for (uint32 iBindingSet = 0; iBindingSet < pipelineDesc.numBindingLayouts; iBindingSet++)
		graphicsState.bindingSets[iBindingSet] = rhiBindingSets[iBindingSet];
	graphicsState.numBindingSets = pipelineDesc.numBindingLayouts;

	SetVertexBufferBindings(meshRenderData.get(), needDummyVertexBuffer, rhiDevice);

	SetIndexBufferBinding(meshRenderData.get());

	drawArguments.indexed = meshRenderData->HasIndicies();
	drawArguments.vertexCount = meshRenderData->HasIndicies() ? meshRenderData->GetIndexCount() : meshRenderData->GetVertexCount();

	referencedTextures.assign(referencedTextureSet.begin(), referencedTextureSet.end());
	referencedSamplers.assign(referencedSamplerSet.begin(), referencedSamplerSet.end());
}

void MeshDrawCommand::UpdateReferencedResources(RHICommandListPtr cmdlist)
{
	for (CombinedConstantBuffer& constantBuffer : constantBuffers)
	{
		if (constantBuffer.dirty)
		{
			constantBuffer.dirty = false;

			cmdlist->UpdateBufferData(constantBuffer.rhiBuffer, constantBuffer.cpuData.data(), constantBuffer.cpuData.size());
		}
	}

	for (size_t i = 0; i < referencedTextures.size(); i++)
	{
		TexturePtr tex = referencedTextures[i];
		tex->UpdateRHIResources(cmdlist.get());
	}

	for (size_t i = 0; i < referencedSamplers.size(); i++)
	{
		SamplerPtr sampler = referencedSamplers[i];
		sampler->UpdateRHIResources(cmdlist.get());
	}
}

void MeshDrawCommand::Execute(RHICommandListPtr cmdlist)
{
	UpdateReferencedResources(cmdlist);

	cmdlist->SetGraphicsState(graphicsState);

	cmdlist->Draw(drawArguments);
}

void MeshDrawCommand::BuildConstantBuffers(RHIDevice* rhiDevice, ShaderPass& shaderPass, const ShaderParamValues** shaderParamValues, uint32 shaderParamValuesCount)
{
	constantBuffers.reserve(shaderPass.cbuffers.size());

	for (uint32 iBuffer = 0; iBuffer < shaderPass.cbuffers.size(); iBuffer++)
	{
		ShaderCBufferSlot* cbufferSlot = shaderPass.cbuffers[iBuffer];

		if (cbufferSlot->group == ShaderParamGroup::ObjectParams)
		{
			constantBuffers.push_back(CombinedConstantBuffer());
			CombinedConstantBuffer& combinedConstantBuffer = constantBuffers.back();
			combinedConstantBuffer.name = cbufferSlot->name;
			combinedConstantBuffer.cpuData.resize(cbufferSlot->byteSize);

			for (uint32 iVariable = 0; iVariable < cbufferSlot->variables.size(); iVariable++)
			{
				ShaderVariable* variable = cbufferSlot->variables[iVariable];

				const void* variableValueAddr = shaderParamValues[(int)cbufferSlot->group]->GetVariableValueAddr(variable->name);
				if (!variableValueAddr)
					variableValueAddr = cbufferSlot->defaultCBufferData.data() + variable->byteOffset;

				memcpy(combinedConstantBuffer.cpuData.data() + variable->byteOffset, variableValueAddr, variable->byteSize);
			}

			BufferDesc bufferDesc(cbufferSlot->byteSize, BufferBindFlag::BBF_CONSTANT_BUFFER, BufferUsage::BU_DEFAULT, CpuAccessFlag::CAF_NONE);
			combinedConstantBuffer.rhiBuffer = rhiDevice->CreateBuffer(bufferDesc, combinedConstantBuffer.cpuData.data());

			combinedConstantBuffer.dirty = false;
		}
	}
}

void MeshDrawCommand::CalcVertexComponents(
	MeshRenderData* meshRenderData, ShaderPass& shaderPass,
	MeshVertexComponent* outVertexComponents, uint32& outCount, bool& outNeedDummyBuffer)
{
	const std::vector<ShaderRequiredVertexComponentDesc>& requiredVertexComponents = shaderPass.GetRequiredVertexComponents();
	const std::vector<MeshVertexComponent>& providedVertexComponents = meshRenderData->GetMeshData()->GetVertexComponents();

	const std::vector<MeshVertexBuffer*>& vertexBuffers = meshRenderData->GetMeshData()->GetVertexBuffers();

	outNeedDummyBuffer = false;

	int numVertexComponents = 0;

	for (int i = 0; i < requiredVertexComponents.size(); i++)
	{
		const ShaderRequiredVertexComponentDesc& requiredComponent = requiredVertexComponents[i];

		bool foundVertexComponent = false;

		for (int j = 0; j < providedVertexComponents.size(); j++)
		{
			const MeshVertexComponent& meshComponent = providedVertexComponents[j];

			if (meshComponent.inputElement == requiredComponent.inputElement
				&& IsCompatibleVertexFormat(meshComponent.format, requiredComponent.format))
			{
				outVertexComponents[numVertexComponents] = meshComponent;
				numVertexComponents++;

				foundVertexComponent = true;
				break;
			}
		}

		if (!foundVertexComponent)
		{
			MeshVertexComponent& dummyComponent = outVertexComponents[numVertexComponents];
			numVertexComponents++;

			dummyComponent.format = requiredComponent.format;
			dummyComponent.inputElement = requiredComponent.inputElement;
			dummyComponent.vertexBufferIndex = vertexBuffers.size();
			dummyComponent.offset = 0;

			outNeedDummyBuffer = true;
		}
	}

	outCount = numVertexComponents;
}

void MeshDrawCommand::SetVertexBufferBindings(MeshRenderData* meshRenderData, bool needDummyVertexBuffer, RHIDevice* rhiDevice)
{
	const std::vector<MeshVertexBuffer*>& vertexBuffers = meshRenderData->GetMeshData()->GetVertexBuffers();
	const std::vector<RHIBufferPtr>& rhiVertexBuffers = meshRenderData->GetRHIVertexBuffers();

	size_t vertexBufferCount = vertexBuffers.size();
	if (needDummyVertexBuffer)
		vertexBufferCount++;

	graphicsState.numVertexBufferBindings = vertexBufferCount;

	size_t currentVertexBufferIndex = 0;
	for (size_t i = 0; i < vertexBuffers.size(); i++)
	{
		VertexBufferBinding& vertexBufferBinding = graphicsState.vertexBufferBindings[i];
		vertexBufferBinding.buffer = rhiVertexBuffers[i];
		vertexBufferBinding.slot = currentVertexBufferIndex;
		vertexBufferBinding.stride = vertexBuffers[i]->stride;
		vertexBufferBinding.offset = 0;

		currentVertexBufferIndex++;
	}

	if (needDummyVertexBuffer)
	{
		MeshRenderData::EnsureDummyVertexBufferSize(sizeof(float4), rhiDevice);

		VertexBufferBinding& vertexBufferBinding = graphicsState.vertexBufferBindings[currentVertexBufferIndex];
		vertexBufferBinding.buffer = MeshRenderData::GetDummyVertexBuffer();
		vertexBufferBinding.slot = currentVertexBufferIndex;
		vertexBufferBinding.stride = 0;
		vertexBufferBinding.offset = 0;

		currentVertexBufferIndex++;
	}

	CHECK(currentVertexBufferIndex == vertexBufferCount);
}

void MeshDrawCommand::SetIndexBufferBinding(MeshRenderData* meshRenderData)
{
	const MeshIndexBuffer& indexBuffer = meshRenderData->GetMeshData()->GetIndexBuffer();
	RHIBufferPtr rhiIndexBuffer = meshRenderData->GetRHIIndexBuffer();

	IndexBufferBinding& indexBufferBinding = graphicsState.indexBufferBinding;
	indexBufferBinding.offset = 0;

	if (meshRenderData->HasIndicies())
	{
		indexBufferBinding.buffer = rhiIndexBuffer;
		indexBufferBinding.format = indexBuffer.indexFormat;
	}
	else
	{
		indexBufferBinding.buffer.reset();
		indexBufferBinding.format = IndexFormat::IF_UINT16;
	}
}
