#pragma once

#include "RenderSystemCommon.h"
#include "RHI/RHIResources.h"

class Sampler
{
public:
	SamplerFilter filter;
	SamplerAddressing addressing;

	RHISamplerPtr rhiSampler;

public:
	Sampler()
		: filter(SF_Bilinear), addressing(SA_Clamp)
	{
	}

	Sampler(SamplerFilter filter, SamplerAddressing addressing)
		: filter(filter), addressing(addressing)
	{
	}

	~Sampler()
	{
		Destroy();
	}

	void Destroy();

	bool CreateRHIResources(RHIDevice* device);
	bool UpdateRHIResources(RHICommandList* rhiCommandList);
	void DestroyRHIResources();

	virtual RHISamplerPtr GetRHISamplerState()
	{
		return rhiSampler;
	}

	RHISamplerPtr GetOrCreateRHISamplerState(RHIDevice* rhiDevice);

public:
	static void CreateDefaultSamplers();
	static void DestroyDefaultSamplers();

	static std::shared_ptr<Sampler> PointClampSampler;
	static std::shared_ptr<Sampler> LinearClampSampler;
	static std::shared_ptr<Sampler> PointRepeatSampler;
	static std::shared_ptr<Sampler> LinearRepeatSampler;
};

typedef std::shared_ptr<Sampler> SamplerPtr;
