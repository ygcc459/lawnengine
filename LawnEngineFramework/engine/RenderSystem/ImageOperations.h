#pragma once

#include "Image2D.h"

namespace ImageOperations
{
	bool Resample(const Image2D& src, Image2D& dst);
	
	bool GenMipmaps(Image2DMipmaps& mipmaps);
	
	sh3rgb CalcDiffuseRadianceSH(const Image2D& src);
	void CalcDiffuseRadianceTexture(const Image2D& src, Image2D& outImage);
	void CalcSpecularReflectionTexture(const Image2D& src, Image2DMipmaps& outMipmaps);
}
