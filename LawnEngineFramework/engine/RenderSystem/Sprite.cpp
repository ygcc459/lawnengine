#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "Sprite.h"

//////////////////////////////////////////////////////////////////////////

IMPL_CLASS_TYPE(Sprite);

Sprite::Sprite()
	: rect(float2(0, 0), float2(0, 0))
	, isNinePatch(false)
	, ninePatchPaddings(0, 0, 0, 0)
{

}

Sprite::~Sprite()
{

}

void Sprite::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("name", &Self::name);
	reflector.AddMember("rect", &Self::rect);
	reflector.AddMember("isNinePatch", &Self::isNinePatch);
	reflector.AddMember("ninePatchPaddings", &Self::ninePatchPaddings);
}

//////////////////////////////////////////////////////////////////////////

IMPL_CLASS_TYPE(SpriteAtlas);

SpriteAtlas::SpriteAtlas()
{
}

SpriteAtlas::~SpriteAtlas()
{
	for (Sprite* s : sprites)
	{
		delete s;
	}
}

Sprite* SpriteAtlas::AddSprite(const std::string& name)
{
	if (nameToIndexMapping.find(name) == nameToIndexMapping.end())
	{
		Sprite* sprite = new Sprite();
		sprite->name = name;

		int index = sprites.size();
		sprites.push_back(sprite);
		nameToIndexMapping.insert(std::make_pair(sprite->name, index));

		return sprite;
	}
	else
	{
		return nullptr;
	}
}

Sprite* SpriteAtlas::FindSprite(const std::string& name)
{
	auto it = nameToIndexMapping.find(name);
	if (it != nameToIndexMapping.end())
		return sprites[it->second];
	else
		return nullptr;
}

void SpriteAtlas::AfterDeserialization()
{
	nameToIndexMapping.clear();

	for (int i = 0; i < sprites.size(); i++)
	{
		Sprite* s = sprites[i];
		if (s)
		{
			nameToIndexMapping.insert(std::make_pair(s->name, i));
		}
	}
}

void SpriteAtlas::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("texture", &Self::texture);
	reflector.AddMember("sprites", &Self::sprites);
}

//////////////////////////////////////////////////////////////////////////

DynamicTextureAtlas::DynamicTextureAtlas(int w, int h, PixelFormat pixelFormat, int Padding)
	: atlasUsedSlots(nullptr), atlasEmptySlots(nullptr), Padding(Padding)
{
	texture.reset(new Texture2D());
	texture->Create(w, h, 1, pixelFormat);
}

DynamicTextureAtlas::~DynamicTextureAtlas()
{
}

bool DynamicTextureAtlas::AllocRectOnAtlas(const int2& RequiredSize, recti& outRect)
{
	if (atlasEmptySlots == nullptr)
	{
		Slot* RootSlot = new Slot(0, 0, texture->width, texture->height);
		RootSlot->LinkHead(atlasEmptySlots);
	}

	Slot* ReturnVal = nullptr;

	// Account for padding on both sides
	const uint32 TotalPadding = Padding * 2;
	const uint32 PaddedWidth = RequiredSize.x + TotalPadding;
	const uint32 PaddedHeight = RequiredSize.y + TotalPadding;

	// Previously, slots were stored as a binary tree - this has been replaced with a linked-list of slots on the edge of the tree
	// (slots on the edge of the tree represent empty slots); this iterates empty slots in same order as a binary depth-first-search,
	// except much faster.
	for (Slot::TIterator SlotIt(atlasEmptySlots); SlotIt; SlotIt++)
	{
		Slot& CurSlot = *SlotIt;

		if (PaddedWidth <= CurSlot.w && PaddedHeight <= CurSlot.h)
		{
			ReturnVal = &CurSlot;
			break;
		}
	}

	if (ReturnVal)
	{
		// The width and height of the new child node
		const uint32 RemainingWidth = std::max<int32>(0, ReturnVal->w - PaddedWidth);
		const uint32 RemainingHeight = std::max<int32>(0, ReturnVal->h - PaddedHeight);

		// New slots must have a minimum width/height, to avoid excessive slots i.e. excessive memory usage and iteration.
		// No glyphs seem to use slots this small, and cutting these slots out improves performance/memory-usage a fair bit
		const uint32 MinSlotDim = 2;

		// Split the remaining area around this slot into two children.
		if (RemainingHeight >= MinSlotDim || RemainingWidth >= MinSlotDim)
		{
			Slot* LeftSlot = NULL;
			Slot* RightSlot = NULL;

			if (RemainingHeight <= RemainingWidth)
			{
				// Split vertically
				// - - - - - - - - -
				// |       |       |
				// |  Slot |       |
				// |       |       |
				// | - - - | Right |
				// |       |       |
				// |  Left |       |
				// |       |       |
				// - - - - - - - - -
				LeftSlot = new Slot(ReturnVal->x, ReturnVal->y + PaddedHeight, PaddedWidth, RemainingHeight);
				RightSlot = new Slot(ReturnVal->x + PaddedWidth, ReturnVal->y, RemainingWidth, ReturnVal->h);
			}
			else
			{
				// Split horizontally
				// - - - - - - - - -
				// |       |       |
				// |  Slot | Left  |
				// |       |       |
				// | - - - - - - - |
				// |               |
				// |     Right     |
				// |               |
				// - - - - - - - - -
				LeftSlot = new Slot(ReturnVal->x + PaddedWidth, ReturnVal->y, RemainingWidth, PaddedHeight);
				RightSlot = new Slot(ReturnVal->x, ReturnVal->y + PaddedHeight, ReturnVal->w, RemainingHeight);
			}

			// Replace the old slot within AtlasEmptySlots, with the new Left and Right slot, then add the old slot to AtlasUsedSlots
			LeftSlot->LinkReplace(ReturnVal);
			RightSlot->LinkAfter(LeftSlot);

			ReturnVal->LinkHead(atlasUsedSlots);
		}
		else
		{
			// Remove the old slot from AtlasEmptySlots, into AtlasUsedSlots
			ReturnVal->Unlink();
			ReturnVal->LinkHead(atlasUsedSlots);
		}

		// Shrink the slot to the remaining area.
		ReturnVal->w = PaddedWidth;
		ReturnVal->h = PaddedHeight;

		outRect = recti(ReturnVal->x, ReturnVal->y, RequiredSize.x, RequiredSize.y);
		return true;
	}
	else
	{
		return false;
	}
}

bool DynamicTextureAtlas::Alloc(const Image2D& image, recti& outRect)
{
	CHECK(image.pixelFormat == texture->pixelFormat);

	recti spriteRect;
	if (AllocRectOnAtlas(int2(image.width, image.height), spriteRect) == false)
	{
		return false;
	}

	Image2D& targetImage = texture->data.GetMipmap(0);

	for (int y = 0; y < spriteRect.h; y++)
	{
		memcpy(targetImage.PixelPointer(spriteRect.x, y + spriteRect.y), image.PixelPointer(0, y), image.pitch);
	}

	texture->MarkDataDirty();

	outRect = spriteRect;

	return true;
}
