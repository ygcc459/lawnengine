#pragma once

#include "RenderSystemCommon.h"
#include "ImagePixel.h"

class Image2D
{
public:
	byte* buffer;
	bool ownsBuffer;

	int width;
	int height;
	PixelFormat pixelFormat;
	int pixelByteSize;
	int pitch;  //byte size for one row

	Image2D()
		:buffer(nullptr), width(0), height(0), pitch(0), pixelFormat(PF_R8G8B8A8), pixelByteSize(0), ownsBuffer(false)
	{
	}

	Image2D(int width, int height, PixelFormat pixelFormat)
		:width(width), height(height), pixelFormat(pixelFormat), pixelByteSize((int)GetPixelByteSize(pixelFormat)), ownsBuffer(true)
	{
		pitch = pixelByteSize * width;

		this->buffer = new byte[TotalByteSize()];
		memset(this->buffer, 0, TotalByteSize());
	}

	Image2D(byte* buffer, int width, int height, PixelFormat pixelFormat, bool makeACopy)
		:width(width), height(height), pixelFormat(pixelFormat), pixelByteSize((int)GetPixelByteSize(pixelFormat)), ownsBuffer(makeACopy)
	{
		pitch = pixelByteSize * width;

		if (makeACopy)
		{
			this->buffer = new byte[TotalByteSize()];
			memcpy(this->buffer, buffer, TotalByteSize());
		}
		else
		{
			this->buffer = buffer;
		}
	}

	Image2D(byte* buffer, int width, int height, int pitch, PixelFormat pixelFormat, bool makeACopy)
		:width(width), height(height), pitch(pitch), pixelFormat(pixelFormat), pixelByteSize((int)GetPixelByteSize(pixelFormat)), ownsBuffer(makeACopy)
	{
		if (makeACopy)
		{
			this->buffer = new byte[TotalByteSize()];
			memcpy(this->buffer, buffer, TotalByteSize());
		}
		else
		{
			this->buffer = buffer;
		}
	}

	Image2D(const Image2D& src_image, bool makeACopy)
		:width(src_image.width), height(src_image.height), pitch(src_image.pitch), pixelFormat(src_image.pixelFormat), pixelByteSize(src_image.pixelByteSize), ownsBuffer(makeACopy)
	{
		if (makeACopy)
		{
			this->buffer = new byte[TotalByteSize()];
			memcpy(this->buffer, src_image.buffer, TotalByteSize());
		}
		else
		{
			this->buffer = src_image.buffer;
		}
	}

	virtual ~Image2D()
	{
		if (ownsBuffer)
		{
			delete buffer;
		}
		buffer = nullptr;
	}

	byte* Data() { return buffer; }

	int TotalByteSize() const { return height * pitch; }

	int PixelByteOffset(int x, int y) const { return y * pitch + x * pixelByteSize; }

	byte* PixelPointer(int x, int y) { return buffer + PixelByteOffset(x, y); }
	const byte* PixelPointer(int x, int y) const { return buffer + PixelByteOffset(x, y); }

	int PixelByteSize() const { return pixelByteSize; }

	int RowByteSize() const { return pitch; }
};

class Image2DMipmaps
{
public:
	byte* buffer;
	int totalByteSize;

	bool ownsBuffer;

	int width;
	int height;
	int numMipmaps;

	PixelFormat pixelFormat;
	int pixelByteSize;

	std::vector<Image2D> mipmapViews;  //image2D views into this->buffer
	
	Image2DMipmaps()
		: buffer(nullptr), totalByteSize(0), width(0), height(0), numMipmaps(0), pixelFormat(PF_R8G8B8A8), pixelByteSize(0), ownsBuffer(false)
	{
	}

	Image2DMipmaps(int width, int height, PixelFormat pixelFormat, int maxNumMipmaps = -1)
		: buffer(nullptr), totalByteSize(0), width(0), height(0), numMipmaps(0), pixelFormat(PF_R8G8B8A8), pixelByteSize(0), ownsBuffer(false)
	{
		Assign(width, height, pixelFormat, maxNumMipmaps);
	}

	Image2DMipmaps(byte* buffer, int width, int height, PixelFormat pixelFormat, bool makeACopy, int maxNumMipmaps = -1)
		: buffer(nullptr), totalByteSize(0), width(0), height(0), numMipmaps(0), pixelFormat(PF_R8G8B8A8), pixelByteSize(0), ownsBuffer(false)
	{
		Assign(buffer, width, height, pixelFormat, makeACopy, maxNumMipmaps);
	}

	Image2DMipmaps(const Image2DMipmaps& src, bool makeACopy)
		: buffer(nullptr), totalByteSize(0), width(0), height(0), numMipmaps(0), pixelFormat(PF_R8G8B8A8), pixelByteSize(0), ownsBuffer(false)
	{
		Assign(src, makeACopy);
	}

	virtual ~Image2DMipmaps();

	void Destroy();

	void Assign(int width, int height, PixelFormat pixelFormat, int maxNumMipmaps = -1);
	void Assign(byte* buffer, int width, int height, PixelFormat pixelFormat, bool makeACopy, int maxNumMipmaps = -1);
	void Assign(const Image2DMipmaps& src, bool makeACopy);

	static int CalcNumFullMipmaps(int width, int height);
	static int CalcTotalByteSize(int width, int height, int mipmaps, int pixelByteSize);

	byte* Data() { return buffer; }
	const byte* Data() const { return buffer; }

	int TotalByteSize() const { return totalByteSize; }

	int PixelByteSize() const { return pixelByteSize; }

	Image2D& GetMipmap(int mipmapIndex)
	{
		return mipmapViews[mipmapIndex];
	}

	const Image2D& GetMipmap(int mipmapIndex) const
	{
		return mipmapViews[mipmapIndex];
	}

public:
	void BuildMipmapViews();
};

class Image2DMipmapsType : public PrimitiveObjectType<Image2DMipmaps>
{
public:
	Image2DMipmapsType()
		: PrimitiveObjectType<Image2DMipmaps>("Image2DMipmaps")
	{}

	virtual bool Serialize(Serializer& serializer, void* p) const override
	{
		Image2DMipmaps& v = *reinterpret_cast<Image2DMipmaps*>(p);

		serializer.SerializeMemberAuto("width", v.width);
		serializer.SerializeMemberAuto("height", v.height);
		serializer.SerializeMemberAuto("numMipmaps", v.numMipmaps);

		serializer.SerializeMemberAuto("pixelFormat", v.pixelFormat);

		serializer.SerializeMemberRawData("data", v.buffer, v.totalByteSize);

		return true;
	}

	virtual bool Deserialize(Deserializer& deserializer, void* p) const override
	{
		Image2DMipmaps& v = *reinterpret_cast<Image2DMipmaps*>(p);

		deserializer.DeserializeMemberAuto("width", v.width);
		deserializer.DeserializeMemberAuto("height", v.height);
		deserializer.DeserializeMemberAuto("numMipmaps", v.numMipmaps);

		deserializer.DeserializeMemberAuto("pixelFormat", v.pixelFormat);

		byte* rawData;
		size_t rawDataSize;
		if (deserializer.DeserializeMemberRawData("data", &rawData, &rawDataSize))
		{
			v.Assign(rawData, v.width, v.height, v.pixelFormat, true, v.numMipmaps);

			CHECK(v.totalByteSize == rawDataSize);
			CHECK(v.totalByteSize == Image2DMipmaps::CalcTotalByteSize(v.width, v.height, v.numMipmaps, v.pixelByteSize));

			return true;
		}
		else
		{
			return false;
		}
	}
};
DECL_PRIMITIVE_TYPE(Image2DMipmaps)

#include "ImageAccess.h"
