#include "../fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "RenderView.h"

#include "EntitySystem/Camera.h"
#include "UISystem/UICanvas.h"

RenderView::RenderView(const RenderViewDesc& desc)
	: desc(desc)
{
	Log::Debug("RenderView() %08X", this);

	CreateRHIResources();
}

RenderView::~RenderView()
{
	Log::Debug("~RenderView() %08X", this);

	DestroyRHIResources();

	for (int i = cameras.size() - 1; i >= 0; i--)
	{
		auto camera = cameras[i];
		camera->SetRenderTarget(nullptr);
	}
	cameras.clear();
}

void RenderView::UpdateRHIResources()
{
	DestroyRHIResources();
	CreateRHIResources();
}

bool RenderView::CreateRHIResources()
{
	RHIDevicePtr rhiDevice = RenderSystem::instance().rhiDevice;

	rhiDisplaySurface = rhiDevice->CreateDisplaySurface((void*)desc.outputWindow, desc.width, desc.height, desc.format);

	return rhiDisplaySurface != nullptr;
}

void RenderView::DestroyRHIResources()
{
	RHIDevicePtr rhiDevice = RenderSystem::instance().rhiDevice;

	if (rhiDisplaySurface)
	{
		rhiDevice->ReleaseDisplaySurface(rhiDisplaySurface);
		rhiDisplaySurface = nullptr;
	}
}

void RenderView::resize(uint newWidth, uint newHeight)
{
	if(newWidth != desc.width || newHeight != desc.height)
	{
		Log::Info("RenderView::resize %d %d", newWidth, newHeight);

		desc.width = newWidth;
		desc.height = newHeight;

		RHIDevicePtr rhiDevice = RenderSystem::instance().rhiDevice;

		// WM_SIZE messages can be sent during
		// destruction or re-creation of the swap chain
		// ensure we have a swap chain to resize
		if(true)
		{
			signal_before_window_resize.emit();

			rhiDevice->UpdateDisplaySurface(rhiDisplaySurface, newWidth, newHeight);

			for (auto camera : cameras)
			{
				camera->OnRenderTargetResized(desc.width, desc.height);
			}

			for (int i = 0; i < canvases.GetSize(); ++i)
			{
				auto canvas = canvases[i];
				canvas->Resize(int2(desc.width, desc.height));
			}

			signal_after_window_resize.emit(newWidth, newHeight);
		}
	}
}

void RenderView::AddCameraInternal(Camera* camera)
{
	cameras.push_back(camera);
}

void RenderView::RemoveCameraInternal(Camera* camera)
{
	for (int i = 0; i < cameras.size(); i++)
	{
		if (cameras[i] == camera)
		{
			cameras.erase(cameras.begin() + i);
		}
	}
}

void RenderView::AddUICanvas(UICanvas* v)
{
	canvases.Register(v, v->renderViewRegistryHandle);

	v->Resize(int2(desc.width, desc.height));
}

void RenderView::RemoveUICanvas(UICanvas* v)
{
	canvases.Unregister(v->renderViewRegistryHandle);
}

bool RenderView::IsUICanvasRegistered(UICanvas* v) const
{
	return canvases.Contains(v->renderViewRegistryHandle);
}

RHITexturePtr RenderView::GetRHIRenderTarget()
{
	return rhiDisplaySurface->RenderTarget();
}

void RenderView::render()
{
	if (!rhiDisplaySurface)
	{
		return;
	}

	for (auto camera : cameras)
	{
		camera->Render();
	}
}
