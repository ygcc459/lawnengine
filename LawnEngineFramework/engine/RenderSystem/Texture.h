#pragma once

#include "../AssetSystem/AssetManager.h"
#include "Sampler.h"
#include "RenderSystemCommon.h"
#include "RHI/RHICommon.h"

class Texture : public Asset
{
	DECL_ABSTRACT_CLASS_TYPE(Texture, Asset);

public:
	TextureType type;

	SamplerPtr sampler;

public:
	Texture(TextureType type)
		:type(type), sampler(Sampler::LinearClampSampler)
	{
	}

	virtual ~Texture()
	{
	}

	virtual void Reflect(Reflector& reflector) override;

	virtual bool CreateRHIResources(RHIDevice* device);
	virtual bool UpdateRHIResources(RHICommandList* rhiCommandList);
	virtual void DestroyRHIResources();

	virtual void SetSampler(SamplerPtr s) { this->sampler = s; }
	virtual SamplerPtr GetSampler() { return this->sampler; }

	virtual RHITexturePtr GetRHITexture() = 0;

	RHITexturePtr GetOrCreateRHITexture(RHIDevice* rhiDevice);
};

typedef std::shared_ptr<Texture> TexturePtr;
