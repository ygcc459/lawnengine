#include "../fast_compile.h"
#include "LawnEngineInternal.h"
#include "RenderSystemInternal.h"
#include "Mesh.h"
#include "RHI/RHIInputAssembler.h"
#include "RenderSystem/RenderCommands.h"

IMPL_PRIMITIVE_TYPE(MeshVertexComponent, MeshVertexComponentType);
IMPL_PRIMITIVE_TYPE(MeshVertexBuffer, MeshVertexBufferType);
IMPL_PRIMITIVE_TYPE(MeshIndexBuffer, MeshIndexBufferType);

////////////////////////////////////////////////////////////////////////////////////////////////////////

IMPL_CLASS_TYPE(MeshData);

void MeshData::Reflect(Reflector& reflector)
{
	reflector.AddMember("primitiveTopology", &Self::primitiveTopology);
	reflector.AddMember("vertexBuffers", &Self::vertexBuffers);
	reflector.AddMember("indexBuffer", &Self::indexBuffer);
	reflector.AddMember("vertexComponents", &Self::vertexComponents);
	reflector.AddMember("vertexCount", &Self::vertexCount);
}

void MeshData::CopyTo(MeshData& target)
{
	target.vertexBuffers.resize(vertexBuffers.size());
	for (size_t i = 0; i < vertexBuffers.size(); i++)
	{
		MeshVertexBuffer* src = vertexBuffers[i];

		MeshVertexBuffer* dst = new MeshVertexBuffer();
		dst->cpuBuffer = src->cpuBuffer;
		dst->size = src->size;
		dst->stride = src->stride;

		target.vertexBuffers[i] = dst;
	}

	target.indexBuffer.cpuBuffer = indexBuffer.cpuBuffer;
	target.indexBuffer.indexCount = indexBuffer.indexCount;
	target.indexBuffer.indexFormat = indexBuffer.indexFormat;

	target.vertexComponents = vertexComponents;

	target.vertexCount = vertexCount;

	target.primitiveTopology = primitiveTopology;
}

int MeshData::CalcVertexBufferComponentsSize(int vertexBufferIndex) const
{
	int size = 0;

	for (int j = 0; j < this->vertexComponents.size(); j++)
	{
		const MeshVertexComponent& meshComponent = this->vertexComponents[j];

		if (meshComponent.vertexBufferIndex == vertexBufferIndex)
		{
			size = Max(size, meshComponent.offset + meshComponent.format.ByteSize());
		}
	}

	return size;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

void MeshDataWrittable::ClearVertexIndexData()
{
	vertexCount = 0;

	for (int i = 0; i < vertexBuffers.size(); i++)
	{
		delete vertexBuffers[i];
	}
	vertexBuffers.clear();

	indexBuffer.Reset();
}

void MeshDataWrittable::ClearAll()
{
	ClearVertexIndexData();

	vertexComponents.clear();

	primitiveTopology = PT_Unknown;
}

void MeshDataWrittable::SetVertexCount(uint32 vertexCount)
{
	vertexCount = vertexCount;
}

void MeshDataWrittable::SetPrimitiveTopology(PrimitiveTopology primitiveTopology)
{
	primitiveTopology = primitiveTopology;
}

int MeshDataWrittable::AddVertexBuffer(int stride, int size, const void* data)
{
	int index = vertexBuffers.size();

	MeshVertexBuffer* vertexBuffer = new MeshVertexBuffer();
	vertexBuffer->stride = stride;
	vertexBuffers.push_back(vertexBuffer);

	UpdateVertexBuffer(index, size, data);

	return index;
}

void MeshDataWrittable::SetVertexBufferStride(int vertexBufferIndex, int stride)
{
	MeshVertexBuffer* vertexBuffer = vertexBuffers[vertexBufferIndex];
	vertexBuffer->stride = stride;
}

void MeshDataWrittable::UpdateVertexBuffer(int vertexBufferIndex, int size, const void* data)
{
	MeshVertexBuffer* vertexBuffer = vertexBuffers[vertexBufferIndex];
	vertexBuffer->size = size;
	vertexBuffer->cpuBuffer.resize(size);
	if (data)
	{
		memcpy(vertexBuffer->cpuBuffer.data(), data, size);
	}
}

int MeshDataWrittable::AddVertexComponent(InputElement inputElement, int vertexBufferIndex, MeshVertexFormat format, int offset)
{
	CHECK(vertexBufferIndex >= 0 && vertexBufferIndex < vertexBuffers.size());

	if (offset < 0)
	{
		offset = CalcVertexBufferComponentsSize(vertexBufferIndex);
	}

	MeshVertexComponent vertexComponent;
	vertexComponent.inputElement = inputElement;
	vertexComponent.vertexBufferIndex = vertexBufferIndex;
	vertexComponent.format = format;
	vertexComponent.offset = offset;

	int componentIndex = vertexComponents.size();

	vertexComponents.push_back(vertexComponent);

	return componentIndex;
}

MeshVertexComponent* MeshDataWrittable::FindVertexComponent(InputElement inputElement)
{
	for (size_t i = 0; i < vertexComponents.size(); i++)
	{
		MeshVertexComponent& component = vertexComponents[i];
		if (component.inputElement == inputElement)
		{
			return &component;
		}
	}

	return nullptr;
}

uint MeshDataWrittable::AddVertices(int count/* = 1*/)
{
	for (size_t i = 0; i < vertexBuffers.size(); i++)
	{
		MeshVertexBuffer* vb = vertexBuffers[i];

		CHECK(vb->cpuBuffer.size() == vb->size);

		uint32 newSize = vb->size + vb->stride * count;

		vb->cpuBuffer.resize(newSize);
		vb->size = newSize;
	}

	uint firstAddedVertexIndex = vertexCount;

	vertexCount += count;

	return firstAddedVertexIndex;
}

void MeshDataWrittable::ClearVertices()
{
	for (size_t i = 0; i < vertexBuffers.size(); i++)
	{
		MeshVertexBuffer* vb = vertexBuffers[i];

		CHECK(vb->cpuBuffer.size() == vb->size);

		uint32 newSize = 0;

		vb->cpuBuffer.clear();
		vb->size = newSize;
	}

	vertexCount = 0;
}

void MeshDataWrittable::AppendIndexBuffer(uint8* data, uint32 byteSize)
{
	uint32 prevSize = indexBuffer.cpuBuffer.size();
	indexBuffer.cpuBuffer.resize(indexBuffer.size() + byteSize);
	memcpy(indexBuffer.cpuBuffer.data() + prevSize, data, byteSize);
}

void MeshDataWrittable::UpdateIndexBuffer(IndexFormat indexFormat, uint indexCount, uint8* data)
{
	indexBuffer.indexFormat = indexFormat;
	indexBuffer.indexCount = indexCount;
	indexBuffer.cpuBuffer.resize(indexBuffer.size());
	if (data)
	{
		memcpy(indexBuffer.cpuBuffer.data(), data, indexBuffer.size());
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

std::atomic<uint32> MeshRenderData::nextMeshRenderDataID(0);

MeshVertexBuffer MeshRenderData::dummyVertexBuffer;
RHIBufferPtr MeshRenderData::rhiDummyVertexBuffer;

MeshRenderData::MeshRenderData()
	: meshRenderDataID(nextMeshRenderDataID++)
{
}

MeshRenderData::~MeshRenderData()
{
	DestroyRHIResources();
}

bool MeshRenderData::CreateRHIResources(RHIDevice* rhiDevice)
{
	DestroyRHIResources();

	const std::vector<MeshVertexBuffer*>& vertexBuffers = data->GetVertexBuffers();
	const MeshIndexBuffer& indexBuffer = data->GetIndexBuffer();

	rhiVertexBuffers.resize(vertexBuffers.size());

	for (int i = 0; i < vertexBuffers.size(); i++)
	{
		MeshVertexBuffer* vertexBuffer = vertexBuffers[i];

		if (rhiVertexBuffers[i] == nullptr)
		{
			CHECK(vertexBuffer->cpuBuffer.size() == vertexBuffer->size);

			BufferDesc bufferDesc = BufferDesc(vertexBuffer->size, BufferBindFlag::BBF_VERTEX_BUFFER);
			rhiVertexBuffers[i] = rhiDevice->CreateBuffer(bufferDesc, vertexBuffer->cpuBuffer.data());
		}
	}

	if (rhiIndexBuffer == nullptr && HasIndicies())
	{
		BufferDesc bufferDesc = BufferDesc(indexBuffer.size(), BufferBindFlag::BBF_INDEX_BUFFER);
		rhiIndexBuffer = rhiDevice->CreateBuffer(bufferDesc, indexBuffer.cpuBuffer.data());
	}

	return true;
}

void MeshRenderData::DestroyRHIResources()
{
	rhiVertexBuffers.clear();
	rhiIndexBuffer.reset();
}

bool MeshRenderData::IsRHIResourcesCreated() const
{
	const std::vector<MeshVertexBuffer*>& vertexBuffers = data->GetVertexBuffers();

	if (rhiVertexBuffers.size() != vertexBuffers.size())
		return false;

	return true;
}

void MeshRenderData::EnsureDummyVertexBufferSize(int byteSize, RHIDevice* rhiDevice)
{
	if (dummyVertexBuffer.size < byteSize)
	{
		dummyVertexBuffer.cpuBuffer.resize(byteSize, 0);
		dummyVertexBuffer.size = byteSize;

		BufferDesc bufferDesc = BufferDesc(byteSize, BufferBindFlag::BBF_VERTEX_BUFFER);
		rhiDummyVertexBuffer = rhiDevice->CreateBuffer(bufferDesc, dummyVertexBuffer.cpuBuffer.data());
	}
}

RHIBufferPtr MeshRenderData::GetDummyVertexBuffer()
{
	return rhiDummyVertexBuffer;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

IMPL_CLASS_TYPE(Mesh);

Mesh::Mesh()
{
}

Mesh::~Mesh()
{
}

void Mesh::OnDestroy()
{
	//wait until modification ends
	modificationMutex.lock();
	{
		CHECK(modifyingData == nullptr);
	}
	modificationMutex.unlock();

	modifyingData.reset();
	data.reset();
	renderData.reset();
}

void Mesh::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("data", &Self::data);
}

MeshDataWrittablePtr Mesh::BeginModify()
{
	modificationMutex.lock();
	CHECK(modifyingData == nullptr);

	if (data == nullptr)
		data = new MeshDataWrittable();

	modifyingData = new MeshDataWrittable();
	data->CopyTo(*modifyingData);

	return modifyingData;
}

MeshDataWrittablePtr Mesh::BeginModifyOnEmptyData()
{
	modificationMutex.lock();
	CHECK(modifyingData == nullptr);

	modifyingData = new MeshDataWrittable();

	return modifyingData;
}

void Mesh::EndModify()
{
	boundingBoxNeedUpdate = true;
	renderDataDirty = true;

	this->data = modifyingData.StaticCast<MeshData>();

	modifyingData = nullptr;

	modificationMutex.unlock();
}

MeshRenderDataPtr Mesh::GetMeshRenderData()
{
	if (renderData && !renderDataDirty)
		return renderData;

	renderDataDirty = false;

	renderData = new MeshRenderData();
	renderData->data = GetMeshData();
	renderData->boundingBox = GetBoundingBox();

	return renderData;
}

void Mesh::UpdateBoundingBox()
{
	boundingBoxNeedUpdate = false;

	boundingBox.set_empty();

	MeshDataPtr meshData = GetMeshData();

	const MeshVertexComponent* positionComp = meshData->FindVertexComponent(InputElement::IE_POSITION);
	if (positionComp)
	{
		// must be compatible with float3
		if (positionComp->format.baseFormat == MVBF_Float && positionComp->format.count >= 3)
		{
			vcpp::StridedArrayView<float3> positionView = meshData->GetVertexDataView<float3>(positionComp);
			for (int i = 0; i < positionView.size(); i++)
			{
				float3 pos = positionView[i];
				boundingBox.unionWith(pos);
			}
		}
	}
}

AxisAlignedBox3<float> Mesh::GetBoundingBox()
{
	if (boundingBoxNeedUpdate)
		UpdateBoundingBox();

	return boundingBox;
}

bool Mesh::Raycast(const float3& rayDirection, const float3& rayOrigin, float rayLength, std::vector<RaycastResult>& outResults, bool sort)
{
	MeshDataPtr meshData = GetMeshData();

	AxisAlignedBox3<float> bounds = GetBoundingBox();

	size_t numPrev = outResults.size();

	if (bounds.intersectsRay(rayOrigin, rayDirection))
	{
		if (meshData->GetPrimitiveTopology() == PrimitiveTopology::PT_TriangleList)
		{
			vcpp::StridedArrayView<float3> positions = meshData->GetVertexDataView<float3>(IE_POSITION);

			if (!meshData->HasIndicies())
			{
				for (int ivert = 0; ivert < positions.size(); ivert += 3)
				{
					const float3& v0 = positions[ivert + 0];
					const float3& v1 = positions[ivert + 1];
					const float3& v2 = positions[ivert + 2];

					RaycastResult result;
					if (vcpp::la::intersection_ray_triangle(rayOrigin, rayDirection, v0, v1, v2, result.distance, result.triangleBarycentric))
					{
						outResults.push_back(result);
					}
				}
			}
			else if (meshData->GetIndexFormat() == IndexFormat::IF_UINT16)
			{
				vcpp::ArrayView<uint16> indices = meshData->GetIndexDataView<uint16>();

				for (int iind = 0; iind < indices.size(); iind += 3)
				{
					uint16 ind0 = indices[iind + 0];
					uint16 ind1 = indices[iind + 1];
					uint16 ind2 = indices[iind + 2];

					const float3& v0 = positions[ind0];
					const float3& v1 = positions[ind1];
					const float3& v2 = positions[ind2];

					RaycastResult result;
					if (vcpp::la::intersection_ray_triangle(rayOrigin, rayDirection, v0, v1, v2, result.distance, result.triangleBarycentric))
					{
						outResults.push_back(result);
					}
				}
			}
			else if (meshData->GetIndexFormat() == IndexFormat::IF_UINT32)
			{
				vcpp::ArrayView<uint32> indices = meshData->GetIndexDataView<uint32>();

				for (int iind = 0; iind < indices.size(); iind += 3)
				{
					uint32 ind0 = indices[iind + 0];
					uint32 ind1 = indices[iind + 1];
					uint32 ind2 = indices[iind + 2];

					const float3& v0 = positions[ind0];
					const float3& v1 = positions[ind1];
					const float3& v2 = positions[ind2];

					RaycastResult result;
					if (vcpp::la::intersection_ray_triangle(rayOrigin, rayDirection, v0, v1, v2, result.distance, result.triangleBarycentric))
					{
						outResults.push_back(result);
					}
				}
			}
		}
	}

	if (sort)
	{
		std::sort(outResults.begin(), outResults.end());
	}

	return outResults.size() > numPrev;
}
