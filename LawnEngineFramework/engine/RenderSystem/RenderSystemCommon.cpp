#include "fast_compile.h"
#include "RenderSystemCommon.h"

struct LightTypeEnumType : public EnumObjectType<LightType>
{
	LightTypeEnumType() : EnumObjectType<LightType>("LightType")
	{
		Add("PointLight", LT_PointLight);
		Add("DirectionalLight", LT_DirectionalLight);
		Add("SpotLight", LT_SpotLight);
	}
};
IMPL_ENUM_TYPE(LightType, LightTypeEnumType)

struct BlendModeEnumType : public EnumObjectType<BlendMode>
{
	BlendModeEnumType() : EnumObjectType<BlendMode>("BlendMode")
	{
		Add("Opaque", BlendMode_Opaque);
		Add("Transparent", BlendMode_Transparent);
	}
};
IMPL_ENUM_TYPE(BlendMode, BlendModeEnumType)
