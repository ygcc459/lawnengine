#pragma once

/*
class DynamicBuffer
{
	size_t _size;
	RHIBufferPtr _buffer;

public:
	DynamicBuffer()
		:_size(), _buffer()
	{}

	~DynamicBuffer() {
		destroy();
	}

	void create(size_t size, BufferBindFlag binding = BBF_VERTEX_BUFFER, void* init_data = NULL) {
		destroy();

		RHIDevicePtr rhiDevice = RenderSystem::instance().rhiDevice;

		this->_size = size;
		this->_buffer = rhiDevice->CreateBuffer(_size, binding, BufferUsage::BU_DYNAMIC, CpuAccessFlag::CAF_WRITE, init_data);
	}

	void destroy() {
		RHIDevicePtr rhiDevice = RenderSystem::instance().rhiDevice;
		rhiDevice->SafeReleaseBuffer(_buffer);
		_size = 0;
	}

	size_t size() const {
		return _size;
	}

	RHIBufferPtr buffer() const {
		return _buffer;
	}

	//if succeed, return the buffer pointer to append to
	//if failed, return NULL
	void* begin_append() {
		D3D11_MAPPED_SUBRESOURCE mapped_resource;
		HRESULT hr = RenderSystem::instance().deviceContext->Map(_buffer, 0, D3D11_MAP_WRITE_NO_OVERWRITE, 0, &mapped_resource);

		if(SUCCEEDED(hr))
			return mapped_resource.pData;
		else
			return NULL;
	}

	void end_append() {
		RenderSystem::instance().deviceContext->Unmap(_buffer, 0);
	}
	
	//if succeed, return the buffer pointer to append to
	//if failed, return NULL
	void* begin_overwrite() {
		D3D11_MAPPED_SUBRESOURCE mapped_resource;
		HRESULT hr = RenderSystem::instance().deviceContext->Map(_buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped_resource);

		if(SUCCEEDED(hr))
			return mapped_resource.pData;
		else
			return NULL;
	}

	void end_overwrite() {
		RenderSystem::instance().deviceContext->Unmap(_buffer, 0);
	}	
};
*/
