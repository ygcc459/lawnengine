#pragma once

/*
template<class T, uint block_capacity>
class GrowableBuffer
{
public:
	typedef T ElementType;

protected:
	struct Block {
		RHIBufferPtr buffer;
		ElementType* mapped_buffer;
		uint capacity;
		uint size;
	};

	std::vector<Block> blocks;

	bool writting;

	BufferBindFlag bind_flags;

public:
	GrowableBuffer(BufferBindFlag bind_flags)
		:writting(false), bind_flags(bind_flags)
	{
	}

	virtual ~GrowableBuffer()
	{
		clear();
	}

	void clear()
	{
		assert(writting==false);

		RHIDevicePtr rhiDevice = RenderSystem::instance().rhiDevice;

		for(uint i=0; i<blocks.size(); i++)
			rhiDevice->SafeReleaseBuffer(blocks[i].buffer);

		blocks.clear();
	}

	bool begin_push_back()
	{
		writting = true;

		lock_block(&blocks.back());
	}

	bool push_back(const ElementType& v)
	{
		assert(writting==true);

		Block* b = &blocks.back();

		if(b->size==b->capacity)
		{
			unlock_block(b);
			
			b = push_back_new_block();

			lock_block(b);
		}

		b->mapped_buffer[b->size] = v;
		b->size++;
	}

	void end_push_back()
	{
		assert(writting==true);

		Block* b = &blocks.back();
		unlock_block(b);

		writting = false;	
	}

	uint buffer_count() const
	{
		return blocks.size();
	}

	RHIBufferPtr get_buffer(uint i)
	{
		assert(writting==false || i!=blocks.size()-1);

		return blocks[i].buffer;
	}

protected:
	Block* push_back_new_block()
	{
		RHIDevicePtr rhiDevice = RenderSystem::instance().rhiDevice;

		blocks.push_back(Block());

		Block& b = blocks.back();

		b.buffer = rhiDevice->CreateBuffer(block_capacity*sizeof(ElementType), bind_flags, BufferUsage::BU_DYNAMIC, CAF_WRITE, NULL);
		b.mapped_buffer = NULL;
		b.capacity = block_capacity;
		b.size = 0;

		return &b;
	}

	bool lock_block(Block* b)
	{
		if(FAILED(b->buffer->Map(D3D11_MAP_WRITE_NO_OVERWRITE, 0, (void**)(&b->mapped_buffer)))
			return false;
		else
			return true;
	}

	void unlock_block(Block* b)
	{
		b->buffer->Unmap();
		b->mapped_buffer = NULL;
	}

};
*/