#pragma once

#include "RenderSystemCommon.h"
#include "RHI/RHIResources.h"
#include "Texture.h"

class DepthStencilRenderTarget : public Texture
{
	DECL_CLASS_TYPE(DepthStencilRenderTarget, Texture);

public:
	int width;
	int height;
	PixelFormat format;

	RHITexturePtr rhiTexture;

public:
	DepthStencilRenderTarget();
	virtual ~DepthStencilRenderTarget();

	virtual void Reflect(Reflector& reflector) override;

	void Create(int width, int height, PixelFormat format);

	void FillTextureDesc(TextureDesc& desc);
	virtual bool CreateRHIResources(RHIDevice* device) override;
	virtual bool UpdateRHIResources(RHICommandList* rhiCommandList) override;
	virtual void DestroyRHIResources();

	virtual RHITexturePtr GetRHITexture() override;
};

typedef std::shared_ptr<DepthStencilRenderTarget> DepthStencilRenderTargetPtr;
