#pragma once

#include "RHI/RHICommon.h"

//////////////////////////////////////////////////////////////////////////

template<PixelFormat pixelFormat>
struct PixelFormatToType
{
};

template<>
struct PixelFormatToType<PixelFormat::PF_R8G8B8A8>
{
	typedef byte4 Value;
};

template<>
struct PixelFormatToType<PixelFormat::PF_R8>
{
	typedef byte Value;
};

template<>
struct PixelFormatToType<PixelFormat::PF_R16G16B16A16_Float>
{
	typedef half4 Value;
};

template<>
struct PixelFormatToType<PixelFormat::PF_R32G32B32A32_Float>
{
	typedef float4 Value;
};

//////////////////////////////////////////////////////////////////////////

template<typename pixelType>
struct PixelTypeToFloatType
{
};

template<>
struct PixelTypeToFloatType<byte>
{
	typedef float Value;
};
template<>
struct PixelTypeToFloatType<byte2>
{
	typedef float2 Value;
};
template<>
struct PixelTypeToFloatType<byte3>
{
	typedef float3 Value;
};
template<>
struct PixelTypeToFloatType<byte4>
{
	typedef float4 Value;
};

template<>
struct PixelTypeToFloatType<half>
{
	typedef float Value;
};
template<>
struct PixelTypeToFloatType<half2>
{
	typedef float2 Value;
};
template<>
struct PixelTypeToFloatType<half3>
{
	typedef float3 Value;
};
template<>
struct PixelTypeToFloatType<half4>
{
	typedef float4 Value;
};

template<>
struct PixelTypeToFloatType<float>
{
	typedef float Value;
};
template<>
struct PixelTypeToFloatType<float2>
{
	typedef float2 Value;
};
template<>
struct PixelTypeToFloatType<float3>
{
	typedef float3 Value;
};
template<>
struct PixelTypeToFloatType<float4>
{
	typedef float4 Value;
};

//////////////////////////////////////////////////////////////////////////

template<typename srctype, typename dsttype>
struct PixelTypeConvert
{
	static dsttype Convert(const srctype& v)
	{
		dsttype r = v;
		return r;
	}
};

//
// bytes to floats
//
template<>
struct PixelTypeConvert<byte, float>
{
	static float Convert(const byte& v)
	{
		return v / 255.0f;
	}
};
template<>
struct PixelTypeConvert<byte2, float2>
{
	static float2 Convert(const byte2& v)
	{
		return float2(v.x / 255.0f, v.y / 255.0f);
	}
};
template<>
struct PixelTypeConvert<byte3, float3>
{
	static float3 Convert(const byte3& v)
	{
		return float3(v.x / 255.0f, v.y / 255.0f, v.z / 255.0f);
	}
};
template<>
struct PixelTypeConvert<byte4, float4>
{
	static float4 Convert(const byte4& v)
	{
		return float4(v.x / 255.0f, v.y / 255.0f, v.z / 255.0f, v.w / 255.0f);
	}
};

//
// floats to bytes
//
template<>
struct PixelTypeConvert<float, byte>
{
	static byte Convert(const float& v)
	{
		return (byte)vcpp::math::round_int(v * 255);
	}
};
template<>
struct PixelTypeConvert<float2, byte2>
{
	static byte2 Convert(const float2& v)
	{
		return byte2((byte)vcpp::math::round_int(v.x * 255), (byte)vcpp::math::round_int(v.y * 255));
	}
};
template<>
struct PixelTypeConvert<float3, byte3>
{
	static byte3 Convert(const float3& v)
	{
		return byte3((byte)vcpp::math::round_int(v.x * 255), (byte)vcpp::math::round_int(v.y * 255), (byte)vcpp::math::round_int(v.z * 255));
	}
};
template<>
struct PixelTypeConvert<float4, byte4>
{
	static byte4 Convert(const float4& v)
	{
		return byte4((byte)vcpp::math::round_int(v.x * 255), (byte)vcpp::math::round_int(v.y * 255), (byte)vcpp::math::round_int(v.z * 255), (byte)vcpp::math::round_int(v.w * 255));
	}
};

//
// halfs to floats
//
template<>
struct PixelTypeConvert<half, float>
{
	static float Convert(const half& v)
	{
		return vcpp::la::HalfToFloat(v);
	}
};
template<>
struct PixelTypeConvert<half2, float2>
{
	static float2 Convert(const half2& v)
	{
		return float2(vcpp::la::HalfToFloat(v.x), vcpp::la::HalfToFloat(v.y));
	}
};
template<>
struct PixelTypeConvert<half3, float3>
{
	static float3 Convert(const half3& v)
	{
		return float3(vcpp::la::HalfToFloat(v.x), vcpp::la::HalfToFloat(v.y), vcpp::la::HalfToFloat(v.z));
	}
};
template<>
struct PixelTypeConvert<half4, float4>
{
	static float4 Convert(const half4& v)
	{
		return float4(vcpp::la::HalfToFloat(v.x), vcpp::la::HalfToFloat(v.y), vcpp::la::HalfToFloat(v.z), vcpp::la::HalfToFloat(v.w));
	}
};

//
// floats to halfs
//
template<>
struct PixelTypeConvert<float, half>
{
	static half Convert(const float& v)
	{
		return vcpp::la::FloatToHalf(v);
	}
};
template<>
struct PixelTypeConvert<float2, half2>
{
	static half2 Convert(const float2& v)
	{
		return half2(vcpp::la::FloatToHalf(v.x), vcpp::la::FloatToHalf(v.y));
	}
};
template<>
struct PixelTypeConvert<float3, half3>
{
	static half3 Convert(const float3& v)
	{
		return half3(vcpp::la::FloatToHalf(v.x), vcpp::la::FloatToHalf(v.y), vcpp::la::FloatToHalf(v.z));
	}
};
template<>
struct PixelTypeConvert<float4, half4>
{
	static half4 Convert(const float4& v)
	{
		return half4(vcpp::la::FloatToHalf(v.x), vcpp::la::FloatToHalf(v.y), vcpp::la::FloatToHalf(v.z), vcpp::la::FloatToHalf(v.w));
	}
};

//
// xxx3 to xxx4
//
template<>
struct PixelTypeConvert<byte3, byte4>
{
	static byte4 Convert(const byte3& v)
	{
		return byte4(v.x, v.y, v.z, 0);
	}
};

template<>
struct PixelTypeConvert<half3, half4>
{
	static half4 Convert(const half3& v)
	{
		return half4(v.x, v.y, v.z, 0);
	}
};

template<>
struct PixelTypeConvert<float3, float4>
{
	static float4 Convert(const float3& v)
	{
		return float4(v.x, v.y, v.z, 0);
	}
};

//
// xxx1 to xxx4
//
template<>
struct PixelTypeConvert<byte, byte4>
{
	static byte4 Convert(const byte& v)
	{
		return byte4(v, v, v, 0);
	}
};

template<>
struct PixelTypeConvert<half, half4>
{
	static half4 Convert(const half& v)
	{
		return half4(v, v, v, 0);
	}
};

template<>
struct PixelTypeConvert<float, float4>
{
	static float4 Convert(const float& v)
	{
		return float4(v, v, v, 0);
	}
};

//////////////////////////////////////////////////////////////////////////
