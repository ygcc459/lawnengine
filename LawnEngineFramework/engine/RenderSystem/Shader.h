#pragma once

#include "RenderSystemCommon.h"
#include "RHI/RHIResources.h"
#include "RHI/RHIShader.h"
#include "PODValueContainer.h"

class Shader;
class ShaderParamValues;
class ShaderTechnique;
class ShaderVariant;
class ShaderPass;
struct ShaderVariable;
struct ShaderCBufferSlot;
struct ShaderSamplerSlot;
struct ShaderResourceSlot;

class Archive;
class ArchiveNode;

#include <functional>

enum class ShaderParamGroup
{
	ObjectParams = 0,
	MaterialParams,
	PassParams,

	Count,
};

struct ShaderVariable
{
	FixedString name;
	int index;
	ShaderVariableType type;

	ShaderCBufferSlot* cbuffer;

	uint32 byteOffset;
	uint32 byteSize;

	ShaderVariable()
		:index(-1), type(SVT_Unknown), cbuffer(nullptr), byteOffset(0), byteSize(0)
	{}

	~ShaderVariable()
	{}
};

struct ShaderCBufferSlot
{
	FixedString name;
	std::vector<ShaderVariable*> variables;

	std::vector<uint8> defaultCBufferData;

	int byteSize;

	uint32 index;

	ShaderParamGroup group;

	uint64 layoutHash;

	ShaderCBufferSlot()
		:index(0xFFFFFFFF), byteSize(0), layoutHash(0)
	{}

	~ShaderCBufferSlot()
	{}
};

struct ShaderResourceSlot
{
	FixedString name;
	int index;
	ShaderResourceType type;

	TexturePtr defaultValue;

	int samplerSlotIndex;

	ShaderParamGroup group;

	ShaderResourceSlot()
		:index(-1), type(SRT_Unknown), samplerSlotIndex(-1)
	{}

	~ShaderResourceSlot()
	{}
};

struct ShaderSamplerSlot
{
	FixedString name;
	int32 index = -1;

	ShaderParamGroup group;

	ShaderResourceSlot* resourceSlot = nullptr;
};

struct ShaderVariantDimension
{
	FixedString name;

	std::map<FixedString, uint32> valueToIndexMap;
	std::vector<FixedString> values;
	
	uint32 defaultValueIndex;

	void SetDefaultValue(const FixedString& v)
	{
		uint32 index = AddValue(v);
		defaultValueIndex = index;
	}

	uint32 AddValue(const FixedString& v)
	{
		auto it = valueToIndexMap.find(v);
		if (it == valueToIndexMap.end())
		{
			uint32 index = (uint32)values.size();
			values.push_back(v);

			valueToIndexMap.insert(std::make_pair(v, index));

			return index;
		}
		else
		{
			return it->second;
		}
	}

	bool GetValueIndex(const FixedString& v, uint32& outIndex)
	{
		auto it = valueToIndexMap.find(v);
		if (it != valueToIndexMap.end())
		{
			outIndex = it->second;
			return true;
		}
		else
		{
			return false;
		}
	}
};

typedef uint64 ShaderVariantKeysCompressed;

struct ShaderVariantCollection
{
	enum
	{
		CompressedKeysTotalBits = sizeof(ShaderVariantKeysCompressed) * 8,
	};

	std::map<FixedString, int32> dimensionNameToIndexMap;

	std::vector<ShaderVariantDimension*> dimensions;

	std::vector<std::pair<uint32, ShaderVariantKeysCompressed>> dimensionValueBitShiftAndMasks;

	ShaderVariantCollection()
	{}

	~ShaderVariantCollection()
	{
		for (auto* dimension : dimensions)
		{
			delete dimension;
		}
	}

	bool IsEmpty() const { return dimensions.empty(); }

	ShaderVariantDimension* GetDimension(int32 index)
	{
		return dimensions[index];
	}

	ShaderVariantDimension* AddDimension(const FixedString& name)
	{
		auto it = dimensionNameToIndexMap.find(name);
		if (it == dimensionNameToIndexMap.end())
		{
			ShaderVariantDimension* dimension = new ShaderVariantDimension();
			dimension->name = name;

			int32 index = (int32)dimensions.size();
			dimensions.push_back(dimension);

			dimensionNameToIndexMap.insert(std::make_pair(name, index));

			return dimensions.back();
		}
		else
		{
			return dimensions[it->second];
		}
	}

	bool HasDimension(const FixedString& name) const
	{
		return dimensionNameToIndexMap.find(name) != dimensionNameToIndexMap.end();
	}

	int32 GetDimensionIndex(const FixedString& name) const
	{
		auto it = dimensionNameToIndexMap.find(name);
		if (it != dimensionNameToIndexMap.end())
			return it->second;
		else
			return -1;
	}

	void BuildValueBitMasks()
	{
		dimensionValueBitShiftAndMasks.resize(dimensions.size());

		uint32 totalBits = 0;
		uint32 maxBits = CompressedKeysTotalBits;

		for (int32 i = 0; i < dimensions.size(); i++)
		{
			ShaderVariantDimension* dimension = dimensions[i];

			if (dimension->values.size() != 0)
			{
				uint32 maxIndex = (uint32)dimension->values.size() - 1;

				uint32 bitsNeeded = 0;
				while (maxIndex != 0)
				{
					bitsNeeded++;
					maxIndex = maxIndex >> 1;
				}

				if (totalBits + bitsNeeded <= maxBits)
				{
					uint32 shift = totalBits;
					ShaderVariantKeysCompressed mask = (1 << bitsNeeded) - 1;

					dimensionValueBitShiftAndMasks[i] = std::make_pair(shift, mask);

					totalBits += bitsNeeded;
				}
				else
				{
					Log::Error("ShaderVariantCollection too large to fit in a ShaderVariantKeysCompressed");
					break;
				}
			}
		}
	}

	ShaderVariantKeysCompressed CompressKeys(const std::vector<uint32>& dimensionValueIndices)
	{
		ShaderVariantKeysCompressed result = 0;

		for (int32 i = 0; i < dimensions.size(); i++)
		{
			ShaderVariantDimension* dimension = dimensions[i];

			if (dimension->values.size() != 0)
			{
				auto& pair = dimensionValueBitShiftAndMasks[i];

				uint32 value = dimensionValueIndices[i];

				uint32 shift = pair.first;
				ShaderVariantKeysCompressed mask = pair.second;

				ShaderVariantKeysCompressed cur = (value & mask) << shift;

				result |= cur;
			}
		}

		return result;
	}
};

struct ShaderVariantKeys
{
	ShaderVariantCollection* collection;

	std::vector<uint32> dimensionValueIndices;  //an index to ShaderVariantDimension.values

	mutable bool compressedKeyDirty;
	mutable ShaderVariantKeysCompressed compressedKey;

	ShaderVariantKeys()
		: collection(nullptr), compressedKeyDirty(true)
	{
	}

	void BindCollection(ShaderVariantCollection* collection)
	{
		CHECK(collection);

		this->collection = collection;

		if (collection)
		{
			dimensionValueIndices.resize(collection->dimensions.size());

			for (int32 i = 0; i < collection->dimensions.size(); i++)
			{
				ShaderVariantDimension* dimension = collection->dimensions[i];
				dimensionValueIndices[i] = dimension->defaultValueIndex;
			}

			compressedKeyDirty = true;
		}
	}

	void SetValue(int32 dimensionIndex, uint32 valueIndex)
	{
		dimensionValueIndices[dimensionIndex] = valueIndex;
		compressedKeyDirty = true;
	}

	void SetValue(const FixedString& name, const FixedString& value)
	{
		int32 dimensionIndex = collection->GetDimensionIndex(name);
		if (dimensionIndex >= 0)
		{
			ShaderVariantDimension* dimension = collection->GetDimension(dimensionIndex);

			uint32 valueIndex;
			if (dimension->GetValueIndex(value, valueIndex))
			{
				SetValue(dimensionIndex, valueIndex);
			}
			else
			{
				SetValue(dimensionIndex, dimension->defaultValueIndex);
			}
		}
	}

	FixedString GetValue(const FixedString& name)
	{
		int32 dimensionIndex = collection->GetDimensionIndex(name);
		if (dimensionIndex >= 0)
		{
			uint32 valueIndex = dimensionValueIndices[dimensionIndex];

			FixedString valueStr = collection->GetDimension(dimensionIndex)->values[valueIndex];

			return valueStr;
		}
		else
		{
			return 0;
		}
	}

	FixedString GetValue(int32 dimensionIndex)
	{
		uint32 valueIndex = dimensionValueIndices[dimensionIndex];

		FixedString valueStr = collection->GetDimension(dimensionIndex)->values[valueIndex];

		return valueStr;
	}

	ShaderVariantKeysCompressed GetCompressedKey() const
	{
		if (compressedKeyDirty)
		{
			compressedKeyDirty = false;
			compressedKey = collection->CompressKeys(this->dimensionValueIndices);
		}
		return compressedKey;
	}

	bool operator < (const ShaderVariantKeys& other) const
	{
		auto key1 = this->GetCompressedKey();
		auto key2 = other.GetCompressedKey();
		return key1 < key2;
	}
};

class ShaderPass : public BaseObject, public RefCountedObject
{
public:
	struct ShaderBindingItem
	{
		ShaderParamGroup group;
		BindingLayoutItem bindingLayout;

		union
		{
			ShaderCBufferSlot* cbufferSlot;
			ShaderResourceSlot* resourceSlot;
			ShaderSamplerSlot* samplerSlot;
		};

		ShaderBindingItem(ShaderCBufferSlot* cbufferSlot, BindingLayoutItem bindingLayout)
			: group(cbufferSlot->group), bindingLayout(bindingLayout)
		{
			this->cbufferSlot = cbufferSlot;
		}

		ShaderBindingItem(ShaderResourceSlot* resourceSlot, BindingLayoutItem bindingLayout)
			: group(resourceSlot->group), bindingLayout(bindingLayout)
		{
			this->resourceSlot = resourceSlot;
		}

		ShaderBindingItem(ShaderSamplerSlot* samplerSlot, BindingLayoutItem bindingLayout)
			: group(samplerSlot->group), bindingLayout(bindingLayout)
		{
			this->samplerSlot = samplerSlot;
		}
	};

	struct ShaderFrequencyData
	{
		RHIShaderPtr rhiShader;

		std::vector<ShaderBindingItem> bindingItems;

		RHIBindingLayoutPtr rhiBindingLayout;

		ShaderFrequencyData()
		{}
	};

public:
	static std::atomic<uint32> nextShaderPassID;

	uint32 shaderPassID;

	bool isValid;
	
	ShaderVariant* variant;

	ShaderFrequencyData frequencyDatas[ShaderFrequency::SF_Count];

	RHIRenderStateDesc renderStateDesc;

	std::vector<ShaderRequiredVertexComponentDesc> requiredVertexComponents;

	std::vector<ShaderCBufferSlot*> cbuffers;
	std::vector<ShaderVariable*> variables;
	std::vector<ShaderResourceSlot*> resources;
	std::vector<ShaderSamplerSlot*> samplers;

	uint32 passParamsCBufferIndex;
	uint64 passParamsLayoutHash;

	uint32 materialParamsCBufferIndex;
	uint64 materialParamsLayoutHash;

	float4 blendConstantColor;

	bool rhiResourcesDirty = true;
	
public:
	ShaderPass(ShaderVariant* variant);
	~ShaderPass();

	void Destroy();

	uint32 GetShaderPassID() const { return shaderPassID; }

	const std::vector<ShaderRequiredVertexComponentDesc>& GetRequiredVertexComponents();

	ShaderCBufferSlot* FindCBuffer(FixedString name);
	ShaderVariable* FindVariable(FixedString name);
	ShaderResourceSlot* FindResource(FixedString name);
	int32 FindResourceIndex(FixedString name);
	ShaderSamplerSlot* FindSampler(FixedString name);

	ShaderCBufferSlot* CreateCBuffer(const RHIShaderCBufferDesc& rhiCBDesc);
	ShaderResourceSlot* CreateResource(const RHIShaderResourceDesc& rhiResDesc);
	ShaderSamplerSlot* CreateSampler(const RHIShaderSamplerDesc& rhiSamplerDesc);

	ShaderCBufferSlot* GetPassParamsCBuffer();
	ShaderCBufferSlot* GetMaterialParamsCBuffer();

	bool CreateRHIResources(RHIDevice* device);
	bool UpdateRHIResources(RHICommandList* rhiCommandList);
	void DestroyRHIResources();
	bool IsRHIResourcesCreated() const;
};

typedef RefCountedPtr<ShaderPass> ShaderPassPtr;

class ShaderVariant
{
public:
	ShaderTechnique* technique;

	ShaderVariantKeys keys;

	std::string name;

	std::vector<ShaderPassPtr> passes;

	ShaderVariant(ShaderTechnique* technique)
		: technique(technique)
	{}

	~ShaderVariant()
	{}

	ShaderPassPtr FirstPass()
	{
		if (!passes.empty())
			return passes[0];
		else
			return nullptr;
	}
};

class ShaderTechnique
{
public:
	Shader* shader;

	FixedString name;

	ShaderVariantCollection variantCollection;

	std::map<ShaderVariantKeysCompressed, std::unique_ptr<ShaderVariant>> variants;

public:
	ShaderTechnique(Shader* shader)
		: shader(shader)
	{}

	~ShaderTechnique()
	{}

	ShaderVariant* FindVariant(const ShaderVariantKeysCompressed& keys)
	{
		auto it = variants.find(keys);
		if (it != variants.end())
		{
			return it->second.get();
		}
		else
		{
			return nullptr;
		}
	}

	ShaderVariant* FindVariant(const ShaderVariantKeys& keys)
	{
		auto keysCompressed = keys.GetCompressedKey();
		return FindVariant(keysCompressed);
	}

	ShaderVariant* DefaultVariant();
};

enum ShaderPropertyType
{
	SPT_VariantKey = 0,
	SPT_Color,
	SPT_Float,
	SPT_Float2,
	SPT_Float3,
	SPT_Float4,
	SPT_Texture1D,
	SPT_Texture2D,
	SPT_Texture3D,
	SPT_TextureCube,
};
DECL_ENUM_TYPE(ShaderPropertyType);

class ShaderProperty
{
public:
	FixedString name;

	ShaderPropertyType type;

	bool hasDefaultValue;
	FixedString defaultValueVariantKey;
	float4 defaultValueFloat4;
	TexturePtr defaultValueTexture;

	bool ranged;
	float2 floatRange;  //x=min, y=max

	ShaderProperty()
		: hasDefaultValue(false), ranged(false)
	{}
};

class Shader : public Asset
{
	DECL_CLASS_TYPE(Shader, Asset);

protected:
	friend class ShaderPass;

	std::string sourceFilePath;

	std::map<FixedString, std::unique_ptr<ShaderProperty>> properties;

	std::vector<std::unique_ptr<ShaderTechnique>> techniques;

	std::map<FixedString, FixedString> samplerToTextureMapping;

	BlendMode blendMode;

	std::map<FixedString, ShaderParamGroup> cbufferNameToGroupMapping;
	std::map<FixedString, ShaderParamGroup> resourceNameToGroupMapping;
	std::map<FixedString, ShaderParamGroup> samplerNameToGroupMapping;

public:
	Shader();
	virtual ~Shader();

	virtual void Reflect(Reflector& reflector) override;

	bool Compile(Archive& archive);

	virtual bool OnCreate();
	virtual bool OnLoad(Archive& archive) override;
	virtual void OnDestroy();

	BlendMode GetBlendMode() const { return blendMode; }

	//
	// variable getters
	//
	ShaderTechnique* FindTechnique(FixedString name);
	ShaderTechnique* DefaultTechnique();

	std::map<FixedString, std::unique_ptr<ShaderProperty>>& GetProperties() { return properties; }

	ShaderParamGroup GetCBufferShaderParamGroup(FixedString name);
	ShaderParamGroup GetResourceShaderParamGroup(FixedString name);
	ShaderParamGroup GetSamplerShaderParamGroup(FixedString name);

public:
	static AssetPath FindShaderInclude(const AssetPath& shaderPath, const char* includeFileName);

private:
	bool ParseShader(ArchiveNode* shaderNode, const std::string& source);
	bool ParseShaderProperties(ArchiveNode* shaderNode);
	bool ParseShaderTechnique(ArchiveNode* techniqueNode, const std::string& rawsource);
	bool ParseShaderVariants(ShaderTechnique& technique, ArchiveNode* techiniqueNode, const std::string& rawsource);
	bool ParseShaderPass(ShaderVariant& variant, ArchiveNode* passNode, const std::string& source);
	bool ParseRenderStateDesc(ShaderPass& pass, ArchiveNode* node);
	bool ParseResourceDefaultValues(ShaderPass& pass, ArchiveNode* node);
	bool ParseShaderParamGroups(ShaderPass& pass, ArchiveNode* node);
	bool ParseSamplerFromTexture(ShaderPass& pass, ArchiveNode* node);

	bool CreateShaderVariants(ShaderTechnique& technique, ShaderVariantKeys& keys, size_t dimensionIndex, ArchiveNode* techniqueNode, const std::string& source);
	bool CreateShaderVariant(ShaderTechnique& technique, const ShaderVariantKeys& keys, ArchiveNode* techniqueNode, const std::string& source);

	bool BuildResourcesAndBindings(ShaderFrequency freq, ShaderPass& pass);

	bool ParseRequiredVertexComponents(ShaderPass& pass, RHIShaderPtr rhiShader);

	//semantic is the d3d standard semantic name, like "POSITION", "TEXCOORD0", "TEXCOORD7", "BINORMAL"
	static InputElement GetInputElement(FixedString semanticName, int semanticIndex);

};

typedef std::shared_ptr<Shader> ShaderPtr;
