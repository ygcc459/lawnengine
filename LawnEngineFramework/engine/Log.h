#pragma once

#include <mutex>

enum LogLevel
{
	LL_Debug = 0,
	LL_Info,
	LL_Warning,
	LL_Error,
};

typedef void(__stdcall *LogCallback)(LogLevel level, const char* content);

class Log
{
private:
	Log() {}

	static bool enableLogFile;
	static bool enableStdOut;
	static bool enablePlatformLog;
	static std::string logFilePath;
	static LogCallback logCallback;

	typedef std::recursive_mutex LogMutex;
	static LogMutex logMutex;

public:
	static void SetLogFilePath(const char* path);
	static void SetTarget(bool enableLogFile, bool enableStdOut, bool enablePlatformLog);

	static void PlatformLog(const char* format, ...);
	static void PlatformLogRaw(const char* msg);

	static void Debug(const char *format, ...);
	static void Info(const char *format, ...);
	static void Warning(const char *format, ...);
	static void Error(const char *format, ...);

	static void WriteLog(LogLevel level, const std::string& msg);
};
