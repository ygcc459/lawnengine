#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "FileSystem.h"

#include <filesystem>

//namespace fs = std::experimental::filesystem::v1;
namespace fs = std::filesystem;

bool FileSystem::Exists(const std::string& path)
{
	return fs::exists(path);
}

bool FileSystem::Delete(const std::string& path)
{
	return fs::remove_all(path);
}

bool FileSystem::CreateDirectory(const std::string& path)
{
	return fs::create_directories(path);
}

bool FileSystem::IsDirectory(const std::string& path)
{
	return fs::is_directory(path);
}

bool FileSystem::IsFile(const std::string& path)
{
	return fs::is_regular_file(path);
}

void FileSystem::List(const std::string& path, std::vector<std::string>& outChildPaths)
{
	if (IsDirectory(path))
	{
		for (auto& p : fs::directory_iterator(path))
		{
			outChildPaths.push_back(p.path().string());
		}
	}
}

void FileSystem::ListRecursive(const std::string& path, std::vector<std::string>& outChildPaths)
{
	if (IsDirectory(path))
	{
		for (auto& p : fs::directory_iterator(path))
		{
			auto& childpath = p.path();
			std::string childpathStr = childpath.string();

			outChildPaths.push_back(childpathStr);

			if (fs::is_directory(childpath))
			{
				ListRecursive(childpathStr, outChildPaths);
			}
		}
	}
}
std::string FileSystem::GetCurrentPath()
{
	return fs::current_path().string();
}

void FileSystem::SetCurrentPath(const std::string& path)
{
	fs::current_path(path);
}

double FileSystem::GetLastWriteTime(const std::string& path)
{
	auto t = fs::last_write_time(path).time_since_epoch();

	auto seconds = std::chrono::duration<double>(t).count();

	return seconds;
}

FileSystem& FileSystem::Default()
{
	static FileSystem defaultFileSystem;
	return defaultFileSystem;
}
