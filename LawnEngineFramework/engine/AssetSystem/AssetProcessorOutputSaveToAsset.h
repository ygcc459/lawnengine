#pragma once

#include "AssetProcessorOutput.h"
#include "AssetManager.h"

class AssetProcessor;

class AssetProcessorOutputSaveToAsset : public AssetProcessorOutput
{
	AssetPath processorPath;
	AssetPath outputPath;
	AssetImportRecordPtr lastImportRecord;

	std::map<std::string, BaseObjectPtr> outputObjects;
	std::string mainOutputName;

public:
	AssetProcessorOutputSaveToAsset(const AssetPath& processorPath, const AssetPath& outputPath);
	virtual ~AssetProcessorOutputSaveToAsset();

	virtual BaseObjectPtr CreateOutput(const std::string& key, Type* type, bool overwrite = true, bool isMainOutput = false) override;
	virtual BaseObjectPtr GetOutput(const std::string& key) override;

	virtual void GetOutputs(std::map<std::string, BaseObjectPtr>& outputs) override;

	virtual BaseObjectPtr GetMainOutput() override;
	virtual void SetAsMainOutput(const std::string& key) override;
};
