#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "TextureImporter.h"

#include "Streams.h"

#include <FreeImage.h>

#include "RenderSystem/Image2D.h"
#include "RenderSystem/ImageBasedLighting.h"

class FreeImageMemoryIO : public FreeImageIO
{
	MemoryReadStream* readStream;
	MemoryWriteStream* writeStream;

public:
	FreeImageMemoryIO(MemoryReadStream* readStream)
		: readStream(readStream), writeStream(nullptr)
	{
		read_proc = _ReadProc;
		write_proc = _WriteProc;
		tell_proc = _TellProc;
		seek_proc = _SeekProc;
	}

	FreeImageMemoryIO(MemoryWriteStream* writeStream)
		: readStream(nullptr), writeStream(writeStream)
	{
		read_proc = _ReadProc;
		write_proc = _WriteProc;
		tell_proc = _TellProc;
		seek_proc = _SeekProc;
	}

	static unsigned DLL_CALLCONV _ReadProc(void* dest, unsigned element_size, unsigned element_count, fi_handle handle)
	{
		FreeImageMemoryIO *memIO = (FreeImageMemoryIO*)handle;
		if (memIO->readStream)
		{
			return memIO->readStream->Read(dest, element_size * element_count);
		}
		else
		{
			return 0;
		}
	}

	static unsigned DLL_CALLCONV _WriteProc(void* src, unsigned element_size, unsigned element_count, fi_handle handle)
	{
		FreeImageMemoryIO *memIO = (FreeImageMemoryIO*)handle;
		if (memIO->writeStream)
		{
			return memIO->writeStream->Write(src, element_size * element_count);
		}
		else
		{
			return 0;
		}
	}

	static int DLL_CALLCONV _SeekProc(fi_handle handle, long offset, int origin)
	{
		FreeImageMemoryIO *memIO = (FreeImageMemoryIO*)handle;
		if (memIO->writeStream)
		{
			if (origin == SEEK_SET)
				memIO->writeStream->SetPosition(offset);
			else if (origin == SEEK_CUR)
				memIO->writeStream->SetPosition(memIO->writeStream->GetPosition() + offset);
			else if (origin == SEEK_END)
				memIO->writeStream->SetPosition(memIO->writeStream->Size() - offset);
			else
				return -1;

			return 0;
		}
		else if (memIO->readStream)
		{
			if (origin == SEEK_SET)
				memIO->readStream->SetPosition(offset);
			else if (origin == SEEK_CUR)
				memIO->readStream->SetPosition(memIO->readStream->GetPosition() + offset);
			else if (origin == SEEK_END)
				memIO->readStream->SetPosition(memIO->readStream->Size() - offset);
			else
				return -1;

			return 0;
		}
		else
		{
			return 0;
		}
	}

	static long DLL_CALLCONV _TellProc(fi_handle handle)
	{
		FreeImageMemoryIO *memIO = (FreeImageMemoryIO*)handle;
		if (memIO->writeStream)
		{
			return memIO->writeStream->GetPosition();
		}
		else if (memIO->readStream)
		{
			return memIO->readStream->GetPosition();
		}
		else
		{
			return 0;
		}
	}
};

struct FreeImageBitmapAutoRelease
{
	FIBITMAP* dib;

	FreeImageBitmapAutoRelease(FIBITMAP* dib)
		:dib(dib)
	{}

	~FreeImageBitmapAutoRelease()
	{
		if (dib)
		{
			FreeImage_Unload(dib);
			dib = nullptr;
		}
	}
};

Image2DMipmaps* ImportImage(AssetPath srcPath)
{
	FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilename(srcPath.ToString().c_str());

	if ((fif == FIF_UNKNOWN) || !FreeImage_FIFSupportsReading(fif))
	{
		Log::Error("ImportTexture2D failed, unknown format: '%s'", srcPath.ToString().c_str());
		return nullptr;
	}

	std::unique_ptr<ReadStream> stream = srcPath.OpenRead();
	if (!stream->CanRead())
	{
		Log::Error("ImportTexture2D failed, can not read from stream '%s'", srcPath.ToString().c_str());
		return nullptr;
	}

	std::vector<byte> data;
	if (!stream->ReadAllData(data))
	{
		Log::Error("ImportTexture2D failed, can not read from stream '%s'", srcPath.ToString().c_str());
		return nullptr;
	}

	MemoryReadStream memoryStream(data.data(), data.size(), false);

	FreeImageMemoryIO memoryIO(&memoryStream);

	FIBITMAP* dib = FreeImage_LoadFromHandle(fif, &memoryIO, (fi_handle)&memoryIO, 0);
	if (dib == nullptr)
	{
		Log::Error("ImportTexture2D failed, load image failed, '%s'", srcPath.ToString().c_str());
		return nullptr;
	}

	FreeImageBitmapAutoRelease dibAutoRelease(dib);

	BYTE* rawData = FreeImage_GetBits(dib);

	int width = FreeImage_GetWidth(dib);
	int height = FreeImage_GetHeight(dib);

	if (rawData == nullptr || width == 0 || height == 0)
	{
		Log::Error("ImportTexture2D failed, get bits or size failed, '%s'", srcPath.ToString().c_str());
		return nullptr;
	}

	FREE_IMAGE_TYPE image_type = FreeImage_GetImageType(dib);

	unsigned bitsPerPixel = FreeImage_GetBPP(dib);

	unsigned bytesPerPixel = bitsPerPixel / 8;

	if (bytesPerPixel * 8 != bitsPerPixel)
	{
		Log::Error("ImportTexture2D failed, bitsPerPixel %d not supported, '%s'", bitsPerPixel, srcPath.ToString().c_str());
		return nullptr;
	}

	PixelFormat pixelFormat;
	Image2DMipmaps* mipmaps;

	switch (image_type)
	{
	case FIT_BITMAP:  //! standard image : 1-, 4-, 8-, 16-, 24-, 32-bit
	{
		if (bitsPerPixel == 24)
		{
			pixelFormat = PF_R8G8B8A8;
			mipmaps = new Image2DMipmaps(width, height, pixelFormat, 1);

			for (int y = 0; y < height; y++)
			{
				RGBTRIPLE* srcline = (RGBTRIPLE*)FreeImage_GetScanLine(dib, height - 1 - y);
				for (int x = 0; x < width; x++)
				{
					RGBTRIPLE src = srcline[x];
					PixelAccess<PF_R8G8B8A8>::Write(mipmaps->GetMipmap(0), x, y, byte4(src.rgbtRed, src.rgbtGreen, src.rgbtBlue, 0));
				}
			}
		}
		else if (bitsPerPixel == 32)
		{
			pixelFormat = PF_R8G8B8A8;
			mipmaps = new Image2DMipmaps(width, height, pixelFormat, 1);

			for (int y = 0; y < height; y++)
			{
				RGBQUAD* srcline = (RGBQUAD*)FreeImage_GetScanLine(dib, height - 1 - y);
				for (int x = 0; x < width; x++)
				{
					RGBQUAD src = srcline[x];
					PixelAccess<PF_R8G8B8A8>::Write(mipmaps->GetMipmap(0), x, y, byte4(src.rgbRed, src.rgbGreen, src.rgbBlue, src.rgbReserved));
				}
			}
		}
		else
		{
			Log::Error("ImportTexture2D failed, unsuppoted bitsPerPixel: %d, '%s'", bitsPerPixel, srcPath.ToString().c_str());
			return nullptr;
		}
		break;
	}
	case FIT_RGBA16:  //! 64-bit RGBA image : 4 x 16-bit
	{
		pixelFormat = PF_R16G16B16A16_Float;
		mipmaps = new Image2DMipmaps(width, height, pixelFormat, 1);

		for (int y = 0; y < height; y++)
		{
			FIRGBA16* srcline = (FIRGBA16*)FreeImage_GetScanLine(dib, height - 1 - y);
			for (int x = 0; x < width; x++)
			{
				FIRGBA16 src = srcline[x];
				PixelAccess<PF_R16G16B16A16_Float>::WriteWithConvert(mipmaps->GetMipmap(0), x, y, float4(src.red, src.green, src.blue, src.alpha));
			}
		}

		break;
	}
	case FIT_RGBF:	//! 96-bit RGB float image	: 3 x 32-bit IEEE floating point
	{
		pixelFormat = PF_R32G32B32A32_Float;
		mipmaps = new Image2DMipmaps(width, height, pixelFormat, 1);

		for (int y = 0; y < height; y++)
		{
			FIRGBF* srcline = (FIRGBF*)FreeImage_GetScanLine(dib, height - 1 - y);
			for (int x = 0; x < width; x++)
			{
				FIRGBF src = srcline[x];
				PixelAccess<PF_R32G32B32A32_Float>::WriteWithConvert(mipmaps->GetMipmap(0), x, y, float4(src.red, src.green, src.blue, 1.0f));
			}
		}

		break;
	}
	case FIT_RGBAF:  //! 128-bit RGBA float image : 4 x 32-bit IEEE floating point
	{
		pixelFormat = PF_R32G32B32A32_Float;
		mipmaps = new Image2DMipmaps(width, height, pixelFormat, 1);

		for (int y = 0; y < height; y++)
		{
			FIRGBAF* srcline = (FIRGBAF*)FreeImage_GetScanLine(dib, height - 1 - y);
			for (int x = 0; x < width; x++)
			{
				FIRGBAF src = srcline[x];
				PixelAccess<PF_R32G32B32A32_Float>::WriteWithConvert(mipmaps->GetMipmap(0), x, y, float4(src.red, src.green, src.blue, src.alpha));
			}
		}

		break;
	}
	default:
	{
		Log::Error("ImportTexture2D failed, get bits or size failed, '%s'", srcPath.ToString().c_str());
		return nullptr;
	}
	}

	return mipmaps;
}

IMPL_CLASS_TYPE(TextureImporter);

TextureImporter::TextureImporter()
	:sRGB(false), useInIBL(false)
{
}

TextureImporter::~TextureImporter()
{
}

void TextureImporter::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);
	reflector.AddMember("sRGB", &Self::sRGB);
	reflector.AddMember("useInIBL", &Self::useInIBL);
}

bool TextureImporter::DoProcess(const std::vector<AssetPath>& inputAssetPaths, AssetProcessorOutput* processorOutput)
{
	if (inputAssetPaths.size() != 1)
	{
		Log::Error("TextureImporter must have one input texture");
		return false;
	}

	AssetPath inputPath = inputAssetPaths[0];
	
	Image2DMipmaps* imageRaw = ImportImage(inputAssetPaths[0]);
	if (!imageRaw)
		return false;

	Texture2DPtr mainTexture = processorOutput->CreateMainOutput<Texture2D>("main");
	if (!mainTexture)
		return false;

	if (!mainTexture->Create(*imageRaw))
	{
		Log::Error("Texture2D.Create failed: '%s'", inputPath.ToString().c_str());
		return false;
	}

	if (inputPath.LastExtension() == "exr")
	{
		this->sRGB = true;
		this->useInIBL = true;
	}

	mainTexture->sRGB = this->sRGB;

	if (useInIBL)
	{
		TextureIBLDataPtr IBLData = processorOutput->CreateMainOutput<TextureIBLData>("IBLData");
		if (!IBLData)
			return false;

		IBLData->InitFromImage(imageRaw->GetMipmap(0));
	}

	return true;
}
