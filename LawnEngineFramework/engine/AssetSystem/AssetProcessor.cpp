#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "AssetProcessor.h"

IMPL_ABSTRACT_CLASS_TYPE(AssetProcessor);

void AssetProcessor::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);
}
