#pragma once

class AssetProcessor;

class AssetProcessorOutput
{
public:
	AssetProcessorOutput() {}
	virtual ~AssetProcessorOutput() {}

	template<typename T>
	std::shared_ptr<T> CreateOutput(const std::string& key, bool overwrite = true)
	{
		BaseObjectPtr ptr = CreateOutput(key, &T::StaticType, overwrite, false);
		return Util::CastPtrWithCheck<T>(ptr);
	}

	template<typename T>
	std::shared_ptr<T> CreateMainOutput(const std::string& key, bool overwrite = true)
	{
		BaseObjectPtr ptr = CreateOutput(key, &T::StaticType, overwrite, true);
		return Util::CastPtrWithCheck<T>(ptr);
	}

	bool HasOutput(const std::string& key)
	{
		BaseObjectPtr ptr = GetOutput(key);
		return ptr != nullptr;
	}

	std::string FindUsableKey(const std::string& key)
	{
		std::string newkey = key;

		for (uint i = 1; HasOutput(newkey); i++)
		{
			newkey = formatString("%s_%d", key.c_str(), i);
		}

		return newkey;
	}

	template<typename T>
	std::shared_ptr<T> GetOutput(const std::string& key)
	{
		BaseObjectPtr ptr = GetOutput(key);
		return Util::CastPtrWithCheck<T>(ptr);
	}

	virtual BaseObjectPtr CreateOutput(const std::string& key, Type* type, bool overwrite = true, bool isMainOutput = false) = 0;
	virtual BaseObjectPtr GetOutput(const std::string& key) = 0;

	virtual void GetOutputs(std::map<std::string, BaseObjectPtr>& outputs) = 0;
	virtual BaseObjectPtr GetMainOutput() = 0;
	virtual void SetAsMainOutput(const std::string& key) = 0;
};
