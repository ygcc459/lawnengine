#pragma once

#include "AssetProcessorOutput.h"
#include "AssetManager.h"

class AssetProcessor;

class AssetProcessorOutputInMemory : public AssetProcessorOutput
{
	std::map<std::string, BaseObjectPtr> outputs;
	std::string mainOutputName;

public:
	AssetProcessorOutputInMemory();
	virtual ~AssetProcessorOutputInMemory();

	virtual BaseObjectPtr CreateOutput(const std::string& key, Type* type, bool overwrite = true, bool isMainOutput = false) override;
	virtual BaseObjectPtr GetOutput(const std::string& key) override;

	virtual void GetOutputs(std::map<std::string, BaseObjectPtr>& outputs) override;

	virtual BaseObjectPtr GetMainOutput() override;
	virtual void SetAsMainOutput(const std::string& key) override;
};
