#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "AssetPath.h"

class AssetPathObjectType : public PrimitiveObjectType<AssetPath>
{
public:
	AssetPathObjectType()
		: PrimitiveObjectType<AssetPath>("AssetPath")
	{}

	virtual void ToString(const void* p, std::string& s) const override
	{
		const AssetPath& v = GetValue(p);
		s = v.ToString();
	}

	virtual bool FromString(const std::string& s, void* p) const override
	{
		AssetPath v = AssetPath::FromString(s);
		SetValue(p, v);
		return true;
	}
};
IMPL_PRIMITIVE_TYPE(AssetPath, AssetPathObjectType);

FileSystem& AssetPath::GetFileSystem() const
{
	//TODO
	return FileSystem::Default();
}

AssetPath AssetPath::Parent() const
{
	std::string parent = vcpp::filesystem::path_get_parent(path);
	return AssetPath(this->type, parent);
}

// e.g. ".png", ".png.meta", ".mesh", ""
std::string AssetPath::Extensions() const
{
	std::string allExtensions = vcpp::filesystem::path_get_all_exts(path);
	return allExtensions;
}

std::string AssetPath::LastExtension() const
{
	std::string allExtensions = vcpp::filesystem::path_get_ext(path);
	return allExtensions;
}

AssetPath AssetPath::ReplaceExtensions(const std::string& ext) const
{
	AssetPath dir;
	std::string filename, extensions;
	Split(dir, filename, extensions);

	std::string filenameWithNewExt = filename + ext;

	AssetPath newPath = dir.Join(filenameWithNewExt);

	return newPath;
}

// filename without extension
std::string AssetPath::Filename() const
{
	AssetPath dir;
	std::string filename;
	std::string extensions;
	Split(dir, filename, extensions);

	return filename;
}

std::string AssetPath::FilenameWithExtension() const
{
	return vcpp::filesystem::path_get_name(path);
}

void AssetPath::Split(AssetPath& dir, std::string& filename, std::string& extensions) const
{
	std::string dirstr;
	vcpp::filesystem::path_split(path, dirstr, filename, extensions);

	dir = AssetPath(this->type, dirstr);
}

bool AssetPath::IsInsideOfDirectory(const AssetPath& dir) const
{
	return vcpp::filesystem::is_insideof_dir(path, dir.path);
}

std::string AssetPath::GetFileSystemAbsolutePath() const
{
	if (type == AST_AbsolutePath)
	{
		return vcpp::filesystem::path_make_absolute(path);
	}
	else if (type == AST_ProjectPath)
	{
		AssetPath projDir = g_lawnEngine.getAssetManager().ProjectDir();
		return vcpp::filesystem::path_make_absolute(this->path, projDir.path);
	}
	else
	{
		CHECK(false && "AssetPath.type not supported");
		return "";
	}
}

AssetPath AssetPath::ToAbsolutePath() const
{
	std::string s = AssetPath::GetFileSystemAbsolutePath();
	return AssetPath(AST_AbsolutePath, s);
}

AssetPath AssetPath::ToProjectPath() const
{
	if (type == AST_AbsolutePath)
	{
		AssetPath projDir = g_lawnEngine.getAssetManager().ProjectDir();

		std::string relativePath;
		if (vcpp::filesystem::get_relative_path(GetFileSystemAbsolutePath(), projDir.GetFileSystemAbsolutePath(), relativePath))
			return AssetPath(AssetPathType::AST_ProjectPath, relativePath);
		else
			return AssetPath();
	}
	else if (type == AST_ProjectPath)
	{
		return *this;
	}
	else
	{
		CHECK(false && "AssetPath.type not supported");
		return AssetPath();
	}
}

AssetPath AssetPath::Join(const std::string& path) const
{
	std::string newpath = vcpp::filesystem::path_join(this->path, path);
	return AssetPath(this->type, newpath);
}

AssetPath AssetPath::Join(const AssetPath& path) const
{
	std::string newpath = vcpp::filesystem::path_join(this->path, path.path);
	return AssetPath(this->type, newpath);
}

double AssetPath::GetLastWriteTime() const
{
	return GetFileSystem().GetLastWriteTime(GetFileSystemAbsolutePath());
}

bool AssetPath::Exists() const
{
	return GetFileSystem().Exists(GetFileSystemAbsolutePath());
}

bool AssetPath::Delete() const
{
	return GetFileSystem().Delete(GetFileSystemAbsolutePath());
}

bool AssetPath::CreateDirectory() const
{
	return GetFileSystem().CreateDirectory(GetFileSystemAbsolutePath());
}

bool AssetPath::EnsureDirectoryExists() const
{
	if (Exists())
	{
		if (!IsDirectory())
		{
			Log::Error("EnsureDirectoryExists failed, it is a file, not a dir: %s", ToString().c_str());
			return false;
		}
	}
	else
	{
		if (!CreateDirectory())
		{
			Log::Error("AssetManager::ImportNewAsset, can not create dir: %s", ToString().c_str());
			return false;
		}
	}

	return true;
}

bool AssetPath::IsDirectory() const
{
	return GetFileSystem().IsDirectory(GetFileSystemAbsolutePath());
}

bool AssetPath::IsFile() const
{
	return GetFileSystem().IsFile(GetFileSystemAbsolutePath());
}

void AssetPath::List(std::vector<AssetPath>& outChildPaths, bool resursive /*= true*/) const
{
	std::vector<std::string> outChildPathStrs;
	List(outChildPathStrs, resursive);

	for (const std::string& s : outChildPathStrs)
	{
		AssetPath absPath = AssetPath(AST_AbsolutePath, s);
		
		if (this->type == AST_AbsolutePath)
			outChildPaths.push_back(absPath);
		else if (this->type == AST_ProjectPath)
			outChildPaths.push_back(absPath.ToProjectPath());
		else
			CHECK(false);
	}
}

// returns absolute file-system paths
void AssetPath::List(std::vector<std::string>& outChildPaths, bool resursive /* = true*/) const
{
	if (resursive)
		GetFileSystem().ListRecursive(GetFileSystemAbsolutePath(), outChildPaths);
	else
		GetFileSystem().List(GetFileSystemAbsolutePath(), outChildPaths);
}

AssetPath AssetPath::FindUsablePath(const AssetPath& dir, const std::string& filename, const std::string& ext)
{
	std::string filenameWithIndex = formatString("%s%s", filename.c_str(), ext.c_str());
	AssetPath path = dir / filenameWithIndex;

	for (uint i = 1; path.Exists(); i++)
	{
		filenameWithIndex = formatString("%s_%d%s", filename.c_str(), i, ext.c_str());
		path = dir / filenameWithIndex;
	}

	return path;
}

std::unique_ptr<ReadStream> AssetPath::OpenRead(bool text /*= false*/) const
{
	if (type == AST_ProjectPath)
	{
		AssetPath finalPath = ToAbsolutePath();
		return std::unique_ptr<ReadStream>(new FileReadStream(finalPath.path.c_str(), text));
	}
	else if (type == AST_AbsolutePath)
	{
		return std::unique_ptr<ReadStream>(new FileReadStream(this->path.c_str(), text));
	}
	else
	{
		return std::unique_ptr<ReadStream>();
	}
}

std::unique_ptr<WriteStream> AssetPath::OpenWrite(bool text /*= false*/) const
{
	if (type == AST_ProjectPath)
	{
		AssetPath finalPath = ToAbsolutePath();
		return std::unique_ptr<WriteStream>(new FileWriteStream(finalPath.path.c_str(), text, true));
	}
	else if (type == AST_AbsolutePath)
	{
		return std::unique_ptr<WriteStream>(new FileWriteStream(this->path.c_str(), text, true));
	}
	else
	{
		return std::unique_ptr<WriteStream>();
	}
}

std::string AssetPath::ToString() const
{
	if (!IsValid())
		return "";
	else if (type == AST_AbsolutePath)
		return "file://" + path;
	else if (type == AST_ProjectPath)
		return "proj://" + path;
	else
		return "unknown://";
}

AssetPath AssetPath::FromString(const std::string& s)
{
	if (s.size() == 0)
	{
		return AssetPath();
	}

	std::string separator = "://";

	auto separatorPos = s.find(separator);
	if (separatorPos != std::string::npos)
	{
		AssetPathType type;

		std::string typePart = stringToLower(s.substr(0, separatorPos));
		if (typePart == "file")
		{
			type = AST_AbsolutePath;
		}
		else if (typePart == "proj")
		{
			type = AST_ProjectPath;
		}
		else
		{
			return AssetPath();
		}

		std::string pathPart = s.substr(separatorPos + separator.size());

		return AssetPath(type, pathPart);
	}
	else
	{
		return AssetPath();
	}
}
