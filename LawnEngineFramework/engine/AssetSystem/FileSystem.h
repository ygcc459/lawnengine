#pragma once

#include <string>
#include <vector>

class FileSystem
{
public:
	bool Exists(const std::string& path);
	bool Delete(const std::string& path);

	bool CreateDirectory(const std::string& path);
	bool IsDirectory(const std::string& path);
	bool IsFile(const std::string& path);

	void List(const std::string& path, std::vector<std::string>& outChildPaths);
	void ListRecursive(const std::string& path, std::vector<std::string>& outChildPaths);

	std::string GetCurrentPath();
	void SetCurrentPath(const std::string& path);

	// return time in seconds since epoch
	double GetLastWriteTime(const std::string& path);
	
	static FileSystem& Default();
};
