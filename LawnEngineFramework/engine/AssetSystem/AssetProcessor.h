#pragma once

#include "Asset.h"
#include "AssetProcessorOutput.h"

class AssetProcessor : public Asset
{
	DECL_ABSTRACT_CLASS_TYPE(AssetProcessor, Asset);

public:
	AssetProcessor()
	{}

	virtual ~AssetProcessor()
	{}

	virtual void Reflect(Reflector& reflector) override;

	virtual bool DoProcess(const std::vector<AssetPath>& inputAssetPaths, AssetProcessorOutput* processorOutput) = 0;
};

typedef std::shared_ptr<AssetProcessor> AssetProcessorPtr;
