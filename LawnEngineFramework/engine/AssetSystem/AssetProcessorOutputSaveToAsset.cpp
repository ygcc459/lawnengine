#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "AssetProcessorOutputSaveToAsset.h"

AssetProcessorOutputSaveToAsset::AssetProcessorOutputSaveToAsset(const AssetPath& processorPath, const AssetPath& outputPath)
	: processorPath(processorPath), outputPath(outputPath)
{
	lastImportRecord = g_lawnEngine.getAssetManager().ImportDatabase()->FindImportRecordByProcessorPath(processorPath);
}

AssetProcessorOutputSaveToAsset::~AssetProcessorOutputSaveToAsset()
{

}

BaseObjectPtr AssetProcessorOutputSaveToAsset::CreateOutput(const std::string& key, Type* type, bool overwrite /*= true*/, bool isMainOutput /*= false*/)
{
	if (isMainOutput)
	{
		mainOutputName = key;
	}

	if (outputObjects.find(key) != outputObjects.end())
	{
		Log::Error("Error, processor output with name '%s' already created", key.c_str());
		return BaseObjectPtr();
	}

	if (lastImportRecord)
	{
		auto it = lastImportRecord->outputAssetPaths.find(key);
		if (it != lastImportRecord->outputAssetPaths.end())
		{
			AssetPath path = it->second;

			if (overwrite)
			{
				g_lawnEngine.getAssetManager().Delete(path);
			}
			else
			{
				BaseObjectPtr ptr = g_lawnEngine.getAssetManager().LoadRaw(path);
				if (ptr)
				{
					if (ptr->GetType()->IsSameOrChildOf(type))
					{
						return ptr;
					}
					else
					{
						Log::Error("Error, processor output with name '%s' already exists, its path is '%s', but type is '%s' instead of '%s'", key.c_str(), path.ToString().c_str(), ptr->GetType()->TypeName().c_str(), type->TypeName().c_str());
						return BaseObjectPtr();
					}
				}
			}
		}
	}
	
	std::string newAssetName = isMainOutput ? outputPath.Filename() : formatString("%s_%s", outputPath.Filename().c_str(), key.c_str());
	std::string newAssetExt = formatString(".%s", type->TypeName().c_str());

	AssetPath newAssetPath = AssetPath::FindUsablePath(outputPath.Parent(), newAssetName, newAssetExt);

	BaseObjectPtr ptr = g_lawnEngine.getAssetManager().CreateRaw(newAssetPath, type);
	
	outputObjects.insert(std::make_pair(key, ptr));

	return ptr;
}

BaseObjectPtr AssetProcessorOutputSaveToAsset::GetOutput(const std::string& key)
{
	auto it = outputObjects.find(key);
	if (it != outputObjects.end())
		return it->second;
	else
		return BaseObjectPtr();
}

void AssetProcessorOutputSaveToAsset::GetOutputs(std::map<std::string, BaseObjectPtr>& outputs)
{
	outputs = this->outputObjects;
}

BaseObjectPtr AssetProcessorOutputSaveToAsset::GetMainOutput()
{
	return outputObjects[mainOutputName];
}

void AssetProcessorOutputSaveToAsset::SetAsMainOutput(const std::string& key)
{
	mainOutputName = key;
}
