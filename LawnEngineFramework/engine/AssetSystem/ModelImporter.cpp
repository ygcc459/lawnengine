#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "ModelImporter.h"

#include "EntitySystem/MeshRenderer.h"
#include "EntitySystem/SceneObject.h"
#include "EntitySystem/Transform.h"
#include "RenderSystem/Texture.h"

#include "Streams.h"

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

IMPL_CLASS_TYPE(ImportModelProcessor);

ImportModelProcessor::ImportModelProcessor()
: CalcTangentSpace(true)
, JoinIdenticalVertices(true)
, MakeLeftHanded(true)
, Triangulate(true)
, RemoveComponent(false)
, GenNormals(false)
, GenSmoothNormals(true)
, SplitLargeMeshes(false)
, PreTransformVertices(false)
, LimitBoneWeights(false)
, ValidateDataStructure(false)
, ImproveCacheLocality(true)
, RemoveRedundantMaterials(false)
, FixInfacingNormals(false)
, PopulateArmatureData(false)
, SortByPType(false)
, FindDegenerates(false)
, FindInvalidData(false)
, GenUVCoords(false)
, TransformUVCoords(false)
, FindInstances(false)
, OptimizeMeshes(false)
, OptimizeGraph(false)
, FlipUVs(true)
, FlipWindingOrder(false)
, SplitByBoneCount(false)
, Debone(false)
, RotationEulerAngles()
, Scale(0.01f, 0.01f, 0.01f)
, CreateSceneObjects(true)
, CreateMeshes(true)
, CreateMaterials(true)
{

}

ImportModelProcessor::~ImportModelProcessor()
{

}

void ImportModelProcessor::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);

	reflector.AddMember("CalcTangentSpace", &Self::CalcTangentSpace);
	reflector.AddMember("JoinIdenticalVertices", &Self::JoinIdenticalVertices);
	reflector.AddMember("MakeLeftHanded", &Self::MakeLeftHanded);
	reflector.AddMember("Triangulate", &Self::Triangulate);
	reflector.AddMember("RemoveComponent", &Self::RemoveComponent);
	reflector.AddMember("GenNormals", &Self::GenNormals);
	reflector.AddMember("GenSmoothNormals", &Self::GenSmoothNormals);
	reflector.AddMember("SplitLargeMeshes", &Self::SplitLargeMeshes);
	reflector.AddMember("PreTransformVertices", &Self::PreTransformVertices);
	reflector.AddMember("LimitBoneWeights", &Self::LimitBoneWeights);
	reflector.AddMember("ValidateDataStructure", &Self::ValidateDataStructure);
	reflector.AddMember("ImproveCacheLocality", &Self::ImproveCacheLocality);
	reflector.AddMember("RemoveRedundantMaterials", &Self::RemoveRedundantMaterials);
	reflector.AddMember("FixInfacingNormals", &Self::FixInfacingNormals);
	reflector.AddMember("PopulateArmatureData", &Self::PopulateArmatureData);
	reflector.AddMember("SortByPType", &Self::SortByPType);
	reflector.AddMember("FindDegenerates", &Self::FindDegenerates);
	reflector.AddMember("FindInvalidData", &Self::FindInvalidData);
	reflector.AddMember("GenUVCoords", &Self::GenUVCoords);
	reflector.AddMember("TransformUVCoords", &Self::TransformUVCoords);
	reflector.AddMember("FindInstances", &Self::FindInstances);
	reflector.AddMember("OptimizeMeshes", &Self::OptimizeMeshes);
	reflector.AddMember("OptimizeGraph", &Self::OptimizeGraph);
	reflector.AddMember("FlipUVs", &Self::FlipUVs);
	reflector.AddMember("FlipWindingOrder", &Self::FlipWindingOrder);
	reflector.AddMember("SplitByBoneCount", &Self::SplitByBoneCount);
	reflector.AddMember("Debone", &Self::Debone);

	reflector.AddMember("RotationEulerAngles", &Self::RotationEulerAngles);
	reflector.AddMember("Scale", &Self::Scale);
}

uint ImportModelProcessor::CalculateImportFlags() const
{
	uint assimp_flags = 0;
	assimp_flags |= CalcTangentSpace ? aiProcess_CalcTangentSpace : 0;
	assimp_flags |= JoinIdenticalVertices ? aiProcess_JoinIdenticalVertices : 0;
	assimp_flags |= MakeLeftHanded ? aiProcess_MakeLeftHanded : 0;
	assimp_flags |= Triangulate ? aiProcess_Triangulate : 0;
	assimp_flags |= RemoveComponent ? aiProcess_RemoveComponent : 0;
	assimp_flags |= GenNormals ? aiProcess_GenNormals : 0;
	assimp_flags |= GenSmoothNormals ? aiProcess_GenSmoothNormals : 0;
	assimp_flags |= SplitLargeMeshes ? aiProcess_SplitLargeMeshes : 0;
	assimp_flags |= PreTransformVertices ? aiProcess_PreTransformVertices : 0;
	assimp_flags |= LimitBoneWeights ? aiProcess_LimitBoneWeights : 0;
	assimp_flags |= ValidateDataStructure ? aiProcess_ValidateDataStructure : 0;
	assimp_flags |= ImproveCacheLocality ? aiProcess_ImproveCacheLocality : 0;
	assimp_flags |= RemoveRedundantMaterials ? aiProcess_RemoveRedundantMaterials : 0;
	assimp_flags |= FixInfacingNormals ? aiProcess_FixInfacingNormals : 0;
	//assimp_flags |= PopulateArmatureData ? aiProcess_PopulateArmatureData : 0;
	assimp_flags |= SortByPType ? aiProcess_SortByPType : 0;
	assimp_flags |= FindDegenerates ? aiProcess_FindDegenerates : 0;
	assimp_flags |= FindInvalidData ? aiProcess_FindInvalidData : 0;
	assimp_flags |= GenUVCoords ? aiProcess_GenUVCoords : 0;
	assimp_flags |= TransformUVCoords ? aiProcess_TransformUVCoords : 0;
	assimp_flags |= FindInstances ? aiProcess_FindInstances : 0;
	assimp_flags |= OptimizeMeshes ? aiProcess_OptimizeMeshes : 0;
	assimp_flags |= OptimizeGraph ? aiProcess_OptimizeGraph : 0;
	assimp_flags |= FlipUVs ? aiProcess_FlipUVs : 0;
	assimp_flags |= FlipWindingOrder ? aiProcess_FlipWindingOrder : 0;
	assimp_flags |= SplitByBoneCount ? aiProcess_SplitByBoneCount : 0;
	assimp_flags |= Debone ? aiProcess_Debone : 0;
	return assimp_flags;
}

bool ImportModelProcessor::DoProcess(const std::vector<AssetPath>& inputAssetPaths, AssetProcessorOutput* processorOutput)
{
	this->processorOutput = processorOutput;

	if (inputAssetPaths.size() != 1)
	{
		Log::Error("ImportModelProcessor: input assets must be one");
		return false;
	}

	AssetPath inputAssetPath = inputAssetPaths[0];

	Log::Info("ImportModelProcessor: processing '%s'", inputAssetPath.ToString().c_str());

	std::unique_ptr<ReadStream> inputAssetStream = inputAssetPath.OpenRead();
	if (!inputAssetStream->CanRead())
	{
		Log::Error("ImportModelProcessor: can not read asset");
		return false;
	}

	std::vector<uint8> inputAssetBuffer;
	if (!inputAssetStream->ReadAllData(inputAssetBuffer))
	{
		Log::Error("ImportModelProcessor: read from stream failed");
		return false;
	}

	// Create an instance of the Importer class
	Assimp::Importer importer;

	// And have it read the given file with some example postprocessing   
	// Usually - if speed is not the most important aspect for you - you'll    
	// propably to request more postprocessing than we do in this example.
	uint assimp_flags = CalculateImportFlags();

	const aiScene* scene = importer.ReadFileFromMemory(inputAssetBuffer.data(), inputAssetBuffer.size(), assimp_flags, inputAssetPath.ToString().c_str());

	if (!scene)
	{
		Log::Error("ImportModelProcessor: import failed: %s", importer.GetErrorString());
		return false;
	}

	if (scene->HasMaterials())
	{
		for (uint iMaterial = 0; iMaterial < scene->mNumMaterials; iMaterial++)
		{
			aiMaterial* srcmat = scene->mMaterials[iMaterial];

			MaterialPtr mat = CreateMaterials ? ConvertMaterial(srcmat) : nullptr;

			convertedMaterials.push_back(mat);
		}
	}

	if (scene->HasMeshes())
	{
		for (uint i = 0; i < scene->mNumMeshes; i++)
		{
			aiMesh* srcmesh = scene->mMeshes[i];

			MeshPtr mesh = CreateMeshes ? ConvertMesh(srcmesh) : nullptr;

			convertedMeshes.push_back(mesh);
		}
	}

	if (CreateSceneObjects)
	{
		SceneObjectPtr rootSceneObject = processorOutput->CreateMainOutput<SceneObject>("main");
		rootSceneObject->Startup(true);

		rootSceneObject->transform->SetLocalRotation(this->RotationEulerAngles);
		rootSceneObject->transform->SetLocalScale(this->Scale);

		ConvertSceneNode(scene, scene->mRootNode, rootSceneObject.get());

		return true;
	}
	else
	{
		return true;
	}
}

void ImportModelProcessor::ConvertSceneNode(const aiScene* scene, const aiNode* node, SceneObject* parentSceneObject)
{
	SceneObject* sceneObject = new SceneObject();
	sceneObject->Startup(true);

	sceneObject->name = node->mName.C_Str();
	sceneObject->SetParent(parentSceneObject);

	aiVector3t<float> scaling, position;
	aiQuaterniont<float> rotation;
	node->mTransformation.Decompose(scaling, rotation, position);
	
	sceneObject->GetTransform().SetLocalPosition(float3(position.x, position.y, position.z));
	sceneObject->GetTransform().SetLocalRotation(quaternionf(rotation.x, rotation.y, rotation.z, rotation.w));
	sceneObject->GetTransform().SetLocalScale(float3(scaling.x, scaling.y, scaling.z));

	if (node->mNumMeshes == 1)
	{
		uint mesh_index = node->mMeshes[0];
		aiMesh* node_mesh = scene->mMeshes[mesh_index];

		MeshPtr mesh = convertedMeshes[mesh_index];
		MaterialPtr material = convertedMaterials[node_mesh->mMaterialIndex];

		MeshRenderer* renderer = sceneObject->AddComponent<MeshRenderer>();
		renderer->SetMesh(mesh);
		renderer->SetMaterial(material);
	}
	else if (node->mNumMeshes > 1)
	{
		for (uint iMesh = 0; iMesh < node->mNumMeshes; iMesh++)
		{
			uint mesh_index = node->mMeshes[iMesh];
			aiMesh* node_mesh = scene->mMeshes[mesh_index];

			MeshPtr mesh = convertedMeshes[mesh_index];
			MaterialPtr material = convertedMaterials[node_mesh->mMaterialIndex];

			MeshRenderer* renderer = sceneObject->AddComponent<MeshRenderer>();
			renderer->SetMesh(mesh);
			renderer->SetMaterial(material);
		}
	}

	if (node->mNumChildren > 0)
	{
		for (uint iChild = 0; iChild < node->mNumChildren; iChild++)
		{
			aiNode* subnode = node->mChildren[iChild];
			
			ConvertSceneNode(scene, subnode, sceneObject);
		}
	}
}

MeshPtr ImportModelProcessor::ConvertMesh(const aiMesh* srcmesh)
{
	if (!(srcmesh->mPrimitiveTypes & aiPrimitiveType_TRIANGLE))
	{
		Log::Error("ImportModelProcessor can not convert non-triangle mesh '%s'", srcmesh->mName.C_Str());
		return MeshPtr();
	}

	std::string mesh_name(srcmesh->mName.C_Str());
	mesh_name = processorOutput->FindUsableKey(mesh_name);

	MeshPtr mesh = processorOutput->CreateOutput<Mesh>(mesh_name);

	MeshDataWrittablePtr meshDataWrittable = mesh->BeginModify();

	meshDataWrittable->SetPrimitiveTopology(PrimitiveTopology::PT_TriangleList);
	meshDataWrittable->SetVertexCount(srcmesh->mNumVertices);

	uint used_texcoord_count = 0;
	while (used_texcoord_count < AI_MAX_NUMBER_OF_TEXTURECOORDS && srcmesh->HasTextureCoords(used_texcoord_count))
		used_texcoord_count++;

	//
	// vertex components
	//
	int vertexBufferIndex = meshDataWrittable->AddVertexBuffer();

	int vertexOffset = 0;
	if (srcmesh->HasPositions() && srcmesh->mVertices)
	{
		meshDataWrittable->AddVertexComponent(IE_POSITION, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 3), vertexOffset);
		vertexOffset += sizeof(float3);
	}
	if (srcmesh->HasNormals() && srcmesh->mNormals)
	{
		meshDataWrittable->AddVertexComponent(IE_NORMAL, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 3), vertexOffset);
		vertexOffset += sizeof(float3);
	}
	if (srcmesh->HasTangentsAndBitangents() && srcmesh->mTangents && srcmesh->mBitangents)
	{
		meshDataWrittable->AddVertexComponent(IE_TANGENT, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 4), vertexOffset);
		vertexOffset += sizeof(float4);

		//meshDataWrittable->AddVertexComponent(IE_BITANGENT, vertexBufferIndex, MeshVertexFormat(MVBF_Float, 3), vertexOffset);
		//vertexOffset += sizeof(float3);
	}
	for (uint j = 0; j < used_texcoord_count; j++)
	{
		meshDataWrittable->AddVertexComponent((InputElement)(IE_TEXCOORD0 + j), vertexBufferIndex, MeshVertexFormat(MVBF_Float, 2), vertexOffset);
		vertexOffset += sizeof(float2);
	}

	//
	// vertex buffer
	//
	int vertexStride = vertexOffset;

	meshDataWrittable->SetVertexBufferStride(vertexBufferIndex, vertexStride);
	meshDataWrittable->UpdateVertexBuffer(vertexBufferIndex, vertexStride * srcmesh->mNumVertices, nullptr);

	if (srcmesh->HasPositions() && srcmesh->mVertices)
	{
		meshDataWrittable->UpdateVertexComponent(meshDataWrittable->FindVertexComponent(IE_POSITION), vcpp::StridedArrayView<float3>(srcmesh->mVertices, srcmesh->mNumVertices, 0, sizeof(float3)));
	}
	if (srcmesh->HasNormals() && srcmesh->mNormals)
	{
		meshDataWrittable->UpdateVertexComponent(meshDataWrittable->FindVertexComponent(IE_NORMAL), vcpp::StridedArrayView<float3>(srcmesh->mNormals, srcmesh->mNumVertices, 0, sizeof(float3)));
	}
	if (srcmesh->HasTangentsAndBitangents() && srcmesh->mTangents && srcmesh->mBitangents)
	{
		CHECK(srcmesh->HasNormals() && srcmesh->mNormals);

		//
		// Append tangentSign of tangent basis determinant
		// binormal = cross(normal, tangent) * tangentSign;
		//
		std::vector<float4> signedTangents;
		signedTangents.resize(srcmesh->mNumVertices);
		for (int i = 0; i < srcmesh->mNumVertices; i++)
		{
			float3 T = *reinterpret_cast<float3*>(srcmesh->mTangents + i);
			float3 B = *reinterpret_cast<float3*>(srcmesh->mBitangents + i);
			float3 N = *reinterpret_cast<float3*>(srcmesh->mNormals + i);

			float tangentSign = (dot(B, cross(N, T)) >= 0) ? 1 : -1;

			signedTangents[i] = float4(T, tangentSign);
		}

		meshDataWrittable->UpdateVertexComponent(meshDataWrittable->FindVertexComponent(IE_TANGENT), vcpp::StridedArrayView<float4>(signedTangents.data(), signedTangents.size(), 0, sizeof(float4)));
		//meshDataWrittable->UpdateVertexComponent(meshDataWrittable->FindVertexComponent(IE_BITANGENT), vcpp::StridedArrayView<float3>(srcmesh->mBitangents, srcmesh->mNumVertices, 0, sizeof(float3)));
	}
	for (uint j = 0; j < used_texcoord_count; j++)
	{
		//
		// Convert 3D uv to 2D
		//
		std::vector<float2> UVs;
		UVs.resize(srcmesh->mNumVertices);
		for (int i = 0; i < srcmesh->mNumVertices; i++)
		{
			float3 OriginalUV = *reinterpret_cast<float3*>(srcmesh->mTextureCoords[j] + i);
			UVs[i] = float2(OriginalUV.x, OriginalUV.y);
		}

		meshDataWrittable->UpdateVertexComponent(meshDataWrittable->FindVertexComponent((InputElement)(IE_TEXCOORD0 + j)), vcpp::StridedArrayView<float2>(UVs.data(), UVs.size(), 0, sizeof(float2)));
	}

	//
	// index buffer
	//
	bool useLargeIndexBuffer = (srcmesh->mNumFaces * 3 >= 65536);

	if (useLargeIndexBuffer)
	{
		std::vector<uint> indicies;
		indicies.reserve(srcmesh->mNumFaces * 3);

		for (uint j = 0; j < srcmesh->mNumFaces; j++)
		{
			indicies.push_back(srcmesh->mFaces[j].mIndices[0]);
			indicies.push_back(srcmesh->mFaces[j].mIndices[1]);
			indicies.push_back(srcmesh->mFaces[j].mIndices[2]);
		}

		meshDataWrittable->UpdateIndexBuffer(indicies);
	}
	else
	{
		std::vector<uint16> indicies;
		indicies.reserve(srcmesh->mNumFaces * 3);

		for (uint j = 0; j < srcmesh->mNumFaces; j++)
		{
			indicies.push_back(srcmesh->mFaces[j].mIndices[0]);
			indicies.push_back(srcmesh->mFaces[j].mIndices[1]);
			indicies.push_back(srcmesh->mFaces[j].mIndices[2]);
		}

		meshDataWrittable->UpdateIndexBuffer(indicies);
	}

	mesh->EndModify();

	return mesh;
}

// contains only lower-case suffix
const std::map<std::string, FixedString>& GetSuffixToTexPropertyNameMapping()
{
	static std::map<std::string, FixedString> suffixToTexPropertyNameMapping;

	if (suffixToTexPropertyNameMapping.size() == 0)
	{
		suffixToTexPropertyNameMapping.insert(std::make_pair("_d", "BaseColorTex"));
		suffixToTexPropertyNameMapping.insert(std::make_pair("_basecolor", "BaseColorTex"));
		suffixToTexPropertyNameMapping.insert(std::make_pair("_albedo", "BaseColorTex"));
		suffixToTexPropertyNameMapping.insert(std::make_pair("_diffuse", "BaseColorTex"));
		suffixToTexPropertyNameMapping.insert(std::make_pair("_diffusecolor", "BaseColorTex"));

		suffixToTexPropertyNameMapping.insert(std::make_pair("_n", "NormalTex"));
		suffixToTexPropertyNameMapping.insert(std::make_pair("_normal", "NormalTex"));

		suffixToTexPropertyNameMapping.insert(std::make_pair("_m", "RoughnessMetallicAOTex"));
		suffixToTexPropertyNameMapping.insert(std::make_pair("_rma", "RoughnessMetallicAOTex"));
		suffixToTexPropertyNameMapping.insert(std::make_pair("_roughnessmetallicao", "RoughnessMetallicAOTex"));
	}

	return suffixToTexPropertyNameMapping;
}

MaterialPtr ImportModelProcessor::ConvertMaterial(const aiMaterial* srcmat)
{
	aiString name;
	srcmat->Get(AI_MATKEY_NAME, name);

	ShaderPtr shader = g_lawnEngine.getAssetManager().Load<Shader>(AssetPath(AssetPathType::AST_ProjectPath, "Shaders/PBRStandard.shader"));

	MaterialPtr material = processorOutput->CreateOutput<Material>(name.C_Str());
	material->Reset();

	material->BindShader(shader);

	aiColor3D diffuseColor;
	if (srcmat->Get(AI_MATKEY_COLOR_DIFFUSE, diffuseColor) == aiReturn_SUCCESS)
	{
		material->SetFloat3("BaseColor", float3(diffuseColor.r, diffuseColor.g, diffuseColor.b));
	}

	material->SetFloat("Roughness", 1.0f);
	material->SetFloat("Specular", 1.0f);
	material->SetFloat("Metallic", 1.0f);

	//
	// auto-match texture for material texture properties
	//
	AssetPath materialPath = material->assetPath;
	std::string materialFilename = materialPath.Filename();

	std::vector<AssetPath> files;
	materialPath.Parent().List(files, false);

	const std::map<std::string, FixedString>& suffixToTexPropertyNameMapping = GetSuffixToTexPropertyNameMapping();

	for (const AssetPath& f : files)
	{
		std::string filenameWithoutExt = f.Filename();

		// format: {material-file-name}{suffix}
		if (filenameWithoutExt.find(materialFilename) == 0 && filenameWithoutExt.size() > materialFilename.size())
		{
			std::string suffix = stringToLower(filenameWithoutExt.substr(materialFilename.size()));

			auto it = suffixToTexPropertyNameMapping.find(suffix);
			if (it != suffixToTexPropertyNameMapping.end())
			{
				FixedString texPropertyName = it->second;

				TexturePtr texture = Util::CastPtr<Texture>(g_lawnEngine.getAssetManager().LoadRaw(f));
				if (texture)
				{
					Log::Info("Auto matched texture '%s' for property '%s' in material '%s'", f.ToString().c_str(), texPropertyName.c_str(), materialPath.ToString().c_str());

					material->SetResource(texPropertyName, texture);
				}
			}
		}
	}

	return material;
}
