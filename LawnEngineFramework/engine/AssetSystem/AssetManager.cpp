#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "AssetManager.h"
#include "AssetSystem/AssetProcessorOutputSaveToAsset.h"
#include "AssetSystem/AssetProcessorOutputInMemory.h"
#include "AssetSystem/ModelImporter.h"
#include "AssetSystem/TextureImporter.h"

#include "../Streams.h"

IMPL_PRIMITIVE_TYPE(AssetImportRecord, AssetImportRecordType);

//////////////////////////////////////////////////////////////////////////

bool AssetImportRecord::IsInputsOrProcessorUpdatedSinceLastImport() const
{
	for (const AssetPath& path : inputAssetPaths)
	{
		if (path.GetLastWriteTime() > this->lastImportTime)
		{
			return true;
		}
	}

	if (processorPath.GetLastWriteTime() > this->lastImportTime)
	{
		return true;
	}

	return false;
}

bool AssetImportRecord::IsAllInputsExist() const
{
	for (const AssetPath& path : inputAssetPaths)
	{
		if (!path.Exists())
		{
			return false;
		}
	}

	return true;
}

bool AssetImportRecord::IsProcessorExist() const
{
	return processorPath.Exists();
}

double AssetImportRecord::CalcLastWriteTime() const
{
	double maxImportTime = -1;

	CHECK(processorPath.Exists());
	maxImportTime = Max(maxImportTime, processorPath.GetLastWriteTime());

	for (auto& path : inputAssetPaths)
	{
		CHECK(path.Exists());
		maxImportTime = Max(maxImportTime, path.GetLastWriteTime());
	}

	return maxImportTime;
}

//////////////////////////////////////////////////////////////////////////

IMPL_CLASS_TYPE(AssetImportDatabase);

AssetImportDatabase::AssetImportDatabase()
{
}

AssetImportDatabase::~AssetImportDatabase()
{
}

AssetPath AssetImportDatabase::GetConfigPath() const
{
	return g_lawnEngine.getAssetManager().ProjectDir() / "AssetImportDatabase";
}

void AssetImportDatabase::Reflect(Reflector& reflector)
{
	Super::Reflect(reflector);
	reflector.AddMember("importRecords", &Self::importRecords);
}

void AssetImportDatabase::Load()
{
	Util::DeserializeFromAsset(GetConfigPath(), *this);
}

void AssetImportDatabase::Save()
{
	Util::SerializeToAsset(*this, GetConfigPath());
}

void AssetImportDatabase::RemoveInvalidRecords(bool removeIfMissingInputs /*= false*/)
{
	std::vector<AssetImportRecordPtr> validRecords;

	for (AssetImportRecordPtr record : importRecords)
	{
		bool valid = true;

		if (removeIfMissingInputs)
		{
			for (AssetPath path : record->inputAssetPaths)
			{
				if (!path.Exists())
				{
					valid = false;
					break;
				}
			}
		}

		valid &= record->processorPath.Exists();
		
		if (valid)
		{
			validRecords.push_back(record);
		}
	}

	importRecords.swap(validRecords);
}

AssetImportRecordPtr AssetImportDatabase::CreateOrUpdateImportRecord(AssetPath processorPath, AssetPath outputPath, const std::vector<AssetPath>& inputs, const std::map<std::string, BaseObjectPtr>& outputs)
{
	AssetImportRecordPtr ptr = FindImportRecordByProcessorPath(processorPath);
	if (ptr == nullptr)
		ptr.reset(new AssetImportRecord());

	ptr->inputAssetPaths = inputs;

	ptr->outputAssetPaths.clear();
	for (auto it = outputs.begin(); it != outputs.end(); ++it)
	{
		std::string key = it->first;
		AssetPath path = it->second->GetAssetPath();
		ptr->outputAssetPaths.insert(std::make_pair(key, path));
	}

	ptr->processorPath = processorPath;
	ptr->outputPath = outputPath;
	ptr->lastImportTime = ptr->CalcLastWriteTime();

	importRecords.push_back(ptr);

	return ptr;
}

AssetImportRecordPtr AssetImportDatabase::FindImportRecordByInput(const AssetPath& path)
{
	for (AssetImportRecordPtr record : importRecords)
	{
		for (AssetPath p : record->inputAssetPaths)
		{
			if (p == path)
			{
				return record;
			}
		}
	}

	return nullptr;
}

AssetImportRecordPtr AssetImportDatabase::FindImportRecordByProcessorPath(const AssetPath& processorPath)
{
	for (AssetImportRecordPtr record : importRecords)
	{
		if (record->processorPath == processorPath)
		{
			return record;
		}
	}

	return nullptr;
}

//////////////////////////////////////////////////////////////////////////

AssetManager::AssetManager()
{
	importDatabase = new AssetImportDatabase();

	RegisterSimpleProcessorType(&ImportModelProcessor::StaticType, { ".fbx", ".obj" });
	RegisterSimpleProcessorType(&TextureImporter::StaticType, { ".png", ".jpg", ".tga", ".exr", ".hdr" });
}

AssetManager::~AssetManager()
{
	SAFE_DELETE(importDatabase);
}

void AssetManager::SetCommonDirectory(CommonDirectories cd, const AssetPath& path)
{
	commonDirectoryMap[cd] = path;
}

AssetPath AssetManager::GetCommonDirectory(CommonDirectories cd) const
{
	auto it = commonDirectoryMap.find(cd);
	if (it != commonDirectoryMap.end())
		return it->second;
	else
		return AssetPath();
}

std::shared_ptr<BaseObject> AssetManager::CreateRaw(const AssetPath& path, Type* type)
{
	auto it = loadedResources.find(path);
	if (it != loadedResources.end())
	{
		Log::Error("AssetManager::Create: asset already loaded: '%s'", path.ToString().c_str());
		return std::shared_ptr<BaseObject>();
	}

	if (Exists(path))
	{
		Log::Error("AssetManager::Create: file already exists: '%s'", path.ToString().c_str());
		return std::shared_ptr<BaseObject>();
	}

	if (!type->IsChildOf(&BaseObject::StaticType))
	{
		Log::Error("AssetManager::Create: Type %s must be a child of BaseObject", type->TypeName().c_str());
		return std::shared_ptr<BaseObject>();
	}

	BaseObject* rawPtr = (BaseObject*)type->CreateInstance();
	if (!rawPtr)
	{
		return std::shared_ptr<BaseObject>();
	}

	std::shared_ptr<BaseObject> ptr(rawPtr);
	ptr->SetAssetPath(path);
	ptr->OnAwake();

	std::weak_ptr<BaseObject> weakPtr(ptr);

	loadedResources.insert(std::make_pair(path, weakPtr));

	return ptr;
}

bool AssetManager::SaveRaw(const std::shared_ptr<BaseObject>& asset)
{
	if (!asset)
	{
		Log::Error("AssetManager::Save: can not save null asset");
		return false;
	}

	if (!asset->IsAsset())
	{
		Log::Error("AssetManager::Save: asset must created as an asset");
		return false;
	}

	const AssetPath path = asset->GetAssetPath();

	AssetPath dir = path.Parent();
	dir.EnsureDirectoryExists();

	std::unique_ptr<WriteStream> stream = path.OpenWrite();
	if (!stream->CanWrite())
	{
		Log::Error("AssetManager::Save: can not write file: '%s'", path.ToString().c_str());
		return false;
	}

	Type* type = asset->GetType();
	if (!type->IsChildOf(&BaseObject::StaticType))
	{
		Log::Error("AssetManager::Save: type '%s' is not a child of BaseObject, when saving '%s'", type->TypeName().c_str(), path.ToString().c_str());
		return false;
	}

	Archive archive;
	archive.CreateNew();

	asset->OnSave(archive);

	bool result = archive.WriteTo(stream.get());
	if (!result)
	{
		Log::Error("AssetManager::Save: can not write file '%s'", path.ToString().c_str());
		return false;
	}

	return true;
}

std::shared_ptr<BaseObject> AssetManager::LoadRaw(const AssetPath& path)
{
	// already loaded?
	auto it = loadedResources.find(path);
	if (it != loadedResources.end())
	{
		std::weak_ptr<BaseObject> assetWeakPtr = it->second;

		std::shared_ptr<BaseObject> assetPtr = assetWeakPtr.lock();
		if (assetPtr)
		{
			return assetPtr;
		}
	}

	// not loaded, do load
	{
		if (!Exists(path))
		{
			Log::Error("AssetManager::Load: file not exist: '%s'", path.ToString().c_str());
			return std::shared_ptr<BaseObject>();
		}

		std::unique_ptr<ReadStream> stream = path.OpenRead();
		if (!stream->CanRead())
		{
			Log::Error("AssetManager::Load: can not read file: '%s'", path.ToString().c_str());
			return std::shared_ptr<BaseObject>();
		}

		Archive archive;
		if (!archive.ReadFrom(stream.get()))
		{
			Log::Error("AssetManager::Load: can not load archive from file: '%s'", path.ToString().c_str());
			return std::shared_ptr<BaseObject>();
		}

		std::string typeName = GetAssetTypeNameFromArchive(archive);
		if (typeName.empty())
		{
			Log::Error("AssetManager::Load: can not get root-object typename from file: '%s'", path.ToString().c_str());
			return std::shared_ptr<BaseObject>();
		}

		Type* type = Type::FindByName(typeName);
		if (type == nullptr)
		{
			Log::Error("AssetManager::Load: can not find type '%s' when loading '%s'", typeName.c_str(), path.ToString().c_str());
			return std::shared_ptr<BaseObject>();
		}

		if (!type->IsChildOf(&BaseObject::StaticType))
		{
			Log::Error("AssetManager::Create: type '%s' is not a child of BaseObject, when loading '%s'", type->TypeName().c_str(), path.ToString().c_str());
			return std::shared_ptr<BaseObject>();
		}

		BaseObject* rawPtr = (BaseObject*)type->CreateInstance();
		if (rawPtr == nullptr)
		{
			Log::Error("AssetManager::Create: type '%s' create instance failed, when loading '%s'", type->TypeName().c_str(), path.ToString().c_str());
			return std::shared_ptr<BaseObject>();
		}

		std::shared_ptr<BaseObject> ptr(rawPtr);
		ptr->SetAssetPath(path);

		rawPtr->OnLoad(archive);

		ptr->OnAwake();

		std::weak_ptr<BaseObject> weakPtr(ptr);
		loadedResources.insert(std::make_pair(path, weakPtr));

		return ptr;
	}
}

std::shared_ptr<BaseObject> AssetManager::GetIfLoadedRaw(const AssetPath& path)
{
	auto it = loadedResources.find(path);
	if (it != loadedResources.end())
	{
		std::weak_ptr<BaseObject> assetWeakPtr = it->second;

		std::shared_ptr<BaseObject> assetPtr = assetWeakPtr.lock();
		if (assetPtr)
		{
			return assetPtr;
		}
	}

	return std::shared_ptr<BaseObject>();
}

bool AssetManager::IsLoaded(const AssetPath& path)
{
	auto it = loadedResources.find(path);
	if (it != loadedResources.end())
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool AssetManager::Exists(const AssetPath& path)
{
	return path.IsValid() && path.Exists() && path.IsFile();
}

std::string AssetManager::GetAssetTypeNameFromArchive(Archive& archive)
{
	// first try, get from _Type node, used in manually write files, like shader
	ArchiveNode* typeNode = archive.Root()->GetChild("_Type");
	if (typeNode)
	{
		std::string typeName = typeNode->GetValue<std::string>();
		return typeName;
	}
	
	// second try, get from root serialized object type, used in serialized files
	Deserializer deserializer(archive.Root());
	std::string typeName = deserializer.GetRootObjectTypeName();
	return typeName;
}

Type* AssetManager::GetAssetType(const AssetPath& path)
{		
	std::unique_ptr<ReadStream> stream = path.OpenRead();
	if (!stream->CanRead())
	{
		Log::Error("AssetManager::Load: can not read file: '%s'", path.ToString().c_str());
		return {};
	}

	Archive archive;
	if (!archive.ReadFrom(stream.get()))
	{
		Log::Error("AssetManager::Load: can not load archive from file: '%s'", path.ToString().c_str());
		return {};
	}

	std::string typeName = GetAssetTypeNameFromArchive(archive);
	if (typeName.empty())
	{
		Log::Error("AssetManager::Load: can not get root-object typename from file: '%s'", path.ToString().c_str());
		return {};
	}

	Type* type = Type::FindByName(typeName);
	if (type == nullptr)
	{
		Log::Error("AssetManager::Load: can not find type '%s' when loading '%s'", typeName.c_str(), path.ToString().c_str());
		return {};
	}

	return type;
}

void AssetManager::UnloadRaw(std::shared_ptr<BaseObject>& asset)
{
	if (!asset)
	{
		Log::Error("AssetManager::Save: can not save null asset");
		return;
	}

	if (!asset->IsAsset())
	{
		Log::Error("AssetManager::UnloadRaw: asset must be an asset");
		return;
	}

	AssetPath path = asset->GetAssetPath();

	asset->OnDestroy();

	loadedResources.erase(path);
}

void AssetManager::UnloadAll()
{
	for (auto& pair : loadedResources)
	{
		std::weak_ptr<BaseObject> weakPtr = pair.second;

		std::shared_ptr<BaseObject> ptr = weakPtr.lock();
		if (ptr)
		{
			ptr->OnDestroy();
		}
	}

	loadedResources.clear();
}

bool AssetManager::Delete(const AssetPath& path)
{
	std::shared_ptr<BaseObject> ptr = GetIfLoadedRaw(path);
	if (ptr)
	{
		UnloadRaw(ptr);
	}

	bool result = path.Delete();

	return result;
}

void AssetManager::RegisterSimpleProcessorType(Type* type, const std::vector<FixedString>& extensions)
{
	CHECK(type->IsChildOf(&AssetProcessor::StaticType));

	for (size_t i = 0; i < extensions.size(); i++)
	{
		FixedString ext = stringToLower(extensions[i].c_str());

		Log::Info("RegisterSimpleProcessorType: '%s' -> %s", ext.c_str(), type->TypeName().c_str());

		if (ext.str().size() == 0)
		{
			Log::Error("Error can not register empty extension '%s'", ext.c_str());
			continue;
		}

		if (ext.str()[0] != '.')
		{
			Log::Error("Error extension '%s' should start with a dot '.'", ext.c_str());
			continue;
		}

		auto it = extensionToSimpleProcessorTypeMap.find(ext);
		if (it != extensionToSimpleProcessorTypeMap.end())
		{
			Log::Error("Error extension '%s' already registered by another AssetImporter", ext.c_str());
			continue;
		}

		extensionToSimpleProcessorTypeMap.insert(std::make_pair(ext, type));
	}
}

void AssetManager::Refresh()
{
	Log::Info("AssetManager::Refresh");

	importDatabase->Load();

	AssetPath rawAssetsDir = GetCommonDirectory(CD_RawAssetsDir);

	if (rawAssetsDir.Exists() && rawAssetsDir.IsDirectory())
	{
		std::vector<AssetPath> files;
		rawAssetsDir.List(files, true);

		for (AssetPath rawAssetPath : files)
		{
			if (rawAssetPath.IsFile())
			{
				AssetImportRecordPtr importRecord;

				if (IsSrcAssetImported(rawAssetPath, &importRecord))
				{
					if (NeedReimportAsset(importRecord->processorPath))
					{
						ReimportAsset(importRecord->processorPath);
					}
				}
				else
				{
					ImportNewAsset(rawAssetPath);
				}
			}
		}
	}
}

Type* AssetManager::FindProcessorForImporting(const AssetPath& srcAssetPath)
{
	std::string extension = stringToLower(srcAssetPath.Extensions());

	auto it = extensionToSimpleProcessorTypeMap.find(FixedString(extension));
	if (it != extensionToSimpleProcessorTypeMap.end())
	{
		Type* type = it->second;

		return type;
	}
	else
	{
		return nullptr;
	}
}

bool AssetManager::IsSrcAssetImported(const AssetPath& srcAssetPath, AssetImportRecordPtr* outLastImportRecord /*= nullptr*/)
{
	AssetImportRecordPtr rec = importDatabase->FindImportRecordByInput(srcAssetPath);
	if (rec)
	{
		if (rec->processorPath.Exists())
		{
			if (outLastImportRecord)
				*outLastImportRecord = rec;
			return true;
		}
	}

	if (outLastImportRecord)
		*outLastImportRecord = AssetImportRecordPtr();
	return false;
}

bool AssetManager::ImportNewAsset(const AssetPath& srcAssetPath, AssetProcessorOutput** outProcessorOutputs /*= nullptr*/)
{
	Log::Info("Importing: %s", srcAssetPath.ToString().c_str());

	//
	// calc output path
	//
	AssetPath outputPath;

	AssetPath rawAssetsDir = GetCommonDirectory(CD_RawAssetsDir);
	std::string relativePathStr;
	if (vcpp::filesystem::get_relative_path(srcAssetPath.GetFileSystemAbsolutePath(), rawAssetsDir.GetFileSystemAbsolutePath(), relativePathStr))
	{
		outputPath = (GetCommonDirectory(CD_AssetsDir) / relativePathStr).ReplaceExtensions("");
	}
	else
	{
		Log::Error("Import asset failed '%s', must be placed under dir '%s'", srcAssetPath.ToString().c_str(), rawAssetsDir.ToString().c_str());
		return false;
	}

	//
	// create processor
	//
	Type* processorType = FindProcessorForImporting(srcAssetPath);
	if (processorType == nullptr)
	{
		Log::Error("Can not find processor type to import: %s", srcAssetPath.ToString().c_str());
		return false;
	}

	AssetPath processorPath = AssetPath::FindUsablePath(outputPath.Parent(), outputPath.FilenameWithExtension(), ".importer");
	BaseObjectPtr processorAsset = CreateRaw(processorPath, processorType);

	AssetProcessorPtr processor = Util::CastPtrWithCheck<AssetProcessor>(processorAsset);

	//
	// create processor output
	//
	AssetProcessorOutputSaveToAsset* processorOuput = new AssetProcessorOutputSaveToAsset(processorPath, outputPath);

	//
	// create processor input
	//
	std::vector<AssetPath> processorInputs;
	processorInputs.push_back(srcAssetPath);

	//
	// do the process
	//
	bool success = processor->DoProcess(processorInputs, processorOuput);
	if (!success)
	{
		if (outProcessorOutputs)
			(*outProcessorOutputs) = nullptr;

		return false;
	}

	if (outProcessorOutputs)
		(*outProcessorOutputs) = processorOuput;

	//
	// save processor
	//
	bool everythingSaved = true;

	if (!Save(processor))  //save after-import properties, e.g. resultAssetPaths
	{
		Log::Error("Can not save AssetProcessor at '%s'", processor->GetAssetPath().ToString().c_str());
		everythingSaved = false;
	}

	//
	// save processor outputs
	//
	std::map<std::string, BaseObjectPtr> processorOutputs;
	processorOuput->GetOutputs(processorOutputs);

	for (auto it = processorOutputs.begin(); it != processorOutputs.end(); ++it)
	{
		BaseObjectPtr objPtr = it->second;

		if (Save(objPtr))
		{
			Log::Info("Save processor output succeed: %s", objPtr->GetAssetPath().ToString().c_str());
		}
		else
		{
			Log::Error("Save processor output failed %s", objPtr->GetAssetPath().ToString().c_str());
			everythingSaved = false;
		}
	}

	//
	// save import record
	//
	importDatabase->CreateOrUpdateImportRecord(processorPath, outputPath, processorInputs, processorOutputs);
	importDatabase->Save();

	return everythingSaved;
}

bool AssetManager::NeedReimportAsset(const AssetPath& processorPath)
{
	AssetImportRecordPtr lastImportRecord = importDatabase->FindImportRecordByProcessorPath(processorPath);
	if (lastImportRecord == nullptr)
	{
		Log::Error("Can not find last import record for: %s", processorPath.ToString().c_str());
		return false;
	}

	bool needReimport = !lastImportRecord->IsAllInputsExist() || lastImportRecord->IsInputsOrProcessorUpdatedSinceLastImport();

	return needReimport;
}

bool AssetManager::ReimportAsset(const AssetPath& processorPath, AssetProcessorOutput** outProcessorOutputs /*= nullptr*/)
{
	//
	// find last import record
	//
	AssetImportRecordPtr lastImportRecord = importDatabase->FindImportRecordByProcessorPath(processorPath);
	if (lastImportRecord == nullptr)
	{
		Log::Error("Can not find last import record for: %s", processorPath.ToString().c_str());
		return false;
	}

	Log::Info("Reimporting: %s -> %s", processorPath.ToString().c_str(), lastImportRecord->outputPath.ToString().c_str());

	//
	// load processor
	//
	BaseObjectPtr processorAsset = LoadRaw(processorPath);
	AssetProcessorPtr processor = Util::CastPtrWithCheck<AssetProcessor>(processorAsset);

	//
	// create processor output
	//
	AssetProcessorOutputSaveToAsset* processorOuput = new AssetProcessorOutputSaveToAsset(processorPath, lastImportRecord->outputPath);

	//
	// create processor input
	//
	const std::vector<AssetPath>& processorInputs = lastImportRecord->inputAssetPaths;

	//
	// do the process
	//
	bool success = processor->DoProcess(processorInputs, processorOuput);
	if (!success)
	{
		if (outProcessorOutputs)
			(*outProcessorOutputs) = nullptr;

		return false;
	}

	if (outProcessorOutputs)
		(*outProcessorOutputs) = processorOuput;

	//
	// save processor
	//
	bool everythingSaved = true;

	if (!Save(processor))  //save after-import properties, e.g. resultAssetPaths
	{
		Log::Error("Can not save AssetProcessor at '%s'", processor->GetAssetPath().ToString().c_str());
		everythingSaved = false;
	}

	//
	// save processor outputs
	//
	std::map<std::string, BaseObjectPtr> processorOutputs;
	processorOuput->GetOutputs(processorOutputs);

	for (auto it = processorOutputs.begin(); it != processorOutputs.end(); ++it)
	{
		BaseObjectPtr objPtr = it->second;

		if (Save(objPtr))
		{
			Log::Info("Save processor output succeed: %s", objPtr->GetAssetPath().ToString().c_str());
		}
		else
		{
			Log::Error("Save processor output failed %s", objPtr->GetAssetPath().ToString().c_str());
			everythingSaved = false;
		}
	}

	//
	// save import record
	//
	importDatabase->CreateOrUpdateImportRecord(processorPath, lastImportRecord->outputPath, processorInputs, processorOutputs);
	importDatabase->Save();

	return everythingSaved;
}

bool AssetManager::ImportDirect(const AssetPath& srcAssetPath, AssetProcessorOutput** outProcessorOutputs /*= nullptr*/)
{
	Log::Info("Importing (Direct): %s -> %s", srcAssetPath.ToString().c_str());

	//
	// create processor
	//
	Type* processorType = FindProcessorForImporting(srcAssetPath);
	if (processorType == nullptr)
	{
		Log::Error("Can not find processor type to import: %s", srcAssetPath.ToString().c_str());
		return false;
	}

	AssetProcessorPtr processor((AssetProcessor*)processorType->CreateInstance());

	//
	// create processor output
	//
	AssetProcessorOutputInMemory* processorOuput = new AssetProcessorOutputInMemory();

	//
	// create processor input
	//
	std::vector<AssetPath> processorInputs;
	processorInputs.push_back(srcAssetPath);

	//
	// do the process
	//
	bool success = processor->DoProcess(processorInputs, processorOuput);
	if (!success)
	{
		if (outProcessorOutputs)
			(*outProcessorOutputs) = nullptr;

		return false;
	}

	if (outProcessorOutputs)
		(*outProcessorOutputs) = processorOuput;

	return true;
}
