#pragma once

#include <vector>
#include <string>

#include <vcpp/string_convert.h>
#include "../Streams.h"

class Archive;

/**

Archive Format:

Root
	importRecords
		Array
			Element
				Pointer 0
			Element
				Pointer 1

	Object
		id 0
		lastImportTime 13268068352.000000
		processorPath "proj://Assets/brickwall.importer"
		outputPath 'proj://Assets/brickwall'

		myBinaryData @bin 123 {0i24rpihdffvl.kn`1209uwer'pjof[v-0q4f2iou3gfbkawbdfuioq2e}

		inputAssetPaths
			Array
				Element """proj://Raw/brickwall.jpg"""
		outputAssetPaths
			Map
				Element
					Key "main"
					Value "proj://Assets/brickwall.Texture2D"

*/

// string view, can also store binary data
struct ArchiveString
{
	char* begin;
	char* end;
	bool isBinary;

	ArchiveString()
		: begin(nullptr), end(nullptr), isBinary(false)
	{}

	ArchiveString(char* begin, char* end, bool isBinary)
		: begin(begin), end(end), isBinary(isBinary)
	{}

	ArchiveString(byte* begin, byte* end, bool isBinary)
		: begin((char*)begin), end((char*)end), isBinary(isBinary)
	{}

	bool IsValid() const { return Size() > 0; }

	int Size() const { return (int)(end - begin); }

	void* Data() { return begin; }

	std::string ToStr() const { return std::string(begin, end); }

	bool operator == (const char* str) const
	{
		int length = (int)::strlen(str);
		if (length != Size())
			return false;
		else
			return memcmp(begin, str, length) == 0;
	}

	bool operator == (const std::string& str) const
	{
		int length = (int)str.size();
		if (length != Size())
			return false;
		else
			return memcmp(begin, str.data(), length) == 0;
	}

	bool operator == (const ArchiveString& str) const
	{
		if (Size() != str.Size())
			return false;
		else
			return memcmp(begin, str.begin, Size()) == 0;
	}
};

class ArchiveNode
{
protected:
	ArchiveString name;
	ArchiveString value;

	Archive* archive;
	int depth;
	ArchiveNode* parent;
	std::vector<ArchiveNode*> children;

public:
	ArchiveNode(Archive* archive);
	~ArchiveNode();

	ArchiveNode* Parent() { return parent; }

	int ChildCount() const { return (int)children.size(); }

	ArchiveNode* GetChild(int i) { return children[i]; }

	ArchiveNode* GetChild(const char* name);

	ArchiveNode* AddChild(const char* name);

	ArchiveNode* GetOrAddChild(const char* name);

	int GetDepth() const { return depth; }

	//
	// get name
	//
	ArchiveString GetName() { return name; }

	//
	// set name
	//
	void SetName(const char* name);
	void SetName(const std::string& name);

	//
	// get value
	//
	bool HasValue() const { return value.IsValid(); }
	ArchiveString GetValue() { return value; }

	template<typename T>
	T GetValue(const T& defaultValue = T())
	{
		if (value.IsValid())
		{
			std::string sv = value.ToStr();

			T v;
			if (GetStaticType<T>()->FromString(sv, &v))
			{
				return v;
			}
			else
			{
				Log::Error("Convert string '%s' to type '%s' failed", sv.c_str(), GetStaticType<T>()->TypeName());
				return defaultValue;
			}
		}
		else
		{
			return defaultValue;
		}
	}

	template<>
	std::string GetValue(const std::string& defaultValue)
	{
		return value.IsValid() ? value.ToStr() : std::string();
	}
	
	template<typename T>
	T GetChildValue(const char* name, const T& defaultValue = T())
	{
		ArchiveNode* childnode = GetChild(name);
		if (childnode)
			return childnode->GetValue<T>(defaultValue);
		else
			return defaultValue;
	}

	template<>
	std::string GetChildValue(const char* name, const std::string& defaultValue)
	{
		ArchiveNode* childnode = GetChild(name);
		if (childnode)
			return childnode->GetValue<std::string>(defaultValue);
		else
			return defaultValue;
	}

	//
	// set value
	//
	void SetValue(const ArchiveString& v) { this->value = v; }

	void SetValueString(const char* v);
	void SetValueString(const std::string& v);

	void SetValueBinary(const char* data, size_t size);

	template<typename T>
	void SetValue(const T& value)
	{
		std::string sv;
		GetStaticType<T>()->ToString(&value, sv);

		SetValueString(sv);
	}

	template<typename T>
	void SetChildValue(const char* name, const T& value)
	{
		ArchiveNode* childnode = GetOrAddChild(name);
		childnode->SetValue(value);
	}
};

class Archive
{
	struct DataBuffer
	{
		byte* buffer;
		int totalSize;
		int usedSize;

		DataBuffer(int size)
		{
			buffer = new byte[size];
			totalSize = size;
			usedSize = 0;
		}

		~DataBuffer()
		{
			delete[] buffer;
		}

		int RemainingSize() const { return totalSize - usedSize; }

		byte* TryAllocate(int size)
		{
			if (usedSize + size <= totalSize)
			{
				byte* p = buffer + usedSize;
				usedSize += size;
				return p;
			}
			else
			{
				return nullptr;
			}
		}
	};

	DataBuffer* bestAllocationBuffer;
	std::vector<DataBuffer*> buffers;
	std::vector<ArchiveNode*> nodes;

	ArchiveNode* root;

	int spacesPerIndent;

public:
	Archive();
	~Archive();

	void Destroy();

	void CreateNew();
	bool ReadFrom(const std::string& str);
	bool ReadFrom(ReadStream* stream);
	bool ReadFrom(const byte* buffer, size_t size);
	bool WriteTo(WriteStream* stream);
	std::string WriteToString();

	ArchiveNode* Root() { return root; }

public:
	ArchiveNode* AllocateNode();
	ArchiveString AllocateString(int size, bool isBinary);
	ArchiveString AllocateString(const char* cstr);
	ArchiveString AllocateString(const char* begin, const char* end, bool isBinary);

	ArchiveString AllocateString(const std::string& v, bool isBinary)
	{
		return AllocateString(v.data(), v.data() + v.size(), isBinary);
	}
	
private:
	bool WriteToStreamRecursive(ArchiveNode* node, WriteStream* stream, std::string& indentsBuffer);

};
