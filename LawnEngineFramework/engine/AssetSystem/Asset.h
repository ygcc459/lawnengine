#pragma once

#include "../BaseObject.h"

class Asset : public BaseObject
{
	DECL_CLASS_TYPE(Asset, BaseObject);

public:
	Asset()
	{}

	virtual ~Asset()
	{}

	virtual void Reflect(Reflector& reflector) override;

	virtual void OnDestroy() {}

};

typedef std::shared_ptr<Asset> AssetPtr;
