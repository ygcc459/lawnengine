#pragma once

#include <vcpp/FileSystem.h>

#include <vector>
#include <map>

enum AssetPathType
{
	AST_Empty,
	AST_AbsolutePath,
	AST_ProjectPath,
};

struct AssetPath
{
	AssetPathType type;
	std::string path;

	AssetPath()
		:type(AST_Empty)
	{}

	AssetPath(AssetPathType type, const std::string& path)
		:type(type), path(vcpp::filesystem::normalize(path))
	{}

	bool IsValid() const
	{
		return type != AST_Empty;
	}

	FileSystem& GetFileSystem() const;

	AssetPath Parent() const;

	// e.g. ".png", ".png.meta", ".mesh", ""
	std::string Extensions() const;

	std::string LastExtension() const;
	AssetPath ReplaceExtensions(const std::string& ext) const;
	
	// filename without extension
	std::string Filename() const;

	std::string FilenameWithExtension() const;

	void Split(AssetPath& dir, std::string& filename, std::string& extensions) const;

	bool IsInsideOfDirectory(const AssetPath& dir) const;

	std::string GetFileSystemAbsolutePath() const;

	AssetPath ToAbsolutePath() const;
	AssetPath ToProjectPath() const;

	AssetPath Join(const std::string& path) const;
	AssetPath Join(const AssetPath& path) const;

	// return time in seconds since epoch
	double GetLastWriteTime() const;

	bool Exists() const;
	bool Delete() const;
	bool CreateDirectory() const;
	bool EnsureDirectoryExists() const;
	bool IsDirectory() const;
	bool IsFile() const;

	void List(std::vector<AssetPath>& outChildPaths, bool resursive = true) const;

	// returns absolute file-system paths
	void List(std::vector<std::string>& outChildPaths, bool resursive = true) const;
	
	static AssetPath FindUsablePath(const AssetPath& dir, const std::string& filename, const std::string& ext);

	std::unique_ptr<ReadStream> OpenRead(bool text = false) const;

	std::unique_ptr<WriteStream> OpenWrite(bool text = false) const;

	bool operator == (const AssetPath& other) const
	{
		return this->type == other.type && this->path == other.path;
	}

	bool operator != (const AssetPath& other) const
	{
		return !(*this == other);
	}

	bool operator < (const AssetPath& other) const
	{
		if (this->type != other.type)
		{
			return this->type < other.type;
		}
		else
		{
			return this->path < other.path;
		}
	}

	AssetPath operator / (const char* path) const
	{
		return this->Join(std::string(path));
	}

	AssetPath operator / (const std::string& path) const
	{
		return this->Join(path);
	}

	AssetPath operator / (const AssetPath& path) const
	{
		return this->Join(path);
	}

	std::string ToString() const;
	static AssetPath FromString(const std::string& s);
};

DECL_PRIMITIVE_TYPE(AssetPath)
