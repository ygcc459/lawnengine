#pragma once

#include "AssetManager.h"
#include "RenderSystem/Mesh.h"
#include "RenderSystem/Material.h"

class SceneObject;
typedef std::shared_ptr<SceneObject> SceneObjectPtr;

struct aiScene;
struct aiMesh;
struct aiMaterial;
struct aiNode;
class SceneObject;

class ImportModelProcessor : public AssetProcessor
{
	DECL_CLASS_TYPE(ImportModelProcessor, AssetProcessor);

public:
	ImportModelProcessor();
	virtual ~ImportModelProcessor();

	virtual void Reflect(Reflector& reflector) override;

	virtual bool DoProcess(const std::vector<AssetPath>& inputAssetPaths, AssetProcessorOutput* processorOutput) override;

private:
	uint CalculateImportFlags() const;
	void ConvertSceneNode(const aiScene* scene, const aiNode* node, SceneObject* parentSceneObject);
	MeshPtr ConvertMesh(const aiMesh* srcmesh);
	MaterialPtr ConvertMaterial(const aiMaterial* srcmat);

	AssetProcessorOutput* processorOutput;

	std::vector<MeshPtr> convertedMeshes;
	std::vector<MaterialPtr> convertedMaterials;

public:
	bool CalcTangentSpace;
	bool JoinIdenticalVertices;
	bool MakeLeftHanded;
	bool Triangulate;
	bool RemoveComponent;
	bool GenNormals;
	bool GenSmoothNormals;
	bool SplitLargeMeshes;
	bool PreTransformVertices;
	bool LimitBoneWeights;
	bool ValidateDataStructure;
	bool ImproveCacheLocality;
	bool RemoveRedundantMaterials;
	bool FixInfacingNormals;
	bool PopulateArmatureData;
	bool SortByPType;
	bool FindDegenerates;
	bool FindInvalidData;
	bool GenUVCoords;
	bool TransformUVCoords;
	bool FindInstances;
	bool OptimizeMeshes;
	bool OptimizeGraph;
	bool FlipUVs;
	bool FlipWindingOrder;
	bool SplitByBoneCount;
	bool Debone;

	quaternionf RotationEulerAngles;
	float3 Scale;

	bool CreateSceneObjects;
	bool CreateMeshes;
	bool CreateMaterials;

};
