#pragma once

#include "AssetPath.h"
#include "Asset.h"
#include "AssetProcessor.h"

#include <vector>
#include <map>

enum CommonDirectories
{
	CD_ProjectDir = 0,
	CD_AssetsDir,
	CD_RawAssetsDir,
	CD_TempDir,

	CD_Count,
};

struct AssetImportRecord
{
	double lastImportTime;

	AssetPath processorPath;

	AssetPath outputPath;

	std::vector<AssetPath> inputAssetPaths;

	std::map<std::string, AssetPath> outputAssetPaths;

	bool IsInputsOrProcessorUpdatedSinceLastImport() const;
	bool IsAllInputsExist() const;
	bool IsProcessorExist() const;
	double CalcLastWriteTime() const;
};
typedef std::shared_ptr<AssetImportRecord> AssetImportRecordPtr;

class AssetImportRecordType : public PrimitiveObjectType<AssetImportRecord>
{
public:
	AssetImportRecordType()
		: PrimitiveObjectType<AssetImportRecord>("AssetImportRecord")
	{}

	virtual bool Serialize(Serializer& serializer, void* p) const override
	{
		ValueType& obj = *reinterpret_cast<ValueType*>(p);

		serializer.SerializeMemberAuto("lastImportTime", obj.lastImportTime);
		serializer.SerializeMemberAuto("processorPath", obj.processorPath);
		serializer.SerializeMemberAuto("outputPath", obj.outputPath);
		serializer.SerializeMemberAuto("inputAssetPaths", obj.inputAssetPaths);
		serializer.SerializeMemberAuto("outputAssetPaths", obj.outputAssetPaths);

		return true;
	}

	virtual bool Deserialize(Deserializer& deserializer, void* p) const override
	{
		ValueType& obj = *reinterpret_cast<ValueType*>(p);

		deserializer.DeserializeMemberAuto("lastImportTime", obj.lastImportTime);
		deserializer.DeserializeMemberAuto("processorPath", obj.processorPath);
		deserializer.DeserializeMemberAuto("outputPath", obj.outputPath);
		deserializer.DeserializeMemberAuto("inputAssetPaths", obj.inputAssetPaths);
		deserializer.DeserializeMemberAuto("outputAssetPaths", obj.outputAssetPaths);

		return true;
	}
};
DECL_PRIMITIVE_TYPE(AssetImportRecord)

class AssetImportDatabase : public BaseObject
{
public:
	DECL_CLASS_TYPE(AssetImportDatabase, BaseObject);

	AssetImportDatabase();
	virtual ~AssetImportDatabase();

	virtual void Reflect(Reflector& reflector) override;

	AssetPath GetConfigPath() const;

	void Load();
	void Save();
	void RemoveInvalidRecords(bool removeIfMissingInputs = false);

	AssetImportRecordPtr CreateOrUpdateImportRecord(AssetPath processorPath, AssetPath outputPath, const std::vector<AssetPath>& inputs, const std::map<std::string, BaseObjectPtr>& outputs);

	AssetImportRecordPtr FindImportRecordByInput(const AssetPath& path);
	AssetImportRecordPtr FindImportRecordByProcessorPath(const AssetPath& processorPath);

public:
	std::vector<AssetImportRecordPtr> importRecords;
};

class AssetManager
{
public:
	AssetManager();
	~AssetManager();

	void SetCommonDirectory(CommonDirectories cd, const AssetPath& path);
	AssetPath GetCommonDirectory(CommonDirectories cd) const;

	AssetPath ProjectDir() const { return GetCommonDirectory(CD_ProjectDir); }
	AssetPath AssetsDir() const { return GetCommonDirectory(CD_AssetsDir); }
	AssetPath RawAssetsDir() const { return GetCommonDirectory(CD_RawAssetsDir); }
	AssetPath TempDir() const { return GetCommonDirectory(CD_TempDir); }

	AssetImportDatabase* ImportDatabase() { return importDatabase; }

	//
	// asset create, load, save
	//
	bool IsLoaded(const AssetPath& path);

	bool Exists(const AssetPath& path);

	std::string GetAssetTypeNameFromArchive(Archive& archive);
	Type* GetAssetType(const AssetPath& path);

	std::shared_ptr<BaseObject> CreateRaw(const AssetPath& path, Type* type);

	bool SaveRaw(const std::shared_ptr<BaseObject>& asset);

	std::shared_ptr<BaseObject> LoadRaw(const AssetPath& path);

	std::shared_ptr<BaseObject> GetIfLoadedRaw(const AssetPath& path);

	//
	// asset create, load, save (with template)
	//
	template<typename T>
	std::shared_ptr<T> Create(const AssetPath& path)
	{
		std::shared_ptr<BaseObject> ptr = CreateRaw(path, &T::StaticType);

		return std::static_pointer_cast<T>(ptr);
	}

	template<typename T>
	bool Save(const std::shared_ptr<T>& asset, const AssetPath& path)
	{
		BaseObjectPtr baseObjectPtr = Util::CastPtrWithCheck<BaseObject>(asset);
		baseObjectPtr->SetAssetPath(path);

		bool result = SaveRaw(baseObjectPtr);

		return result;
	}

	template<typename T>
	bool Save(const std::shared_ptr<T>& asset)
	{
		BaseObjectPtr baseObjectPtr = Util::CastPtrWithCheck<BaseObject>(asset);

		bool result = SaveRaw(baseObjectPtr);

		return result;
	}

	template<typename T>
	std::shared_ptr<T> Load(const AssetPath& path)
	{
		std::shared_ptr<BaseObject> ptr = LoadRaw(path);

		std::shared_ptr<T> typedPtr = Util::CastPtrWithCheck<T>(ptr);

		return typedPtr;
	}
	
	template<typename T>
	std::shared_ptr<T> GetIfLoaded(const AssetPath& path)
	{
		std::shared_ptr<BaseObject> ptr = GetIfLoadedRaw(path);

		std::shared_ptr<T> typedPtr = Util::CastPtrWithCheck<T>(ptr);

		return typedPtr;
	}

	template<typename T>
	std::shared_ptr<T> LoadOrCreate(const AssetPath& path)
	{
		std::shared_ptr<T> ptr = Load<T>(path);
		if (ptr)
		{
			return ptr;
		}

		ptr = Create<T>(path);

		return ptr;
	}

	//
	// asset unload
	//
	void UnloadRaw(std::shared_ptr<BaseObject>& asset);

	template<class T>
	void Unload(std::shared_ptr<T> asset)
	{
		std::shared_ptr<BaseObject> baseObjectPtr = Util::CastPtrWithCheck<BaseObject>(asset);
		if (baseObjectPtr)
		{
			UnloadRaw(baseObjectPtr);
		}
	}

	template<class T>
	void UnloadSafe(std::shared_ptr<T>& asset)
	{
		if (asset)
		{
			Unload(asset);
			asset = nullptr;
		}
	}

	void UnloadAll();

	//
	// asset delete
	//
	bool Delete(const AssetPath& path);

	template<class T>
	bool Delete(const std::shared_ptr<T>& ptr)
	{
		std::shared_ptr<BaseObject> baseObjectPtr = Util::CastPtrWithCheck<BaseObject>(ptr);

		if (!baseObjectPtr->IsAsset())
		{
			Log::Error("AssetManager::Delete: ptr must be an asset");
			return false;
		}

		AssetPath path = baseObjectPtr->GetAssetPath();

		return Delete(path);
	}

	//
	// asset importing
	//
	void RegisterSimpleProcessorType(Type* type, const std::vector<FixedString>& extensions);

	void Refresh();

	Type* FindProcessorForImporting(const AssetPath& srcAssetPath);

	bool IsSrcAssetImported(const AssetPath& srcAssetPath, AssetImportRecordPtr* outLastImportRecord = nullptr);

	bool ImportNewAsset(const AssetPath& srcAssetPath, AssetProcessorOutput** outProcessorOutputs = nullptr);

	bool NeedReimportAsset(const AssetPath& processorPath);

	bool ReimportAsset(AssetImportRecordPtr importRecord, AssetProcessorOutput** outProcessorOutputs = nullptr)
	{
		return ReimportAsset(importRecord->processorPath, outProcessorOutputs);
	}
	bool ReimportAsset(const AssetPath& processorPath, AssetProcessorOutput** outProcessorOutputs = nullptr);

	template<typename T>
	std::shared_ptr<T> ImportNewAsset(const AssetPath& srcAssetPath)
	{
		AssetProcessorOutput* processorOutputs;
		if (ImportNewAsset(srcAssetPath, &processorOutputs))
		{
			BaseObjectPtr mainOutputPtr = processorOutputs->GetMainOutput();
			return Util::CastPtrWithCheck<T>(mainOutputPtr);
		}

		return std::shared_ptr<T>();
	}

	template<typename T>
	std::shared_ptr<T> ReimportAsset(const AssetPath& processorPath)
	{
		AssetProcessorOutput* processorOutputs;
		if (ReimportAsset(processorPath, &processorOutputs))
		{
			BaseObjectPtr mainOutputPtr = processorOutputs->GetMainOutput();
			return Util::CastPtrWithCheck<T>(mainOutputPtr);
		}

		return std::shared_ptr<T>();
	}

	//
	// asset importing (directly in memory)
	//
	bool ImportDirect(const AssetPath& srcAssetPath, AssetProcessorOutput** outProcessorOutputs = nullptr);

	template<typename T>
	std::shared_ptr<T> ImportDirect(const AssetPath& srcAssetPath)
	{
		AssetProcessorOutput* processorOutputs;
		if (ImportDirect(srcAssetPath, &processorOutputs))
		{
			BaseObjectPtr mainOutputPtr = processorOutputs->GetMainOutput();
			return Util::CastPtrWithCheck<T>(mainOutputPtr);
		}

		return std::shared_ptr<T>();
	}

protected:
	AssetImportDatabase* importDatabase;

	typedef std::map<AssetPath, std::shared_ptr<BaseObject> > ResourceMap;
	ResourceMap loadedResources;

	typedef std::map<CommonDirectories, AssetPath> CommonDirectoryMap;
	CommonDirectoryMap commonDirectoryMap;

	typedef std::map<FixedString, Type*> AssetExtensionToSimpleProcessorTypeMap;
	AssetExtensionToSimpleProcessorTypeMap extensionToSimpleProcessorTypeMap;
};
