#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "AssetProcessorOutputInMemory.h"

AssetProcessorOutputInMemory::AssetProcessorOutputInMemory()
{
}

AssetProcessorOutputInMemory::~AssetProcessorOutputInMemory()
{
}

BaseObjectPtr AssetProcessorOutputInMemory::CreateOutput(const std::string& key, Type* type, bool overwrite /*= true*/, bool isMainOutput /*= false*/)
{
	if (!type->IsSameOrChildOf(&BaseObject::StaticType))
	{
		Log::Error("Invalid processor output type '%s', must be child of 'BaseObject'", type->TypeName().c_str());
		return nullptr;
	}

	if (isMainOutput)
	{
		mainOutputName = key;
	}

	if (overwrite)
	{
		outputs.erase(key);
	}

	auto it = outputs.find(key);
	if (it != outputs.end())
	{
		BaseObjectPtr ptr = it->second;
		if (ptr->GetType()->IsSameOrChildOf(type))
		{
			return ptr;
		}
		else
		{
			Log::Error("Error, processor output with name '%s' already exists, and is type '%s' instead of '%s'", key.c_str(), ptr->GetType()->TypeName().c_str(), type->TypeName().c_str());
			return nullptr;
		}
	}
	else
	{
		BaseObject* ptr = (BaseObject*)type->CreateInstance();
		if (ptr)
		{
			BaseObjectPtr sharedPtr(ptr);

			outputs.insert(std::make_pair(key, sharedPtr));

			return sharedPtr;
		}
		else
		{
			Log::Error("Error, Failed to create instance of type '%s'", type->TypeName().c_str());
			return nullptr;
		}
	}
}

BaseObjectPtr AssetProcessorOutputInMemory::GetOutput(const std::string& key)
{
	auto it = outputs.find(key);
	if (it != outputs.end())
		return it->second;
	else
		return BaseObjectPtr();
}

void AssetProcessorOutputInMemory::GetOutputs(std::map<std::string, BaseObjectPtr>& outputs)
{
	outputs = this->outputs;
}

BaseObjectPtr AssetProcessorOutputInMemory::GetMainOutput()
{
	return outputs[mainOutputName];
}

void AssetProcessorOutputInMemory::SetAsMainOutput(const std::string& key)
{
	mainOutputName = key;
}
