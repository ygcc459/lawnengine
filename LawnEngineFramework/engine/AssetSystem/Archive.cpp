#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "Archive.h"

#include "../Streams.h"
#include <vcpp/string_convert.h>

#define ARCHIVE_STRING_DEBUG_ON 1

ArchiveNode::ArchiveNode(Archive* archive)
	:archive(archive), parent(nullptr), depth(0)
{
}

ArchiveNode::~ArchiveNode()
{
}

ArchiveNode* ArchiveNode::AddChild(const char* name)
{
	ArchiveNode* node = archive->AllocateNode();
	node->name = archive->AllocateString(name);
	node->parent = this;
	this->children.push_back(node);
	node->depth = this->depth + 1;

	return node;
}

ArchiveNode* ArchiveNode::GetChild(const char* name)
{
	for (int i = 0; i < children.size(); i++)
	{
		ArchiveNode* node = children[i];
		if (node->name == name)
			return node;
	}

	return nullptr;
}

ArchiveNode* ArchiveNode::GetOrAddChild(const char* name)
{
	ArchiveNode* node = GetChild(name);
	if (node == nullptr)
		node = AddChild(name);
	return node;
}

void ArchiveNode::SetName(const char* name)
{
	this->name = archive->AllocateString(name);
}

void ArchiveNode::SetName(const std::string& name)
{
	this->name = archive->AllocateString(name, false);
}

void ArchiveNode::SetValueString(const char* v)
{
	this->value = archive->AllocateString(v, false);
}

void ArchiveNode::SetValueString(const std::string& v)
{
	this->value = archive->AllocateString(v, false);
}

void ArchiveNode::SetValueBinary(const char* data, size_t size)
{
	this->value = archive->AllocateString(data, data + size, true);
}

//////////////////////////////////////////////////////////////////////////

Archive::Archive()
	: root(nullptr), bestAllocationBuffer(nullptr)
{
	spacesPerIndent = 4;
}

Archive::~Archive()
{
	Destroy();
}

void Archive::Destroy()
{
	root = nullptr;
	bestAllocationBuffer = nullptr;

	for (int i = 0; i < buffers.size(); i++)
		delete buffers[i];
	buffers.clear();

	for (int i = 0; i < nodes.size(); i++)
		delete nodes[i];
	nodes.clear();
}

ArchiveNode* Archive::AllocateNode()
{
	ArchiveNode* node = new ArchiveNode(this);
	nodes.push_back(node);
	return node;
}

ArchiveString Archive::AllocateString(int size, bool isBinary)
{
#if ARCHIVE_STRING_DEBUG_ON
	int allocateSize = size + 1;  //append '\0' for visualize string data in debuggers
#else
	int allocateSize = size;
#endif

	byte* allocatedBuffer = nullptr;

	// allocate from last bestAllocationBuffer
	if (bestAllocationBuffer)
	{
		allocatedBuffer = bestAllocationBuffer->TryAllocate(allocateSize);
	}

	// create new DataBuffer, and allocate
	if (allocatedBuffer == nullptr)
	{
		int dataBufferSize = (buffers.size() != 0) ? buffers.back()->totalSize * 2 : 1024;  //first 1KB, then 2 times bigger
		dataBufferSize = std::min(dataBufferSize, 1024 * 1024);  //smaller than 1MB
		dataBufferSize = std::max(dataBufferSize, allocateSize);  //must meet required size

		DataBuffer* buffer = new DataBuffer(dataBufferSize);
		buffers.push_back(buffer);

		allocatedBuffer = buffer->TryAllocate(allocateSize);

		//usually bestAllocationBuffer is the last buffer in buffers
		//but when allocating size is huge, bestAllocationBuffer shouldn't be the newly created DataBuffer (which have no remaining size after this allocation)
		if (buffer->RemainingSize() > 0)
		{
			bestAllocationBuffer = buffer;
		}
	}

	CHECK(allocatedBuffer != nullptr);

	if (allocatedBuffer)
	{
#if ARCHIVE_STRING_DEBUG_ON
		allocatedBuffer[size] = '\0';  //append '\0' for visualize string data in debuggers
#endif

		return ArchiveString(allocatedBuffer, allocatedBuffer + size, isBinary);
	}
	else
	{
		return ArchiveString((byte*)nullptr, (byte*)nullptr, isBinary);
	}
}

ArchiveString Archive::AllocateString(const char* cstr)
{
	size_t length = ::strlen(cstr);
	ArchiveString t = AllocateString(length, false);
	if (t.IsValid())
	{
		memcpy(t.Data(), cstr, t.Size());
		return t;
	}
	else
	{
		return ArchiveString();
	}
}

ArchiveString Archive::AllocateString(const char* begin, const char* end, bool isBinary)
{
	if (end > begin)
	{
		size_t size = end - begin;

		ArchiveString t = AllocateString(size, isBinary);
		if (t.IsValid())
		{
			memcpy(t.Data(), begin, size);
			return t;
		}
	}

	return ArchiveString();
}

void Archive::CreateNew()
{
	Destroy();

	root = AllocateNode();
	root->SetName("Root");
}

bool Archive::ReadFrom(ReadStream* stream)
{
	Destroy();

	std::vector<byte> data;
	if (stream->ReadAllData(data))
	{
		return ReadFrom(data.data(), data.size());
	}
	else
	{
		return false;
	}
}

bool Archive::ReadFrom(const std::string& str)
{
	Destroy();

	return ReadFrom((byte*)str.data(), str.size());
}

bool Archive::ReadFrom(const byte* buffer, size_t size)
{
	CreateNew();

	std::vector<ArchiveNode*> parsingNodeStack;
	parsingNodeStack.push_back(root);

	vcpp::char_set inlineDataChars;
	inlineDataChars.set_range('a', 'z', true);
	inlineDataChars.set_range('A', 'Z', true);
	inlineDataChars.set_range('0', '9', true);
	inlineDataChars.set('_', true);

	char cc;
	const char* end = (const char*)buffer + size;
	const char* p = (const char*)buffer;

	std::string rootNodeName;
	if (!vcpp::parsers::parseWord(&p, rootNodeName))
	{
		Log::Error("Archive must starts with Root or root");
		return false;
	}

	if (rootNodeName != "Root" && rootNodeName != "root")
	{
		Log::Error("Archive must starts with Root or root");
		return false;
	}

	while (p < end)
	{
		//
		// newline
		//
		cc = (*p);
		if (cc == '\r' || cc == '\n')
		{
			++p;
			continue;
		}

		//
		// indents
		//
		const char* indentsBegin = p;
		const char* indentsEnd = vcpp::parsers::findFirstNotInSet(indentsBegin, end, vcpp::char_set::indents(), indentsBegin);
		
		bool haveBadIndents = false;
		int indents = vcpp::parsers::countIndents(indentsBegin, indentsEnd, spacesPerIndent, &haveBadIndents);

		if (haveBadIndents)
		{
			Log::Error("Must have valid indents, i.e. tabs and %d-spaces", spacesPerIndent);
			return false;
		}

		if (indents <= 0)
		{
			Log::Error("Must have more than one indent depth");
			return false;
		}

		p = indentsEnd;

		//
		// node name
		//
		std::string nodeName;
		if (!vcpp::parsers::parseWord(&p, nodeName))
		{
			continue;
		}

		//
		// spaces
		// 
		p = vcpp::parsers::findFirstNotInSet(p, end, vcpp::char_set::indents());
		if (p == nullptr)
			continue;
		
		//
		// node value
		//
		ArchiveString nodeValue;

		const char* matchEndPtr = nullptr;

		if (vcpp::parsers::startsWith(p, end, "@bin ", &matchEndPtr))
		{
			p = matchEndPtr;

			uint32 dataSize;
			if (!vcpp::parsers::parseUInt32(&p, dataSize))
			{
				Log::Error("Parse binary value failed after @bin, expecting binary-data-size");
				return false;
			}

			if (!vcpp::parsers::startsWith(p, end, " ", &p))
			{
				Log::Error("Parse binary value failed after binary-data-size, expecting a space");
				return false;
			}

			const char* dataBegin = p;
			const char* dataEnd = dataBegin + dataSize;

			nodeValue = AllocateString(dataBegin, dataEnd, true);

			p = dataEnd;
		}
		else if (vcpp::parsers::startsWith(p, end, "\"\"\"", &matchEndPtr))
		{
			p = matchEndPtr;

			const char* rawStringBegin = p;
			const char* rawStringEnd = vcpp::parsers::findString(rawStringBegin, end, "\"\"\"", rawStringBegin);
			if (rawStringEnd > rawStringBegin)
			{
				p = rawStringEnd + strlen("\"\"\"");

				nodeValue = AllocateString(rawStringBegin, rawStringEnd, false);
			}
			else
			{
				Log::Error("Parse raw string failed");
				return false;
			}
		}
		else if (vcpp::parsers::startsWith(p, end, "\""))
		{
			std::string stringValue;
			if (!vcpp::parsers::parseQuotedString(&p, end, stringValue, '"', true))
			{
				Log::Error("Parse quoted string value failed");
				return false;
			}

			nodeValue = AllocateString(stringValue.data(), stringValue.data() + stringValue.size(), false);
		}
		else if (vcpp::parsers::startsWith(p, end, "'"))
		{
			std::string stringValue;
			if (!vcpp::parsers::parseQuotedString(&p, end, stringValue, '\'', true))
			{
				Log::Error("Parse quoted string value failed");
				return false;
			}

			nodeValue = AllocateString(stringValue.data(), stringValue.data() + stringValue.size(), false);
		}
		else
		{
			const char* inlineDataBegin = p;
			const char* inlineDataEnd = vcpp::parsers::findFirstInSet(inlineDataBegin, end, vcpp::char_set::line_endings(), inlineDataBegin);

			p = inlineDataEnd;

			if (vcpp::parsers::contains(inlineDataBegin, inlineDataEnd, inlineDataChars))
			{
				nodeValue = AllocateString(inlineDataBegin, inlineDataEnd, false);
			}
		}

		//
		// create node
		//
		if (nodeName.size() != 0)
		{
			// find parent node
			int parentNodeIndex = -1;
			ArchiveNode* parentNode = nullptr;
			for (int i = parsingNodeStack.size() - 1; i >= 0; --i)
			{
				ArchiveNode* node = parsingNodeStack[i];
				if (node->GetDepth() == indents - 1)
				{
					parentNode = node;
					parentNodeIndex = i;
					break;
				}
			}

			if (parentNode == nullptr)
			{
				Log::Error("Can not find valid parent for indent %d", indents);
				return false;
			}

			// pop stack
			parsingNodeStack.erase(parsingNodeStack.begin() + parentNodeIndex + 1, parsingNodeStack.end());

			// create new node
			ArchiveNode* node = parentNode->AddChild(nodeName.c_str());
			node->SetValue(nodeValue);

			parsingNodeStack.push_back(node);
		}
	}

	return true;
}

bool Archive::WriteTo(WriteStream* stream)
{
	std::string indentsBuffer;

	return WriteToStreamRecursive(root, stream, indentsBuffer);
}

bool Archive::WriteToStreamRecursive(ArchiveNode* node, WriteStream* stream, std::string& indentsBuffer)
{
	int indents = node->GetDepth();
	ArchiveString nodeName = node->GetName();
	ArchiveString nodeValue = node->GetValue();

	indentsBuffer.resize(indents, '\t');
	stream->Write(indentsBuffer.data(), indentsBuffer.size());

	stream->Write(nodeName.Data(), nodeName.Size());

	if (nodeValue.IsValid())
	{
		stream->Write(" ", 1);

		if (nodeValue.isBinary)
		{
			std::string header = formatString("@bin %d ", nodeValue.Size());
			stream->Write(header.data(), header.size());

			stream->Write(nodeValue.Data(), nodeValue.Size());
		}
		else
		{
			stream->Write("\"", 1);
			stream->Write(nodeValue.Data(), nodeValue.Size());
			stream->Write("\"", 1);
		}
	}

	stream->Write("\n", 1);

	for (int i = 0; i < node->ChildCount(); i++)
	{
		ArchiveNode* childNode = node->GetChild(i);
		WriteToStreamRecursive(childNode, stream, indentsBuffer);
	}

	return true;
}

std::string Archive::WriteToString()
{
	MemoryWriteStream stream;
	if (WriteTo(&stream))
	{
		std::string stringData;
		stringData.assign((const char*)stream.Data(), stream.Size());

		return stringData;
	}
	else
	{
		return std::string();
	}
}
