#pragma once

#include "AssetManager.h"
#include "../RenderSystem/Texture2D.h"

bool ImportTexture2D(AssetPath srcPath, Texture2D& dstTexture);

class TextureImporter : public AssetProcessor
{
	DECL_CLASS_TYPE(TextureImporter, AssetProcessor);

	TextureImporter();
	virtual ~TextureImporter();

	virtual void Reflect(Reflector& reflector) override;

	virtual bool DoProcess(const std::vector<AssetPath>& inputAssetPaths, AssetProcessorOutput* processorOutput) override;

	bool sRGB;

	bool useInIBL;

};
