#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "InputManager.h"

IMPL_ABSTRACT_CLASS_TYPE(InputManager);

KeyEventListener::KeyEventListener()
{
}

KeyEventListener::~KeyEventListener()
{
	g_lawnEngine.getInputManager().removeKeyEventListener(this);
}

MouseEventListener::MouseEventListener()
{
}

MouseEventListener::~MouseEventListener()
{
	g_lawnEngine.getInputManager().removeMouseEventListener(this);
}
