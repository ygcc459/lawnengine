#include "../fast_compile.h"
#include "LawnEngineInternal.h"
#include "OISInputManager.h"

#include <sstream>
#include <OIS.h>
#include <algorithm>

#include "UISystem/imgui/imgui.h"

//////////////////////////////////////////////////////////////////////////

class OISEventProxy
	: public OIS::KeyListener, public OIS::MouseListener
{
private:
	OISInputManager* theInputManager;

public:
	OISEventProxy(OISInputManager* theInputManager)
		:theInputManager(theInputManager)
	{}

	bool keyPressed(const OIS::KeyEvent &arg)
	{
		ImGuiIO& io = ImGui::GetIO();
		if (io.WantCaptureKeyboard)
			return true;

		theInputManager->triggerKeyPressed((KeyCode)arg.key, (uint32_t)arg.text);
		//Log::Info("keyPressed %d %d", (KeyCode)arg.key, (uint32_t)arg.text);
		return true;
	}

	bool keyReleased(const OIS::KeyEvent &arg)
	{
		ImGuiIO& io = ImGui::GetIO();
		if (io.WantCaptureKeyboard)
			return true;

		theInputManager->triggerKeyReleased((KeyCode)arg.key, (uint32_t)arg.text);
		//Log::Info("keyReleased %d %d", (KeyCode)arg.key, (uint32_t)arg.text);
		return true;
	}

	bool mouseMoved(const OIS::MouseEvent &arg)
	{
		ImGuiIO& io = ImGui::GetIO();
		if (io.WantCaptureMouse)
			return true;

		int3 position = int3(arg.state.X.abs, arg.state.Y.abs, arg.state.Z.abs);
		int3 delta = int3(arg.state.X.rel, arg.state.Y.rel, arg.state.Z.rel);

		theInputManager->triggerMouseMove(position, delta);

		return true;
	}

	bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
	{
		ImGuiIO& io = ImGui::GetIO();
		if (io.WantCaptureMouse)
			return true;

		MouseButton button = ConvertMouseButton(id);

		int3 position = int3(arg.state.X.abs, arg.state.Y.abs, arg.state.Z.abs);
		int3 delta = int3(arg.state.X.rel, arg.state.Y.rel, arg.state.Z.rel);

		theInputManager->triggerMousePressed(button, position, delta);

		return true;
	}

	bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
	{
		ImGuiIO& io = ImGui::GetIO();
		if (io.WantCaptureMouse)
			return true;

		MouseButton button = ConvertMouseButton(id);

		int3 position = int3(arg.state.X.abs, arg.state.Y.abs, arg.state.Z.abs);
		int3 delta = int3(arg.state.X.rel, arg.state.Y.rel, arg.state.Z.rel);

		theInputManager->triggerMouseReleased(button, position, delta);

		return true;
	}

private:
	static MouseButton ConvertMouseButton(OIS::MouseButtonID id)
	{
		switch (id)
		{
		case OIS::MouseButtonID::MB_Left: return MouseButton::MB_Left;
		case OIS::MouseButtonID::MB_Right: return MouseButton::MB_Right;
		case OIS::MouseButtonID::MB_Middle: return MouseButton::MB_Middle;
		case OIS::MouseButtonID::MB_Button3: return MouseButton::MB_Button3;
		case OIS::MouseButtonID::MB_Button4: return MouseButton::MB_Button4;
		case OIS::MouseButtonID::MB_Button5: return MouseButton::MB_Button5;
		case OIS::MouseButtonID::MB_Button6: return MouseButton::MB_Button6;
		case OIS::MouseButtonID::MB_Button7: return MouseButton::MB_Button7;
		default: return MouseButton::MB_None;
		}
	}
};

//////////////////////////////////////////////////////////////////////////

IMPL_CLASS_TYPE(OISInputManager, InputManager);

OISInputManager::OISInputManager()
	: inputManager()
	, keyboard()
	, mouse()
	, eventProxy(nullptr)
	, keyFocus(nullptr)
	, keyFocusCustomData(nullptr)
	, mouseFocus(nullptr)
	, mouseFocusCustomData(nullptr)
	, curMouseRaycastListener(nullptr)
	, curMouseRaycastCustomData(nullptr)
{
}

OISInputManager::OISInputManager(HWND captureWindow)
	: inputManager()
	, keyboard()
	, mouse()
	, eventProxy(new OISEventProxy(this))
	, keyFocus(nullptr)
	, keyFocusCustomData(nullptr)
	, mouseFocus(nullptr)
	, mouseFocusCustomData(nullptr)
	, curMouseRaycastListener(nullptr)
	, curMouseRaycastCustomData(nullptr)
{
	using namespace OIS;

	try
	{
		ParamList pl;

		std::ostringstream wnd;
		wnd << (size_t)captureWindow;

		pl.insert(std::make_pair(std::string("WINDOW"), wnd.str()));

		//Default mode is foreground exclusive..but, we want to show mouse - so nonexclusive
		pl.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_FOREGROUND" )));
		pl.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_NONEXCLUSIVE")));
		pl.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_FOREGROUND")));
		pl.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_NONEXCLUSIVE")));

		//This never returns null.. it will raise an exception on errors
		inputManager = OIS::InputManager::createInputSystem(pl);

		//Lets enable all add-ons that were compiled in:
		inputManager->enableAddOnFactory(OIS::InputManager::AddOn_All);

		keyboard = (Keyboard*)inputManager->createInputObject(OISKeyboard, true);
		keyboard->setEventCallback(eventProxy);

		mouse = (Mouse*)inputManager->createInputObject(OISMouse, true);
		mouse->setEventCallback(eventProxy);
	}
	catch(OIS::Exception& e)
	{
		Log::Error("OIS Exception: %s", e.what());
		Log::Error("exception at '%s' line %d", e.eFile, e.eLine);
	}
}

OISInputManager::~OISInputManager()
{
	if(inputManager)
	{
		OIS::InputManager::destroyInputSystem(inputManager);
	}

	delete eventProxy;
}

void OISInputManager::onWindowResized(int newWidth, int newHeight)
{
	const OIS::MouseState &ms = mouse->getMouseState();
	ms.width = newWidth;
	ms.height = newHeight;
}

void OISInputManager::onNextFrame()
{
	keyboard->capture();
	mouse->capture();
}

//////////////////////////////////////////////////////////////////////////

void OISInputManager::addKeyEventListener(KeyEventListener* t)
{
	if (std::find(keyListeners.begin(), keyListeners.end(), t) == keyListeners.end())
		keyListeners.push_back(t);
}

void OISInputManager::removeKeyEventListener(KeyEventListener* t)
{
	std::remove(keyListeners.begin(), keyListeners.end(), t);

	if (keyFocus == t)
	{
		setKeyFocus(nullptr, nullptr);
	}
}

void OISInputManager::clearKeyEventListeners()
{
	keyListeners.clear();
}

bool OISInputManager::isKeyDown(KeyCode keycode)
{
	return keyboard->isKeyDown((OIS::KeyCode)keycode);
}

void OISInputManager::setKeyFocus(KeyEventListener* listener, BaseObject* customData)
{
	if (keyFocus != listener || keyFocusCustomData != customData)
	{
		if (keyFocus)
		{
			keyFocus->OnKeyEvent(KeyEvent(KeyEventType::KET_LostFocus, KeyCode::KC_UNASSIGNED, 0), keyFocusCustomData);
		}

		keyFocus = listener;
		keyFocusCustomData = customData;

		if (keyFocus)
		{
			keyFocus->OnKeyEvent(KeyEvent(KeyEventType::KET_GetFocus, KeyCode::KC_UNASSIGNED, 0), keyFocusCustomData);
		}
	}
}

void OISInputManager::getKeyFocus(KeyEventListener** outListener, BaseObject** outCustomData)
{
	if (outListener)
		(*outListener) = keyFocus;

	if (outCustomData)
		(*outCustomData) = keyFocusCustomData;
}

void OISInputManager::triggerKeyPressed(KeyCode keycode, uint32_t text)
{
	if (keyFocus)
	{
		keyFocus->OnKeyEvent(KeyEvent(KeyEventType::KET_KeyPressed, keycode, text), keyFocusCustomData);
	}
}

void OISInputManager::triggerKeyReleased(KeyCode keycode, uint32_t text)
{
	if (keyFocus)
	{
		keyFocus->OnKeyEvent(KeyEvent(KeyEventType::KET_KeyReleased, keycode, text), keyFocusCustomData);
	}
}

void OISInputManager::triggerCharacterInput(uint32 c)
{
	if (keyFocus)
	{
		keyFocus->OnKeyEvent(KeyEvent(KeyEventType::KET_CharacterInput, KeyCode::KC_UNASSIGNED, c), keyFocusCustomData);
	}
}

//////////////////////////////////////////////////////////////////////////

void OISInputManager::addMouseEventListener(MouseEventListener* t)
{
	if (std::find(mouseListeners.begin(), mouseListeners.end(), t) == mouseListeners.end())
	{
		mouseListeners.push_back(t);

		std::sort(mouseListeners.begin(), mouseListeners.end(),
			[](MouseEventListener* a, MouseEventListener* b)
			{
				return a->GetMouseRaycastPriority() > b->GetMouseRaycastPriority();
			});
	}
}

void OISInputManager::removeMouseEventListener(MouseEventListener* t)
{
	std::remove(mouseListeners.begin(), mouseListeners.end(), t);
}

void OISInputManager::clearMouseEventListeners()
{
	mouseListeners.clear();
}

MouseState& OISInputManager::GetMouseState()
{
	return mouseState;
}

void OISInputManager::setMouseFocus(MouseEventListener* listener, BaseObject* customData)
{
	this->mouseFocus = listener;
	this->mouseFocusCustomData = customData;
}

void OISInputManager::getMouseFocus(MouseEventListener** outListener, BaseObject** outCustomData)
{
	(*outListener) = this->mouseFocus;
	(*outCustomData) = this->mouseFocusCustomData;
}

void OISInputManager::MouseRaycastAndFocus(const MouseEvent& ev, const MouseState& state)
{
	MouseEventListener* raycastListener = nullptr;
	BaseObject* raycastCustomData = nullptr;

	if (mouseFocus)
	{
		raycastListener = mouseFocus;
		raycastCustomData = mouseFocusCustomData;
	}
	else
	{
		for (MouseEventListener* listener : mouseListeners)
		{
			BaseObject* p = listener->OnMouseRaycast(ev, state);
			if (p)
			{
				raycastListener = listener;
				raycastCustomData = p;
				break;
			}
		}
	}

	if (raycastCustomData != curMouseRaycastCustomData || raycastListener != curMouseRaycastListener)
	{
		// send mouse leave event
		if (curMouseRaycastListener)
		{
			curMouseRaycastListener->OnMouseEvent(curMouseRaycastCustomData, MouseEvent(MouseEventType::MET_Leave, MouseButton::MB_None), state);
		}

		curMouseRaycastListener = raycastListener;
		curMouseRaycastCustomData = raycastCustomData;

		if (curMouseRaycastCustomData)
		{
			curMouseRaycastCustomData->destructionEvent.AddListener(this, [=](BaseObject*)
				{
					curMouseRaycastCustomData = nullptr;
				});
		}

		// send mouse enter event
		if (curMouseRaycastListener)
		{
			curMouseRaycastListener->OnMouseEvent(curMouseRaycastCustomData, MouseEvent(MouseEventType::MET_Enter, MouseButton::MB_None), state);
		}
	}
}

void OISInputManager::triggerMouseMove(const int3& position, const int3& delta)
{
	mouseState.position = position;
	mouseState.delta = delta;

	MouseEvent mouseEvent = MouseEvent(MouseEventType::MET_Move, MouseButton::MB_None);

	MouseRaycastAndFocus(mouseEvent, mouseState);

	if (curMouseRaycastListener)
	{
		curMouseRaycastListener->OnMouseEvent(curMouseRaycastCustomData, mouseEvent, mouseState);
		return;
	}

	// pointer events
	{
		for (int pointerIdInt = (int)PointerId::MouseButtonLeft; pointerIdInt != (int)PointerId::MouseButtonMax; pointerIdInt++)
		{
			PointerId pointerId = (PointerId)pointerIdInt;
			if (HasPointerState(pointerId))
			{
				PointerState& pointerState = GetOrCreatePointerState(pointerId);

				if (pointerState.internalState == PointerInternalState::Pressed)
				{
					pointerState.internalState = PointerInternalState::Dragging;
					pointerState.startPosition = pointerState.curPosition;

					float2 newPosition = float2((float)position.x, (float)position.y);
					pointerState.curDeltaPosition = newPosition - pointerState.curPosition;
					pointerState.curPosition = newPosition;

					triggerPointerBeginDrag(pointerState);
				}
				else if (pointerState.internalState == PointerInternalState::Dragging)
				{
					float2 newPosition = float2((float)position.x, (float)position.y);
					pointerState.curDeltaPosition = newPosition - pointerState.curPosition;
					pointerState.curPosition = newPosition;
					triggerPointerDragging(pointerState);
				}
				else if (pointerState.internalState == PointerInternalState::Released)
				{
					// do nothing
				}
			}
		}
	}
}

void OISInputManager::triggerMousePressed(MouseButton button, const int3& position, const int3& delta)
{
	mouseState.position = position;
	mouseState.delta = delta;
	mouseState.SetButtonDown(button, true);

	MouseEvent mouseEvent = MouseEvent(MouseEventType::MET_ButtonDown, button);

	MouseRaycastAndFocus(mouseEvent, mouseState);

	// if anything can handle this mouse-press event, send the event
	if (curMouseRaycastListener)
	{
		curMouseRaycastListener->OnMouseEvent(curMouseRaycastCustomData, mouseEvent, mouseState);
		return;
	}

	// if nothing can handle mouse-press, prepare for touch clicking or dragging
	{
		PointerId pointerId;
		if (button == MouseButton::MB_Left)
			pointerId = PointerId::MouseButtonLeft;
		else if (button == MouseButton::MB_Right)
			pointerId = PointerId::MouseButtonRight;
		else if (button == MouseButton::MB_Middle)
			pointerId = PointerId::MouseButtonMiddle;
		else
			return;

		PointerState& pointerState = GetOrCreatePointerState(pointerId);
		pointerState.curPosition = float2((float)position.x, (float)position.y);
		pointerState.curDeltaPosition = float2(0, 0);

		if (pointerState.internalState == PointerInternalState::Dragging)
		{
			triggerPointerEndDrag(pointerState);
		}

		pointerState.internalState = PointerInternalState::Pressed;
		pointerState.startPosition = pointerState.curPosition;
	}
}

void OISInputManager::triggerMouseReleased(MouseButton button, const int3& position, const int3& delta)
{
	mouseState.position = position;
	mouseState.delta = delta;
	mouseState.SetButtonDown(button, false);

	MouseEvent mouseEvent = MouseEvent(MouseEventType::MET_ButtonUp, button);

	MouseRaycastAndFocus(mouseEvent, mouseState);

	if (curMouseRaycastListener)
	{
		curMouseRaycastListener->OnMouseEvent(curMouseRaycastCustomData, mouseEvent, mouseState);
	}

	// cancel pointer events
	{
		PointerId pointerId;
		if (button == MouseButton::MB_Left)
			pointerId = PointerId::MouseButtonLeft;
		else if (button == MouseButton::MB_Right)
			pointerId = PointerId::MouseButtonRight;
		else if (button == MouseButton::MB_Middle)
			pointerId = PointerId::MouseButtonMiddle;
		else
			return;

		if (HasPointerState(pointerId))
		{
			PointerState& pointerState = GetOrCreatePointerState(pointerId);
			pointerState.curPosition = float2((float)position.x, (float)position.y);
			pointerState.curDeltaPosition = float2(0, 0);

			if (pointerState.internalState == PointerInternalState::Pressed)
			{
				triggerPointerClick(pointerState);
			}
			else if (pointerState.internalState == PointerInternalState::Dragging)
			{
				triggerPointerEndDrag(pointerState);
			}

			pointerState.internalState = PointerInternalState::Released;
			pointerState.startPosition = pointerState.curPosition;
		}
	}
}

//////////////////////////////////////////////////////////////////////////

void OISInputManager::addPointerEventListener(PointerEventListener* t)
{
	if (std::find(pointerListeners.begin(), pointerListeners.end(), t) == pointerListeners.end())
	{
		pointerListeners.push_back(t);

		std::sort(pointerListeners.begin(), pointerListeners.end(),
			[](PointerEventListener* a, PointerEventListener* b)
			{
				return a->GetPointerEventPriority() > b->GetPointerEventPriority();
			});
	}
}

void OISInputManager::removePointerEventListener(PointerEventListener* t)
{
	std::remove(pointerListeners.begin(), pointerListeners.end(), t);
}

bool OISInputManager::HasPointerState(PointerId pointerId) const
{
	auto it = pointers.find(pointerId);
	return it != pointers.end();
}

PointerState& OISInputManager::GetOrCreatePointerState(PointerId pointerId)
{
	auto it = pointers.find(pointerId);
	if (it == pointers.end())
		it = pointers.insert(std::make_pair(pointerId, PointerState(pointerId))).first;

	PointerState& pointerState = it->second;

	return pointerState;
}

bool OISInputManager::triggerPointerClick(PointerState& pointer)
{
	for (size_t i = 0; i < pointerListeners.size(); i++)
	{
		PointerEventListener* listener = pointerListeners[i];
		if (listener->OnPointerClick(pointer))
			return true;
	}
	return false;
}


bool OISInputManager::triggerPointerBeginDrag(PointerState& pointer)
{
	for (size_t i = 0; i < pointerListeners.size(); i++)
	{
		PointerEventListener* listener = pointerListeners[i];
		if (listener->OnPointerBeginDrag(pointer))
			return true;
	}
	return false;
}

bool OISInputManager::triggerPointerDragging(PointerState& pointer)
{
	for (size_t i = 0; i < pointerListeners.size(); i++)
	{
		PointerEventListener* listener = pointerListeners[i];
		if (listener->OnPointerDragging(pointer))
			return true;
	}
	return false;
}

bool OISInputManager::triggerPointerEndDrag(PointerState& pointer)
{
	for (size_t i = 0; i < pointerListeners.size(); i++)
	{
		PointerEventListener* listener = pointerListeners[i];
		if (listener->OnPointerEndDrag(pointer))
			return true;
	}
	return false;
}
