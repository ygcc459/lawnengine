#pragma	once

#include "InputManager.h"

namespace OIS
{
	class InputManager;
	class Keyboard;
	class Mouse;
}

class OISEventProxy;

class OISInputManager : public InputManager
{
	DECL_CLASS_TYPE(OISInputManager, InputManager);

public:
	OISInputManager();
	OISInputManager(HWND captureWindow);

	virtual ~OISInputManager();

	virtual void onWindowResized(int newWidth, int newHeight);
	virtual void onNextFrame();

	virtual void setKeyFocus(KeyEventListener* listener, BaseObject* customData) override;
	virtual void getKeyFocus(KeyEventListener** outListener, BaseObject** outCustomData) override;

	virtual void setMouseFocus(MouseEventListener* listener, BaseObject* customData) override;
	virtual void getMouseFocus(MouseEventListener** outListener, BaseObject** outCustomData) override;

	virtual void addPointerEventListener(PointerEventListener* t) override;
	virtual void removePointerEventListener(PointerEventListener* t) override;

	virtual void addKeyEventListener(KeyEventListener* t);
	virtual void removeKeyEventListener(KeyEventListener* t);
	virtual void clearKeyEventListeners();

	virtual bool isKeyDown(KeyCode keycode);

	virtual void addMouseEventListener(MouseEventListener* t);
	virtual void removeMouseEventListener(MouseEventListener* t);
	virtual void clearMouseEventListeners();

	virtual void triggerCharacterInput(uint32 c) override;

	virtual MouseState& GetMouseState() override;

	void triggerKeyPressed(KeyCode keycode, uint32_t text);
	void triggerKeyReleased(KeyCode keycode, uint32_t text);

	void triggerMouseMove(const int3& position, const int3& delta);
	void triggerMousePressed(MouseButton button, const int3& position, const int3& delta);
	void triggerMouseReleased(MouseButton button, const int3& position, const int3& delta);

	bool triggerPointerClick(PointerState& pointer);
	bool triggerPointerBeginDrag(PointerState& pointer);
	bool triggerPointerDragging(PointerState& pointer);
	bool triggerPointerEndDrag(PointerState& pointer);

private:
	void MouseRaycastAndFocus(const MouseEvent& ev, const MouseState& state);

	bool HasPointerState(PointerId pointerId) const;
	PointerState& GetOrCreatePointerState(PointerId pointerId);

public:
	OIS::InputManager* getOISInputManager() const {
		return inputManager;
	}

	OIS::Keyboard* getOISKeyboardDevice() const {
		return keyboard;
	}

	OIS::Mouse* getOISMouseDevice() const {
		return mouse;
	}

protected:
	OIS::InputManager* inputManager;
	OIS::Keyboard* keyboard;
	OIS::Mouse* mouse;

	KeyEventListener* keyFocus;
	BaseObject* keyFocusCustomData;

	std::vector<KeyEventListener*> keyListeners;

	MouseEventListener* mouseFocus;
	BaseObject* mouseFocusCustomData;

	std::vector<MouseEventListener*> mouseListeners;

	std::vector<PointerEventListener*> pointerListeners;

	OISEventProxy* eventProxy;

	MouseState mouseState;
	MouseEventListener* curMouseRaycastListener;
	BaseObject* curMouseRaycastCustomData;

	std::map<PointerId, PointerState> pointers;

};
