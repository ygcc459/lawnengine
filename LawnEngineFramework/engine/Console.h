#pragma once

class ConsoleVariableBase
{
public:
	virtual FixedString GetName() const = 0;
	virtual Type* GetType() const = 0;
	virtual std::string ToString() const = 0;
	virtual bool FromString(const std::string& s) = 0;
	virtual void* GetValuePtr() = 0;
};

template<typename T>
class ConsoleVariable : public ConsoleVariableBase
{
public:
	T value;	
	
	Type* type;
	
	FixedString name;

public:
	ConsoleVariable(const FixedString& name, const T& initialValue)
		: name(name), value(initialValue), type(GetStaticType<T>())
	{
	}

	~ConsoleVariable()
	{
	}

	T GetValue()
	{
		return value;
	}

	virtual FixedString GetName() const override
	{
		return name;
	}

	virtual Type* GetType() const override
	{
		return type;
	}

	virtual std::string ToString() const override
	{
		std::string s;
		type->ToString(&value, s);
		return s;
	}

	virtual bool FromString(const std::string& s) override
	{
		return type->FromString(s, &value);
	}

	virtual void* GetValuePtr()
	{
		return &value;
	}

private:
	void Register();

};

class Console
{
public:
	static Console& GetInstance()
	{
		if (instance == nullptr)
			instance = new Console();
		return *instance;
	}

	template<typename T>
	ConsoleVariable<T>* GetVariable(const FixedString& name)
	{
		auto it = nameToVariableMap.find(name);
		if (it != nameToVariableMap.end())
		{
			ConsoleVariableBase* variableBase = it->second;
			CHECK(variableBase->GetType() == GetStaticType<T>());

			ConsoleVariable<T>* variableTyped = (ConsoleVariable<T>*)variableBase;
			return variableTyped;
		}
		else
		{
			return nullptr;
		}
	}

	template<typename T>
	T GetVariableValue(const FixedString& name, const T& defaultValue = T())
	{
		ConsoleVariable<T>* variableTyped = GetVariable<T>(name);
		if (variableTyped)
		{
			return variableTyped->GetValue();
		}
		else
		{
			return defaultValue;
		}
	}

	template<typename T>
	void Register(ConsoleVariable<T>* variableTyped)
	{
		CHECK(GetVariable(variableTyped->name) == nullptr);

		variables.push_back(variableTyped);
		nameToVariableMap.insert(std::make_pair(variableTyped->name, variableTyped));
	}

private:
	static Console* instance;

	std::vector<ConsoleVariableBase*> variables;

	std::map<FixedString, ConsoleVariableBase*> nameToVariableMap;
};

template<typename T>
void ConsoleVariable<T>::Register()
{
	Console::GetInstance().Register(this);
}
