#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "InGameEditor.h"
#include "ImGUISystem.h"
#include "RenderSystem/RenderSystem.h"
#include "EntitySystem/World.h"
#include "EntitySystem/WorldQuery.h"
#include "EntitySystem/Transform.h"
#include "RenderSystem/Material.h"
#include "EntitySystem/Camera.h"
#include "EntitySystem/MeshRenderer.h"
#include "EntitySystem/GizmoRenderer.h"
#include "AssetSystem/AssetManager.h"
#include "AssetSystem/FileSystem.h"
#include "RenderSystem/Texture.h"
#include "RenderSystem/Texture2D.h"
#include "RenderSystem/Sprite.h"
#include "UISystem/UIImage.h"
#include "UISystem/UIRawImage.h"
#include "UISystem/UIText.h"
#include "UISystem/UIButton.h"
#include "UISystem/UIToggleButton.h"
#include "UISystem/UIInput.h"
#include "UISystem/UILinearLayout.h"
#include "RenderSystem/RenderView.h"
#include "AssetSystem/TextureImporter.h"

InGameEditor InGameEditor::instance;

InGameEditor::InGameEditor()
	: axisGizmoGroup(), gridGizmoGroup()
{
}

InGameEditor::~InGameEditor()
{
}

void InGameEditor::Startup()
{
	GizmoRenderer* gizmoRenderer = g_lawnEngine.getWorld().GetSingletonComponent<GizmoRenderer>();

	axisGizmoGroup = gizmoRenderer->AddGroup();
	{
		axisGizmoGroup->AddLine(float3(0, 0, 0), float3(1, 0, 0), color32(255, 0, 0, 255));
		axisGizmoGroup->AddLine(float3(0, 0, 0), float3(0, 1, 0), color32(0, 255, 0, 255));
		axisGizmoGroup->AddLine(float3(0, 0, 0), float3(0, 0, 1), color32(0, 0, 255, 255));

		axisGizmoGroup->AddSphere(float3(0, 0, 0), 0.1f, color32(255, 255, 255, 255));
		axisGizmoGroup->AddSphere(float3(1, 0, 0), 0.1f, color32(255, 0, 0, 255));
		axisGizmoGroup->AddSphere(float3(0, 1, 0), 0.1f, color32(0, 255, 0, 255));
		axisGizmoGroup->AddSphere(float3(0, 0, 1), 0.1f, color32(0, 0, 255, 255));
	}

	gridGizmoGroup = gizmoRenderer->AddGroup();
	{
		for (int ix = -100; ix <= 100; ix++)
		{
			gridGizmoGroup->AddLine(float3((float)ix, 0, -100), float3((float)ix, 0, 100), color32(128, 128, 128, 255));
		}
		for (int iz = -100; iz <= 100; iz++)
		{
			gridGizmoGroup->AddLine(float3(-100, 0, (float)iz), float3(100, 0, (float)iz), color32(128, 128, 128, 255));
		}
	}


	g_lawnEngine.getInputManager().addPointerEventListener(this);

	sceneHierarchyWindow.Startup();
	inspectorWindow.Startup();
	assetsWindow.Startup();
	texturePreviewWindow.Startup();
}

void InGameEditor::Shutdown()
{
	g_lawnEngine.getInputManager().removePointerEventListener(this);

	sceneHierarchyWindow.Shutdown();
	inspectorWindow.Shutdown();
	assetsWindow.Shutdown();
	texturePreviewWindow.Shutdown();
}

void InGameEditor::Render()
{
	sceneHierarchyWindow.Render();
	inspectorWindow.Render();
	assetsWindow.Render();
	texturePreviewWindow.Render();
}

void InGameEditor::SetSelectedObject(BaseObject* obj)
{
	if (selectedObject != obj)
	{
		if (selectedObject)
		{
			selectedObject->destructionEvent.RemoveListener(this);

			selectedObject = nullptr;
		}
		
		selectedObject = obj;

		if (selectedObject)
		{
			selectedObject->destructionEvent.AddListener(this, [=](BaseObject* o)
				{
					SetSelectedObject(nullptr);
				});

			inspectorWindow.SetTarget(selectedObject);
			sceneHierarchyWindow.SetSelectedObject(Util::CastPtrWithCheck<SceneObject>(selectedObject));
		}
	}
}

float InGameEditor::GetPointerEventPriority()
{
	return 10;
}

bool InGameEditor::OnPointerClick(PointerState& pointer)
{
	if (pointer.pointerId == PointerId::MouseButtonLeft)
	{
		World& world = g_lawnEngine.getWorld();

		std::vector<Component*>& cameras = world.GetComponentRegistry().GetComponents(ComponentRegistryType::CRT_Cameras);
		
		if (cameras.size() > 0)
		{
			Camera* cam = Util::CastPtrWithCheck<Camera>(cameras[0]);
			if (cam)
			{
				EditorPickInput pickInput;
				pickInput.rayOrigin = cam->GetTransform().WorldPosition();
				pickInput.rayDirection = cam->ScreenPointToRayDir(pointer.curPosition);

				std::vector<Component*>& pickables = world.GetComponentRegistry().GetComponents(ComponentRegistryType::CRT_EditorPickable);

				std::vector<std::pair<Component*, float>> pickableDistances;

				for (int i = 0; i < pickables.size(); i++)
				{
					Component* comp = pickables[i];

					float distance = comp->OnEditorPick(pickInput);
					if (distance >= 0 && distance != std::numeric_limits<float>::max())
					{
						pickableDistances.push_back(std::make_pair(comp, distance));
					}
				}

				if (pickableDistances.size() > 0)
				{
					std::sort(pickableDistances.begin(), pickableDistances.end(), [](auto a, auto b)
						{
							return a.second < b.second;
						});

					//
					// set selection to nearest hit component
					//
					Component* pickedComponent = pickableDistances[0].first;

					InGameEditor::GetInstance().SetSelectedObject(&pickedComponent->GetSceneObject());

					return true;
				}

				//
				// clear selection if not hit while something is selected
				//
				if (GetSelectedObject() != nullptr)
				{
					InGameEditor::GetInstance().SetSelectedObject(nullptr);
					return true;
				}
			}
		}
	}

	return false;
}

bool InGameEditor::OnPointerBeginDrag(PointerState& pointer)
{
	return false;
}

bool InGameEditor::OnPointerDragging(PointerState& pointer)
{
	return false;
}

bool InGameEditor::OnPointerEndDrag(PointerState& pointer)
{
	return false;
}

//////////////////////////////////////////////////////////////////////////

SceneHierarchyWindow::SceneHierarchyWindow()
	: show(true)
	, clickingSceneObject(nullptr)
{
}

SceneHierarchyWindow::~SceneHierarchyWindow()
{
}

void SceneHierarchyWindow::Startup()
{
	baseNodeFlags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_SpanFullWidth;

	g_lawnEngine.getWorld().DestroySceneObjectEvent.AddListener(this, [=](SceneObject* so) { OnWorldSceneObjectDestroyed(so); });
}

void SceneHierarchyWindow::Shutdown()
{
}

void SceneHierarchyWindow::Render()
{
	clickingSceneObject = nullptr;

	if (ImGui::Begin("World Hierarchy", &show))
	{
		World& world = g_lawnEngine.getWorld();

		std::vector<SceneObject*>& allSceneObjects = world.GetSceneObjects();

		for (size_t i = 0; i < allSceneObjects.size(); i++)
		{
			SceneObject* so = allSceneObjects[i];

			if (so->GetParent() == nullptr)  //draw from roots
			{
				RenderSceneObjectNode(so);
			}
		}

		// Update selection state
		// (process outside of tree loop to avoid visual inconsistencies during the clicking frame)
		if (clickingSceneObject)
		{
			auto* node = GetOrCreateSceneObjectNode(clickingSceneObject);

			if (ImGui::GetIO().KeyCtrl)  // ctrl + click to toggle selection
			{
				node->selected = !node->selected;
			}
			else  //click to selection single SceneObject
			{
				SetSelectedObject(clickingSceneObject);

				InGameEditor::GetInstance().SetSelectedObject(clickingSceneObject);
			}

			clickingSceneObject = nullptr;
		}
	}
	ImGui::End();
}

void SceneHierarchyWindow::RenderSceneObjectNode(SceneObject* so)
{
	SceneObjectNode* node = GetOrCreateSceneObjectNode(so);

	ImGuiTreeNodeFlags node_flags = baseNodeFlags;

	if (node->selected)
		node_flags |= ImGuiTreeNodeFlags_Selected;

	if (so->GetChildCount() == 0)
		node_flags |= ImGuiTreeNodeFlags_Leaf;

	bool node_open = ImGui::TreeNodeEx(node, node_flags, "%s", so->name.c_str());

	if (ImGui::IsItemClicked() && !ImGui::IsItemToggledOpen())
	{
		clickingSceneObject = so;
	}

	if (ImGui::BeginDragDropSource())
	{
		ImGui::SetDragDropPayload("_TREENODE", NULL, 0);
		ImGui::Text("This is a drag and drop source");
		ImGui::EndDragDropSource();
	}

	if (node_open)
	{
		for (size_t i = 0; i < so->GetChildCount(); i++)
		{
			SceneObject* child = so->GetChild(i);

			RenderSceneObjectNode(child);
		}

		ImGui::TreePop();
	}
}

void SceneHierarchyWindow::SetSelectedObject(SceneObject* obj)
{
	ClearSceneObjectNodeSelection();

	auto* node = GetOrCreateSceneObjectNode(obj);
	if (node)
	{
		node->selected = true;
	}
}

void SceneHierarchyWindow::ClearSceneObjectNodeSelection()
{
	for (auto it = nodes.begin(); it != nodes.end(); it++)
	{
		SceneObjectNode* node = it->second.get();
		node->selected = false;
	}
}

SceneHierarchyWindow::SceneObjectNode* SceneHierarchyWindow::GetOrCreateSceneObjectNode(SceneObject* so)
{
	auto it = nodes.find(so);
	if (it == nodes.end())
		it = nodes.insert(std::make_pair(so, std::make_unique<SceneObjectNode>())).first;

	return it->second.get();
}

void SceneHierarchyWindow::OnWorldSceneObjectDestroyed(SceneObject* so)
{
	nodes.erase(so);
}

//////////////////////////////////////////////////////////////////////////

InspectorWindow::InspectorWindow()
	: show(true)
	, navigationCurrentIndex(0)
{
}

InspectorWindow::~InspectorWindow()
{
}

void InspectorWindow::Startup()
{
	tableFlags = ImGuiTableFlags_SizingStretchSame | ImGuiTableFlags_Resizable | ImGuiTableFlags_BordersOuter | ImGuiTableFlags_ContextMenuInBody;
}

void InspectorWindow::Shutdown()
{
}

void InspectorWindow::Render()
{
	if (ImGui::Begin("Inspector", &show))
	{
		bool backButtonDisabled = !NavigationCanGoBack();
		ImGui::BeginDisabled(backButtonDisabled);
		if (ImGui::Button("<"))
		{
			NavigationGoBack();
		}
		ImGui::EndDisabled();

		bool forwardButtonDisabled = !NavigationCanGoForward();
		ImGui::BeginDisabled(forwardButtonDisabled);
		ImGui::SameLine();
		if (ImGui::Button(">"))
		{
			NavigationGoForward();
		}
		ImGui::EndDisabled();

		BaseObject* target = NavigationGetCurrent();

		ImGui::SameLine();
		ImGui::Text("%s", target ? target->GetType()->TypeName().c_str() : "null");

		RenderReflectiveObjectMembers("target", target);
	}
	ImGui::End();
}

void InspectorWindow::SetTarget(BaseObject* newTarget)
{
	if (newTarget != NavigationGetCurrent())
	{
		NavigationClear();
		NavigationPush(newTarget);
	}
}

void InspectorWindow::NavigationClear()
{
	navigationStack.clear();
}

void InspectorWindow::NavigationPush(BaseObject* obj)
{
	if (obj)
	{
		for (size_t i = navigationCurrentIndex + 1; i < navigationStack.size(); i++)
			NavigationRemove(navigationStack[i]);

		navigationStack.push_back(obj);
		navigationCurrentIndex = navigationStack.size() - 1;

		obj->destructionEvent.AddListener(this, [=](BaseObject*)
			{
				NavigationRemove(obj);
			});
	}
}

BaseObject* InspectorWindow::NavigationGetCurrent()
{
	if (navigationCurrentIndex >= 0 && navigationCurrentIndex < navigationStack.size())
		return navigationStack[navigationCurrentIndex];
	else
		return nullptr;
}

bool InspectorWindow::NavigationCanGoBack()
{
	return navigationCurrentIndex - 1 >= 0 && navigationCurrentIndex - 1 < navigationStack.size();
}

void InspectorWindow::NavigationGoBack()
{
	if (NavigationCanGoBack())
		navigationCurrentIndex--;
}

bool InspectorWindow::NavigationCanGoForward()
{
	return navigationCurrentIndex + 1 >= 0 && navigationCurrentIndex + 1 < navigationStack.size();
}

void InspectorWindow::NavigationGoForward()
{
	if (NavigationCanGoForward())
		navigationCurrentIndex++;
}

void InspectorWindow::NavigationRemove(BaseObject* obj)
{
	if (obj)
	{
		auto it = std::find(navigationStack.begin(), navigationStack.end(), obj);
		if (it != navigationStack.end())
			navigationStack.erase(it);

		obj->destructionEvent.RemoveListener(this);
		obj = nullptr;
	}
}

void InspectorWindow::RenderReflectiveObjectMembers(const std::string& name, ReflectiveObject* reflectiveObject, const BaseObjectProperty* prop, const BaseProperty* parentProp, void* parentObj)
{
	if (reflectiveObject)
	{
		Type* type = reflectiveObject->GetType();

		if (type->IsSameOrChildOf(GetStaticType<Material>()))
		{
			RenderMaterialInspector(name, reinterpret_cast<Material*>(reflectiveObject));
		}
		else if (type->IsSameOrChildOf(GetStaticType<SceneObject>()))
		{
			RenderSceneObjectInspector(name, reinterpret_cast<SceneObject*>(reflectiveObject));
		}
		else
		{
			if (ImGui::BeginTable(name.c_str(), 2, tableFlags))
			{
				ImGui::TableSetupColumn("Name", ImGuiTableColumnFlags_WidthFixed);
				ImGui::TableSetupColumn("Value", ImGuiTableColumnFlags_WidthStretch);

				Reflector reflector;
				reflectiveObject->Reflect(reflector);

				for (size_t i = 0; i < reflector.members.size(); i++)
				{
					ObjectMember& member = reflector.members[i];

					if (member.GetAttribute<HideInInspectorAttribute>() != nullptr)
						continue;

					ImGui::TableNextRow();

					ImGui::TableSetColumnIndex(0);
					ImGui::Text("%s", member.memberName.c_str());

					ImGui::TableSetColumnIndex(1);
					RenderBaseProperty(member.memberName, member.GetAddr(reflectiveObject), member.GetProperty(), nullptr, nullptr, reflectiveObject, &member);
				}

				for (size_t i = 0; i < reflector.functions.size(); i++)
				{
					ObjectFunction& function = reflector.functions[i];

					if (function.parameters.size() == 0)
					{
						ImGui::TableNextRow();

						ImGui::TableSetColumnIndex(0);
						ImGui::Text("%s", function.name.c_str());

						ImGui::TableSetColumnIndex(1);
						if (ImGui::Button(function.name.c_str()))
						{
							FunctionCall functionCall(function);
							functionCall.targetObject = reflectiveObject;
							functionCall.Invoke();
						}
					}
				}

				ImGui::EndTable();
			}
		}
	}
	else
	{
		if (prop)
		{
			ImGui::Text("null (%s)", prop->valueType->TypeName().c_str());
		}
		else
		{
			ImGui::Text("null (unknown-type)");
		}
	}
}

template<typename T>
T* GetPtr(T* ptr)
{
	return ptr;
}

template<typename T>
T* GetPtr(const std::shared_ptr<T>& ptr)
{
	return ptr.get();
}

template<typename T>
std::shared_ptr<T> GetSharedPtr(T* ptr)
{
	return std::shared_ptr<T>();
}

template<typename T>
std::shared_ptr<T> GetSharedPtr(const std::shared_ptr<T>& ptr)
{
	return ptr;
}

template<typename T1, typename T2>
void SetPtr(T1*& ptr, T2* value)
{
	ptr = value;
}
template<typename T1, typename T2>
void SetPtr(T1*& ptr, const std::shared_ptr<T2>& value)
{
	ptr = value.get();
}
template<typename T1>
void SetPtr(T1*& ptr, nullptr_t value)
{
	ptr = value;
}

template<typename T1, typename T2>
void SetPtr(std::shared_ptr<T1>& ptr, T2* value)
{
	ptr.reset(value);
}
template<typename T1, typename T2>
void SetPtr(std::shared_ptr<T1>& ptr, const std::shared_ptr<T2>& value)
{
	ptr = value;
}
template<typename T1>
void SetPtr(std::shared_ptr<T1>& ptr, nullptr_t value)
{
	ptr.reset();
}

template<typename T>
struct PtrUnderlyingType  //others
{
};

template<typename T>
struct PtrUnderlyingType<T*>  //pointer type
{
	typedef T value;
};

template<typename T>
struct PtrUnderlyingType<std::shared_ptr<T>>  //shared-pointer type
{
	typedef T value;
};

template<typename PointerType>
PointerType InspectorWindow::RenderBaseObjectPicker(PointerType value, Type* valueType)
{
	typedef typename PtrUnderlyingType<PointerType>::value ObjectType;

	PointerType obj = value;

	std::string text;
	if (value)
	{
		Type* dynamicType = GetDynamicType(*value);

		if (value->IsAsset())
			text = value->GetAssetPath().Filename();
		else
			text = formatString("%s(%p)", dynamicType->TypeName().c_str(), GetPtr<BaseObject>(value));
	}
	else
	{
		text = "null";
	}

	std::string menuName = formatString("inspector_menu_%p", GetPtr(value));

	if (ImGui::BeginPopupContextItem(menuName.c_str()))
	{
		if (ImGui::Selectable("Inspect"))
		{
			NavigationPush(GetPtr<BaseObject>(value));
		}

		if (valueType && valueType->IsSameOrChildOf(GetStaticType<Texture>()))
		{
			if (ImGui::Selectable("Preview Texture"))
			{
				std::shared_ptr<ReflectiveObject> sharedObj = GetSharedPtr<ReflectiveObject>(value);
				TexturePtr textureObj = std::dynamic_pointer_cast<Texture>(sharedObj);
				if (textureObj)
				{
					InGameEditor::GetInstance().texturePreviewWindow.SetTexture(textureObj);
				}
				else
				{
					Log::Error("InspectorWindow::RenderBaseObjectPicker Type error");
				}
			}
		}

		if (ImGui::Selectable("Set from asset window"))
		{
			AssetPath assetPath = InGameEditor::GetInstance().assetsWindow.GetSelection();
			BaseObjectPtr loadedAssetPtr = g_lawnEngine.getAssetManager().Load<BaseObject>(assetPath);
			if (loadedAssetPtr && loadedAssetPtr->GetType()->IsSameOrChildOf(valueType))
			{
				std::shared_ptr<ObjectType> castedPtr = std::static_pointer_cast<ObjectType>(loadedAssetPtr);
				SetPtr(obj, castedPtr);
			}
		}

		if (ImGui::Selectable("Set to null"))
		{
			SetPtr(obj, nullptr);
		}

		if (ImGui::Selectable("Select in asset window"))
		{
			if (value && value->IsAsset())
			{
				AssetPath assetPath = value->GetAssetPath();
				InGameEditor::GetInstance().assetsWindow.SetSelection(assetPath);
			}
		}

		ImGui::EndPopup();
	}

	if (ImGui::Button(text.c_str()))
	{
		ImGui::OpenPopup(menuName.c_str());
	}

	return obj;
}

void InspectorWindow::RenderBaseProperty(const std::string& name, void* obj, const BaseProperty* prop, const BaseProperty* parentProp, void* parentObj, ReflectiveObject* ownerObject, ObjectMember* member)
{
	if (prop->type == PropertyType::Primitive)
	{
		const BasePrimitiveProperty* primProp = reinterpret_cast<const BasePrimitiveProperty*>(prop);
		RenderBasePrimitiveProperty(name, obj, primProp, parentProp, parentObj, ownerObject, member);
	}
	else if (prop->type == PropertyType::Object)
	{
		const BaseObjectProperty* objProp = reinterpret_cast<const BaseObjectProperty*>(prop);
		RenderBaseObjectProperty(name, obj, objProp, parentProp, parentObj, ownerObject, member);
	}
	else if (prop->type == PropertyType::Array)
	{
		const BaseArrayProperty* arrayProp = reinterpret_cast<const BaseArrayProperty*>(prop);
		RenderBaseArrayProperty(name, obj, arrayProp, parentProp, parentObj, ownerObject, member);
	}
	else if (prop->type == PropertyType::Map)
	{
		const BaseMapProperty* mapProp = reinterpret_cast<const BaseMapProperty*>(prop);
		RenderBaseMapProperty(name, obj, mapProp, parentProp, parentObj, ownerObject, member);
	}
	else if (prop->type == PropertyType::RawPointer)
	{
		const BaseRawPointerProperty* ptrProp = reinterpret_cast<const BaseRawPointerProperty*>(prop);
		RenderBaseRawPointerProperty(name, obj, ptrProp, parentProp, parentObj, ownerObject, member);
	}
	else if (prop->type == PropertyType::SharedPointer)
	{
		const BaseSharedPointerProperty* ptrProp = reinterpret_cast<const BaseSharedPointerProperty*>(prop);
		RenderBaseSharedPointerProperty(name, obj, ptrProp, parentProp, parentObj, ownerObject, member);
	}
	else if (prop->type == PropertyType::Object)
	{
		const BaseObjectProperty* objProp = reinterpret_cast<const BaseObjectProperty*>(prop);
		RenderBaseObjectProperty(name, obj, objProp, parentProp, parentObj, ownerObject, member);
	}
	else
	{
		CHECK(false);
	}
}

void InspectorWindow::RenderBasePrimitiveProperty(const std::string& name, void* obj, const BasePrimitiveProperty* prop, const BaseProperty* parentProp, void* parentObj, ReflectiveObject* ownerObject, ObjectMember* member)
{
	bool valueChanged = false;

	ImGui::SetHideLabel(true);

	Type* type = prop->valueType;
	if (type->Category() == TypeCategory::Enum)
	{
		BaseEnumObjectType* enumType = static_cast<BaseEnumObjectType*>(type);
		const std::vector<FixedString>& enumNames = enumType->GetNames();

		int currentValue = enumType->GetIntValue(obj);
		FixedString currentName = enumType->GetNameFromIntValue(currentValue);

		if (ImGui::BeginCombo(name.c_str(), currentName.c_str(), ImGuiComboFlags_None))
		{
			FixedString selectedName;

			for (int n = 0; n < enumNames.size(); n++)
			{
				const bool is_selected = (currentName == enumNames[n]);
				if (ImGui::Selectable(enumNames[n].c_str(), is_selected))
				{
					selectedName = enumNames[n];
				}

				// Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
				if (is_selected)
					ImGui::SetItemDefaultFocus();
			}
			ImGui::EndCombo();

			if (selectedName != FixedString())
			{
				valueChanged = true;

				int selectedValue;
				enumType->GetIntValueFromName(selectedName, selectedValue);
				enumType->SetIntValue(obj, selectedValue);
			}
		}
	}
	else if (type == GetStaticType<bool>())
	{
		bool& value = prop->GetValue<bool>(obj);
		ImGui::Checkbox(name.c_str(), &value);
	}
	else if (type == GetStaticType<int32>())
	{
		int32& value = prop->GetValue<int32>(obj);
		int32 minValue = std::numeric_limits<int32>::min();
		int32 maxValue = std::numeric_limits<int32>::max();
		ImGui::DragScalar(name.c_str(), ImGuiDataType_S32, &value, 1.0f, &minValue, &maxValue, "%d");
	}
	else if (type == GetStaticType<uint32>())
	{
		uint32& value = prop->GetValue<uint32>(obj);
		uint32 minValue = std::numeric_limits<uint32>::min();
		uint32 maxValue = std::numeric_limits<uint32>::max();
		ImGui::DragScalar(name.c_str(), ImGuiDataType_U32, &value, 1.0f, &minValue, &maxValue, "%u");
	}
	else if (type == GetStaticType<int64>())
	{
		int64& value = prop->GetValue<int64>(obj);
		int64 minValue = std::numeric_limits<int64>::min();
		int64 maxValue = std::numeric_limits<int64>::max();
		ImGui::DragScalar(name.c_str(), ImGuiDataType_S64, &value, 1.0f, &minValue, &maxValue, "%lld");
	}
	else if (type == GetStaticType<uint64>())
	{
		uint64& value = prop->GetValue<uint64>(obj);
		uint64 minValue = std::numeric_limits<uint64>::min();
		uint64 maxValue = std::numeric_limits<uint64>::max();
		ImGui::DragScalar(name.c_str(), ImGuiDataType_U64, &value, 1.0f, &minValue, &maxValue, "%llu");
	}
	else if (type == GetStaticType<float>())
	{
		if (member && member->HasAttribute<FloatRangeAttribute>())
		{
			const FloatRangeAttribute* floatRangeAttr = member->GetAttribute<FloatRangeAttribute>();

			float& value = prop->GetValue<float>(obj);
			ImGui::SliderFloat(name.c_str(), &value, floatRangeAttr->minValue, floatRangeAttr->maxValue, "%f");
		}
		else
		{
			float& value = prop->GetValue<float>(obj);
			float minValue = -FLT_MAX;
			float maxValue = FLT_MAX;
			ImGui::DragScalar(name.c_str(), ImGuiDataType_Float, &value, 0.1f, &minValue, &maxValue, "%f");
		}
	}
	else if (type == GetStaticType<double>())
	{
		double& value = prop->GetValue<double>(obj);
		double minValue = -DBL_MAX;
		double maxValue = DBL_MAX;
		ImGui::DragScalar(name.c_str(), ImGuiDataType_Double, &value, 0.1f, &minValue, &maxValue, "%lf");
	}
	else if (type == GetStaticType<float2>())
	{
		float2& value = prop->GetValue<float2>(obj);
		ImGui::DragFloat2(name.c_str(), (float*)value, 0.1f);
	}
	else if (type == GetStaticType<float3>())
	{
		float3& value = prop->GetValue<float3>(obj);
		ImGui::DragFloat3(name.c_str(), (float*)value, 0.1f);
	}
	else if (type == GetStaticType<float4>())
	{
		float4& value = prop->GetValue<float4>(obj);
		ImGui::DragFloat4(name.c_str(), (float*)value, 0.1f);
	}
	else if (type == GetStaticType<quaternionf>())
	{
	}
	else
	{
		ImGui::Text("%s", prop->ToString(obj).c_str());
	}

	ImGui::SetHideLabel(false);

	if (ImGui::IsItemEdited() || valueChanged)
	{
		if (ownerObject)
		{
			ownerObject->OnPropertyChangedByReflection(*member);
		}
	}
}

void InspectorWindow::RenderBaseObjectProperty(const std::string& name, void* obj, const BaseObjectProperty* prop, const BaseProperty* parentProp, void* parentObj, ReflectiveObject* ownerObject, ObjectMember* member)
{
	ReflectiveObject* reflectiveObject = prop->GetPointer(obj);

	Type* type = reflectiveObject ? reflectiveObject->GetType() : prop->valueType;

	bool canPickAsset =
		member &&
		member->HasAttribute<AssetSelectorAttribute>() &&
		type->IsSameOrChildOf(GetStaticType<BaseObject>()) &&
		parentProp &&
		(parentProp->type == PropertyType::RawPointer || parentProp->type == PropertyType::SharedPointer);

	if (canPickAsset)
	{
		if (parentProp->type == PropertyType::RawPointer)
		{
			const BaseRawPointerProperty* pickableValueProp = reinterpret_cast<const BaseRawPointerProperty*>(parentProp);

			BaseObject* curValue = static_cast<BaseObject*>(pickableValueProp->GetPtr(parentObj));
			BaseObject* pickedValue = RenderBaseObjectPicker<BaseObject*>(curValue, prop->valueType);

			if (pickedValue != curValue)
			{
				pickableValueProp->SetPtr(parentObj, pickedValue);

				if (ownerObject)
				{
					ownerObject->OnPropertyChangedByReflection(*member);
				}
			}
		}
		else if (parentProp->type == PropertyType::SharedPointer)
		{
			const BaseSharedPointerProperty* pickableValueProp = reinterpret_cast<const BaseSharedPointerProperty*>(parentProp);
			
			std::shared_ptr<BaseObject> curValue = std::static_pointer_cast<BaseObject>(pickableValueProp->GetSharedPtr(parentObj));
			std::shared_ptr<BaseObject> pickedValue = RenderBaseObjectPicker<std::shared_ptr<BaseObject>>(curValue, prop->valueType);

			if (pickedValue != curValue)
			{
				pickableValueProp->SetSharedPtr(parentObj, pickedValue);

				if (ownerObject)
				{
					ownerObject->OnPropertyChangedByReflection(*member);
				}
			}
		}
		else
			CHECK(false);
	}
	else
	{
		std::string headerName = formatString("%s", type->TypeName().c_str());

		bool expanded = ImGui::CollapsingHeader(headerName.c_str(), ImGuiTreeNodeFlags_None);
		if (expanded)
		{
			RenderReflectiveObjectMembers(name, reflectiveObject, prop);
		}
	}
}

void InspectorWindow::RenderBaseArrayProperty(const std::string& name, void* obj, const BaseArrayProperty* arrayProp, const BaseProperty* parentProp, void* parentObj, ReflectiveObject* ownerObject, ObjectMember* member)
{
	BaseProperty* elementProperty = arrayProp->GetElementProperty();

	if (ImGui::BeginTable(name.c_str(), 2, tableFlags))
	{
		ImGui::TableSetupColumn("Name", ImGuiTableColumnFlags_WidthFixed);
		ImGui::TableSetupColumn("Value", ImGuiTableColumnFlags_WidthStretch);

		size_t size = arrayProp->GetSize(obj);
		for (size_t i = 0; i < size; i++)
		{
			void* element = arrayProp->GetElementAddress(obj, i);

			std::string name = formatString("[%d]", i);

			ImGui::TableNextRow();

			ImGui::TableSetColumnIndex(0);
			ImGui::Text("%s", name.c_str());

			ImGui::TableSetColumnIndex(1);
			RenderBaseProperty(name, element, elementProperty, arrayProp, obj, ownerObject, member);
		}

		ImGui::EndTable();
	}
}

void InspectorWindow::RenderBaseMapProperty(const std::string& name, void* obj, const BaseMapProperty* mapProp, const BaseProperty* parentProp, void* parentObj, ReflectiveObject* ownerObject, ObjectMember* member)
{
	BaseProperty* keyProperty = mapProp->GetKeyProperty();
	BaseProperty* valueProperty = mapProp->GetValueProperty();

	if (ImGui::BeginTable(name.c_str(), 2, tableFlags))
	{
		ImGui::TableSetupColumn("Name", ImGuiTableColumnFlags_WidthFixed);
		ImGui::TableSetupColumn("Value", ImGuiTableColumnFlags_WidthStretch);

		BaseMapProperty::Iterator* it = mapProp->GetIterator(obj);

		while (it->IsValid())
		{
			ImGui::TableNextRow();

			void* keyAddr = it->GetKeyAddress();
			void* valueAddr = it->GetValueAddress();

			ImGui::TableSetColumnIndex(0);
			RenderBaseProperty(formatString("%p", keyAddr), keyAddr, keyProperty, mapProp, obj, ownerObject, member);

			ImGui::TableSetColumnIndex(1);
			RenderBaseProperty(formatString("%p", valueAddr), valueAddr, valueProperty, mapProp, obj, ownerObject, member);

			it->Next();
		}

		delete it;

		ImGui::EndTable();
	}
}

void InspectorWindow::RenderBaseRawPointerProperty(const std::string& name, void* obj, const BaseRawPointerProperty* ptrProp, const BaseProperty* parentProp, void* parentObj, ReflectiveObject* ownerObject, ObjectMember* member)
{
	void* element = ptrProp->GetPtr(obj);
	RenderBaseProperty(name, element, ptrProp->GetDereferencedProperty(), ptrProp, obj, ownerObject, member);
}

void InspectorWindow::RenderBaseSharedPointerProperty(const std::string& name, void* obj, const BaseSharedPointerProperty* ptrProp, const BaseProperty* parentProp, void* parentObj, ReflectiveObject* ownerObject, ObjectMember* member)
{
	std::shared_ptr<void> element = ptrProp->GetSharedPtr(obj);
	RenderBaseProperty(name, element.get(), ptrProp->GetDereferencedProperty(), ptrProp, obj, ownerObject, member);
}

void InspectorWindow::RenderSceneObjectInspector(const std::string& name, SceneObject* so)
{
	std::vector<Component*>& components = so->GetComponents();

	for (size_t i = 0; i < components.size(); i++)
	{
		Component* comp = components[i];

		std::string typeName = comp->GetType()->TypeName();

		if (ImGui::CollapsingHeader(typeName.c_str(), ImGuiTreeNodeFlags_DefaultOpen))
		{
			RenderReflectiveObjectMembers(formatString("%p", comp), comp);
		}
	}
}

void InspectorWindow::RenderMaterialInspector(const std::string& name, Material* mat)
{
	ShaderPtr shader = mat->GetShader();

	if (ImGui::BeginTable(name.c_str(), 2, tableFlags))
	{
		ImGui::TableSetupColumn("Name", ImGuiTableColumnFlags_WidthFixed);
		ImGui::TableSetupColumn("Value", ImGuiTableColumnFlags_WidthStretch);

		std::map<FixedString, std::unique_ptr<ShaderProperty>>& properties = shader->GetProperties();

		for (auto it = properties.begin(); it != properties.end(); it++)
		{
			FixedString name = it->first;
			ShaderProperty* prop = it->second.get();

			ImGui::TableNextRow();

			ImGui::TableSetColumnIndex(0);
			ImGui::Text("%s", name.c_str());

			ImGui::TableSetColumnIndex(1);
			switch (prop->type)
			{
			case SPT_VariantKey:
				break;
			case SPT_Color:
			{
				float4 value = mat->HasSetValue(name) ? mat->GetFloat4(name) : prop->defaultValueFloat4;
				if (ImGui::ColorEdit4(name.c_str(), (float*)value))
					mat->SetFloat4(name, value);
				break;
			}
			case SPT_Float:
			{
				float value = mat->HasSetValue(name) ? mat->GetFloat(name) : prop->defaultValueFloat4.x;
				if (prop->ranged)
				{
					if (ImGui::SliderFloat(name.c_str(), &value, prop->floatRange[0], prop->floatRange[1], "%.3f", 1.0f))
						mat->SetFloat(name, value);
				}
				else
				{
					if (ImGui::InputFloat(name.c_str(), &value))
						mat->SetFloat(name, value);
				}
				break;
			}
			case SPT_Float2:
			{
				float2 value = mat->HasSetValue(name) ? mat->GetFloat2(name) : prop->defaultValueFloat4.xy();
				if (prop->ranged)
				{
					if (ImGui::SliderFloat2(name.c_str(), (float*)value, prop->floatRange[0], prop->floatRange[1], "%.3f", 1.0f))
						mat->SetFloat2(name, value);
				}
				else
				{
					if (ImGui::InputFloat2(name.c_str(), (float*)value))
						mat->SetFloat2(name, value);
				}
				break;
			}
			case SPT_Float3:
			{
				float3 value = mat->HasSetValue(name) ? mat->GetFloat3(name) : prop->defaultValueFloat4.xyz();
				if (prop->ranged)
				{
					if (ImGui::SliderFloat3(name.c_str(), (float*)value, prop->floatRange[0], prop->floatRange[1], "%.3f", 1.0f))
						mat->SetFloat3(name, value);
				}
				else
				{
					if (ImGui::InputFloat3(name.c_str(), (float*)value))
						mat->SetFloat3(name, value);
				}
				break;
			}
			case SPT_Float4:
			{
				float4 value = mat->HasSetValue(name) ? mat->GetFloat4(name) : prop->defaultValueFloat4;
				if (prop->ranged)
				{
					if (ImGui::SliderFloat4(name.c_str(), (float*)value, prop->floatRange[0], prop->floatRange[1], "%.3f", 1.0f))
						mat->SetFloat4(name, value);
				}
				else
				{
					if (ImGui::InputFloat4(name.c_str(), (float*)value))
						mat->SetFloat4(name, value);
				}
				break;
			}
			case SPT_Texture1D:
				break;
			case SPT_Texture2D:
			{
				TexturePtr value = mat->HasSetResource(name) ? mat->GetResource(name) : prop->defaultValueTexture;
				TexturePtr newValue = RenderBaseObjectPicker<TexturePtr>(value, GetStaticType<Texture2D>());
				if (newValue != value)
					mat->SetResource(name, newValue);
				break;
			}
			case SPT_Texture3D:
				break;
			case SPT_TextureCube:
				break;
			default:
				break;
			}
		}

		ImGui::EndTable();
	}
}

//////////////////////////////////////////////////////////////////////////

AssetsWindow::AssetsWindow()
	: show(true)
{
}

AssetsWindow::~AssetsWindow()
{
}

void AssetsWindow::Startup()
{
	rootPath = g_lawnEngine.getAssetManager().ProjectDir();
}

void AssetsWindow::Shutdown()
{

}

enum MyItemColumnID
{
	MyItemColumnID_Name,
	MyItemColumnID_Type,
};

void AssetsWindow::Render()
{
	FileSystem& fileSystem = currentPath.GetFileSystem();

	if (!currentPath.IsInsideOfDirectory(rootPath))
		currentPath = rootPath;

	while (currentPath != rootPath && !currentPath.Exists())
		currentPath = currentPath.Parent();
	if (!currentPath.Exists())
		return;

	clickingPath = AssetPath();

	if (ImGui::Begin("Assets", &show))
	{
		AssetPath relativePath = currentPath.ToProjectPath();
		std::string editPath = relativePath.path;
		ImGui::AlignTextToFramePadding();
		ImGui::Text("Path");
		ImGui::SameLine();
		if (ImGui::InputText("##Path", &editPath, ImGuiInputTextFlags_EnterReturnsTrue))
		{
			if (editPath != relativePath.path)
			{
				relativePath.path = editPath;
				if (relativePath.Exists())
				{
					currentPath = relativePath.ToAbsolutePath();
					SetSelection(currentPath);
				}
			}
		}

		ImGui::SameLine();
		if (ImGui::Button("Refresh"))
		{
			g_lawnEngine.getAssetManager().Refresh();
		}

		{
			ImGuiWindowFlags window_flags = ImGuiWindowFlags_HorizontalScrollbar | ImGuiWindowFlags_AlwaysAutoResize;
			ImGui::BeginChild("ChildL", ImVec2(300, 0), true, window_flags);
			if (rootPath.IsDirectory())
			{
				RenderTreeNode(rootPath);
			}
			ImGui::EndChild();
		}

		ImGui::SameLine();

		{
			ImGuiWindowFlags window_flags = ImGuiWindowFlags_HorizontalScrollbar;
			ImGui::BeginChild("ChildR", ImVec2(0, 0), true, window_flags);
			RenderFilesView();
			ImGui::EndChild();
		}

		if (clickingPath.IsValid())
		{
			SetSelection(clickingPath);
		}
	}
	ImGui::End();
}

void AssetsWindow::RenderTreeNode(const AssetPath& path)
{
	CHECK(path.IsDirectory());

	ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_SpanFullWidth;

	if (selectedPaths.find(path) != selectedPaths.end())
		node_flags |= ImGuiTreeNodeFlags_Selected;

	std::vector<AssetPath> childDirs;
	{
		std::vector<AssetPath> childPaths;
		path.List(childPaths, false);

		for (size_t i = 0; i < childPaths.size(); i++)
		{
			AssetPath childPath = childPaths[i];
			if (childPath.IsDirectory())
			{
				childDirs.push_back(childPath);
			}
		}
	}

	if (childDirs.size() == 0)
		node_flags |= ImGuiTreeNodeFlags_Leaf;

	bool node_open = ImGui::TreeNodeEx(path.path.c_str(), node_flags, "%s", path.FilenameWithExtension().c_str());

	if (ImGui::IsItemClicked() && !ImGui::IsItemToggledOpen())
	{
		clickingPath = path;
	}

	if (node_open)
	{
		for (size_t i = 0; i < childDirs.size(); i++)
		{
			AssetPath childDir = childDirs[i];
			RenderTreeNode(childDir);
		}

		ImGui::TreePop();
	}
}

void AssetsWindow::RenderFilesView()
{
	ImGuiTableFlags tableFlags = ImGuiTableFlags_Resizable | ImGuiTableFlags_Sortable | ImGuiTableFlags_ContextMenuInBody;
	if (ImGui::BeginTable("FilesView", 2, tableFlags))
	{
		// Declare columns
		// We use the "user_id" parameter of TableSetupColumn() to specify a user id that will be stored in the sort specifications.
		// This is so our sort function can identify a column given our own identifier. We could also identify them based on their index!
		ImGui::TableSetupColumn("Name", ImGuiTableColumnFlags_DefaultSort | ImGuiTableColumnFlags_NoHide, 0.0f, MyItemColumnID::MyItemColumnID_Name);
		ImGui::TableSetupColumn("type", ImGuiTableColumnFlags_DefaultSort | ImGuiTableColumnFlags_WidthFixed, 0.0f, MyItemColumnID::MyItemColumnID_Type);
		ImGui::TableSetupScrollFreeze(0, 1);

		bool items_need_sort = false;

		// Sort our data if sort specs have been changed!
		ImGuiTableSortSpecs* sorts_specs = ImGui::TableGetSortSpecs();
		if (sorts_specs && sorts_specs->SpecsDirty)
			items_need_sort = true;
		if (sorts_specs && items_need_sort && files.size() > 1)
		{
			for (int iSort = 0; iSort < sorts_specs->SpecsCount; iSort++)
			{
				const ImGuiTableColumnSortSpecs& sortInfo = sorts_specs->Specs[iSort];

				if (sortInfo.ColumnIndex == 0)
				{
					std::sort(files.begin(), files.end(), [&](const AssetPath& a, const AssetPath& b)->bool
						{
							if (sortInfo.SortDirection == ImGuiSortDirection_Ascending)
								return a.FilenameWithExtension() < b.FilenameWithExtension();
							else
								return a.FilenameWithExtension() > b.FilenameWithExtension();
						});
				}
				else if (sortInfo.ColumnIndex == 1)
				{
					std::sort(files.begin(), files.end(), [=](const AssetPath& a, const AssetPath& b)->bool
						{
							if (sortInfo.SortDirection == ImGuiSortDirection_Ascending)
								return a.Extensions() < b.Extensions();
							else
								return a.Extensions() > b.Extensions();
						});
				}
			}

			sorts_specs->SpecsDirty = false;
		}
		items_need_sort = false;

		// Show headers
		ImGui::TableHeadersRow();

		// Demonstrate using clipper for large vertical lists
		ImGuiListClipper clipper;
		clipper.Begin(files.size());
		while (clipper.Step())
		{
			for (int row_n = clipper.DisplayStart; row_n < clipper.DisplayEnd; row_n++)
			{
				AssetPath& file = files[row_n];

				ImGui::TableNextRow(ImGuiTableRowFlags_None);

				if (ImGui::TableSetColumnIndex(0))
				{
					bool selected = currentPath == file;
					if (ImGui::Selectable(file.FilenameWithExtension().c_str(), &selected, ImGuiSelectableFlags_SpanAllColumns))
					{
						clickingPath = file;
					}

					if (ImGui::BeginPopupContextItem())
					{
						clickingPath = file;
						RenderFileContextMenu(file);
						ImGui::EndPopup();
					}
				}

				if (ImGui::TableSetColumnIndex(1))
				{
					ImGui::Text("%s", file.Extensions().c_str());
				}
			}
		}

		ImGui::EndTable();
	}
}

void AssetsWindow::RenderFileContextMenu(const AssetPath& path)
{	
	AssetPath assetProjPath = path.ToProjectPath();
	if (!assetProjPath.IsValid())
	{
		ImGui::Text("Not in project dir");
		return;
	}

	Type* assetType = g_lawnEngine.getAssetManager().GetAssetType(assetProjPath);
	if (assetType && assetType->IsSameOrChildOf(GetStaticType<Texture>()))
	{
		if (ImGui::Selectable("Preview Texture"))
		{
			TexturePtr tex = g_lawnEngine.getAssetManager().Load<Texture>(assetProjPath);
			InGameEditor::GetInstance().texturePreviewWindow.SetTexture(tex);
		}
	}

	Type* importerType;
	AssetImportRecordPtr importRecord;
	if (g_lawnEngine.getAssetManager().IsSrcAssetImported(assetProjPath, &importRecord))
	{
		if (ImGui::Selectable("Re-Import"))
		{
			g_lawnEngine.getAssetManager().ReimportAsset(importRecord);
		}
	}
	else if ((importerType = g_lawnEngine.getAssetManager().FindProcessorForImporting(assetProjPath)) != nullptr)
	{
		if (ImGui::Selectable("Import"))
		{
			g_lawnEngine.getAssetManager().ImportNewAsset(assetProjPath);
		}
	}
	else
	{
		ImGui::Text("Can not import");
	}
}

void AssetsWindow::SetSelection(const AssetPath& path)
{
	AssetPath absPath = path.ToAbsolutePath();

	std::string relativePath;
	if (vcpp::filesystem::get_relative_path(absPath.path, rootPath.path, relativePath))
	{
		currentPath = path;

		std::vector<AssetPath> childEntries;
		if (absPath.IsDirectory())
			absPath.List(childEntries, false);
		else if (absPath.Parent().Exists())
			absPath.Parent().List(childEntries, false);

		files.clear();
		for (auto& path : childEntries)
		{
			if (path.IsFile())
				files.push_back(path);
		}

		selectedPaths.clear();
		if (absPath.IsDirectory())
			selectedPaths.insert(absPath);
		else if (absPath.Parent().Exists())
			selectedPaths.insert(absPath.Parent());
	}
}

void AssetsWindow::SetSelection(const std::vector<std::string>& pathParts)
{
	std::string relativePath = stringJoin(pathParts.begin(), pathParts.end(), "/");
	SetSelection(rootPath / relativePath);
}

AssetPath AssetsWindow::GetSelection()
{
	return currentPath;
}

//////////////////////////////////////////////////////////////////////////

TexturePreviewWindow::TexturePreviewWindow()
	: uiCanvas(), uiRawImage()
{
	channelEnabled[0] = channelEnabled[1] = channelEnabled[2] = channelEnabled[3] = true;
}

TexturePreviewWindow::~TexturePreviewWindow()
{
}

void TexturePreviewWindow::SetEnabled(bool b)
{
	RenderViewPtr mainRenderView = g_lawnEngine.getRenderViews().front();
	
	if (b)
	{
		if (!mainRenderView->IsUICanvasRegistered(uiCanvas))
			mainRenderView->AddUICanvas(uiCanvas);
	}
	else
	{
		if (mainRenderView->IsUICanvasRegistered(uiCanvas))
			mainRenderView->RemoveUICanvas(uiCanvas);
	}
}

void TexturePreviewWindow::Startup()
{
	AssetManager& assetManager = g_lawnEngine.getAssetManager();
	SpriteAtlasPtr atlas = assetManager.Load<SpriteAtlas>(assetManager.ProjectDir() / "DefaultUIAtlas.SpriteAtlas");

	SceneObject* canvasObj = g_lawnEngine.getWorld().CreateSceneObject();
	canvasObj->name = "TexturePreviewWindow";

	uiCanvas = canvasObj->AddComponent<UICanvas>();

	ShaderPtr uiRawImageInspectorShader = assetManager.Load<Shader>(assetManager.ProjectDir() / "Shaders/UIRawImageInspector.shader");
	if (uiRawImageInspectorShader)
	{
		uiRawImageInspectorMaterial.reset(new Material());
		uiRawImageInspectorMaterial->BindShader(uiRawImageInspectorShader);
	}

	UIImage* frame = uiCanvas->AddUIChild<UIImage>("frame");
	{
		frame->SetXAsFilledWithPadding(64, 64);
		frame->SetYAsFilledWithPadding(64, 64);
		
		UILinearLayout* toolbar = frame->AddUIChild<UILinearLayout>("toolbar");
		{
			toolbar->SetXAsFilledWithPadding(0, 0);
			toolbar->SetYAsTopAlignedFixedSize(0, 32);
			toolbar->SetDirection(LinearLayoutDirection::Horizontal);
			toolbar->SetFillHorizontal(false);
			toolbar->SetFillVertical(true);
			toolbar->SetItemSpacing(2);
			toolbar->SetItemMinSize(32);
			
			auto* rBtn = toolbar->AddUIChild<UIToggleButton>("rBtn");
			{
				rBtn->SetLayoutMinSize(float2(64, 32));
				rBtn->targetText->SetText("R");
				rBtn->SetValue(channelEnabled[0]);
				rBtn->OnValueChanged = [=]()
				{
					channelEnabled[0] = rBtn->GetValue() ? 1.0f : 0.0f;
					OnChannelEnabledChanged();
				};
			}
			auto* gBtn = toolbar->AddUIChild<UIToggleButton>("gBtn");
			{
				gBtn->SetLayoutMinSize(float2(64, 32));
				gBtn->targetText->SetText("G");
				gBtn->SetValue(channelEnabled[1]);
				gBtn->OnValueChanged = [=]()
				{
					channelEnabled[1] = gBtn->GetValue() ? 1.0f : 0.0f;
					OnChannelEnabledChanged();
				};
			}
			auto* bBtn = toolbar->AddUIChild<UIToggleButton>("bBtn");
			{
				bBtn->SetLayoutMinSize(float2(64, 32));
				bBtn->targetText->SetText("B");
				bBtn->SetValue(channelEnabled[2]);
				bBtn->OnValueChanged = [=]()
				{
					channelEnabled[2] = bBtn->GetValue() ? 1.0f : 0.0f;
					OnChannelEnabledChanged();
				};
			}
			auto* aBtn = toolbar->AddUIChild<UIToggleButton>("aBtn");
			{
				aBtn->SetLayoutMinSize(float2(64, 32));
				aBtn->targetText->SetText("A");
				aBtn->SetValue(channelEnabled[3]);
				aBtn->OnValueChanged = [=]()
				{
					channelEnabled[3] = aBtn->GetValue() ? 1.0f : 0.0f;
					OnChannelEnabledChanged();
				};
			}
		}
		
		auto* closeBtn = frame->AddUIChild<UIButton>("closeBtn");
		{
			closeBtn->SetXAsRightAlignedFixedSize(2, 32);
			closeBtn->SetYAsTopAlignedFixedSize(2, 32);
			closeBtn->targetText->SetColor(float4(1, 0, 0, 0.5f));
			closeBtn->targetText->SetText("X");
			closeBtn->OnClick = [=]()
			{
				SetTexture(nullptr);
			};
		}

		uiRawImage = frame->AddUIChild<UIRawImage>("texture");
		{
			uiRawImage->SetXAsFilledWithPadding(2, 2);
			uiRawImage->SetYAsFilledWithPadding(34, 0);
			uiRawImage->SetMaterial(uiRawImageInspectorMaterial);
		}
	}
	
	OnChannelEnabledChanged();
}

void TexturePreviewWindow::Shutdown()
{
}

void TexturePreviewWindow::Render()
{
}

void TexturePreviewWindow::SetTexture(TexturePtr texture)
{
	if (texture)
	{
		if (uiRawImage)
			uiRawImage->SetTexture(Util::CastPtrWithCheck<Texture2D>(texture));
		SetEnabled(true);
	}
	else
	{
		if (uiRawImage)
			uiRawImage->SetTexture(nullptr);
		SetEnabled(false);
	}
}

void TexturePreviewWindow::OnChannelEnabledChanged()
{
	float4 channelOffsets = float4(0, 0, 0, 0);

	int enabledRGBChannelsCount = 0;
	for (int i = 0; i < 3; i++)
		enabledRGBChannelsCount += channelEnabled[i] ? 1 : 0;

	float4x4 channelMixer = la::matrix_zero_4x4();
	channelMixer(0, 0) = channelEnabled[0] ? 1.0f : 0.0f;	 // r multiplier
	channelMixer(1, 1) = channelEnabled[1] ? 1.0f : 0.0f;	 // g multiplier
	channelMixer(2, 2) = channelEnabled[2] ? 1.0f : 0.0f;	 // b multiplier
	channelMixer(3, 3) = channelEnabled[3] ? 1.0f : 0.0f;	 // a multiplier

	if (enabledRGBChannelsCount == 1)
	{
		if (channelEnabled[0])
		{
			channelMixer(0, 0) = channelMixer(0, 1) = channelMixer(0, 2) = 1;  //rgb all use r
		}
		else if (channelEnabled[1])
		{
			channelMixer(1, 0) = channelMixer(1, 1) = channelMixer(1, 2) = 1;  //rgb all use g
		}
		else if (channelEnabled[2])
		{
			channelMixer(2, 0) = channelMixer(2, 1) = channelMixer(2, 2) = 1;  //rgb all use b
		}
	}
		
	if (enabledRGBChannelsCount == 0 && channelEnabled[3] == true)
	{
		channelMixer(3, 0) = channelMixer(3, 1) = channelMixer(3, 2) = 1;  //rgb all use a
		channelMixer(3, 3) = 0;  //alpha use 1
		channelOffsets.w = 1;  //alpha use 1
	}
	
	if (channelEnabled[3] == false)
		channelOffsets.w = 1;  //alpha use 1

	if (uiRawImageInspectorMaterial)
	{
		uiRawImageInspectorMaterial->SetFloat4x4("ChannelMixer", channelMixer);
		uiRawImageInspectorMaterial->SetFloat4("ChannelOffsets", channelOffsets);
	}

	if (uiRawImage)
	{
		uiRawImage->MarkMeshDirty();
	}
}

//////////////////////////////////////////////////////////////////////////

TranslationHandle::TranslationHandle()
{
}

TranslationHandle::~TranslationHandle()
{
}

void TranslationHandle::Render()
{
	
}

float TranslationHandle::GetMouseRaycastPriority()
{
	return 10;
}

BaseObject* TranslationHandle::OnMouseRaycast(const MouseEvent& ev, const MouseState& state)
{
	return nullptr;
}

void TranslationHandle::OnMouseEvent(BaseObject* raycastResult, const MouseEvent& ev, const MouseState& state)
{

}
