#pragma once

#include "allocator.h"

template<typename TElement, typename TAllocator = DefaultAllocator<TElement>>
class array
{
	typedef uint32_t size_type;
	typedef TElement element_type;
	typedef array<element_type> self_type;
	typedef element_type* iterator_type;
	typedef const element_type* const_iterator_type;

	element_type* _data;
	size_type _size;
	size_type _capacity;

public:
	array()
		: _data(nullptr), _size(0), _capacity(0)
	{}

	~array()
	{}

	void destroy()
	{
		free(_data);
		_data = nullptr;

		_size = 0;
		_capacity = 0;
	}

	void clear()
	{
		for (size_type i = 0; i < _size; i++)
			destruct(_data + i);

		_size = 0;
	}

	void resizeUninitialized(size_type newSize)
	{
		if (newSize > _size)
		{
			if (newSize > _capacity)
				set_capacity(newSize);

			_size = newSize;
		}
		else if (newSize < _size)
		{
			for (size_type i = newSize; i < _size; i++)
				destruct(_data + i);

			_size = newSize;
		}
	}

	void resize(size_type newSize)
	{
		if (newSize > _size)
		{
			if (newSize > _capacity)
				set_capacity(newSize);

			for (size_type i = _size; i < newSize; i++)
				construct(_data + i);

			_size = newSize;
		}
		else if (newSize < _size)
		{
			for (size_type i = newSize; i < _size; i++)
				destruct(_data + i);

			_size = newSize;
		}
	}

	void set_capacity(size_type newCapacity, bool allowShrink = false)
	{
		if (newCapacity < _size)
			newCapacity = _size;

		if ((!allowShrink && (newCapacity > _capacity)) || 
			(allowShrink && newCapacity != _capacity))
		{
			auto dataNew = allocate(sizeof(element_type) * newCapacity);

			move(_data, dataNew, ((newSize > _size) ? _size : newSize));

			free(_data);

			_data = dataNew;
			_capacity = newSize;
		}
	}

	element_type& push_back()
	{
		ensure_space_for_insert(1);

		element_type* p = _data + _size;

		construct(p);

		return *p;
	}

	bool pop_back(element_type& outElement)
	{
		if (_size > 0)
		{
			outElement = _data[_size - 1];

			destruct(_data + _size - 1);
			--_size;

			return true;
		}
		else
		{
			return false;
		}
	}

	element_type pop_back_unchecked()
	{
		assert(_size > 0);

		element_type outElement = _data[_size - 1];

		destruct(_data + _size - 1);
		--_size;

		return outElement;
	}

	void ensure_space_for_insert(size_type newElementCount)
	{
		if (_size + newElementCount > _capacity)
		{
			size_type newCapacity = (_capacity < 1024) ? (_capacity * 2) : _capacity + 1024;
			set_capacity(newCapacity);
		}
	}

	iterator_type begin() { return _data; }
	iterator_type end() { return _data + _size; }

	const_iterator_type cbegin() const { return _data; }
	const_iterator_type cend() const { return _data + _size; }

	size_type size() const { return _size; }
	size_type capacity() const { return _capacity; }

	element_type* data() { return _data; }
	const element_type* data() const { return _data; }

	element_type& front() { assert(_size > 0); return _data[0]; }
	const element_type& front() const { assert(_size > 0); return _data[0]; }

	element_type& back() { assert(_size > 0); return _data[_size - 1]; }
	const element_type& back() const { assert(_size > 0); return _data[_size - 1]; }

	//element_type& operator[] (size_type i) { return _data[i]; }
	//const element_type& operator[] const (size_type i) { return _data[i]; }

private:
	element_type* allocate(size_type byteSize)
	{
		return TAllocator::Allocate(byteSize);
	}

	void free(element_type* p)
	{
		return TAllocator::Free(p);
	}

	void construct(element_type* p)
	{
		new element_type(p) ();
	}

	void destruct(element_type* p)
	{
		p->~element_type();
	}

	void move(element_type* src, element_type* dst, size_type count)
	{
		memcpy(dst, src, sizeof(element_type) * count);
	}
};

template<typename TElement, typename TAllocator = DefaultAllocator<TElement>>
class sparse_array
{
	typedef uint32_t size_type;
	typedef TElement element_type;
	typedef sparse_array<element_type> self_type;
	typedef element_type* iterator_type;
	typedef const element_type* const_iterator_type;

	typedef unsigned long long chunk_type;

	enum { ChunkSize = sizeof(unsigned long long) };

	element_type* _data;
	size_type _size;
	size_type _capacity;

public:
	array()
		: _data(nullptr), _size(0), _capacity(0)
	{}

	~array()
	{}

	void destroy()
	{
		free(_data);
		_data = nullptr;

		_size = 0;
		_capacity = 0;
	}

	void clear()
	{
		for (size_type i = 0; i < _size; i++)
			destruct(_data + i);

		_size = 0;
	}

	void resizeUninitialized(size_type newSize)
	{
		if (newSize > _size)
		{
			if (newSize > _capacity)
				set_capacity(newSize);

			_size = newSize;
		}
		else if (newSize < _size)
		{
			for (size_type i = newSize; i < _size; i++)
				destruct(_data + i);

			_size = newSize;
		}
	}

	void resize(size_type newSize)
	{
		if (newSize > _size)
		{
			if (newSize > _capacity)
				set_capacity(newSize);

			for (size_type i = _size; i < newSize; i++)
				construct(_data + i);

			_size = newSize;
		}
		else if (newSize < _size)
		{
			for (size_type i = newSize; i < _size; i++)
				destruct(_data + i);

			_size = newSize;
		}
	}

	void set_capacity(size_type newCapacity, bool allowShrink = false)
	{
		if (newCapacity < _size)
			newCapacity = _size;

		if ((!allowShrink && (newCapacity > _capacity)) ||
			(allowShrink && newCapacity != _capacity))
		{
			auto dataNew = allocate(sizeof(element_type) * newCapacity);

			move(_data, dataNew, ((newSize > _size) ? _size : newSize));

			free(_data);

			_data = dataNew;
			_capacity = newSize;
		}
	}

	element_type& push_back()
	{
		ensure_space_for_insert(1);

		element_type* p = _data + _size;

		construct(p);

		return *p;
	}

	bool pop_back(element_type& outElement)
	{
		if (_size > 0)
		{
			outElement = _data[_size - 1];

			destruct(_data + _size - 1);
			--_size;

			return true;
		}
		else
		{
			return false;
		}
	}

	element_type pop_back_unchecked()
	{
		assert(_size > 0);

		element_type outElement = _data[_size - 1];

		destruct(_data + _size - 1);
		--_size;

		return outElement;
	}

	void ensure_space_for_insert(size_type newElementCount)
	{
		if (_size + newElementCount > _capacity)
		{
			size_type newCapacity = (_capacity < 1024) ? (_capacity * 2) : _capacity + 1024;
			set_capacity(newCapacity);
		}
	}

	iterator_type begin() { return _data; }
	iterator_type end() { return _data + _size; }

	const_iterator_type cbegin() const { return _data; }
	const_iterator_type cend() const { return _data + _size; }

	size_type size() const { return _size; }
	size_type capacity() const { return _capacity; }

	element_type* data() { return _data; }
	const element_type* data() const { return _data; }

	element_type& front() { assert(_size > 0); return _data[0]; }
	const element_type& front() const { assert(_size > 0); return _data[0]; }

	element_type& back() { assert(_size > 0); return _data[_size - 1]; }
	const element_type& back() const { assert(_size > 0); return _data[_size - 1]; }

	//element_type& operator[] (size_type i) { return _data[i]; }
	//const element_type& operator[] const (size_type i) { return _data[i]; }

private:
	element_type* allocate(size_type byteSize)
	{
		return TAllocator::Allocate(byteSize);
	}

	void free(element_type* p)
	{
		return TAllocator::Free(p);
	}

	void construct(element_type* p)
	{
		new element_type(p) ();
	}

	void destruct(element_type* p)
	{
		p->~element_type();
	}

	void move(element_type* src, element_type* dst, size_type count)
	{
		memcpy(dst, src, sizeof(element_type) * count);
	}
};

template<typename TKey>
class DefaultKeyComparer
{
	bool operator() (const TKey& a, const TKey& b)
	{
		return a < b;
	}
};

template<typename TKey, typename TValue, typename TKeyComparer = DefaultKeyComparer<TKey>>
class SortedArrayMap
{
public:
	typedef uint32_t size_type;
	typedef TKey key_type;
	typedef TValue value_type;
	typedef SortedArrayMap<element_type> self_type;
	typedef TKeyComparer key_comparer_type;

protected:
	key_type* keys;
	value_type* values;

	size_type size;
	size_type capacity;

	bool needSort;

public:
	SortedArrayMap()
		: data(nullptr), size(0), capacity(0), needSort(false)
	{}

	~SortedArrayMap()
	{
		Destroy();
	}

	size_type Size() const { return size; }
	size_type Capacity() const { return capacity; }

	key_type* Keys() { return keys; }
	const key_type* Keys() const { return keys; }

	value_type* Values() { return values; }
	const value_type* Values() const { return values; }

	static size_type InvalidIndex() { return (size_type)-1; }

	size_type Find(const key_type& key) const
	{
		if (needSort)
			Sort();

		key_type* keysEnd = keys + size;

		key_type* first = std::lower_bound(keys, keysEnd, key);
		if ((first != keysEnd) && (*first) == key)
		{
			size_type index = (size_type)(first - keys);
			return index;
		}
		else
			return InvalidIndex();
	}

	void Add()
	{

	}

	void Destroy()
	{
		for (size_type i = 0; i < size; i++)
		{
			keys[i].~key_type();
			values[i].~value_type();
		}

		::free(keys);
		::free(values);

		size = 0;
		capacity = 0;
		needSort = false;
	}

	void Sort()
	{
		needSort = false;

		struct KeyValuePair
		{
			key_type k;
			value_type v;

			bool operator < (const KeyValuePair& other)
			{
				return key_comparer_type(*this, other);
			}
		};

		KeyValuePair* sortArray = new KeyValuePair[size];
		for (size_type i = 0; i < size; i++)
		{
			sortArray[i].k = keys[i];
			sortArray[i].v = values[i];
		}

		std::sort(sortArray, sortArray + size, [](const KeyValuePair& a, const KeyValuePair& b)
			{
				return a.k < b.k;
			});
	}

	void Reserve(size_type newCapacity)
	{
		if (newCapacity > capacity)
		{

		}
	}
};
