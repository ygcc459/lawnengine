#include "fast_compile.h"
#include "LawnEngineInternal.h"
#include "ImGUISystem.h"
#include "InGameEditor.h"
#include "RenderSystem/RenderSystem.h"

#include "UISystem/imgui/imgui.h"
#if VCPP_PLATFORM_FAMILY == VCPP_PLATFORM_FAMILY_WINDOWS
#include "D3D11RHI/D3D11RHI.h"
#include "UISystem/imgui/imgui_impl_dx11.h"
#include "UISystem/imgui/imgui_impl_win32.h"
#endif

void ImGUI::Startup(void* windowHandle)
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();

	AssetPath fontFilePath = AssetPath(AST_ProjectPath, "consola.ttf");
	io.Fonts->AddFontFromFileTTF(fontFilePath.ToAbsolutePath().path.c_str(), 20);

	ImGui::StyleColorsDark();

	D3D11RHIDevice* d3d11Device = static_cast<D3D11RHIDevice*>(RenderSystem::instance().rhiDevice);
	ImGui_ImplDX11_Init(d3d11Device->NativeDevice(), d3d11Device->NativeDeviceContext());

#if VCPP_PLATFORM_FAMILY == VCPP_PLATFORM_FAMILY_WINDOWS
	HWND hwnd = (HWND)windowHandle;
	ImGui_ImplWin32_Init(hwnd);
#endif

	InGameEditor::GetInstance().Startup();
}

void ImGUI::Shutdown()
{
	InGameEditor::GetInstance().Shutdown();

	ImGui_ImplDX11_Shutdown();

#if VCPP_PLATFORM_FAMILY == VCPP_PLATFORM_FAMILY_WINDOWS
	ImGui_ImplWin32_Shutdown();
#endif

	ImGui::DestroyContext();
}

void ImGUI::BeginFrame()
{
	ImGui_ImplDX11_NewFrame();

#if VCPP_PLATFORM_FAMILY == VCPP_PLATFORM_FAMILY_WINDOWS
	ImGui_ImplWin32_NewFrame();
#endif

	ImGui::NewFrame();
}

void ImGUI::Render()
{
	bool show_demo_window = true;
	ImGui::ShowDemoWindow(&show_demo_window);

	InGameEditor::GetInstance().Render();

	ImGui::Render();

	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
}

void ImGUI::EndFrame()
{

}
