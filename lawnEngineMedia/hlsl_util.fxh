
SamplerState g_linear_sampler
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};

SamplerState g_point_sampler
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Clamp;
    AddressV = Clamp;
};

SamplerState g_anisotropic_sampler
{
    Filter = ANISOTROPIC;
    AddressU = Clamp;
    AddressV = Clamp;
};

SamplerState g_cube_sampler
{
    Filter = MIN_MAG_MIP_LINEAR;
};

// transform the x,y position from post-projection space to render-target space
float2 g_pos_pps_to_rts(float2 pos_pps)
{
	return pos_pps.xy * float2(0.5f, -0.5f) + float2(0.5f, 0.5f);
}

float rgb_to_grey(float3 rgb)
{
    return dot(rgb, float3(0.2126f, 0.7152f, 0.0722f));
}

// float3 grey_to_rgb(float grey)
// {

// }


//
// other declations
//
BlendState bs_normalBlend_0rgbaTarget {
    BlendEnable[0] = True;
    BlendEnable[1] = False;
    BlendEnable[2] = False;
    BlendEnable[3] = False;
    BlendEnable[4] = False;
    BlendEnable[5] = False;
    BlendEnable[6] = False;
    BlendEnable[7] = False;
    
    SrcBlend = SRC_ALPHA;
    DestBlend = INV_SRC_ALPHA;
    BlendOp = ADD;
    
    SrcBlendAlpha = One;
    DestBlendAlpha = Zero;
    BlendOpAlpha = Add;
    
    RenderTargetWriteMask[0] = 0x0F;
    RenderTargetWriteMask[1] = 0x00;
    RenderTargetWriteMask[2] = 0x00;
    RenderTargetWriteMask[3] = 0x00;
    RenderTargetWriteMask[4] = 0x00;
    RenderTargetWriteMask[5] = 0x00;
    RenderTargetWriteMask[6] = 0x00;
    RenderTargetWriteMask[7] = 0x00;

    AlphaToCoverageEnable = False;
};

BlendState bs_noBlend_0rgbaTarget {
    BlendEnable[0] = False;
    BlendEnable[1] = False;
    BlendEnable[2] = False;
    BlendEnable[3] = False;
    BlendEnable[4] = False;
    BlendEnable[5] = False;
    BlendEnable[6] = False;
    BlendEnable[7] = False;
    
    RenderTargetWriteMask[0] = 0x0F;
    RenderTargetWriteMask[1] = 0x00;
    RenderTargetWriteMask[2] = 0x00;
    RenderTargetWriteMask[3] = 0x00;
    RenderTargetWriteMask[4] = 0x00;
    RenderTargetWriteMask[5] = 0x00;
    RenderTargetWriteMask[6] = 0x00;
    RenderTargetWriteMask[7] = 0x00;
    
    AlphaToCoverageEnable = False;
};

BlendState bs_noBlend_0rTarget {
    BlendEnable[0] = False;
    BlendEnable[1] = False;
    BlendEnable[2] = False;
    BlendEnable[3] = False;
    BlendEnable[4] = False;
    BlendEnable[5] = False;
    BlendEnable[6] = False;
    BlendEnable[7] = False;
    RenderTargetWriteMask[0] = 0x01;  //only enable red channel
    RenderTargetWriteMask[1] = 0x00;
    RenderTargetWriteMask[2] = 0x00;
    RenderTargetWriteMask[3] = 0x00;
    RenderTargetWriteMask[4] = 0x00;
    RenderTargetWriteMask[5] = 0x00;
    RenderTargetWriteMask[6] = 0x00;
    RenderTargetWriteMask[7] = 0x00;
    AlphaToCoverageEnable = False;
};

RasterizerState rs_wireframe_noCull {
    FillMode = Wireframe;
    CullMode = None;
    FrontCounterClockwise = false;
};

RasterizerState rs_solid_noCull {
    FillMode = Solid;
    CullMode = None;
    FrontCounterClockwise = false;
};

RasterizerState rs_solid_cullBack {
    FillMode = Solid;
    CullMode = Back;
    FrontCounterClockwise = false;
};

RasterizerState rs_solid_cullFront {
    FillMode = Solid;
    CullMode = Front;
    FrontCounterClockwise = false;
};

DepthStencilState dss_lessDepth_noStencil {
    depthenable = true;
    stencilenable = false;
    DepthFunc = LESS;
};

DepthStencilState dss_leDepth_noStencil {
    depthenable = true;
    stencilenable = false;
    DepthFunc = LESS_EQUAL;
};

DepthStencilState dss_noDepthStencil {
    depthenable = false;
    stencilenable = false;
};
