
#include "hlsl_util.fxh"
#include "deferred_lighting.fxh"

Texture2D depthTexture;
Texture2D normalTexture;

float4x4 invProj;
float4x4 proj;

float2 sphere_xy[2] = {float2(0.66667, 0), float2(0, 0.33333)};

float radius = 0.5f;
float ambientAmount = 0.15f;

float epsilon = 0.0001f;
float depth_bias = 0.002f;
float reject_dist_sqr = 1.0f;
float intensity_scale = 5.0f;

struct vs_inputs_type
{
	float3 pos : POSITION;
};

struct vs_outputs_type
{
	float4 sv_position : SV_POSITION;  //position in render-target space
	float2 position_pps : TEXCOORD0;  //position in post-projection space
	float2 position_rts : TEXCOORD1;  //position in render-target space
	float3 view_dir : TEXCOORD3;
};


void ssvo_vs(vs_inputs_type vs_inputs, out vs_outputs_type vs_outputs)
{
	vs_outputs.sv_position = float4(vs_inputs.pos.xyz, 1.0f);
	vs_outputs.position_pps = vs_outputs.sv_position.xy;
	vs_outputs.position_rts = pos_pps_to_rts(vs_outputs.position_pps);
	
	vs_outputs.view_dir = mul(vs_outputs.sv_position, invProj).xyz;
}


float CalcAlchemyObscurance(float3 pos_es, float3 x_dir, float3 y_dir, float3 normal, float radius)
{
	float sqr_radius = radius * radius;
	float obscurance = 0;
	float total_weight = 0;
	for (int i = 0; i < 2; ++ i)
	{
		float2 sxy = sphere_xy[i];
		for (int j = 0; j < 2; ++ j)
		{
			float3 sample_point_offset = (sxy.x * x_dir + sxy.y * y_dir) * radius;
			float3 sample_point = pos_es + sample_point_offset;
			float4 sample_point_ss = mul(float4(sample_point, 1), proj);
		
			float2 tc_sample = pos_pps_to_rts(sample_point_ss.xy / sample_point_ss.w);
			float depth_sample = depthTexture.Sample(linearSampler, tc_sample).r;

			float3 view_dir_sample = normalize(sample_point);
			float3 pos_es_sample = view_dir_sample * (depth_sample / view_dir_sample.z);
			float3 dir_es_sample = pos_es_sample - pos_es;
	
			float dist_sqr = dot( dir_es_sample, dir_es_sample );
			if( dist_sqr < reject_dist_sqr ){
				obscurance += max( 0.0f, dot(dir_es_sample, normal) - depth_sample * depth_bias ) / ( dist_sqr + epsilon );
				total_weight += 1.0f;
			}
			
			sxy = -sxy;
		}
	}

	return (total_weight > 1e-6f) ? 2.0f * intensity_scale * obscurance / total_weight : 0;
}


void ssvo_ps(vs_outputs_type vs_outputs, out float4 outColor : SV_Target)
{
	float depth = depthTexture.Sample(pointSampler, vs_outputs.position_rts).r;
	half3 normal = decodeNormal(normalTexture.Sample(pointSampler, vs_outputs.position_rts));
	
	float3 view_dir = normalize(vs_outputs.view_dir);
	float3 pos_es = view_dir * (depth / view_dir.z);
	
	float3 y_dir = cross(normal, float3(0, 0, 1));
	float3 x_dir = cross(y_dir, normal);
	
	float obscurance = CalcAlchemyObscurance(pos_es, x_dir, y_dir, normal, radius);
	
	outColor = float4(max(0.0, 1.0 - obscurance) * ambientAmount, 0, 0, 1);
	//outColor = float4(0.1f, 0, 0, 1);
}


BlendState blendState {
	BlendEnable[0] = False;
	RenderTargetWriteMask[0] = 0x01;  //only red channel
	AlphaToCoverageEnable = False;
};

DepthStencilState noDepthStencil {
	depthenable = false;
	stencilenable = false;
};

RasterizerState rasterizerState {
	FillMode = Solid;
	CullMode = None;
};

		
technique tech_ssvo
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, ssvo_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, ssvo_ps()));

		SetBlendState(blendState, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetRasterizerState(rasterizerState);
		SetDepthStencilState(noDepthStencil, 0xffffffff);
	}
}
