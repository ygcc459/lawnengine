
// interface for shadow
// return how much a pixel is lighted, 0.0 means totally occluded, 1.0 means totally lighted
float shadow(float3 pos_viewSpace);
