
float4x4 matrix_wvp;  //world * view * proj
float distance;
TextureCube sky_texture;

	
struct vs_inputs_type
{
	float3 pos : POSITION;
};

struct vs_outputs_type
{
	float4 sv_position : SV_POSITION;
	float3 tex : TEXCOORD0;
};


void skybox_vs(vs_inputs_type vs_inputs, out vs_outputs_type vs_outputs)
{
	vs_outputs.sv_position = mul(float4(vs_inputs.pos.xyz, 1.0f), matrix_wvp);
	vs_outputs.tex = vs_inputs.pos.xyz;
}

void skybox_ps(vs_outputs_type vs_outputs, out float4 outColor : SV_TARGET)
{
	float4 albedo = sky_texture.Sample(cubeSampler, vs_outputs.tex);
	outColor = albedo;
}


BlendState noBlend {
	BlendEnable[0] = False;
};

DepthStencilState noDepthStencil {
	depthenable = false;
	stencilenable = false;
};

RasterizerState rasterizerState {
	FillMode = Solid;
	CullMode = None;
};


technique DrawSkyBox
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, skybox_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, skybox_ps()));

		SetBlendState(noBlend, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetRasterizerState(rasterizerState);
		SetDepthStencilState(noDepthStencil, 0xffffffff);
	}
}
