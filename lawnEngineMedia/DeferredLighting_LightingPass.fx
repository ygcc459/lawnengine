
#include "hlsl_util.fxh"
#include "shadow.fxh"
#include "deferred_lighting_util.fxh"
#include "PBRT.fxh"

Texture2D linear_depth_texture;
Texture2D gbuffer0_texture;

cbuffer cb_per_scene
{
	float4x4 matInvProj;
}

struct vs_inputs_type
{
	float3 pos : POSITION;
};

struct vs_outputs_type
{
	float4 sv_position : SV_POSITION;  //position in render-target space
	float4 position_pps : TEXCOORD0;  //position in post-projection space
	
	float3 view_dir : TEXCOORD3;
};

/////////////////////////////////////////////////////////////////

cbuffer cb_directionalLight
{
	float3 dl_lightColor;
	
	float3 dl_lightDirection;  //in view space
}

void vs_directionalLight(vs_inputs_type vs_inputs, out vs_outputs_type vs_outputs)
{
	vs_outputs.sv_position = float4(vs_inputs.pos.xyz, 1.0f);
	vs_outputs.position_pps = vs_outputs.sv_position;
	
	vs_outputs.view_dir = mul(vs_outputs.sv_position, matInvProj).xyz;
}

float4 ps_directionalLight(vs_outputs_type vs_outputs) : SV_Target
{
	float2 texCoord = g_pos_pps_to_rts(vs_outputs.position_pps.xy / vs_outputs.position_pps.w);
	
	float4 gbuffer0 = gbuffer0_texture.Sample(g_point_sampler, texCoord);
	float3 n = gbuffer_decode_normal(gbuffer0);
	float shiness = gbuffer_decode_shiness(gbuffer0);

	float3 l = normalize(-dl_lightDirection);
	float3 v = -normalize(vs_outputs.view_dir);

	float ndotl = dot(n, l);
	if(ndotl <= 0)
		return 0;

	float4 light_acc = lighting_per_light(shiness, dl_lightColor, ndotl, n, l, v);
	
	return light_acc;
}

/////////////////////////////////////////////////////////////////

cbuffer cb_pointLight
{
	float3 pl_lightColor;
	
	float pl_lightRadius;
	float3 pl_lightPosition;  //in view space

	float4x4 pl_light_obj_wvp;
}

void vs_pointLight(vs_inputs_type vs_inputs, out vs_outputs_type vs_outputs)
{
	vs_outputs.sv_position = mul(float4(vs_inputs.pos.xyz, 1.0f), pl_light_obj_wvp);
	vs_outputs.position_pps = vs_outputs.sv_position;
	
	vs_outputs.view_dir = mul(vs_outputs.sv_position, matInvProj).xyz;
}

float4 ps_pointLight(vs_outputs_type vs_outputs) : SV_Target
{
	float2 texCoord = g_pos_pps_to_rts(vs_outputs.position_pps.xy / vs_outputs.position_pps.w);
	
	float depth = linear_depth_texture.Sample(g_point_sampler, texCoord).r;
	
	float4 gbuffer0 = gbuffer0_texture.Sample(g_point_sampler, texCoord);
	float3 n = gbuffer_decode_normal(gbuffer0);
	float shiness = gbuffer_decode_shiness(gbuffer0);
	
	float3 view_dir = normalize(vs_outputs.view_dir);
	float3 pos_es = view_dir * (depth / view_dir.z);
	
	float3 l = pl_lightPosition - pos_es.xyz;

	float ndotl = dot(n, l);
	if(ndotl <= 0)
		return 0;

	float distance = length(l);

	l = normalize(l);
	
	float attenuation = smoothstep(pl_lightRadius, 0.0f, distance);
	
	float4 light_acc = lighting_per_light(shiness, pl_lightColor, ndotl, n, l, -view_dir);
	
	return light_acc * attenuation;
}

/////////////////////////////////////////////////////////////////

cbuffer cb_spotLight
{
	float3 sl_lightColor;
	
	float3 sl_lightPosition;  //in view space
	float3 sl_lightDirection;  //in view space
	float2 sl_light_cos_inner_outer;
	//float sl_sl_light_falloff;

	float4x4 sl_light_obj_wvp;
}

void vs_spotLight(vs_inputs_type vs_inputs, out vs_outputs_type vs_outputs)
{
	vs_outputs.sv_position = mul(float4(vs_inputs.pos.xyz, 1.0f), sl_light_obj_wvp);
	vs_outputs.position_pps = vs_outputs.sv_position;
	
	vs_outputs.view_dir = mul(vs_outputs.sv_position, matInvProj).xyz;
}

float4 ps_spotLight(vs_outputs_type vs_outputs) : SV_Target
{
	float2 texCoord = g_pos_pps_to_rts(vs_outputs.position_pps.xy / vs_outputs.position_pps.ww);
	
	float depth = linear_depth_texture.Sample(g_point_sampler, texCoord).r;
	
	float4 gbuffer0 = gbuffer0_texture.Sample(g_point_sampler, texCoord);
	float3 n = gbuffer_decode_normal(gbuffer0);
	
	float3 v = normalize(vs_outputs.view_dir);
	float3 pos_es = v * (depth / v.z);
	
	float3 l = normalize(sl_lightPosition - pos_es.xyz);
	
	float ndotl = dot(n, l);
	if(ndotl<=0)
		return 0;
	
	float shiness = gbuffer_decode_shiness(gbuffer0);
	
	float4 light_acc = lighting_per_light(shiness, sl_lightColor, ndotl, n, l, -v);

	float ldotd = dot(l, -sl_lightDirection);  //FIXME: -lightDirection is constant, maybe optimize out later
	float lightIntensity = smoothstep(sl_light_cos_inner_outer[1], sl_light_cos_inner_outer[0], ldotd);
	
	return light_acc * lightIntensity;
}

float4 ps_spotLight_shadowed(vs_outputs_type vs_outputs) : SV_Target
{
	float2 texCoord = g_pos_pps_to_rts(vs_outputs.position_pps.xy / vs_outputs.position_pps.w);;

	float depth = linear_depth_texture.Sample(g_linear_sampler, texCoord).r;
	
	float4 gbuffer0 = gbuffer0_texture.Sample(g_linear_sampler, texCoord);
	float3 n = gbuffer_decode_normal(gbuffer0);
	
	float3 v = normalize(vs_outputs.view_dir);
	float3 pos_es = v * (depth / v.z);
	
	float3 l = normalize(sl_lightPosition - pos_es.xyz);
	
	float ndotl = dot(n, l);
	if(ndotl<=0)
		return 0;
	
	float shiness = gbuffer_decode_shiness(gbuffer0);
	
	float4 light_acc = lighting_per_light(shiness, sl_lightColor, ndotl, n, l, -v);

	float ldotd = dot(l, -sl_lightDirection);  //FIXME: -lightDirection is constant, maybe optimize out later
	float lightIntensity = smoothstep(sl_light_cos_inner_outer[1], sl_light_cos_inner_outer[0], ldotd);
	
	float shadowTerm = shadow(pos_es);

	return light_acc * lightIntensity * shadowTerm;
}

/////////////////////////////////////////////////////////////////

BlendState bs_light_obj {
	BlendEnable[0] = True;
	
	SrcBlend = One;
	DestBlend = One;
	BlendOp = Add;
	
	SrcBlendAlpha = One;
	DestBlendAlpha = One;
	BlendOpAlpha = Add;
	
	RenderTargetWriteMask[0] = 0x0F;  //rgba
	AlphaToCoverageEnable = False;
};

DepthStencilState dss_light_obj {
	DepthEnable = true;
	DepthWriteMask = zero;
	DepthFunc = less_equal;

	StencilEnable = true;
	StencilReadMask = 0xFF;
	StencilWriteMask = 0x00;
	
	FrontfaceStencilFunc = equal;
	FrontfaceStencilPass = keep;
	FrontfaceStencilFail = keep;
	FrontfaceStencilDepthFail = keep;
	
	BackfaceStencilFunc = equal;
	BackfaceStencilPass = keep;
	BackfaceStencilFail = keep;
	BackfaceStencilDepthFail = keep;
};


technique10 directionalLight
{
	pass p0
	{
		SetBlendState(bs_light_obj, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		//SetDepthStencilState(dss_light_obj, 0x01);
		//SetRasterizerState(rs_solid_noCull);
		
		SetVertexShader(CompileShader(vs_4_0, vs_directionalLight()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, ps_directionalLight()));
	}
}

technique10 pointLight
{
	pass p0
	{
		SetBlendState(bs_light_obj, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		//SetDepthStencilState(dss_light_obj, 0x01);
		//SetRasterizerState(rs_solid_cullBack);
		
		SetVertexShader(CompileShader(vs_4_0, vs_pointLight()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, ps_pointLight()));
	}
}

technique10 spotLight
{
	pass p0
	{
		SetBlendState(bs_light_obj, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		//SetDepthStencilState(dss_light_obj, 0x01);
		//SetRasterizerState(rs_solid_cullBack);
		
		SetVertexShader(CompileShader(vs_4_0, vs_spotLight()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, ps_spotLight()));
	}
}

technique10 spotLightShadowed
{
	pass p0
	{
		SetBlendState(bs_light_obj, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		//SetDepthStencilState(dss_light_obj, 0x01);
		//SetRasterizerState(rs_solid_cullBack);
		
		SetVertexShader(CompileShader(vs_4_0, vs_spotLight()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, ps_spotLight_shadowed()));
	}
}

