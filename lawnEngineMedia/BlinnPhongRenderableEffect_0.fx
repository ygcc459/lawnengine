
#include "hlsl_util.fxh"
#include "renderable_effect.fxh"
#include "deferred_lighting_util.fxh"
#include "deferred_lighting_use.fxh"
#include "PBRT.fxh"

cbuffer cb_per_object
{
	float4x4 u_mx_wvp : worldviewproj_matrix;
	float4x4 u_mx_wv : worldview_matrix;
}

cbuffer cb_material
{
	float u_shiness = 1.0f;
	float3 u_material_diffuse = {1.0f, 1.0f, 1.0f};
	float3 u_material_specular = {1.0f, 1.0f, 1.0f};
}

Texture2D u_albedo_texture;

/////////////////////////////////////////////////////////////////////////////////////////
// gbuffer

void gbuffer_vs(
	float3 pos : POSITION,
	float3 normal : NORMAL,	
	out float4 out_pos : SV_POSITION,
	out float3 out_normal : TEXCOORD0)  //normal in view space
{
	out_pos = mul(float4(pos.xyz, 1.0f), u_mx_wvp);
	out_normal = mul(float4(normal.xyz, 0.0f), u_mx_wv).xyz;
}

void gbuffer_ps(
	float4 pos : SV_POSITION,
	float3 normal : TEXCOORD0,
	out float4 out_gbuffer0 : SV_Target)
{
	out_gbuffer0 = gbuffer_encode(normalize(normal), u_shiness);
}

technique10 gbuffer
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, gbuffer_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, gbuffer_ps()));

		SetBlendState(bs_dl_gbuffer, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetRasterizerState(rs_solid_cullBack);
		SetDepthStencilState(dss_dl_gbuffer, 0x01);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////
// deferred lighting

void deferred_lighting_vs(
	float3 pos : POSITION,
	float3 normal : NORMAL,	
	float2 albedo_tex : TEXCOORD0,
	out float4 out_pos : SV_POSITION,
	out float4 out_pps_tex : TEXCOORD0,  //pixel pos in pps
	out float2 out_albedo_tex : TEXCOORD1, 
	out float3 out_viewdir_vs : TEXCOORD2)
{
	out_pos = mul(float4(pos.xyz, 1.0f), u_mx_wvp);
	out_pps_tex = out_pos;
	out_albedo_tex = albedo_tex;
	out_viewdir_vs = -mul(float4(pos.xyz, 1.0f), u_mx_wv).xyz;
}

void deferred_lighting_ps(
	float4 pos : SV_POSITION,
	float4 pps_tex : TEXCOORD0,  //pixel pos in pps
	float2 albedo_tex : TEXCOORD1,
	float3 viewdir_vs : TEXCOORD2,
	out float4 out_color : SV_Target)
{
	float2 rts_tex = g_pos_pps_to_rts(pps_tex.xy / pps_tex.ww);

	float4 light_acc = dl_light_acculumation_texture.Sample(g_point_sampler, rts_tex);

	float4 gbuffer0 = dl_gbuffer0_texture.Sample(g_point_sampler, rts_tex);
	float3 normal = gbuffer_decode_normal(gbuffer0);
	float shiness = gbuffer_decode_shiness(gbuffer0);

	float3 albedo_color = u_albedo_texture.Sample(g_linear_sampler, albedo_tex).rgb;

	out_color.rgb = lighting_composition(
						u_material_diffuse * albedo_color, u_material_specular, shiness, 
						normal, normalize(viewdir_vs), 
						light_acc);
	out_color.a = 1.0f;
}

technique10 deferred_lighting
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, deferred_lighting_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, deferred_lighting_ps()));

		SetBlendState(bs_noBlend_0rgbaTarget, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetRasterizerState(rs_solid_cullBack);
		SetDepthStencilState(dss_dl_deferred_lighting, 0xffffffff);
	}
}
