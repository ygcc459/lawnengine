
float3 fresnel(float3 f0, float ldoth)
{
	return f0 + (1.0f - f0) * pow(1.0f - ldoth, 5);
}

//
// L_out(v) = sum( [ lighting(..) for each light ] )
//
// lighting(..) = pi * brdf(l,v) * c_light * dot(n, l)
//
float3 lighting(
	float3 Kd, float3 Ks, float roughness, 
	float3 c_light, 
	float3 n, float3 l, float3 v)
{
	float3 h = normalize(l + v);

	float ndotl = max(dot(n, l), 0);
	float ndoth = max(dot(n, h), 0);

	float3 F = fresnel(Ks, max(dot(l, h), 0));
	
	float m2 = roughness * roughness;
	float ndoth2 = ndoth * ndoth;
	float D_beckmann = exp((ndoth2 - 1) / (m2 * ndoth2)) * 0.25f / (m2 * ndoth2*ndoth2);
	//float D_gaussian = gaussian_weight * exp(-acos(ndoth) / m2);
	float D = D_beckmann;

	float Vis = 1.0f;

	return c_light * ndotl * (Kd + 0.25f * F * D * Vis);
}

// the returned rgba color is to be added in lighting buffer directly
float4 lighting_per_light(
	float roughness, 
	float3 c_light, 
	float ndotl, float3 n, float3 l, float3 v)
{
	float3 h = normalize(l + v);
	float ndoth = max(dot(n, h), 0.0f);

	float m2 = roughness * roughness;
	float ndoth2 = ndoth * ndoth;
	float D_beckmann = exp((ndoth2 - 1) / (m2 * ndoth2)) * 0.25f / (m2 * ndoth2*ndoth2);
	//float D_gaussian = gaussian_weight * exp(-acos(ndoth) / m2);
	float D = D_beckmann;

	float Vis = 1.0f;

	return float4(c_light * ndotl, D * Vis);
}

float3 lighting_composition(
	float3 Kd, float3 Ks, float roughness, 
	float3 n, float3 v, 
	float4 light_acc)
{
	float3 F = fresnel(Ks, max(dot(n, v), 0));

	float3 c = Kd * light_acc.rgb + 0.25f * F * light_acc.a * light_acc.rgb;
	return c;
}
