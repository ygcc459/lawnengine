
#include "shadow.fxh"

Texture2D shadowMap;
float4x4 matViewInverse;
float4x4 matLightViewProj;

float shadow(float3 pos_viewSpace)
{
	float4 pos_world = mul(float4(pos_viewSpace, 1.0f), matViewInverse);
	float4 pos_light_ndc = mul(pos_world, matLightViewProj);
	pos_light_ndc = pos_light_ndc / pos_light_ndc.w;
	
	float2 pos_rts;
	pos_rts.x = 0.5f + 0.5f * pos_light_ndc.x;
	pos_rts.y = 0.5f - 0.5f * pos_light_ndc.y;
	
	float this_depth = pos_light_ndc.z;
	float occluder_depth = shadowMap.Sample(linearSampler, pos_rts).r;
	
	float shadow = (occluder_depth < this_depth - 0.0001f) ? 0.0f : 1.0f;
	
	return shadow;
}
