
#include "hlsl_util.fxh"
#include "FullScreenQuadRenderPass.fx"

Texture2D sourceTexture;

float4 down_sample_3x3_single_channel_ps(vs_outputs_type vs_outputs) : SV_Target
{
	float color = 0.0f;

	for(int y = -1; y <= 1; y++)
	{
		for(int x = -1; x <= 1; x++)
		{
			float c = sourceTexture.Sample(g_linear_sampler, vs_outputs.position_rts, int2(x, y)).r;
			color += max(c, 0.0f);
		}
	}

	color /= 9.0f;

	return float4(color, color, color, 1.0f);
}


technique10 down_sample_3x3_single_channel
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, pass_through_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, down_sample_3x3_single_channel_ps()));

		SetBlendState(bs_noBlend_0rgbaTarget, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetRasterizerState(rs_solid_noCull);
		SetDepthStencilState(dss_noDepthStencil, 0xffffffff);
	}
}
