
#include "hlsl_util.fxh"
#include "FullScreenQuadRenderPass.fx"


Texture2D source_texture;


void add_ps(vs_outputs_type vs_outputs, out float4 outColor : SV_Target)
{
	outColor = source_texture.Sample(linearSampler, vs_outputs.position_rts);
}

void scatter_add_ps(vs_outputs_type vs_outputs, out float4 outColor : SV_Target)
{
	outColor = source_texture.Sample(linearSampler, vs_outputs.position_rts).rrrr;
}


BlendState addBlend {
	BlendEnable[0] = True;
	BlendEnable[1] = False;
	BlendEnable[2] = False;
	BlendEnable[3] = False;
	BlendEnable[4] = False;
	BlendEnable[5] = False;
	BlendEnable[6] = False;
	BlendEnable[7] = False;
	
	SrcBlend = One;
	DestBlend = One;
	BlendOp = Add;
	
	SrcBlendAlpha = One;
	DestBlendAlpha = Zero;
	BlendOpAlpha = Add;

	RenderTargetWriteMask[0] = 0x0F;
	RenderTargetWriteMask[1] = 0x00;
	RenderTargetWriteMask[2] = 0x00;
	RenderTargetWriteMask[3] = 0x00;
	RenderTargetWriteMask[4] = 0x00;
	RenderTargetWriteMask[5] = 0x00;
	RenderTargetWriteMask[6] = 0x00;
	RenderTargetWriteMask[7] = 0x00;
	AlphaToCoverageEnable = False;
};

DepthStencilState noDepthStencil {
	depthenable = false;
	stencilenable = false;
};

RasterizerState rasterizerState {
	FillMode = Solid;
	CullMode = None;
};


technique tech_add
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, pass_through_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, add_ps()));

		SetBlendState(addBlend, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetRasterizerState(rasterizerState);
		SetDepthStencilState(noDepthStencil, 0xffffffff);
	}
}

technique tech_scatter_add
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, pass_through_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, scatter_add_ps()));

		SetBlendState(addBlend, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetRasterizerState(rasterizerState);
		SetDepthStencilState(noDepthStencil, 0xffffffff);
	}
}
