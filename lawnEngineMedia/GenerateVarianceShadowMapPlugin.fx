
float3 lightPosition;

void generateShadowMap_ps(float4 sv_position : SV_POSITION, float dist : TEXCOORD0, out float4 out_dist : SV_Target0)
{
	out_dist = float4(dist, dist * dist, 0, 0);
}

BlendState generateShadowMapBlend {
	BlendEnable[0] = False;
	BlendEnable[1] = False;
	BlendEnable[2] = False;
	BlendEnable[3] = False;
	BlendEnable[4] = False;
	BlendEnable[5] = False;
	BlendEnable[6] = False;
	BlendEnable[7] = False;
	RenderTargetWriteMask[0] = 0x03;  //only enable red,green channel
	RenderTargetWriteMask[1] = 0x00;
	RenderTargetWriteMask[2] = 0x00;
	RenderTargetWriteMask[3] = 0x00;
	RenderTargetWriteMask[4] = 0x00;
	RenderTargetWriteMask[5] = 0x00;
	RenderTargetWriteMask[6] = 0x00;
	RenderTargetWriteMask[7] = 0x00;
	AlphaToCoverageEnable = False;
};
