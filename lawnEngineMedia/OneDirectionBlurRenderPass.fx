
#include "hlsl_util.fxh"
#include "FullScreenQuadRenderPass.fx"


//
// blur_step = blur_direction / viewportSize;
//   in which: blur_direction = float2(1,0) to blur along X axis,
//                            = float2(0,1) to blur along Y axis.
//             viewportSize = float2(viewportWidth, viewportHeight), in pixels
//
float2 blur_step;
float blur_length;

Texture2D sourceTexture;


float4 blur_ps(vs_outputs_type vs_outputs) : SV_Target
{
	//note: float2 blur_step = blur_direction / viewportSize;
	
	float start_offset = (blur_length - 1.0f) / 2.0f;
	float2 coord = vs_outputs.position_rts - start_offset * blur_step;
	
	float4 color = float4(0.0f, 0.0f, 0.0f, 0.0f);
	for(int i=0; i < blur_length; ++i)
	{
		color += sourceTexture.Sample(g_linear_sampler, coord);

		coord += blur_step;
	}
	
	return color / blur_length;
}


technique10 blur
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, pass_through_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, blur_ps()));

		SetBlendState(bs_noBlend_0rgbaTarget, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetRasterizerState(rs_solid_noCull);
		SetDepthStencilState(dss_noDepthStencil, 0xffffffff);
	}
}
