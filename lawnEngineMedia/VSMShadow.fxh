
#include "shadow.fxh"

Texture2D shadowMap;

float4x4 matLight;  //transform a pixel from camera view space to light post-projection space
float3 lightPosition;  //light position in view space

SamplerState vsmShadowSampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Clamp;
	AddressV = Clamp;
};

float lightBleeding_reduce_amount = 0.2f;
float sceneDepthBias = -0.015f;

float linstep(float min, float max, float v)
{
	return saturate((v-min)/(max-min));
}

float ReduceLightBleeding(float p_max, float Amount)
{
	return linstep(Amount, 1, p_max);
}

float VSM_FILTER(float2 tex, float fragDepth)
{
	float lit = (float)0.0f;
	
	float2 moments = shadowMap.Sample(vsmShadowSampler, float3(tex, 0.0f)).xy;
	
	float E_x2 = moments.y;
	float Ex_2 = moments.x * moments.x;
	float variance = E_x2 - Ex_2;    
	float mD = (moments.x - fragDepth);
	float mD_2 = mD * mD;
	float p = variance / (variance + mD_2);
	lit = max(p, fragDepth <= moments.x);
	
	lit = ReduceLightBleeding(lit, lightBleeding_reduce_amount);
	return lit;
}

float shadow(float3 pos_viewSpace)
{
	float4 pos_light_ndc = mul(float4(pos_viewSpace, 1.0f), matLight);
	pos_light_ndc = pos_light_ndc / pos_light_ndc.w;
	
	float2 pos_rts = float2(0.5f, 0.5f) + float2(0.5f, -0.5f) * pos_light_ndc.xy;
	
	float this_depth = length(lightPosition.xyz - pos_viewSpace.xyz);
	
	float shadow = VSM_FILTER(pos_rts, this_depth + sceneDepthBias);
	
	return shadow;
}
