
#include "hlsl_util.fxh"

cbuffer cb_per_line : register(b0)
{
	float3 line_pt1, line_pt2;
	float4 line_color;
}

cbuffer cb_per_scene : register(b1)
{
	float4x4 mat_vp;
}

void vs_line_nobuffer(
	uint vertex_id : SV_VertexID, 
	out float4 sv_position : SV_POSITION, 
	out float4 out_color : TEXCOORD0)
{
	float4 vertex_pos;
	if(vertex_id==0)
		vertex_pos = float4(line_pt1, 1.0f);
	else if(vertex_id==1)
		vertex_pos = float4(line_pt2, 1.0f);

	sv_position = mul(vertex_pos, mat_vp);
	out_color = line_color;
}

void ps_line(
	float4 sv_position : SV_POSITION, 
	float4 in_color : TEXCOORD0,
	out float4 out_color : SV_Target)
{
	out_color = in_color;
}

technique10 tech_line
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, vs_line_nobuffer()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, ps_line()));

		SetBlendState(bs_normalBlend_0rgbaTarget, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetRasterizerState(rs_wireframe_noCull);
		SetDepthStencilState(dss_lessDepth_noStencil, 0xffffffff);
	}
}
