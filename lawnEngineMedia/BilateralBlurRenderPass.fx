
#include "hlsl_util.fxh"
#include "FullScreenQuadRenderPass.fx"


Texture2D sourceTexture;
Texture2D diffTexture;

int kernel_radius;
float2 src_tex_size;

float init_g;
float blur_factor;
float sharpness_factor;


float BilateralDistribution(float r, float d)
{
	return init_g * exp(-r * r * blur_factor - d * d * sharpness_factor);
}


float4 CalcBlur(float2 tc0, bool x_dir)
{
	float color = 0;

	float center_d = diffTexture.Sample(pointSampler, tc0).r;

	float total_weight = 0;
	for (int i = -kernel_radius; i <= kernel_radius; ++ i)
	{
		float2 offset = float2(i * src_tex_size.y, 0);
		float2 tc = tc0 + (x_dir ? offset.xy : offset.yx);
		float ddiff = diffTexture.Sample(pointSampler, tc).r - center_d;
		float weight = BilateralDistribution(i, ddiff);
		color += sourceTexture.Sample(pointSampler, tc).r * weight;
		total_weight += weight;
	}

	float final = color / total_weight;
	return float4(final, final, final, final);
}


void blurX_ps(in vs_outputs_type vs_outputs, out float4 outColor : SV_Target)
{
	outColor = CalcBlur(vs_outputs.position_rts, true);
}

void blurY_ps(in vs_outputs_type vs_outputs, out float4 outColor : SV_Target)
{
	outColor = CalcBlur(vs_outputs.position_rts, false);
}


BlendState noBlend {
	BlendEnable[0] = False;
};

BlendState mulBlend {
	BlendEnable[0] = True;
	
	SrcBlend = Zero;
	DestBlend = Src_Color;
	BlendOp = ADD;
	
	SrcBlendAlpha = Zero;
	DestBlendAlpha = Zero;
	BlendOpAlpha = Add;
	
	RenderTargetWriteMask[0] = 0x07;  //rgb
	AlphaToCoverageEnable = False;
};

BlendState addBlend {
	BlendEnable[0] = True;
	
	SrcBlend = One;
	DestBlend = One;
	BlendOp = ADD;
	
	SrcBlendAlpha = Zero;
	DestBlendAlpha = Zero;
	BlendOpAlpha = Add;
	
	RenderTargetWriteMask[0] = 0x07;  //rgb
	AlphaToCoverageEnable = False;
};

DepthStencilState noDepthStencil {
	depthenable = false;
	stencilenable = false;
};

RasterizerState rasterizerState {
	FillMode = Solid;
	CullMode = None;
};


technique BlurX
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, pass_through_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, blurX_ps()));

		SetBlendState(noBlend, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetRasterizerState(rasterizerState);
		SetDepthStencilState(noDepthStencil, 0xffffffff);
	}
}

technique BlurY
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, pass_through_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, blurY_ps()));

		SetBlendState(noBlend, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetRasterizerState(rasterizerState);
		SetDepthStencilState(noDepthStencil, 0xffffffff);
	}
}

technique BlurX_add
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, pass_through_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, blurX_ps()));

		SetBlendState(addBlend, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetRasterizerState(rasterizerState);
		SetDepthStencilState(noDepthStencil, 0xffffffff);
	}
}

technique BlurY_add
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, pass_through_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, blurY_ps()));

		SetBlendState(addBlend, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetRasterizerState(rasterizerState);
		SetDepthStencilState(noDepthStencil, 0xffffffff);
	}
}
