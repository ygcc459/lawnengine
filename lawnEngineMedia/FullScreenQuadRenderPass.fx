	
struct vs_inputs_type
{
	float3 pos : POSITION;
};

struct vs_outputs_type
{
	float4 sv_position : SV_POSITION;  //position in render-target space
	float2 position_pps : TEXCOORD0;  //position in post-projection space
	float2 position_rts : TEXCOORD1;  //position in render-target space
};

void pass_through_vs(vs_inputs_type vs_inputs, out vs_outputs_type vs_outputs)
{
	vs_outputs.sv_position = float4(vs_inputs.pos.xyz, 1.0f);
	vs_outputs.position_pps = vs_outputs.sv_position.xy;
	vs_outputs.position_rts = vs_outputs.position_pps * float2(0.5f, -0.5f) + float2(0.5f, 0.5f);
}
