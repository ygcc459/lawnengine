
#include "hlsl_util.fxh"
#include "FullScreenQuadRenderPass.fx"


Texture2D hdr_image;
Texture2D greyed_hdr_image_1x1;  //the down-sampled, greyed, hdr image
Texture2D bloomed_light_area;  //the fully-bloomed light area

float middle_gray = 0.72f;
float lum_white = 1.5f;


void tone_mapping_ps(vs_outputs_type vs_outputs, out float4 outColor : SV_Target)
{
	float4 color = hdr_image.Sample(g_point_sampler, vs_outputs.position_rts);
	float average_luminance = greyed_hdr_image_1x1.Sample(g_point_sampler, float2(0, 0)).r;
	float3 bloomed_light = bloomed_light_area.Sample(g_linear_sampler, vs_outputs.position_rts).rgb;

	// Tone mapping
	color.rgb *= middle_gray / (average_luminance + 0.001f);
	color.rgb *= (1.0f + color/lum_white);
	color.rgb /= (1.0f + color);

	color.rgb += 0.6f * bloomed_light;
	color.a = 1.0f;

	outColor = color;
}


technique10 HDRFinalPass
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, pass_through_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, tone_mapping_ps()));

		SetBlendState(bs_noBlend_0rgbaTarget, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetRasterizerState(rs_solid_noCull);
		SetDepthStencilState(dss_noDepthStencil, 0xffffffff);
	}
}
