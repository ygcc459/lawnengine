
shared cbuffer cb_shared_per_scene
{
	float4x4 g_mx_view : ViewMatrix;
	float4x4 g_mx_proj : ProjMatrix;
	float4x4 g_mx_view_proj : ViewProjMatrix;
}
