
#include "hlsl_util.fxh"
#include "FullScreenQuadRenderPass.fx"


Texture2D sourceTexture;
float3 luminance_weight = float3(0.2125f, 0.7154f, 0.0721f);


void measure_luminance_rgb(vs_outputs_type vs_outputs, out float4 outColor : SV_Target)
{
	float4 src = sourceTexture.Sample(g_point_sampler, vs_outputs.position_rts);
	
	float luminance = dot(src.rgb, luminance_weight);
	
	outColor = float4(luminance, luminance, luminance, 0.0f);
}


technique10 MeasureLuminance
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, pass_through_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, measure_luminance_rgb()));

		SetBlendState(bs_noBlend_0rgbaTarget, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetRasterizerState(rs_solid_noCull);
		SetDepthStencilState(dss_noDepthStencil, 0xffffffff);
	}
}
