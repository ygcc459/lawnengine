
#include "hlsl_util.fxh"
#include "FullScreenQuadRenderPass.fx"


Texture2D depthTexture;

float2 q;  //where: q.y = camera->FarPlane() / (camera->FarPlane() - camera->NearPlane()); q.x = camera->NearPlane() * q.y;


void toLinearDepth_ps(vs_outputs_type vs_outputs, out float4 outColor : SV_Target)
{
	float depth = depthTexture.Sample(g_point_sampler, vs_outputs.position_rts).r;
	
	//float q = camera->FarPlane() / (camera->FarPlane() - camera->NearPlane());
	//float near_mul_q = camera->NearPlane() * q
	//float d = near_mul_q / (q - depth);
	
	float d = q.x / (q.y - depth);
	
	outColor = float4(d, 0, 0, 1);
}


BlendState depthOnlyNoBlend {
	BlendEnable[0] = False;
	BlendEnable[1] = False;
	BlendEnable[2] = False;
	BlendEnable[3] = False;
	BlendEnable[4] = False;
	BlendEnable[5] = False;
	BlendEnable[6] = False;
	BlendEnable[7] = False;
	RenderTargetWriteMask[0] = 0x01;  //only enable red channel
	RenderTargetWriteMask[1] = 0x00;
	RenderTargetWriteMask[2] = 0x00;
	RenderTargetWriteMask[3] = 0x00;
	RenderTargetWriteMask[4] = 0x00;
	RenderTargetWriteMask[5] = 0x00;
	RenderTargetWriteMask[6] = 0x00;
	RenderTargetWriteMask[7] = 0x00;
	AlphaToCoverageEnable = False;
};

DepthStencilState noDepthStencil {
	depthenable = false;
	stencilenable = false;
};

RasterizerState rasterizerState {
	FillMode = Solid;
	CullMode = None;
};


technique10 toLinearDepth
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, pass_through_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, toLinearDepth_ps()));

		SetBlendState(depthOnlyNoBlend, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetRasterizerState(rasterizerState);
		SetDepthStencilState(noDepthStencil, 0xffffffff);
	}
}

