
//
// deferred lighting:
//   gbuffer:
//     renderTarget[0]: rgba16 uint
//       rg = normal.xy (normalized normal in view space)
//       b = (empty)
//       a = shiness (material shiness)
//     depth-buffer: 24bit depth + 8bit stencil
//       stencil: 0b00000000
//                         ^-- marks if this pixel is on an object or in background. 0=background, 1=object
//

float4 gbuffer_encode(float3 normal, float shiness)
{
	return float4(normal.x, normal.y, 0.0f, shiness);
}

float3 gbuffer_decode_normal(float4 enc)
{
	float3 n;
	n.xy = enc.xy;
	n.z = -sqrt(1-dot(n.xy, n.xy));
	return n;
}

float gbuffer_decode_shiness(float4 enc)
{
	return enc.w;
}

BlendState bs_dl_gbuffer {
	BlendEnable[0] = False;
	BlendEnable[1] = False;
	BlendEnable[2] = False;
	BlendEnable[3] = False;
	BlendEnable[4] = False;
	BlendEnable[5] = False;
	BlendEnable[6] = False;
	BlendEnable[7] = False;
	RenderTargetWriteMask[0] = 0x0F;  //rgba
	RenderTargetWriteMask[1] = 0x00;
	RenderTargetWriteMask[2] = 0x00;
	RenderTargetWriteMask[3] = 0x00;
	RenderTargetWriteMask[4] = 0x00;
	RenderTargetWriteMask[5] = 0x00;
	RenderTargetWriteMask[6] = 0x00;
	RenderTargetWriteMask[7] = 0x00;
	AlphaToCoverageEnable = False;
};

DepthStencilState dss_dl_gbuffer {
	DepthEnable = true;
	DepthWriteMask = all;
	DepthFunc = less;

	StencilEnable = true;
	StencilReadMask = 0xFF;
	StencilWriteMask = 0xFF;
	
	FrontfaceStencilFunc = always;
	FrontfaceStencilPass = replace;
	FrontfaceStencilFail = keep;
	FrontfaceStencilDepthFail = keep;
	
	BackfaceStencilFunc = always;
	BackfaceStencilPass = replace;
	BackfaceStencilFail = keep;
	BackfaceStencilDepthFail = keep;
};

DepthStencilState dss_dl_deferred_lighting {
    DepthEnable = true;
    DepthWriteMask = zero;
    DepthFunc = less_equal;

    StencilEnable = false;
};
