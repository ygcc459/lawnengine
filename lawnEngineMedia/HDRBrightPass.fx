
#include "hlsl_util.fxh"
#include "FullScreenQuadRenderPass.fx"


Texture2D hdr_image;
Texture2D greyed_hdr_image_1x1;  //the down-sampled, greyed, hdr_image

float bright_threshold = 0.5f;
float middle_gray = 0.72f;
float lum_white = 1.5f;


void filterLight_downSampled3x3_ps(vs_outputs_type vs_outputs, out float4 outColor : SV_Target)
{
	float average_luminance = greyed_hdr_image_1x1.Sample(g_point_sampler, float2(0, 0)).r;

	float3 color = float3(0.0f, 0.0f, 0.0f);
	for(int y = -1; y <= 1; y++) 
		for(int x = -1; x <= 1; x++)
			color += hdr_image.Sample(g_point_sampler, vs_outputs.position_rts, int2(x,y)).rgb;

	color /= 9;

	// Bright pass and tone mapping
	color = max(0.0f, color - bright_threshold);
	color *= middle_gray / (average_luminance + 0.001f);
	color *= (1.0f + color/lum_white);
	color /= (1.0f + color);

	outColor = float4(color, 1.0f);
}


technique10 HDRBrightPass
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, pass_through_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, filterLight_downSampled3x3_ps()));

		SetBlendState(bs_noBlend_0rgbaTarget, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetRasterizerState(rs_solid_noCull);
		SetDepthStencilState(dss_noDepthStencil, 0xffffffff);
	}
}
