
#include "shadow.fxh"


Texture2D depthTexture;
Texture2D normalTexture;
Texture2D albedoTexture;

cbuffer per_scene
{
	float4x4 matInvProj;
}

float3 lightColor = {1.0f, 1.0f, 1.0f};
float4 materialDiffuse = {0.8f, 0.2f, 0.2f, 1.0f};

cbuffer cb_directionalLight
{
	float3 lightDirection;  //in view space
}

cbuffer cb_pointLight
{
	float lightRadius;
	float3 lightPosition;  //in view space
}

ps_outputs
{
	float4 color : SV_Target;
}

//half3 decodeNormal(half4 enc)
//{
//	half4 nn = enc*half4(2,2,0,0) + half4(-1,-1,1,-1);
//	half l = dot(nn.xyz,-nn.xyw);
//	nn.z = l;
//	nn.xy *= sqrt(l);
//	half3 nn2 = nn.xyz * 2 + half3(0,0,-1);
//	nn2.z = -nn2.z;
//	return nn2;
//}

half3 decodeNormal(float4 enc)
{
	half3 n;
	n.xy = enc.xy;
	n.z = -sqrt(1-dot(n.xy, n.xy));
	return n;
}

float3 phong_lighting(
	half3 normal, float3 vecToLight, float3 vecToEye, 
	float3 albedo, 
	float3 materialAmbient, float3 materialDiffuse, float3 materialSpecular, float materialSpecularPower)
{
	float ndotl = max(0, dot(normal, vecToLight));
	
	float3 halfway = normalize(vecToEye + vecToLight);
	float ndoth = max(0, dot(normal, halfway));

	float3 color = (materialAmbient.rgb + ndotl * materialDiffuse.rgb) * albedo.rgb + pow(ndoth, materialSpecularPower) * materialSpecular.rgb;

	return color * lightColor;
}


void ps_directionalLight(@vs_outputs, out @ps_outputs)
{
	float2 texCoord = float2(0.5f, 0.5f) + vs_outputs.position_pps * float2(0.5f, -0.5f);
	
	float depth = depthTexture.Sample(pointSampler, texCoord).rgb;
	if(depth==1.0f)
		discard;
	
	half3 normal = decodeNormal(normalTexture.Sample(pointSampler, texCoord));  //normal is in view space
	float3 albedo = albedoTexture.Sample(pointSampler, texCoord).rgb;
	
	float4 pixel_position = mul(float4(vs_outputs.position_pps.x, vs_outputs.position_pps.y, depth, 1.0f), matInvProj);  //pixel_position is in eye space
	pixel_position.xyz = pixel_position.xyz / pixel_position.www;
	pixel_position.w = 1.0f;
	
	float3 vecToLight = normalize(-lightDirection.xyz);
	float3 vecToEye = normalize(/*float3(0,0,0)*/ - pixel_position.xyz);  //camera is at (0,0,0) in view space
	
	float3 color = phong_lighting(normal, vecToLight, vecToEye, albedo.rgb, 
							float3(0.1f, 0.1f, 0.1f), float3(0.7f, 0.7f, 0.7f), float3(0.4f, 0.4f, 0.4f), 8.0f);
							
	color.rgb *= shadow(pixel_position.xyz);
	
	ps_outputs.color = float4(color.rgb, 1.0f);
}

void ps_pointLight(@vs_outputs, out @ps_outputs)
{
	float2 texCoord = float2(0.5f, 0.5f) + vs_outputs.position_pps * float2(0.5f, -0.5f);
	
	float depth = depthTexture.Sample(pointSampler, texCoord).rgb;
	if(depth==1.0f)
		discard;
	
	half3 normal = decodeNormal(normalTexture.Sample(pointSampler, texCoord));  //normal is in view space
	float3 albedo = albedoTexture.Sample(pointSampler, texCoord).rgb;
	
	float4 pixel_position = mul(float4(vs_outputs.position_pps.x, vs_outputs.position_pps.y, depth, 1.0f), matInvProj);  //pixel_position is in eye space
	pixel_position.xyz = pixel_position.xyz / pixel_position.www;
	pixel_position.w = 1.0f;
	
	float3 vecToLight = lightPosition - pixel_position.xyz;
	float3 vecToEye = /*float3(0,0,0)*/ - pixel_position.xyz;  //camera is at (0,0,0) in view space
	
	float distance = length(vecToLight);
	float attenuation = smoothstep(lightRadius, 0.0f, distance);
	
	float3 color = phong_lighting(normal, normalize(vecToLight), normalize(vecToEye), albedo.rgb, 
							float3(0.1f, 0.1f, 0.1f), float3(0.7f, 0.7f, 0.7f), float3(0.4f, 0.4f, 0.4f), 2.0f);
							
	color.rgb *= attenuation * shadow(pixel_position.xyz);
	
	ps_outputs.color = float4(color.rgb, 1.0f);
}


BlendState blendState {
	BlendEnable[0] = True;
	
	SrcBlend = ONE;
	DestBlend = ONE;
	BlendOp = ADD;
	
	SrcBlendAlpha = One;
	DestBlendAlpha = Zero;
	BlendOpAlpha = Add;
	
	RenderTargetWriteMask[0] = 0x0F;
	AlphaToCoverageEnable = False;
};

DepthStencilState depthStencilState {
	depthenable = false;
	stencilenable = false;
};

RasterizerState rasterizerState {
	FillMode = Solid;
	CullMode = None;
};


technique directionalLight
{
	pass p0
	{
		SetBlendState(blendState, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		SetDepthStencilState(depthStencilState, 0);
		SetRasterizerState(rasterizerState);
		
		SetVertexShader(CompileShader(vs_4_0, pass_through_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, ps_directionalLight()));
	}
}

technique pointLight
{
	pass p0
	{
		SetBlendState(blendState, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		SetDepthStencilState(depthStencilState, 0);
		SetRasterizerState(rasterizerState);
		
		SetVertexShader(CompileShader(vs_4_0, pass_through_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, ps_pointLight()));
	}
}
