
//#include "lighting_blinnPhong.fxh"
#include "lighting_cookTorrence.fxh"

/**
//
// the following functions should be implemented in lighting models
//

//
// L_out(v) = sum( [ lighting(..) for each light ] )
//
// lighting(..) = pi * brdf(l,v) * c_light * dot(n, l)
//
float3 lighting(
	float3 Kd, float3 Ks, float shiness, 
	float3 c_light, 
	float3 n, float3 l, float3 v);

// the returned rgba color is to be added in lighting buffer directly
float4 lighting_per_light(
	float shiness, 
	float3 c_light, 
	float ndotl, float3 n, float3 l, float3 v);

float3 lighting_composition(
	float3 Kd, float3 Ks, float shiness, 
	float3 n, float3 v, 
	float4 light_acc);
**/
