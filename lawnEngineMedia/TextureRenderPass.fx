
#include "hlsl_util.fxh"
#include "FullScreenQuadRenderPass.fx"


Texture2D source_texture;
float4 color_mul, color_add;


void ps_pointSampler(vs_outputs_type vs_outputs, out float4 outColor : SV_Target)
{
	float4 color = source_texture.Sample(g_point_sampler, vs_outputs.position_rts);
	
	outColor = color * color_mul + color_add;
}


technique10 texture_render
{
	pass p0
	{
		SetVertexShader(CompileShader(vs_4_0, pass_through_vs()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, ps_pointSampler()));

		SetBlendState(bs_noBlend_0rgbaTarget, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetRasterizerState(rs_solid_noCull);
		SetDepthStencilState(dss_noDepthStencil, 0xffffffff);
	}
}
