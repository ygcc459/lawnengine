
float roughness_to_spec_power(float roughness)
{
	return 4.0f * (1 / roughness);
}

//
// L_out(v) = sum( [ lighting(..) for each light ] )
//
// lighting(..) = pi * brdf(l,v) * c_light * dot(n, l)
//
float3 lighting(
	float3 Kd, float3 Ks, float roughness, 
	float3 c_light, 
	float3 n, float3 l, float3 v)
{
	float3 h = normalize(l + v);

	float ndotl = max(dot(n, l), 0);
	float ndoth = max(dot(n, h), 0);

	float spec_power = roughness_to_spec_power(roughness);

	return c_light * ndotl * (Kd + Ks * pow(ndoth, spec_power));
}

// the returned rgba color is to be added in lighting buffer directly
float4 lighting_per_light(
	float roughness, 
	float3 c_light, 
	float ndotl, float3 n, float3 l, float3 v)
{
	float3 h = normalize(l + v);
	float ndoth = max(dot(n, h), 0.0f);

	float spec_power = roughness_to_spec_power(roughness);

	return float4(c_light * ndotl, pow(ndoth, spec_power));
}

float3 lighting_composition(
	float3 Kd, float3 Ks, float roughness, 
	float3 n, float3 v, 
	float4 light_acc)
{
	float3 c = Kd * light_acc.rgb + Ks * light_acc.a * light_acc.rgb;
	return c;
	//return pow(c, 1/2.2);
	//return pow(c, 2.2);
}
